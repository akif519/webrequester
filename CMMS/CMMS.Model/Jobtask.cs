﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    // <summary>
    /// This class is used to Define Model for Table - jobtask
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("jobtask")]
    public sealed class Jobtask : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WoNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WoNo { get; set; }

        /// <summary>
        /// Gets or sets the SeqId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SeqId { get; set; }

        /// <summary>
        /// Gets or sets the Details value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Details { get; set; }

        /// <summary>
        /// Gets or sets the AltDetails value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string AltDetails { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the job plan id value.
        /// </summary>
        [NotMapped]
        public int JobPlanID { get; set; }

        /// <summary>
        /// Gets or sets the job plan No value.
        /// </summary>
        [NotMapped]
        public string JobPlanNo { get; set; }

        /// <summary>
        /// Gets or sets the Job Plan Name value.
        /// </summary>
        [NotMapped]
        public string JobPlanName { get; set; }

        /// <summary>
        /// Gets or sets the Alternative Job Plan Name value.
        /// </summary>
        [NotMapped]
        public string AltJobPlanName { get; set; }
        #endregion
    }
}
