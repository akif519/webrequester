﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Model;

namespace CMMS.Model
{
    public class EmailNotificationProcessedRule : BaseModel
    {
        public Emailnotificationrule ObjectEmailNotificationRule { get; set; }
        public List<EmailReceiptant> ListEmailReceiptant_Employee { get; set; }
        public EmailTemplateWorkRequest ObjectWorkRequest { get; set; }
        public EmailTemplateWorkOrder ObjectWorkOrder { get; set; }
        public EmailTemplateSupplier ObjectSupplier { get; set; }
    }

    public class EmailTemplateSupplier : BaseModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public int SupplierID { get; set; }
        public string SupplierNo { get; set; }
        public string SupplierName { get; set; }
        public string AltSupplierName { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}
