//-----------------------------------------------------------------------
// <copyright file="TblJobPlanItem.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - tblJobPlanItems
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("tblJobPlanItems")]
	public sealed class TblJobPlanItem : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the JobPlanID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int? JobPlanID { get; set; }

		/// <summary>
		/// Gets or sets the SeqID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int SeqID { get; set; }

		/// <summary>
		/// Gets or sets the Description value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the AltDescription value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string AltDescription { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the HDN seq identifier.
        /// </summary>
        /// <value>
        /// The HDN seq identifier.
        /// </value>
        [NotMapped]
        public int? HdnSeqID { get; set; }

		#endregion
	}
}
