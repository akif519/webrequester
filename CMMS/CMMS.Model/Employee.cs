//-----------------------------------------------------------------------
// <copyright file="Employee.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using CMMS.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;



	/// <summary>
	/// This class is used to Define Model for Table - employees
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>16-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("employees")]
	public sealed class Employee : BaseModel
	{
        #region Properties
                
        public string EmployeeNO { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeID { get; set; }
        
        public Nullable<int> L2ID { get; set; }
                
        public Nullable<int> CategoryID { get; set; }
        
        public Nullable<int> WorkTradeID { get; set; }
        
        public Nullable<int> UserGroupId { get; set; }
                
        public string Name { get; set; }
        
        public string AltName { get; set; }
        
        public string Positions { get; set; }
        
        public string Extension { get; set; }
        
        public string WorkPhone { get; set; }
        
        public string HandPhone { get; set; }
        
        public string Email { get; set; }
        
        public string Fax { get; set; }
        
        public string Housephone { get; set; }
        
        
        public string Address { get; set; }
        
        public string OfficeLocation { get; set; }
        
        public Nullable<decimal> HourlySalary { get; set; }
        
        public Nullable<decimal> OverTime1 { get; set; }
        
        public Nullable<decimal> OverTime2 { get; set; }
        
        public Nullable<decimal> OverTime3 { get; set; }
        
        public string Accessibility { get; set; }
        
        public string UserID { get; set; }
        
        public string Password { get; set; }
        
        public Nullable<int> Central { get; set; }
        
        public Nullable<int> Auth { get; set; }
        
        public Nullable<int> ExecLevel { get; set; }
        
        public Nullable<int> WebAccess { get; set; }
        
        public Nullable<int> OpenWorkorders { get; set; }
        
        public Nullable<int> ReopenWorkorders { get; set; }
        
        public Nullable<int> Connected { get; set; }
        
        public Nullable<int> WRConnected { get; set; }
        
        public Nullable<int> AllowAdjustment { get; set; }
        
        public Nullable<decimal> pr_auto_amount { get; set; }
        
        public string EmpDeptCode { get; set; }
        
        public Nullable<int> AllowCancelPO { get; set; }
        
        public Nullable<int> ConnectedMW { get; set; }
        
        public Nullable<int> WOReturn { get; set; }
        
        public Nullable<int> WOIssue { get; set; }
        
        public Nullable<int> hide_cost { get; set; }
        
        public string LanguageCode { get; set; }
        
        public Nullable<int> AllowInterSiteTransfer { get; set; }
        
        public string postal_address { get; set; }
        
        public Nullable<int> maintDeptID { get; set; }
        
        public Nullable<int> MaintSubDeptID { get; set; }
        
        public Nullable<int> MaintDivisionID { get; set; }
        
        public Nullable<int> EmployeeStatusId { get; set; }
        
        public string CreatedBy { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
        
        public string ModifiedBy { get; set; }
        
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        
        public Nullable<bool> IsStoredSupervisor { get; set; }
        
        public Nullable<bool> IsLogIn { get; set; }
        
        public Nullable<decimal> POAuthorisationLimit { get; set; }
        
        public Nullable<bool> EmpHasCert { get; set; }
        
        public string ClassificationLevel { get; set; }
        
        public string ID_No { get; set; }
        
        public Nullable<System.DateTime> ID_ExpiryDate { get; set; }
        
        public string Remarks { get; set; }
        
        public string PassportNo { get; set; }
        
        public Nullable<System.DateTime> PassportExp { get; set; }
        
        public string Nationality { get; set; }
        
        public Nullable<int> EmployeeProfessionID { get; set; }
        
        public Nullable<int> NationalityID { get; set; }
        
        public Nullable<int> ProjectTypeID { get; set; }

		#endregion

        //For Conlimit,Session & Heartbeat

        public string LoginDateTime { get; set; }

        public string LogoutDateTime { get; set; }

        public string IPAddress { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string ExceedConnectionLimitMessage { get; set; }

        public string InActiveUserMessage { get; set; }

        public string SameUserAlreadyLoginMessage { get; set; }

        public int? HeartbeatValInSec { get; set; }

        public DateTime? UTCDateTime { get; set; }
                
        public DateTime? LastConfigChangeDateTime { get; set; }

        public string ClientCode { get; set; }


	}
}
