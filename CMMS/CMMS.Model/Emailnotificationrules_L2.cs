//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_L2.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_L2
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_L2")]
	public sealed class Emailnotificationrules_L2 : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleCityID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleCityID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public int L1ID { get; set; }
        [NotMapped]
        public int L2ClassID { get; set; }
        [NotMapped]
        public int SectorID { get; set; }
        [NotMapped]
        public string L2Code { get; set; }
        [NotMapped]
        public string L2name { get; set; }
        [NotMapped]
        public string L2Altname { get; set; }
        [NotMapped]
        public string L1No { get; set; }
        [NotMapped]
        public string SectorCode { get; set; }
        [NotMapped]
        public string L2ClassCode { get; set; }

        [NotMapped]
        public string IsChecked { get; set; }

		#endregion
	}
}
