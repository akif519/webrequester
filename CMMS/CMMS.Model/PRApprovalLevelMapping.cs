//-----------------------------------------------------------------------
// <copyright file="PRApprovalLevelMapping.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PRApprovalLevelMappings
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PRApprovalLevelMappings")]
    public sealed class PRApprovalLevelMapping : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AutoId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoId { get; set; }

        /// <summary>
        /// Gets or sets the PRApprovalLevelId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PRApprovalLevelId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Mandatory is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool Mandatory { get; set; }

        /// <summary>
        /// Gets or sets the Auth_status_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Auth_status_id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsPrintingEnable is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsPrintingEnable { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the level.
        /// </summary>
        /// <value>
        /// The name of the level.
        /// </value>
        [NotMapped]
        public string LevelName { get; set; }

        /// <summary>
        /// Gets or sets the level no.
        /// </summary>
        /// <value>
        /// The level no.
        /// </value>
        [NotMapped]
        public int LevelNo { get; set; }

        /// <summary>
        /// Gets or sets the auth_status_desc.
        /// </summary>
        /// <value>
        /// The auth_status_desc.
        /// </value>
        [NotMapped]
        public string auth_status_desc { get; set; }

        [NotMapped]
        public int? L1ID { get; set; }
        #endregion
    }
}
