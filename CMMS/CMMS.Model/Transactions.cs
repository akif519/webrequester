﻿//-----------------------------------------------------------------------
// <copyright file="Stockcode.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - Transactions
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>20-Dec-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Transactions")]
    public sealed class Transactions : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the TransactionID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransactionID { get; set; }

        /// <summary>
        /// Gets or sets the TransactionNO value.
        /// </summary>
        [Required(ErrorMessage="Transaction No cannot be blank.")]
        [StringLength(50)]
        public string TransactionNO { get; set; }

        /// <summary>
        /// Gets or sets the Transaction Type value.
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Gets or sets the Transaction Type value.
        /// </summary>
        [NotMapped]
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the Alternative Transaction Type value.
        /// </summary>
        [NotMapped]
        public string AltTypeName { get; set; }

        /// <summary>
        /// Gets or sets the Transaction Status value.
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedByName value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedByName value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
