﻿//-----------------------------------------------------------------------
// <copyright file="Warrantycontract.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - warrantycontract
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("warrantycontract")]
    public sealed class WarrantyContract : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Warranty_contractID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Warranty_contractID { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_contract value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Warranty_contract { get; set; }

        /// <summary>
        /// Gets or sets the AltWarranty_contract value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltWarranty_contract { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
