//-----------------------------------------------------------------------
// <copyright file="Mr_authorisation_statu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - mr_authorisation_status
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("mr_authorisation_status")]
	public sealed class Mr_authorisation_statu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the Auth_status_id value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Auth_status_id { get; set; }

		/// <summary>
		/// Gets or sets the Auth_status_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Auth_status_desc { get; set; }

		/// <summary>
		/// Gets or sets the Altauth_status_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string Altauth_status_desc { get; set; }

		#endregion
	}
}
