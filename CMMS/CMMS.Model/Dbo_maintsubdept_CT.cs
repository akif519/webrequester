//-----------------------------------------------------------------------
// <copyright file="Dbo_maintsubdept_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_maintsubdept_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_maintsubdept_CT")]
	public sealed class Dbo_maintsubdept_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }


		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string MaintSubDeptDesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptAltDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string MaintSubDeptAltDesc { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintSubDeptID value.
        ///// </summary>
        //public int? MaintSubDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDeptID value.
        ///// </summary>
        //public int? MaintDeptID { get; set; }

		
        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the CmActivityID value.
        ///// </summary>
        //public int? CmActivityID { get; set; }

        ///// <summary>
        ///// Gets or sets the IsFromCM value.
        ///// </summary>
        //public bool? IsFromCM { get; set; }

		#endregion
	}
}
