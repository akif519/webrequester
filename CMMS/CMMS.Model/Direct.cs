﻿//-----------------------------------------------------------------------
// <copyright file="Direct.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - direct
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("direct")]
    public sealed class Direct : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the DirectID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DirectID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

        /// <summary>
        /// Gets or sets the Supplierid value.
        /// </summary>
        public int? Supplierid { get; set; }

        /// <summary>
        /// Gets or sets the PRNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PRNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDesc value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(1000, ErrorMessage = "*")]
        public string StockDesc { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDesc value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string AltStockDesc { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Date)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the Price value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public decimal? Price { get; set; }

        /// <summary>
        /// Gets or sets the Invoicenumber value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string Invoicenumber { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }
            
        /// <summary>
        /// Gets or sets the SupplierNo value.
        /// </summary>
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the Total Price value.
        /// </summary>
        [NotMapped]
        public decimal? TotalPrice { get; set; }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

        #endregion
    }
}
