﻿//-----------------------------------------------------------------------
// <copyright file="Page.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Pages
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>12-Sep-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Pages")]
    public sealed class Page : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PageID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PageID { get; set; }

        /// <summary>
        /// Gets or sets the PageName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "*")]
        public string PageName { get; set; }

        /// <summary>
        /// Gets or sets the DisplayColumns value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(-1, ErrorMessage = "*")]
        public string DisplayColumns { get; set; }

        /// <summary>
        /// Gets or sets the TableJoins value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string TableJoins { get; set; }

        #endregion
    }
}
