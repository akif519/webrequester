//-----------------------------------------------------------------------
// <copyright file="Dbo_assets_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_assets_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_assets_CT")]
	public sealed class Dbo_assets_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }


        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetAltDescription { get; set; }

        [NotMapped]
        public string assetstatusdesc { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string assetcatcode { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string L3No { get; set; }

        [NotMapped]
        public string L4No { get; set; }

        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the ModelNumber value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string ModelNumber { get; set; }

        /// <summary>
        /// Gets or sets the SerialNumber value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string SerialNumber { get; set; }

        [NotMapped]
        public string Criticality { get; set; }
        
        [NotMapped]
        public string WorkTrade { get; set; }

        [NotMapped]
        public string AuthEmployee { get; set; }

        [NotMapped]
        public string Warranty_contract { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        [NotMapped]
        public string PartsListNo { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the AssetID value.
        ///// </summary>
        //public int? AssetID { get; set; }

        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetCatID value.
        ///// </summary>
        //public int? AssetCatID { get; set; }

        ///// <summary>
        ///// Gets or sets the PartsListID value.
        ///// </summary>
        //public int? PartsListID { get; set; }

        ///// <summary>
        ///// Gets or sets the ContractorID value.
        ///// </summary>
        //public int? ContractorID { get; set; }

        ///// <summary>
        ///// Gets or sets the SupplierID value.
        ///// </summary>
        //public int? SupplierID { get; set; }

        ///// <summary>
        ///// Gets or sets the CriticalityID value.
        ///// </summary>
        //public int? CriticalityID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetStatusID value.
        ///// </summary>
        //public int? AssetStatusID { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeID value.
        ///// </summary>
        //public int? EmployeeID { get; set; }

        ///// <summary>
        ///// Gets or sets the Warranty_ContractID value.
        ///// </summary>
        //public int? Warranty_ContractID { get; set; }


		

        ///// <summary>
        ///// Gets or sets the DateAcquired value.
        ///// </summary>
        //public string DateAcquired { get; set; }

        ///// <summary>
        ///// Gets or sets the PurchasePrice value.
        ///// </summary>
        //public float? PurchasePrice { get; set; }

        ///// <summary>
        ///// Gets or sets the Comments value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string Comments { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo1 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo1 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo2 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo2 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo3 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo3 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo4 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo4 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo5 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo5 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo6 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo6 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo7 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo7 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo8 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo8 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo9 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo9 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo10 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo10 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo11 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo11 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo12 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo12 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo13 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo13 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo14 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo14 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo15 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo15 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo16 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo16 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo17 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo17 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo18 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo18 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo19 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo19 { get; set; }

        ///// <summary>
        ///// Gets or sets the NameplateInfo20 value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NameplateInfo20 { get; set; }

        ///// <summary>
        ///// Gets or sets the NextSchedMaint value.
        ///// </summary>
        //public string NextSchedMaint { get; set; }

        ///// <summary>
        ///// Gets or sets the Warranty_ContractExpiry value.
        ///// </summary>
        //public string Warranty_ContractExpiry { get; set; }

        ///// <summary>
        ///// Gets or sets the Warranty_ContractNotes value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string Warranty_ContractNotes { get; set; }

        ///// <summary>
        ///// Gets or sets the DataDisposed value.
        ///// </summary>
        //public string DataDisposed { get; set; }

        ///// <summary>
        ///// Gets or sets the NotesToTech value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string NotesToTech { get; set; }

        ///// <summary>
        ///// Gets or sets the EstLife value.
        ///// </summary>
        //public float? EstLife { get; set; }

        ///// <summary>
        ///// Gets or sets the Manufacturer value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string Manufacturer { get; set; }

        ///// <summary>
        ///// Gets or sets the WorkTradeID value.
        ///// </summary>
        //public int? WorkTradeID { get; set; }

        ///// <summary>
        ///// Gets or sets the GroupID value.
        ///// </summary>
        //public int? GroupID { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

		
		
        ///// <summary>
        ///// Gets or sets the AssetSubCatID value.
        ///// </summary>
        //public int? AssetSubCatID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetConditionID value.
        ///// </summary>
        //public int? AssetConditionID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetClassID value.
        ///// </summary>
        //public int? AssetClassID { get; set; }

		#endregion
	}
}
