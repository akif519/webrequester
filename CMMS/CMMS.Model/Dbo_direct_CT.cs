//-----------------------------------------------------------------------
// <copyright file="Dbo_direct_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_direct_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_direct_CT")]
	public sealed class Dbo_direct_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

		/// <summary>
		/// Gets or sets the __$operation value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int operation { get; set; }

	

        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDesc value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string StockDesc { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDesc value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string AltStockDesc { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the Price value.
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Gets or sets the Invoicenumber value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Invoicenumber { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        public string Date { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the DirectID value.
        ///// </summary>
        //public int? DirectID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

		

        ///// <summary>
        ///// Gets or sets the Supplierid value.
        ///// </summary>
        //public int? Supplierid { get; set; }

        ///// <summary>
        ///// Gets or sets the PRNo value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string PRNo { get; set; }

		
		

		

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

		

		#endregion
	}
}
