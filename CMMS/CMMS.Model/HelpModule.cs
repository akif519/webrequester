//-----------------------------------------------------------------------
// <copyright file="HelpModule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - HelpModule
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>20-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("HelpModule")]
	public sealed class HelpModule : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the HelpModuleID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int HelpModuleID { get; set; }

		/// <summary>
		/// Gets or sets the ModuleName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModuleName { get; set; }

		/// <summary>
		/// Gets or sets the ParentID value.
		/// </summary>
		public int? ParentID { get; set; }

		/// <summary>
		/// Gets or sets the DisplayOrder value.
		/// </summary>
		public int? DisplayOrder { get; set; }

		/// <summary>
		/// Gets or sets the HasHelpDescription value.
		/// </summary>
		public bool? HasHelpDescription { get; set; }

		/// <summary>
		/// Gets or sets the ModuleAccess value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ModuleAccess { get; set; }

        /// <summary>
        /// Gets or sets the name of the page.
        /// </summary>
        /// <value>
        /// The name of the page.
        /// </value>
        [NotMapped]
        public string PageName { get; set; }

        /// <summary>
        /// Gets or sets the help moduel detail identifier.
        /// </summary>
        /// <value>
        /// The help moduel detail identifier.
        /// </value>
        [NotMapped]
        public int? HelpModuelDetailID { get; set; }

        /// <summary>
        /// Gets or sets the language identifier.
        /// </summary>
        /// <value>
        /// The language identifier.
        /// </value>
        [NotMapped]
        public int? LanguageID { get; set; }

        [NotMapped]
        public string HelpTitle { get; set; }

        [NotMapped]
        public int ChildCount { get; set; }

        [NotMapped]
        public string PropertyName { get; set; }

        [NotMapped]
        public bool Value { get; set; }

		#endregion
	}
}
