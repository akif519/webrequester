//-----------------------------------------------------------------------
// <copyright file="Cleaningelement.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cleaningelement
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cleaningelement")]
	public sealed class Cleaningelement : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CleaningElementId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CleaningElementId { get; set; }

		/// <summary>
		/// Gets or sets the Element value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string Element { get; set; }

		/// <summary>
		/// Gets or sets the AltElement value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltElement { get; set; }

		#endregion
	}
}
