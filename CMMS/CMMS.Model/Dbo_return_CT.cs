//-----------------------------------------------------------------------
// <copyright file="Dbo_return_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_return_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_return_CT")]
    public sealed class Dbo_return_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string StockDescription { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }

        [NotMapped]
        public decimal? AvgPrice { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the Comment value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the RetPrice value.
        /// </summary>
        public decimal? RetPrice { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the ReturnID value.
        ///// </summary>
        //public int? ReturnID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the StockID value.
        ///// </summary>
        //public int? StockID { get; set; }

        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the SubStoreID value.
        ///// </summary>
        //public int? SubStoreID { get; set; }

        

        

        ///// <summary>
        ///// Gets or sets the Person value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Person { get; set; }

        ///// <summary>
        ///// Gets or sets the Comment value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string Comment { get; set; }

        ///// <summary>
        ///// Gets or sets the RetPrice value.
        ///// </summary>
        //public decimal? RetPrice { get; set; }

        ///// <summary>
        ///// Gets or sets the BatchNo value.
        ///// </summary>
        //[StringLength(30, ErrorMessage = "*")]
        //public string BatchNo { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

      
        #endregion
    }
}
