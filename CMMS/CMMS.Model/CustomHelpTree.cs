﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class CustomHelpTree
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool HasChild { get; set; }
        public bool? HasHelpDescription { get; set; }
        public int? HelpModuelDetailID { get; set; }
    }
}
