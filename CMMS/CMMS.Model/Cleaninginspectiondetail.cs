//-----------------------------------------------------------------------
// <copyright file="Cleaninginspectiondetail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cleaninginspectiondetails
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>24-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cleaninginspectiondetails")]
	public sealed class Cleaninginspectiondetail : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CleaningInspectionDetailId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CleaningInspectionDetailId { get; set; }

		/// <summary>
		/// Gets or sets the CleaningInspectionId value.
		/// </summary>
		public int? CleaningInspectionId { get; set; }

		/// <summary>
		/// Gets or sets the LocationID value.
		/// </summary>
		public int? LocationID { get; set; }

		/// <summary>
		/// Gets or sets the InspectionDate value.
		/// </summary>
		public DateTime? InspectionDate { get; set; }

		/// <summary>
		/// Gets or sets the CleaningGroupId value.
		/// </summary>
		public int CleaningGroupId { get; set; }

		/// <summary>
		/// Gets or sets the Floor value.
		/// </summary>
		public int Floor { get; set; }

		/// <summary>
		/// Gets or sets the Wall value.
		/// </summary>
		public int Wall { get; set; }

		/// <summary>
		/// Gets or sets the Window value.
		/// </summary>
		public int Window { get; set; }

		/// <summary>
		/// Gets or sets the Ceiling value.
		/// </summary>
		public int Ceiling { get; set; }

		/// <summary>
		/// Gets or sets the Furniture value.
		/// </summary>
		public int Furniture { get; set; }

		/// <summary>
		/// Gets or sets the WasteBag value.
		/// </summary>
		public int WasteBag { get; set; }

		/// <summary>
		/// Gets or sets the Consumables value.
		/// </summary>
		public int Consumables { get; set; }

		/// <summary>
		/// Gets or sets the Remarks value.
		/// </summary>
		[StringLength(2000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string Remarks { get; set; }

		/// <summary>
		/// Gets or sets the ArabicRemarks value.
		/// </summary>
		[StringLength(2000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string ArabicRemarks { get; set; }

		/// <summary>
		/// Gets or sets the TotalPercentage value.
		/// </summary>
		public float? TotalPercentage { get; set; }

		/// <summary>
		/// Gets or sets the SatisfactionResult value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string SatisfactionResult { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
        /// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string LocationDescription { get; set; }

        [NotMapped]
        public string LocationAltDescription { get; set; }

        [NotMapped]
        public string GroupDesc { get; set; }

		#endregion
	}
}
