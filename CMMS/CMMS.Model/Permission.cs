﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - permissions
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>13-Sep-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("permissions")]
    public sealed class Permission : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PermissionId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionId { get; set; }

        /// <summary>
        /// Gets or sets the ModuleName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "*")]
        public string ModuleName { get; set; }

        /// <summary>
        /// Gets or sets the SubModuleName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "*")]
        public string SubModuleName { get; set; }

        /// <summary>
        /// Gets or sets the PermissionDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string PermissionDesc { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the AllowAccessByUserGroup value.
        /// </summary>
        [NotMapped]
        public bool AllowAccessByUserGroup { get; set; }

        /// <summary>
        /// Gets or sets the AllowAccessByEmployee value.
        /// </summary>
        [NotMapped]
        public bool AllowAccessByEmployee { get; set; }

        /// <summary>
        /// Gets or sets the AllowAccess value.
        /// </summary>
        [NotMapped]
        public bool AllowAccess { get; set; }

        /// <summary>
        /// Gets or sets the AllowAccess value.
        /// </summary>
        [NotMapped]
        public string PermissionName { get; set; }
        #endregion
    }

    public class PermissionAccess
    {
        public bool JobRequest_JobRequest_Allowaccess { get; set; }
        public bool JobRequest_JobRequest_CreatenewJobRequest { get; set; }
        public bool JobRequest_JobRequest_EditJobRequest { get; set; }
        public bool JobRequest_JobRequest_CancelJobRequest { get; set; }
        public bool JobRequest_JobRequest_Export { get; set; }

        public bool JobRequest_JobRequestEscalation_Allowaccess { get; set; }
        public bool JobRequest_JobRequestEscalation_Export { get; set; }
        public bool JobOrder_JobOrderEscalation_Allowaccess { get; set; }
        public bool JobOrder_JobOrderEscalation_Export { get; set; }

        public bool JobOrder_JobOrder_Allowaccess { get; set; }
        public bool JobOrder_JobOrder_Createnewrecords { get; set; }
        public bool JobOrder_JobOrder_Editrecords { get; set; }
        public bool JobOrder_JobOrder_AssignEmployees { get; set; }
        public bool JobOrder_JobOrder_CancelJobOrder { get; set; }
        public bool JobOrder_JobOrder_CloseJobOrder { get; set; }
        public bool JobOrder_JobOrder_ReopenaclosedJobOrder { get; set; }
        public bool JobOrder_JobOrder_ViewJobOrdercost { get; set; }
        public bool JobOrder_JobOrder_IssueParts { get; set; }
        public bool JobOrder_JobOrder_ReturnParts { get; set; }
        public bool JobOrder_JobOrder_SaveDirectIssues { get; set; }
        public bool JobOrder_JobOrder_Export { get; set; }

        public bool Assets_Assets_Allowaccess { get; set; }
        public bool Assets_Assets_Createnewrecords { get; set; }
        public bool Assets_Assets_Editrecords { get; set; }
        public bool Assets_Assets_TransferAssets { get; set; }
        public bool Assets_Assets_Deleterecords { get; set; }
        public bool Assets_Assets_ChangeMastersCodes { get; set; }
        public bool Locations_Locations_Allowaccess { get; set; }
        public bool Locations_Locations_Createnewrecords { get; set; }
        public bool Locations_Locations_Editrecords { get; set; }
        public bool Locations_Locations_Deleterecords { get; set; }
        public bool Locations_Locations_ChangeMastersCodes { get; set; }
        public bool Locations_Locations_Export { get; set; }
        public bool Locations_Locations_LimitAccesstoLocation { get; set; }
        public bool Preventive_PMChecklists_Allowaccess { get; set; }
        public bool Preventive_PMChecklists_Createnewrecords { get; set; }
        public bool Preventive_PMChecklists_Editrecords { get; set; }
        public bool Preventive_PMChecklists_Deleterecords { get; set; }
        public bool Preventive_PMChecklists_ChangeMastersCodes { get; set; }
        public bool Preventive_PMChecklists_Export { get; set; }

        public bool Preventive_TimeSchedules_Allowaccess { get; set; }
        public bool Preventive_TimeSchedules_Createnewrecords { get; set; }
        public bool Preventive_TimeSchedules_Editrecords { get; set; }
        public bool Preventive_TimeSchedules_Deleterecords { get; set; }
        public bool Preventive_TimeSchedules_GeneratePMWorkOrders { get; set; }
        public bool Preventive_TimeSchedules_ChangeMastersCodes { get; set; }
        public bool Preventive_TimeSchedules_Export { get; set; }

        public bool Preventive_MeterSchedules_Allowaccess { get; set; }
        public bool Preventive_MeterSchedules_Createnewrecords { get; set; }
        public bool Preventive_MeterSchedules_Export { get; set; }

        public bool Preventive_MeterSchedules_Editrecords { get; set; }
        public bool Preventive_MeterSchedules_Deleterecords { get; set; }
        public bool Preventive_MeterSchedules_UpdateReadings { get; set; }
        public bool Preventive_MeterSchedules_GeneratePMWorkOrders { get; set; }
        public bool Preventive_MeterSchedules_ChangeMastersCodes { get; set; }
        public bool Stores_Stores_Allowaccess { get; set; }
        public bool Stores_Stores_Createnewrecords { get; set; }
        public bool Stores_Stores_Editrecords { get; set; }
        public bool Stores_Stores_Deleterecords { get; set; }
        public bool Stores_Stores_ChangeMastersCodes { get; set; }
        public bool ItemRequisitions_ItemRequisitions_Allowaccess { get; set; }
        public bool ItemRequisitions_ItemRequisitions_Createnewrecords { get; set; }
        public bool ItemRequisitions_ItemRequisitions_Editrecords { get; set; }
        public bool ItemRequisitions_ItemRequisitions_AuthoriseRequisitions { get; set; }
        public bool ItemRequisitions_ItemRequisitions_CloseRequisitions { get; set; }
        public bool ItemRequisitions_ItemRequisitions_CancelRequisitions { get; set; }
        public bool ItemRequisitions_ItemRequisitions_Export { get; set; }
        public bool Employee_Employee_Allowaccess { get; set; }
        public bool Employee_Employee_Createnewrecords { get; set; }
        public bool Employee_Employee_Editrecords { get; set; }
        public bool Employee_Employee_Deleterecords { get; set; }
        public bool Employee_Employee_ChangeMastersCodes { get; set; }
        public bool Employee_Employee_Export { get; set; }
        public bool Management_User_Allowaccess { get; set; }
        public bool Management_User_Createnewrecords { get; set; }
        public bool Management_User_Editrecords { get; set; }
        public bool Management_User_Deleterecords { get; set; }
        public bool Management_User_Export { get; set; }
        public bool SetupConfiguration_Organisation_Allowaccess { get; set; }
        public bool SetupConfiguration_Organisation_Createnewrecords { get; set; }
        public bool SetupConfiguration_Organisation_Editrecords { get; set; }
        public bool SetupConfiguration_Organisation_Deleterecords { get; set; }
        public bool SetupConfiguration_Organisation_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_City_Allowaccess { get; set; }
        public bool SetupConfiguration_City_Createnewrecords { get; set; }
        public bool SetupConfiguration_City_Editrecords { get; set; }
        public bool SetupConfiguration_City_Deleterecords { get; set; }
        public bool SetupConfiguration_City_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_Area_Allowaccess { get; set; }
        public bool SetupConfiguration_Area_Createnewrecords { get; set; }
        public bool SetupConfiguration_Area_Editrecords { get; set; }
        public bool SetupConfiguration_Area_Deleterecords { get; set; }
        public bool SetupConfiguration_Area_ChangeMastersCodes { get; set; }
        public bool Locations_Street_Allowaccess { get; set; }
        public bool Locations_Street_Createnewrecords { get; set; }
        public bool Locations_Street_Editrecords { get; set; }
        public bool Locations_Street_Deleterecords { get; set; }
        public bool Locations_Street_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_Allowaccess { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_Createnewrecords { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_Editrecords { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_Deleterecords { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_Allowaccess { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_Createnewrecords { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_Editrecords { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_Deleterecords { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_Allowaccess { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_Createnewrecords { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_Editrecords { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_Deleterecords { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_Allowaccess { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_Createnewrecords { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_Editrecords { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_Deleterecords { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_FailureCodes_Allowaccess { get; set; }
        public bool SetupConfiguration_FailureCodes_Createnewrecords { get; set; }
        public bool SetupConfiguration_FailureCodes_Editrecords { get; set; }
        public bool SetupConfiguration_FailureCodes_Deleterecords { get; set; }
        public bool SetupConfiguration_FailureCodes_ChangeMastersCodes { get; set; }

        public bool SetupConfiguration_Specifications_Allowaccess { get; set; }
        public bool SetupConfiguration_Specifications_Createnewrecords { get; set; }
        public bool SetupConfiguration_Specifications_Editrecords { get; set; }
        public bool SetupConfiguration_Specifications_Deleterecords { get; set; }
        public bool SetupConfiguration_Specifications_Export { get; set; }

        public bool SetupConfiguration_AssetCategories_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetCategories_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetCategories_Editrecords { get; set; }
        public bool SetupConfiguration_AssetCategories_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetCategories_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_Suppliers_Allowaccess { get; set; }
        public bool SetupConfiguration_Suppliers_Createnewrecords { get; set; }
        public bool SetupConfiguration_Suppliers_Editrecords { get; set; }
        public bool SetupConfiguration_Suppliers_Deleterecords { get; set; }
        public bool SetupConfiguration_Suppliers_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_BoMList_Allowaccess { get; set; }
        public bool SetupConfiguration_BoMList_Createnewrecords { get; set; }
        public bool SetupConfiguration_BoMList_Editrecords { get; set; }
        public bool SetupConfiguration_BoMList_Deleterecords { get; set; }
        public bool SetupConfiguration_BoMList_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_JobPlans_Allowaccess { get; set; }
        public bool SetupConfiguration_JobPlans_Createnewrecords { get; set; }
        public bool SetupConfiguration_JobPlans_Editrecords { get; set; }
        public bool SetupConfiguration_JobPlans_Deleterecords { get; set; }
        public bool SetupConfiguration_JobPlans_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_SafetyInstructions_Allowaccess { get; set; }
        public bool SetupConfiguration_SafetyInstructions_Createnewrecords { get; set; }
        public bool SetupConfiguration_SafetyInstructions_Editrecords { get; set; }
        public bool SetupConfiguration_SafetyInstructions_Deleterecords { get; set; }
        public bool SetupConfiguration_SafetyInstructions_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_JobPriority_Allowaccess { get; set; }
        public bool SetupConfiguration_JobPriority_Createnewrecords { get; set; }
        public bool SetupConfiguration_JobPriority_Editrecords { get; set; }
        public bool SetupConfiguration_JobPriority_Deleterecords { get; set; }
        public bool SetupConfiguration_JobStatus_Allowaccess { get; set; }
        public bool SetupConfiguration_JobStatus_Createnewrecords { get; set; }
        public bool SetupConfiguration_JobStatus_Editrecords { get; set; }
        public bool SetupConfiguration_JobStatus_Deleterecords { get; set; }
        public bool SetupConfiguration_JobType_Allowaccess { get; set; }
        public bool SetupConfiguration_JobType_Createnewrecords { get; set; }
        public bool SetupConfiguration_JobType_Editrecords { get; set; }
        public bool SetupConfiguration_JobType_Deleterecords { get; set; }
        public bool SetupConfiguration_JobTrade_Allowaccess { get; set; }
        public bool SetupConfiguration_JobTrade_Createnewrecords { get; set; }
        public bool SetupConfiguration_JobTrade_Editrecords { get; set; }
        public bool SetupConfiguration_JobTrade_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetStatus_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetStatus_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetStatus_Editrecords { get; set; }
        public bool SetupConfiguration_AssetStatus_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetCriticality_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetCriticality_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetCriticality_Editrecords { get; set; }
        public bool SetupConfiguration_AssetCriticality_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetWarrantyContract_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetWarrantyContract_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetWarrantyContract_Editrecords { get; set; }
        public bool SetupConfiguration_AssetWarrantyContract_Deleterecords { get; set; }
        public bool SetupConfiguration_EmployeeCategory_Allowaccess { get; set; }
        public bool SetupConfiguration_EmployeeCategory_Createnewrecords { get; set; }
        public bool SetupConfiguration_EmployeeCategory_Editrecords { get; set; }
        public bool SetupConfiguration_EmployeeCategory_Deleterecords { get; set; }
        public bool SetupConfiguration_EmployeeStatus_Allowaccess { get; set; }
        public bool SetupConfiguration_EmployeeStatus_Createnewrecords { get; set; }
        public bool SetupConfiguration_EmployeeStatus_Editrecords { get; set; }
        public bool SetupConfiguration_EmployeeStatus_Deleterecords { get; set; }
        public bool Management_UserGroups_Allowaccess { get; set; }
        public bool Management_UserGroups_Createnewrecords { get; set; }
        public bool Management_UserGroups_Editrecords { get; set; }
        public bool Management_UserGroups_Deleterecords { get; set; }
        public bool Management_UserGroups_Export { get; set; }
        public bool SetupConfiguration_CostCenter_Allowaccess { get; set; }
        public bool SetupConfiguration_CostCenter_Createnewrecords { get; set; }
        public bool SetupConfiguration_CostCenter_Editrecords { get; set; }
        public bool SetupConfiguration_CostCenter_Deleterecords { get; set; }
        public bool SetupConfiguration_CostCenter_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_AccountCode_Allowaccess { get; set; }
        public bool SetupConfiguration_AccountCode_Createnewrecords { get; set; }
        public bool SetupConfiguration_AccountCode_Editrecords { get; set; }
        public bool SetupConfiguration_AccountCode_Deleterecords { get; set; }
        public bool SetupConfiguration_AccountCode_ChangeMastersCodes { get; set; }
        public bool Locations_Buildings_Allowaccess { get; set; }
        public bool Locations_Buildings_Createnewrecords { get; set; }
        public bool Locations_Buildings_Editrecords { get; set; }
        public bool Locations_Buildings_Deleterecords { get; set; }
        public bool Locations_Buildings_ChangeMastersCodes { get; set; }
        public bool Stores_ItemType_Allowaccess { get; set; }
        public bool Stores_ItemType_Createnewrecords { get; set; }
        public bool Stores_ItemType_Editrecords { get; set; }
        public bool Stores_ItemType_Deleterecords { get; set; }
        public bool Stores_ItemType_ChangeMastersCodes { get; set; }
        public bool Stores_ItemSubType_Allowaccess { get; set; }
        public bool Stores_ItemSubType_Createnewrecords { get; set; }
        public bool Stores_ItemSubType_Editrecords { get; set; }
        public bool Stores_ItemSubType_Deleterecords { get; set; }
        public bool Stores_ItemSubType_ChangeMastersCodes { get; set; }
        public bool Stores_ItemLocation_Allowaccess { get; set; }
        public bool Stores_ItemLocation_Createnewrecords { get; set; }
        public bool Stores_ItemLocation_Editrecords { get; set; }
        public bool Stores_ItemLocation_Deleterecords { get; set; }
        public bool Stores_ItemList_Allowaccess { get; set; }
        public bool Stores_ItemList_Createnewrecords { get; set; }
        public bool Stores_ItemList_Editrecords { get; set; }
        public bool Stores_ItemList_Deleterecords { get; set; }
        public bool Stores_ItemList_ChangeMastersCodes { get; set; }
        public bool Stores_ItemList_Export { get; set; }

        public bool SetupConfiguration_IRApprovalLevels_Allowaccess { get; set; }
        public bool SetupConfiguration_IRApprovalLevels_Createnewrecords { get; set; }
        public bool SetupConfiguration_IRApprovalLevels_Editrecords { get; set; }
        public bool SetupConfiguration_IRApprovalLevels_Deleterecords { get; set; }
        public bool SetupConfiguration_IRApprovalLevels_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_IRApprovalLevelMappings_Allowaccess { get; set; }
        public bool SetupConfiguration_IRApprovalLevelMappings_Createnewrecords { get; set; }
        public bool SetupConfiguration_IRApprovalLevelMappings_Editrecords { get; set; }
        public bool SetupConfiguration_IRApprovalLevelMappings_Deleterecords { get; set; }
        public bool SetupConfiguration_IRAuthoriseStatus_Allowaccess { get; set; }
        public bool SetupConfiguration_IRAuthoriseStatus_Createnewrecords { get; set; }
        public bool SetupConfiguration_IRAuthoriseStatus_Editrecords { get; set; }
        public bool SetupConfiguration_IRAuthoriseStatus_Deleterecords { get; set; }
        public bool Management_LanguageTranslations_Allowaccess { get; set; }
        public bool Management_LanguageTranslations_Createnewrecords { get; set; }
        public bool Management_LanguageTranslations_Editrecords { get; set; }
        public bool Management_LanguageTranslations_Deleterecords { get; set; }
        public bool Management_LanguageTranslations_Export { get; set; }
        public bool JobOrder_JobOrder_Deleterecords { get; set; }
        public bool Management_AuditTrail_Allowaccess { get; set; }
        public bool Management_AuditTrail_Createnewrecords { get; set; }
        public bool Management_AuditTrail_Deleterecords { get; set; }
        public bool Management_AuditTrail_Export { get; set; }
        public bool Management_ActiveSessions_Allowaccess { get; set; }
        public bool Management_LoginHistory_Allowaccess { get; set; }
        public bool SetupConfiguration_Sector_Allowaccess { get; set; }
        public bool SetupConfiguration_Sector_Createnewrecords { get; set; }
        public bool SetupConfiguration_Sector_Editrecords { get; set; }
        public bool SetupConfiguration_Sector_Deleterecords { get; set; }
        public bool SetupConfiguration_Sector_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_AssetClass_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetClass_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetClass_Editrecords { get; set; }
        public bool SetupConfiguration_AssetClass_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetClass_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_AssetCondition_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetCondition_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetCondition_Editrecords { get; set; }
        public bool SetupConfiguration_AssetCondition_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetCondition_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_CityClass_Allowaccess { get; set; }
        public bool SetupConfiguration_CityClass_Createnewrecords { get; set; }
        public bool SetupConfiguration_CityClass_Editrecords { get; set; }
        public bool SetupConfiguration_CityClass_Deleterecords { get; set; }
        public bool SetupConfiguration_CityClass_ChangeMastersCodes { get; set; }
        public bool Stores_Transaction_ViewTransactions { get; set; }
        public bool Stores_Transaction_ReceiveParts { get; set; }
        public bool Stores_Transaction_IssueParts { get; set; }
        public bool Stores_Transaction_ReturnParts { get; set; }
        public bool Stores_Transaction_Adjustment { get; set; }
        public bool Stores_Transaction_Transfer { get; set; }
        public bool Stores_Transaction_Export { get; set; }
        public bool SetupConfiguration_PurchsingCurrency_Allowaccess { get; set; }
        public bool SetupConfiguration_PurchsingTermOfSale_Allowaccess { get; set; }
        public bool SetupConfiguration_PurchsingShippedVia_Allowaccess { get; set; }
        public bool SetupConfiguration_PurchsingPaymentTerms_Allowaccess { get; set; }
        public bool SetupConfiguration_PurchsingCurrency_Createnewrecords { get; set; }
        public bool SetupConfiguration_PurchsingTermOfSale_Createnewrecords { get; set; }
        public bool SetupConfiguration_PurchsingShippedVia_Createnewrecords { get; set; }
        public bool SetupConfiguration_PurchsingPaymentTerms_Createnewrecords { get; set; }
        public bool SetupConfiguration_PurchsingCurrency_Editrecords { get; set; }
        public bool SetupConfiguration_PurchsingTermOfSale_Editrecords { get; set; }
        public bool SetupConfiguration_PurchsingShippedVia_Editrecords { get; set; }
        public bool SetupConfiguration_PurchsingPaymentTerms_Editrecords { get; set; }
        public bool SetupConfiguration_PurchsingCurrency_Deleterecords { get; set; }
        public bool SetupConfiguration_PurchsingTermOfSale_Deleterecords { get; set; }
        public bool SetupConfiguration_PurchsingShippedVia_Deleterecords { get; set; }
        public bool SetupConfiguration_PurchsingPaymentTerms_Deleterecords { get; set; }
        public bool SetupConfiguration_PRAuthoriseStatus_Allowaccess { get; set; }
        public bool SetupConfiguration_PRAuthoriseStatus_Createnewrecords { get; set; }
        public bool SetupConfiguration_PRAuthoriseStatus_Export { get; set; }
        public bool SetupConfiguration_PRAuthoriseStatus_Editrecords { get; set; }
        public bool SetupConfiguration_PRAuthoriseStatus_Deleterecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_Allowaccess { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_Createnewrecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_Editrecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_Deleterecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_PRApprovalLevels_Export { get; set; }
        public bool SetupConfiguration_PRApprovalLevelMappings_Allowaccess { get; set; }
        public bool SetupConfiguration_PRApprovalLevelMappings_Createnewrecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevelMappings_Editrecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevelMappings_Deleterecords { get; set; }
        public bool SetupConfiguration_PRApprovalLevelMappings_Export { get; set; }
        public bool SetupConfiguration_DefaultSetups_Allowaccess { get; set; }
        public bool SetupConfiguration_DefaultSetups_Editrecords { get; set; }
        public bool Cleaning_CleaningInspection_Allowaccess { get; set; }
        public bool Cleaning_CleaningInspection_Createnewrecords { get; set; }
        public bool Cleaning_CleaningInspection_Editrecords { get; set; }
        public bool Cleaning_CleaningInspection_Deleterecords { get; set; }
        public bool Cleaning_CleaningInspection_Export { get; set; }
        public bool Cleaning_CleaningInspectionGroup_Allowaccess { get; set; }
        public bool Cleaning_CleaningInspectionGroup_Createnewrecords { get; set; }
        public bool Cleaning_CleaningInspectionGroup_Editrecords { get; set; }
        public bool Cleaning_CleaningInspectionGroup_Deleterecords { get; set; }
        public bool Cleaning_CleaningInspectionGroup_Export { get; set; }
        public bool Cleaning_CleaningChecklist_Allowaccess { get; set; }
        public bool Cleaning_CleaningChecklist_Createnewrecords { get; set; }
        public bool Cleaning_CleaningChecklist_Editrecords { get; set; }
        public bool Cleaning_CleaningChecklist_Deleterecords { get; set; }
        public bool Cleaning_CleaningChecklist_ChangeMastersCodes { get; set; }
        public bool Cleaning_CleaningChecklist_Export { get; set; }

        public bool Cleaning_CleaningScheduleList_Allowaccess { get; set; }
        public bool Cleaning_CleaningScheduleList_Createnewrecords { get; set; }
        public bool Cleaning_CleaningScheduleList_Editrecords { get; set; }
        public bool Cleaning_CleaningScheduleList_Deleterecords { get; set; }
        public bool Cleaning_CleaningScheduleList_Export { get; set; }
        public bool Preventive_CMProject_CanmassupdateProjectNumbers { get; set; }
        public bool MobileApp_WR_Allowaccess { get; set; }
        public bool MobileApp_WO_Allowaccess { get; set; }
        public bool MobileApp_Asset_Allowaccess { get; set; }
        public bool JobOrder_JobOrder_DonotlimitWOlisttoAssignedWOsonly { get; set; }
        public bool MobileApp_ForceScanforLocation_Allowaccess { get; set; }
        public bool MobileApp_ForceScanforAsset_Allowaccess { get; set; }
        public bool MobileApp_OnlyAssignedJO_Allowaccess { get; set; }
        public bool MobileApp_CreatedbyJO_Allowaccess { get; set; }
        //public bool Reports_AssetAssetList_View { get; set; }
        //public bool Reports_AssetAssetDetails_View { get; set; }
        //public bool Reports_AssetAssetMainteanceHistory_View { get; set; }
        //public bool Reports_AssetTop10AssetDowntime_View { get; set; }
        //public bool Reports_AssetTop10AssetBreakdowns_View { get; set; }
        //public bool Reports_AssetMTBF_View { get; set; }
        //public bool Reports_AssetBreakdownFrequency_View { get; set; }
        //public bool Reports_AssetBreakdownDowntime_View { get; set; }
        //public bool Reports_AssetManHoursByAsset_View { get; set; }
        //public bool Reports_AssetMTTR_View { get; set; }
        //public bool Reports_AssetMaint.History(noCost)_View { get; set; }
        //public bool Reports_JobOrdersList_View { get; set; }
        //public bool Reports_JobOrdersStatusStatistics_View { get; set; }
        //public bool Reports_JobOrdersAging_View { get; set; }
        //public bool Reports_JobOrdersStatusChart_View { get; set; }
        //public bool Reports_JobOrdersCostByLocationChart_View { get; set; }
        //public bool Reports_JobOrdersAllJoborderStatus_View { get; set; }
        //public bool Reports_JobOrdersAllJobOrderStatusChart_View { get; set; }
        //public bool Reports_JobOrdersFailurePer.AnalysisChart_View { get; set; }
        //public bool Reports_JobOrdersManHoursbyLocationChart_View { get; set; }
        //public bool Reports_JobOrdersCostbyJobTypeChart_View { get; set; }
        //public bool Reports_JobOrdersCostbyMaintenanceDiv.Dept.Subdept._View { get; set; }
        //public bool Reports_JobOrdersDistributionChart_View { get; set; }
        //public bool Reports_JobOrdersFailureDowntimeAnaylsisChart_View { get; set; }
        //public bool Reports_JobOrdersBreakdownFailureSummary_View { get; set; }
        //public bool Reports_JobOrdersCostbycostcenter_View { get; set; }
        //public bool Reports_JobOrdersByEmployees_View { get; set; }
        //public bool Reports_JobOrdersEmployeeJObyManHours_View { get; set; }
        //public bool Reports_JobOrdersRatingsReport_View { get; set; }
        //public bool Reports_JobOrdersPMJOAgingList_View { get; set; }
        //public bool Reports_JobOrdersPMJOStatus_View { get; set; }
        //public bool Reports_JobOrdersPMJOlist_View { get; set; }
        //public bool Reports_JobOrdersPMJOStatusChart_View { get; set; }
        //public bool Reports_JobOrdersCountbyemployees_View { get; set; }
        //public bool Reports_PreventivePMChecklist_View { get; set; }
        //public bool Reports_PreventivePMChecklistTasks_View { get; set; }
        //public bool Reports_PreventivePMCompliance_View { get; set; }
        //public bool Reports_PreventivePMMaterialRequirements_View { get; set; }
        //public bool Reports_PreventiveAnnualPMSchedule_View { get; set; }
        //public bool Reports_StoreItemList_View { get; set; }
        //public bool Reports_StoreStockTakeList_View { get; set; }
        //public bool Reports_StoreStockBalanceList_View { get; set; }
        //public bool Reports_StoreTransactionList_View { get; set; }
        //public bool Reports_StorePartReceiveQtyList_View { get; set; }
        //public bool Reports_StorePartIssueQtyList_View { get; set; }
        //public bool Reports_StorePartReceiveTotalCostlist_View { get; set; }
        //public bool Reports_StorePartIssueTotalCostList_View { get; set; }
        //public bool Reports_StorePurchaseProposalList(Normal)_View { get; set; }
        //public bool Reports_StorePurchaseProposalList(Critical)_View { get; set; }
        //public bool Reports_StoreDormantParts_View { get; set; }
        //public bool Reports_StoreItemIssuebyJO_View { get; set; }
        //public bool Reports_EmployeeRequesterList_View { get; set; }
        //public bool Reports_EmployeeRequesterDetails_View { get; set; }
        //public bool Reports_SetupLocationList_View { get; set; }
        //public bool Reports_SetupSupplierList_View { get; set; }
        //public bool Reports_PurchasingPurchaseRequestList_View { get; set; }
        //public bool Reports_PurchasingPurchaseRequestDetailsList_View { get; set; }
        //public bool Reports_PurchasingPurchaseOrderList_View { get; set; }
        //public bool Reports_PurchasingPurchaseOrderDetailsList_View { get; set; }
        //public bool Reports_PurchasingPurchaseOrderCost_View { get; set; }
        public bool Assets__ShowHideModule { get; set; }
        public bool Cleaning__ShowHideModule { get; set; }
        public bool Employee__ShowHideModule { get; set; }
        public bool JobOrder__ShowHideModule { get; set; }
        public bool JobRequest__ShowHideModule { get; set; }
        public bool Locations__ShowHideModule { get; set; }
        public bool Management__ShowHideModule { get; set; }
        public bool MobileApp__ShowHideModule { get; set; }
        public bool Preventive__ShowHideModule { get; set; }
        public bool Reports__ShowHideModule { get; set; }
        public bool SetupConfiguration__ShowHideModule { get; set; }
        public bool Stores__ShowHideModule { get; set; }
        public bool RequestCard__ShowHideModule { get; set; }
        public bool ItemRequisitions__ShowHideModule { get; set; }
        public bool SLA__ShowHideModule { get; set; }

        public bool SetupConfiguration_Sector_Export { get; set; }
        public bool SetupConfiguration_MaintenanceDivision_Export { get; set; }
        public bool Locations_Street_Export { get; set; }
        public bool Locations_Buildings_Export { get; set; }
        public bool SetupConfiguration_MaintenanceDepartment_Export { get; set; }
        public bool SetupConfiguration_MaintenanceSubDepartment_Export { get; set; }
        public bool SetupConfiguration_JobPriority_Export { get; set; }
        public bool SetupConfiguration_JobStatus_Export { get; set; }
        public bool SetupConfiguration_JobType_Export { get; set; }
        public bool SetupConfiguration_JobTrade_Export { get; set; }
        public bool SetupConfiguration_EmployeeCategory_Export { get; set; }
        public bool SetupConfiguration_EmployeeStatus_Export { get; set; }
        public bool SetupConfiguration_CityClass_Export { get; set; }
        public bool SetupConfiguration_City_Export { get; set; }
        public bool Stores_Stores_Export { get; set; }
        public bool Stores_ItemType_Export { get; set; }
        public bool Stores_ItemSubType_Export { get; set; }
        public bool Stores_ItemLocation_Export { get; set; }
        public bool SetupConfiguration_IRAuthoriseStatus_Export { get; set; }
        public bool SetupConfiguration_IRApprovalLevelMappings_Export { get; set; }
        public bool SetupConfiguration_IRApprovalLevels_Export { get; set; }
        public bool SetupConfiguration_PurchsingCurrency_Export { get; set; }
        public bool SetupConfiguration_PurchsingTermOfSale_Export { get; set; }
        public bool SetupConfiguration_PurchsingShippedVia_Export { get; set; }
        public bool SetupConfiguration_PurchsingPaymentTerms_Export { get; set; }
        public bool SetupConfiguration_Organisation_Export { get; set; }
        public bool SetupConfiguration_Area_Export { get; set; }
        public bool Management_ActiveSessions_Export { get; set; }
        public bool Management_ActiveSessions_Deleterecords { get; set; }
        public bool Management_LoginHistory_Export { get; set; }
        public bool SetupConfiguration_MaintenanceGroups_Export { get; set; }
        public bool SetupConfiguration_AccountCode_Export { get; set; }
        public bool SetupConfiguration_JobPlans_Export { get; set; }
        public bool SetupConfiguration_SafetyInstructions_Export { get; set; }
        public bool SetupConfiguration_CostCenter_Export { get; set; }
        public bool SetupConfiguration_BoMList_Export { get; set; }
        public bool SetupConfiguration_AssetClass_Export { get; set; }
        public bool SetupConfiguration_AssetCategories_Export { get; set; }
        public bool SetupConfiguration_AssetStatus_Export { get; set; }
        public bool SetupConfiguration_AssetCondition_Export { get; set; }
        public bool SetupConfiguration_AssetCriticality_Export { get; set; }
        public bool SetupConfiguration_AssetWarrantyContract_Export { get; set; }
        public bool SetupConfiguration_Suppliers_Export { get; set; }
        public bool Assets_Assets_Export { get; set; }

        public bool SetupConfiguration_ToolCategory_Allowaccess { get; set; }
        public bool SetupConfiguration_ToolCategory_Createnewrecords { get; set; }
        public bool SetupConfiguration_ToolCategory_Editrecords { get; set; }
        public bool SetupConfiguration_ToolCategory_Deleterecords { get; set; }
        public bool SetupConfiguration_ToolCategory_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_ToolCategory_Export { get; set; }
        public bool SetupConfiguration_ToolSubCategory_Allowaccess { get; set; }
        public bool SetupConfiguration_ToolSubCategory_Createnewrecords { get; set; }
        public bool SetupConfiguration_ToolSubCategory_Editrecords { get; set; }
        public bool SetupConfiguration_ToolSubCategory_Deleterecords { get; set; }
        public bool SetupConfiguration_ToolSubCategory_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_ToolSubCategory_Export { get; set; }
        public bool SetupConfiguration_Tools_Allowaccess { get; set; }
        public bool SetupConfiguration_Tools_Createnewrecords { get; set; }
        public bool SetupConfiguration_Tools_Editrecords { get; set; }
        public bool SetupConfiguration_Tools_Deleterecords { get; set; }
        public bool SetupConfiguration_Tools_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_Tools_Export { get; set; }
        public bool SetupConfiguration_LocationType_Allowaccess { get; set; }
        public bool SetupConfiguration_LocationType_Createnewrecords { get; set; }
        public bool SetupConfiguration_LocationType_Editrecords { get; set; }
        public bool SetupConfiguration_LocationType_Deleterecords { get; set; }
        public bool SetupConfiguration_LocationType_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_LocationType_Export { get; set; }
        public bool SetupConfiguration_FailureCodes_Export { get; set; }
        public bool SetupConfiguration_CleaningGroup_Allowaccess { get; set; }
        public bool SetupConfiguration_CleaningGroup_Createnewrecords { get; set; }
        public bool SetupConfiguration_CleaningGroup_Editrecords { get; set; }
        public bool SetupConfiguration_CleaningGroup_Deleterecords { get; set; }
        public bool SetupConfiguration_CleaningGroup_Export { get; set; }
        public bool SetupConfiguration_CleaningClassification_Allowaccess { get; set; }
        public bool SetupConfiguration_CleaningClassification_Createnewrecords { get; set; }
        public bool SetupConfiguration_CleaningClassification_Editrecords { get; set; }
        public bool SetupConfiguration_CleaningClassification_Deleterecords { get; set; }
        public bool SetupConfiguration_CleaningClassification_Export { get; set; }
        public bool SetupConfiguration_CleaningWeightage_Allowaccess { get; set; }
        public bool SetupConfiguration_CleaningWeightage_Editrecords { get; set; }

        public bool SetupConfiguration_EmailServerConfiguration_Allowaccess { get; set; }
        public bool SetupConfiguration_EmailServerConfiguration_Editrecords { get; set; }
        public bool SetupConfiguration_SMSConfiguration_Allowaccess { get; set; }
        public bool SetupConfiguration_SMSConfiguration_Editrecords { get; set; }
        public bool SetupConfiguration_NotificationRules_Editrecords { get; set; }
        public bool SetupConfiguration_NotificationRules_Deleterecords { get; set; }
        public bool SetupConfiguration_NotificationRules_Export { get; set; }
        public bool SetupConfiguration_NotificationRules_Createnewrecords { get; set; }
        public bool SetupConfiguration_NotificationRules_Allowaccess { get; set; }
        public bool SetupConfiguration_NotificationTemplates_Allowaccess { get; set; }
        public bool SetupConfiguration_NotificationTemplates_Editrecords { get; set; }
        public bool SetupConfiguration_NotificationTemplates_Deleterecords { get; set; }
        public bool SetupConfiguration_NotificationTemplates_Export { get; set; }
        public bool SetupConfiguration_NotificationTemplates_Createnewrecords { get; set; }

        public bool Purchasing_PurchaseOrder_Allowaccess { get; set; }
        public bool Purchasing_PurchaseOrder_Createnewrecords { get; set; }
        public bool Purchasing_PurchaseOrder_Editrecords { get; set; }
        public bool Purchasing_PurchaseOrder_Deleterecords { get; set; }
        public bool Purchasing_PurchaseOrder_Export { get; set; }
        public bool Purchasing_PurchaseRequest_Allowaccess { get; set; }
        public bool Purchasing_PurchaseRequest_Createnewrecords { get; set; }
        public bool Purchasing_PurchaseRequest_Editrecords { get; set; }
        public bool Purchasing_PurchaseRequest_Deleterecords { get; set; }
        public bool Purchasing_PurchaseRequest_Export { get; set; }
        public bool Purchasing_GoodsReceipt_Allowaccess { get; set; }
        public bool Purchasing_GoodsReceipt_Createnewrecords { get; set; }
        public bool Purchasing_GoodsReceipt_Editrecords { get; set; }
        public bool Purchasing_GoodsReceipt_Deleterecords { get; set; }
        public bool Purchasing_GoodsReceipt_Export { get; set; }
        public bool Purchasing__ShowHideModule { get; set; }
        public bool SetupConfiguration_AssetSubCategories_Allowaccess { get; set; }
        public bool SetupConfiguration_AssetSubCategories_Createnewrecords { get; set; }
        public bool SetupConfiguration_AssetSubCategories_Editrecords { get; set; }
        public bool SetupConfiguration_AssetSubCategories_Deleterecords { get; set; }
        public bool SetupConfiguration_AssetSubCategories_ChangeMastersCodes { get; set; }
        public bool SetupConfiguration_AssetSubCategories_Export { get; set; }

        public bool RequestCard_Consumables_Allowaccess { get; set; }
        public bool RequestCard_Consumables_Createnewrecords { get; set; }
        public bool RequestCard_Consumables_Editrecords { get; set; }
        public bool RequestCard_Consumables_Deleterecords { get; set; }
        public bool RequestCard_Consumables_Export { get; set; }
        public bool RequestCard_Personal_Allowaccess { get; set; }
        public bool RequestCard_Personal_Createnewrecords { get; set; }
        public bool RequestCard_Personal_Editrecords { get; set; }
        public bool RequestCard_Personal_Deleterecords { get; set; }
        public bool RequestCard_Personal_Export { get; set; }
        public bool SetupConfiguration_GeneralConfiguration_Allowaccess { get; set; }
        public bool SetupConfiguration_GeneralConfiguration_Editrecords { get; set; }

        public bool SetupConfiguration_HolidaySetting_Allowaccess { get; set; }
        public bool SetupConfiguration_HolidaySetting_Createnewrecords { get; set; }
        public bool SetupConfiguration_HolidaySetting_Editrecords { get; set; }
        public bool SetupConfiguration_HolidaySetting_Deleterecords { get; set; }
        public bool SetupConfiguration_HolidaySetting_Export { get; set; }

        public bool SLA_AssetCategoryNotification_Allowaccess { get; set; }
        public bool SLA_AssetCategoryNotification_Createnewrecords { get; set; }
        public bool SLA_AssetCategoryNotification_Editrecords { get; set; }
        public bool SLA_AssetCategoryNotification_Deleterecords { get; set; }
        public bool SLA_AssetCategoryNotification_Export { get; set; }
        
        public bool SLA_CriticalityNotification_Allowaccess { get; set; }
        public bool SLA_CriticalityNotification_Createnewrecords { get; set; }
        public bool SLA_CriticalityNotification_Editrecords { get; set; }
        public bool SLA_CriticalityNotification_Deleterecords { get; set; }
        public bool SLA_CriticalityNotification_Export { get; set; }

        public bool SLA_JOStatusNotification_Allowaccess { get; set; }
        public bool SLA_JOStatusNotification_Createnewrecords { get; set; }
        public bool SLA_JOStatusNotification_Editrecords { get; set; }
        public bool SLA_JOStatusNotification_Deleterecords { get; set; }
        public bool SLA_JOStatusNotification_Export { get; set; }

        public bool SLA_JRStatusNotification_Allowaccess { get; set; }
        public bool SLA_JRStatusNotification_Createnewrecords { get; set; }
        public bool SLA_JRStatusNotification_Editrecords { get; set; }
        public bool SLA_JRStatusNotification_Deleterecords { get; set; }
        public bool SLA_JRStatusNotification_Export { get; set; }

        public bool SLA_LocationTypeNotification_Allowaccess { get; set; }
        public bool SLA_LocationTypeNotification_Createnewrecords { get; set; }
        public bool SLA_LocationTypeNotification_Editrecords { get; set; }
        public bool SLA_LocationTypeNotification_Deleterecords { get; set; }
        public bool SLA_LocationTypeNotification_Export { get; set; }

        public bool SLA_WorkPriorityNotification_Allowaccess { get; set; }
        public bool SLA_WorkPriorityNotification_Createnewrecords { get; set; }
        public bool SLA_WorkPriorityNotification_Editrecords { get; set; }
        public bool SLA_WorkPriorityNotification_Deleterecords { get; set; }
        public bool SLA_WorkPriorityNotification_Export { get; set; }

        public bool SLA_WorkTypeNotification_Allowaccess { get; set; }
        public bool SLA_WorkTypeNotification_Createnewrecords { get; set; }
        public bool SLA_WorkTypeNotification_Editrecords { get; set; }
        public bool SLA_WorkTypeNotification_Deleterecords { get; set; }
        public bool SLA_WorkTypeNotification_Export { get; set; }
    }
}
