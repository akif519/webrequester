﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cmNationality
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmNationality")]
    public sealed class CmNationality : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NationaID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NationaID { get; set; }

        /// <summary>
        /// Gets or sets the National value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string National { get; set; }

        /// <summary>
        /// Gets or sets the AltNational value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltNational { get; set; }

        #endregion
    }
}
