//-----------------------------------------------------------------------
// <copyright file="L3.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - L3
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("L3")]
    public sealed class L3 : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L1ID value.
        /// </summary>
        public int? L1ID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L3Desc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the L3AltDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        [NotMapped]
        public int HasChild1 { get; set; }

        /// <summary>
        /// Gets or sets the is enable city.
        /// </summary>
        /// <value>
        /// The is enable city.
        /// </value>
        [NotMapped]
        public bool? IsEnableCity { get; set; }

        [NotMapped]
        public string L1No { get; set; }

        [NotMapped]
        public bool IsL3Level { get; set; }

        #endregion
    }
}
