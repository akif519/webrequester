﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public partial class PurchaeOrderItemTable
    {
        public string Rowno { get; set; }
        public string StockNo { get; set; }
        public string PartDescription { get; set; }
        public string uom { get; set; }
        public decimal Quantity { get; set; }
        public string WorkorderNo { get; set; }
        public int ID { get; set; }
        public decimal Tax { get; set; }
        public decimal Discount { get; set; }
        public decimal LineCost { get; set; }
        public decimal BaseCost { get; set; }

    }

    public class PurchaeOrderTable
    {
        public string RowNo { get; set; }
        public string PurchaseOrderNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Name { get; set; }
        public string CostCenterName { get; set; }
        public string AccountCode { get; set; }
        public string AccountCodeDesc { get; set; }
        public string ShippedViaDesc { get; set; }
        public string TermofSaleDesc { get; set; }
        public string PaymentTermsDesc { get; set; }

        public string OrderStatus { get; set; }
        public string DeliveryStatus { get; set; }
        public string SupplierName { get; set; }
        public string L2Name { get; set; }
        public string Currency_TotalCost { get; set; }
        public decimal TotalCost_BaseCurrency { get; set; }
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AltStockDescription { get; set; }
        public string UOM { get; set; }
        public string WorkorderNo { get; set; }
        public decimal Quantity { get; set; }
        public decimal LineCost { get; set; }
        public decimal BaseCost { get; set; }

    }
}
