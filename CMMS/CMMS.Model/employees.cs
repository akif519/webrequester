﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    
    /// <summary>
    /// This class is used to Define Model for Table - employees
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("EMPLOYEES")]
    public sealed class employees : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the User Image Path.
        /// </summary>
        public string ImagePath  { get; set; }
        
        [NotMapped]
        public string OldImagePath { get; set; }
        
        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the CategoryID value.
        /// </summary>
        [AllowNullForSave]
        public int? CategoryID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTradeID value.
        /// </summary>
        [AllowNullForSave]
        public int? WorkTradeID { get; set; }

        /// <summary>
        /// Gets or sets the UserGroupId value.
        /// </summary>
        [AllowNullForSave]
        public int? UserGroupId { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeNO value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Name value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the AltName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or sets the Positions value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Positions { get; set; }

        /// <summary>
        /// Gets or sets the Extension value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets the WorkPhone value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the HandPhone value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string HandPhone { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Fax value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the Housephone value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Housephone { get; set; }

        /// <summary>
        /// Gets or sets the Address value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "Address length cannot be greater than 4000 characters.")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the OfficeLocation value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string OfficeLocation { get; set; }

        /// <summary>
        /// Gets or sets the HourlySalary value.
        /// </summary>
        [AllowNullForSave]
        public decimal? HourlySalary { get; set; }

        /// <summary>
        /// Gets or sets the OverTime1 value.
        /// </summary>
        [AllowNullForSave]
        public decimal? OverTime1 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime2 value.
        /// </summary>
        [AllowNullForSave]
        public decimal? OverTime2 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime3 value.
        /// </summary>
        [AllowNullForSave]
        public decimal? OverTime3 { get; set; }

        /// <summary>
        /// Gets or sets the Accessibility value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Accessibility { get; set; }

        /// <summary>
        /// Gets or sets the UserID value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the Password value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the Central value.
        /// </summary>
        public int? Central { get; set; }

        /// <summary>
        /// Gets or sets the Auth value.
        /// </summary>
        public int? Auth { get; set; }

        /// <summary>
        /// Gets or sets the ExecLevel value.
        /// </summary>
        public int? ExecLevel { get; set; }

        /// <summary>
        /// Gets or sets the WebAccess value.
        /// </summary>
        public int? WebAccess { get; set; }

        /// <summary>
        /// Gets or sets the OpenWorkorders value.
        /// </summary>
        public int? OpenWorkorders { get; set; }

        /// <summary>
        /// Gets or sets the ReopenWorkorders value.
        /// </summary>
        public int? ReopenWorkorders { get; set; }

        /// <summary>
        /// Gets or sets the Connected value.
        /// </summary>
        public int? Connected { get; set; }

        /// <summary>
        /// Gets or sets the WRConnected value.
        /// </summary>
        public int? WRConnected { get; set; }

        /// <summary>
        /// Gets or sets the AllowAdjustment value.
        /// </summary>
        public int? AllowAdjustment { get; set; }

        /// <summary>
        /// Gets or sets the Pr_auto_amount value.
        /// </summary>
        public decimal? Pr_auto_amount { get; set; }

        /// <summary>
        /// Gets or sets the EmpDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string EmpDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the AllowCancelPO value.
        /// </summary>
        public int? AllowCancelPO { get; set; }

        /// <summary>
        /// Gets or sets the ConnectedMW value.
        /// </summary>
        public int? ConnectedMW { get; set; }

        /// <summary>
        /// Gets or sets the WOReturn value.
        /// </summary>
        public int? WOReturn { get; set; }

        /// <summary>
        /// Gets or sets the WOIssue value.
        /// </summary>
        public int? WOIssue { get; set; }

        /// <summary>
        /// Gets or sets the Hide_cost value.
        /// </summary>
        public int? Hide_cost { get; set; }

        /// <summary>
        /// Gets or sets the LanguageCode value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string LanguageCode { get; set; }

        /// <summary>
        /// Gets or sets the AllowInterSiteTransfer value.
        /// </summary>
        public int? AllowInterSiteTransfer { get; set; }

        /// <summary>
        /// Gets or sets the Postal_address value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Postal_address { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeStatusId value.
        /// </summary>
        public int? EmployeeStatusId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the IsStoredSupervisor value.
        /// </summary>
        public bool? IsStoredSupervisor { get; set; }

        /// <summary>
        /// Gets or sets the IsLogIn value.
        /// </summary>
        public bool? IsLogIn { get; set; }

        /// <summary>
        /// Gets or sets the POAuthorisationLimit value.
        /// </summary>
        public decimal? POAuthorisationLimit { get; set; }

        /// <summary>
        /// Gets or sets the EmpHasCert value.
        /// </summary>
        public bool? EmpHasCert { get; set; }

        /// <summary>
        /// Gets or sets the ClassificationLevel value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ClassificationLevel { get; set; }

        /// <summary>
        /// Gets or sets the ID_No value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ID_No { get; set; }

        /// <summary>
        /// Gets or sets the ID_ExpiryDate value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? ID_ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the PassportNo value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PassportNo { get; set; }

        /// <summary>
        /// Gets or sets the PassportExp value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? PassportExp { get; set; }

        /// <summary>
        /// Gets or sets the Nationality value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeProfessionID value.
        /// </summary>
        [AllowNullForSave]
        public int? EmployeeProfessionID { get; set; }

        /// <summary>
        /// Gets or sets the NationalityID value.
        /// </summary>
        public int? NationalityID { get; set; }

        /// <summary>
        /// Gets or sets the ProjectTypeID value.
        /// </summary>
        public int? ProjectTypeID { get; set; }

        /// <summary>
        /// Gets or sets the SponsorID value.
        /// </summary>
        public int? SponsorID { get; set; }

        /// <summary>
        /// Gets or sets the TBtransaction value.
        /// </summary>
        public int? TBtransaction { get; set; }

        /// <summary>
        /// Gets or sets the TBSetup value.
        /// </summary>
        public int? TBSetup { get; set; }

        /// <summary>
        /// Gets or sets the TBReport value.
        /// </summary>
        public int? TBReport { get; set; }

        /// <summary>
        /// Gets or sets the TBDashboard value.
        /// </summary>
        public int? TBDashboard { get; set; }

        /// <summary>
        /// Gets or sets the LastConfigChangeDateTime value.
        /// </summary>
        public DateTime? LastConfigChangeDateTime { get; set; }

        public int? ClientId { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }

        [NotMapped]
        public string AltCategoryName { get; set; }

        [NotMapped]
        public string EmployeeStatus { get; set; }

        [NotMapped]
        public string EmployeeAltStatus { get; set; }

        [NotMapped]
        public int isfrmTransaction { get; set; }

        [NotMapped]
        public bool boolCentral
        {
            get {
                 if (this.Central.HasValue)
                {
                    return Convert.ToBoolean(this.Central);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.Central = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolTBtransaction
        {
            get
            {
                if (this.TBtransaction.HasValue)
                {
                    return Convert.ToBoolean(this.TBtransaction);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.TBtransaction = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolTBSetup
        {
            get
            {
                if (this.TBSetup.HasValue)
                {
                    return Convert.ToBoolean(this.TBSetup);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.TBSetup = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolTBReport
        {
            get
            {
                if (this.TBReport.HasValue)
                {
                    return Convert.ToBoolean(this.TBReport);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.TBReport = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolTBDashboard
        {
            get
            {
                if (this.TBDashboard.HasValue)
                {
                    return Convert.ToBoolean(this.TBDashboard);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.TBDashboard = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolIsStoreSuperWiser
        {
            get
            {
                if (this.IsStoredSupervisor.HasValue)
                {
                    return Convert.ToBoolean(this.IsStoredSupervisor);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.IsStoredSupervisor = Convert.ToBoolean(value);
            }
        }
        [NotMapped]
        public string IDExpiryDate
        {
            get
            {
                if (this.ID_ExpiryDate.HasValue)
                {
                    return ID_ExpiryDate.Value.ToString("d");
                }
                else
                {
                    return "";
                }
            }

            set
            {
                this.ID_ExpiryDate = Convert.ToDateTime(value);
            }
        }

        [NotMapped]
        public string PassPortExpiryDate
        {
            get
            {
                if (this.PassportExp.HasValue)
                {
                    return PassportExp.Value.ToString("d");
                }
                else
                {
                    return "";
                }
            }

            set
            {
                this.PassportExp = Convert.ToDateTime(value);
            }
        }

        [NotMapped]
        public List<EmployeeDocuments> lstEmployeeDocumentModel { get; set; }

        [NotMapped]
        public string JsonlstEmployeeDocumentModel { get; set; }// use for maintain temporary kendoGrid datasource in hidden field as json
        #endregion
    }
}
