﻿//-----------------------------------------------------------------------
// <copyright file="Tbl_Feature.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Tbl_Feature
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>06-Mar-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Tbl_Feature")]
    public sealed class Tbl_Feature : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Area is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool Area { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the CMProject is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool CMProject { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the CMProjectNoMandatory is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool CMProjectNoMandatory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the WebRequesterLicence is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool WebRequesterLicence { get; set; }

        #endregion
    }
}
