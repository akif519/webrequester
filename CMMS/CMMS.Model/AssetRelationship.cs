﻿//-----------------------------------------------------------------------
// <copyright file="Assetrelationship.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - assetrelationship
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("assetrelationship")]
    public sealed class AssetRelationship : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int AssetID { get; set; }

        /// <summary>
        /// Gets or sets the ChildId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ChildId { get; set; }

        #endregion
    }
}
