﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class EmailTemplateWorkOrder : BaseModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public System.Nullable<System.DateTime> DateReceived { get; set; }
        public System.Nullable<System.DateTime> DateRequired { get; set; }

        public System.Nullable<int> L2ID { get; set; }
        public string L2Code { get; set; }
        public string L2name { get; set; }
        public string L2Altname { get; set; }

        public System.Nullable<int> L3ID { get; set; }
        public string L3No { get; set; }
        public string L3Desc { get; set; }
        public string L3AltDesc { get; set; }

        public System.Nullable<int> L4ID { get; set; }
        public string L4No { get; set; }
        public string L4Description { get; set; }
        public string L4AltDescription { get; set; }

        public System.Nullable<int> L5ID { get; set; }
        public string L5No { get; set; }
        public string L5Description { get; set; }
        public string L5AltDescription { get; set; }

        public System.Nullable<int> LocationID { get; set; }
        public string LocationNo { get; set; }
        public string LocationDescription { get; set; }
        public string LocationAltDescription { get; set; }

        public System.Nullable<int> AssetID { get; set; }
        public string AssetNumber { get; set; }
        public string AssetDescription { get; set; }
        public string AssetAltDescription { get; set; }

        public System.Nullable<int> MaintDivisionID { get; set; }
        public string MaintDivisionCode { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDivisionAltName { get; set; }

        public System.Nullable<int> MaintDeptID { get; set; }
        public string MaintDeptCode { get; set; }
        public string MaintDeptdesc { get; set; }
        public string MaintDeptAltdesc { get; set; }

        public System.Nullable<int> MaintSubDeptID { get; set; }
        public string MaintSubDeptCode { get; set; }
        public string MaintSubDeptDesc { get; set; }
        public string MaintSubDeptAltDesc { get; set; }

        public System.Nullable<int> WorkTradeID { get; set; }
        public string WorkTrade { get; set; }
        public string AltWorkTrade { get; set; }

        public System.Nullable<int> WorkStatusID { get; set; }
        public string WorkStatus { get; set; }
        public string AltWorkStatus { get; set; }

        public System.Nullable<int> WorkTypeID { get; set; }
        public string WorkTypeDescription { get; set; }
        public string AltWorkTypeDescription { get; set; }

        public System.Nullable<int> WorkPriorityID { get; set; }
        public string WorkPriority { get; set; }
        public string AltWorkPriority { get; set; }

        public System.Nullable<int> RequestorID { get; set; }
        public string RequestorNo { get; set; }
        public string RequestorName { get; set; }
        public string RequestorAltName { get; set; }
        public string RequestorEmail { get; set; }

        public System.Nullable<int> CreatedID { get; set; }
        public string CreatedNo { get; set; }
        public string CreatedName { get; set; }
        public string CreatedAltName { get; set; }
        public string CreatedEmail { get; set; }
    }
}
