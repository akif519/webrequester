//-----------------------------------------------------------------------
// <copyright file="WorkingDay.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - WorkingDays
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>16-Jan-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("WorkingDays")]
	public sealed class WorkingDay : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the WorkingDayID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int WorkingDayID { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L1ID value.
        /// </summary>
        public int? L1ID { get; set; }

		/// <summary>
		/// Gets or sets the StartDayOfWeek value.
		/// </summary>
        [AllowNullForSave]
		public int? StartDayOfWeek { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the l2 altname.
        /// </summary>
        /// <value>
        /// The l2 altname.
        /// </value>
        [NotMapped]
        public string L2Altname { get; set; } 

        /// <summary>
        /// Gets or sets the l1 no.
        /// </summary>
        /// <value>
        /// The l1 no.
        /// </value>
        [NotMapped]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the LST working day hour.
        /// </summary>
        /// <value>
        /// The LST working day hour.
        /// </value>
        public List<WorkingDayHour> lstWorkingDayHour { get; set; }

        /// <summary>
        /// Gets or sets the working day hour.
        /// </summary>
        /// <value>
        /// The working day hour.
        /// </value>
        [NotMapped]
        public WorkingDayHour objWorkingDayHour { get; set; }

        /// <summary>
        /// Gets or sets the user group i ds.
        /// </summary>
        /// <value>
        /// The user group i ds.
        /// </value>
        [NotMapped]
        public string L2IDs { get; set; }
		
        #endregion
	}
}
