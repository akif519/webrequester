﻿//-----------------------------------------------------------------------
// <copyright file="Pmmeters_assignto.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - pmmeters_assignto
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("pmmeters_assignto")]
    public sealed class PmMeters_AssignTo : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Meterid value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Meterid { get; set; }

        /// <summary>
        /// Gets or sets the Employeeid value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Employeeid { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [NotMapped]
        public string MeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltMeterDescription value.
        /// </summary>
        [NotMapped]
        public string AltMeterDescription { get; set; }

        [NotMapped]
        public string EmployeeNO { get; set; }

        [NotMapped]
        public string Name { get; set; }

        #endregion
    }
}
