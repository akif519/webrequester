﻿//-----------------------------------------------------------------------
// <copyright file="Account.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Accounts
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>14-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Accounts")]
    public sealed class Account : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        /// 
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the UserName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the Password value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Password { get; set; }

        [StringLength(50, ErrorMessage = "*")]
        public string ClientCode { get; set; }

        /// <summary>
        /// Gets or sets the ConCurrentUsers value.
        /// </summary>
        public int? ConCurrentUsers { get; set; }

        /// <summary>
        /// Gets or sets the MobAppLicCon value.
        /// </summary>
        public int? MobAppLicCon { get; set; }

        /// <summary>
        /// Gets or sets the MobAppLicNamed value.
        /// </summary>
        public int? MobAppLicNamed { get; set; }

        /// <summary>
        /// Gets or sets the DateCreated value.
        /// </summary>
        public DateTime? DateCreated { get; set; }

        /// <summary>
        /// Gets or sets the SubCycle value.
        /// </summary>
        public int? SubCycle { get; set; }

        /// <summary>
        /// Gets or sets the ExpDate value.
        /// </summary>
        public DateTime? ExpDate { get; set; }

        /// <summary>
        /// Gets or sets the AccStatus value.
        /// </summary>
        public bool? AccStatus { get; set; }

        /// <summary>
        /// Gets or sets the DbType value.
        /// </summary>
        public int? DbType { get; set; }

        /// <summary>
        /// Gets or sets the DbName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string DbName { get; set; }

        /// <summary>
        /// Gets or sets the DbConString value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string DbConString { get; set; }

        /// <summary>
        /// Gets or sets the HdrLogo value.
        /// </summary>
        public string HdrLogo { get; set; }

        /// <summary>
        /// Gets or sets the RprtLogo value.
        /// </summary>
        public string RprtLogo { get; set; }

        /// <summary>
        /// Gets or sets the HdrNameEn value.
        /// </summary>
        public string HdrNameEn { get; set; }

        /// <summary>
        /// Gets or sets the HdrNameAr value.
        /// </summary>
        public string HdrNameAr { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the SessionTimeOut value.
        /// </summary>
        public int? SessionTimeOut { get; set; }

        /// <summary>
        /// Gets or sets the LicenseNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LicenseNo { get; set; }
       
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public bool? IsNamedUser { get; set; }

        public int? NoOfLicenses { get; set; }


        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public bool? IsItemNoAutoIncrement { get; set; }

        [StringLength(500, ErrorMessage = "*")]
        public string SharedFolderPath { get; set; }

        [StringLength(500, ErrorMessage = "*")]
        public string AllowedFileTypes { get; set; }

        public int AllowedMaxFilesize { get; set; }

        public int? DashboardRefreshTime { get; set; }
        
        public int? SessionTimeOutMobApp { get; set; }

        public TabAccessModel TabAccess { get; set; }

        public ReportAccessModel ReportsAccess { get; set; }

        public FeatureAccessModel FeatureAccess { get; set; }

        public ModulesAccessModel ModulesAccess { get; set; }

        public LevelNamesModel LevelNames { get; set; }

        public CEUML_configModel CEUML_config { get; set; }

        public FirstLevelTblModel FirstLevelTbl { get; set; }
                
        #endregion
    }

    public class TabAccessModel
    {

        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the Transactions value.
        /// </summary>
        /// 
        public bool TransactionValue
        {
            get { return Transactions == 1; }
            set { Transactions = value ? 1 : 0; }
        }
        public int Transactions { get; set; }

        /// <summary>
        /// Gets or sets the Setup value.
        /// </summary>
        /// 
        public bool SetupValue
        {
            get { return Setup == 1; }
            set { Setup = value ? 1 : 0; }
        }
        public int Setup { get; set; }

        /// <summary>
        /// Gets or sets the Reports value.
        /// </summary>
        /// 
        public bool ReportsValue
        {
            get { return Reports == 1; }
            set { Reports = value ? 1 : 0; }
        }
        public int Reports { get; set; }

        /// <summary>
        /// Gets or sets the Dashboard value.
        /// </summary>
        /// 
        public bool DashboardValue
        {
            get { return Dashboard == 1; }
            set { Dashboard = value ? 1 : 0; }
        }
        public int Dashboard { get; set; }
    
    }

    public class ReportAccessModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the Assets value.
        /// </summary>
        /// 
        public bool AssetsValue
        {
            get { return Assets == 1; }
            set { Assets = value ? 1 : 0; }
        }
        public int? Assets { get; set; }

        /// <summary>
        /// Gets or sets the JobOrder value.
        /// </summary>
        /// 
        public bool JobOrderValue
        {
            get { return JobOrder == 1; }
            set { JobOrder = value ? 1 : 0; }
        }
        public int? JobOrder { get; set; }

        /// <summary>
        /// Gets or sets the Preventive value.
        /// </summary>
        /// 

        public bool PreventiveValue
        {
            get { return Preventive == 1; }
            set { Preventive = value ? 1 : 0; }
        }
        public int? Preventive { get; set; }

        /// <summary>
        /// Gets or sets the Store value.
        /// </summary>
        /// 
        public bool StoreValue
        {
            get { return Store == 1; }
            set { Store = value ? 1 : 0; }
        }
        public int? Store { get; set; }

        /// <summary>
        /// Gets or sets the Employee value.
        /// </summary>
        /// 

        public bool EmployeeValue
        {
            get { return Employee == 1; }
            set { Employee = value ? 1 : 0; }
        }
        public int? Employee { get; set; }

        /// <summary>
        /// Gets or sets the Setups value.
        /// </summary>
        /// 
        public bool SetupsValue
        {
            get { return Setups == 1; }
            set { Setups = value ? 1 : 0; }
        }
        public int? Setups { get; set; }

        /// <summary>
        /// Gets or sets the Purchasing value.
        /// </summary>
        /// 

        public bool PurchasingValue
        {
            get { return Purchasing == 1; }
            set { Purchasing = value ? 1 : 0; }
        }
        public int? Purchasing { get; set; }

        /// <summary>
        /// Gets or sets the Cleaning value.
        /// </summary>
        /// 
        public bool CleaningValue
        {
            get { return Cleaning == 1; }
            set { Cleaning = value ? 1 : 0; }
        }
        public int? Cleaning { get; set; }

        #endregion
    }

    public class FeatureAccessModel
    {

        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Area is enabled.
        /// </summary>
        public bool Area { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the CMProject is enabled.
        /// </summary>
        public bool CMProject { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the CMProjectNoMandatory is enabled.
        /// </summary>
        public bool CMProjectNoMandatory { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the WebRequesterLicence is enabled.
        /// </summary>
        public bool WebRequesterLicence { get; set; }

        #endregion

    }

    public class ModulesAccessModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the JobRequest value.
        /// </summary>
        public bool JobRequest { get; set; }

        /// <summary>
        /// Gets or sets the JobOrder value.
        /// </summary>
        public bool JobOrder { get; set; }

        /// <summary>
        /// Gets or sets the Assets value.
        /// </summary>
        public bool Assets { get; set; }

        /// <summary>
        /// Gets or sets the Locations value.
        /// </summary>
        public bool Locations { get; set; }

        /// <summary>
        /// Gets or sets the PreventiveMaintenance value.
        /// </summary>
        public bool PreventiveMaintenance { get; set; }

        /// <summary>
        /// Gets or sets the Store value.
        /// </summary>
        public bool Store { get; set; }

        /// <summary>
        /// Gets or sets the Purchasing value.
        /// </summary>
        public bool Purchasing { get; set; }

        /// <summary>
        /// Gets or sets the Employee value.
        /// </summary>
        public bool Employee { get; set; }

        /// <summary>
        /// Gets or sets the Cleaning value.
        /// </summary>
        public bool Cleaning { get; set; }

        /// <summary>
        /// Gets or sets the CEUML value.
        /// </summary>
        public bool CEUML { get; set; }

        /// <summary>
        /// Gets or sets the AuditTrail value.
        /// </summary>
        public bool AuditTrail { get; set; }

        /// <summary>
        /// Gets or sets the Sector value.
        /// </summary>
        public bool Sector { get; set; }

        /// <summary>
        /// Gets or sets the Zone value.
        /// </summary>
        public bool Zone { get; set; }

        /// <summary>
        /// Gets or sets the Building value.
        /// </summary>
        public bool Building { get; set; }

        /// <summary>
        /// Gets or sets the Division value.
        /// </summary>
        public bool Division { get; set; }

        /// <summary>
        /// Gets or sets the Department value.
        /// </summary>
        public bool Department { get; set; }

        /// <summary>
        /// Gets or sets the SubDept value.
        /// </summary>
        public bool SubDept { get; set; }

        /// <summary>
        /// Gets or sets the Groups value.
        /// </summary>
        public bool Groups { get; set; }

        /// <summary>
        /// Gets or sets the FailureCodes value.
        /// </summary>
        public bool FailureCodes { get; set; }

        /// <summary>
        /// Gets or sets the SLA value.
        /// </summary>
        public bool SLA { get; set; }

        /// <summary>
        /// Gets or sets the Suppliers/Contractors value.
        /// </summary>
        public bool Suppliers_Contractors { get; set; }

        /// <summary>
        /// Gets or sets the BoMList value.
        /// </summary>
        public bool BoMList { get; set; }

        /// <summary>
        /// Gets or sets the CostCenter value.
        /// </summary>
        public bool CostCenter { get; set; }

        /// <summary>
        /// Gets or sets the AccountCode value.
        /// </summary>
        public bool AccountCode { get; set; }

        /// <summary>
        /// Gets or sets the JobPlans value.
        /// </summary>
        public bool JobPlans { get; set; }

        /// <summary>
        /// Gets or sets the SafetyInstructions value.
        /// </summary>
        public bool SafetyInstructions { get; set; }

        /// <summary>
        /// Gets or sets the SiteSetups value.
        /// </summary>
        public bool SiteSetups { get; set; }

        /// <summary>
        /// Gets or sets the StoreSetups value.
        /// </summary>
        public bool StoreSetups { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequisition value.
        /// </summary>
        public bool PurchaseRequisition { get; set; }

        /// <summary>
        /// Gets or sets the JobManagement value.
        /// </summary>
        public bool JobManagement { get; set; }

        /// <summary>
        /// Gets or sets the AssetSetup value.
        /// </summary>
        public bool AssetSetup { get; set; }

        /// <summary>
        /// Gets or sets the Employee/User value.
        /// </summary>
        public bool Employee_User { get; set; }

        /// <summary>
        /// Gets or sets the CleaningSetup value.
        /// </summary>
        public bool CleaningSetup { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseSetup value.
        /// </summary>
        public bool PurchaseSetup { get; set; }

        /// <summary>
        /// Gets or sets the NotificationSetup value.
        /// </summary>
        public bool NotificationSetup { get; set; }

        /// <summary>
        /// Gets or sets the Item Requisition value.
        /// </summary>
        public bool ItemRequisition { get; set; }


        public bool RequestCard { get; set; }

        public bool Help { get; set; }

        #endregion
    }

    public class LevelNamesModel
    {

        public int L1_ID { get; set; }

        /// <summary>
        /// Gets or sets the L2_en value.
        /// </summary>
        public string L2_en { get; set; }

        /// <summary>
        /// Gets or sets the L2_ar value.
        /// </summary>
        public string L2_ar { get; set; }

        /// <summary>
        /// Gets or sets the L3_en value.
        /// </summary>
        public string L3_en { get; set; }

        /// <summary>
        /// Gets or sets the L3_ar value.
        /// </summary>
        public string L3_ar { get; set; }

        /// <summary>
        /// Gets or sets the L4_en value.
        /// </summary>
        public string L4_en { get; set; }

        /// <summary>
        /// Gets or sets the L4_ar value.
        /// </summary>
        public string L4_ar { get; set; }

        /// <summary>
        /// Gets or sets the L5_en value.
        /// </summary>
        public string L5_en { get; set; }

        /// <summary>
        /// Gets or sets the L5_ar value.
        /// </summary>
        public string L5_ar { get; set; }
    }

    public class CEUML_configModel
    {
        public int CEUML_configId { get; set; }

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the CeUser value.
        /// </summary>
        public string CeUser { get; set; }

        /// <summary>
        /// Gets or sets the Token value.
        /// </summary>
        public string Token { get; set; }
    }

    public class FirstLevelTblModel
    {
        public int L1_ID { get; set; }

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the L1-en value.
        /// </summary>
        public string L1_en { get; set; }

        /// <summary>
        /// Gets or sets the L1-ar value.
        /// </summary>
        public string L1_ar { get; set; }
    }
}
