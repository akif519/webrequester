//-----------------------------------------------------------------------
// <copyright file="Dbo_workorders_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using CMMS.Model;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_workorders_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_workorders_CT")]
    public sealed class Dbo_workorders_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

       

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the ProblemDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Gets or sets the WorkStatus value.
        /// </summary>
        [NotMapped]
        public string WorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeDescription value.
        /// </summary>
        [NotMapped]
        public string WorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the WorkPriority value.
        /// </summary>
        [NotMapped]
        public string WorkPriority { get; set; }

        /// <summary>
        /// Gets or sets the WorkTrade value.
        /// </summary>
        [NotMapped]
        public string WorkTrade { get; set; }


        /// <summary>
        /// Gets or sets the PMTarStartDate value.
        /// </summary>
        public string PMTarStartDate { get; set; }

        /// <summary>
        /// Gets or sets the PMTarCompDate value.
        /// </summary>
        public string PMTarCompDate { get; set; }

        /// <summary>
        /// Gets or sets the AstartDate value.
        /// </summary>
        public string AstartDate { get; set; }

        /// <summary>
        /// Gets or sets the AEndDate value.
        /// </summary>
        public string AEndDate { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string L2Code { get; set; }
        [NotMapped]
        public string L3No { get; set; }
        [NotMapped]
        public string L4No { get; set; }
        [NotMapped]
        public string L5No { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the ActionTaken value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string ActionTaken { get; set; }

        /// <summary>
        /// Gets or sets the PreventionTaken value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string PreventionTaken { get; set; }

        /// <summary>
        /// Gets or sets the DateReceived value.
        /// </summary>
        public string DateReceived { get; set; }

        /// <summary>
        /// Gets or sets the DateRequired value.
        /// </summary>
        public string DateRequired { get; set; }

        /// <summary>
        /// Gets or sets the ActDateStart value.
        /// </summary>
        public string ActDateStart { get; set; }

        /// <summary>
        /// Gets or sets the ActDateEnd value.
        /// </summary>
        public string ActDateEnd { get; set; }
        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        public string EstDateStart { get; set; }

        /// <summary>
        /// Gets or sets the EstDateEnd value.
        /// </summary>
        public string EstDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the DateHandover value.
        /// </summary>
        public string DateHandover { get; set; }

        /// <summary>
        /// Gets or sets the WOCost value.
        /// </summary>
        public float? WOCost { get; set; }

        /// <summary>
        /// Gets or sets the EstDuration value.
        /// </summary>
        public float? EstDuration { get; set; }

        [NotMapped]
        public string WOClosedBy { get; set; }

        [NotMapped]
        public string GroupCode { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        [NotMapped]
        public string AcceptedName { get; set; }

        [NotMapped]
        public string FailureCauseCode { get; set; }

        [NotMapped]
        public string CostCenterNo { get; set; }

        [NotMapped]
        public string CheckListNo { get; set; }

        [NotMapped]
        public string RateNo { get; set; }

        [NotMapped]
        public string AuthEmp { get; set; }

        [NotMapped]
        public string Requester { get; set; }

        /// <summary>
        /// Gets or sets the WOLaborCost value.
        /// </summary>
        public float? WOLaborCost { get; set; }

        /// <summary>
        /// Gets or sets the WODICost value.
        /// </summary>
        public float? WODICost { get; set; }

        /// <summary>
        /// Gets or sets the WOPartCost value.
        /// </summary>
        public float? WOPartCost { get; set; }

        /// <summary>
        /// Gets or sets the WOPMtype value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WOPMtype { get; set; }

        /// <summary>
        /// Gets or sets the PMJOGeneratedReading value.
        /// </summary>
        public float? PMJOGeneratedReading { get; set; }

        /// <summary>
        /// Gets or sets the PMMetReading value.
        /// </summary>
        public float? PMMetReading { get; set; }

        /// <summary>
        /// Gets or sets the DownTime value.
        /// </summary>
        public float? DownTime { get; set; }

        /// <summary>
        /// Gets or sets the LineDowntime value.
        /// </summary>
        public float? LineDowntime { get; set; }

        /// <summary>
        /// Gets or sets the MTask value.
        /// </summary>
        public int? MTask { get; set; }

        /// <summary>
        /// Gets or sets the Notes value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the Print_date value.
        /// </summary>
        public string Print_date { get; set; }

        /// <summary>
        /// Gets or sets the AssignTo value.
        /// </summary>
        [StringLength(1024, ErrorMessage = "*")]
        public string AssignTo { get; set; }

        /// <summary>
        /// Gets or sets the TelNo value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string TelNo { get; set; }


        [NotMapped]
        public string AccountCode { get; set; }

        [NotMapped]
        public string Statusname { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }
        ///// <summary>
        ///// Gets or sets the RequestorID value.
        ///// </summary>
        //public int? RequestorID { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeID value.
        ///// </summary>
        //public int? EmployeeID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L3ID value.
        ///// </summary>
        //public int? L3ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L4ID value.
        ///// </summary>
        //public int? L4ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L5ID value.
        ///// </summary>
        //public int? L5ID { get; set; }

        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetID value.
        ///// </summary>
        //public int? AssetID { get; set; }



        ///// <summary>
        ///// Gets or sets the WorkPriorityID value.
        ///// </summary>
        //public int? WorkPriorityID { get; set; }

        ///// <summary>
        ///// Gets or sets the FailureCauseID value.
        ///// </summary>
        //public int? FailureCauseID { get; set; }

        ///// <summary>
        ///// Gets or sets the WorkTypeID value.
        ///// </summary>
        //public int? WorkTypeID { get; set; }

        ///// <summary>
        ///// Gets or sets the WOTradeID value.
        ///// </summary>
        //public int? WOTradeID { get; set; }

        ///// <summary>
        ///// Gets or sets the MeterID value.
        ///// </summary>
        //public int? MeterID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMID value.
        ///// </summary>
        //public int? PMID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDivisionID value.
        ///// </summary>
        //public int? MaintDivisionID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdeptID value.
        ///// </summary>
        //public int? MaintdeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintsubdeptID value.
        ///// </summary>
        //public int? MaintsubdeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the GroupID value.
        ///// </summary>
        //public int? GroupID { get; set; }

        ///// <summary>
        ///// Gets or sets the SupplierId value.
        ///// </summary>
        //public int? SupplierId { get; set; }

        ///// <summary>
        ///// Gets or sets the AcceptedbyID value.
        ///// </summary>
        //public int? AcceptedbyID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMChecklistID value.
        ///// </summary>
        //public int? PMChecklistID { get; set; }

        ///// <summary>
        ///// Gets or sets the RatingsID value.
        ///// </summary>
        //public int? RatingsID { get; set; }

        ///// <summary>
        ///// Gets or sets the WOclosebyID value.
        ///// </summary>
        //public int? WOclosebyID { get; set; }

        ///// <summary>
        ///// Gets or sets the CostCenterId value.
        ///// </summary>
        //public int? CostCenterId { get; set; }


       



       

        



        ///// <summary>
        ///// Gets or sets the CauseDescription value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string CauseDescription { get; set; }



       


        

        

        ///// <summary>
        ///// Gets or sets the MPMID value.
        ///// </summary>
        //public long? MPMID { get; set; }

     


        /////// <summary>
        /////// Gets or sets the CreatedBy value.
        /////// </summary>
        ////[StringLength(100, ErrorMessage = "*")]
        ////public string CreatedBy { get; set; }

        /////// <summary>
        /////// Gets or sets the CreatedBy Name value.
        /////// </summary>
        ////[NotMapped]
        ////public string CreatedByName { get; set; }

        /////// <summary>
        /////// Gets or sets the CreatedDate value.
        /////// </summary>
        ////public string CreatedDate { get; set; }

       

        ///// <summary>
        ///// Gets or sets the AuthorisedEmployeeId value.
        ///// </summary>
        //public int? AuthorisedEmployeeId { get; set; }

        ///// <summary>
        ///// Gets or sets the CIGID value.
        ///// </summary>
        //public int? CIGID { get; set; }

        ///// <summary>
        ///// Gets or sets the IsCleaningModule value.
        ///// </summary>
        //public bool? IsCleaningModule { get; set; }

        ///// <summary>
        ///// Gets or sets the ProjectNumber value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string ProjectNumber { get; set; }

        ///// <summary>
        ///// Gets or sets the ProjectID value.
        ///// </summary>
        //public int? ProjectID { get; set; }

        #endregion
    }
}
