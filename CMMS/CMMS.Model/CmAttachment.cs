﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cmAttachment
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmAttachment")]
    public  class CmAttachment : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AutoId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoId { get; set; }

        /// <summary>
        /// Gets or sets the FkId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string FkId { get; set; }

        /// <summary>
        /// Gets or sets the FileLink value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileLink { get; set; }

        /// <summary>
        /// Gets or sets the FileDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileDescription { get; set; }

        /// <summary>
        /// Gets or sets the FileName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the ModuleType value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ModuleType { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
