﻿//-----------------------------------------------------------------------
// <copyright file="Stockcode_level.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - stockcode_levels
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("stockcode_levels")]
    public sealed class Stockcode_level : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Stock_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Stock_id { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the Manufacturer value.
        /// </summary>
        [NotMapped]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the Sub_store_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Sub_store_id { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the Sub_storecode value.
        /// </summary>
        [NotMapped]
        public string SubStoreCode { get; set; }

        [NotMapped]
        public string SubStoreDesc { get; set; }

        [NotMapped]
        public string AltSubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the PartLocationID value.
        /// </summary>
        public int? PartLocationID { get; set; }

        /// <summary>
        /// Gets or sets the Sub_storecode value.
        /// </summary>
        [NotMapped]
        public string PartLocation { get; set; }

        /// <summary>
        /// Gets or sets the Max_level value.
        /// </summary>
        public decimal? Max_level { get; set; }

        /// <summary>
        /// Gets or sets the Re_order_level value.
        /// </summary>
        public decimal? Re_order_level { get; set; }

        /// <summary>
        /// Gets or sets the Min_level value.
        /// </summary>
        public decimal? Min_level { get; set; }

        /// <summary>
        /// Gets or sets the Reorder_qty value.
        /// </summary>
        public decimal? Reorder_qty { get; set; }

        /// <summary>
        /// Gets or sets the Balance value.
        /// </summary>
        public decimal? Balance { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string Specification { get; set; }

        /// <summary>
        /// Gets or sets the BalanceStock value.
        /// </summary>
         [NotMapped]
        public decimal? BalanceStock { get; set; }
        #endregion
    }
}
