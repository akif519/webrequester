//-----------------------------------------------------------------------
// <copyright file="Dbo_workorderlabor_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_workorderlabor_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_workorderlabor_CT")]
	public sealed class Dbo_workorderlabor_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

		

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        [NotMapped]
        public string EmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the HourlySalary value.
        /// </summary>
        public decimal? HourlySalary { get; set; }

        /// <summary>
        /// Gets or sets the Comment value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the OverTime1 value.
        /// </summary>
        public decimal? OverTime1 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime2 value.
        /// </summary>
        public decimal? OverTime2 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime3 value.
        /// </summary>
        public decimal? OverTime3 { get; set; }

        /// <summary>
        /// Gets or sets the StartDate value.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate value.
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets the TotHour value.
        /// </summary>
        public float? TotHour { get; set; }

        /// <summary>
        /// Gets or sets the TotCost value.
        /// </summary>
        public decimal? TotCost { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }


        ///// <summary>
        ///// Gets or sets the WorkorderLaborID value.
        ///// </summary>
        //public int? WorkorderLaborID { get; set; }

		///// <summary>
        ///// Gets or sets the EmployeeID value.
        ///// </summary>
        //public int? EmployeeID { get; set; }

		

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

		#endregion
	}
}
