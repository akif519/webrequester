//-----------------------------------------------------------------------
// <copyright file="Material_req_detail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - material_req_details
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("material_req_details")]
    public sealed class Material_req_detail : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Mr_id value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Mr_id { get; set; }

        /// <summary>
        /// Gets or sets the Mr_no value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string Mr_no { get; set; }

        /// <summary>
        /// Gets or sets the Partid value.
        /// </summary>
        public int? Partid { get; set; }

        /// <summary>
        /// Gets or sets the Part_desc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Part_desc { get; set; }

        /// <summary>
        /// Gets or sets the Altpart_desc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Altpart_desc { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the Uom value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Uom { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the AssetId value.
        /// </summary>
        public int? AssetId { get; set; }

        /// <summary>
        /// Gets or sets the LocationId value.
        /// </summary>
        public int? LocationId { get; set; }

        /// <summary>
        /// Gets or sets the ReceivedQty value.
        /// </summary>
        public decimal? ReceivedQty { get; set; }

        /// <summary>
        /// Gets or sets the CanceledQty value.
        /// </summary>
        public decimal? CanceledQty { get; set; }

        /// <summary>
        /// Gets or sets the SubWHSupId value.
        /// </summary>
        public int? SubWHSupId { get; set; }

        /// <summary>
        /// Gets or sets the SubWHIssueTechId value.
        /// </summary>
        public int? SubWHIssueTechId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public int ? StockID { get; set; }

        [NotMapped]
        public int isStock { get; set; }
   
        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string LocationDescription { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string AssetDescription { get; set; }

        [NotMapped]
        public string AssetAltDescription { get; set; }

        [NotMapped]
        public string supName { get; set; }

        [NotMapped]
        public string supNo { get; set; }

        [NotMapped]
        public string issueNo { get; set; }

        [NotMapped]
        public string issueName { get; set; }

        [NotMapped]
        public decimal? AvailableQty { get; set; }

        [NotMapped]
        public Guid Guid_ItemID { get; set; }

        #endregion
    }
}
