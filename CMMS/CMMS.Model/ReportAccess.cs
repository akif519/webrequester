﻿//-----------------------------------------------------------------------
// <copyright file="ReportAccess.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - ReportAccess
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>14-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("ReportAccess")]
    public sealed class ReportAccess : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the Assets value.
        /// </summary>
        public int? Assets { get; set; }

        /// <summary>
        /// Gets or sets the JobOrder value.
        /// </summary>
        public int? JobOrder { get; set; }

        /// <summary>
        /// Gets or sets the Preventive value.
        /// </summary>
        public int? Preventive { get; set; }

        /// <summary>
        /// Gets or sets the Store value.
        /// </summary>
        public int? Store { get; set; }

        /// <summary>
        /// Gets or sets the Employee value.
        /// </summary>
        public int? Employee { get; set; }

        /// <summary>
        /// Gets or sets the Setups value.
        /// </summary>
        public int? Setups { get; set; }

        /// <summary>
        /// Gets or sets the Purchasing value.
        /// </summary>
        public int? Purchasing { get; set; }

        /// <summary>
        /// Gets or sets the Cleaning value.
        /// </summary>
        public int? Cleaning { get; set; }

        #endregion
    }
}
