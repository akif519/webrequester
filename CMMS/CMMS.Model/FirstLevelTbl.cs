﻿//-----------------------------------------------------------------------
// <copyright file="FirstLevelTbl.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - FirstLevelTbl
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>14-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("FirstLevelTbl")]
	public sealed class FirstLevelTbl : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the L1_ID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int L1_ID { get; set; }

		/// <summary>
		/// Gets or sets the ClientID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int ClientID { get; set; }

		/// <summary>
		/// Gets or sets the L1-en value.
		/// </summary>
		[StringLength(-1, ErrorMessage = "*")]
		public string L1_en { get; set; }

		/// <summary>
		/// Gets or sets the L1-ar value.
		/// </summary>
		[StringLength(-1, ErrorMessage = "*")]
		public string L1_ar { get; set; }

		#endregion
	}
}
