﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - return
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("\"return\"")]
    public sealed class Return : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ReturnID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReturnID { get; set; }

        /// <summary>
        /// Gets or sets the GUID value.
        /// </summary>
        [NotMapped]
        public string GUID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        public int? StockID { get; set; }

        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreCode value.
        /// </summary>
        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the TotalPrice value.
        /// </summary>
        [NotMapped]
        public decimal? TotalPrice { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the Location Description value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the Location Alt Description value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the HdnL2ID value.
        /// </summary>
        [NotMapped]
        public int? HdnL2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnSubStoreID value.
        /// </summary>
        [NotMapped]
        public int? HdnSubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreID value.
        /// </summary>
        public int? SubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

        /// <summary>
        /// Gets or sets the HDnWONo value.
        /// </summary>
        [NotMapped]
        public string HDNWoNo { get; set; }


        /// <summary>
        /// Gets or sets the Qty Issued value.
        /// </summary>
        [NotMapped]
        public decimal? QtyIssued { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public decimal? Qty { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the Person value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Person { get; set; }

        /// <summary>
        /// Gets or sets the Comment value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the RetPrice value.
        /// </summary>
        public decimal? RetPrice { get; set; }

        /// <summary>
        /// Gets or sets the BatchNo value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptDesc value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptDesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptdesc value.
        /// </summary>
        [NotMapped]
        public string MaintDeptdesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionName value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionName { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string SubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the List of Issue value.
        /// </summary>
        [NotMapped]
        public string lstReturn { get; set; }

        /// <summary>
        /// Gets or sets the TransactionID value.
        /// </summary>
        public long? TransactionID { get; set; }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

        #endregion  
    }
}
