//-----------------------------------------------------------------------
// <copyright file="JOStatusTrack.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - JOStatusTrack
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>29-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("JOStatusTrack")]
	public sealed class JOStatusTrack : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the JOStatusTrackID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int JOStatusTrackID { get; set; }

		/// <summary>
		/// Gets or sets the WorkorderNo value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(50, ErrorMessage = "*")]
		public string WorkorderNo { get; set; }

		/// <summary>
		/// Gets or sets the Old_WorkStatusID value.
		/// </summary>
		public int? Old_WorkStatusID { get; set; }

		/// <summary>
		/// Gets or sets the New_WorkStatusID value.
		/// </summary>
		public int? New_WorkStatusID { get; set; }

		/// <summary>
		/// Gets or sets the Latitude value.
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the Longitude value.
		/// </summary>
		public float? Longitude { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the old work status.
        /// </summary>
        /// <value>
        /// The old work status.
        /// </value>
        [NotMapped]
        public string OldWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the old alt work status.
        /// </summary>
        /// <value>
        /// The old alt work status.
        /// </value>
        [NotMapped]
        public string OldAltWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the new work status.
        /// </summary>
        /// <value>
        /// The new work status.
        /// </value>
        [NotMapped]
        public string NewWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the new alt work status.
        /// </summary>
        /// <value>
        /// The new alt work status.
        /// </value>
        [NotMapped]
        public string NewAltWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt.
        /// </summary>
        /// <value>
        /// The name of the alt.
        /// </value>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

		#endregion
	}
}
