﻿//-----------------------------------------------------------------------
// <copyright file="EmailNotification_SubCategory.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - EmailNotification_SubCategory
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>11-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("EmailNotification_SubCategory")]
    public sealed class EmailNotification_SubCategory : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationSubCategoryID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationSubCategoryID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        public int? NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the AssetSubCatID value.
        /// </summary>
        public int? AssetSubCatID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public int AssetCatID { get; set; }

        [NotMapped]
        public string AssetCatCode { get; set; }

        [NotMapped]
        public string AssetCategory { get; set; }

        [NotMapped]
        public string AltAssetCategory { get; set; }

        [NotMapped]
        public string AssetSubCatCode { get; set; }

        [NotMapped]
        public string AssetSubCategory { get; set; }

        [NotMapped]
        public string AltAssetSubCategory { get; set; }

        #endregion
    }
}
