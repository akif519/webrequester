//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_asset.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_assets
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_assets")]
	public sealed class Emailnotificationrules_asset : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleAssetID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleAssetID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the AssetID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int? AssetID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }


        [NotMapped]
        public int LocationID { get; set; }

        [NotMapped]
        public int AssetCatID { get; set; }

        [NotMapped]
        public int PartsListID { get; set; }

        [NotMapped]
        public int? CriticalityID { get; set; }

        [NotMapped]
        public int AssetStatusID { get; set; }

        [NotMapped]
        public int EmployeeID { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string AssetDescription { get; set; }

        [NotMapped]
        public string AssetAltDescription { get; set; }

        [NotMapped]
        public string ModelNumber { get; set; }

        [NotMapped]
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the asset status desc.
        /// </summary>
        /// <value>
        /// The asset status desc.
        /// </value>
        [NotMapped]
        public string AssetStatusDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt asset status desc.
        /// </summary>
        /// <value>
        /// The alt asset status desc.
        /// </value>
        [NotMapped]
        public string AltAssetStatusDesc { get; set; }

        /// <summary>
        /// Gets or sets the location no.
        /// </summary>
        /// <value>
        /// The location no.
        /// </value>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the location description.
        /// </summary>
        /// <value>
        /// The location description.
        /// </value>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the location alt description.
        /// </summary>
        /// <value>
        /// The location alt description.
        /// </value>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the asset cat code.
        /// </summary>
        /// <value>
        /// The asset cat code.
        /// </value>
        [NotMapped]
        public string AssetCatCode { get; set; }

        /// <summary>
        /// Gets or sets the asset sub cat code.
        /// </summary>
        /// <value>
        /// The asset sub cat code.
        /// </value>
        [NotMapped]
        public string AssetSubCatCode { get; set; }

        /// <summary>
        /// Gets or sets the asset condition code.
        /// </summary>
        /// <value>
        /// The asset condition code.
        /// </value>
        [NotMapped]
        public string AssetConditionCode { get; set; }

        /// <summary>
        /// Gets or sets the asset class code.
        /// </summary>
        /// <value>
        /// The asset class code.
        /// </value>
        [NotMapped]
        public string AssetClassCode { get; set; }

        /// <summary>
        /// Gets or sets the asset class.
        /// </summary>
        /// <value>
        /// The asset class.
        /// </value>
        [NotMapped]
        public string AssetClass { get; set; }

        /// <summary>
        /// Gets or sets the alt asset class.
        /// </summary>
        /// <value>
        /// The alt asset class.
        /// </value>
        [NotMapped]
        public string AltAssetClass { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2.
        /// </summary>
        /// <value>
        /// The name of the l2.
        /// </value>
        [NotMapped]
        public string L2Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2 alt.
        /// </summary>
        /// <value>
        /// The name of the l2 alt.
        /// </value>
        [NotMapped]
        public string L2AltName { get; set; }

        /// <summary>
        /// Gets or sets the l2 class code.
        /// </summary>
        /// <value>
        /// The l2 class code.
        /// </value>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the l3 no.
        /// </summary>
        /// <value>
        /// The l3 no.
        /// </value>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the l3 desc.
        /// </summary>
        /// <value>
        /// The l3 desc.
        /// </value>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the l3 alt desc.
        /// </summary>
        /// <value>
        /// The l3 alt desc.
        /// </value>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the l4 no.
        /// </summary>
        /// <value>
        /// The l4 no.
        /// </value>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the l4 description.
        /// </summary>
        /// <value>
        /// The l4 description.
        /// </value>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the l4 alt description.
        /// </summary>
        /// <value>
        /// The l4 alt description.
        /// </value>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the l5 no.
        /// </summary>
        /// <value>
        /// The l5 no.
        /// </value>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the l5 description.
        /// </summary>
        /// <value>
        /// The l5 description.
        /// </value>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the l5 alt description.
        /// </summary>
        /// <value>
        /// The l5 alt description.
        /// </value>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the sector code.
        /// </summary>
        /// <value>
        /// The sector code.
        /// </value>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector.
        /// </summary>
        /// <value>
        /// The name of the sector.
        /// </value>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector alt.
        /// </summary>
        /// <value>
        /// The name of the sector alt.
        /// </value>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the Manufacturer value.
        /// </summary>
        [NotMapped]
        public string Manufacturer { get; set; }
		#endregion
	}
}
