﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - WR_ProblemDesc By: Akif519
    /// </summary>
    /// <CreatedBy>Mohammad Aqeef</CreatedBy>
    /// <CreatedDate>14-Mar-2018</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WR_ProblemDesc")]
    public sealed class WR_ProblemDesc : BaseModel
    {
        #region Properties
        public int? ProblemDescId { get; set; }
        public Nullable<int> ProblemTypeIdRef { get; set; }

        [StringLength(50, ErrorMessage = "*")]
        public string ProblemDescCode { get; set; }

        [StringLength(1500, ErrorMessage = "*")]
        public string ProblemDesc { get; set; }

        [StringLength(1500, ErrorMessage = "*")]
        public string ProblemDescAr { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModfiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        #endregion
    }
}
