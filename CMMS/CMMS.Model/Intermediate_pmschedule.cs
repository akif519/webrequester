﻿//-----------------------------------------------------------------------
// <copyright file="Intermediate_pmschedule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - intermediate_pmschedule
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("intermediate_pmschedule")]
    public sealed class Intermediate_pmschedule : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        public int? PMID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the PhyLocationID value.
        /// </summary>
        public int? PhyLocationID { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistID value.
        /// </summary>
        public int? ChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeID value.
        /// </summary>
        public int? WorkTypeID { get; set; }

        /// <summary>
        /// Gets or sets the Worktradeid value.
        /// </summary>
        public int? Worktradeid { get; set; }

        /// <summary>
        /// Gets or sets the TypePMgenID value.
        /// </summary>
        public int? TypePMgenID { get; set; }

        /// <summary>
        /// Gets or sets the Workpriorityid value.
        /// </summary>
        public int? Workpriorityid { get; set; }

        /// <summary>
        /// Gets or sets the PMNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string AltPMName { get; set; }

        /// <summary>
        /// Gets or sets the PeriodDays value.
        /// </summary>
        public int? PeriodDays { get; set; }

        /// <summary>
        /// Gets or sets the NextSchComp value.
        /// </summary>
        public DateTime? NextSchComp { get; set; }

        /// <summary>
        /// Gets or sets the FreqUnits value.
        /// </summary>
        public int? FreqUnits { get; set; }

        /// <summary>
        /// Gets or sets the Frequency value.
        /// </summary>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the TargetStartDate value.
        /// </summary>
        public DateTime TargetStartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetCompDate value.
        /// </summary>
        public DateTime? TargetCompDate { get; set; }

        /// <summary>
        /// Gets or sets the ActualCompDate value.
        /// </summary>
        public DateTime? ActualCompDate { get; set; }

        /// <summary>
        /// Gets or sets the NextDate value.
        /// </summary>
        public DateTime? NextDate { get; set; }

        /// <summary>
        /// Gets or sets the TypePM value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string TypePM { get; set; }

        /// <summary>
        /// Gets or sets the PMCounter value.
        /// </summary>
        public int? PMCounter { get; set; }

        /// <summary>
        /// Gets or sets the PMMultiple value.
        /// </summary>
        public int? PMMultiple { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the WeekofMonth value.
        /// </summary>
        public int? WeekofMonth { get; set; }

        /// <summary>
        /// Gets or sets the DayofWeek value.
        /// </summary>
        public int? DayofWeek { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the GroupId value.
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// Gets or sets the Userid value.
        /// </summary>
        public int? Userid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsCleaningModule is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsCleaningModule { get; set; }

        #endregion
    }
}
