﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - SearchFilters
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>29-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("SearchFilters")]
    public sealed class SearchFilter : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SearchFilterID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SearchFilterID { get; set; }

        /// <summary>
        /// Gets or sets the PageID value.
        /// </summary>
        [NotMapped]
        public int PageID { get; set; }

        /// <summary>
        /// Gets or sets the SearchFieldID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SearchFieldID { get; set; }


        /// <summary>
        /// Gets or sets the DBColumnType value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string DBColumnType { get; set; }


        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the OperatorID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int OperatorID { get; set; }

        /// <summary>
        /// Gets or sets the Value1 value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(3000, ErrorMessage = "*")]
        public string Value1 { get; set; }

        /// <summary>
        /// Gets or sets the Value2 value.
        /// </summary>
        [StringLength(3000, ErrorMessage = "*")]
        public string Value2 { get; set; }

        /// <summary>
        /// Gets or sets the Condition value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string Condition { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedOn value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public DateTime? CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedOn value.
        /// </summary>
        public DateTime? ModifiedOn { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string CustomReportFilter { get; set; }

        /// <summary>
        /// Gets or sets the IsSaved Value.
        /// </summary>
        public bool IsSaved { get; set; }


        #endregion
    }
}
