﻿//-----------------------------------------------------------------------
// <copyright file="Assetsubcategorie.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - assetsubcategories
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("assetsubcategories")]
    public sealed class AssetSubCategorie : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AssetSubCatID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssetSubCatID { get; set; }

        /// <summary>
        /// Gets or sets the AssetSubCatCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string AssetSubCatCode { get; set; }

        /// <summary>
        /// Gets or sets the AssetSubCategory value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string AssetSubCategory { get; set; }

        /// <summary>
        /// Gets or sets the AltAssetSubCategory value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltAssetSubCategory { get; set; }

        /// <summary>
        /// Gets or sets the AssetCatID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int AssetCatID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the AssetClassID value.
        /// </summary>
        public int? AssetClassID { get; set; }

        /// <summary>
        /// Gets or sets the asset cat code.
        /// </summary>
        /// <value>
        /// The asset cat code.
        /// </value>
        [NotMapped]
        public string AssetCatCode { get; set; }
        #endregion
    }
}
