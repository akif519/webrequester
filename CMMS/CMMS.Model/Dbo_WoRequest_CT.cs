﻿//-----------------------------------------------------------------------
// <copyright file="Dbo_WoRequest_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_WoRequest_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>10-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_WoRequest_CT")]
    public sealed class Dbo_WoRequest_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the RequestNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string RequestNo { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the ProblemDesc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string ProblemDesc { get; set; }

        /// <summary>
        /// Gets or sets the ReceivedDate value.
        /// </summary>
        public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Gets or sets the RequiredDate value.
        /// </summary>
        public DateTime? RequiredDate { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the ExternalRequesterName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string ExternalRequesterName { get; set; }


        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the work status name value.
        /// </summary>
        [NotMapped]
        public string JobStatus { get; set; }

        /// <summary>
        /// Gets or sets the Alt Job Status value.
        /// </summary>
        [NotMapped]
        public string AltWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the Work Type Description value.
        /// </summary>
        [NotMapped]
        public string WorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AltWorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets Name value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }
        
        /// <summary>
        /// Gets or sets the WorkPriorityName value.
        /// </summary>
        [NotMapped]
        public string WorkPriorityName { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AltWorkPriority { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [NotMapped]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [NotMapped]
        public string AssetAltDescription { get; set; }



        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2ClassCode value.
        /// </summary>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L3Desc value.
        /// </summary>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the L3AltDesc value.
        /// </summary>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L4Description value.
        /// </summary>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the L4AltDescription value.
        /// </summary>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the L5AltDescription value.
        /// </summary>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [NotMapped]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the ProjectName value.
        /// </summary>
        [NotMapped]
        public string ProjectName { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }



        #endregion
    }
}
