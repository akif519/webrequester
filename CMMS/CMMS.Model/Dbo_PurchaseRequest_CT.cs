//-----------------------------------------------------------------------
// <copyright file="Dbo_PurchaseRequest_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_PurchaseRequest_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_PurchaseRequest_CT")]
    public sealed class Dbo_PurchaseRequest_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the Id value.
        ///// </summary>
        //public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestID value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string PurchaseRequestID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the DateRequired value.
        ///// </summary>
        //public DateTime? DateRequired { get; set; }

        ///// <summary>
        ///// Gets or sets the RequestedBy value.
        ///// </summary>
        //public int? RequestedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //public int? CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ApprovalStatus value.
        /// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string ApprovalStatus { get; set; }

        ///// <summary>
        ///// Gets or sets the OrderStatus value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string OrderStatus { get; set; }

        ///// <summary>
        ///// Gets or sets the Note value.
        ///// </summary>
        //[StringLength(500, ErrorMessage = "*")]
        //public string Note { get; set; }

        ///// <summary>
        ///// Gets or sets the Auth_status value.
        ///// </summary>
        //public int? Auth_status { get; set; }

        ///// <summary>
        ///// Gets or sets the CancelNotes value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string CancelNotes { get; set; }

        ///// <summary>
        ///// Gets or sets the Cancel_by value.
        ///// </summary>
        //public int? Cancel_by { get; set; }

        ///// <summary>
        ///// Gets or sets the Cancel_date value.
        ///// </summary>
        //public DateTime? Cancel_date { get; set; }

        ///// <summary>
        ///// Gets or sets the Status_id value.
        ///// </summary>
        //public int? Status_id { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdeptId value.
        ///// </summary>
        //public int? MaintdeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MainSubDeptId value.
        ///// </summary>
        //public int? MainSubDeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdivId value.
        ///// </summary>
        //public int? MaintdivId { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedDate value.
        /// </summary>
        //public DateTime? LastPrintedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedBy value.
        /// </summary>
        //public int? LastPrintedBy { get; set; }                

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }


        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the Maintainance dept code value.
        /// </summary>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the status_desc.
        /// </summary>
        /// <value>
        /// The status_desc.
        /// </value>
        [NotMapped]
        public string status_desc { get; set; }


        /// <summary>
        /// Gets or sets the auth_status_desc.
        /// </summary>
        /// <value>
        /// The auth_status_desc.
        /// </value>
        [NotMapped]
        public string auth_status_desc { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }
        #endregion
    }
}
