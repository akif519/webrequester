//-----------------------------------------------------------------------
// <copyright file="TblJobPlan.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - tblJobPlan
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("tblJobPlan")]
	public sealed class TblJobPlan : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the JobPlanID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int JobPlanID { get; set; }

		/// <summary>
		/// Gets or sets the JobPlanNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string JobPlanNo { get; set; }

		/// <summary>
		/// Gets or sets the JobPlanName value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
		public string JobPlanName { get; set; }

		/// <summary>
		/// Gets or sets the AltJobPlanName value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltJobPlanName { get; set; }

		/// <summary>
		/// Gets or sets the L2Id value.
		/// </summary>
		public int? L2Id { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the LST job plan item.
        /// </summary>
        /// <value>
        /// The LST job plan item.
        /// </value>
        [NotMapped]
        public List<TblJobPlanItem> lstJobPlanItem { get; set; }

        /// <summary>
        /// Gets or sets the jsonlst job plan item.
        /// </summary>
        /// <value>
        /// The jsonlst job plan item.
        /// </value>
        [NotMapped]
        public string JsonlstJobPlanItem { get; set; }

		#endregion
	}
}
