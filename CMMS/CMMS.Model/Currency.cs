//-----------------------------------------------------------------------
// <copyright file="Currency.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - Currency
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("Currency")]
	public sealed class Currency : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CurrencyID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CurrencyID { get; set; }

		/// <summary>
		/// Gets or sets the CurrencyCode value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(10, ErrorMessage = "*")]
		public string CurrencyCode { get; set; }

		/// <summary>
		/// Gets or sets the CurrencyDescription value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string CurrencyDescription { get; set; }

		/// <summary>
		/// Gets or sets the AltCurrencyDescription value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltCurrencyDescription { get; set; }

		/// <summary>
		/// Gets or sets the Rate value.
		/// </summary>
		public decimal? Rate { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
