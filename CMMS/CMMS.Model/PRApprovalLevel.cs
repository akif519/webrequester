//-----------------------------------------------------------------------
// <copyright file="PRApprovalLevel.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - PRApprovalLevels
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("PRApprovalLevels")]
	public sealed class PRApprovalLevel : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the PRApprovalLevelId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PRApprovalLevelId { get; set; }

		/// <summary>
		/// Gets or sets the LevelNo value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int LevelNo { get; set; }

		/// <summary>
		/// Gets or sets the LevelName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string LevelName { get; set; }

		/// <summary>
		/// Gets or sets the AltLevelName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltLevelName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the IsFinalLevel is enabled.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public bool IsFinalLevel { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the Createddate value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public DateTime Createddate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the Modifieddate value.
		/// </summary>
		public DateTime? Modifieddate { get; set; }

        [NotMapped]
        public int AutoId { get; set; }

        [NotMapped]
        public bool Mandatory { get; set; }

		#endregion
	}
}
