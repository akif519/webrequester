//-----------------------------------------------------------------------
// <copyright file="ToolSubCategory.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - ToolSubCategory
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>24-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("ToolSubCategory")]
	public sealed class ToolSubCategory : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ToolSubCategoryID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ToolSubCategoryID { get; set; }

		/// <summary>
		/// Gets or sets the ToolSubCategoryCode value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string ToolSubCategoryCode { get; set; }

		/// <summary>
		/// Gets or sets the ToolSubCategoryDesc value.
		/// </summary>
		[StringLength(2000, ErrorMessage = "*")]
		public string ToolSubCategoryDesc { get; set; }

		/// <summary>
		/// Gets or sets the ToolSubCategoryAltDesc value.
		/// </summary>
		[StringLength(2000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string ToolSubCategoryAltDesc { get; set; }

		/// <summary>
		/// Gets or sets the ToolCategoryID value.
		/// </summary>
		public int? ToolCategoryID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the tool category code.
        /// </summary>
        /// <value>
        /// The tool category code.
        /// </value>
        [NotMapped]
        public string ToolCategoryCode { get; set; }

		#endregion
	}
}
