﻿//-----------------------------------------------------------------------
// <copyright file="Supplier.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using CMMS.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - suppliers
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("suppliers")]
    public sealed class Supplier : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SupplierID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the SupplierNo value.
        /// </summary>
                
        [StringLength(50, ErrorMessage = "*")]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the SupplierName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AltSupplierName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the ContactName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the ContactTitle value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactTitle { get; set; }

        /// <summary>
        /// Gets or sets the Address value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the City value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the PostalCode value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the StateOrProvince value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string StateOrProvince { get; set; }

        /// <summary>
        /// Gets or sets the Country value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the PhoneNumber value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the FaxNumber value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the Services value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Services { get; set; }

        /// <summary>
        /// Gets or sets the Phone1 value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Phone1 { get; set; }

        /// <summary>
        /// Gets or sets the ContactName2 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactName2 { get; set; }

        /// <summary>
        /// Gets or sets the ContactTitle2 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactTitle2 { get; set; }

        /// <summary>
        /// Gets or sets the Phone2 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Phone2 { get; set; }

        /// <summary>
        /// Gets or sets the ContactName3 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactName3 { get; set; }

        /// <summary>
        /// Gets or sets the ContactTitle3 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ContactTitle3 { get; set; }

        /// <summary>
        /// Gets or sets the Phone3 value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Phone3 { get; set; }

        /// <summary>
        /// Gets or sets the Oem value.
        /// </summary>
        public short? Oem { get; set; }

        /// <summary>
        /// Gets or sets the AttrID value.
        /// </summary>
        public int? AttrID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the CategoryID value.
        /// </summary>
        public int? CategoryID { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Email1 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email1 { get; set; }

        /// <summary>
        /// Gets or sets the Email2 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email2 { get; set; }

        /// <summary>
        /// Gets or sets the Email3 value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Email3 { get; set; }

        /// <summary>
        /// Gets or sets the ClassificationTradeIds value.
        /// </summary>        
        public string ClassificationTradeIds { get; set; }

        /// <summary>
        /// Gets or sets the ClassificationLevel value.
        /// </summary>
        [AllowNullForSave]
        public int? ClassificationLevel { get; set; }

        /// <summary>
        /// Gets or sets the CR_No value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CR_No { get; set; }

        /// <summary>
        /// Gets or sets the CR_ExpiryDate value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? CR_ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the string c r_ expiry date.
        /// </summary>
        /// <value>
        /// The string c r_ expiry date.
        /// </value>
        [NotMapped]
        public string strCR_ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the supplier category.
        /// </summary>
        /// <value>
        /// The name of the supplier category.
        /// </value>
        [NotMapped]
        public string SupplierCategoryName { get; set; }

        /// <summary>
        /// Gets or sets the CategoryID value.
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Gets or sets the CategoryID value.
        /// </summary>
        public int? AccCodeid { get; set; }

        #endregion
    }
}
