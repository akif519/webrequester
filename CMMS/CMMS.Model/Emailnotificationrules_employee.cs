//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_employee.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_employees
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_employees")]
	public sealed class Emailnotificationrules_employee : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleEmployeeID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleEmployeeID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the IsSelectedCriticalProposal value.
		/// </summary>
		public bool? IsSelectedCriticalProposal { get; set; }

		/// <summary>
		/// Gets or sets the IsSelectedNormalProposal value.
		/// </summary>
		public bool? IsSelectedNormalProposal { get; set; }

		/// <summary>
		/// Gets or sets the LastEmailSentTime value.
		/// </summary>
		public DateTime? LastEmailSentTime { get; set; }

        [NotMapped]
		public string EmployeeNO { get; set; }

        [NotMapped]
		public string L2Code { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
		public string MaintDivisionCode { get; set; }

        [NotMapped]
		public string MaintDeptCode { get; set; }

        [NotMapped]
		public string MaintSubDeptCode { get; set; }

        [NotMapped]
		public string Positions { get; set; }

        [NotMapped]
		public string CategoryName { get; set; }

        [NotMapped]
		public string EmployeeStatus { get; set; }

		#endregion
	}
}
