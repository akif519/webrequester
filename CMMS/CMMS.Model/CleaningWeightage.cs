﻿//-----------------------------------------------------------------------
// <copyright file="Cleaningweightage.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - cleaningweightage
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cleaningweightage")]
    public sealed class CleaningWeightage : BaseModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CleaningWeightage"/> class.
        /// </summary>
        public CleaningWeightage()
        {
            PublicFloor = 0;
            PublicWall  = 0 ;
            PublicWindow = 0;
            PublicCeiling = 0;
            PublicFurniture = 0;
            PublicWaste = 0;
            PublicConsumables = 0;
            RoomFloor = 0;
            RoomWall = 0;
            RoomWindow = 0;
            RoomCeiling = 0;
            RoomFurniture = 0;
            RoomWaste = 0;
            RoomConsumables = 0;
            OutsideFloor = 0;
            OutsideWall = 0;
            OutsideWindow = 0;
            OutsideCeiling = 0;
            OutsideFurniture = 0;
            OutsideWaste = 0;
            OutsideConsumables = 0;
            SatisfactionLevel = 0;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the CleaningWeightageId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CleaningWeightageId { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the PublicFloor value.
        /// </summary>
        public int PublicFloor { get; set; }

        /// <summary>
        /// Gets or sets the PublicWall value.
        /// </summary>
        public int PublicWall { get; set; }

        /// <summary>
        /// Gets or sets the PublicWindow value.
        /// </summary>
        public int PublicWindow { get; set; }

        /// <summary>
        /// Gets or sets the PublicCeiling value.
        /// </summary>
        public int PublicCeiling { get; set; }

        /// <summary>
        /// Gets or sets the PublicFurniture value.
        /// </summary>
        public int PublicFurniture { get; set; }

        /// <summary>
        /// Gets or sets the PublicWaste value.
        /// </summary>
        public int PublicWaste { get; set; }

        /// <summary>
        /// Gets or sets the PublicConsumables value.
        /// </summary>
        public int PublicConsumables { get; set; }

        /// <summary>
        /// Gets or sets the RoomFloor value.
        /// </summary>
        public int RoomFloor { get; set; }

        /// <summary>
        /// Gets or sets the RoomWall value.
        /// </summary>
        public int RoomWall { get; set; }

        /// <summary>
        /// Gets or sets the RoomWindow value.
        /// </summary>
        public int RoomWindow { get; set; }

        /// <summary>
        /// Gets or sets the RoomCeiling value.
        /// </summary>
        public int RoomCeiling { get; set; }

        /// <summary>
        /// Gets or sets the RoomFurniture value.
        /// </summary>
        public int RoomFurniture { get; set; }

        /// <summary>
        /// Gets or sets the RoomWaste value.
        /// </summary>
        public int RoomWaste { get; set; }

        /// <summary>
        /// Gets or sets the RoomConsumables value.
        /// </summary>
        public int RoomConsumables { get; set; }

        /// <summary>
        /// Gets or sets the OutsideFloor value.
        /// </summary>
        public int OutsideFloor { get; set; }

        /// <summary>
        /// Gets or sets the OutsideWall value.
        /// </summary>
        public int OutsideWall { get; set; }

        /// <summary>
        /// Gets or sets the OutsideWindow value.
        /// </summary>
        public int OutsideWindow { get; set; }

        /// <summary>
        /// Gets or sets the OutsideCeiling value.
        /// </summary>
        public int OutsideCeiling { get; set; }

        /// <summary>
        /// Gets or sets the OutsideFurniture value.
        /// </summary>
        public int OutsideFurniture { get; set; }

        /// <summary>
        /// Gets or sets the OutsideWaste value.
        /// </summary>
        public int OutsideWaste { get; set; }

        /// <summary>
        /// Gets or sets the OutsideConsumables value.
        /// </summary>
        public int OutsideConsumables { get; set; }

        /// <summary>
        /// Gets or sets the SatisfactionLevel value.
        /// </summary>
        public int? SatisfactionLevel { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
