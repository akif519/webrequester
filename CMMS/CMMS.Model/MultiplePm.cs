﻿//-----------------------------------------------------------------------
// <copyright file="Multiplepm.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - multiplepm
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("multiplepm")]
    public sealed class MultiplePM : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PMID { get; set; }

        /// <summary>
        /// Gets or sets the MTaskID value.
        /// </summary>
        public int? MTaskID { get; set; }

        /// <summary>
        /// Gets or sets the MSeq value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int MSeq { get; set; }

        /// <summary>
        /// Gets or sets the MDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string MDescription { get; set; }

        /// <summary>
        /// Gets or sets the MStartDate value.
        /// </summary>
        public DateTime? MStartDate { get; set; }

        /// <summary>
        /// Gets or sets the MStartDateCount value.
        /// </summary>
        public int? MStartDateCount { get; set; }

        /// <summary>
        /// Gets or sets the MTaskSeq value.
        /// </summary>
        public int? MTaskSeq { get; set; }

        /// <summary>
        /// Gets or sets the MScount value.
        /// </summary>
        public int? MScount { get; set; }

        /// <summary>
        /// Gets or sets the MPMCounter value.
        /// </summary>
        public int? MPMCounter { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string CheckListNo { get; set; }

        [NotMapped]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [NotMapped]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [NotMapped]
        public string AltPMName { get; set; }

        [NotMapped]
        public int TypePMgenID { get; set; }

        [NotMapped]
        public string PMGroupNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [NotMapped]
        public string PMGroupName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [NotMapped]
        public string AltPMGroupName { get; set; }
        #endregion
    }
}
