﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - assigntoworkorder
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("assigntoworkorder")]
    public sealed class Assigntoworkorder : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public int SupplierId { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]

        public string AltSupplierName { get; set; }
        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public int MaintGroupID { get; set; }

        [NotMapped]
        public string MaintGroupCode { get; set; }

        [NotMapped]
        public string MaintGroupDesc { get; set; }

        [NotMapped]
        public string MaintGroupAltDesc { get; set; }

        /// <summary>
        /// Gets or sets the Employee NO value.
        /// </summary>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Employee Name value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the EstStartDate value.
        /// </summary>
        public DateTime? EstStartDate { get; set; }

        /// <summary>
        /// Gets or sets the EstStartTime value.
        /// </summary>
        [NotMapped]
        public DateTime? EstStartTime { get; set; }

        /// <summary>
        /// Gets or sets the EstEndDate value.
        /// </summary>
        public DateTime? EstEndDate { get; set; }

        /// <summary>
        /// Gets or sets the EstEndTime value.
        /// </summary>
        [NotMapped]
        public DateTime? EstEndTime { get; set; }

        /// <summary>
        /// Gets or sets the message value.
        /// </summary>
        [NotMapped]
        public string AssetWarrentyMessage { get; set; }

        /// <summary>
        /// Gets or sets the message value.
        /// </summary>
        [NotMapped]
        public int? LocationAuthEmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the message value.
        /// </summary>
        [NotMapped]
        public int? AssetAuthEmployeeID { get; set; }
        #endregion
    }
}
