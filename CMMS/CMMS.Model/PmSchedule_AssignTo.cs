﻿//-----------------------------------------------------------------------
// <copyright file="Pmschedule_assignto.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - pmschedule_assignto
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("pmschedule_assignto")]
    public sealed class PmSchedule_AssignTo : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PMID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmployeeID { get; set; }

        [NotMapped]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [NotMapped]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [NotMapped]
        public string AltPMName { get; set; }

        [NotMapped]
        public string EmployeeNO { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string PMGroupNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [NotMapped]
        public string PMGroupName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [NotMapped]
        public string AltPMGroupName { get; set; }

        [NotMapped]
        public bool IsCleaning { get; set; }
        #endregion
    }
}
