﻿//-----------------------------------------------------------------------
// <copyright file="L1.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - L1
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("L1")]
    public sealed class L1 : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L1ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int L1ID { get; set; }

        /// <summary>
        /// Gets or sets the L1No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the L1Name value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string L1Name { get; set; }

        /// <summary>
        /// Gets or sets the L1AltName value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string L1AltName { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public bool? IsAreaEnable { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        [NotMapped]
        public bool isChecked { get; set; }

        #endregion
    }
}
