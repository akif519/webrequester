﻿//-----------------------------------------------------------------------
// <copyright file="Extassetfile.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - extassetfiles
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("extassetfiles")]
    public sealed class ExtAssetFile : BaseModel
    {
        public ExtAssetFile()
        {
            IsSaveButtonEnable = true;
            IsDeleteButtonEnable = true;
            IsEditButtonEnable = true;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the AutoId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoId { get; set; }

        /// <summary>
        /// Gets or sets the FkId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string FkId { get; set; }

        /// <summary>
        /// Gets or sets the FileLink value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileLink { get; set; }

        /// <summary>
        /// Gets or sets the FileDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileDescription { get; set; }

        /// <summary>
        /// Gets or sets the FileName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the ModuleType value.
        /// </summary>
        [StringLength(2, ErrorMessage = "*")]
        public string ModuleType { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public int AssetID { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string AssetDescription { get; set; }

        [NotMapped]
        public string AssetAltDescription { get; set; }

        [NotMapped]
        public bool IsSaveButtonEnable { get; set; }

        [NotMapped]
        public bool IsDeleteButtonEnable { get; set; }

        [NotMapped]
        public bool IsEditButtonEnable { get; set; }

        [NotMapped]
        public bool IsLocationDiagram { get; set; }

        [NotMapped]
        public string FileSaveMessage { get; set; }

        [NotMapped]
        public string strFileToAttach { get; set; }

        [NotMapped]
        public byte[] FileToAttach
        {
            get
            {
                if (!string.IsNullOrEmpty(strFileToAttach))
                {
                    return Convert.FromBase64String(strFileToAttach);
                }
                return new byte[0];
            }
        }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

        [NotMapped]
        public int Extassetid { get; set; }

        #endregion
    }
}
