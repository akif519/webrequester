//-----------------------------------------------------------------------
// <copyright file="Sector.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - sector
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("sector")]
    public sealed class Sector : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SectorID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SectorID { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltname value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SectorAltname { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the DefaultCalendar value.
        /// </summary>
        public int? DefaultCalendar { get; set; }

        /// <summary>
        /// Gets or sets the Timezone value.
        /// </summary>
        public decimal? Timezone { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        public int? L1ID { get; set; }

        [NotMapped]
        public string L1No { get; set; }

        #endregion
    }
}
