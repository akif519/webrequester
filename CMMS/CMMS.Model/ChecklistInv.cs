//-----------------------------------------------------------------------
// <copyright file="ChecklistInv.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - checklistInv
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("checklistInv")]
	public sealed class ChecklistInv : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ChecklistInvId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ChecklistInvId { get; set; }

		/// <summary>
		/// Gets or sets the PartID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int PartID { get; set; }

		/// <summary>
		/// Gets or sets the Qty value.
		/// </summary>
		public float? Qty { get; set; }

		/// <summary>
		/// Gets or sets the ChecklistID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int ChecklistID { get; set; }

		/// <summary>
		/// Gets or sets the ChecklistelementId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int ChecklistelementId { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string PartsListNo { get; set; }
        [NotMapped]
        public string PartsListDescription { get; set; }
        [NotMapped]
        public string PartsListAltDescription { get; set; }
        [NotMapped]
        public int SeqNo { get; set; }

		#endregion
	}
}
