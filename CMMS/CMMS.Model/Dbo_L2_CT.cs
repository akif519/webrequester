//-----------------------------------------------------------------------
// <copyright file="Dbo_L2_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_L2_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>17-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_L2_CT")]
	public sealed class Dbo_L2_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the end_lsn value.
        ///// </summary>
        //public byte[] end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the L2Altname value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the Timezone value.
        /// </summary>
        public decimal? Timezone { get; set; }

        /// <summary>
        /// Gets or sets the DefaultCalendar value.
        /// </summary>
        public int? DefaultCalendar { get; set; }

        /// <summary>
        /// Gets or sets the operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L1ID value.
        ///// </summary>
        //public int? L1ID { get; set; }

		///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the SectorID value.
        ///// </summary>
        //public int? SectorID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ClassID value.
        ///// </summary>
        //public int? L2ClassID { get; set; }

        ///// <summary>
        ///// Gets or sets the Status value.
        ///// </summary>
        //public int? Status { get; set; }

        ///// <summary>
        ///// Gets or sets the CurrencyID value.
        ///// </summary>
        //public int? CurrencyID { get; set; }

        ///// <summary>
        ///// Gets or sets the DirectorDeputyName value.
        ///// </summary>
        //[StringLength(200, ErrorMessage = "*")]
        //public string DirectorDeputyName { get; set; }

        ///// <summary>
        ///// Gets or sets the SiteDirectorName value.
        ///// </summary>
        //[StringLength(200, ErrorMessage = "*")]
        //public string SiteDirectorName { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintManagerContractor value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string MaintManagerContractor { get; set; }

        ///// <summary>
        ///// Gets or sets the MoHSupervisorName value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string MoHSupervisorName { get; set; }

        ///// <summary>
        ///// Gets or sets the FaxNumber value.
        ///// </summary>
        //[StringLength(30, ErrorMessage = "*")]
        //public string FaxNumber { get; set; }

        ///// <summary>
        ///// Gets or sets the Phone value.
        ///// </summary>
        //[StringLength(30, ErrorMessage = "*")]
        //public string Phone { get; set; }

		#endregion
	}
}
