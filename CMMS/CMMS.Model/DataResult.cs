﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    
    public class DataResult : CommonWebServiceFields
    {
        public DataResult() { }
          
        public DataResult(List<Employee> data, int success, string message)
        {
            Data = data;
            Success = success;
            Message = message;
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
          
        public List<Employee> Data { get; set; }

    }

    public class CommonWebServiceFields
    {
       
        public int Success { get; set; }

       
        public string Message { get; set; }

       
        public string CurrentTimeStamp { get; set; }

       
        public Guid TokenId { get; set; }

       
        public int IsUserActive { get; set; }

       
        public int IsFKViolation { get; set; }

       
        public int IsStatusCloseAtServer { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public int TotalCount { get; set; }

        public int TotalPages { get; set; }

    }

    #region Logout Result
    
    public class LogoutResult : CommonWebServiceFields
    {
        public LogoutResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
            
        public int EmployeeID { get; set; }
    }

    #endregion


    public class ChangePasswordModel : CommonWebServiceFields
    {
        public ChangePasswordModel()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public int EmployeeID { get; set; }
                
        public string NewPassword { get; set; }
                
        public string OldPassword { get; set; }

    }

    public class GetWorkRequestMasterResult : CommonWebServiceFields
    {
        public GetWorkRequestMasterResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<L2> L2Table { get; set; }


        public List<Worktype> WorktypeTable { get; set; }


        public List<Workpriority> WorkpriorityTable { get; set; }

        
        public List<Worktrade> WorktradeTable { get; set; }

        
        public List<Sector> SectorTable { get; set; }

        
        public List<MainenanceDivision> MainenanceDivisionTable { get; set; }

        
        public List<MaintenanceDepartment> MaintenanceDepartmentTable { get; set; }

        
        public List<MaintSubDept> MaintSubDeptTable { get; set; }


        public List<Workstatu> WorkStatusTable { get; set; }
    }

    public class GetCurrentTimeStamp : CommonWebServiceFields
    {        
        public string TimeStamp { get; set; }
        public int? EmployeeID { get; set; }

    }

    public class _Employee 
    {
        public string Address { get; set; }
        public string AltName { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public Nullable<int> Central { get; set; }
        public string Email { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeNO { get; set; }
        public string Fax { get; set; }
        public string HandPhone { get; set; }
        public Nullable<int> L2ID { get; set; }
        public Nullable<int> UserGroupId { get; set; }
        public string WorkPhone { get; set; }
        public string Name { get; set; }
        public Nullable<int> EmployeeStatusId { get; set; }
        public int? HeartbeatValInSec { get; set; }
        public Nullable<int> MaintSubDeptID { get; set; }
        public Nullable<int> maintDeptID { get; set; }
        public Nullable<int> MaintDivisionID { get; set; }
    }

    public class GetWorkRequestAssetInfoMasterResult : CommonWebServiceFields
    {
        public GetWorkRequestAssetInfoMasterResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<_Employee> EmployeeTable { get; set; }

       // public List<Asset> AssetTable { get; set; }

        //public List<Location> LocationTable { get; set; }

        public List<L3> L3Table { get; set; }

        public List<L4> L4Table { get; set; }

        public List<L5> L5Table { get; set; }

       // public List<Employees_L2> Employee_L2Table { get; set; }

        public List<EmployeeStatuse> EmployeeStatusTable { get; set; }

        public List<Tbl_Feature> Table_FeatureTable { get; set; }

       // public List<Translation> TranslationTable { get; set; }

        public List<AssetStatus> AssetStatusTable { get; set; }

        public List<Employees_MaintSubDept> Employees_MaintSubDeptTable { get; set; }

    }

    public class GetWorkRequestAssetInfoMasteAssetTablerResult : CommonWebServiceFields
    {
        public GetWorkRequestAssetInfoMasteAssetTablerResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<Asset> AssetTable { get; set; }       

    }

    public class GetWorkRequestAssetInfoMasterEmployeesL2Result : CommonWebServiceFields
    {
        public GetWorkRequestAssetInfoMasterEmployeesL2Result()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
     
        public List<Employees_L2> Employee_L2Table { get; set; }
    }


    public class GetEmployeesLocationResult : CommonWebServiceFields
    {
        public GetEmployeesLocationResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<_Employees_Location> Employee_LocationTable { get; set; }
    }

    public class _Employees_Location
    {
        public int LocationID { get; set; }
        public int EmployeeID { get; set; }
        public string CreatedBy { get; set; }        
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }       
    }




    public class GetWorkRequestAssetInfoMasterLocationResult : CommonWebServiceFields
    {
        public GetWorkRequestAssetInfoMasterLocationResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<Location> LocationTable { get; set; }
    }



    public class GetWorkRequestAssetInfoMasterTranslationTableResult : CommonWebServiceFields
    {
        public GetWorkRequestAssetInfoMasterTranslationTableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<Translation> TranslationTable { get; set; }
    }


    

    public class GetExistingWorkRequestMasterIDsResult : CommonWebServiceFields
    {
        public GetExistingWorkRequestMasterIDsResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        
        public List<int> lstL2ID { get; set; }

        
        public List<int> lstWorkTypeID { get; set; }

        
        public List<int> lstWorkPriorityID { get; set; }

        
        public List<int> lstWorkTradeID { get; set; }

        
        public List<int> lstSectorID { get; set; }

        
        public List<int> lstMaintDivisionID { get; set; }

        
        public List<int> lstMaintDeptID { get; set; }

        
        public List<int> lstMaintSubDeptID { get; set; }

    }

    public class GetExistingWorkRequestAssetInfoMasterIDsResult : CommonWebServiceFields
    {
        public GetExistingWorkRequestAssetInfoMasterIDsResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<int> lstEmployeeID { get; set; }
                
        //public List<int> lstAssetID { get; set; }
                
        //public List<int> lstLocationID { get; set; }
                
        public List<int> lstL3ID { get; set; }
                
        public List<int> lstL4ID { get; set; }
                
        public List<int> lstL5ID { get; set; }
                
        public List<int> lstEmployeeStatusId { get; set; }

        public List<int> lstAssetStatusID { get; set; }

        //public List<Employees_L2> Employee_L2Table { get; set; }
                
        //public List<Employees_MaintSubDept> Employees_MaintSubDeptTable { get; set; }

    }

    public class GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult : CommonWebServiceFields
    {
        public GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<int> lstLocationID { get; set; }
    }

    public class GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult : CommonWebServiceFields
    {
        public GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
        public List<int> lstAssetID { get; set; }
    }


    public class GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult : CommonWebServiceFields
    {
        public GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
        
        public List<Employees_L2> Employee_L2Table { get; set; }               

    }




    #region JobOrderList List
    public class GetJobOrderListResult : CommonWebServiceFields
    {
        public GetJobOrderListResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
    
        public List<WorkOrder> WorkOrderTable { get; set; }
            
        public int EmployeeID { get; set; }
                
        public bool IsCentral { get; set; }        
                
        public string WorkOrderNoToRefresh { get; set; }

        //To open JO from JOHistory in Asset        
        public bool FromJoHistory { get; set; }

        //To synch Job-Order List        
        public string SynchTime { get; set; }
    }

    public class _WorkOrder
    {
        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the DateReceived value.
        /// </summary>
        public DateTime? DateReceived { get; set; }

        /// <summary>
        /// Gets or sets the ProblemDescription value.
        /// </summary>        
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Gets or sets the WorkStatusID value.
        /// </summary>
        public int? WorkStatusID { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>        
        public string WorkorderNo { get; set; }

        public string LabourCost { get; set; }
                
        public string ManHours { get; set; }
                
        public string ItemCost { get; set; }
               
        public string DICost { get; set; }



    }

    #endregion

    #region Asset

    public class GetAssetListResult : CommonWebServiceFields
    {
        public GetAssetListResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<_Asset> AssetList { get; set; }
                
        public int EmployeeID { get; set; }
                
        public bool IsCentral { get; set; }
                
        public int? AssetIdToRefresh { get; set; }
                
        public int JobOrderHistoryCount { get; set; }

        //To synch Asset List        
        public string SynchTime { get; set; }
    }


    public class _Asset 
    {        
        public string AltAssetCategory { get; set; }
        public string AltAssetClass { get; set; }
        public string AltAssetCondition { get; set; }
        public string AltAssetStatusDesc { get; set; }
        public string AltAssetSubCategory { get; set; }
        public string AltCriticality { get; set; }
        public string AltSupplierName { get; set; }
        public string AltWarranty_contract { get; set; }
        public string AltWorkTrade { get; set; }
        public string AssetAltDescription { get; set; }
        public string AssetCatCode { get; set; }
        public string AssetCategory { get; set; }
        public string AssetClass { get; set; }
        public string AssetClassCode { get; set; }
        public string AssetConditionCode { get; set; }
        public string AssetDescription { get; set; }
        public int AssetID { get; set; }
        public string AssetNotes { get; set; }
        public string AssetNumber { get; set; }
        public string AssetStatusDesc { get; set; }
        public string AssetSubCatCode { get; set; }
        public string AssetSubCategory { get; set; }
        public DateTime? CommissionedDate { get; set; }
        public string Criticality { get; set; }
        public DateTime? DataDisposed { get; set; }
        public string EmployeeAltName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeNO { get; set; }
        public double? EstLife { get; set; }
        public string L2AltName { get; set; }
        public string L2Code { get; set; }
        public string L2Name { get; set; }
        public string L3AltDesc { get; set; }
        public string L3Desc { get; set; }
        public string L3No { get; set; }
        public string L4AltDescription { get; set; }
        public string L4Description { get; set; }
        public string L4No { get; set; }
        public string L5AltDescription { get; set; }
        public string L5Description { get; set; }
        public string L5No { get; set; }
        public string LocationAltDescription { get; set; }
        public string LocationDescription { get; set; }
        public string LocationNo { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNumber { get; set; }
        public string NotesToTech { get; set; }
        public int? PartsListID { get; set; }
        public double? PurchasePrice { get; set; }
        public string SectorAltName { get; set; }
        public string SectorCode { get; set; }
        public string SectorName { get; set; }
        public string SerialNumber { get; set; }
        public string SupplierName { get; set; }
        public string SupplierNo { get; set; }
        public DateTime? Warranty_ContractExpiry { get; set; }
        public string Warranty_ContractNotes { get; set; }
        public string Warranty_contract { get; set; }
        public string WorkTrade { get; set; }
        public string AssetCondition { get; set; }        
        public int? AssetStatusID { get; set; }
        public int? LocationID { get; set; }
        public int? EmployeeID { get; set; }

        public List<PartsListDetail> PartslistdetailsTable { get; set; }
        public List<AssetRelationship> AssetrelationshipTable { get; set; }
        public List<Reading> PMMeterHistory { get; set; }
        public List<PmMeter> PMMeterSchedule { get; set; }
        public List<_WorkOrder> JoHistory { get; set; }
        public List<ExtAssetFile> ExtassetfileTable { get; set; }
        public List<AssetSpecification> SpecificationTable { get; set; }

    }


    #endregion


    #region WorkRequest List, Add/Update , Change Status
        
    public class GetWorkRequestListResult : CommonWebServiceFields
    {
        public GetWorkRequestListResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<Worequest> WorkRequestTable { get; set; }

        public string RequestStatusIds { get; set; }

        public int EmployeeID { get; set; }

        public bool IsCentral { get; set; }                
       
        public string RequestNoToRefresh { get; set; }

        //To synch Work-Request List        
        public string SynchTime { get; set; }

        /// <summary>
        /// Comma separated Site Ids from Mobile
        /// </summary>        
        public List<int> lstMobileSiteIDs { get; set; }

    }

    public class AddUpdateWorkRequestResult : CommonWebServiceFields
    {
        public AddUpdateWorkRequestResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public Worequest WorkRequestTable { get; set; }
                
        public int EmployeeID { get; set; }
    }


    public class WorkOrderUploadSignatureResult : CommonWebServiceFields
    {
        public WorkOrderUploadSignatureResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public string WorkorderNo { get; set; }

        public byte[] ImageRaw { get; set; }

        public string UserSignatureFileName { get; set; }

        public string NewFilePath { get; set; }
    }


    public class ChangeWorkRequestStatusResult : CommonWebServiceFields
    {
        public ChangeWorkRequestStatusResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public string RequestNo { get; set; }
                
        public int EmployeeID { get; set; }
                
        public int RequestStatusId { get; set; }
                
        public string Remarks { get; set; }
    }


    #endregion

    #region AddFileAttachment
       
    public class AddAttachmentResult : CommonWebServiceFields
    {
        public AddAttachmentResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<ExtAssetFile> ExtassetfileTable { get; set; }

        /// <summary>
        /// This is to check wheather User is Active or Not
        /// </summary>        
        public int EmployeeID { get; set; }
    }

    #endregion

    #region DeleteFileAttachment

    public class DeleteAttachmentResult : CommonWebServiceFields
    {
        public DeleteAttachmentResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        [DataMember]
        public List<ExtAssetFile> ExtassetfileTable { get; set; }

        /// <summary>
        /// This is to check wheather User is Active or Not
        /// </summary>
        [DataMember]
        public int EmployeeID { get; set; }
    }

    #endregion


    #region Master Result
            
    public class GetWorkOrderMasterResult : CommonWebServiceFields
    {
        public GetWorkOrderMasterResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<Accountcode> AccountcodeTable { get; set; }
                
        public List<CostCenter> CostCenterTable { get; set; }
                
        public List<Failurecause> FailurecauseTable { get; set; }
                
        public List<Rating> RatingTable { get; set; }
                
        //public List<StockCode> StockcodeTable { get; set; }
                
        public List<Substore> SubstoreTable { get; set; }

        public List<Type_pm_gen> Type_Pm_GensTable { get; set; }

        public List<Usergroup> UserGroupTable { get; set; }
                
        //public List<PMCheckList> pmchecklistTable { get; set; }

        //public List<Checklistelement> checklistelementsTable { get; set; }
                
        public List<ChecklistInv> checklistInvTable { get; set; }
                
        public List<ChecklistPPE> ChecklistPPETable { get; set; }
                
        //public List<ChecklistTool> ChecklistToolsTable { get; set; }

        //public int count { get; set; }
    }

    public class GetWorkOrderMasterStockcodeTableResult : CommonWebServiceFields
    {
        public GetWorkOrderMasterStockcodeTableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<StockCode> StockcodeTable { get; set; }

    }


    public class GetWorkOrderMasterChecklistelementsTableResult : CommonWebServiceFields
    {
        public GetWorkOrderMasterChecklistelementsTableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }      

        public List<Checklistelement> checklistelementsTable { get; set; }               
        
    }

    public class GetWorkOrderMasterChecklistToolsTableResult : CommonWebServiceFields
    {
        public GetWorkOrderMasterChecklistToolsTableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<ChecklistTool> ChecklistToolsTable { get; set; }

    }

    public class GetWorkOrderMasterPMChecklistTableResult : CommonWebServiceFields
    {
        public GetWorkOrderMasterPMChecklistTableResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<PMCheckList> pmchecklistTable { get; set; }

    }




    public class GetExistingWorkOrderMasterIDsResult : CommonWebServiceFields
    {
        public GetExistingWorkOrderMasterIDsResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
            
        public List<int> lstFailureCauseID { get; set; }
                
        public List<int> lstRateID { get; set; }
                
        public List<int> lstCostCenterId { get; set; }
                
        //public List<int> lstStockID { get; set; }
                
        public List<int> lstAccCodeid { get; set; }
                
        public List<int> lstSubStoreID { get; set; }
                
        public List<int> lstType_Pm_GensID { get; set; }
                
        public List<int> lstUserGroupId { get; set; }

    }

    public class GetExistingWorkOrderMasterIDsLstStockIDResult : CommonWebServiceFields
    {
        public GetExistingWorkOrderMasterIDsLstStockIDResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
        
        public List<int> lstStockID { get; set; }
    }


    #endregion

    #region Permissions

    public class PermissionMappingResult : CommonWebServiceFields
    {
        public PermissionMappingResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<Permission> PermissionTable { get; set; }
                
        public Nullable<int> EmployeeId { get; set; }
                
        public Nullable<int> UserGroupId { get; set; }
    }

    #endregion

    #region Project No Masters

    public class GetProjectNoMasterResult : CommonWebServiceFields
    {
        public GetProjectNoMasterResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
            
        public List<CmApprovedProjectRequest> cmApprovedProjectRequestTable { get; set; }
                
        public List<Cmprojectrequest> cmprojectrequestTable { get; set; }

        public List<Cmprojectsite> cmprojectsiteTable { get; set; }

        public List<CmProjectStatu> cmProjectStatusTable { get; set; }

        //public List<CmProjectPerformanceEvaluation> cmProjPerformanceEvalutionTable { get; set; }
    }

    public class GetProjectNoMasterCMProjectPerformanceEvaluationResult : CommonWebServiceFields
    {
        public GetProjectNoMasterCMProjectPerformanceEvaluationResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }        
        
        public List<CmProjectPerformanceEvaluation> cmProjPerformanceEvalutionTable { get; set; }
    }
        
    public class GetExistingProjectNoMasterIdsResult : CommonWebServiceFields
    {
        public GetExistingProjectNoMasterIdsResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
                
        public List<string> lstProjectNumber { get; set; }
        
        public List<string> lstProjectReqNo { get; set; }
        
        public List<int> lstcmprojectsiteIds { get; set; }
        
        public List<int> lstProjectStatusIds { get; set; }
        
        //public List<int> lstcmProjectPerformanceEvaluationIds { get; set; }
    }

    public class GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult : CommonWebServiceFields
    {
        public GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }

        public List<int> lstcmProjectPerformanceEvaluationIds { get; set; }
    }

    #endregion

    #region Keep Last Heart beat updated

    public class KeepLastHeartBeatUpdated
    {    
        public int employeeId { get; set; }
     
        public int HeartbeatUpdated { get; set; }
     
        public DateTime? LastConfigChangeDateTime { get; set; }
    }

    #endregion

    #region Generate Work Order from Work Request
       
    public class GenerateWorkOrderResult : CommonWebServiceFields
    {
        public GenerateWorkOrderResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
            
        public Worequest WorkRequestResult { get; set; }

        public GenerateWorkOrderModel WorkOrderResult { get; set; }
    }

    public class GenerateWorkOrderModel
    {
        
        public string WorkorderNo { get; set; }
        
        public Nullable<int> RequestorID { get; set; }
        
        public Nullable<int> EmployeeID { get; set; }
        
        public Nullable<int> L2ID { get; set; }
        
        public Nullable<int> L3ID { get; set; }
        
        public Nullable<int> L4ID { get; set; }
        
        public Nullable<int> L5ID { get; set; }
        
        public Nullable<int> LocationID { get; set; }
        
        public Nullable<int> AssetID { get; set; }
        
        public Nullable<int> WorkStatusID { get; set; }
        
        public Nullable<int> WorkPriorityID { get; set; }
        
        public Nullable<int> FailureCauseID { get; set; }
        
        public Nullable<int> WorkTypeID { get; set; }
        
        public Nullable<int> WOTradeID { get; set; }
        
        public Nullable<int> MeterID { get; set; }
        
        public Nullable<int> PMID { get; set; }
        
        public Nullable<int> MaintDivisionID { get; set; }
        
        public Nullable<int> maintdeptID { get; set; }
        
        public Nullable<int> maintsubdeptID { get; set; }
        
        public Nullable<int> groupID { get; set; }
        
        public Nullable<int> SupplierId { get; set; }
        
        public Nullable<int> AcceptedbyID { get; set; }
        
        public Nullable<int> PMChecklistID { get; set; }
        
        public Nullable<int> RatingsID { get; set; }
        
        public Nullable<int> WOclosebyID { get; set; }
        
        public Nullable<int> CostCenterId { get; set; }
        
        public string ProblemDescription { get; set; }
        
        public Nullable<System.DateTime> DateReceived { get; set; }
        
        public Nullable<System.DateTime> EstDateStart { get; set; }
        
        public Nullable<System.DateTime> EstDateEnd { get; set; }
        
        public Nullable<System.DateTime> ActDateStart { get; set; }
        
        public Nullable<System.DateTime> ActDateEnd { get; set; }
        
        public Nullable<System.DateTime> DateRequired { get; set; }
        
        public Nullable<System.DateTime> DateHandover { get; set; }
        
        public Nullable<double> EstDuration { get; set; }
        
        public string ActionTaken { get; set; }
        
        public string CauseDescription { get; set; }
        
        public string PreventionTaken { get; set; }
        
        public Nullable<double> WOCost { get; set; }
        
        public Nullable<System.DateTime> PMTarStartDate { get; set; }
        
        public Nullable<System.DateTime> PMTarCompDate { get; set; }
        
        public Nullable<System.DateTime> AstartDate { get; set; }
        
        public Nullable<System.DateTime> AEndDate { get; set; }
        
        public Nullable<double> WOLaborCost { get; set; }
        
        public Nullable<double> WODICost { get; set; }
        
        public Nullable<double> WOPartCost { get; set; }
        
        public string WOPMtype { get; set; }
        
        public Nullable<double> PMJOGeneratedReading { get; set; }
        
        public Nullable<double> PMMetReading { get; set; }
        
        public Nullable<double> DownTime { get; set; }
        
        public Nullable<double> LineDowntime { get; set; }
        
        public Nullable<int> MTask { get; set; }
        
        public string Notes { get; set; }
        
        public Nullable<long> MPMID { get; set; }
        
        public Nullable<System.DateTime> print_date { get; set; }
        
        public string AssignTo { get; set; }
        
        public string TelNo { get; set; }
        
        public Nullable<int> accountcodeid { get; set; }
        
        public Nullable<int> Documentstatusid { get; set; }
        
        public string CreatedBy { get; set; }
        
        public Nullable<System.DateTime> CreatedDate { get; set; }
        
        public string ModifiedBy { get; set; }
        
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        
        public Nullable<int> AuthorisedEmployeeId { get; set; }
        
        public Nullable<int> CIGID { get; set; }
        
        public bool IsCleaningModule { get; set; }
        
        public string ProjectNumber { get; set; }
    }

    #endregion

    #region Add/Update/Delete Job-Order
        
    public class AddUpdateWorkOrderResult : CommonWebServiceFields
    {
        public AddUpdateWorkOrderResult()
        {
            CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
        }
            
        public WorkOrder WorkOrderTable { get; set; }
    }
    #endregion

}
