﻿//-----------------------------------------------------------------------
// <copyright file="Translation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - translations
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("translations")]
    public sealed class Translation : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the TranslationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TranslationID { get; set; }

        /// <summary>
        /// Gets or sets the Labelname value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Labelname { get; set; }

        /// <summary>
        /// Gets or sets the FromLanguageCode value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string FromLanguageCode { get; set; }

        /// <summary>
        /// Gets or sets the FromWord value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string FromWord { get; set; }

        /// <summary>
        /// Gets or sets the ToLanguageCode value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string ToLanguageCode { get; set; }

        /// <summary>
        /// Gets or sets the ModuleName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ModuleName { get; set; }

        /// <summary>
        /// Gets or sets the ToWord value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string ToWord { get; set; }

        /// <summary>
        /// Gets or sets the Type value.
        /// </summary>
        [StringLength(2, ErrorMessage = "*")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
