//-----------------------------------------------------------------------
// <copyright file="MainenanceDivision.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
	
    /// <summary>
	/// This class is used to Define Model for Table - MainenanceDivision
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("MainenanceDivision")]
	public sealed class MainenanceDivision : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the MaintDivisionID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int MaintDivisionID { get; set; }

		/// <summary>
		/// Gets or sets the MaintDivisionName value.
		/// </summary>
		[StringLength(250, ErrorMessage = "*")]
		public string MaintDivisionName { get; set; }

		/// <summary>
		/// Gets or sets the MaintDivisionCode value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string MaintDivisionCode { get; set; }

		/// <summary>
		/// Gets or sets the MaintDivisionAltName value.
		/// </summary>
		[StringLength(250, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string MaintDivisionAltName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the ProjectType value.
		/// </summary>
		public int? ProjectType { get; set; }

        [NotMapped]
        public bool IsChecked { get; set; }

		#endregion
	}
}
