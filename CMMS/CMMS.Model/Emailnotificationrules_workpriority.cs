//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_workpriority.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_workpriority
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_workpriority")]
	public sealed class Emailnotificationrules_workpriority : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleWorkPriorityID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleWorkPriorityID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the WorkPriorityID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int? WorkPriorityID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string WorkPriority { get; set; }
        [NotMapped]
        public string AltWorkPriority { get; set; }
		#endregion
	}
}
