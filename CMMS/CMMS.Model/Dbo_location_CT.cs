//-----------------------------------------------------------------------
// <copyright file="Dbo_location_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_location_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_location_CT")]
	public sealed class Dbo_location_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationAltDescription { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string L3No { get; set; }

        [NotMapped]
        public string L4No { get; set; }

        [NotMapped]
        public string L5No { get; set; }

         [NotMapped]
        public string Criticality { get; set; }


         /// <summary>
         /// Gets or sets the NotetoTech value.
         /// </summary>
         [StringLength(4000, ErrorMessage = "*")]
         public string NotetoTech { get; set; }


         /// <summary>
         /// Gets or sets the __$operation value.
         /// </summary>
         [Required(ErrorMessage = "*")]
         public int operation { get; set; }

         /// <summary>
         /// Gets or sets the ModifiedBy value.
         /// </summary>
         [StringLength(100, ErrorMessage = "*")]
         public string ModifiedBy { get; set; }

         /// <summary>
         /// Gets or sets the ModifiedDate value.
         /// </summary>
         public string ModifiedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L3ID value.
        ///// </summary>
        //public int? L3ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L4ID value.
        ///// </summary>
        //public int? L4ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L5ID value.
        ///// </summary>
        //public int? L5ID { get; set; }

        ///// <summary>
        ///// Gets or sets the CriticalityID value.
        ///// </summary>
        //public int? CriticalityID { get; set; }

		

		

		

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the CleaningClassificationId value.
        ///// </summary>
        //public int? CleaningClassificationId { get; set; }

		#endregion
	}
}
