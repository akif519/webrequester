﻿//-----------------------------------------------------------------------
// <copyright file="Stockcode.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - stockcode
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("stockcode")]
    public sealed class StockCode : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int StockID { get; set; }

        /// <summary>
        /// Gets or sets the CriticalityID value.
        /// </summary>
        public bool? CriticalityID { get; set; }

        /// <summary>
        /// Gets or sets the User Image Path.
        /// </summary>
        public string ImagePath { get; set; }

        [NotMapped]
        public string OldImagePath { get; set; }

        [NotMapped]
        public bool isCritical
        {
            get
            {
                if (this.CriticalityID.HasValue)
                {
                    return Convert.ToBoolean(this.CriticalityID);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.CriticalityID = Convert.ToBoolean(value);
            }
        }

        /// <summary>
        /// Gets or sets the AttrGroupID value.
        /// </summary>
        public int? AttrGroupID { get; set; }

        /// <summary>
        /// Gets or sets the AttrSubID value.
        /// </summary>
        public int? AttrSubID { get; set; }

        /// <summary>
        /// Gets or sets the Default_supplier_id value.
        /// </summary>
        [AllowNullForSave]
        public int? Default_supplier_id { get; set; }

        /// <summary>
        /// Gets or sets the SupplierNo
        /// </summary>
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the SupplierName
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AltSupplierName
        /// </summary>
        [NotMapped]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [Required(ErrorMessage = "Stock No cannot be blank.")]
        [StringLength(255, ErrorMessage = "*")]
        public string StockNo { get; set; }

        [NotMapped]
        public string OldStockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [Required(ErrorMessage = "Stock Description cannot be blank.")]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the UOM value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UOM { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the StatusText value.
        /// </summary>
        [NotMapped]
        public string StatusText { get; set; }

        /// <summary>
        /// Gets or sets the Price2 value.
        /// </summary>
        [Range(typeof(decimal), "0", "99999999", ErrorMessage = "{0} can only be between {1} and {2}")]
        [AllowNullForSave]
        public decimal? Price2 { get; set; }

        /// <summary>
        /// Gets or sets the Notes value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the LeadTime value.
        /// </summary>
        [Range(typeof(int), "0", "9999", ErrorMessage = "{0} can only be between {1} and {2}")]
        public int? LeadTime { get; set; }

        /// <summary>
        /// Gets or sets the AvgPrice value.
        /// </summary>
        public decimal? AvgPrice { get; set; }

        /// <summary>
        /// Gets or sets the Specification value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Specification { get; set; }

        /// <summary>
        /// Gets or sets the Manufacturer value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the ANV value.
        /// </summary>
        public bool? ANV { get; set; }

        [NotMapped]
        public bool allowNegativeBalance
        {
            get
            {
                if (this.ANV.HasValue)
                {
                    return Convert.ToBoolean(this.ANV);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.ANV = Convert.ToBoolean(value);
            }
        }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the name of the attribute.
        /// </summary>
        /// <value>
        /// The name of the attribute.
        /// </value>
        [NotMapped]
        public string AttrName { get; set; }

        /// <summary>
        /// Gets or sets the name of the attribute sub.
        /// </summary>
        /// <value>
        /// The name of the attribute sub.
        /// </value>
        [NotMapped]
        public string AttrSubName { get; set; }

        /// <summary>
        /// Gets or sets the criticality.
        /// </summary>
        /// <value>
        /// The criticality.
        /// </value>
        [NotMapped]
        public string Criticality { get; set; }

        /// <summary>
        /// Gets or sets the Balance.
        /// </summary>
        /// <value>
        /// The Balance.
        /// </value>
        [NotMapped]
        public decimal? Balanace { get; set; }

        [NotMapped]
        public int? SubStoreID { get; set; }

        [NotMapped]
        public string SubStoreDesc { get; set; }

        [NotMapped]
        public string AltSubStoreDesc { get; set; }

        [NotMapped]
        public decimal? Max_level { get; set; }

        [NotMapped]
        public decimal? Re_order_level { get; set; }

        [NotMapped]
        public decimal? Min_level { get; set; }

        [NotMapped]
        public decimal? Reorder_qty { get; set; }

        [NotMapped]
        public int? L2ID { get; set; }
        #endregion
    }
}
