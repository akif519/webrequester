﻿//-----------------------------------------------------------------------
// <copyright file="Purchase_order_auth.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - purchase_order_auth
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("purchase_order_auth")]
    public sealed class Purchase_order_auth : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PO_Auth_ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PO_Auth_ID { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseOrderID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PurchaseOrderID { get; set; }

        /// <summary>
        /// Gets or sets the AuthorisedBy value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int AuthorisedBy { get; set; }

        /// <summary>
        /// Gets or sets the ApprovalDate value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public DateTime ApprovalDate { get; set; }

        [NotMapped]
        public string EmployeeNO { get; set; }

        [NotMapped]
        public string Name { get; set; }
        #endregion
    }
}
