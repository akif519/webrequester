//-----------------------------------------------------------------------
// <copyright file="Language.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - Language
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>20-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("Language")]
	public sealed class Language : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the LanguageID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int LanguageID { get; set; }

		/// <summary>
		/// Gets or sets the LanguageCode value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(10, ErrorMessage = "*")]
		public string LanguageCode { get; set; }

		/// <summary>
		/// Gets or sets the Language name value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string LanguageName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
