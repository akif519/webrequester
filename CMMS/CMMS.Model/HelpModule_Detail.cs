//-----------------------------------------------------------------------
// <copyright file="HelpModule_Detail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;
    using System.Web.Mvc;

	/// <summary>
	/// This class is used to Define Model for Table - HelpModule_Detail
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>20-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("HelpModule_Detail")]
	public sealed class HelpModule_Detail : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the HelpModuelDetailID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int HelpModuelDetailID { get; set; }

		/// <summary>
		/// Gets or sets the LanguageID value.
		/// </summary>
		public int? LanguageID { get; set; }

		/// <summary>
		/// Gets or sets the HelpTitle value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string HelpTitle { get; set; }

		/// <summary>
		/// Gets or sets the HelpDescription value.
		/// </summary>		
        [AllowHtml]
		public string HelpDescription { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the HelpModuleID value.
		/// </summary>
		public int? HelpModuleID { get; set; }

        /// <summary>
        /// Gets or sets the name of the module.
        /// </summary>
        /// <value>
        /// The name of the module.
        /// </value>
        [NotMapped]
        public string ModuleName { get; set; }

        /// <summary>
        /// Gets or sets the name of the page.
        /// </summary>
        /// <value>
        /// The name of the page.
        /// </value>
        [NotMapped]
        public string PageName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [has help description].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has help description]; otherwise, <c>false</c>.
        /// </value>
        [NotMapped]
        public bool HasHelpDescription { get; set; }

        [NotMapped]
        public bool isBookmarked { get; set; }

        [NotMapped]
        public IList<HelpChapter> HelpChapter { get; set; }

        [NotMapped]
        public IList<CustomHelpRelatedItem> CustomHelpRelatedItem { get; set; }

        [NotMapped]
        public int? PrevHelpModuelDetailID { get; set; }

        [NotMapped]
        public int? NextHelpModuelDetailID { get; set; }

		#endregion
	}
}
