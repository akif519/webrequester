﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// Item And Labour Details
    /// </summary>
    public partial class ItemAndLabourDetails
    {
        /// <summary>
        /// Gets or sets the parts no.
        /// </summary>
        /// <value>
        /// The parts no.
        /// </value>
        public string PartsNo { get; set; }
        /// <summary>
        /// Gets or sets the stock desc.
        /// </summary>
        /// <value>
        /// The stock desc.
        /// </value>
        public string StockDesc { get; set; }
        /// <summary>
        /// Gets or sets the alt stock desc.
        /// </summary>
        /// <value>
        /// The alt stock desc.
        /// </value>
        public string AltStockDesc { get; set; }
        /// <summary>
        /// Gets or sets the type of the issue.
        /// </summary>
        /// <value>
        /// The type of the issue.
        /// </value>
        public string IssueType { get; set; }
        /// <summary>
        /// Gets or sets the uom.
        /// </summary>
        /// <value>
        /// The uom.
        /// </value>
        public string UOM { get; set; }
        /// <summary>
        /// Gets or sets the qty.
        /// </summary>
        /// <value>
        /// The qty.
        /// </value>
        public decimal? Qty { get; set; }
    }
}
