﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - employeeStatuses
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("employeeStatuses")]
    public sealed class EmployeeStatuse : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the EmployeeStatusId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeStatusId { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeStatus value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string EmployeeStatus { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeAltStatus value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string EmployeeAltStatus { get; set; }

        /// <summary>
        /// Gets or sets the DisplayInModule value.
        /// </summary>
        public bool? DisplayInModule { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeStatus value.
        /// </summary>        
        [NotMapped]
        [StringLength(100, ErrorMessage = "*")]
        public string EmployeeStatus1 { get; set; }

        #endregion
    }
}
