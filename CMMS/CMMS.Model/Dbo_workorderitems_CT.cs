//-----------------------------------------------------------------------
// <copyright file="Dbo_workorderitems_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_workorderitems_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_workorderitems_CT")]
    public sealed class Dbo_workorderitems_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

       
        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        public int? StockID { get; set; }

        /// <summary>
        /// Gets or sets the WorkOrderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkOrderNo { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string StockDescription { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the SeqNo value.
        /// </summary>
        public int? SeqNo { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public float? Qty { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        #endregion
    }
}
