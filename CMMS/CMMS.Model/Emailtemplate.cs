//-----------------------------------------------------------------------
// <copyright file="Emailtemplate.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailtemplate
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailtemplate")]
	public sealed class Emailtemplate : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the EmailTemplateID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int EmailTemplateID { get; set; }

		/// <summary>
		/// Gets or sets the EmailTemplateName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string EmailTemplateName { get; set; }

		/// <summary>
		/// Gets or sets the EmailSubject value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string EmailSubject { get; set; }

		/// <summary>
		/// Gets or sets the EmailBody value.
		/// </summary>
		public string EmailBody { get; set; }

        /// <summary>
        /// Gets or sets the EmailBody value.
        /// </summary>
        public string SMSBody { get; set; }

		/// <summary>
		/// Gets or sets the EmailFieldType value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string EmailFieldType { get; set; }

		/// <summary>
		/// Gets or sets the LanguageRef value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string LanguageRef { get; set; }

		/// <summary>
		/// Gets or sets the WOWRFieldList value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string WOWRFieldList { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the IsAppliedToSMS value.
        /// </summary>
        public bool IsAppliedToSMS { get; set; }

        /// <summary>
        /// Gets or sets the IsAppliedToEmail value.
        /// </summary>
        public bool IsAppliedToEmail { get; set; }
		#endregion
	}
}
