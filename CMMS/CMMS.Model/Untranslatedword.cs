//-----------------------------------------------------------------------
// <copyright file="Untranslatedword.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - untranslatedwords
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("untranslatedwords")]
	public sealed class Untranslatedword : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the UntranslatedwordID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int UntranslatedwordID { get; set; }

		/// <summary>
		/// Gets or sets the Labelname value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Labelname { get; set; }

		/// <summary>
		/// Gets or sets the LanguageCode value.
		/// </summary>
		[StringLength(10, ErrorMessage = "*")]
		public string LanguageCode { get; set; }

		/// <summary>
		/// Gets or sets the Type value.
		/// </summary>
		[StringLength(2, ErrorMessage = "*")]
		public string Type { get; set; }

		/// <summary>
		/// Gets or sets the Word value.
		/// </summary>
		[StringLength(1000, ErrorMessage = "*")]
		public string Word { get; set; }

		/// <summary>
		/// Gets or sets the ModuleName value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ModuleName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

        [NotMapped]
        public string ToWord { get; set; }

		#endregion
	}
}
