﻿//-----------------------------------------------------------------------
// <copyright file="Assetstransfer.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - assetstransfer
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("assetstransfer")]
    public sealed class AssetsTransfer : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the TransferId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TransferId { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the ParentAssetID value.
        /// </summary>
        public int? ParentAssetID { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the TransferDate value.
        /// </summary>
        public DateTime? TransferDate { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the location description.
        /// </summary>
        /// <value>
        /// The location description.
        /// </value>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the location alt description.
        /// </summary>
        /// <value>
        /// The location alt description.
        /// </value>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2.
        /// </summary>
        /// <value>
        /// The name of the l2.
        /// </value>
        [NotMapped]
        public string L2Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2 alt.
        /// </summary>
        /// <value>
        /// The name of the l2 alt.
        /// </value>
        [NotMapped]
        public string L2AltName { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [NotMapped]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [NotMapped]
        public string AssetAltDescription { get; set; }

        #endregion
    }
}
