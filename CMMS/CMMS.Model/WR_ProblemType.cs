﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - WR_ProblemType By: Akif519
    /// </summary>
    /// <CreatedBy>Mohammad Aqeef</CreatedBy>
    /// <CreatedDate>14-Mar-2018</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WR_ProblemType")]
    public sealed class WR_ProblemType: BaseModel
    {
    #region Properties
        // Gets or sets the ProblemTypeID value.
        public int? ProblemTypeId { get; set; }
        [StringLength(50, ErrorMessage="*")]
        public string ProblemTypeCode { get; set; }

        [StringLength(1500,ErrorMessage="*")]
        public string ProblemTypeDesc { get; set; }

        [StringLength(1500,ErrorMessage="*")]
        public string ProblemTypeDescAr { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    #endregion
    }
}
