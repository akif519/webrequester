﻿//-----------------------------------------------------------------------
// <copyright file="Employees_L2.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;    

    /// <summary>
    /// This class is used to Define Model for Table - employees_L2
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("employees_L2")]
    public sealed class Employees_L2 : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>        
        [Required(ErrorMessage = "*")]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the L2Altname value.
        /// </summary>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public bool ChkL2 { get; set; }

        /// <summary>
        /// Gets or sets the IsInserted value. If Value is to be inserted then 
        /// it will be true or value is to be deleted then it will be false
        /// </summary>
        [NotMapped]
        public bool IsInserted { get; set; }

        /// <summary>
        /// Gets or sets the EmpID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmpID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
