//-----------------------------------------------------------------------
// <copyright file="Dbo_PurchaseOrder_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_PurchaseOrder_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_PurchaseOrder_CT")]
    public sealed class Dbo_PurchaseOrder_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the ID value.
        /// </summary>
        //public int? ID { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseOrderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PurchaseOrderNo { get; set; }      

        /// <summary>
        /// Gets or sets the OrderedDate value.
        /// </summary>
        public DateTime? OrderedDate { get; set; }

        /// <summary>
        /// Gets or sets the ExpDeliveryDate value.
        /// </summary>
        public DateTime? ExpDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the OrderStatus value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets the DeliveryStatus value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string DeliveryStatus { get; set; }

        /// <summary>
        /// Gets or sets the OrderTotal value.
        /// </summary>
        public decimal? OrderTotal { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the l2 altname.
        /// </summary>
        /// <value>
        /// The l2 altname.
        /// </value>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the Maintainance dept code value.
        /// </summary>
        [NotMapped]
        public string MaintDeptCode { get; set; }        

        /// <summary>
        /// Gets or sets the name of the supplier.
        /// </summary>
        /// <value>
        /// The name of the supplier.
        /// </value>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt supplier.
        /// </summary>
        /// <value>
        /// The name of the alt supplier.
        /// </value>
        [NotMapped]
        public string AltSupplierName { get; set; }


        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        #endregion
    }
}
