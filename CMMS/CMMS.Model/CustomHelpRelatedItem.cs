﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class CustomHelpRelatedItem
    {
        public int RelatedTopicID { get; set; }
        public int ParentHelpModuleDetailID { get; set; }
        public int ChildHelpModuleDetailID { get; set; }
        public string RelatedHelpTitle { get; set; }
    }
}
