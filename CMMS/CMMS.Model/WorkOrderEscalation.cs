﻿//-----------------------------------------------------------------------
// <copyright file="WorkOrderEscalation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - WorkOrderEscalation
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>09-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkOrderEscalation")]
    public sealed class WorkOrderEscalation : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkOrderEscalationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkOrderEscalationID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the WorkOrderNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WorkOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the EscalationTimeInMin value.
        /// </summary>
        public int? EscalationTimeInMin { get; set; }

        /// <summary>
        /// Gets or sets the EscalationLevel value.
        /// </summary>
        public int? EscalationLevel { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
