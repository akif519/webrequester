//-----------------------------------------------------------------------
// <copyright file="Dbo_pmchecklist_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_pmchecklist_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_pmchecklist_CT")]
    public sealed class Dbo_pmchecklist_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }


        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        [NotMapped]
        public string SafetyNo { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string CheckListName { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string AltCheckListName { get; set; }

        /// <summary>
        /// Gets or sets the EstimatedLaborHours value.
        /// </summary>
        public decimal? EstimatedLaborHours { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        ///// <summary>
        ///// Gets or sets the ChecklistID value.
        ///// </summary>
        //public int? ChecklistID { get; set; }

        ///// <summary>
        ///// Gets or sets the SafetyID value.
        ///// </summary>
        //public int? SafetyID { get; set; }

        ///// <summary>
        ///// Gets or sets the FileLink value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string FileLink { get; set; }

        ///// <summary>
        ///// Gets or sets the L2Id value.
        ///// </summary>
        //public int? L2Id { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdeptId value.
        ///// </summary>
        //public int? MaintdeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MainSubDeptId value.
        ///// </summary>
        //public int? MainSubDeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdivId value.
        ///// </summary>
        //public int? MaintdivId { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the IsCleaningModule value.
        ///// </summary>
        //public bool? IsCleaningModule { get; set; }

        #endregion
    }
}
