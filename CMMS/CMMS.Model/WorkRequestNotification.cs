﻿//-----------------------------------------------------------------------
// <copyright file="WorkRequestNotification.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - WorkRequestNotification
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>10-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkRequestNotification")]
    public sealed class WorkRequestNotification : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkRequestNotificationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRequestNotificationID { get; set; }

        /// <summary>
        /// Gets or sets the RequestNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string RequestNo { get; set; }

        /// <summary>
        /// Gets or sets the NotificationTypeID value.
        /// </summary>
        public int? NotificationTypeID { get; set; }

        /// <summary>
        /// Gets or sets the RelevantTypeID value.
        /// </summary>
        public int? RelevantTypeID { get; set; }

        /// <summary>
        /// Gets or sets the Title value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsActive is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
