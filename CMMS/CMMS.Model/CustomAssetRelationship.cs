﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class CustomAssetRelationship : BaseModel
    {
        public int AssetID { get; set; }
        public string AssetNumber { get; set; }
        public string AssetDescription { get; set; }
        public string AssetAltDescription { get; set; }
        public string AltAssetStatusDesc { get; set; }
        public string AssetStatusDesc { get; set; }
        public int AssetStatusID { get; set; }
        public int LocationID { get; set; }

        public int ParentAssetID { get; set; }
        public string ParentAssetNumber { get; set; }
        public string ParentAssetDescription { get; set; }
        public string ParentAssetAltDescription { get; set; }
        public string ParentAltAssetStatusDesc { get; set; }
        public string ParentAssetStatusDesc { get; set; }

        public int ChildId { get; set; }
    }
}
