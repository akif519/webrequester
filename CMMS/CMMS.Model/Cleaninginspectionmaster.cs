//-----------------------------------------------------------------------
// <copyright file="Cleaninginspectionmaster.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cleaninginspectionmaster
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>24-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cleaninginspectionmaster")]
	public sealed class Cleaninginspectionmaster : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CleaningInspectionId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CleaningInspectionId { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int L2ID { get; set; }

		/// <summary>
		/// Gets or sets the ReferenceNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ReferenceNo { get; set; }

		/// <summary>
		/// Gets or sets the DateInspection value.
		/// </summary>
		public DateTime? DateInspection { get; set; }

		/// <summary>
		/// Gets or sets the InspectionBy value.
		/// </summary>
		public int? InspectionBy { get; set; }

		/// <summary>
		/// Gets or sets the Comments value.
		/// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		[StringLength(2000, ErrorMessage = "*")]
		public string Comments { get; set; }

		/// <summary>
		/// Gets or sets the ArabicComments value.
		/// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		[StringLength(2000, ErrorMessage = "*")]
		public string ArabicComments { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string InspectedBy { get; set; }

        [NotMapped]
        public string EmployeeNO { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string AltName { get; set; }

		#endregion
	}
}
