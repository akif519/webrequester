﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class EmailReceiptant
    {
        public int ReceiptantID { get; set; }
        public string ReceiptantNo { get; set; }
        public string ReceiptantName { get; set; }
        public string ReceiptantAltName { get; set; }
        public string ReceiptantEmail { get; set; }
        public string LanguageCode { get; set; }
        public string HandPhone { get; set; }
        public System.Nullable<int> EscalationTimeInMin { get; set; }
        public System.Nullable<System.DateTime> LastEmailSentTime { get; set; }
        public Nullable<bool> IsSelectedCriticalProposal { get; set; }
        public Nullable<bool> IsSelectedNormalProposal { get; set; }
    }
}
