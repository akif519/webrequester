﻿//-----------------------------------------------------------------------
// <copyright file="EmailNotification_Supplier.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - EmailNotification_Supplier
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>04-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("EmailNotification_Supplier")]
    public sealed class EmailNotification_Supplier : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationRuleSupplierID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationRuleSupplierID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the SupplierID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastEmailSentTime value.
        /// </summary>
        public DateTime? LastEmailSentTime { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public string AltSupplierName { get; set; }

        #endregion
    }
}
