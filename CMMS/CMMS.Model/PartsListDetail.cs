﻿//-----------------------------------------------------------------------
// <copyright file="Partslistdetail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - partslistdetails
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("partslistdetails")]
    public sealed class PartsListDetail : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PartsListID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PartsListID { get; set; }

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int StockID { get; set; }

        /// <summary>
        /// Gets or sets the PartsQty value.
        /// </summary>
        public decimal? PartsQty { get; set; }

        /// <summary>
        /// Gets or sets the Comments value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the stock no.
        /// </summary>
        /// <value>
        /// The stock no.
        /// </value>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the stock description.
        /// </summary>
        /// <value>
        /// The stock description.
        /// </value>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the alt stock description.
        /// </summary>
        /// <value>
        /// The alt stock description.
        /// </value>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the HDN parts list identifier.
        /// </summary>
        /// <value>
        /// The HDN parts list identifier.
        /// </value>
        [NotMapped]
        public int? hdnPartsListDetailID { get; set; }

        #endregion
    }
}
