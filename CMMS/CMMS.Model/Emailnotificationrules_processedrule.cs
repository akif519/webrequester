//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_processedrule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_processedrule
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_processedrule")]
	public sealed class Emailnotificationrules_processedrule : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleProcessedRuleID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleProcessedRuleID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
        [AllowNullForSave]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the ReferenceID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(50, ErrorMessage = "*")]
		public string ReferenceID { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the IsMailSent is enabled.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public bool IsMailSent { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

        [NotMapped]
        public Emailnotificationrule emailnotificationrule { get; set; }
        
		#endregion
	}
}
