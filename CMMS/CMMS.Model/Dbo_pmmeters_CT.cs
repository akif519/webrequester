//-----------------------------------------------------------------------
// <copyright file="Dbo_pmmeters_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_pmmeters_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_pmmeters_CT")]
	public sealed class Dbo_pmmeters_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        [NotMapped]
        public string MasterMeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string MeterDescription { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        [NotMapped]
        public string LowChecklistNo { get; set; }

        [NotMapped]
        public string HighChecklistNo { get; set; }

        [NotMapped]
        public string IncChecklistNo { get; set; }
        [NotMapped]
        public string WorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the Lowerlimit value.
        /// </summary>
        public float? Lowerlimit { get; set; }

        /// <summary>
        /// Gets or sets the UpperLimit value.
        /// </summary>
        public float? UpperLimit { get; set; }

        /// <summary>
        /// Gets or sets the LastPMreading value.
        /// </summary>
        public float? LastPMreading { get; set; }

        /// <summary>
        /// Gets or sets the StartMeterReading value.
        /// </summary>
        public float? StartMeterReading { get; set; }

        /// <summary>
        /// Gets or sets the IncFreq value.
        /// </summary>
        public float? IncFreq { get; set; }

        /// <summary>
        /// Gets or sets the IncPMDue value.
        /// </summary>
        public float? IncPMDue { get; set; }


        [NotMapped]
        public string PMTypeName { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }



        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the MeterID value.
        ///// </summary>
        //public int? MeterID { get; set; }

        ///// <summary>
        ///// Gets or sets the MeterMasterID value.
        ///// </summary>
        //public int? MeterMasterID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetID value.
        ///// </summary>
        //public int? AssetID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMLowTaskID value.
        ///// </summary>
        //public int? PMLowTaskID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMUppTaskID value.
        ///// </summary>
        //public int? PMUppTaskID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMIncTaskID value.
        ///// </summary>
        //public int? PMIncTaskID { get; set; }

        ///// <summary>
        ///// Gets or sets the NextPMIncTaskID value.
        ///// </summary>
        //public int? NextPMIncTaskID { get; set; }

        ///// <summary>
        ///// Gets or sets the Workpriorityid value.
        ///// </summary>
        //public int? Workpriorityid { get; set; }

        ///// <summary>
        ///// Gets or sets the Worktradeid value.
        ///// </summary>
        //public int? Worktradeid { get; set; }

        ///// <summary>
        ///// Gets or sets the MeterNo value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string MeterNo { get; set; }

        ///// <summary>
        ///// Gets or sets the MeterDescription value.
        ///// </summary>
        //[StringLength(1000, ErrorMessage = "*")]
        //public string MeterDescription { get; set; }

        ///// <summary>
        ///// Gets or sets the AltMeterDescription value.
        ///// </summary>
        //[StringLength(1000, ErrorMessage = "*")]
        //public string AltMeterDescription { get; set; }

        ///// <summary>
        ///// Gets or sets the MeterType value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string MeterType { get; set; }

		
        ///// <summary>
        ///// Gets or sets the PMLimit value.
        ///// </summary>
        //public int? PMLimit { get; set; }

		

        ///// <summary>
        ///// Gets or sets the Units value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Units { get; set; }

		

		
        ///// <summary>
        ///// Gets or sets the PeriodDays value.
        ///// </summary>
        //public int? PeriodDays { get; set; }

        ///// <summary>
        ///// Gets or sets the FreqUnits value.
        ///// </summary>
        //public int? FreqUnits { get; set; }

        ///// <summary>
        ///// Gets or sets the PMType value.
        ///// </summary>
        //public int? PMType { get; set; }

        ///// <summary>
        ///// Gets or sets the Frequency value.
        ///// </summary>
        //public int? Frequency { get; set; }

        ///// <summary>
        ///// Gets or sets the StartDate value.
        ///// </summary>
        //public DateTime? StartDate { get; set; }

        ///// <summary>
        ///// Gets or sets the TargetStartDate value.
        ///// </summary>
        //public DateTime? TargetStartDate { get; set; }

        ///// <summary>
        ///// Gets or sets the TargetCompDate value.
        ///// </summary>
        //public DateTime? TargetCompDate { get; set; }

        ///// <summary>
        ///// Gets or sets the NextDate value.
        ///// </summary>
        //public DateTime? NextDate { get; set; }

        ///// <summary>
        ///// Gets or sets the PMCounter value.
        ///// </summary>
        //public int? PMCounter { get; set; }

		

        ///// <summary>
        ///// Gets or sets the TimeBased value.
        ///// </summary>
        //public int? TimeBased { get; set; }

        ///// <summary>
        ///// Gets or sets the ActCompPMReading value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string ActCompPMReading { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDeptID value.
        ///// </summary>
        //public int? MaintDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintSubDeptID value.
        ///// </summary>
        //public int? MaintSubDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDivisionID value.
        ///// </summary>
        //public int? MaintDivisionID { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

		
        ///// <summary>
        ///// Gets or sets the ProjectNumber value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string ProjectNumber { get; set; }

		#endregion
	}
}
