//-----------------------------------------------------------------------
// <copyright file="Receive.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

    public class ViewModelReceive
    {
        public int? StockID { get; set; }
        public int? SupplierID { get; set; }
        public int? L2ID { get; set; }
        public int? SubStoreID { get; set; }
        public string SubStoreCode { get; set; }
        public DateTime? Date { get; set; }
        public string MRNNo { get; set; }
        public string PONo { get; set; }
                
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AltStockDescription { get; set; }        
        public decimal? QtyRec { get; set; }
        public decimal? Price { get; set; }
        public decimal? TotalPrice { get; set; }
        public string GUID { get; set; }
        public decimal? HdnAvgPrice { get; set; }
        public string Comments { get; set; }
        


    }


	/// <summary>
	/// This class is used to Define Model for Table - receive
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("receive")]
	public sealed class Receive : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ReceiveID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ReceiveID { get; set; }

        /// <summary>
        /// Gets or sets the GUID value.
        /// </summary>
        [NotMapped]
        public string GUID { get; set; }

		/// <summary>
		/// Gets or sets the StockID value.
		/// </summary>
		public int? StockID { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreCode value.
        /// </summary>
        [NotMapped]
        public string SubStoreCode { get; set; }

		/// <summary>
		/// Gets or sets the SupplierID value.
		/// </summary>
		public int? SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the MaintGroupID value.
        /// </summary>
        [NotMapped]
        public string AltSupplierName { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnL2ID value.
        /// </summary>
        [NotMapped]
        public int? HdnL2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnSubStoreID value.
        /// </summary>
        [NotMapped]
        public int? HdnSubStoreID { get; set; }

		/// <summary>
		/// Gets or sets the SubStoreID value.
		/// </summary>
		public int? SubStoreID { get; set; }

		/// <summary>
		/// Gets or sets the FromL2ID value.
		/// </summary>
		public int? FromL2ID { get; set; }

		/// <summary>
		/// Gets or sets the FromSubStoreID value.
		/// </summary>
		public int? FromSubStoreID { get; set; }

		/// <summary>
		/// Gets or sets the Date value.
		/// </summary>
		public DateTime? Date { get; set; }

		/// <summary>
		/// Gets or sets the MRNNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string MRNNo { get; set; }

		/// <summary>
		/// Gets or sets the QtyRec value.
		/// </summary>
		public decimal? QtyRec { get; set; }

		/// <summary>
		/// Gets or sets the PONo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string PONo { get; set; }

		/// <summary>
		/// Gets or sets the Price value.
		/// </summary>
		public decimal? Price { get; set; }

        /// <summary>
        /// Gets or sets the Average Price value.
        /// </summary>
        [NotMapped]
        public decimal? AvgPrice { get; set; }

        /// <summary>
        /// Gets or sets the Average Price value.
        /// </summary>
        [NotMapped]
        public decimal? HdnAvgPrice { get; set; }

        /// <summary>
        /// Gets or sets the TotalPrice value.
        /// </summary>
        [NotMapped]
        public decimal? TotalPrice { get; set; }

		/// <summary>
		/// Gets or sets the BatchNo value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string BatchNo { get; set; }

		/// <summary>
		/// Gets or sets the BatchExpiryDate value.
		/// </summary>
		public DateTime? BatchExpiryDate { get; set; }

		/// <summary>
		/// Gets or sets the Comments value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Comments { get; set; }

		/// <summary>
		/// Gets or sets the Transfer value.
		/// </summary>
		public short? Transfer { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the InvoiceDate value.
		/// </summary>
		public DateTime? InvoiceDate { get; set; }

		/// <summary>
		/// Gets or sets the DeliveryOrderNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string DeliveryOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [NotMapped]
        public int? ID { get; set; }

        /// <summary>
        /// Gets or sets the po item identifier.
        /// </summary>
        /// <value>
        /// The po item identifier.
        /// </value>
        [NotMapped]
        public int? POItemID { get; set; }

        /// <summary>
        /// Gets or sets the wo no.
        /// </summary>
        /// <value>
        /// The wo no.
        /// </value>
        [NotMapped]
        public string WONo { get; set; }

        /// <summary>
        /// Gets or sets the pr no.
        /// </summary>
        /// <value>
        /// The pr no.
        /// </value>
        [NotMapped]
        public string PRNo { get; set; }

        /// <summary>
        /// Gets or sets the stock desc.
        /// </summary>
        /// <value>
        /// The stock desc.
        /// </value>
        [NotMapped]
        public string StockDesc { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string SubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the TransferText value.
        /// </summary>
        [NotMapped]
        public string TransferText { get; set; }

        /// <summary>
        /// Gets or sets the ToSectorName value.
        /// </summary>
        [NotMapped]
        public string FromSectorName { get; set; }

        /// <summary>
        /// Gets or sets the ToL2Name value.
        /// </summary>
        [NotMapped]
        public string FromL2name { get; set; }

        /// <summary>
        /// Gets or sets the ToSubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string FromSubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the List of Receive value.
        /// </summary>
        [NotMapped]
        public string lstReceives { get; set; }

        /// <summary>
        /// Gets or sets the TransactionID value.
        /// </summary>
        public long? TransactionID { get; set; }

		#endregion
	}
}
