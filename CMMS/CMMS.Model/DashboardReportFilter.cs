//-----------------------------------------------------------------------
// <copyright file="DashboardReportFilter.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - DashboardReportFilter
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("DashboardReportFilter")]
	public sealed class DashboardReportFilter : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the DashboardReportFilterID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int DashboardReportFilterID { get; set; }

		/// <summary>
		/// Gets or sets the FieldName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string FieldName { get; set; }

		/// <summary>
		/// Gets or sets the FieldValue value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string FieldValue { get; set; }

		/// <summary>
		/// Gets or sets the ReportUniqueName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string ReportUniqueName { get; set; }

		/// <summary>
		/// Gets or sets the UserID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int UserID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the dashboard repot menu identifier.
        /// </summary>
        /// <value>
        /// The dashboard repot menu identifier.
        /// </value>
        public int? DashboardRepotMenuID { get; set; }

		#endregion
	}
}
