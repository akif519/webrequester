﻿//-----------------------------------------------------------------------
// <copyright file="LevelNamesTbl.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - LevelNamesTbl
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>14-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("LevelNamesTbl")]
    public sealed class LevelNamesTbl : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L1_ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int L1_ID { get; set; }

        /// <summary>
        /// Gets or sets the L2_en value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L2_en { get; set; }

        /// <summary>
        /// Gets or sets the L2_ar value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L2_ar { get; set; }

        /// <summary>
        /// Gets or sets the L3_en value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L3_en { get; set; }

        /// <summary>
        /// Gets or sets the L3_ar value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L3_ar { get; set; }

        /// <summary>
        /// Gets or sets the L4_en value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L4_en { get; set; }

        /// <summary>
        /// Gets or sets the L4_ar value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L4_ar { get; set; }

        /// <summary>
        /// Gets or sets the L5_en value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L5_en { get; set; }

        /// <summary>
        /// Gets or sets the L5_ar value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string L5_ar { get; set; }

        #endregion
    }
}
