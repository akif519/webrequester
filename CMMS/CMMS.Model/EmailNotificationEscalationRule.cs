﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;

namespace CMMS.Model
{
    public class EmailNotificationEscalationRule : BaseModel
    {

        public Emailnotificationrule ObjectEmailNotificationRule { get; set; }
        public List<EmailReceiptant> ListEmailReceiptant_Employee { get; set; }
        public List<EmailTemplateWorkRequest> ListWorkRequest { get; set; }
        public List<EmailTemplateWorkOrder> ListWorkOrder { get; set; }

    }
}
