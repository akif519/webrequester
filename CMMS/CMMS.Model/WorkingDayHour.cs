//-----------------------------------------------------------------------
// <copyright file="WorkingDayHour.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - WorkingDayHours
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>19-Jan-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("WorkingDayHours")]
	public sealed class WorkingDayHour : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the WorkingDayHourID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int WorkingDayHourID { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the L1ID value.
		/// </summary>
		public int? L1ID { get; set; }

        /// <summary>
        /// Gets or sets the WorkingDayID value.
        /// </summary>
        public int? WorkingDayID { get; set; } 

		/// <summary>
		/// Gets or sets the StartDayOfWeek value.
		/// </summary>
		public int? StartDayOfWeek { get; set; }

		/// <summary>
		/// Gets or sets the DayID value.
		/// </summary>
		public int? DayID { get; set; }

		/// <summary>
		/// Gets or sets the IsWorkingDay value.
		/// </summary>
		public bool IsWorkingDay { get; set; }

		/// <summary>
		/// Gets or sets the StartTime value.
		/// </summary>
		public DateTime? StartTime { get; set; }

		/// <summary>
		/// Gets or sets the EndTime value.
		/// </summary>
		public DateTime? EndTime { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l1 no.
        /// </summary>
        /// <value>
        /// The l1 no.
        /// </value>
        [NotMapped]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public DateTime? StartTime1 { get; set; }

        [NotMapped]
        public DateTime? StartTime2 { get; set; }

        [NotMapped]
        public DateTime? StartTime3 { get; set; }

        [NotMapped]
        public DateTime? StartTime4 { get; set; }

        [NotMapped]
        public DateTime? StartTime5 { get; set; }

        [NotMapped]
        public DateTime? StartTime6 { get; set; }

        [NotMapped]
        public DateTime? StartTime7 { get; set; }

        [NotMapped]
        public DateTime? EndTime1 { get; set; }

        [NotMapped]
        public DateTime? EndTime2 { get; set; }

        [NotMapped]
        public DateTime? EndTime3 { get; set; }

        [NotMapped]
        public DateTime? EndTime4 { get; set; }

        [NotMapped]
        public DateTime? EndTime5 { get; set; }

        [NotMapped]
        public DateTime? EndTime6 { get; set; }

        [NotMapped]
        public DateTime? EndTime7 { get; set; }

        /// <summary>
        /// Gets or sets the Monday value.
        /// </summary>
        [NotMapped]
        public bool Monday { get; set; }

        /// <summary>
        /// Gets or sets the Tuesday value.
        /// </summary>
        [NotMapped]
        public bool Tuesday { get; set; }

        /// <summary>
        /// Gets or sets the Wednesday value.
        /// </summary>
        [NotMapped]
        public bool Wednesday { get; set; }

        /// <summary>
        /// Gets or sets the Thursday value.
        /// </summary>
        [NotMapped]
        public bool Thursday { get; set; }

        /// <summary>
        /// Gets or sets the Friday value.
        /// </summary>
        [NotMapped]
        public bool Friday { get; set; }

        /// <summary>
        /// Gets or sets the Saturday value.
        /// </summary>
        [NotMapped]
        public bool Saturday { get; set; }

        /// <summary>
        /// Gets or sets the Sunday value.
        /// </summary>
        [NotMapped]
        public bool Sunday { get; set; }

		#endregion
	}
}
