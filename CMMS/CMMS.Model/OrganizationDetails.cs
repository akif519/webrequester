﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace CMMS.Model
{
    [Table("OrganizationDetails")]
    public class OrganizationDetails
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Org_ID { get; set; }
        public string Country { get; set; }
        public string Country_A { get; set; }
        public string District { get; set; }
        public string District_A { get; set; }
        public string Branch_Name { get; set; }
        public string Branch_Name_A { get; set; }
        public string MODA { get; set; }
        public string MODA_A { get; set; }
        public string GDMW_A { get; set; }
        public string GDMW { get; set; }
        public string OMName { get; set; }
        public string OMName_A { get; set; }
        public string DirectorName { get; set; }
        public string DirectorName_A { get; set; }
        public string DirectorRank { get; set; }
        public string DirectorRank_A { get; set; }
        public string DirectorJobTitle { get; set; }
        public string DirectorJobTitle_A { get; set; }
        public string MaintDept { get; set; }
        public string MaintDept_A { get; set; }
        public string MaintDeptSprvsr { get; set; }
        public string MaintDeptSprvsr_A { get; set; }
        public string MaintDeptPosition { get; set; }
        public string MaintDeptPosition_A { get; set; }
        public string DeptOfficerName { get; set; }
        public string DeptOfficerName_A { get; set; }
        public string DeptOfficerPosition { get; set; }
        public string DeptOfficerPosition_A { get; set; }
        public string Moda_Logo { get; set; }
        public string Branch_Logo { get; set; }
        public bool ShowReportFooter { get; set; }
        public bool ShowOnLastPage { get; set; }
    }
}
