﻿//-----------------------------------------------------------------------
// <copyright file="Workorder.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - workorders
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("workorders")]
    public sealed class WorkOrder : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the RequestorID value.
        /// </summary>
        public int? RequestorID { get; set; }

        /// <summary>
        /// Gets or sets the RequestorName value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the AltName value.
        /// </summary>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeNO value.
        /// </summary>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        [NotMapped]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the WorkPhone value.
        /// </summary>
        [NotMapped]
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the Fax value.
        /// </summary>
        [NotMapped]
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the HandPhone value.
        /// </summary>
        [NotMapped]
        public string HandPhone { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2ClassCode value.
        /// </summary>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L3Desc value.
        /// </summary>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the L3AltDesc value.
        /// </summary>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L4Description value.
        /// </summary>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the L4AltDescription value.
        /// </summary>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the L5AltDescription value.
        /// </summary>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L4ID value.
        /// </summary>
        public int? L4ID { get; set; }

        /// <summary>
        /// Gets or sets the L5ID value.
        /// </summary>
        public int? L5ID { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the WorkStatusID value.
        /// </summary>
        public int? WorkStatusID { get; set; }

        /// <summary>
        /// Gets or sets the WorkPriorityID value.
        /// </summary>
        public int? WorkPriorityID { get; set; }

        /// <summary>
        /// Gets or sets the FailureCauseID value.
        /// </summary>
        public int? FailureCauseID { get; set; }

        /// <summary>
        /// Gets or sets the FailureCauseCode value.
        /// </summary>
        [NotMapped]
        public string FailureCauseCode { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeID value.
        /// </summary>
        public int? WorkTypeID { get; set; }

        /// <summary>
        /// Gets or sets the WOTradeID value.
        /// </summary>
        public int? WOTradeID { get; set; }

        /// <summary>
        /// Gets or sets the MeterID value.
        /// </summary>
        public int? MeterID { get; set; }

        [NotMapped]
        public PmMeter PMMeter { get; set; }

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        public int? PMID { get; set; }

        /// <summary>
        /// Gets or sets the PMNo value.
        /// </summary>
        [NotMapped]
        public string PMNo { get; set; }

        [NotMapped]
        public PmSchedule PMSchedule { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the MaintdeptID value.
        /// </summary>
        public int? MaintdeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintsubdeptID value.
        /// </summary>
        public int? MaintsubdeptID { get; set; }

        /// <summary>
        /// Gets or sets the GroupID value.
        /// </summary>
        public int? GroupID { get; set; }

        /// <summary>
        /// Gets or sets the GroupCode Value.
        /// </summary>
        [NotMapped]
        public string GroupCode { get; set; }

        /// <summary>
        /// Gets or sets the SupplierId value.
        /// </summary>
        public int? SupplierId { get; set; }

        /// <summary>
        /// Gets or sets the SupplierName value.
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AltSupplierName value.
        /// </summary>
       [NotMapped]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AcceptedbyID value.
        /// </summary>
        public int? AcceptedbyID { get; set; }

        /// <summary>
        /// Gets or sets the Acceptedby NO,
        /// </summary>
        [NotMapped]
        public string AcceptedByEmpNo { get; set; }

        /// <summary>
        /// Gets or sets the Acceptedby Name,
        /// </summary>
       [NotMapped]
        public string AcceptedByEmpName { get; set; }

        /// <summary>
        /// Gets or sets the Acceptedby Name,
        /// </summary>
        [NotMapped]
        public string AcceptedByEmpAltName { get; set; }

        /// <summary>
        /// Gets or sets the PMChecklistID value.
        /// </summary>
        public int? PMChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the PMChecklistNO value.
        /// </summary>
        [NotMapped]
        public string ChecklistNo { get; set; }

        [NotMapped]
        public string CheckListName { get; set; }

        [NotMapped]
        public string AltCheckListName { get; set; }

        /// <summary>
        /// Gets or sets the RatingsID value.
        /// </summary>
        public int? RatingsID { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the WOclosebyID value.
        /// </summary>
        public int? WOclosebyID { get; set; }

        /// <summary>
        /// Gets or sets the WOcloseby Name.
        /// </summary>
        [NotMapped]
        public string WOclosebyName { get; set; }

        /// <summary>
        /// Gets or sets the CostCenterId value.
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Gets or sets the ProblemDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string ProblemDescription { get; set; }

        /// <summary>
        /// Gets or sets the DateReceived value.
        /// </summary>
        public DateTime? DateReceived { get; set; }

        /// <summary>
        /// Gets or sets the DateReceived value.
        /// </summary>
        [NotMapped]
        public DateTime? TimeReceived { get; set; }

        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        public DateTime? EstDateStart { get; set; }

        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        [NotMapped]
        public DateTime? EstTimeStart { get; set; }

        /// <summary>
        /// Gets or sets the EstDateEnd value.
        /// </summary>
        public DateTime? EstDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        [NotMapped]
        public DateTime? EstTimeEnd { get; set; }

        /// <summary>
        /// Gets or sets the ActDateStart value.
        /// </summary>
        public DateTime? ActDateStart { get; set; }

        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        [NotMapped]
        public DateTime? ActTimeStart { get; set; }

        /// <summary>
        /// Gets or sets the ActDateEnd value.
        /// </summary>
        public DateTime? ActDateEnd { get; set; }

        /// <summary>
        /// Gets or sets the EstDateStart value.
        /// </summary>
        [NotMapped]
        public DateTime? ActTimeEnd { get; set; }

        /// <summary>
        /// Gets or sets the DateRequired value.
        /// </summary>
        public DateTime? DateRequired { get; set; }

        /// <summary>
        /// Gets or sets the DateRequired value.
        /// </summary>
        [NotMapped]
        public DateTime? TimeRequired { get; set; }

        /// <summary>
        /// Gets or sets the DateHandover value.
        /// </summary>
        public DateTime? DateHandover { get; set; }

        /// <summary>
        /// Gets or sets the DateHandover value.
        /// </summary>
        [NotMapped]
        public DateTime? TimeHandover { get; set; }

        /// <summary>
        /// Gets or sets the EstDuration value.
        /// </summary>
        public float? EstDuration { get; set; }

        /// <summary>
        /// Gets or sets the ActionTaken value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ActionTaken { get; set; }

        /// <summary>
        /// Gets or sets the CauseDescription value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string CauseDescription { get; set; }

        /// <summary>
        /// Gets or sets the PreventionTaken value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string PreventionTaken { get; set; }

        /// <summary>
        /// Gets or sets the WOCost value.
        /// </summary>
        public float? WOCost { get; set; }

        /// <summary>
        /// Gets or sets the PMTarStartDate value.
        /// </summary>
        public DateTime? PMTarStartDate { get; set; }

        /// <summary>
        /// Gets or sets the PMTarStartDate value.
        /// </summary>
        [NotMapped]
        public DateTime? PMTarStartTime { get; set; }

        /// <summary>
        /// Gets or sets the PMTarCompDate value.
        /// </summary>
        public DateTime? PMTarCompDate { get; set; }

        /// <summary>
        /// Gets or sets the PMTarStartDate value.
        /// </summary>
        [NotMapped]
        public DateTime? PMTarCompTime { get; set; }

        /// <summary>
        /// Gets or sets the AstartDate value.
        /// </summary>
        public DateTime? AstartDate { get; set; }

        /// <summary>
        /// Gets or sets the AstartTime value.
        /// </summary>
        [NotMapped]
        public DateTime? AstartTime { get; set; }

        /// <summary>
        /// Gets or sets the AEndDate value.
        /// </summary>
        public DateTime? AEndDate { get; set; }

        /// <summary>
        /// Gets or sets the AEndTime value.
        /// </summary>
        [NotMapped]
        public DateTime? AEndTime { get; set; }

        /// <summary>
        /// Gets or sets the WOLaborCost value.
        /// </summary>
        public float? WOLaborCost { get; set; }

        /// <summary>
        /// Gets or sets the WODICost value.
        /// </summary>
        public float? WODICost { get; set; }

        /// <summary>
        /// Gets or sets the WOPartCost value.
        /// </summary>
        public float? WOPartCost { get; set; }

        /// <summary>
        /// Gets or sets the WOPMtype value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WOPMtype { get; set; }

        /// <summary>
        /// Gets or sets the PMJOGeneratedReading value.
        /// </summary>
        public float? PMJOGeneratedReading { get; set; }

        /// <summary>
        /// Gets or sets the PMMetReading value.
        /// </summary>
        public float? PMMetReading { get; set; }

        /// <summary>
        /// Gets or sets the DownTime value.
        /// </summary>
        public float? DownTime { get; set; }

        /// <summary>
        /// Gets or sets the LineDowntime value.
        /// </summary>
        public float? LineDowntime { get; set; }

        /// <summary>
        /// Gets or sets the MTask value.
        /// </summary>
        public int? MTask { get; set; }

        /// <summary>
        /// Gets or sets the Notes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the MPMID value.
        /// </summary>
        public long? MPMID { get; set; }

        /// <summary>
        /// Gets or sets the Print_date value.
        /// </summary>
        public DateTime? Print_date { get; set; }

        /// <summary>
        /// Gets or sets the AssignTo value.
        /// </summary>
        [StringLength(1024, ErrorMessage = "*")]
        public string AssignTo { get; set; }

        /// <summary>
        /// Gets or sets the TelNo value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string TelNo { get; set; }

        /// <summary>
        /// Gets or sets the Accountcodeid value.
        /// </summary>
        public int? Accountcodeid { get; set; }

        /// <summary>
        /// Gets or sets the Documentstatusid value.
        /// </summary>
        public int? Documentstatusid { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the AuthorisedEmployeeId value.
        /// </summary>
        public int? AuthorisedEmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the CIGID value.
        /// </summary>
        public int? CIGID { get; set; }

        public CleaningInspectionGroup cleaningInspectionGroup { get;set; }

        /// <summary>
        /// Gets or sets the CIGNumber.
        /// </summary>
        [NotMapped]
        public int? GroupNumber { get; set; }

        /// <summary>
        /// Gets or sets the CIGNumber.
        /// </summary>
        [NotMapped]
        public string GroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the CIGNumber.
        /// </summary>
        [NotMapped]
        public string ArabicGroupDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsCleaningModule is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsCleaningModule { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the ProjectID value.
        /// </summary>
        public int? ProjectID { get; set; }

        [NotMapped]
        public string WorkStatus { get; set; }

        [NotMapped]
        public string AltWorkStatus { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string AssetAltDescription { get; set; }

        [NotMapped]
        public string AssetDescription { get; set; }

        [NotMapped]
        public string WorkTypeDescription { get; set; }

        [NotMapped]
        public string AltWorkTypeDescription { get; set; }

        [NotMapped]
        public string WorkPriority { get; set; }

        [NotMapped]
        public string AltWorkPriority { get; set; }

        [NotMapped]
        public string WorkTrade { get; set; }

        [NotMapped]
        public string AltWorkTrade { get; set; }
        
        [NotMapped]
        public string MaintDeptCode { get; set; }
        
        [NotMapped]
        public string ManHours { get; set; }

        [NotMapped]
        public string LabourCost { get; set; }

        [NotMapped]
        public string DICost { get; set; }

        [NotMapped]
        public string ItemCost { get; set; }

        /// <summary>
        /// Gets or sets the RequestNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string RequestNo { get; set; }

        /// <summary>
        /// Gets or sets the RequestorName value.
        /// </summary>
        [NotMapped]
        public string RequestorName { get; set; }

        /// <summary>
        /// Gets or sets the AuthEmployeeName value.
        /// </summary>
        [NotMapped]
        public string AuthEmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the AuthEmployeeName value.
        /// </summary>
        [NotMapped]
        public string LocationAuthEmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the NoteToTechLocation value.
        /// </summary>
        [NotMapped]
        public string NoteToTechLocation { get; set; }

        /// <summary>
        /// Gets or sets the NoteToTechAsset value.
        /// </summary>
        [NotMapped]
        public string NoteToTechAsset { get; set; }

        /// <summary>
        /// Gets or sets the Asset value.
        /// </summary>
        [NotMapped]
        public Asset Asset { get; set; }

        [NotMapped]
        public int WorkOrderElementCount { get; set; }


        public string MeterNo { get; set; }        
        public Nullable<int> TypePMgenID { get; set; }

        [NotMapped]
        public int? AssignedEmployeeId { get; set; }        
        public string WorkReqNo { get; set; }

        public byte[] ImageRaw { get; set; }
        /// <summary>
        /// 1 - When PM Checklist is added
        /// 2 - When New PM Checklist is inserted and need to delete associated records
        /// 0 - When PM Checklist is not changed
        /// </summary>                
        [NotMapped]
        public int FlagForPMCheckList { get; set; }
               
        public List<Assigntoworkorder> AssignToWorkOrderTable { get; set; }        
        public List<Direct> DirectTable { get; set; }        
        public List<Issue> IssueTable { get; set; }        
        public List<Return> ReturnTable { get; set; }        
        public List<Jobtask> JobTaskTable { get; set; }        
        public List<SITask> SITaskTable { get; set; }        
        public List<Workorderelement> WorkOrderElementTable { get; set; }        
        public List<WorkOrderItem> WorkOrderItemTable { get; set; }        
        public List<WorkOrderTool> WorkOrderToolTable { get; set; }        
        public List<WorkOrderPPE> WorkOrderPPETable { get; set; }        
        public List<PurchaseRequestItem> PurchaseRequestItemTable { get; set; }        
        public List<PurchaseOrderItem> PurchaseOrderItemsTable { get; set; }        
        public List<ExtAssetFile> ExtassetfileTable { get; set; }

        public List<JOStatusTrack> StatusTrackTable { get; set; }
        public List<Material_req_detail> ItemRequisitionTable { get; set; }

        public List<ExtAssetFile> DiagramTable { get; set; }
 
        

        //
        //public List<PMscheduleModel> PMScheduleTable { get; set; }
                
        public float? Latitude { get; set; }        
        public float? Longitude { get; set; }        
        public string Source { get; set; }        
        public int? LoggedInEmployeeID { get; set; }                
        //public string SignatureLink { get; set; }


        #endregion
    }
}


namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - workorders
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("test")]
    public sealed class test : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int testid { get; set; }


        /// <summary>
        /// Gets or sets the RequestorName value.
        /// </summary>
        public string testname { get; set; }



        #endregion
    }
}
