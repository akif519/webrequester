﻿//-----------------------------------------------------------------------
// <copyright file="TabAccess.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - TabAccess
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>14-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("TabAccess")]
    public sealed class TabAccess : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ClientID { get; set; }


        //public bool TransactionValue
        //{
        //    get { return Transactions == 1; }
        //    set { Transactions = value ? 1 : 0; }
        //}
        /// <summary>
        /// Gets or sets the Transactions value.
        /// </summary>
        public bool? Transactions { get; set; }

        /// <summary>
        /// Gets or sets the Setup value.
        /// </summary>
        public bool? Setup { get; set; }

        /// <summary>
        /// Gets or sets the Reports value.
        /// </summary>
        public bool? Reports { get; set; }

        /// <summary>
        /// Gets or sets the Dashboard value.
        /// </summary>
        public bool? Dashboard { get; set; }

        #endregion
    }
}
