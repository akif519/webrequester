﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - SearchFields
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>29-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("SearchFields")]
    public sealed class SearchField : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SearchFieldID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SearchFieldID { get; set; }

        /// <summary>
        /// Gets or sets the PageID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PageID { get; set; }

        /// <summary>
        /// Gets or sets the Display value.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the Display Label Name.
        /// </summary>
        public string DisplayLabelName { get; set; }

        /// <summary>
        /// Gets or sets the DBColumnName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string DBColumnName { get; set; }

        /// <summary>
        /// Gets or sets the MainTableName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string MainTableName { get; set; }

        /// <summary>
        /// Gets or sets the TableName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the DBColumnType value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string DBColumnType { get; set; }

        [NotMapped]
        public int OperatorID { get; set; }

        [NotMapped]
        public string Value1{ get; set; }

        [NotMapped]
        public string Value2{ get; set; }

        [NotMapped]
        public string Condition { get; set; }

        #endregion
    }

}
