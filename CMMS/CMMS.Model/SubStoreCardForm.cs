//-----------------------------------------------------------------------
// <copyright file="SubStoreCardForm.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - SubStoreCardForm
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("SubStoreCardForm")]
	public sealed class SubStoreCardForm : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CardId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CardId { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the CardNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string CardNo { get; set; }

		/// <summary>
		/// Gets or sets the FileNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string FileNo { get; set; }

		/// <summary>
		/// Gets or sets the PrivateNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string PrivateNo { get; set; }

		/// <summary>
		/// Gets or sets the SNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string SNo { get; set; }

		/// <summary>
		/// Gets or sets the DateValue value.
		/// </summary>
		public DateTime? DateValue { get; set; }

		/// <summary>
		/// Gets or sets the Attached value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string Attached { get; set; }

		/// <summary>
		/// Gets or sets the SubStoreIdRef value.
		/// </summary>
		public int? SubStoreIdRef { get; set; }

		/// <summary>
		/// Gets or sets the Status value.
		/// </summary>
		public int? Status { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModfiedBy value.
		/// </summary>
		public int? ModfiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the employee no.
        /// </summary>
        /// <value>
        /// The employee no.
        /// </value>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the positions.
        /// </summary>
        /// <value>
        /// The positions.
        /// </value>
        [NotMapped]
        public string Positions { get; set; }

        /// <summary>
        /// Gets or sets the sub store code.
        /// </summary>
        /// <value>
        /// The sub store code.
        /// </value>
        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the sub store desc.
        /// </summary>
        /// <value>
        /// The sub store desc.
        /// </value>
        [NotMapped]
        public string SubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt sub store desc.
        /// </summary>
        /// <value>
        /// The alt sub store desc.
        /// </value>
        [NotMapped]
        public string AltSubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the name of the status.
        /// </summary>
        /// <value>
        /// The name of the status.
        /// </value>
        [NotMapped]
        public string StatusName { get; set; }

        /// <summary>
        /// Gets or sets the name of the status alt.
        /// </summary>
        /// <value>
        /// The name of the status alt.
        /// </value>
        [NotMapped]
        public string StatusAltName { get; set; }

        /// <summary>
        /// Gets or sets the maint sub dept identifier.
        /// </summary>
        /// <value>
        /// The maint sub dept identifier.
        /// </value>
        [NotMapped]
        public int MaintSubDeptID { get; set; }

		#endregion
	}
}
