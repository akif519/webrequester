//-----------------------------------------------------------------------
// <copyright file="SubStoreFormRetunDetail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - SubStoreFormRetunDetail
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("SubStoreFormRetunDetail")]
	public sealed class SubStoreFormRetunDetail : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ReturnId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ReturnId { get; set; }

		/// <summary>
		/// Gets or sets the CardIdRef value.
		/// </summary>
		public int? CardIdRef { get; set; }

		/// <summary>
		/// Gets or sets the StockIdRef value.
		/// </summary>
		public int? StockIdRef { get; set; }

		/// <summary>
		/// Gets or sets the QtyIssued value.
		/// </summary>
		public decimal? QtyIssued { get; set; }

		/// <summary>
		/// Gets or sets the IsType value.
		/// </summary>
		public int? IsType { get; set; }

		/// <summary>
		/// Gets or sets the Remarks value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string Remarks { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModfiedDate value.
		/// </summary>
		public DateTime? ModfiedDate { get; set; }

		#endregion
	}
}
