﻿//-----------------------------------------------------------------------
// <copyright file="Specification.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Specifications
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>27-Dec-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Specifications")]
    public sealed class Specification : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SpecID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SpecID { get; set; }

        /// <summary>
        /// Gets or sets the SpecDesc value.
        /// </summary>        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SpecDesc { get; set; }

        /// <summary>
        /// Gets or sets the SpecAltDesc value.
        /// </summary>        
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SpecAltDesc { get; set; }

        /// <summary>
        /// Gets or sets the UOM value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string UOM { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>        
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
