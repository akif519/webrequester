﻿//-----------------------------------------------------------------------
// <copyright file="PurchaseOrder.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PurchaseOrder
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PurchaseOrder")]
    public sealed class PurchaseOrder : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseOrderNo value.
        /// </summary>    
        public string PurchaseOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the OrderBy value.
        /// </summary>
        [AllowNullForSave]
        public int? OrderBy { get; set; }

        /// <summary>
        /// Gets or sets the InvoiceTo value.
        /// </summary>
        [AllowNullForSave]
        public int? InvoiceTo { get; set; }

        /// <summary>
        /// Gets or sets the SupplierID value.
        /// </summary>
        public int? SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the QuotationDate value.
        /// </summary>
        public DateTime? QuotationDate { get; set; }

        /// <summary>
        /// Gets or sets the OrderedDate value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? OrderedDate { get; set; }

        /// <summary>
        /// Gets or sets the ExpDeliveryDate value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? ExpDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the TermOfSaleID value.
        /// </summary>
        public int? TermOfSaleID { get; set; }

        /// <summary>
        /// Gets or sets the ShippedViaID value.
        /// </summary>
        public int? ShippedViaID { get; set; }

        /// <summary>
        /// Gets or sets the PaymentTermsID value.
        /// </summary>
        public int? PaymentTermsID { get; set; }

        /// <summary>
        /// Gets or sets the AccountCodeID value.
        /// </summary>
        public int? AccountCodeID { get; set; }

        /// <summary>
        /// Gets or sets the CurrencyID value.
        /// </summary>
        [AllowNullForSave]
        public int? CurrencyID { get; set; }

        /// <summary>
        /// Gets or sets the DeliverTo value.
        /// </summary>   
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DeliverTo { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary> 
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the OrderStatus value.
        /// </summary>        
        public string OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets the DeliveryStatus value.
        /// </summary>        
        public string DeliveryStatus { get; set; }

        /// <summary>
        /// Gets or sets the AuthorisedBy value.
        /// </summary>
        public int? AuthorisedBy { get; set; }

        /// <summary>
        /// Gets or sets the OrderTotal value.
        /// </summary>
        public decimal? OrderTotal { get; set; }

        /// <summary>
        /// Gets or sets the DiscountPercentage value.
        /// </summary>        
        public decimal? DiscountPercentage { get; set; }

        /// <summary>
        /// Gets or sets the GSTPercentage value.
        /// </summary>
        public decimal? GSTPercentage { get; set; }

        /// <summary>
        /// Gets or sets the FreightCharges value.
        /// </summary>
        public decimal? FreightCharges { get; set; }

        /// <summary>
        /// Gets or sets the TaxPercentage value.
        /// </summary>
        public decimal? TaxPercentage { get; set; }

        /// <summary>
        /// Gets or sets the GRNNo value.
        /// </summary>        
        public string GRNNo { get; set; }

        /// <summary>
        /// Gets or sets the GRNCreatedDate value.
        /// </summary>
        public DateTime? GRNCreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the QuotationReference value.
        /// </summary>  
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string QuotationReference { get; set; }

        /// <summary>
        /// Gets or sets the TotalBaseCost value.
        /// </summary>
        public decimal? TotalBaseCost { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedDate value.
        /// </summary>
        public DateTime? LastPrintedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedBy value.
        /// </summary>
        public int? LastPrintedBy { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>        
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the MaintsubdeptID value.
        /// </summary>
        public int? MaintsubdeptID { get; set; }

        /// <summary>
        /// Gets or sets the ProjectID value.
        /// </summary>
        public int? ProjectID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the l2 altname.
        /// </summary>
        /// <value>
        /// The l2 altname.
        /// </value>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the Maintainance dept code value.
        /// </summary>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the name of the supplier.
        /// </summary>
        /// <value>
        /// The name of the supplier.
        /// </value>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt supplier.
        /// </summary>
        /// <value>
        /// The name of the alt supplier.
        /// </value>
        [NotMapped]
        public string AltSupplierName { get; set; }

        [NotMapped]
        public string OrderByEmployeeNO { get; set; }

        [NotMapped]
        public string OrderByEmployeeName { get; set; }

        [NotMapped]
        public string OrderByAltEmployeeName { get; set; }

        [NotMapped]
        public string InvoiceToEmployeeNO { get; set; }

        [NotMapped]
        public string InvoiceToEmployeeName { get; set; }

        [NotMapped]
        public string InvoiceToAltEmployeeName { get; set; }

        [NotMapped]
        public int CostCenterId { get; set; }

        [NotMapped]
        public string CostCenterNo { get; set; }

        [NotMapped]
        public string CostCenterName { get; set; }

        [NotMapped]
        public string AltCostCenterName { get; set; }

        [NotMapped]
        public string AccountCode { get; set; }

        [NotMapped]
        public string AccountCodeDesc { get; set; }

        [NotMapped]
        public string AltAccountCodeDesc { get; set; }

        [NotMapped]
        public string CurrencyCode { get; set; }

        [NotMapped]
        public string CurrencyRate { get; set; }

        [NotMapped]
        public string CurrencyDescription { get; set; }

        [NotMapped]
        public string AltCurrencyDescription { get; set; }

        [NotMapped]
        public bool IsAnyPOItemReceivedOrCancelled { get; set; }

        [NotMapped]
        public string JsonlstItemModel { get; set; }

        [NotMapped]
        public List<PurchaseOrderItem> lstPurchaseOrderItem { get; set; }

        /// <summary>
        /// Gets or sets the payment terms desc.
        /// </summary>
        /// <value>
        /// The payment terms desc.
        /// </value>
        [NotMapped]
        public string PaymentTermsDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt payment terms desc.
        /// </summary>
        /// <value>
        /// The alt payment terms desc.
        /// </value>
        [NotMapped]
        public string AltPaymentTermsDesc { get; set; }

        /// <summary>
        /// Gets or sets the shipped via desc.
        /// </summary>
        /// <value>
        /// The shipped via desc.
        /// </value>
        [NotMapped]
        public string ShippedViaDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt shipped via desc.
        /// </summary>
        /// <value>
        /// The alt shipped via desc.
        /// </value>
        [NotMapped]
        public string AltShippedViaDesc { get; set; }

        /// <summary>
        /// Gets or sets the termof sale desc.
        /// </summary>
        /// <value>
        /// The termof sale desc.
        /// </value>
        [NotMapped]
        public string TermofSaleDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt termof sale desc.
        /// </summary>
        /// <value>
        /// The alt termof sale desc.
        /// </value>
        [NotMapped]
        public string AltTermofSaleDesc { get; set; }

        [NotMapped]
        public string workOrderNo { get; set; }

        #endregion
    }
}
