﻿//-----------------------------------------------------------------------
// <copyright file="CmSupplierContractorDocument.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - cmSupplierContractorDocument
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmSupplierContractorDocument")]
    public sealed class CmSupplierContractorDocument : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the DocID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocID { get; set; }

        /// <summary>
        /// Gets or sets the SupplierContractorID value.
        /// </summary>
        public int? SupplierContractorID { get; set; }

        /// <summary>
        /// Gets or sets the DocName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string DocName { get; set; }

        /// <summary>
        /// Gets or sets the DocArabicName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DocArabicName { get; set; }

        /// <summary>
        /// Gets or sets the Doc_ExpiryDate value.
        /// </summary>
        public DateTime? Doc_ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the ClassificationLevel value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string ClassificationLevel { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>  
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the IssueDate value.
        /// </summary>
        public DateTime? IssueDate { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
