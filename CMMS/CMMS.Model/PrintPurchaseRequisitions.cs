﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
   public class PrintPurchaseRequisitions
    {
        public string Rowno { get; set; }
        public string StockNo { get; set; }
        public string PartDescription { get; set; }
        public string Altpart_desc { get; set; }
        public string UoM { get; set; }
        public decimal? Quantity { get; set; }
        public string WorkorderNo { get; set; }
        public int Id { get; set; }
        public int L2ID { get; set; }
        public string L2Code { get; set; }
        public string PurchaseRequestID { get; set; }
        public int MaintdivId { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDivisionAltName { get; set; }
        public int MaintdeptId { get; set; }
        public string MaintDeptdesc { get; set; }
        public string MaintDeptAltdesc { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
