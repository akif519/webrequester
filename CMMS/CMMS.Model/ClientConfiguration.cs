﻿//-----------------------------------------------------------------------
// <copyright file="ClientConfiguration.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - ClientConfiguration
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>09-Sep-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("ClientConfiguration")]
    public sealed class ClientConfiguration : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ConfigurationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConfigurationID { get; set; }

        /// <summary>
        /// Gets or sets the CompanyName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the NoOfUsers value.
        /// </summary>
        public int? NoOfUsers { get; set; }

        /// <summary>
        /// Gets or sets the LogoImage value.
        /// </summary>
        [StringLength(250, ErrorMessage = "*")]
        public string LogoImage { get; set; }

        /// <summary>
        /// Gets or sets the IsModuleInclude value.
        /// </summary>
        public bool? IsModuleInclude { get; set; }

        /// <summary>
        /// Gets or sets the IsEnableMobileApp value.
        /// </summary>
        public bool? IsEnableMobileApp { get; set; }

        /// <summary>
        /// Gets or sets the ExpiryDate value.
        /// </summary>
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the DatabaseType value.
        /// </summary>
        public short? DatabaseType { get; set; }

        /// <summary>
        /// Gets or sets the ConnectionString value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string ConnectionString { get; set; }

        #endregion
    }
}
