﻿

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public sealed class DatabaseParameter : BaseModel
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string DataFileName { get; set; }
        public string DataPathName { get; set; }
        public string DataFileGrowth { get; set; }
        public string LogFileName { get; set; }
        public string LogPathName { get; set; }
        public string LogFileGrowth { get; set; }
    }
}
