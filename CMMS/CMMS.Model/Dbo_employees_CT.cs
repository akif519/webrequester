//-----------------------------------------------------------------------
// <copyright file="Dbo_employees_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_employees_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_employees_CT")]
    public sealed class Dbo_employees_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }



        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeNO value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Name value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Name { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }


        /// <summary>
        /// Gets or sets the Positions value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string Positions { get; set; }


        [NotMapped]
        public string CategoryName { get; set; }

        [NotMapped]
        public string EmployeeStatus { get; set; }

        [NotMapped]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the WorkPhone value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the HandPhone value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string HandPhone { get; set; }

        /// <summary>
        /// Gets or sets the Housephone value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Housephone { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Fax value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the Address value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the OfficeLocation value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string OfficeLocation { get; set; }

        /// <summary>
        /// Gets or sets the HourlySalary value.
        /// </summary>
        public decimal? HourlySalary { get; set; }

        /// <summary>
        /// Gets or sets the OverTime1 value.
        /// </summary>
        public decimal? OverTime1 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime2 value.
        /// </summary>
        public decimal? OverTime2 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime3 value.
        /// </summary>
        public decimal? OverTime3 { get; set; }

        /// <summary>
        /// Gets or sets the IsStoredSupervisor value.
        /// </summary>
        public bool? IsStoredSupervisor { get; set; }

        /// <summary>
        /// Gets or sets the UserID value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the Central value.
        /// </summary>
        public int? Central { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeID value.
        ///// </summary>
        //public int? EmployeeID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the CategoryID value.
        ///// </summary>
        //public int? CategoryID { get; set; }

        ///// <summary>
        ///// Gets or sets the WorkTradeID value.
        ///// </summary>
        //public int? WorkTradeID { get; set; }

        ///// <summary>
        ///// Gets or sets the UserGroupId value.
        ///// </summary>
        //public int? UserGroupId { get; set; }


        ///// <summary>
        ///// Gets or sets the AltName value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string AltName { get; set; }

        ///// <summary>
        ///// Gets or sets the Extension value.
        ///// </summary>
        //[StringLength(30, ErrorMessage = "*")]
        //public string Extension { get; set; }







        ///// <summary>
        ///// Gets or sets the Accessibility value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Accessibility { get; set; }


        ///// <summary>
        ///// Gets or sets the Password value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Password { get; set; }




        ///// <summary>
        ///// Gets or sets the Auth value.
        ///// </summary>
        //public int? Auth { get; set; }

        ///// <summary>
        ///// Gets or sets the ExecLevel value.
        ///// </summary>
        //public int? ExecLevel { get; set; }

        ///// <summary>
        ///// Gets or sets the WebAccess value.
        ///// </summary>
        //public int? WebAccess { get; set; }

        ///// <summary>
        ///// Gets or sets the OpenWorkorders value.
        ///// </summary>
        //public int? OpenWorkorders { get; set; }

        ///// <summary>
        ///// Gets or sets the ReopenWorkorders value.
        ///// </summary>
        //public int? ReopenWorkorders { get; set; }

        ///// <summary>
        ///// Gets or sets the Connected value.
        ///// </summary>
        //public int? Connected { get; set; }

        ///// <summary>
        ///// Gets or sets the WRConnected value.
        ///// </summary>
        //public int? WRConnected { get; set; }

        ///// <summary>
        ///// Gets or sets the AllowAdjustment value.
        ///// </summary>
        //public int? AllowAdjustment { get; set; }

        ///// <summary>
        ///// Gets or sets the Pr_auto_amount value.
        ///// </summary>
        //public decimal? Pr_auto_amount { get; set; }

        ///// <summary>
        ///// Gets or sets the EmpDeptCode value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string EmpDeptCode { get; set; }

        ///// <summary>
        ///// Gets or sets the AllowCancelPO value.
        ///// </summary>
        //public int? AllowCancelPO { get; set; }

        ///// <summary>
        ///// Gets or sets the ConnectedMW value.
        ///// </summary>
        //public int? ConnectedMW { get; set; }

        ///// <summary>
        ///// Gets or sets the WOReturn value.
        ///// </summary>
        //public int? WOReturn { get; set; }

        ///// <summary>
        ///// Gets or sets the WOIssue value.
        ///// </summary>
        //public int? WOIssue { get; set; }

        ///// <summary>
        ///// Gets or sets the Hide_cost value.
        ///// </summary>
        //public int? Hide_cost { get; set; }

        ///// <summary>
        ///// Gets or sets the LanguageCode value.
        ///// </summary>
        //[StringLength(10, ErrorMessage = "*")]
        //public string LanguageCode { get; set; }

        ///// <summary>
        ///// Gets or sets the AllowInterSiteTransfer value.
        ///// </summary>
        //public int? AllowInterSiteTransfer { get; set; }

        ///// <summary>
        ///// Gets or sets the Postal_address value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string Postal_address { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDeptID value.
        ///// </summary>
        //public int? MaintDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintSubDeptID value.
        ///// </summary>
        //public int? MaintSubDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDivisionID value.
        ///// </summary>
        //public int? MaintDivisionID { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeStatusId value.
        ///// </summary>
        //public int? EmployeeStatusId { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }




        ///// <summary>
        ///// Gets or sets the IsLogIn value.
        ///// </summary>
        //public bool? IsLogIn { get; set; }

        ///// <summary>
        ///// Gets or sets the POAuthorisationLimit value.
        ///// </summary>
        //public decimal? POAuthorisationLimit { get; set; }

        ///// <summary>
        ///// Gets or sets the EmpHasCert value.
        ///// </summary>
        //public bool? EmpHasCert { get; set; }

        ///// <summary>
        ///// Gets or sets the ClassificationLevel value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string ClassificationLevel { get; set; }

        ///// <summary>
        ///// Gets or sets the ID_No value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string ID_No { get; set; }

        ///// <summary>
        ///// Gets or sets the ID_ExpiryDate value.
        ///// </summary>
        //public DateTime? ID_ExpiryDate { get; set; }

        ///// <summary>
        ///// Gets or sets the Remarks value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string Remarks { get; set; }

        ///// <summary>
        ///// Gets or sets the PassportNo value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string PassportNo { get; set; }

        ///// <summary>
        ///// Gets or sets the PassportExp value.
        ///// </summary>
        //public DateTime? PassportExp { get; set; }

        ///// <summary>
        ///// Gets or sets the Nationality value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string Nationality { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeProfessionID value.
        ///// </summary>
        //public int? EmployeeProfessionID { get; set; }

        ///// <summary>
        ///// Gets or sets the NationalityID value.
        ///// </summary>
        //public int? NationalityID { get; set; }

        ///// <summary>
        ///// Gets or sets the ProjectTypeID value.
        ///// </summary>
        //public int? ProjectTypeID { get; set; }

        ///// <summary>
        ///// Gets or sets the SponsorID value.
        ///// </summary>
        //public int? SponsorID { get; set; }

        ///// <summary>
        ///// Gets or sets the TBtransaction value.
        ///// </summary>
        //public int? TBtransaction { get; set; }

        ///// <summary>
        ///// Gets or sets the TBSetup value.
        ///// </summary>
        //public int? TBSetup { get; set; }

        ///// <summary>
        ///// Gets or sets the TBReport value.
        ///// </summary>
        //public int? TBReport { get; set; }

        ///// <summary>
        ///// Gets or sets the TBDashboard value.
        ///// </summary>
        //public int? TBDashboard { get; set; }

        ///// <summary>
        ///// Gets or sets the LastConfigChangeDateTime value.
        ///// </summary>
        //public DateTime? LastConfigChangeDateTime { get; set; }

        ///// <summary>
        ///// Gets or sets the ImagePath value.
        ///// </summary>
        //[StringLength(1000, ErrorMessage = "*")]
        //public string ImagePath { get; set; }

        #endregion
    }
}
