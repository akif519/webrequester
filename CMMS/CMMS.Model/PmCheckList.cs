﻿//-----------------------------------------------------------------------
// <copyright file="Pmchecklist.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - pmchecklist
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("pmchecklist")]
    public sealed class PMCheckList : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ChecklistID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the SafetyID value.
        /// </summary>
        public int? SafetyID { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string CheckListName { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltCheckListName { get; set; }

        /// <summary>
        /// Gets or sets the EstimatedLaborHours value.
        /// </summary>
        public decimal? EstimatedLaborHours { get; set; }

        /// <summary>
        /// Gets or sets the FileLink value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileLink { get; set; }

        /// <summary>
        /// Gets or sets the L2Id value.
        /// </summary>
        [AllowNullForSave]
        public int? L2Id { get; set; }

        /// <summary> 
        /// Gets or sets the MaintdeptId value.
        /// </summary>
        [AllowNullForSave]
        public int? MaintdeptId { get; set; }

        /// <summary>
        /// Gets or sets the MainSubDeptId value.
        /// </summary>
        [AllowNullForSave]
        public int? MainSubDeptId { get; set; }

        /// <summary>
        /// Gets or sets the MaintdivId value.
        /// </summary>
        [AllowNullForSave]
        public int? MaintdivId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsCleaningModule is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsCleaningModule { get; set; }

        [NotMapped]
        public string L2Code { get; set; }
        [NotMapped]
        public string MaintDivisionCode { get; set; }
        [NotMapped]
        public string MaintDeptCode { get; set; }
        [NotMapped]
        public string MaintSubDeptCode { get; set; }
        [NotMapped]
        public string SafetyNo { get; set; }
        [NotMapped]
        public string SafetyName { get; set; }
        [NotMapped]
        public string AltSafetyName { get; set; }
        [NotMapped]
        public string FileDescription { get; set; }
        [NotMapped]
        public string FileName { get; set; }
        [NotMapped]
        public int ChecklistelementId { get; set; }
        [NotMapped]
        public int SeqNo { get; set; }
        /// <summary>
        /// Auto ID From ExtAssetFiles Table For Attachment
        /// </summary>
        [NotMapped]
        public int AutoId { get; set; }

        #endregion
    }
}
