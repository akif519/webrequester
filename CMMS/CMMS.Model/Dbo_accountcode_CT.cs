//-----------------------------------------------------------------------
// <copyright file="Dbo_accountcode_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_accountcode_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_accountcode_CT")]
	public sealed class Dbo_accountcode_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the AccCodeid value.
        ///// </summary>
        //public int? AccCodeid { get; set; }

		/// <summary>
		/// Gets or sets the AccountCode value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string AccountCode { get; set; }

		/// <summary>
		/// Gets or sets the AccountCodeDesc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string AccountCodeDesc { get; set; }

		/// <summary>
		/// Gets or sets the AltAccountCodeDesc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string AltAccountCodeDesc { get; set; }

        [NotMapped]
        public string CostCenterNo { get; set; }

        ///// <summary>
        ///// Gets or sets the CostCenterId value.
        ///// </summary>
        //public int? CostCenterId { get; set; }

		/// <summary>
		/// Gets or sets the DateStart value.
		/// </summary>
		public string DateStart { get; set; }

		/// <summary>
		/// Gets or sets the DateEnd value.
		/// </summary>
		public string DateEnd { get; set; }

		/// <summary>
		/// Gets or sets the Status value.
		/// </summary>
		public bool? Status { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
        public string ModifiedDate { get; set; }

		#endregion
	}
}
