//-----------------------------------------------------------------------
// <copyright file="MaintenanceDepartment.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;    

	/// <summary>
	/// This class is used to Define Model for Table - MaintenanceDepartment
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("MaintenanceDepartment")]
	public sealed class MaintenanceDepartment : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the MaintDeptID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int MaintDeptID { get; set; }

		/// <summary>
		/// Gets or sets the MaintDeptCode value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string MaintDeptCode { get; set; }

		/// <summary>
		/// Gets or sets the MaintDeptdesc value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string MaintDeptdesc { get; set; }

		/// <summary>
		/// Gets or sets the MaintDivisionID value.
		/// </summary>
		public int? MaintDivisionID { get; set; }

		/// <summary>
		/// Gets or sets the MaintDeptAltdesc value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string MaintDeptAltdesc { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the CmActivityGroupID value.
		/// </summary>               
		public int? CmActivityGroupID { get; set; }

        /// <summary>
        /// Gets or sets the maint division code.
        /// </summary>
        /// <value>
        /// The maint division code.
        /// </value>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the is enable maint div.
        /// </summary>
        /// <value>
        /// The is enable maint div.
        /// </value>
        [NotMapped]
        public bool? IsEnableMaintDiv { get; set; }

        [NotMapped]
        public bool IsChecked { get; set; }
		#endregion
	}
}
