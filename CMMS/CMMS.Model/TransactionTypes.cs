﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - Transactions Types
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>20-Dec-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("TransactionTypes")]
    public sealed class TransactionTypes : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Transaction Type ID value.
        /// </summary>
        public Int32 TransactionTypeID { get; set; }

        /// <summary>
        /// Gets or sets the TransactionNO value.
        /// </summary>
        [Required(ErrorMessage = "TypeName cannot be blank.")]
        [StringLength(50)]
        public string TypeName { get; set; }

        /// <summary>
        /// Gets or sets the TransactionNO value.
        /// </summary>
        [Required(ErrorMessage = "AltTypeName cannot be blank.")]
        [StringLength(50)]
        public string AltTypeName { get; set; }

        #endregion
    }
}
