//-----------------------------------------------------------------------
// <copyright file="Dbo_PurchaseRequestItems_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_PurchaseRequestItems_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_PurchaseRequestItems_CT")]
    public sealed class Dbo_PurchaseRequestItems_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestID value.
        /// </summary>
        public string PurchaseRequestID { get; set; }        

        /// <summary>
        /// Gets or sets the PartDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string PartDescription { get; set; }

        /// <summary>
        /// Gets or sets the UoM value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string UoM { get; set; }

        /// <summary>
        /// Gets or sets the Quantity value.
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the Source value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the JoborderID value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string JoborderID { get; set; }

        /// <summary>
        /// Gets or sets the RefPrice value.
        /// </summary>
        public decimal? RefPrice { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
