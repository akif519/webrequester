﻿//-----------------------------------------------------------------------
// <copyright file="ModulesAccess.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - ModulesAccess
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>14-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("ModulesAccess")]
	public sealed class ModulesAccess : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ClientID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int ClientID { get; set; }

		/// <summary>
		/// Gets or sets the JobRequest value.
		/// </summary>
		public bool? JobRequest { get; set; }

		/// <summary>
		/// Gets or sets the JobOrder value.
		/// </summary>
		public bool? JobOrder { get; set; }

		/// <summary>
		/// Gets or sets the Assets value.
		/// </summary>
		public bool? Assets { get; set; }

		/// <summary>
		/// Gets or sets the Locations value.
		/// </summary>
		public bool? Locations { get; set; }

		/// <summary>
		/// Gets or sets the PreventiveMaintenance value.
		/// </summary>
		public bool? PreventiveMaintenance { get; set; }

		/// <summary>
		/// Gets or sets the Store value.
		/// </summary>
		public bool? Store { get; set; }


        /// <summary>
		/// Gets or sets the Store value.
		/// </summary>
        public bool? RequestCard { get; set; }        

		/// <summary>
		/// Gets or sets the Purchasing value.
		/// </summary>
		public bool? Purchasing { get; set; }

		/// <summary>
		/// Gets or sets the Employee value.
		/// </summary>
		public bool? Employee { get; set; }

		/// <summary>
		/// Gets or sets the Cleaning value.
		/// </summary>
		public bool? Cleaning { get; set; }

		/// <summary>
		/// Gets or sets the CEUML value.
		/// </summary>
		public bool? CEUML { get; set; }

		/// <summary>
		/// Gets or sets the AuditTrail value.
		/// </summary>
		public bool? AuditTrail { get; set; }

		/// <summary>
		/// Gets or sets the Sector value.
		/// </summary>
		public bool? Sector { get; set; }

		/// <summary>
		/// Gets or sets the Zone value.
		/// </summary>
		public bool? Zone { get; set; }

		/// <summary>
		/// Gets or sets the Building value.
		/// </summary>
		public bool? Building { get; set; }

		/// <summary>
		/// Gets or sets the Division value.
		/// </summary>
		public bool? Division { get; set; }

		/// <summary>
		/// Gets or sets the Department value.
		/// </summary>
		public bool? Department { get; set; }

		/// <summary>
		/// Gets or sets the SubDept value.
		/// </summary>
		public bool? SubDept { get; set; }

		/// <summary>
		/// Gets or sets the Groups value.
		/// </summary>
		public bool? Groups { get; set; }

		/// <summary>
		/// Gets or sets the FailureCodes value.
		/// </summary>
		public bool? FailureCodes { get; set; }

		/// <summary>
		/// Gets or sets the Suppliers/Contractors value.
		/// </summary>
		public bool? Suppliers_Contractors { get; set; }

		/// <summary>
		/// Gets or sets the BoMList value.
		/// </summary>
		public bool? BoMList { get; set; }

		/// <summary>
		/// Gets or sets the CostCenter value.
		/// </summary>
		public bool? CostCenter { get; set; }

		/// <summary>
		/// Gets or sets the AccountCode value.
		/// </summary>
		public bool? AccountCode { get; set; }

		/// <summary>
		/// Gets or sets the JobPlans value.
		/// </summary>
		public bool? JobPlans { get; set; }

		/// <summary>
		/// Gets or sets the SafetyInstructions value.
		/// </summary>
		public bool? SafetyInstructions { get; set; }

		/// <summary>
		/// Gets or sets the SiteSetups value.
		/// </summary>
		public bool? SiteSetups { get; set; }

		/// <summary>
		/// Gets or sets the StoreSetups value.
		/// </summary>
		public bool? StoreSetups { get; set; }

		/// <summary>
		/// Gets or sets the PurchaseRequisition value.
		/// </summary>
		public bool? PurchaseRequisition { get; set; }

		/// <summary>
		/// Gets or sets the JobManagement value.
		/// </summary>
		public bool? JobManagement { get; set; }

		/// <summary>
		/// Gets or sets the AssetSetup value.
		/// </summary>
		public bool? AssetSetup { get; set; }

		/// <summary>
		/// Gets or sets the Employee/User value.
		/// </summary>
		public bool? Employee_User { get; set; }

		/// <summary>
		/// Gets or sets the CleaningSetup value.
		/// </summary>
		public bool? CleaningSetup { get; set; }

		/// <summary>
		/// Gets or sets the PurchaseSetup value.
		/// </summary>
		public bool? PurchaseSetup { get; set; }

        /// <summary>
        /// Gets or sets the SLA value.
        /// </summary>
        public bool? SLA { get; set; }

		/// <summary>
		/// Gets or sets the NotificationSetup value.
		/// </summary>
		public bool? NotificationSetup { get; set; }

        /// <summary>
        /// Gets or sets the Item Requisition value.
        /// </summary>
        public bool? ItemRequisition { get; set; }

        public bool Help { get; set; }

		#endregion
	}
}
