//-----------------------------------------------------------------------
// <copyright file="Dbo_cleaninginspectiondetails_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_cleaninginspectiondetails_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_cleaninginspectiondetails_CT")]
    public sealed class Dbo_cleaninginspectiondetails_CT : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the __$start_lsn value.
        /// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        public string LocationDescription { get; set; }

        public string GroupDesc { get; set; }
        ///// <summary>
        ///// Gets or sets the CleaningInspectionDetailId value.
        ///// </summary>
        //public int? CleaningInspectionDetailId { get; set; }

        ///// <summary>
        ///// Gets or sets the CleaningInspectionId value.
        ///// </summary>
        //public int? CleaningInspectionId { get; set; }

        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the InspectionDate value.
        /// </summary>
        public DateTime? InspectionDate { get; set; }

        /// <summary>
        /// Gets or sets the CleaningGroupId value.
        /// </summary>
        //public int? CleaningGroupId { get; set; }

        /// <summary>
        /// Gets or sets the Floor value.
        /// </summary>
        public int? Floor { get; set; }

        /// <summary>
        /// Gets or sets the Wall value.
        /// </summary>
        public int? Wall { get; set; }

        /// <summary>
        /// Gets or sets the Window value.
        /// </summary>
        public int? Window { get; set; }

        /// <summary>
        /// Gets or sets the Ceiling value.
        /// </summary>
        public int? Ceiling { get; set; }

        /// <summary>
        /// Gets or sets the Furniture value.
        /// </summary>
        public int? Furniture { get; set; }

        /// <summary>
        /// Gets or sets the WasteBag value.
        /// </summary>
        public int? WasteBag { get; set; }

        /// <summary>
        /// Gets or sets the Consumables value.
        /// </summary>
        public int? Consumables { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(2000, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the ArabicRemarks value.
        /// </summary>
        [StringLength(2000, ErrorMessage = "*")]
        public string ArabicRemarks { get; set; }

        /// <summary>
        /// Gets or sets the TotalPercentage value.
        /// </summary>
        public float? TotalPercentage { get; set; }

        /// <summary>
        /// Gets or sets the SatisfactionResult value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string SatisfactionResult { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }



        #endregion
    }
}
