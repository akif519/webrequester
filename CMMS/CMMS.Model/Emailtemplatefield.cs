//-----------------------------------------------------------------------
// <copyright file="Emailtemplatefield.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailtemplatefield
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailtemplatefield")]
	public sealed class Emailtemplatefield : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the EmailTemplateFieldID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int EmailTemplateFieldID { get; set; }

		/// <summary>
		/// Gets or sets the EmailTemplateFieldDisplayName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string EmailTemplateFieldDisplayName { get; set; }

		/// <summary>
		/// Gets or sets the EmailTemplateDBFieldName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string EmailTemplateDBFieldName { get; set; }

		/// <summary>
		/// Gets or sets the FieldType value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(10, ErrorMessage = "*")]
		public string FieldType { get; set; }

		#endregion
	}
}
