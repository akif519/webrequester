//-----------------------------------------------------------------------
// <copyright file="Dbo_stockcode_levels_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_stockcode_levels_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_stockcode_levels_CT")]
	public sealed class Dbo_stockcode_levels_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        [NotMapped]
        public string SubStoreCode { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string PartLocation { get; set; }

        /// <summary>
        /// Gets or sets the Max_level value.
        /// </summary>
        public decimal? Max_level { get; set; }

        /// <summary>
        /// Gets or sets the Re_order_level value.
        /// </summary>
        public decimal? Re_order_level { get; set; }

        /// <summary>
        /// Gets or sets the Min_level value.
        /// </summary>
        public decimal? Min_level { get; set; }

        /// <summary>
        /// Gets or sets the Reorder_qty value.
        /// </summary>
        public decimal? Reorder_qty { get; set; }

        /// <summary>
        /// Gets or sets the Balance value.
        /// </summary>
        public decimal? Balance { get; set; }

		/// <summary>
		/// Gets or sets the __$operation value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int operation { get; set; }

		

        ///// <summary>
        ///// Gets or sets the Stock_id value.
        ///// </summary>
        //public int? Stock_id { get; set; }

        ///// <summary>
        ///// Gets or sets the Sub_store_id value.
        ///// </summary>
        //public int? Sub_store_id { get; set; }

        ///// <summary>
        ///// Gets or sets the PartLocationID value.
        ///// </summary>
        //public int? PartLocationID { get; set; }

		

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

		#endregion
	}
}
