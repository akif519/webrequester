//-----------------------------------------------------------------------
// <copyright file="DashboardReportMenu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - DashboardReportMenu
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("DashboardReportMenu")]
	public sealed class DashboardReportMenu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the DashboardRepotMenuID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int DashboardRepotMenuID { get; set; }

		/// <summary>
		/// Gets or sets the ReportUniqueName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string ReportUniqueName { get; set; }

		/// <summary>
		/// Gets or sets the UserID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int UserID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the type of the chart.
        /// </summary>
        /// <value>
        /// The type of the chart.
        /// </value>
        public int ChartType { get; set; }

        /// <summary>
        /// Gets or sets the display order.
        /// </summary>
        /// <value>
        /// The display order.
        /// </value>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the filter by.
        /// </summary>
        /// <value>
        /// The filter by.
        /// </value>
        public string FilterBy { get; set; }

        /// <summary>
        /// Gets or sets the jo start date.
        /// </summary>
        /// <value>
        /// The jo start date.
        /// </value>
        public DateTime? JOStartDate { get; set; }

        /// <summary>
        /// Gets or sets the jo end date.
        /// </summary>
        /// <value>
        /// The jo end date.
        /// </value>
        public DateTime? JOEndDate { get; set; }

        /// <summary>
        /// Gets or sets the date picker identifier.
        /// </summary>
        /// <value>
        /// The date picker identifier.
        /// </value>
        public int? DatePickerID { get; set; }

		#endregion
	}
}
