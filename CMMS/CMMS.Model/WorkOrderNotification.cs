﻿//-----------------------------------------------------------------------
// <copyright file="WorkOrderNotification.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - WorkOrderNotification
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>10-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkOrderNotification")]
    public sealed class WorkOrderNotification : BaseModel
    {
        #region Properties


        /// <summary>
        /// Gets or sets the WorkOrderNotificationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkOrderNotificationID { get; set; }

        /// <summary>
        /// Gets or sets the WOWRNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WOWRNo { get; set; }

        /// <summary>
        /// Gets or sets the WRWO value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int WRWO { get; set; }

        /// <summary>
        /// Gets or sets the NotificationTypeID value.
        /// </summary>
        public int? NotificationTypeID { get; set; }       

        /// <summary>
        /// Gets or sets the RelevantTypeID value.
        /// </summary>
        public int? RelevantTypeID { get; set; }

        /// <summary>
        /// Gets or sets the Title value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsActive is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsRead or not  
        /// 0 for unread
        /// 1 for read
        /// </summary>
        public bool IsRead { get; set; }


        /// <summary>
        /// Gets or sets the Posted date.
        /// </summary>        
        /// 
        [NotMapped]
        public string Postedon { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [NotMapped]
        public string NotificationTypeDesc { get; set; }

        #endregion
    }

    public class WorkOrderNotificationExportExcel 
    {
        public string WOWRNo { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string NotificationType { get; set; }
    }
}
