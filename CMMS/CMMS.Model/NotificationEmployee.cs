﻿//-----------------------------------------------------------------------
// <copyright file="NotificationEmployee.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - NotificationEmployee
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>11-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("NotificationEmployee")]
    public sealed class NotificationEmployee : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationEmployeeID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationEmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationTypeID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int NotificationTypeID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }


        /// <summary>
        /// Gets or sets the Employee Name value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Employee No value.
        /// </summary>
        public string EmployeeNO { get; set; }

        #endregion
    }
}
