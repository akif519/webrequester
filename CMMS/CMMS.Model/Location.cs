﻿//-----------------------------------------------------------------------
// <copyright file="Location.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - location
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("location")]
    public sealed class Location : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LocationID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L4ID value.
        /// </summary>
        public int? L4ID { get; set; }

        /// <summary>
        /// Gets or sets the L5ID value.
        /// </summary>
        public int? L5ID { get; set; }

        /// <summary>
        /// Gets or sets the CriticalityID value.
        /// </summary>
        public int? CriticalityID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the NotetoTech value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string NotetoTech { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the CleaningClassificationId value.
        /// </summary>
        public int? CleaningClassificationId { get; set; }

        [NotMapped]
        public string L2Code { get; set; }  //City-Site
        [NotMapped]
        public string L4No { get; set; } //Zone
        [NotMapped]
        public string L4Description { get; set; }
        [NotMapped]
        public string L4AltDescription { get; set; }
        [NotMapped]
        public string L5No { get; set; }  //Building
        [NotMapped]
        public string L5Description { get; set; }
        [NotMapped]
        public string L5AltDescription { get; set; }
        [NotMapped]
        public string Criticality { get; set; }
        [NotMapped]
        public string SectorCode { get; set; }
        [NotMapped]
        public string SectorName { get; set; }
        [NotMapped]
        public string SectorAltName { get; set; }

        [NotMapped]
        public int AssetsCount { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        [NotMapped]
        public string L3No { get; set; }

        [NotMapped]
        public string L3Desc { get; set; }

        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the location type identifier.
        /// </summary>
        /// <value>
        /// The location type identifier.
        /// </value>
        public int? LocationTypeID { get; set; }

        /// <summary>
        /// Gets or sets the location type code.
        /// </summary>
        /// <value>
        /// The location type code.
        /// </value>
        [NotMapped]
        public string LocationTypeCode { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary> 
        [AllowNullForSave]
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeNO value.
        /// </summary>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or Sets Location Authorised Employee Name.
        /// </summary>
        [NotMapped]
        public string LocationAuthEmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the RequestorName value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the AltName value.
        /// </summary>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or sets the location Longitude .
        /// </summary>
        /// <value>
        /// The location Longitude.
        /// </value>
        /// 
        [AllowNullForSave]
        public decimal Longitude { get; set; }

        /// <summary>
        /// Gets or sets the Latitude Longitude .
        /// </summary>
        /// <value>
        /// The location Latitude.
        /// </value>
        [AllowNullForSave]
        public decimal Latitude { get; set; }

        [NotMapped]
        public int? L1ID { get; set; }

        [NotMapped]
        public bool IsAreaEnableOrgWise { get; set; }

        [NotMapped]
        public bool LocationHasReferences { get; set; }
        #endregion
    }
}
