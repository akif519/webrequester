//-----------------------------------------------------------------------
// <copyright file="Dbo_issue_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_issue_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_issue_CT")]
    public sealed class Dbo_issue_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }


        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string StockDescription { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }

        [NotMapped]
        public decimal? AvgPrice { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the DateIssue value.
        /// </summary>
        public string DateIssue { get; set; }

        /// <summary>
        /// Gets or sets the QtyIssue value.
        /// </summary>
        public decimal? QtyIssue { get; set; }

        /// <summary>
        /// Gets or sets the Price3 value.
        /// </summary>
        public decimal? Price3 { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the IssueID value.
        ///// </summary>
        //public int? IssueID { get; set; }

        ///// <summary>
        ///// Gets or sets the StockID value.
        ///// </summary>
        //public int? StockID { get; set; }

        ///// <summary>
        ///// Gets or sets the LocationID value.
        ///// </summary>
        //public int? LocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the SubStoreID value.
        ///// </summary>
        //public int? SubStoreID { get; set; }

        ///// <summary>
        ///// Gets or sets the ToSubStoreID value.
        ///// </summary>
        //public int? ToSubStoreID { get; set; }

        ///// <summary>
        ///// Gets or sets the ToL2ID value.
        ///// </summary>
        //public int? ToL2ID { get; set; }



        ///// <summary>
        ///// Gets or sets the Description value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string Description { get; set; }



        ///// <summary>
        ///// Gets or sets the IWTNo value.
        ///// </summary>
        //[StringLength(255, ErrorMessage = "*")]
        //public string IWTNo { get; set; }

        ///// <summary>
        ///// Gets or sets the Warehouse value.
        ///// </summary>
        //[StringLength(255, ErrorMessage = "*")]
        //public string Warehouse { get; set; }

        ///// <summary>
        ///// Gets or sets the UoM value.
        ///// </summary>
        //[StringLength(255, ErrorMessage = "*")]
        //public string UoM { get; set; }





        ///// <summary>
        ///// Gets or sets the BatchNo value.
        ///// </summary>
        //[StringLength(30, ErrorMessage = "*")]
        //public string BatchNo { get; set; }

        ///// <summary>
        ///// Gets or sets the IsTransfer value.
        ///// </summary>
        //public short? IsTransfer { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }



        #endregion
    }
}
