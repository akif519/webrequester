//-----------------------------------------------------------------------
// <copyright file="CmProjectStatu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cmProjectStatus
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>24-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cmProjectStatus")]
	public sealed class CmProjectStatu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ProjectStatusId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ProjectStatusId { get; set; }

		/// <summary>
		/// Gets or sets the StatusName value.
		/// </summary>
		[StringLength(200, ErrorMessage = "*")]
		public string StatusName { get; set; }

		/// <summary>
		/// Gets or sets the StatusAltName value.
		/// </summary>
		[StringLength(200, ErrorMessage = "*")]
		public string StatusAltName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
