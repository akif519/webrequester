﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - Employees_MaintSubDept
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Employees_MaintSubDept")]
    public sealed class Employees_MaintSubDept : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        [Required(ErrorMessage = "*")]        
        public int MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the EmpId value.
        /// </summary>
        [Required(ErrorMessage = "*")]        
        public int EmpId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the IsInserted value.
        /// </summary>
        [NotMapped]
        public bool IsInserted { get; set; }

        #endregion
    }
}
