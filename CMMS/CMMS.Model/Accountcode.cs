//-----------------------------------------------------------------------
// <copyright file="Accountcode.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - accountcode
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("accountcode")]
    public sealed class Accountcode : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AccCodeid value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AccCodeid { get; set; }

        /// <summary>
        /// Gets or sets the AccountCode value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string AccountCode { get; set; }

        /// <summary>
        /// Gets or sets the AccountCodeDesc value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AccountCodeDesc { get; set; }

        /// <summary>
        /// Gets or sets the AltAccountCodeDesc value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltAccountCodeDesc { get; set; }

        /// <summary>
        /// Gets or sets the CostCenterId value.
        /// </summary>
        public int? CostCenterId { get; set; }

        /// <summary>
        /// Gets or sets the DateStart value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? DateStart { get; set; }

        /// <summary>
        /// Gets or sets the DateEnd value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? DateEnd { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the cost center no.
        /// </summary>
        /// <value>
        /// The cost center no.
        /// </value>
        [NotMapped]
        public string CostCenterNo { get; set; }

        [NotMapped]
        public string CostCenterName { get; set; }

        [NotMapped]
        public string AltCostCenterName { get; set; }

        /// <summary>
        /// Gets or sets the is enable cost center.
        /// </summary>
        /// <value>
        /// The is enable cost center.
        /// </value>
        [NotMapped]
        public bool? IsEnableCostCenter { get; set; }

        #endregion
    }
}
