﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    [Table("DatabaseType")]
    public sealed class DatabaseType : BaseModel
    {
        public int DbTypeID { get; set; }

        public string DbTypeName { get; set; }

        public int DbTypeValue { get; set; }
    }
}
