//-----------------------------------------------------------------------
// <copyright file="IRApprovalLevelMapping.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - IRApprovalLevelMappings
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("IRApprovalLevelMappings")]
    public sealed class IRApprovalLevelMapping : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AutoId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoId { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SubStoreId { get; set; }

        /// <summary>
        /// Gets or sets the IRApprovalLevelId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int IRApprovalLevelId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Mandatory is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool Mandatory { get; set; }

        /// <summary>
        /// Gets or sets the Auth_status_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Auth_status_id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsPrintingEnable is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsPrintingEnable { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2 identifier.
        /// </summary>
        /// <value>
        /// The l2 identifier.
        /// </value>
        [NotMapped]
        public int L2ID { get; set; }

        [NotMapped]
        public int L1ID { get; set; }

        /// <summary>
        /// Gets or sets the sub store code.
        /// </summary>
        /// <value>
        /// The sub store code.
        /// </value>
        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the level.
        /// </summary>
        /// <value>
        /// The name of the level.
        /// </value>
        [NotMapped]
        public string LevelName { get; set; }

        [NotMapped]
        public string AltLevelName { get; set; }

        /// <summary>
        /// Gets or sets the level no.
        /// </summary>
        /// <value>
        /// The level no.
        /// </value>
        [NotMapped]
        public int LevelNo { get; set; }

        /// <summary>
        /// Gets or sets the auth_status_desc.
        /// </summary>
        /// <value>
        /// The auth_status_desc.
        /// </value>
        [NotMapped]
        public string auth_status_desc { get; set; }

        #endregion
    }
}
