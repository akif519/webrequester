//-----------------------------------------------------------------------
// <copyright file="PRApprovalLevelMappings_employee.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - PRApprovalLevelMappings_employees
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("PRApprovalLevelMappings_employees")]
	public sealed class PRApprovalLevelMappings_employee : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the AutoId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int AutoId { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeId { get; set; }

		#endregion
	}
}
