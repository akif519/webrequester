﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class EmployeeDocuments 
    {
        public Guid Guid_DocID { get; set; }
        public string kendoDocgriddata { get; set; }
        public Cmemployeedocument employeedocs { get; set; }
        public List<clsextassetfiles> lstAttachment { get; set; }
    }

    public class clsextassetfiles : CmAttachment
    {
        public Guid guidId { get; set; }
    }
}
