﻿//-----------------------------------------------------------------------
// <copyright file="Pmmeter.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - pmmeters
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("pmmeters")]
    public sealed class PmMeter : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the MeterID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MeterID { get; set; }

        /// <summary>
        /// Gets or sets the MeterMasterID value.
        /// </summary>
        public int? MeterMasterID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the PMLowTaskID value.
        /// </summary>
        public int? PMLowTaskID { get; set; }

        /// <summary>
        /// Gets or sets the PMUppTaskID value.
        /// </summary>
        public int? PMUppTaskID { get; set; }

        /// <summary>
        /// Gets or sets the PMIncTaskID value.
        /// </summary>
        public int? PMIncTaskID { get; set; }

        /// <summary>
        /// Gets or sets the NextPMIncTaskID value.
        /// </summary>
        public int? NextPMIncTaskID { get; set; }

        /// <summary>
        /// Gets or sets the Workpriorityid value.
        /// </summary>
        public int? Workpriorityid { get; set; }

        /// <summary>
        /// Gets or sets the Worktradeid value.
        /// </summary>
        public int? Worktradeid { get; set; }

        /// <summary>
        /// Gets or sets the MeterNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string MeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltMeterDescription value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string AltMeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the MeterType value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterType { get; set; }

        /// <summary>
        /// Gets or sets the Lowerlimit value.
        /// </summary>
        public float? Lowerlimit { get; set; }

        /// <summary>
        /// Gets or sets the UpperLimit value.
        /// </summary>
        public float? UpperLimit { get; set; }

        /// <summary>
        /// Gets or sets the PMLimit value.
        /// </summary>
        public int? PMLimit { get; set; }

        /// <summary>
        /// Gets or sets the LastPMreading value.
        /// </summary>
        public float? LastPMreading { get; set; }

        /// <summary>
        /// Gets or sets the Units value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Units { get; set; }

        /// <summary>
        /// Gets or sets the StartMeterReading value.
        /// </summary>
        public float? StartMeterReading { get; set; }

        /// <summary>
        /// Gets or sets the IncFreq value.
        /// </summary>
        public float? IncFreq { get; set; }

        /// <summary>
        /// Gets or sets the IncPMDue value.
        /// </summary>
        public float? IncPMDue { get; set; }

        /// <summary>
        /// Gets or sets the PeriodDays value.
        /// </summary>
        public int? PeriodDays { get; set; }

        /// <summary>
        /// Gets or sets the FreqUnits value.
        /// </summary>
        public int? FreqUnits { get; set; }

        /// <summary>
        /// Gets or sets the PMType value.
        /// </summary>
        public int? PMType { get; set; }

        /// <summary>
        /// Gets or sets the Frequency value.
        /// </summary>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the StartDate value.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetStartDate value.
        /// </summary>
        public DateTime? TargetStartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetCompDate value.
        /// </summary>
        public DateTime? TargetCompDate { get; set; }

        /// <summary>
        /// Gets or sets the NextDate value.
        /// </summary>
        public DateTime? NextDate { get; set; }

        /// <summary>
        /// Gets or sets the PMCounter value.
        /// </summary>
        public int? PMCounter { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the TimeBased value.
        /// </summary>
        public int? TimeBased { get; set; }

        /// <summary>
        /// Gets or sets the ActCompPMReading value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ActCompPMReading { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the work trade.
        /// </summary>
        /// <value>
        /// The work trade.
        /// </value>
        [NotMapped]
        public string WorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the alt work trade.
        /// </summary>
        /// <value>
        /// The alt work trade.
        /// </value>
        [NotMapped]
        public string AltWorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the maint division code.
        /// </summary>
        /// <value>
        /// The maint division code.
        /// </value>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the maint dept code.
        /// </summary>
        /// <value>
        /// The maint dept code.
        /// </value>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the maint sub dept code.
        /// </summary>
        /// <value>
        /// The maint sub dept code.
        /// </value>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the MeterMasterNo value.
        /// </summary>
        [NotMapped]
        public string MeterMasterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterMasterDescription value.
        /// </summary>
        [NotMapped]
        public string MeterMasterDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltMeterMasterDescription value.
        /// </summary>
        [NotMapped]
        public string AltMeterMasterDescription { get; set; }

        /// <summary>
        /// Gets or sets the MeterMasterUnit value.
        /// </summary>
        [NotMapped]
        public string MeterMasterUnit { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>
        [NotMapped]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [NotMapped]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [NotMapped]
        public string AssetAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the LastReading value.
        /// </summary>
        [MaxLength(8, ErrorMessage = "*")]
        public float? LastReadingMaster { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [NotMapped]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string CheckListName { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string AltCheckListName { get; set; }

        [NotMapped]
        public string MasterMeterType { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [NotMapped]
        public string ChecklistNo1 { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string CheckListName1 { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string AltCheckListName1 { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [NotMapped]
        public string ChecklistNo2 { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string CheckListName2 { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [NotMapped]
        public string AltCheckListName2 { get; set; }

        [NotMapped]
        public bool boolPMActive
        {
            get
            {
                if (this.PMActive.HasValue)
                {
                    return Convert.ToBoolean(this.PMActive);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.PMActive = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public string WorkTradeName { get; set; }

        #endregion
    }
}
