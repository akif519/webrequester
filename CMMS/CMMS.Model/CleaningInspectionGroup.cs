﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cleaningInspectionGroup
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cleaningInspectionGroup")]
    public sealed class CleaningInspectionGroup : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CleaningInspectionGroupId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CleaningInspectionGroupId { get; set; }

        /// <summary>
        /// Gets or sets the GroupNumber value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessage = "*")]
        public string GroupNumber { get; set; }

        /// <summary>
        /// Gets or sets the GroupDescription value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(500, ErrorMessage = "*")]
        public string GroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the ArabicGroupDescription value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(500, ErrorMessage = "*")]
        public string ArabicGroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the Type value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the TotalArea value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(200, ErrorMessage = "*")]
        public string TotalArea { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }

}
