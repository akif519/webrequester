//-----------------------------------------------------------------------
// <copyright file="Dbo_extassetfiles_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_extassetfiles_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_extassetfiles_CT")]
	public sealed class Dbo_extassetfiles_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        [NotMapped]
        public string No { get; set; }

        /// <summary>
        /// Gets or sets the FileName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string FileName { get; set; }


        /// <summary>
        /// Gets or sets the FileDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FileDescription { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the AutoId value.
        ///// </summary>
        //public int? AutoId { get; set; }

        ///// <summary>
        ///// Gets or sets the FkId value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string FkId { get; set; }

        ///// <summary>
        ///// Gets or sets the FileLink value.
        ///// </summary>
        //[StringLength(4000, ErrorMessage = "*")]
        //public string FileLink { get; set; }

        ///// <summary>
        ///// Gets or sets the ModuleType value.
        ///// </summary>
        //[StringLength(1, ErrorMessage = "*")]
        //public string ModuleType { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //public int? CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

		#endregion
	}
}
