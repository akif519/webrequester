﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class DashboardChart
    {
        public string CategoryEnglish { get; set; }
        public string CategoryArabic { get; set; }
        public decimal TotalCount { get; set; }
    }

    public class NoOfJobOrderByTypeDashboardChart
    {
        public string CategoryEnglish { get; set; }
        public string CategoryArabic { get; set; }
        public decimal JOCount { get; set; }
        public string MonthYear { get; set; }        
    }

    public class JobTradingDashboardChart
    {
        public int WorkTypeID { get; set; }
        public int DateYear { get; set; }
        public int DateMonth { get; set; }
        public string CategoryEnglish { get; set; }
        public string CategoryArabic { get; set; }
        public decimal JOCount { get; set; }
        public string MonthYear { get; set; }
    }

    public class ColumnChartModel
    {
        public string MonthYear { get; set; }
        public int DateMonth { get; set; }
        public int DateYear { get; set; }
        public string category { get; set; }
        public decimal value { get; set; }
        public decimal Pervalue { get; set; }
        public string color { get; set; }
    }

    public class PIChartModel
    {
        public string category { get; set; }
        public decimal value { get; set; }
        public decimal Pervalue { get; set; }
        public string color { get; set; }
    }

    public class BARChartModel
    {
        public String Year { get; set; }
        public decimal ColA { get; set; }
        public decimal ColB { get; set; }
        public decimal ColC { get; set; }
        public decimal ColD { get; set; }
        public decimal ColE { get; set; }
        public decimal ColF { get; set; }

    }

    public class JobOrderStatusBySite
    {
        public int L2ID { get; set; }
        public string L2name { get; set; }
        public string L2Altname { get; set; }
        public int WorkTypeID { get; set; }
        public string WorkTypeDescription { get; set; }
        public string AltWorkTypeDescription { get; set; }
        public int CountOutstanding { get; set; }
        public int CountClosed { get; set; }
    }

    public class Top10AssetBreakdowns
    {
        public int RowNo { get; set; }
        public int AssetID { get; set; }
        public string AssetDescription { get; set; }
        public string AssetAltDescription { get; set; }
        public string AssetNumber { get; set; }
        public string L2Code { get; set; }
        public string L3No { get; set; }
        public string L4No { get; set; }
        public string L5No { get; set; }
        public string LocationNo { get; set; }
        public int TotalCount { get; set; }
    }

    public class DashboardModule
    {
        public bool? PMJobOrderStatus { get; set; }
        public bool? JobOrderStatus { get; set; }
        public bool? JobOrderTypeDistribution { get; set; }
        public bool? PMCompilance { get; set; }
        public bool? Top10AssetBreakdown { get; set; }
        public bool? Top10AssetDowntime { get; set; }
        public bool? JobOrderAndRequestTrending { get; set; }
        public bool? NoOfJobOrdersByType { get; set; }
        public bool? JobOrderStatusBySite { get; set; }
        public bool? JobPriorityDistribution { get; set; }
        public bool? JobTradeDistribution { get; set; }
        public bool? OutstandingJobsByPriority { get; set; }
        public bool? OutstandingJobsByTrade { get; set; }
        public bool? OutstandingJobsByType { get; set; }


        public DashboardModule()
        {
            PMJobOrderStatus = false;
            JobOrderStatus = false;
            JobOrderTypeDistribution = false;
            PMCompilance = false;
            Top10AssetBreakdown = false;
            Top10AssetDowntime = false;
            JobOrderAndRequestTrending = false;
            NoOfJobOrdersByType = false;
            JobOrderStatusBySite = false;
            JobPriorityDistribution = false;
            JobTradeDistribution = false;
            OutstandingJobsByPriority = false;
            OutstandingJobsByTrade = false;
            OutstandingJobsByType = false;
        }
    }

    public class ReportBindJobOrder
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public string JobTypeID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
    }

    public class JobOrderStatusBySiteSearchData
    {
        public int DashboardRepotMenuID { get; set; }
        public int L1ID { get; set; }
        public int reportID { get; set; }
        public string L2ID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
    }

    public class JobOrderTypeChart
    {
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
        [NotMapped]
        public string L2Code { get; set; }
    }

    public class TopTenAssetModal
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public string LocationID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
        [NotMapped]
        public string LocationNo { get; set; }
    }


    public class PMComplianceModal
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public string JobTradeID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
    }

    public class JobOrderTypeChartFilterModal
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
        [NotMapped]
        public string L2Code { get; set; }
    }

    public class JobTradeingModal
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public string JobTradeID { get; set; }
        public string JobTypeID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
    }


    public class WorkReportModal
    {
        public int ID { get; set; }
        public string Tasks { get; set; }
        public int Total { get; set; }
        public int Open { get; set; }
        public int Close { get; set; }
        public int Cancel { get; set; }
        public decimal OpenP { get; set; }
        public decimal CloseP { get; set; }
        public decimal CancelP { get; set; }
    }

    public class WorkReportFilterModal
    {
        public int DashboardRepotMenuID { get; set; }
        public int reportID { get; set; }
        public int L1ID { get; set; }
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public string JobTradeID { get; set; }
        public string JobTypeID { get; set; }
        public DateTime JoDateFrom { get; set; }
        public DateTime JoDateTo { get; set; }
        public int DatePickerID { get; set; }
    }

    public class WorkStatusDetailModal
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public DateTime ReceiveDate { get; set; }
        public string L2Code { get; set; }
        public string WorkStatus { get; set; }
    }
}
