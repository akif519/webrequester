﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - tbl_LanguageSelection
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("tbl_LanguageSelection")]
    public sealed class Tbl_LanguageSelection : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the LanguageCode value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(10, ErrorMessage = "*")]
        public string LanguageCode { get; set; }

        /// <summary>
        /// Gets or sets the Language value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the FlowDirection value.
        /// </summary>
        public bool? FlowDirection { get; set; }

        /// <summary>
        /// Gets or sets the DefaultLanguage value.
        /// </summary>
        public bool? DefaultLanguage { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }

}
