﻿//-----------------------------------------------------------------------
// <copyright file="MobileSession.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using CMMS.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - mobileSession
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>16-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("mobileSession")]
    public sealed class mobilesession : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the SessionId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long SessionId { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the Logintime value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public DateTime Logintime { get; set; }

		/// <summary>
		/// Gets or sets the Lastheartbeat value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public DateTime Lastheartbeat { get; set; }

		#endregion
	}
}
