﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - workorderlabor
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("workorderlabor")]
    public sealed class Workorderlabor : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkorderLaborID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkorderLaborID { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the Employee NO value.
        /// </summary>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Employee Name value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Employee Name value.
        /// </summary>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or sets the HourlySalary value.
        /// </summary>
        public decimal? HourlySalary { get; set; }

        /// <summary>
        /// Gets or sets the Comment value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the OverTime1 value.
        /// </summary>
        public decimal? OverTime1 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime2 value.
        /// </summary>
        public decimal? OverTime2 { get; set; }

        /// <summary>
        /// Gets or sets the OverTime3 value.
        /// </summary>
        public decimal? OverTime3 { get; set; }

        /// <summary>
        /// Gets or sets the StartDate value.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the StartTime value.
        /// </summary>
        [NotMapped]
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Gets or sets the EndDate value.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the EndDate value.
        /// </summary>
        [NotMapped]
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Gets or sets the TotHour value.
        /// </summary>
        public float? TotHour { get; set; }

        /// <summary>
        /// Gets or sets the TotCost value.
        /// </summary>
        public decimal? TotCost { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }
        
        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }

}
