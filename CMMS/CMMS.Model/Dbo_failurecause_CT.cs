//-----------------------------------------------------------------------
// <copyright file="Dbo_failurecause_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_failurecause_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_failurecause_CT")]
    public sealed class Dbo_failurecause_CT : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }


        /// <summary>
        /// Gets or sets the FailureCauseCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string FailureCauseCode { get; set; }

        /// <summary>
        /// Gets or sets the FailureCauseDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FailureCauseDescription { get; set; }

        /// <summary>
        /// Gets or sets the FailureCauseAltDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string FailureCauseAltDescription { get; set; }

        ///// <summary>
        ///// Gets or sets the FailureCauseID value.
        ///// </summary>
        //public int? FailureCauseID { get; set; }


        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        #endregion
    }
}
