//-----------------------------------------------------------------------
// <copyright file="Substore.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - substore
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("substore")]
	public sealed class Substore : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the SubStoreID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SubStoreID { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the SubStoreCode value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string SubStoreCode { get; set; }

		/// <summary>
		/// Gets or sets the SubStoreDesc value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
		public string SubStoreDesc { get; set; }

		/// <summary>
		/// Gets or sets the AltSubStoreDesc value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltSubStoreDesc { get; set; }

		/// <summary>
		/// Gets or sets the IsDefault value.
		/// </summary>
		public bool? IsDefault { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the is enable city.
        /// </summary>
        /// <value>
        /// The is enable city.
        /// </value>
        [NotMapped]
        public bool? IsEnableCity { get; set; }

		#endregion
	}
}
