//-----------------------------------------------------------------------
// <copyright file="Tran.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - trans
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("trans")]
	public sealed class Tran : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the TransactionID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long TransactionID { get; set; }

		/// <summary>
		/// Gets or sets the PurchaseOrderItemsId value.
		/// </summary>
		public int? PurchaseOrderItemsId { get; set; }

		/// <summary>
		/// Gets or sets the Quantity value.
		/// </summary>
		public decimal? Quantity { get; set; }

		/// <summary>
		/// Gets or sets the TransactionType value.
		/// </summary>
		public short? TransactionType { get; set; }

		/// <summary>
		/// Gets or sets the Comments value.
		/// </summary>
		[StringLength(1000, ErrorMessage = "*")]
		public string Comments { get; set; }

		/// <summary>
		/// Gets or sets the InvoiceDate value.
		/// </summary>
		public DateTime? InvoiceDate { get; set; }

		/// <summary>
		/// Gets or sets the DeliveryOrderNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string DeliveryOrderNo { get; set; }

		/// <summary>
		/// Gets or sets the InvoiceNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string InvoiceNo { get; set; }

		/// <summary>
		/// Gets or sets the TransDate value.
		/// </summary>
		public DateTime? TransDate { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string StockDescription { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }

        [NotMapped]
        public string PartDescription { get; set; }

        [NotMapped]
        public decimal UnitPrice { get; set; }

        [NotMapped]
        public decimal TotalQuantity { get; set; } 

		#endregion
	}

    public class PoItemDetailModal
    {
        public DateTime? LastReceivedDate { get; set; }
        public decimal? TotalOrdered { get; set; }
        public decimal? TotalReceived { get; set; }
        public decimal? TotalCancelled { get; set; }
        public decimal? Balance { get; set; }
        public string Status { get; set; }
        public string OrderStatus { get; set; }

    }
}
