//-----------------------------------------------------------------------
// <copyright file="Configuration.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - configuration
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("configuration")]
    public sealed class Configuration : BaseModel
    {
        public Configuration()
        {
            this.message = new Message();
        }

        public Message message;

        public class Message
        {
            
        }

        #region Properties

        /// <summary>
        /// Gets or sets the Id value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Key value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessage = "*")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the Value value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessage = "*")]
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets the Module value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessage = "*")]
        public string Module { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string smtp_server { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        [NotMapped]
        public string smtp_server_port { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        [NotMapped]
        public bool uses_authentication { get; set; }
        
        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        [NotMapped]
        public bool uses_ssl { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        [NotMapped]
        public string smtp_username { get; set; }

        /// <summary>
        /// Gets or sets the smtp_password value.
        /// </summary>
        [NotMapped]
        public string smtp_password { get; set; }

        /// <summary>
        /// Gets or sets the SMS GateWay API
        /// </summary>
        [NotMapped]
        public string SMS_GateWay_API_URL { get; set; }

        [NotMapped]
        public string SMS_UserName { get; set; }

        [NotMapped]
        public string SMS_Password { get; set; }

        [NotMapped]
        public string SMS_ApiID { get; set; }

        [NotMapped]
        public string Sender_Name { get; set; } 

        #endregion
    }
}
