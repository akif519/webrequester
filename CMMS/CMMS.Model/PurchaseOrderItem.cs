﻿//-----------------------------------------------------------------------
// <copyright file="PurchaseOrderItem.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PurchaseOrderItems
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PurchaseOrderItems")]
    public sealed class PurchaseOrderItem : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseOrderID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PurchaseOrderID { get; set; }

        /// <summary>
        /// Gets or sets the PartNumber value.
        /// </summary>
        public int? PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the PartDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string PartDescription { get; set; }

        /// <summary>
        /// Gets or sets the UoM value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string UoM { get; set; }

        /// <summary>
        /// Gets or sets the Quantity value.
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the JobOrderID value.
        /// </summary>        
        public string JobOrderID { get; set; }

        /// <summary>
        /// Gets or sets the UnitPrice value.
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the Discount value.
        /// </summary>
        public decimal? Discount { get; set; }

        /// <summary>
        /// Gets or sets the Tax value.
        /// </summary>
        public decimal? Tax { get; set; }

        /// <summary>
        /// Gets or sets the LineCost value.
        /// </summary>
        public decimal? LineCost { get; set; }

        /// <summary>
        /// Gets or sets the BaseCost value.
        /// </summary>
        public decimal? BaseCost { get; set; }

        /// <summary>
        /// Gets or sets the Source value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestItemID value.
        /// </summary>
        public int? PurchaseRequestItemID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestNo value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string PurchaseRequestNo { get; set; }

        /// <summary>
        /// Gets or sets the purchase order  value.
        /// </summary>
        [NotMapped]
        public string PurchaseOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the Supplier Name value.
        /// </summary>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AltSupplier Name value.
        /// </summary>
        [NotMapped]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public DateTime? OrderedDate { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string DeliveryStatus { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string ItemNo { get; set; }

        /// <summary>
        /// Gets or sets the alt stock description.
        /// </summary>
        /// <value>
        /// The alt stock description.
        /// </value>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the guid_ document identifier.
        /// </summary>
        /// <value>
        /// The guid_ document identifier.
        /// </value>
        [NotMapped]
        public Guid Guid_ItemID { get; set; }
        #endregion
    }
}
