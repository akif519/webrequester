//-----------------------------------------------------------------------
// <copyright file="Dbo_multiplepm_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_multiplepm_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_multiplepm_CT")]
	public sealed class Dbo_multiplepm_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		
		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        [NotMapped]
        public string PMNo { get; set; }

        [NotMapped]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the MSeq value.
        /// </summary>
        public int? MSeq { get; set; }

        /// <summary>
        /// Gets or sets the MDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string MDescription { get; set; }

        /// <summary>
        /// Gets or sets the MStartDate value.
        /// </summary>
        public string MStartDate { get; set; }

        /// <summary>
        /// Gets or sets the MStartDateCount value.
        /// </summary>
        public int? MStartDateCount { get; set; }

        /// <summary>
        /// Gets or sets the MTaskSeq value.
        /// </summary>
        public int? MTaskSeq { get; set; }

        /// <summary>
        /// Gets or sets the MPMCounter value.
        /// </summary>
        public int? MPMCounter { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }


        ///// <summary>
        ///// Gets or sets the PMID value.
        ///// </summary>
        //public int? PMID { get; set; }

        ///// <summary>
        ///// Gets or sets the MTaskID value.
        ///// </summary>
        //public int? MTaskID { get; set; }

        ///// <summary>
        ///// Gets or sets the MScount value.
        ///// </summary>
        //public int? MScount { get; set; }


        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public string ModifiedDate { get; set; }

		#endregion
	}
}
