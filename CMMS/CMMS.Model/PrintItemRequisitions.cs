﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public partial class PrintItemRequisitions
    {
        public string Rowno { get; set; }
        public string IssuanceStatus { get; set; }
        public string WorkorderNo { get; set; }
        public string AssetNumber { get; set; }
        public decimal Qty { get; set; }
        public string uom { get; set; }
        public string part_desc { get; set; }
        public string Altpart_desc { get; set; }
        public string StockNo { get; set; }
        public string MaintdivId { get; set; }
        public string MaintdeptId { get; set; }
        public string SubWHIssueTechId { get; set; }
        public string SubWHSupId { get; set; }
        public string mr_no { get; set; }
        public DateTime mr_date { get; set; }
        public DateTime order_date { get; set; }
        public string l2code { get; set; }
        public string MaintDeptCode { get; set; }
        public string MaintDeptdesc { get; set; }
        public string MaintDeptAltdesc { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDivisionCode { get; set; }
        public string MaintDivisionAltName { get; set; }
        public string SubStoreCode { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public string RequestedBy { get; set; }        
    }
}
