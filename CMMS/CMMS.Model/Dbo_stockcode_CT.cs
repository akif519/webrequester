//-----------------------------------------------------------------------
// <copyright file="Dbo_stockcode_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_stockcode_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_stockcode_CT")]
	public sealed class Dbo_stockcode_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Specification value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Specification { get; set; }

        /// <summary>
        /// Gets or sets the Manufacturer value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the CriticalityID value.
        /// </summary>
        public bool? CriticalityID { get; set; }

        [NotMapped]
        public string AttrName { get; set; }

        [NotMapped]
        public string AttrSubName { get; set; }

        /// <summary>
        /// Gets or sets the UOM value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string UOM { get; set; }



        /// <summary>
        /// Gets or sets the Price2 value.
        /// </summary>
        public decimal? Price2 { get; set; }

        /// <summary>
        /// Gets or sets the Notes value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets the LeadTime value.
        /// </summary>
        public int? LeadTime { get; set; }

        /// <summary>
        /// Gets or sets the AvgPrice value.
        /// </summary>
        public decimal? AvgPrice { get; set; }

        ///// <summary>
        ///// Gets or sets the StockID value.
        ///// </summary>
        //public int? StockID { get; set; }

		

        ///// <summary>
        ///// Gets or sets the AttrGroupID value.
        ///// </summary>
        //public int? AttrGroupID { get; set; }

        ///// <summary>
        ///// Gets or sets the AttrSubID value.
        ///// </summary>
        //public int? AttrSubID { get; set; }

        ///// <summary>
        ///// Gets or sets the Default_supplier_id value.
        ///// </summary>
        //public int? Default_supplier_id { get; set; }

	    ///// <summary>
        ///// Gets or sets the ANV value.
        ///// </summary>
        //public bool? ANV { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
        public string ModifiedDate { get; set; }

		#endregion
	}
}
