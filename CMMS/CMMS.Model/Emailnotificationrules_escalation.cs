//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_escalation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_escalation
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_escalation")]
	public sealed class Emailnotificationrules_escalation : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleEscalationID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleEscalationID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeID { get; set; }

		/// <summary>
		/// Gets or sets the EscalationTimeInMin value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EscalationTimeInMin { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the LastEmailSentTime value.
		/// </summary>
		public DateTime? LastEmailSentTime { get; set; }

        [NotMapped]
		public string EmployeeNO { get; set; }
        [NotMapped]
		public string Name { get; set; }
       
		#endregion
	}
}
