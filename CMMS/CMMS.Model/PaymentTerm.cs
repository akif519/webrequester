//-----------------------------------------------------------------------
// <copyright file="PaymentTerm.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - PaymentTerms
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("PaymentTerms")]
	public sealed class PaymentTerm : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ID { get; set; }

		/// <summary>
		/// Gets or sets the PaymentTermsDesc value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string PaymentTermsDesc { get; set; }

		/// <summary>
		/// Gets or sets the AltPaymentTermsDesc value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltPaymentTermsDesc { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
