﻿//-----------------------------------------------------------------------
// <copyright file="PurchaseRequestItem.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PurchaseRequestItems
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PurchaseRequestItems")]
    public sealed class PurchaseRequestItem : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestID value.
        /// </summary>
        public int PurchaseRequestID { get; set; }

        /// <summary>
        /// Gets or sets the PartNumber value.
        /// </summary>
        public int? PartNumber { get; set; }

        /// <summary>
        /// Gets or sets the PartDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string PartDescription { get; set; }

        /// <summary>
        /// Gets or sets the UoM value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string UoM { get; set; }

        /// <summary>
        /// Gets or sets the Quantity value.
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the Source value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the JoborderID value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string JoborderID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the Altpart_desc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Altpart_desc { get; set; }

        /// <summary>
        /// Gets or sets the ReceivedQty value.
        /// </summary>
        public decimal? ReceivedQty { get; set; }

        /// <summary>
        /// Gets or sets the CanceledQty value.
        /// </summary>
        public decimal? CanceledQty { get; set; }

        /// <summary>
        /// Gets or sets the SubWHSupId value.
        /// </summary>
        public int? SubWHSupId { get; set; }

        /// <summary>
        /// Gets or sets the SubWHIssueTechId value.
        /// </summary>
        public int? SubWHIssueTechId { get; set; }

        /// <summary>
        /// Gets or sets the order status value.
        /// </summary>
        [NotMapped]
        public string status_desc { get; set; }

        /// <summary>
        /// Gets or sets the altorder status value.
        /// </summary>
        [NotMapped]
        public string altstatus_desc { get; set; }

        /// <summary>
        /// Gets or sets the aprovel status value.
        /// </summary>
        [NotMapped]
        public string auth_status_desc { get; set; }

        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string Altauth_status_desc { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string ItemNo { get; set; }
                
        [NotMapped]
        public string AltStockDescription { get; set; }

        [NotMapped]
        public Guid Guid_ItemID { get; set; }

        [NotMapped]
        public string PurchaseRequestNo { get; set; }

        [NotMapped]
        public string RequestNo { get; set; }

        [AllowNullForSave]
        public decimal? RefPrice { get; set; }
        #endregion
    }
}
