﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cmApprovedProjectRequest
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmApprovedProjectRequest")]
    public sealed class CmApprovedProjectRequest : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the ProjectReqNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string ProjectReqNo { get; set; }

        /// <summary>
        /// Gets or sets the ProjectName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [NotMapped]
        public string ProjectName { get; set; }

        /// <summary>
        /// Gets or sets the AltProjectName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        [NotMapped]
        public string AltProjectName { get; set; }

        /// <summary>
        /// Gets or sets the ApprovedAmount value.
        /// </summary>
        public decimal? ApprovedAmount { get; set; }

        /// <summary>
        /// Gets or sets the ApprovalDate value.
        /// </summary>
        public DateTime? ApprovalDate { get; set; }

        /// <summary>
        /// Gets or sets the ExpiryDate value.
        /// </summary>
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the Classification value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Classification { get; set; }

        /// <summary>
        /// Gets or sets the StartDate value.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the ReqDate value.
        /// </summary>
        [NotMapped]
        public DateTime? ReqDate { get; set; }

        /// <summary>
        /// Gets or sets the RequestedAmount value.
        /// </summary>
        [NotMapped]
        public decimal? RequestedAmount { get; set; }

        /// <summary>
        /// Gets or sets the POLtrDate value.
        /// </summary>
        public DateTime? POLtrDate { get; set; }

        /// <summary>
        /// Gets or sets the POLtrNo value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string POLtrNo { get; set; }

        /// <summary>
        /// Gets or sets the ContractorID value.
        /// </summary>
        public int? ContractorID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the ContractTemplateID value.
        /// </summary>
        public int? ContractTemplateID { get; set; }

        /// <summary>
        /// Gets or sets the TenderClosingDate value.
        /// </summary>
        public DateTime? TenderClosingDate { get; set; }

        /// <summary>
        /// Gets or sets the TenderPublishDate value.
        /// </summary>
        public DateTime? TenderPublishDate { get; set; }

        /// <summary>
        /// Gets or sets the BOQVisibleTabIDs value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string BOQVisibleTabIDs { get; set; }

        /// <summary>
        /// Gets or sets the ProjectID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProjectID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the SectorID value.
        /// </summary>
        public int? SectorID { get; set; }

        /// <summary>
        /// Gets or sets the ProjectStatusId value.
        /// </summary>
        public int? ProjectStatusId { get; set; }

        #endregion
    }
}
