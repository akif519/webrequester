//-----------------------------------------------------------------------
// <copyright file="Material_req_statu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - material_req_status
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("material_req_status")]
	public sealed class Material_req_statu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the Mr_status_id value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Mr_status_id { get; set; }

		/// <summary>
		/// Gets or sets the Status_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Status_desc { get; set; }

		/// <summary>
		/// Gets or sets the Altstatus_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string Altstatus_desc { get; set; }

		#endregion
	}
}
