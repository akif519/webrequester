//-----------------------------------------------------------------------
// <copyright file="SubStoreStatu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - SubStoreStatus
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("SubStoreStatus")]
	public sealed class SubStoreStatu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the SubStoreStatusId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SubStoreStatusId { get; set; }

		/// <summary>
		/// Gets or sets the StatusName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string StatusName { get; set; }

		/// <summary>
		/// Gets or sets the StatusAltName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string StatusAltName { get; set; }

		/// <summary>
		/// Gets or sets the IsActive value.
		/// </summary>
		public bool? IsActive { get; set; }

		#endregion
	}
}
