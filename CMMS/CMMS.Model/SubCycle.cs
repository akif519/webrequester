﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
     [Table("SubCycleTbl")]
        public sealed class SubCycle : BaseModel
        {
            public int SubCycleID { get; set; }

            public string SubCycleName { get; set; }

            public int SubCycleValue { get; set; }
        }
   
}
