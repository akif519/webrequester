//-----------------------------------------------------------------------
// <copyright file="MaintGroup.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - MaintGroup
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("MaintGroup")]
	public sealed class MaintGroup : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the GroupID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int GroupID { get; set; }

		/// <summary>
		/// Gets or sets the GroupCode value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string GroupCode { get; set; }

		/// <summary>
		/// Gets or sets the GroupDesc value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string GroupDesc { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the GroupAltDesc value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string GroupAltDesc { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

		#endregion
	}
}
