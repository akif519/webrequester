﻿//-----------------------------------------------------------------------
// <copyright file="EmailNotification_Criticality.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - EmailNotification_Criticality
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>04-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("EmailNotification_Criticality")]
    public sealed class EmailNotification_Criticality : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationCriticalityID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationCriticalityID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the CriticalityID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int? CriticalityID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string Criticality { get; set; }

        [NotMapped]
        public string AltCriticality { get; set; }

        #endregion
    }
}
