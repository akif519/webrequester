﻿//-----------------------------------------------------------------------
// <copyright file="WorkRequestEscalation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - WorkRequestEscalation
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>09-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkRequestEscalation")]
    public sealed class WorkRequestEscalation : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkRequestEscalationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkRequestEscalationID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the RequestNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string RequestNo { get; set; }

        /// <summary>
        /// Gets or sets the EscalationTimeInMin value.
        /// </summary>
        public int? EscalationTimeInMin { get; set; }

        /// <summary>
        /// Gets or sets the EscalationLevel value.
        /// </summary>
        public int? EscalationLevel { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
