namespace CMMS.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class CMMSDBContext : DbContext
    {
        public CMMSDBContext()
            : base("name=CMMSDBContext")
        {
        }

        public virtual DbSet<cmBank> cmBank { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //}
    }
}
