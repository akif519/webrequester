//-----------------------------------------------------------------------
// <copyright file="PurchaseRequest.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PurchaseRequest
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PurchaseRequest")]
    public sealed class PurchaseRequest : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Id value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestID value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string PurchaseRequestID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the DateRequired value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? DateRequired { get; set; }

        /// <summary>
        /// Gets or sets the RequestedBy value.
        /// </summary>
        public int? RequestedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ApprovalStatus value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ApprovalStatus { get; set; }

        /// <summary>
        /// Gets or sets the OrderStatus value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets the Note value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets the Auth_status value.
        /// </summary>
        public int? Auth_status { get; set; }

        /// <summary>
        /// Gets or sets the CancelNotes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string CancelNotes { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_by value.
        /// </summary>
        public int? Cancel_by { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_date value.
        /// </summary>
        public DateTime? Cancel_date { get; set; }

        /// <summary>
        /// Gets or sets the Status_id value.
        /// </summary>
        public int? Status_id { get; set; }

        /// <summary>
        /// Gets or sets the MaintdeptId value.
        /// </summary>
        public int? MaintdeptId { get; set; }

        /// <summary>
        /// Gets or sets the MainSubDeptId value.
        /// </summary>
        public int? MainSubDeptId { get; set; }

        /// <summary>
        /// Gets or sets the MaintdivId value.
        /// </summary>
        public int? MaintdivId { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedDate value.
        /// </summary>
        public DateTime? LastPrintedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastPrintedBy value.
        /// </summary>
        public int? LastPrintedBy { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        [NotMapped]
        public string ProjectName { get; set; }

        [NotMapped]
        public string AltProjectName { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the l2 altname.
        /// </summary>
        /// <value>
        /// The l2 altname.
        /// </value>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the Maintainance dept code value.
        /// </summary>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the status_desc.
        /// </summary>
        /// <value>
        /// The status_desc.
        /// </value>
        [NotMapped]
        public string status_desc { get; set; }

        /// <summary>
        /// Gets or sets the altstatus_desc.
        /// </summary>
        /// <value>
        /// The altstatus_desc.
        /// </value>
        [NotMapped]
        public string altstatus_desc { get; set; }

        /// <summary>
        /// Gets or sets the auth_status_desc.
        /// </summary>
        /// <value>
        /// The auth_status_desc.
        /// </value>
        [NotMapped]
        public string auth_status_desc { get; set; }

        /// <summary>
        /// Gets or sets the altauth_status_desc.
        /// </summary>
        /// <value>
        /// The altauth_status_desc.
        /// </value>
        [NotMapped]
        public string Altauth_status_desc { get; set; }

        [NotMapped]
        public int? PRIID { get; set; }

        [NotMapped]
        public string PRNO { get; set; }

        [NotMapped]
        public int? PartNumber { get; set; }

        [NotMapped]
        public string PartDescription { get; set; }

        [NotMapped]
        public string UoM { get; set; }

        [NotMapped]
        public decimal? Quantity { get; set; }

        [NotMapped]
        public string JoborderID { get; set; }

        [NotMapped]
        public string Source { get; set; }

        [NotMapped]
        public decimal? POIQuantity { get; set; }

        [NotMapped]
        public string Status { get; set; }

        [NotMapped]
        public string StockID { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string RequestedByEmployeeNO { get; set; }

        [NotMapped]
        public string RequestedByEmployeeName { get; set; }

        [NotMapped]
        public string RequestedByAltEmployeeName { get; set; }

        [NotMapped]
        public string CreatedByEmployeeNO { get; set; }

        [NotMapped]
        public string CreatedByEmployeeName { get; set; }

        [NotMapped]
        public string CreatedByAltEmployeeName { get; set; }

        [NotMapped]
        public string JsonlstItemModel { get; set; }

        [NotMapped]
        public List<PurchaseRequestItem> lstPurchaseRequestItem { get; set; }

        [NotMapped]
        public bool IsAnyPRItemRaisedToPO { get; set; }

        [NotMapped]
        public string Cancel_byName { get; set; }

        [NotMapped]
        public string workOrderNo { get; set; }

        [NotMapped]
        public decimal? RefPrice { get; set; }

        #endregion
    }
}
