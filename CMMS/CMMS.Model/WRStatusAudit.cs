//-----------------------------------------------------------------------
// <copyright file="WRStatusAudit.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - WRStatusAudit
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>22-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("WRStatusAudit")]
	public sealed class WRStatusAudit : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Gets or sets the WRNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string WRNo { get; set; }

		/// <summary>
		/// Gets or sets the WRStatus value.
		/// </summary>
		public int? WRStatus { get; set; }

		/// <summary>
		/// Gets or sets the Date value.
		/// </summary>
		public DateTime? Date { get; set; }

		/// <summary>
		/// Gets or sets the EmpID value.
		/// </summary>
		public int? EmpID { get; set; }

		/// <summary>
		/// Gets or sets the Source value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
        [AllowNullForSave]
        public string source { get; set; }

		/// <summary>
		/// Gets or sets the Latitude value.
		/// </summary>
        [AllowNullForSave]
        public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the Longitude value.
		/// </summary>
        [AllowNullForSave]
        public float? Longitude { get; set; }

        //[NotMapped]
        //public virtual employees employee { get; set; }
        //[NotMapped]
        //public virtual Worequest worequest { get; set; }
        //[NotMapped]
        //public virtual Workstatu workstatu { get; set; }

		#endregion
	}
}
