//-----------------------------------------------------------------------
// <copyright file="Checklistelement.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - checklistelements
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("checklistelements")]
    public sealed class Checklistelement : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ChecklistelementId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChecklistelementId { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the SeqNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SeqNo { get; set; }

        /// <summary>
        /// Gets or sets the TaskDesc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string TaskDesc { get; set; }

        /// <summary>
        /// Gets or sets the AltTaskDesc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string AltTaskDesc { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the HDN seq identifier.
        /// </summary>
        /// <value>
        /// The HDN seq identifier.
        /// </value>
        [NotMapped]
        public int? HdnSeqID { get; set; }

        [NotMapped]
        public int SeqNoText { get; set; }

        #endregion
    }
}
