//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - emailnotificationrules
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("emailnotificationrules")]
    public sealed class Emailnotificationrule : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationRuleID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationRuleID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleName value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(200, ErrorMessage = "*")]
        public string NotificationRuleName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleModuleID value.
        /// </summary>
        public int? NotificationRuleModuleID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationRuleEventID value.
        /// </summary>
        public int? NotificationRuleEventID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the IsSendToRequester value.
        /// </summary>
        public bool? IsSendToRequester { get; set; }

        /// <summary>
        /// Gets or sets the IsActive value.
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// Gets or sets the IsAppliedToSMS value.
        /// </summary>
        [AllowNullForSave]
        public bool? IsAppliedToSMS { get; set; }

        /// <summary>
        /// Gets or sets the IsAppliedToEmail value.
        /// </summary>
        [AllowNullForSave]
        public bool? IsAppliedToEmail { get; set; }

        /// <summary>
        /// Gets or sets the EmailTemplateName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string EmailTemplateName { get; set; }

        [NotMapped]
        public int L5Count { get; set; }
        [NotMapped]
        public int LocationCount { get; set; }
        [NotMapped]
        public int AssetCount { get; set; }

        [NotMapped]
        public string EmailRule_L2 { get; set; }
        [NotMapped]
        public string EmailRule_WorkType { get; set; }
        [NotMapped]
        public string EmailRule_WorkPriority { get; set; }
        [NotMapped]
        public string EmailRule_WorkTrade { get; set; }
        [NotMapped]
        public string EmailRule_WorkStatus { get; set; }
        [NotMapped]
        public string EmailRule_Employee { get; set; }
        [NotMapped]
        public string EmailRule_EmployeeESC { get; set; }
        [NotMapped]
        public string EmailRule_EmployeeCritical { get; set; }
        [NotMapped]
        public string EmailRule_EmployeeNormal { get; set; }
        [NotMapped]
        public string EmailRule_Supplier { get; set; }
        [NotMapped]
        public string EmailRule_L5 { get; set; }
        [NotMapped]
        public string EmailRule_Location { get; set; }

        [NotMapped]
        public string EmailRule_Assets { get; set; }

        [NotMapped]
        public string EmailRule_LocationType { get; set; }

        [NotMapped]
        public string EmailRule_SubCategory { get; set; }

        [NotMapped]
        public string EmailRule_Criticality { get; set; }

        [NotMapped]
        public string EmailRule_EscTime { get; set; }

        #endregion
    }
}
