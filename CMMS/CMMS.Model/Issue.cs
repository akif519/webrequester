﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    public class ViewModelIssue
    {
        public int? StockID { get; set; }
        public int? SubStoreID { get; set; }
        public DateTime? DateIssue { get; set; }
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AltStockDescription { get; set; }
        public string SubStoreCode { get; set; }
        public decimal? QtyIssue { get; set; }
        public decimal? Price3 { get; set; }
        public decimal? TotalPrice { get; set; }
        public string UoM { get; set; }
        public string GUID { get; set; }

        public int? LocationID { get; set; }
        public int? L2ID { get; set; }
        public int? ToSubStoreID { get; set; }
        public int? ToL2ID { get; set; }
        public string WONo { get; set; }
        public string Description { get; set; }
        public string Warehouse { get; set; }
        public string IWTNo { get; set; }
        public string BatchNo { get; set; }
        public short? IsTransfer { get; set; }

    }

    /// <summary>
    /// This class is used to Define Model for Table - issue
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("issue")]
    public sealed class Issue : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the IssueID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IssueID { get; set; }

        /// <summary>
        /// Gets or sets the GUID value.
        /// </summary>
        [NotMapped]
        public string GUID { get; set; }

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        public int? StockID { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreCode value.
        /// </summary>
        [NotMapped]
        public string SubStoreCode { get; set; }

         /// <summary>
        /// Gets or sets the TotalPrice value.
        /// </summary>
        [NotMapped]
        public decimal? TotalPrice { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the Location Description value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the Location Alt Description value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnL2ID value.
        /// </summary>
        [NotMapped]
        public int? HdnL2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnSubStoreID value.
        /// </summary>
        [NotMapped]
        public int? HdnSubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreID value.
        /// </summary>
        public int? SubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the ToSubStoreID value.
        /// </summary>
        public int? ToSubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the ToL2ID value.
        /// </summary>
        public int? ToL2ID { get; set; }

        /// <summary>
        /// Gets or sets the WONo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WONo { get; set; }

        /// <summary>
        /// Gets or sets the HDnWONo value.
        /// </summary>
        [NotMapped]
        public string HDNWoNo { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the DateIssue value.
        /// </summary>
        public DateTime? DateIssue { get; set; }

        /// <summary>
        /// Gets or sets the IWTNo value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string IWTNo { get; set; }

        /// <summary>
        /// Gets or sets the Warehouse value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string Warehouse { get; set; }

        /// <summary>
        /// Gets or sets the UoM value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        public string UoM { get; set; }

        /// <summary>
        /// Gets or sets the QtyIssue value.
        /// </summary>
        public decimal? QtyIssue { get; set; }

        /// <summary>
        /// Gets or sets the Price3 value.
        /// </summary>
        public decimal? Price3 { get; set; }

        /// <summary>
        /// Gets or sets the BatchNo value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the IsTransfer value.
        /// </summary>
        public short? IsTransfer { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptDesc value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptDesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptdesc value.
        /// </summary>
        [NotMapped]
        public string MaintDeptdesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionName value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionName { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string SubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the TransferText value.
        /// </summary>
        [NotMapped]
        public string TransferText { get; set; }

        /// <summary>
        /// Gets or sets the ToSectorName value.
        /// </summary>
        [NotMapped]
        public string ToSectorName { get; set; }

        /// <summary>
        /// Gets or sets the ToL2Name value.
        /// </summary>
        [NotMapped]
        public string ToL2Name { get; set; }

        /// <summary>
        /// Gets or sets the ToSubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string ToSubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the List of Issue value.
        /// </summary>
        [NotMapped]
        public string lstIssues { get; set; }

        /// <summary>
        /// Gets or sets the TransactionID value.
        /// </summary>
        public long? TransactionID { get; set; }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

        #endregion
    }
}
