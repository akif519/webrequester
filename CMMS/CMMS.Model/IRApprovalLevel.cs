//-----------------------------------------------------------------------
// <copyright file="IRApprovalLevel.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - IRApprovalLevels
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("IRApprovalLevels")]
	public sealed class IRApprovalLevel : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the IRApprovalLevelId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int IRApprovalLevelId { get; set; }

		/// <summary>
		/// Gets or sets the LevelNo value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int LevelNo { get; set; }

		/// <summary>
		/// Gets or sets the LevelName value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(100, ErrorMessage = "*")]
		public string LevelName { get; set; }

		/// <summary>
		/// Gets or sets the AltLevelName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string AltLevelName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the IsFinalLevel is enabled.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public bool IsFinalLevel { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the Createddate value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public DateTime Createddate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the Modifieddate value.
		/// </summary>
		public DateTime? Modifieddate { get; set; }

		#endregion
	}
}
