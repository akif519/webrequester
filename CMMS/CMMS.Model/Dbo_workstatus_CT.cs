//-----------------------------------------------------------------------
// <copyright file="Dbo_workstatus_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_workstatus_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_workstatus_CT")]
	public sealed class Dbo_workstatus_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		
		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the WorkStatusID value.
        ///// </summary>
        //public int? WorkStatusID { get; set; }

		/// <summary>
		/// Gets or sets the WorkStatus value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string WorkStatus { get; set; }

		/// <summary>
		/// Gets or sets the AltWorkStatus value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string AltWorkStatus { get; set; }

		/// <summary>
		/// Gets or sets the IsDefault value.
		/// </summary>
		public bool? IsDefault { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		
		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the Color value.
        ///// </summary>
        //[StringLength(15, ErrorMessage = "*")]
        //public string Color { get; set; }

		#endregion
	}
}
