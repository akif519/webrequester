//-----------------------------------------------------------------------
// <copyright file="Dbo_suppliers_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_suppliers_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_suppliers_CT")]
	public sealed class Dbo_suppliers_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		
		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the SupplierID value.
        ///// </summary>
        //public int? SupplierID { get; set; }

		/// <summary>
		/// Gets or sets the SupplierNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string SupplierNo { get; set; }

		/// <summary>
		/// Gets or sets the SupplierName value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
		public string SupplierName { get; set; }

		/// <summary>
		/// Gets or sets the AltSupplierName value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
		public string AltSupplierName { get; set; }

		/// <summary>
		/// Gets or sets the ContactName value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactName { get; set; }

		/// <summary>
		/// Gets or sets the ContactTitle value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactTitle { get; set; }

		/// <summary>
		/// Gets or sets the Address value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Address { get; set; }

		/// <summary>
		/// Gets or sets the City value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string City { get; set; }

		/// <summary>
		/// Gets or sets the PostalCode value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string PostalCode { get; set; }

		/// <summary>
		/// Gets or sets the StateOrProvince value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string StateOrProvince { get; set; }

		/// <summary>
		/// Gets or sets the Country value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string Country { get; set; }

		/// <summary>
		/// Gets or sets the PhoneNumber value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string PhoneNumber { get; set; }

		/// <summary>
		/// Gets or sets the FaxNumber value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string FaxNumber { get; set; }

		/// <summary>
		/// Gets or sets the Services value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Services { get; set; }

		/// <summary>
		/// Gets or sets the Phone1 value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string Phone1 { get; set; }

		/// <summary>
		/// Gets or sets the ContactName2 value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactName2 { get; set; }

		/// <summary>
		/// Gets or sets the ContactTitle2 value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactTitle2 { get; set; }

		/// <summary>
		/// Gets or sets the Phone2 value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string Phone2 { get; set; }

		/// <summary>
		/// Gets or sets the ContactName3 value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactName3 { get; set; }

		/// <summary>
		/// Gets or sets the ContactTitle3 value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ContactTitle3 { get; set; }

		/// <summary>
		/// Gets or sets the Phone3 value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string Phone3 { get; set; }

        ///// <summary>
        ///// Gets or sets the Oem value.
        ///// </summary>
        //public short? Oem { get; set; }

        ///// <summary>
        ///// Gets or sets the AttrID value.
        ///// </summary>
        //public int? AttrID { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the CategoryID value.
        ///// </summary>
        //public int? CategoryID { get; set; }

        ///// <summary>
        ///// Gets or sets the Email value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Email { get; set; }

        ///// <summary>
        ///// Gets or sets the Email1 value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Email1 { get; set; }

        ///// <summary>
        ///// Gets or sets the Email2 value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Email2 { get; set; }

        ///// <summary>
        ///// Gets or sets the Email3 value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Email3 { get; set; }

        ///// <summary>
        ///// Gets or sets the ClassificationTradeIds value.
        ///// </summary>
        //[StringLength(-1, ErrorMessage = "*")]
        //public string ClassificationTradeIds { get; set; }

        ///// <summary>
        ///// Gets or sets the ClassificationLevel value.
        ///// </summary>
        //public int? ClassificationLevel { get; set; }

        ///// <summary>
        ///// Gets or sets the CR_No value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CR_No { get; set; }

        ///// <summary>
        ///// Gets or sets the CR_ExpiryDate value.
        ///// </summary>
        //public DateTime? CR_ExpiryDate { get; set; }

        ///// <summary>
        ///// Gets or sets the Remarks value.
        ///// </summary>
        //public string Remarks { get; set; }

		#endregion
	}
}
