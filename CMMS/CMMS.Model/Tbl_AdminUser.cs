﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    [Table("Tbl_AdminUser")]
    public sealed class Tbl_AdminUser : BaseModel
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserID { get; set; }  

        /// <summary>
        /// Gets or sets the UserName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Password value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the UserName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the Password value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the Password value.
        /// </summary>
        
        //public bool IsActive { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string ModifiedBy { get; set; }
        //public DateTime ModifiedDate { get; set; }
    }
}
