﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
	/// This class is used to Define Model for Table - worequest
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("worequest")]
	public sealed class Worequest : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the RequestNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string RequestNo { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the L3ID value.
		/// </summary>
		public int? L3ID { get; set; }

		/// <summary>
		/// Gets or sets the L4ID value.
		/// </summary>
		public int? L4ID { get; set; }

		/// <summary>
		/// Gets or sets the LocationID value.
		/// </summary>
		public int? LocationID { get; set; }

		/// <summary>
		/// Gets or sets the AssetID value.
		/// </summary>
		public int? AssetID { get; set; }

		/// <summary>
		/// Gets or sets the WorkorderNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string WorkorderNo { get; set; }

		/// <summary>
		/// Gets or sets the Worktradeid value.
		/// </summary>
		public int? Worktradeid { get; set; }

		/// <summary>
		/// Gets or sets the WorkTypeID value.
		/// </summary>
		public int? WorkTypeID { get; set; }

		/// <summary>
		/// Gets or sets the Workpriorityid value.
		/// </summary>
		public int? Workpriorityid { get; set; }

		/// <summary>
		/// Gets or sets the CancelledByID value.
		/// </summary>
		public int? CancelledByID { get; set; }

		/// <summary>
		/// Gets or sets the RequesterID value.
		/// </summary>
		public int? RequesterID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedByID value.
		/// </summary>
		public int? CreatedByID { get; set; }

		/// <summary>
		/// Gets or sets the RequestStatusID value.
		/// </summary>
		public int? RequestStatusID { get; set; }

		/// <summary>
		/// Gets or sets the MaintdeptId value.
		/// </summary>
		public int? MaintdeptId { get; set; }

		/// <summary>
		/// Gets or sets the MainSubDeptId value.
		/// </summary>
		public int? MainSubDeptId { get; set; }

		/// <summary>
		/// Gets or sets the MaintdivId value.
		/// </summary>
		public int? MaintdivId { get; set; }

		/// <summary>
		/// Gets or sets the ProblemDesc value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string ProblemDesc { get; set; }

		/// <summary>
		/// Gets or sets the ReceivedDate value.
		/// </summary>
		public DateTime? ReceivedDate { get; set; }

        /// <summary>
        /// Gets or sets the ReceivedDate value.
        /// </summary>
        [NotMapped]
        public DateTime? ReceivedTime { get; set; }

		/// <summary>
		/// Gets or sets the RequiredDate value.
		/// </summary>
		public DateTime? RequiredDate { get; set; }

        /// <summary>
        /// Gets or sets the RequiredDate value.
        /// </summary>
        [NotMapped]
        public DateTime? RequiredTime { get; set; }

		/// <summary>
		/// Gets or sets the Remarks value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string Remarks { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the L5ID value.
		/// </summary>
		public int? L5ID { get; set; }

		/// <summary>
		/// Gets or sets the AuthorisedEmployeeId value.
		/// </summary>
		public int? AuthorisedEmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the ExternalRequesterName value.
		/// </summary>
		[StringLength(200, ErrorMessage = "*")]
		public string ExternalRequesterName { get; set; }

        /// <summary>
        /// Gets or sets the Maintainance dept code value.
        /// </summary>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the work status name value.
        /// </summary>
        [NotMapped]
        public string JobStatus { get; set; }

        /// <summary>
        /// Gets or sets the Alt Job Status value.
        /// </summary>
        [NotMapped]
        public string AltWorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the Work Status value.
        /// </summary>
        [NotMapped]
        public string WorkStatus { get; set; }

        /// <summary>
        /// Gets or sets the Work Type Description value.
        /// </summary>
        [NotMapped]
        public string WorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AltWorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets Name value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets AltName value.
        /// </summary>
        [NotMapped]

        public string AltName { get; set; }
        /// <summary>
        /// Gets or sets the WorkPriorityName value.
        /// </summary>
        [NotMapped]
        public string WorkPriorityName { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AltWorkPriority { get; set; }

        /// <summary>
        /// Gets or sets the Alt Work Type Description value.
        /// </summary>
        [NotMapped]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [NotMapped]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [NotMapped]
        public string AssetAltDescription { get; set; }



        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2ClassCode value.
        /// </summary>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorCode value.
        /// </summary>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }
        
        /// <summary>
        /// Gets or sets the SectorAltName value.
        /// </summary>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L3Desc value.
        /// </summary>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the L3AltDesc value.
        /// </summary>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L4Description value.
        /// </summary>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the L4AltDescription value.
        /// </summary>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the L5AltDescription value.
        /// </summary>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [NotMapped]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the ProjectName value.
        /// </summary>
        [NotMapped]
        public string ProjectName { get; set; }

        [NotMapped]
        public string AltProjectName { get; set; }
        
        /// <summary>
        /// Gets or sets the EmployeeNO value.
        /// </summary>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the Email value.
        /// </summary>
        [NotMapped]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the WorkPhone value.
        /// </summary>
        [NotMapped]
        public string WorkPhone { get; set; }

        /// <summary>
        /// Gets or sets the Fax value.
        /// </summary>
        [NotMapped]
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the HandPhone value.
        /// </summary>
        [NotMapped]
        public string HandPhone { get; set; }

        /// <summary>
        /// Gets or sets the AuthEmployeeName value.
        /// </summary>
        [NotMapped]
        public string AuthEmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the NoteToTechLocation value.
        /// </summary>
        [NotMapped]
        public string NoteToTechLocation { get; set; }

        /// <summary>
        /// Gets or sets the Asset value.
        /// </summary>
        [NotMapped]
        public Asset Asset { get; set; }

        [NotMapped]
        public int? L1ID { get; set; }
        #endregion


        [NotMapped]
        public List<ExtAssetFile> ExtassetfileTable { get; set; }
        
        [NotMapped]
        public Nullable<int> WorkStatusID { get; set; }

        [NotMapped]
        public float? Latitude { get; set; }
        [NotMapped]
        public float? Longitude { get; set; }
        [NotMapped]
        public string Source { get; set; }
        [NotMapped]
        public int EmployeeID { get; set; }
        
        //For WebReq By: Akif519
        public string ProblemTypeIdRef { get; set; }

        public string ProblemDescIdRef { get; set; }


	}
}


