﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - SITask
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("SITask")]
    public sealed class SITask : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WoNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WoNo { get; set; }

        /// <summary>
        /// Gets or sets the SeqId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SeqId { get; set; }

        /// <summary>
        /// Gets or sets the Safety ID value.
        /// </summary>
        [NotMapped]
        public int SafetyID { get; set; }

        /// <summary>
        /// Gets or sets the Safety No value.
        /// </summary>
        [NotMapped]
        public string SafetyNo { get; set; }

        /// <summary>
        /// Gets or sets the Safety Name value.
        /// </summary>
        [NotMapped]
        public string SafetyName { get; set; }

        /// <summary>
        /// Gets or sets the Alternative Job Plan Name value.
        /// </summary>
        [NotMapped]
        public string AltSafetyName { get; set; }

        /// <summary>
        /// Gets or sets the Details value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Details { get; set; }

        /// <summary>
        /// Gets or sets the AltDetails value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string AltDetails { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
