//-----------------------------------------------------------------------
// <copyright file="WorkingHour.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - WorkingHours
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>16-Jan-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("WorkingHours")]
	public sealed class WorkingHour : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the WorkingHourID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int WorkingHourID { get; set; }

		/// <summary>
		/// Gets or sets the L2ID value.
		/// </summary>
		public int? L2ID { get; set; }

		/// <summary>
		/// Gets or sets the WorkingHour value.
		/// </summary>
		[StringLength(1000, ErrorMessage = "*")]
        public string WorkingHourName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2name.
        /// </summary>
        /// <value>
        /// The l2name.
        /// </value>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the l2 altname.
        /// </summary>
        /// <value>
        /// The l2 altname.
        /// </value>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the l1 identifier.
        /// </summary>
        /// <value>
        /// The l1 identifier.
        /// </value>
        public int? L1ID { get; set; }

        /// <summary>
        /// Gets or sets the l1 no.
        /// </summary>
        /// <value>
        /// The l1 no.
        /// </value>
        [NotMapped]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the name of the l1.
        /// </summary>
        /// <value>
        /// The name of the l1.
        /// </value>
        [NotMapped]
        public string L1Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the l1 alt.
        /// </summary>
        /// <value>
        /// The name of the l1 alt.
        /// </value>
        [NotMapped]
        public string L1AltName { get; set; }

		#endregion
	}
}
