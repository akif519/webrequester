//-----------------------------------------------------------------------
// <copyright file="MaintSubDept.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - MaintSubDept
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy> 
    /// <ReviewDate></ReviewDate>
    [Table("MaintSubDept")]
    public sealed class MaintSubDept : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string MaintSubDeptDesc { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptAltDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MaintSubDeptAltDesc { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the CmActivityID value.
        /// </summary>
        public int? CmActivityID { get; set; }

        /// <summary>
        /// Gets or sets the is from cm.
        /// </summary>
        /// <value>
        /// The is from cm.
        /// </value>
        public bool? IsFromCM { get; set; }
        
        /// <summary>
        /// Gets or sets the maint division code.
        /// </summary>
        /// <value>
        /// The maint division code.
        /// </value>
        [NotMapped]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the maint dept code.
        /// </summary>
        /// <value>
        /// The maint dept code.
        /// </value>
        [NotMapped]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the maint division identifier.
        /// </summary>
        /// <value>
        /// The maint division identifier.
        /// </value>
        [NotMapped]
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the is enable maint div dept.
        /// </summary>
        /// <value>
        /// The is enable maint div dept.
        /// </value>
        [NotMapped]
        public bool? IsEnableMaintDivDept { get; set; }

        /// <summary>
        /// Gets or sets the ChkL2 value.
        /// </summary>
        [NotMapped]
        public bool ChkMaintSubDept { get; set; }

        #endregion
    }
}
