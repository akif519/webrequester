//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_workstatu.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_workstatus
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_workstatus")]
	public sealed class Emailnotificationrules_workstatu : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleWorkStatusID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleWorkStatusID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the WorkStatusID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int WorkStatusID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string WorkStatus { get; set; }
        [NotMapped]
        public string AltWorkStatus { get; set; }
        
        
		#endregion
	}
}
