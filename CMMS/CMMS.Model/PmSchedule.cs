﻿//-----------------------------------------------------------------------
// <copyright file="Pmschedule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - pmschedule
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("pmschedule")]
    public sealed class PmSchedule : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PMID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the PhyLocationID value.
        /// </summary>
        [AllowNullForSave]
        public int? PhyLocationID { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistID value.
        /// </summary>
        public int? ChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeID value.
        /// </summary>
        public int? WorkTypeID { get; set; }

        /// <summary>
        /// Gets or sets the Worktradeid value.
        /// </summary>
        public int? Worktradeid { get; set; }

        /// <summary>
        /// Gets or sets the TypePMgenID value.
        /// </summary>
        public int? TypePMgenID { get; set; }

        /// <summary>
        /// Gets or sets the Workpriorityid value.
        /// </summary>
        public int? Workpriorityid { get; set; }

        /// <summary>
        /// Gets or sets the PMNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltPMName { get; set; }

        /// <summary>
        /// Gets or sets the PeriodDays value.
        /// </summary>
        public int? PeriodDays { get; set; }

        /// <summary>
        /// Gets or sets the NextSchComp value.
        /// </summary>
        public DateTime? NextSchComp { get; set; }

        /// <summary>
        /// Gets or sets the FreqUnits value.
        /// </summary>        
        public int? FreqUnits { get; set; }

        /// <summary>
        /// Gets or sets the Frequency value.
        /// </summary>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the TargetStartDate value.
        /// </summary>
        public DateTime? TargetStartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetCompDate value.
        /// </summary>
        public DateTime? TargetCompDate { get; set; }

        /// <summary>
        /// Gets or sets the ActualCompDate value.
        /// </summary>
        public DateTime? ActualCompDate { get; set; }

        /// <summary>
        /// Gets or sets the NextDate value.
        /// </summary>
        public DateTime? NextDate { get; set; }

        /// <summary>
        /// Gets or sets the TypePM value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string TypePM { get; set; }

        /// <summary>
        /// Gets or sets the PMCounter value.
        /// </summary>
        public int? PMCounter { get; set; }

        /// <summary>
        /// Gets or sets the PMMultiple value.
        /// </summary>
        public int? PMMultiple { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the WeekofMonth value.
        /// </summary>
        public int? WeekofMonth { get; set; }

        /// <summary>
        /// Gets or sets the DayofWeek value.
        /// </summary>
        public int? DayofWeek { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        [AllowNullForSave]
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        [AllowNullForSave]
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the GroupId value.
        /// </summary>
        public int? GroupId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsCleaningModule is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsCleaningModule { get; set; }

        /// <summary>
        /// Gets or sets the CIGID value.
        /// </summary>
        public int? CIGID { get; set; }

        /// <summary>
        /// Gets or sets the PMGroupNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMGroupNo { get; set; }

        /// <summary>
        /// Gets or sets the PMGroupName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string PMGroupName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMGroupName value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(100, ErrorMessage = "*")]
        public string AltPMGroupName { get; set; }

        /// <summary>
        /// Gets or sets the AssetCategoryID value.
        /// </summary>
        public int? AssetCategoryID { get; set; }

        /// <summary>
        /// Gets or sets the AssetSubCategoryID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetSubCategoryID { get; set; }

        /// <summary>
        /// Gets or sets the MultiL2 value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string MultiL2 { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNumber value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Gets or sets the SupplierID value.
        /// </summary>
        public int? SupplierID { get; set; }

        [NotMapped]
        public string PMType { get; set; }
        [NotMapped]
        public string ChecklistNo { get; set; }
        [NotMapped]
        public string CheckListName { get; set; }
        [NotMapped]
        public string AltCheckListName { get; set; }
        [NotMapped]
        public string L2Code { get; set; }
        [NotMapped]
        public string L3No { get; set; }
        [NotMapped]
        public string L3Desc { get; set; }
        [NotMapped]
        public string L3AltDesc { get; set; }
        [NotMapped]
        public string L4No { get; set; }
        [NotMapped]
        public string L4Description { get; set; }
        [NotMapped]
        public string L4AltDescription { get; set; }
        [NotMapped]
        public string L5No { get; set; }
        [NotMapped]
        public string L5Description { get; set; }
        [NotMapped]
        public string L5AltDescription { get; set; }
        [NotMapped]
        public string LocationNo { get; set; }
        [NotMapped]
        public string LocationDescription { get; set; }
        [NotMapped]
        public string LocationAltDescription { get; set; }
        [NotMapped]
        public string AssetNumber { get; set; }
        [NotMapped]
        public string AssetDescription { get; set; }
        [NotMapped]
        public string AssetAltDescription { get; set; }
        [NotMapped]
        public string MaintDivisionCode { get; set; }
        [NotMapped]
        public string MaintDeptCode { get; set; }
        [NotMapped]
        public string MaintSubDeptCode { get; set; }
        [NotMapped]
        public string AssetCatCode { get; set; }
        [NotMapped]
        public string AssetSubCatCode { get; set; }
        [NotMapped]
        public int? LocationIDAsset { get; set; }
        [NotMapped]
        public string LocationNoAsset { get; set; }
        [NotMapped]
        public string LocationDescriptionAsset { get; set; }
        [NotMapped]
        public string LocationAltDescriptionAsset { get; set; }

        [NotMapped]
        public bool boolPMMultiple
        {
            get
            {
                if (this.PMMultiple.HasValue)
                {
                    return Convert.ToBoolean(this.PMMultiple);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.PMMultiple = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public bool boolPMActive
        {
            get
            {
                if (this.PMActive.HasValue)
                {
                    return Convert.ToBoolean(this.PMActive);
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.PMActive = Convert.ToInt32(value);
            }
        }

        [NotMapped]
        public int? Month { get; set; }

        [NotMapped]
        public int? Year { get; set; }

        [NotMapped]
        public int? Days { get; set; }

        [NotMapped]
        public int? Frequency_Week { get; set; }

        [NotMapped]
        public int? PeriodDays_Week { get; set; }

        [NotMapped]
        public DateTime? TargetStartDate_Week { get; set; }

        [NotMapped]
        public DateTime? TargetCompDate_Week { get; set; }

        [NotMapped]
        public DateTime? NextDate_Week { get; set; }

        [NotMapped]
        public int? Frequency_Month { get; set; }

        [NotMapped]
        public int? PeriodDays_Month { get; set; }

        [NotMapped]
        public DateTime? TargetStartDate_Month { get; set; }

        [NotMapped]
        public DateTime? TargetCompDate_Month { get; set; }

        [NotMapped]
        public DateTime? NextDate_Month { get; set; }

        [NotMapped]
        public DateTime? LastGenDate { get; set; }

        [NotMapped]
        public string MultiAssetIDs { get; set; }

        [NotMapped]
        public DateTime DefaultDate { get; set; }

        [NotMapped]
        public string CIGGroupNumber { get; set; }

        [NotMapped]
        public string CIGGroupDescription { get; set; }

        [NotMapped]
        public string CIGArabicGroupDescription { get; set; }

        [NotMapped]
        public string JsonlstItemModel { get; set; }
        #endregion
    }

    public class PmForecastTemp : BaseModel
    {
        public DateTime PMStartDate { get; set; }
        public DateTime PmTarDate1 { get; set; }
        public int PMCount { get; set; }
        public string PmTask1 { get; set; }
        public string PmTask2 { get; set; }
        public string PmTask3 { get; set; }
        public string PmTask4 { get; set; }
        public string PmTask5 { get; set; }
        public string PmTask6 { get; set; }
        public string PmTask7 { get; set; }
    }
}
