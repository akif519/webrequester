//-----------------------------------------------------------------------
// <copyright file="Dbo_mr_authorisation_status_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_mr_authorisation_status_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_mr_authorisation_status_CT")]
	public sealed class Dbo_mr_authorisation_status_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		/// <summary>
		/// Gets or sets the __$operation value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int operation { get; set; }

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the Auth_status_id value.
        ///// </summary>
        //public int? Auth_status_id { get; set; }

		/// <summary>
		/// Gets or sets the Auth_status_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Auth_status_desc { get; set; }

		/// <summary>
		/// Gets or sets the Altauth_status_desc value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Altauth_status_desc { get; set; }

		#endregion
	}
}
