//-----------------------------------------------------------------------
// <copyright file="CmProjectPerformanceEvaluation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cmProjectPerformanceEvaluation
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>24-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cmProjectPerformanceEvaluation")]
	public sealed class CmProjectPerformanceEvaluation : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ID { get; set; }

		/// <summary>
		/// Gets or sets the ActivityDesc value.
		/// </summary>
		[StringLength(400, ErrorMessage = "*")]
		public string ActivityDesc { get; set; }

		/// <summary>
		/// Gets or sets the ActivityDescArabic value.
		/// </summary>
		[StringLength(400, ErrorMessage = "*")]
		public string ActivityDescArabic { get; set; }

		/// <summary>
		/// Gets or sets the MaxScore value.
		/// </summary>
		public int? MaxScore { get; set; }

		/// <summary>
		/// Gets or sets the Group value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string Group { get; set; }

		/// <summary>
		/// Gets or sets the Projectnumber value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string Projectnumber { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the ProjectID value.
		/// </summary>
		public int? ProjectID { get; set; }

		#endregion
	}
}
