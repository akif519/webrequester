//-----------------------------------------------------------------------
// <copyright file="Material_req_auth.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - material_req_auth
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>24-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("material_req_auth")]
	public sealed class Material_req_auth : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the IRAuthId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int IRAuthId { get; set; }

		/// <summary>
		/// Gets or sets the Mr_no value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string Mr_no { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeID value.
		/// </summary>
		public int? EmployeeID { get; set; }

		/// <summary>
		/// Gets or sets the Auth_date value.
		/// </summary>
		public DateTime? Auth_date { get; set; }

		/// <summary>
		/// Gets or sets the IRApprovalLevelMappingId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int IRApprovalLevelMappingId { get; set; }

        [NotMapped]
        public int LevelNo { get; set; }

        [NotMapped]
        public string LevelName { get; set; }

        [NotMapped]
        public string EmployeeNO { get; set; }

        [NotMapped]
        public string Name { get; set; }

        [NotMapped]
        public string Password { get; set; }

		#endregion
	}
}
