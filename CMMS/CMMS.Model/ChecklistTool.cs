//-----------------------------------------------------------------------
// <copyright file="ChecklistTool.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - ChecklistTools
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("ChecklistTools")]
    public sealed class ChecklistTool : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ChecklistToolID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ChecklistToolID { get; set; }

		/// <summary>
		/// Gets or sets the ChecklistID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int ChecklistID { get; set; }

		/// <summary>
		/// Gets or sets the SeqNo value.
		/// </summary>
		public int? SeqNo { get; set; }

		/// <summary>
		/// Gets or sets the ToolDescription value.
		/// </summary>
		[StringLength(200, ErrorMessage = "*")]
		public string ToolDescription { get; set; }

		/// <summary>
		/// Gets or sets the AltToolDescription value.
		/// </summary>
		[StringLength(200, ErrorMessage = "*")]
		public string AltToolDescription { get; set; }

		/// <summary>
		/// Gets or sets the Quantity value.
		/// </summary>
		public decimal? Quantity { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
