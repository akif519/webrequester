//-----------------------------------------------------------------------
// <copyright file="Dbo_irapprovallevels_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_irapprovallevels_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_irapprovallevels_CT")]
    public sealed class Dbo_irapprovallevels_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the LevelNo value.
        /// </summary>
        public int? LevelNo { get; set; }

        /// <summary>
        /// Gets or sets the LevelName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string LevelName { get; set; }

        /// <summary>
        /// Gets or sets the AltLevelName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltLevelName { get; set; }

        /// <summary>
        /// Gets or sets the IsFinalLevel value.
        /// </summary>
        public bool? IsFinalLevel { get; set; }

        ///// <summary>
        ///// Gets or sets the IRApprovalLevelId value.
        ///// </summary>
        //public int? IRApprovalLevelId { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //public int? CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the Createddate value.
        ///// </summary>
        //public DateTime? Createddate { get; set; }


        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the Modifieddate value.
        /// </summary>
        public string Modifieddate { get; set; }

        #endregion
    }
}
