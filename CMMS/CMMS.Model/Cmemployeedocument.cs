﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cmemployeedocument
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmemployeedocument")]
    public class Cmemployeedocument : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the DocID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the DocName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string DocName { get; set; }

        /// <summary>
        /// Gets or sets the DocArabicName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string DocArabicName { get; set; }

        /// <summary>
        /// Gets or sets the Doc_ExpiryDate value.
        /// </summary>
        public DateTime? Doc_ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the Issuer value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string Issuer { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(300, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the Status of the document.
        /// </summary>
        [NotMapped]
        public int DocStatus { get; set; }
        #endregion
    }
}
