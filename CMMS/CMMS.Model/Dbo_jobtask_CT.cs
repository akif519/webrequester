//-----------------------------------------------------------------------
// <copyright file="Dbo_jobtask_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_jobtask_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_jobtask_CT")]
	public sealed class Dbo_jobtask_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

		/// <summary>
		/// Gets or sets the WoNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string WoNo { get; set; }

		/// <summary>
		/// Gets or sets the SeqId value.
		/// </summary>
		public int? SeqId { get; set; }

		/// <summary>
		/// Gets or sets the Details value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Details { get; set; }

		/// <summary>
		/// Gets or sets the AltDetails value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string AltDetails { get; set; }

		/// <summary>
		/// Gets or sets the Remarks value.
		/// </summary>
		[StringLength(4000, ErrorMessage = "*")]
		public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }
        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string ModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedDate value.
        ///// </summary>
        //public DateTime? ModifiedDate { get; set; }

		#endregion
	}
}
