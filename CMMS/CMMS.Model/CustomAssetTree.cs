﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class CustomAssetTree
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool HasChild { get; set; }
    }
}
