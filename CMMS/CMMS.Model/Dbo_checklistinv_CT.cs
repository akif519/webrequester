//-----------------------------------------------------------------------
// <copyright file="Dbo_checklistinv_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_checklistinv_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_checklistinv_CT")]
    public sealed class Dbo_checklistinv_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        [NotMapped]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the SeqNo value.
        /// </summary>
        public int? SeqNo { get; set; }

        /// <summary>
        /// Gets or sets the Qty value.
        /// </summary>
        public float? Qty { get; set; }

        [NotMapped]
        public string StockNo { get; set; }

        [NotMapped]
        public string StockDescription { get; set; }

        [NotMapped]
        public string AltStockDescription { get; set; }


        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        ///// <summary>
        ///// Gets or sets the ChecklistInvId value.
        ///// </summary>
        //public int? ChecklistInvId { get; set; }

        ///// <summary>
        ///// Gets or sets the PartID value.
        ///// </summary>
        //public int? PartID { get; set; }


        ///// <summary>
        ///// Gets or sets the ChecklistID value.
        ///// </summary>
        //public int? ChecklistID { get; set; }

        ///// <summary>
        ///// Gets or sets the ChecklistelementId value.
        ///// </summary>
        //public int? ChecklistelementId { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the ModifiedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        #endregion
    }
}
