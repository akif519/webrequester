//-----------------------------------------------------------------------
// <copyright file="HelpRelatedTopic.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - HelpRelatedTopics
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>20-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("HelpRelatedTopics")]
	public sealed class HelpRelatedTopic : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the RelatedTopicID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int RelatedTopicID { get; set; }

		/// <summary>
		/// Gets or sets the ParentHelpModuleDetailID value.
		/// </summary>
		public int? ParentHelpModuleDetailID { get; set; }

		/// <summary>
		/// Gets or sets the ChildHelpModuleDetailID value.
		/// </summary>
		public int? ChildHelpModuleDetailID { get; set; }

        /// <summary>
        /// Gets or sets the parent help title.
        /// </summary>
        /// <value>
        /// The parent help title.
        /// </value>
        [NotMapped]
        public string ParentHelpTitle { get; set; }

        /// <summary>
        /// Gets or sets the child help title.
        /// </summary>
        /// <value>
        /// The child help title.
        /// </value>
        [NotMapped]
        public string ChildHelpTitle { get; set; } 

		#endregion
	}
}
