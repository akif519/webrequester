//-----------------------------------------------------------------------
// <copyright file="PartLocation.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - PartLocation
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("PartLocation")]
	public sealed class PartLocations : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ID { get; set; }

		/// <summary>
		/// Gets or sets the PartLocation value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(50, ErrorMessage = "*")]
		public string PartLocation { get; set; }

		/// <summary>
		/// Gets or sets the SubStoreID value.
		/// </summary>
		public int? SubStoreID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2 identifier.
        /// </summary>
        /// <value>
        /// The l2 identifier.
        /// </value>
        [NotMapped]
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the sub store code.
        /// </summary>
        /// <value>
        /// The sub store code.
        /// </value>
        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the is enable city store.
        /// </summary>
        /// <value>
        /// The is enable city store.
        /// </value>
        [NotMapped]
        public bool? IsEnableCityStore { get; set; }

		#endregion
	}
}
