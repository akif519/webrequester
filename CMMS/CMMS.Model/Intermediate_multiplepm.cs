﻿//-----------------------------------------------------------------------
// <copyright file="Intermediate_multiplepm.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - intermediate_multiplepm
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("intermediate_multiplepm")]
    public sealed class Intermediate_multiplepm : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PMID { get; set; }

        /// <summary>
        /// Gets or sets the MTaskID value.
        /// </summary>
        public int? MTaskID { get; set; }

        /// <summary>
        /// Gets or sets the MSeq value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int MSeq { get; set; }

        /// <summary>
        /// Gets or sets the MDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string MDescription { get; set; }

        /// <summary>
        /// Gets or sets the MStartDate value.
        /// </summary>
        public DateTime? MStartDate { get; set; }

        /// <summary>
        /// Gets or sets the MStartDateCount value.
        /// </summary>
        public int? MStartDateCount { get; set; }

        /// <summary>
        /// Gets or sets the MTaskSeq value.
        /// </summary>
        public int? MTaskSeq { get; set; }

        /// <summary>
        /// Gets or sets the MScount value.
        /// </summary>
        public int? MScount { get; set; }

        /// <summary>
        /// Gets or sets the MPMCounter value.
        /// </summary>
        public int? MPMCounter { get; set; }

        /// <summary>
        /// Gets or sets the MCount value.
        /// </summary>
        public int? MCount { get; set; }

        /// <summary>
        /// Gets or sets the Userid value.
        /// </summary>
        public int? Userid { get; set; }

        #endregion
    }
}
