﻿//-----------------------------------------------------------------------
// <copyright file="NotificationType.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - NotificationType
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>10-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("NotificationType")]
    public sealed class NotificationType : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the NotificationTypeID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationTypeID { get; set; }

        /// <summary>
        /// Gets or sets the NotificationTypeDesc value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string NotificationTypeDesc { get; set; }

        /// <summary>
        /// Gets or sets the NotificationTitle value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string NotificationTitle { get; set; }

        /// <summary>
        /// Gets or sets the NotificationDescription value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string NotificationDescription { get; set; }

        #endregion
    }
}