//-----------------------------------------------------------------------
// <copyright file="Dbo_pmschedule_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_pmschedule_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_pmschedule_CT")]
	public sealed class Dbo_pmschedule_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		
		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the PMNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string AltPMName { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string AssetNumber { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string ChecklistNo { get; set; }

        [NotMapped]
        public string WorkTypeDescription { get; set; }

        [NotMapped]
        public string WorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the PeriodDays value.
        /// </summary>
        public int? PeriodDays { get; set; }

        /// <summary>
        /// Gets or sets the FreqUnits value.
        /// </summary>
        public int? FreqUnits { get; set; }

        /// <summary>
        /// Gets or sets the Frequency value.
        /// </summary>
        public int? Frequency { get; set; }

        /// <summary>
        /// Gets or sets the TargetStartDate value.
        /// </summary>
        public string TargetStartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetCompDate value.
        /// </summary>
        public string TargetCompDate { get; set; }

        /// <summary>
        /// Gets or sets the ActualCompDate value.
        /// </summary>
        public string ActualCompDate { get; set; }

        /// <summary>
        /// Gets or sets the NextDate value.
        /// </summary>
        public string NextDate { get; set; }

        /// <summary>
        /// Gets or sets the TypePM value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string TypePM { get; set; }

        /// <summary>
        /// Gets or sets the PMMultiple value.
        /// </summary>
        public int? PMMultiple { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the WeekofMonth value.
        /// </summary>
        public int? WeekofMonth { get; set; }

        /// <summary>
        /// Gets or sets the DayofWeek value.
        /// </summary>
        public int? DayofWeek { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }


        ///// <summary>
        ///// Gets or sets the PMID value.
        ///// </summary>
        //public int? PMID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetID value.
        ///// </summary>
        //public int? AssetID { get; set; }

        ///// <summary>
        ///// Gets or sets the PhyLocationID value.
        ///// </summary>
        //public int? PhyLocationID { get; set; }

        ///// <summary>
        ///// Gets or sets the ChecklistID value.
        ///// </summary>
        //public int? ChecklistID { get; set; }

        ///// <summary>
        ///// Gets or sets the WorkTypeID value.
        ///// </summary>
        //public int? WorkTypeID { get; set; }

        ///// <summary>
        ///// Gets or sets the Worktradeid value.
        ///// </summary>
        //public int? Worktradeid { get; set; }

        ///// <summary>
        ///// Gets or sets the TypePMgenID value.
        ///// </summary>
        //public int? TypePMgenID { get; set; }

        ///// <summary>
        ///// Gets or sets the Workpriorityid value.
        ///// </summary>
        //public int? Workpriorityid { get; set; }

		

		

        ///// <summary>
        ///// Gets or sets the NextSchComp value.
        ///// </summary>
        //public string NextSchComp { get; set; }

		

        ///// <summary>
        ///// Gets or sets the PMCounter value.
        ///// </summary>
        //public int? PMCounter { get; set; }

		
		

		
        ///// <summary>
        ///// Gets or sets the MaintDeptID value.
        ///// </summary>
        //public int? MaintDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintSubDeptID value.
        ///// </summary>
        //public int? MaintSubDeptID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintDivisionID value.
        ///// </summary>
        //public int? MaintDivisionID { get; set; }

        ///// <summary>
        ///// Gets or sets the GroupId value.
        ///// </summary>
        //public int? GroupId { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public string CreatedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the IsCleaningModule value.
        ///// </summary>
        //public bool? IsCleaningModule { get; set; }

        ///// <summary>
        ///// Gets or sets the CIGID value.
        ///// </summary>
        //public int? CIGID { get; set; }

        ///// <summary>
        ///// Gets or sets the PMGroupNo value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string PMGroupNo { get; set; }

        ///// <summary>
        ///// Gets or sets the PMGroupName value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string PMGroupName { get; set; }

        ///// <summary>
        ///// Gets or sets the AltPMGroupName value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string AltPMGroupName { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetCategoryID value.
        ///// </summary>
        //public int? AssetCategoryID { get; set; }

        ///// <summary>
        ///// Gets or sets the AssetSubCategoryID value.
        ///// </summary>
        //public int? AssetSubCategoryID { get; set; }

        ///// <summary>
        ///// Gets or sets the MultiL2 value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string MultiL2 { get; set; }

        ///// <summary>
        ///// Gets or sets the ProjectNumber value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string ProjectNumber { get; set; }

        ///// <summary>
        ///// Gets or sets the SupplierID value.
        ///// </summary>
        //public int? SupplierID { get; set; }

		#endregion
	}
}
