//-----------------------------------------------------------------------
// <copyright file="DefaultSetup.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - DefaultSetups
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("DefaultSetups")]
	public sealed class DefaultSetup : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the SetupId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int SetupId { get; set; }

		/// <summary>
		/// Gets or sets the SettingName value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string SettingName { get; set; }

		/// <summary>
		/// Gets or sets the Value value.
		/// </summary>
		public decimal? Value { get; set; }

        /// <summary>
        /// Gets or sets the default GST.
        /// </summary>
        /// <value>
        /// The default GST.
        /// </value>
        [NotMapped]
        public decimal? DefaultGST { get; set; }

        /// <summary>
        /// Gets or sets the default tax.
        /// </summary>
        /// <value>
        /// The default tax.
        /// </value>
        [NotMapped]
        public decimal? DefaultTax { get; set; }
		#endregion
	}
}
