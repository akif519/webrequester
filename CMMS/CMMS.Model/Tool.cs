//-----------------------------------------------------------------------
// <copyright file="Tool.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - Tools
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>24-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("Tools")]
	public sealed class Tool : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ToolsID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ToolsID { get; set; }

		/// <summary>
		/// Gets or sets the ToolsDesc value.
		/// </summary>
		[StringLength(2000, ErrorMessage = "*")]
		public string ToolsDesc { get; set; }

		/// <summary>
		/// Gets or sets the ToolsAltDesc value.
		/// </summary>
        [StringLength(2000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
		public string ToolsAltDesc { get; set; }

		/// <summary>
		/// Gets or sets the ToolCategoryID value.
		/// </summary>
		public int? ToolCategoryID { get; set; }

		/// <summary>
		/// Gets or sets the ToolSubCategoryID value.
		/// </summary>
		public int? ToolSubCategoryID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

		/// <summary>
		/// Gets or sets the ToolsCode value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string ToolsCode { get; set; }

        /// <summary>
        /// Gets or sets the tool category code.
        /// </summary>
        /// <value>
        /// The tool category code.
        /// </value>
        [NotMapped]
        public string ToolCategoryCode { get; set; }

        /// <summary>
        /// Gets or sets the tool sub category code.
        /// </summary>
        /// <value>
        /// The tool sub category code.
        /// </value>
        [NotMapped]
        public string ToolSubCategoryCode { get; set; }


		#endregion
	}
}
