﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - stock_code_supplier
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("stock_code_supplier")]
    public sealed class Stock_code_supplier : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Stock_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Stock_id { get; set; }

        /// <summary>
        /// Gets or sets the Supplier_id value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int Supplier_id { get; set; }

        [StringLength(50, ErrorMessage = "*")]
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the SupplierName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the AltSupplierName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [NotMapped]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
