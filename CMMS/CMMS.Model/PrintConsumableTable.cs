﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
   public partial class PrintConsumableTable
    {
       public string RowNo { get; set; }
       public string Remarks { get; set; }
       public decimal QtyIssue { get; set; }
       public string UOM { get; set; }
       public string AltStockDescription { get; set; }
       public string StockDescription { get; set; }
       public string StockNo { get; set; }
    }
}
