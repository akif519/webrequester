//-----------------------------------------------------------------------
// <copyright file="ErrorLog.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - ErrorLog
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>15-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("ErrorLog")]
	public sealed class ErrorLog : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ErrorLogId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ErrorLogId { get; set; }

		/// <summary>
		/// Gets or sets the ErrorDate value.
		/// </summary>
		public DateTime? ErrorDate { get; set; }

		/// <summary>
		/// Gets or sets the LoginID value.
		/// </summary>
		public int? LoginID { get; set; }

		/// <summary>
		/// Gets or sets the IPAddress value.
		/// </summary>
		[StringLength(20, ErrorMessage = "*")]
		public string IPAddress { get; set; }

		/// <summary>
		/// Gets or sets the ClientBrowser value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string ClientBrowser { get; set; }

		/// <summary>
		/// Gets or sets the ErrorMessage value.
		/// </summary>
		[StringLength(1000, ErrorMessage = "*")]
		public string ErrorMessage { get; set; }

		/// <summary>
		/// Gets or sets the ErrorStackTrace value.
		/// </summary>
        [StringLength(5000, ErrorMessage = "*")]
		public string ErrorStackTrace { get; set; }

		/// <summary>
		/// Gets or sets the URL value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string URL { get; set; }

		/// <summary>
		/// Gets or sets the URLReferrer value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string URLReferrer { get; set; }

		/// <summary>
		/// Gets or sets the ErrorSource value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string ErrorSource { get; set; }

		/// <summary>
		/// Gets or sets the ErrorTargetSite value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string ErrorTargetSite { get; set; }

		/// <summary>
		/// Gets or sets the QueryString value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string QueryString { get; set; }

		/// <summary>
		/// Gets or sets the PostData value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string PostData { get; set; }

		/// <summary>
		/// Gets or sets the SessionInfo value.
		/// </summary>
        [StringLength(1000, ErrorMessage = "*")]
		public string SessionInfo { get; set; }

        /// <summary>
        /// Gets or sets the employee no.
        /// </summary>
        /// <value>
        /// The employee no.
        /// </value>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt.
        /// </summary>
        /// <value>
        /// The name of the alt.
        /// </value>
        [NotMapped]
        public string AltName { get; set; }

		#endregion
	}
}
