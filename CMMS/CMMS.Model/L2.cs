//-----------------------------------------------------------------------
// <copyright file="L2.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - L2
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("L2")]
    public sealed class L2 : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L1ID value.
        /// </summary>
        public int? L1ID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the L2Altname value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the DefaultCalendar value.
        /// </summary>
        [AllowNullForSave]
        public int? DefaultCalendar { get; set; }

        /// <summary>
        /// Gets or sets the Timezone value.
        /// </summary>
        [AllowNullForSave]
        public decimal? Timezone { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the SectorID value.
        /// </summary>
        public int? SectorID { get; set; }

        /// <summary>
        /// Gets or sets the L2ClassID value.
        /// </summary>
        [AllowNullForSave]
        public int? L2ClassID { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        [AllowNullForSave]
        public int? Status { get; set; }

        /// <summary>
        /// Gets or sets the CurrencyID value.
        /// </summary>
        [AllowNullForSave]
        public int? CurrencyID { get; set; }

        /// <summary>
        /// Gets or sets the DirectorDeputyName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DirectorDeputyName { get; set; }

        /// <summary>
        /// Gets or sets the SiteDirectorName value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SiteDirectorName { get; set; }

        /// <summary>
        /// Gets or sets the MaintManagerContractor value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MaintManagerContractor { get; set; }

        /// <summary>
        /// Gets or sets the MoHSupervisorName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string MoHSupervisorName { get; set; }

        /// <summary>
        /// Gets or sets the FaxNumber value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string FaxNumber { get; set; }

        /// <summary>
        /// Gets or sets the Phone value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the sector code.
        /// </summary>
        /// <value>
        /// The sector code.
        /// </value>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector.
        /// </summary>
        /// <value>
        /// The name of the sector.
        /// </value>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the l2 class code.
        /// </summary>
        /// <value>
        /// The l2 class code.
        /// </value>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the employee identifier.
        /// </summary>
        /// <value>
        /// The employee identifier.
        /// </value>
        [NotMapped]
        public int EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the l1 no.
        /// </summary>
        /// <value>
        /// The l1 no.
        /// </value>
        [NotMapped]
        public string L1No { get; set; }

        [NotMapped]
        public int HasChild { get; set; }
        #endregion
    }
}
