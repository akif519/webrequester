//-----------------------------------------------------------------------
// <copyright file="Dbo_maintgroup_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_maintgroup_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_maintgroup_CT")]
	public sealed class Dbo_maintgroup_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the GroupCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string GroupCode { get; set; }

        /// <summary>
        /// Gets or sets the GroupDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string GroupDesc { get; set; }

        /// <summary>
        /// Gets or sets the GroupAltDesc value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string GroupAltDesc { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        ///// <summary>
        ///// Gets or sets the GroupID value.
        ///// </summary>
        //public int? GroupID { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }


        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
        public string ModifiedDate { get; set; }

		#endregion
	}
}
