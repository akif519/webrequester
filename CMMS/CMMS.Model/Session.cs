//-----------------------------------------------------------------------
// <copyright file="Session.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - sessions
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("sessions")]
    public sealed class Session : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SessionId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SessionId { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the Logintime value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [AllowNullForSave]
        public DateTime? Logintime { get; set; }

        /// <summary>
        /// Gets or sets the Lastheartbeat value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [AllowNullForSave]
        public DateTime? Lastheartbeat { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the l2 identifier.
        /// </summary>
        /// <value>
        /// The l2 identifier.
        /// </value>
        [NotMapped]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the employee no.
        /// </summary>
        /// <value>
        /// The employee no.
        /// </value>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string NAME { get; set; }

        #endregion
    }
}
