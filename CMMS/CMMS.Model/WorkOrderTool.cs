﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - WorkOrderTools
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkOrderTools")]
    public sealed class WorkOrderTool : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkOrderToolID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkOrderToolID { get; set; }

        /// <summary>
        /// Gets or sets the WorkOrderNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string WorkOrderNo { get; set; }

        /// <summary>
        /// Gets or sets the SeqNo value.
        /// </summary>
        public int? SeqNo { get; set; }

        /// <summary>
        /// Gets or sets the ToolDescription value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string ToolDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltToolDescription value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string AltToolDescription { get; set; }

        /// <summary>
        /// Gets or sets the Quantity value.
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// 1 - For Add
        /// 2 - For Update
        /// 3 - For Delete
        /// </summary>
        [NotMapped]
        public int FlagForAddUpdateDelete { get; set; }

        #endregion
    }
}
