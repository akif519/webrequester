//-----------------------------------------------------------------------
// <copyright file="Emailnotificationrules_location.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - emailnotificationrules_location
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("emailnotificationrules_location")]
	public sealed class Emailnotificationrules_location : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the NotificationRuleLocationID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int NotificationRuleLocationID { get; set; }

		/// <summary>
		/// Gets or sets the NotificationRuleID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int NotificationRuleID { get; set; }

		/// <summary>
		/// Gets or sets the LocationID value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int? LocationID { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		[StringLength(100, ErrorMessage = "*")]
		public string ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedDate value.
		/// </summary>
		public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string LocationDescription { get; set; }

        [NotMapped]
        public string LocationAltDescription { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string SectorCode { get; set; }

        [NotMapped]
        public string Criticality { get; set; }

        [NotMapped]
        public string SectorName { get; set; }

        [NotMapped]
        public string L4No { get; set; }

        [NotMapped]
        public string L4Description { get; set; }

        [NotMapped]
        public string L5Description { get; set; }

        [NotMapped]
        public string L4AltDescription { get; set; }

        [NotMapped]
        public string L5AltDescription { get; set; }

        [NotMapped]
        public string L5No { get; set; }

		#endregion
	}
}
