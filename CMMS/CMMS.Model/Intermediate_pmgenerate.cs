﻿//-----------------------------------------------------------------------
// <copyright file="Intermediate_pmgenerate.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - intermediate_pmgenerate
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("intermediate_pmgenerate")]
    public sealed class Intermediate_pmgenerate : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMGenerateID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PMGenerateID { get; set; }

        /// <summary>
        /// Gets or sets the PMID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int PMID { get; set; }

        /// <summary>
        /// Gets or sets the PMNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string PMNo { get; set; }

        /// <summary>
        /// Gets or sets the PMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string PMName { get; set; }

        /// <summary>
        /// Gets or sets the AltPMName value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string AltPMName { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistID value.
        /// </summary>
        public int? ChecklistID { get; set; }

        /// <summary>
        /// Gets or sets the ChecklistNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ChecklistNo { get; set; }

        /// <summary>
        /// Gets or sets the CheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string CheckListName { get; set; }

        /// <summary>
        /// Gets or sets the AltCheckListName value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string AltCheckListName { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L4ID value.
        /// </summary>
        public int? L4ID { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L5ID value.
        /// </summary>
        public int? L5ID { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the WorkPriorityID value.
        /// </summary>
        public int? WorkPriorityID { get; set; }

        /// <summary>
        /// Gets or sets the WorkPriority value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string WorkPriority { get; set; }

        /// <summary>
        /// Gets or sets the AltWorkPriority value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltWorkPriority { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeID value.
        /// </summary>
        public int? WorkTypeID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTypeDescription value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string WorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltWorkTypeDescription value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltWorkTypeDescription { get; set; }

        /// <summary>
        /// Gets or sets the WOTradeID value.
        /// </summary>
        public int? WOTradeID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTrade value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string WorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the AltWorkTrade value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltWorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the TargetStartDate value.
        /// </summary>
        public DateTime? TargetStartDate { get; set; }

        /// <summary>
        /// Gets or sets the TargetCompDate value.
        /// </summary>
        public DateTime? TargetCompDate { get; set; }

        /// <summary>
        /// Gets or sets the ActualCompDate value.
        /// </summary>
        public DateTime? ActualCompDate { get; set; }

        /// <summary>
        /// Gets or sets the NextDate value.
        /// </summary>
        public DateTime? NextDate { get; set; }

        /// <summary>
        /// Gets or sets the WOPMtype value.
        /// </summary>
        public int? WOPMtype { get; set; }

        /// <summary>
        /// Gets or sets the TypePMgenID value.
        /// </summary>
        public int? TypePMgenID { get; set; }

        /// <summary>
        /// Gets or sets the Userid value.
        /// </summary>
        public int? Userid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the IsCleaningModule is enabled.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public bool IsCleaningModule { get; set; }

        public bool IsTargetStartDateHoliday { get; set; }
        #endregion
    }
}
