﻿//-----------------------------------------------------------------------
// <copyright file="HelpBookmarks.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - HelpBookmarks
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Dec-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("HelpBookmarks")]
    public sealed class HelpBookmarks : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the HelpBookmarksID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HelpBookmarksID { get; set; }

        /// <summary>
        /// Gets or sets the HelpModuleID value.
        /// </summary>
        public int? HelpModuleID { get; set; }

        /// <summary>
        /// Gets or sets the HelpModuleID value.
        /// </summary>
        public int? EmployeeID { get; set; }

        [NotMapped]
        public string HelpTitle { get; set; }

        [NotMapped]
        public int HelpModuelDetailID { get; set; }

        #endregion
    }
}
