﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - intermediate_pmmetersgenerate
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("intermediate_pmmetersgenerate")]
    public sealed class Intermediate_pmmetersgenerate : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the PMMetersGenerateID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PMMetersGenerateID { get; set; }

        /// <summary>
        /// Gets or sets the ReadingScheduleID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ReadingScheduleID { get; set; }

        /// <summary>
        /// Gets or sets the MeterID value.
        /// </summary>
        public int? MeterID { get; set; }

        /// <summary>
        /// Gets or sets the MeterNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        public string MeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the MeterType value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterType { get; set; }

        /// <summary>
        /// Gets or sets the WorkorderNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string WorkorderNo { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string AssetAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L4ID value.
        /// </summary>
        public int? L4ID { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L5ID value.
        /// </summary>
        public int? L5ID { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionID value.
        /// </summary>
        public int? MaintDivisionID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDivisionCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintDivisionCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptID value.
        /// </summary>
        public int? MaintDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptID value.
        /// </summary>
        public int? MaintSubDeptID { get; set; }

        /// <summary>
        /// Gets or sets the MaintSubDeptCode value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MaintSubDeptCode { get; set; }

        /// <summary>
        /// Gets or sets the Lowerlimit value.
        /// </summary>
        public float? Lowerlimit { get; set; }

        /// <summary>
        /// Gets or sets the PMLowTaskID value.
        /// </summary>
        public int? PMLowTaskID { get; set; }

        /// <summary>
        /// Gets or sets the PMLowTaskNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMLowTaskNo { get; set; }

        /// <summary>
        /// Gets or sets the UpperLimit value.
        /// </summary>
        public float? UpperLimit { get; set; }

        /// <summary>
        /// Gets or sets the PMUppTaskID value.
        /// </summary>
        public int? PMUppTaskID { get; set; }

        /// <summary>
        /// Gets or sets the PMUppTaskNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMUppTaskNo { get; set; }

        /// <summary>
        /// Gets or sets the PMIncTaskID value.
        /// </summary>
        public int? PMIncTaskID { get; set; }

        /// <summary>
        /// Gets or sets the PMIncTaskNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string PMIncTaskNo { get; set; }

        /// <summary>
        /// Gets or sets the Reading value.
        /// </summary>
        public float? Reading { get; set; }

        /// <summary>
        /// Gets or sets the MeterUnit value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string MeterUnit { get; set; }

        /// <summary>
        /// Gets or sets the LastReading value.
        /// </summary>
        public float? LastReading { get; set; }

        /// <summary>
        /// Gets or sets the LastPMDue value.
        /// </summary>
        public float? LastPMDue { get; set; }

        /// <summary>
        /// Gets or sets the WOTradeID value.
        /// </summary>
        public int? WOTradeID { get; set; }

        /// <summary>
        /// Gets or sets the WorkTrade value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string WorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the AltWorkTrade value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltWorkTrade { get; set; }

        /// <summary>
        /// Gets or sets the WOPMtype value.
        /// </summary>
        public int? WOPMtype { get; set; }

        /// <summary>
        /// Gets or sets the Userid value.
        /// </summary>
        public int? Userid { get; set; }

        #endregion
    }
}
