//-----------------------------------------------------------------------
// <copyright file="Dbo_assigntoworkorder_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_assigntoworkorder_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>30-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_assigntoworkorder_CT")]
	public sealed class Dbo_assigntoworkorder_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

		/// <summary>
		/// Gets or sets the __$operation value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int operation { get; set; }

		/// <summary>
		/// Gets or sets the __$update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        [NotMapped]
        public string EmployeeName { get; set; }

        ///// <summary>
        ///// Gets or sets the EmployeeID value.
        ///// </summary>
        //public int? EmployeeID { get; set; }

		/// <summary>
		/// Gets or sets the WorkorderNo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string WorkorderNo { get; set; }

       
		/// <summary>
		/// Gets or sets the EstStartDate value.
		/// </summary>
        public string EstStartDate { get; set; }

		/// <summary>
		/// Gets or sets the EstEndDate value.
		/// </summary>
        public string EstEndDate { get; set; }

		#endregion
	}
}
