﻿//-----------------------------------------------------------------------
// <copyright file="PMMeterMaster.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - PMMeterMaster
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("PMMeterMaster")]
    public sealed class PMMeterMaster : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the MeterMasterID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MeterMasterID { get; set; }

        /// <summary>
        /// Gets or sets the MeterNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(1000, ErrorMessage = "*")]
        public string MeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltMeterDescription value.
        /// </summary>
        [StringLength(1000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AltMeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetID { get; set; }

        /// <summary>
        /// Gets or sets the MeterType value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string MeterType { get; set; }

        /// <summary>
        /// Gets or sets the MeterUnit value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string MeterUnit { get; set; }

        /// <summary>
        /// Gets or sets the LastReading value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public float LastReading { get; set; }

        /// <summary>
        /// Gets or sets the MaxDailyIncrement value.
        /// </summary>
        public decimal? MaxDailyIncrement { get; set; }

        /// <summary>
        /// Gets or sets the StartMeterReading value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public float StartMeterReading { get; set; }

        /// <summary>
        /// Gets or sets the Active value.
        /// </summary>
        public bool Active { get; set; }

        [StringLength(50, ErrorMessage = "*")]
        public string TypeMeter { get; set; }

        [AllowNullForSave]
        public int? LocationId { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>
        [NotMapped]
        public string AssetNumber { get; set; }


        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        [NotMapped]
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [NotMapped]
        public string AssetAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }


        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the L5AltDescription value.
        /// </summary>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L4No value.
        /// </summary>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the L4Description value.
        /// </summary>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the L4AltDescription value.
        /// </summary>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L3No value.
        /// </summary>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the L3Desc value.
        /// </summary>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the L3AltDesc value.
        /// </summary>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the L2Altname value.
        /// </summary>
        [NotMapped]
        public string L2Altname { get; set; }

        [NotMapped]
        public DateTime? ReadingDate { get; set; }

        [NotMapped]
        public DateTime? LastReadingDate { get; set; }

        [NotMapped]
        public float? CurrentReading { get; set; }

        [NotMapped]
        public bool IsValidMeterNo { get; set; }

        [NotMapped]
        public bool IsNotDuplicateMeterNo { get; set; }

        [NotMapped]
        public bool IsValidCityCode { get; set; }

        [NotMapped]
        public bool IsGreaterReading { get; set; }

        [NotMapped]
        public bool IsGreaterReadingDate { get; set; }

        [NotMapped]
        public bool IsAvgDailyInc { get; set; }

        [NotMapped]
        public bool IsSuccess { get; set; }

        [NotMapped]
        public string Reason { get; set; }

        [NotMapped]
        public bool IsValidReadingDate { get; set; }

        [NotMapped]
        public bool IsValidReading { get; set; }

        [NotMapped]
        public int? LocationIDAsset { get; set; }
        [NotMapped]
        public string LocationNoAsset { get; set; }
        [NotMapped]
        public string LocationDescriptionAsset { get; set; }
        [NotMapped]
        public string LocationAltDescriptionAsset { get; set; }

        [NotMapped]
        public bool IsValidGreaterReadingDate { get; set; }

        #endregion
    }
}
