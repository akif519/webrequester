//-----------------------------------------------------------------------
// <copyright file="Dbo_CleaningInspectionGroup_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_CleaningInspectionGroup_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_CleaningInspectionGroup_CT")]
    public sealed class Dbo_CleaningInspectionGroup_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the CleaningInspectionGroupId value.
        /// </summary>
        //public int? CleaningInspectionGroupId { get; set; }

        /// <summary>
        /// Gets or sets the GroupNumber value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string GroupNumber { get; set; }

        /// <summary>
        /// Gets or sets the GroupDescription value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string GroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the ArabicGroupDescription value.
        /// </summary>
        [StringLength(500, ErrorMessage = "*")]
        public string ArabicGroupDescription { get; set; }

        /// <summary>
        /// Gets or sets the Type value.
        /// </summary>
        //[StringLength(200, ErrorMessage = "*")]
        //public string Type { get; set; }

        /// <summary>
        /// Gets or sets the TotalArea value.
        /// </summary>
        [StringLength(200, ErrorMessage = "*")]
        public string TotalArea { get; set; }

        /// <summary>
        /// Gets or sets the Status value.
        /// </summary>
        //public bool? Status { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        //public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        //[NotMapped]
        //public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }
              

        #endregion
    }
}
