﻿//-----------------------------------------------------------------------
// <copyright file="WorkPriorityNotification.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - WorkPriorityNotification
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>03-May-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("WorkPriorityNotification")]
    public sealed class WorkPriorityNotification : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the WorkPriorityNotificationID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkPriorityNotificationID { get; set; }

        /// <summary>
        /// Gets or sets the L1ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int L1ID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the WorkPriorityID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int WorkPriorityID { get; set; }

        /// <summary>
        /// Gets or sets the Response value.
        /// </summary>
        public int? Response { get; set; }

        /// <summary>
        /// Gets or sets the Resolution value.
        /// </summary>
        public int? Resolution { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>        
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>        
        public int ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string WorkPriority { get; set; }

        [NotMapped]
        public string AltWorkPriority { get; set; }

        #endregion
    }
}
