//-----------------------------------------------------------------------
// <copyright file="Dbo_L4_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - dbo_L4_CT
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>17-Nov-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("dbo_L4_CT")]
	public sealed class Dbo_L4_CT : BaseModel
	{
		#region Properties

        ///// <summary>
        ///// Gets or sets the start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the end_lsn value.
        ///// </summary>
        //public byte[] end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] seqval { get; set; }

		
		/// <summary>
		/// Gets or sets the update_mask value.
		/// </summary>
		public byte[] update_mask { get; set; }

        ///// <summary>
        ///// Gets or sets the L4ID value.
        ///// </summary>
        //public int? L4ID { get; set; }

		/// <summary>
		/// Gets or sets the L4No value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string L4No { get; set; }

		/// <summary>
		/// Gets or sets the L4Description value.
		/// </summary>
		[StringLength(1500, ErrorMessage = "*")]
		public string L4Description { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }


        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L3ID value.
        ///// </summary>
        //public int? L3ID { get; set; }

        ///// <summary>
        ///// Gets or sets the L4AltDescription value.
        ///// </summary>
        //[StringLength(1500, ErrorMessage = "*")]
        //public string L4AltDescription { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //[StringLength(100, ErrorMessage = "*")]
        //public string CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedDate value.
        ///// </summary>
        //public DateTime? CreatedDate { get; set; }

        

		#endregion
	}
}
