//-----------------------------------------------------------------------
// <copyright file="MobileConlimit.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using CMMS.Model;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - mobileConlimit
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>16-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("mobileConlimit")]
	public sealed class MobileConlimit : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the ConnectionKey value.
		/// </summary>
		public string ConnectionKey { get; set; }

		/// <summary>
		/// Gets or sets the Conn value.
		/// </summary>
		public string Conn { get; set; }

		#endregion
	}
}
