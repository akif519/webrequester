//-----------------------------------------------------------------------
// <copyright file="Dbo_material_req_master_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_material_req_master_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>30-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_material_req_master_CT")]
    public sealed class Dbo_material_req_master_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }


        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        /// <summary>
        /// Gets or sets the Mr_no value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string Mr_no { get; set; }

        /// <summary>
        /// Gets or sets the Mr_date value.
        /// </summary>
        public string Mr_date { get; set; }

        /// <summary>
        /// Gets or sets the Order_date value.
        /// </summary>
        public string Order_date { get; set; }

        [NotMapped]
        public string Status_Desc { get; set; }

        [NotMapped]
        public string auth_status_desc { get; set; }

        [NotMapped]
        public string RequestBy { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the Requiredby_date value.
        /// </summary>
        public string Requiredby_date { get; set; }

        /// <summary>
        /// Gets or sets the Mr_notes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Mr_notes { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_date value.
        /// </summary>
        public string Cancel_date { get; set; }

        [NotMapped]
        public string CancelBy { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_notes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Cancel_notes { get; set; }

        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string MaintDivisionCode { get; set; }

        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        [NotMapped]
        public string CostCenterNo { get; set; }

        [NotMapped]
        public string AccountCode { get; set; }

        [NotMapped]
        public string SectionSupName { get; set; }

        [NotMapped]
        public string StoreSupName { get; set; }

        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the PrintDate value.
        /// </summary>
        public string PrintDate { get; set; }

        ///// <summary>
        ///// Gets or sets the Status_id value.
        ///// </summary>
        //public int? Status_id { get; set; }

        ///// <summary>
        ///// Gets or sets the Auth_status value.
        ///// </summary>
        //public int? Auth_status { get; set; }

        ///// <summary>
        ///// Gets or sets the Req_by value.
        ///// </summary>
        //public int? Req_by { get; set; }

        ///// <summary>
        ///// Gets or sets the Supplier_Id value.
        ///// </summary>
        //public int? Supplier_Id { get; set; }

       

       

       

        ///// <summary>
        ///// Gets or sets the Cancel_by value.
        ///// </summary>
        //public int? Cancel_by { get; set; }

       

        ///// <summary>
        ///// Gets or sets the Po_no value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Po_no { get; set; }

        ///// <summary>
        ///// Gets or sets the Pr_no value.
        ///// </summary>
        //[StringLength(50, ErrorMessage = "*")]
        //public string Pr_no { get; set; }

        ///// <summary>
        ///// Gets or sets the L2ID value.
        ///// </summary>
        //public int? L2ID { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdeptId value.
        ///// </summary>
        //public int? MaintdeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MainSubDeptId value.
        ///// </summary>
        //public int? MainSubDeptId { get; set; }

        ///// <summary>
        ///// Gets or sets the MaintdivId value.
        ///// </summary>
        //public int? MaintdivId { get; set; }

        ///// <summary>
        ///// Gets or sets the CostCenterID value.
        ///// </summary>
        //public int? CostCenterID { get; set; }

        ///// <summary>
        ///// Gets or sets the Accountcodeid value.
        ///// </summary>
        //public int? Accountcodeid { get; set; }

        ///// <summary>
        ///// Gets or sets the SectionSupId value.
        ///// </summary>
        //public int? SectionSupId { get; set; }

        ///// <summary>
        ///// Gets or sets the StoreSupId value.
        ///// </summary>
        //public int? StoreSupId { get; set; }

        ///// <summary>
        ///// Gets or sets the SubStoreId value.
        ///// </summary>
        //public int? SubStoreId { get; set; }

       

        ///// <summary>
        ///// Gets or sets the Documentstatusid value.
        ///// </summary>
        //public int? Documentstatusid { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy value.
        ///// </summary>
        //public int? CreatedBy { get; set; }

        ///// <summary>
        ///// Gets or sets the CreatedBy Name value.
        ///// </summary>
        //[NotMapped]
        //public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public string ModifiedDate { get; set; }

        ///// <summary>
        ///// Gets or sets the PrintedBy value.
        ///// </summary>
        //public int? PrintedBy { get; set; }

        #endregion
    }
}
