//-----------------------------------------------------------------------
// <copyright file="L5.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - L5
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("L5")]
    public sealed class L5 : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L5ID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int L5ID { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L1ID { get; set; }

        /// <summary>
        /// Gets or sets the L3ID value.
        /// </summary>
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the L4ID value.
        /// </summary>
        public int? L4ID { get; set; }

        /// <summary>
        /// Gets or sets the L5AltDescription value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        [NotMapped]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the sector identifier.
        /// </summary>
        /// <value>
        /// The sector identifier.
        /// </value>
        [NotMapped]
        public int SectorID { get; set; }

        /// <summary>
        /// Gets or sets the sector code.
        /// </summary>
        /// <value>
        /// The sector code.
        /// </value>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector.
        /// </summary>
        /// <value>
        /// The name of the sector.
        /// </value>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the l4 no.
        /// </summary>
        /// <value>
        /// The l4 no.
        /// </value>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the l3 no.
        /// </summary>
        /// <value>
        /// The l3 no.
        /// </value>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [is enable city area].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is enable city area]; otherwise, <c>false</c>.
        /// </value>
        [NotMapped]
        public bool? IsEnableCityArea { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        [NotMapped]
        public bool IsL5Level { get; set; }

        [NotMapped]
        public bool IsAreaEnableOrgWise { get; set; }
        #endregion
    }
}
