﻿//-----------------------------------------------------------------------
// <copyright file="Asset.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - assets
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("assets")]
    public sealed class Asset : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AssetID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssetID { get; set; }

        /// <summary>
        /// Gets or sets the LocationID value.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// Gets or sets the AssetCatID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetCatID { get; set; }

        /// <summary>
        /// Gets or sets the PartsListID value.
        /// </summary>
        public int? PartsListID { get; set; }

        /// <summary>
        /// Gets or sets the ContractorID value.
        /// </summary>
        public int? ContractorID { get; set; }

        /// <summary>
        /// Gets or sets the SupplierID value.
        /// </summary>
        [AllowNullForSave]
        public int? SupplierID { get; set; }

        /// <summary>
        /// Gets or sets the CriticalityID value.
        /// </summary>
        public int? CriticalityID { get; set; }

        /// <summary>
        /// Gets or sets the AssetStatusID value.
        /// </summary>
        public int? AssetStatusID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeID value.
        /// </summary>
        [AllowNullForSave]
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_ContractID value.
        /// </summary>
        [AllowNullForSave]
        public int? Warranty_ContractID { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_ContractID value.
        /// </summary>
        [NotMapped]
        public string Warranty_contract { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_ContractID value.
        /// </summary>
        [NotMapped]
        public string AltWarranty_contract { get; set; }

        /// <summary>
        /// Gets or sets the AssetNumber value.
        /// </summary>

        [StringLength(50, ErrorMessage = "*")]
        public string AssetNumber { get; set; }

        /// <summary>
        /// Gets or sets the AssetDescription value.
        /// </summary>
        public string AssetDescription { get; set; }

        /// <summary>
        /// Gets or sets the AssetAltDescription value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string AssetAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the ModelNumber value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        [StringLength(255, ErrorMessage = "*")]
        public string ModelNumber { get; set; }

        /// <summary>
        /// Gets or sets the SerialNumber value.
        /// </summary>
        [StringLength(255, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SerialNumber { get; set; }

        /// <summary>
        /// Gets or sets the DateAcquired value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? DateAcquired { get; set; }

        /// <summary>
        /// Gets or sets the PurchasePrice value.
        /// </summary>
        [AllowNullForSave]
        public double? PurchasePrice { get; set; }

        /// <summary>
        /// Gets or sets the Comments value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo1 value.
        /// </summary>
        public string NameplateInfo1 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo2 value.
        /// </summary>
        public string NameplateInfo2 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo3 value.
        /// </summary>
        public string NameplateInfo3 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo4 value.
        /// </summary>
        public string NameplateInfo4 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo5 value.
        /// </summary>
        public string NameplateInfo5 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo6 value.
        /// </summary>       
        public string NameplateInfo6 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo7 value.
        /// </summary>
        public string NameplateInfo7 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo8 value.
        /// </summary>
        public string NameplateInfo8 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo9 value.
        /// </summary>       
        public string NameplateInfo9 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo10 value.
        /// </summary>
        public string NameplateInfo10 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo11 value.
        /// </summary>
        public string NameplateInfo11 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo12 value.
        /// </summary>
        public string NameplateInfo12 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo13 value.
        /// </summary>
        public string NameplateInfo13 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo14 value.
        /// </summary>
        public string NameplateInfo14 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo15 value.
        /// </summary>
        public string NameplateInfo15 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo16 value.
        /// </summary>
        public string NameplateInfo16 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo17 value.
        /// </summary>
        public string NameplateInfo17 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo18 value.
        /// </summary>
        public string NameplateInfo18 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo19 value.
        /// </summary>
        public string NameplateInfo19 { get; set; }

        /// <summary>
        /// Gets or sets the NameplateInfo20 value.
        /// </summary>
        public string NameplateInfo20 { get; set; }

        /// <summary>
        /// Gets or sets the NextSchedMaint value.
        /// </summary>
        public DateTime? NextSchedMaint { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_ContractExpiry value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? Warranty_ContractExpiry { get; set; }

        /// <summary>
        /// Gets or sets the Warranty_ContractNotes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Warranty_ContractNotes { get; set; }

        /// <summary>
        /// Gets or sets the DataDisposed value.
        /// </summary>
        [AllowNullForSave]
        public DateTime? DataDisposed { get; set; }

        /// <summary>
        /// Gets or sets the NotesToTech value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NotesToTech { get; set; }

        /// <summary>
        /// Gets or sets the EstLife value.
        /// </summary>
        [AllowNullForSave]
        public double? EstLife { get; set; }

        /// <summary>
        /// Gets or sets the Manufacturer value.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// Gets or sets the WorkTradeID value.
        /// </summary>
        [AllowNullForSave]
        public int? WorkTradeID { get; set; }

        /// <summary>
        /// Gets or sets the GroupID value.
        /// </summary>
        public int? GroupID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the AssetSubCatID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetSubCatID { get; set; }

        /// <summary>
        /// Gets or sets the AssetConditionID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetConditionID { get; set; }

        /// <summary>
        /// Gets or sets the AssetClassID value.
        /// </summary>
        [AllowNullForSave]
        public int? AssetClassID { get; set; }

        /// <summary>
        /// Gets or sets the asset status desc.
        /// </summary>
        /// <value>
        /// The asset status desc.
        /// </value>
        [NotMapped]
        public string AssetStatusDesc { get; set; }

        /// <summary>
        /// Gets or sets the alt asset status desc.
        /// </summary>
        /// <value>
        /// The alt asset status desc.
        /// </value>
        [NotMapped]
        public string AltAssetStatusDesc { get; set; }

        /// <summary>
        /// Gets or sets the location no.
        /// </summary>
        /// <value>
        /// The location no.
        /// </value>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the NoteToTech.
        /// </summary>
        /// <value>
        /// The NoteToTech
        /// </value>
        [NotMapped]
        public string NoteToTech { get; set; }

        /// <summary>
        /// Gets or sets the location description.
        /// </summary>
        /// <value>
        /// The location description.
        /// </value>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the location alt description.
        /// </summary>
        /// <value>
        /// The location alt description.
        /// </value>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the asset cat code.
        /// </summary>
        /// <value>
        /// The asset cat code.
        /// </value>
        [NotMapped]
        public string AssetCatCode { get; set; }

        /// <summary>
        /// Gets or sets the asset sub cat code.
        /// </summary>
        /// <value>
        /// The asset sub cat code.
        /// </value>
        [NotMapped]
        public string AssetSubCatCode { get; set; }

        /// <summary>
        /// Gets or sets the asset condition code.
        /// </summary>
        /// <value>
        /// The asset condition code.
        /// </value>
        [NotMapped]
        public string AssetConditionCode { get; set; }

        /// <summary>
        /// Gets or sets the asset class code.
        /// </summary>
        /// <value>
        /// The asset class code.
        /// </value>
        [NotMapped]
        public string AssetClassCode { get; set; }

        /// <summary>
        /// Gets or sets the asset class.
        /// </summary>
        /// <value>
        /// The asset class.
        /// </value>
        [NotMapped]
        public string AssetClass { get; set; }

        /// <summary>
        /// Gets or sets the alt asset class.
        /// </summary>
        /// <value>
        /// The alt asset class.
        /// </value>
        [NotMapped]
        public string AltAssetClass { get; set; }

        /// <summary>
        /// Gets or sets the l2 code.
        /// </summary>
        /// <value>
        /// The l2 code.
        /// </value>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2.
        /// </summary>
        /// <value>
        /// The name of the l2.
        /// </value>
        [NotMapped]
        public string L2Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the l2 alt.
        /// </summary>
        /// <value>
        /// The name of the l2 alt.
        /// </value>
        [NotMapped]
        public string L2AltName { get; set; }

        /// <summary>
        /// Gets or sets the l2 class code.
        /// </summary>
        /// <value>
        /// The l2 class code.
        /// </value>
        [NotMapped]
        public string L2ClassCode { get; set; }

        /// <summary>
        /// Gets or sets the l3 id.
        /// </summary>
        /// <value>
        /// The l3 id.
        /// </value>
        [NotMapped]
        public int? L3ID { get; set; }

        /// <summary>
        /// Gets or sets the l3 no.
        /// </summary>
        /// <value>
        /// The l3 no.
        /// </value>
        [NotMapped]
        public string L3No { get; set; }

        /// <summary>
        /// Gets or sets the l3 desc.
        /// </summary>
        /// <value>
        /// The l3 desc.
        /// </value>
        [NotMapped]
        public string L3Desc { get; set; }

        /// <summary>
        /// Gets or sets the l3 alt desc.
        /// </summary>
        /// <value>
        /// The l3 alt desc.
        /// </value>
        [NotMapped]
        public string L3AltDesc { get; set; }

        /// <summary>
        /// Gets or sets the l4 ID.
        /// </summary>
        /// <value>
        /// The l4 ID.
        /// </value>
        [NotMapped]
        public string L4ID { get; set; }

        /// <summary>
        /// Gets or sets the l4 no.
        /// </summary>
        /// <value>
        /// The l4 no.
        /// </value>
        [NotMapped]
        public string L4No { get; set; }

        /// <summary>
        /// Gets or sets the l4 description.
        /// </summary>
        /// <value>
        /// The l4 description.
        /// </value>
        [NotMapped]
        public string L4Description { get; set; }

        /// <summary>
        /// Gets or sets the l4 alt description.
        /// </summary>
        /// <value>
        /// The l4 alt description.
        /// </value>
        [NotMapped]
        public string L4AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the l5 ID.
        /// </summary>
        /// <value>
        /// The l5 ID.
        /// </value>
        [NotMapped]
        public string L5ID { get; set; }

        /// <summary>
        /// Gets or sets the l5 no.
        /// </summary>
        /// <value>
        /// The l5 no.
        /// </value>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the l5 description.
        /// </summary>
        /// <value>
        /// The l5 description.
        /// </value>
        [NotMapped]
        public string L5Description { get; set; }

        /// <summary>
        /// Gets or sets the l5 alt description.
        /// </summary>
        /// <value>
        /// The l5 alt description.
        /// </value>
        [NotMapped]
        public string L5AltDescription { get; set; }

        /// <summary>
        /// Gets or sets the sector code.
        /// </summary>
        /// <value>
        /// The sector code.
        /// </value>
        [NotMapped]
        public string SectorCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector.
        /// </summary>
        /// <value>
        /// The name of the sector.
        /// </value>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the name of the sector alt.
        /// </summary>
        /// <value>
        /// The name of the sector alt.
        /// </value>
        [NotMapped]
        public string SectorAltName { get; set; }

        /// <summary>
        /// Gets or sets the l2 identifier.
        /// </summary>
        /// <value>
        /// The l2 identifier.
        /// </value>
        [NotMapped]
        public int L2ID { get; set; }

        /// <summary>
        /// Gets or sets the central.
        /// </summary>
        /// <value>
        /// The central.
        /// </value>
        [NotMapped]
        public int Central { get; set; }

        /// <summary>
        /// Gets or sets the employee no.
        /// </summary>
        /// <value>
        /// The employee no.
        /// </value>
        [NotMapped]
        public string EmployeeNO { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt.
        /// </summary>
        /// <value>
        /// The name of the alt.
        /// </value>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or Sets Location Authorised Employee Name.
        /// </summary>
        [NotMapped]
        public int? LocationAuthEmpID { get; set; }

        /// <summary>
        /// Gets or Sets Location Authorised Employee Name.
        /// </summary>
        [NotMapped]
        public string LocationAuthEmployeeName { get; set; }

        /// <summary>
        /// Gets or sets the supplier no.
        /// </summary>
        /// <value>
        /// The supplier no.
        /// </value>
        [NotMapped]
        public string SupplierNo { get; set; }

        /// <summary>
        /// Gets or sets the name of the supplier.
        /// </summary>
        /// <value>
        /// The name of the supplier.
        /// </value>
        [NotMapped]
        public string SupplierName { get; set; }

        /// <summary>
        /// Gets or sets the name of the alt supplier.
        /// </summary>
        /// <value>
        /// The name of the alt supplier.
        /// </value>
        [NotMapped]
        public string AltSupplierName { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [NotMapped]
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the parts list no.
        /// </summary>
        /// <value>
        /// The parts list no.
        /// </value>
        [NotMapped]
        public string PartsListNo { get; set; }

        /// <summary>
        /// Gets or sets the parts list description.
        /// </summary>
        /// <value>
        /// The parts list description.
        /// </value>
        [NotMapped]
        public string PartsListDescription { get; set; }

        /// <summary>
        /// Gets or sets the parts list alt description.
        /// </summary>
        /// <value>
        /// The parts list alt description.
        /// </value>
        [NotMapped]
        public string PartsListAltDescription { get; set; }

        /// <summary>
        /// Gets or Sets the AssetWarrentyStatus Value
        /// </summary>
        [NotMapped]
        public string AssetWarrentyStatus { get; set; }

        [NotMapped]
        public int HasChild { get; set; }

        [NotMapped]
        public int RelationAssetID { get; set; }

        [NotMapped]
        public int RelationChildID { get; set; }

        [NotMapped]
        public int fromPageID { get; set; }

        [NotMapped]
        public string AssetCategory { get; set; }

        [NotMapped]
        public string AltAssetCategory { get; set; }

        [NotMapped]
        public DateTime? TargetStartDate { get; set; }

        [NotMapped]
        public DateTime? TargetCompDate { get; set; }

        [NotMapped]
        public DateTime? NextDate { get; set; }

        [NotMapped]
        public bool? IsInclude { get; set; }

        [NotMapped]
        public int? L1ID { get; set; }

        #endregion

        #region additional webservice Properties

        //AssetSubCategory
        public string AssetSubCategory { get; set; }
        //AltAssetSubCategory
        public string AltAssetSubCategory { get; set; }
        //AssetConditionID	
        public string AssetCondition { get; set; }
        public string AltAssetCondition { get; set; }
        //CriticalityID
        public string Criticality { get; set; }
        public string AltCriticality { get; set; }

        //WorkTradeID        
        public string WorkTrade { get; set; }
        public string AltWorkTrade { get; set; }

        public string EmployeeName { get; set; }
        public string EmployeeAltName { get; set; }

        //Comments        
        public string AssetNotes { get; set; }

        //DateAcquired
        public DateTime? CommissionedDate { get; set; }

        public decimal? CurrentValue { get; set; }

        public List<PartsListDetail> PartslistdetailsTable { get; set; }
        public List<AssetRelationship> AssetrelationshipTable { get; set; }
        public List<Reading> PMMeterHistory { get; set; }
        public List<PmMeter> PMMeterSchedule { get; set; }
        public List<_WorkOrder> JoHistory { get; set; }
        public List<ExtAssetFile> ExtassetfileTable { get; set; }

        public List<AssetSpecification> SpecificationTable { get; set; }

        #endregion
    }
}
