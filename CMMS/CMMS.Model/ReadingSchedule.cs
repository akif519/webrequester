﻿//-----------------------------------------------------------------------
// <copyright file="ReadingSchedule.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - ReadingSchedule
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("ReadingSchedule")]
    public sealed class ReadingSchedule : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ReadingScheduleID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReadingScheduleID { get; set; }

        /// <summary>
        /// Gets or sets the MeterID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int MeterID { get; set; }

        /// <summary>
        /// Gets or sets the ReadingID value.
        /// </summary>
        public int? ReadingID { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the MeterDescription value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string MeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltMeterDescription value.
        /// </summary>
        [StringLength(1500, ErrorMessage = "*")]
        public string AltMeterDescription { get; set; }

        /// <summary>
        /// Gets or sets the Reading value.
        /// </summary>
        public float? Reading { get; set; }

        /// <summary>
        /// Gets or sets the GenStatus value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string GenStatus { get; set; }

        /// <summary>
        /// Gets or sets the GenDate value.
        /// </summary>
        public DateTime? GenDate { get; set; }

        /// <summary>
        /// Gets or sets the LastReading value.
        /// </summary>
        public float? LastReading { get; set; }

        /// <summary>
        /// Gets or sets the LastPMDue value.
        /// </summary>
        public float? LastPMDue { get; set; }

        /// <summary>
        /// Gets or sets the LowerLimit value.
        /// </summary>
        public float? LowerLimit { get; set; }

        /// <summary>
        /// Gets or sets the UpperLimit value.
        /// </summary>
        public float? UpperLimit { get; set; }

        /// <summary>
        /// Gets or sets the Enterdate value.
        /// </summary>
        public DateTime? Enterdate { get; set; }

        /// <summary>
        /// Gets or sets the PMActive value.
        /// </summary>
        public int? PMActive { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string MeterMasterNo { get; set; }

        [NotMapped]
        public string MeterNo { get; set; }

        /// <summary>
        /// Gets or sets the PMType value.
        /// </summary>
        [NotMapped]
        public int? PMType { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [NotMapped]
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the ReadingDate value.
        /// </summary>
        [NotMapped]
        public DateTime? ReadingDate { get; set; }

        /// <summary>
        /// Gets or sets the temp ReadingDate value.
        /// </summary>
        [NotMapped]
        public string TempReadingDate { get; set; }
        #endregion
    }

    public class ReadingGraph
    {
        public string MonthYear { get; set; }
        public int Reading { get; set; }
        public int AvgUpperLimit { get; set; }
        public int AvgLowerLimit { get; set; }
        public string color { get; set; }
    }
}
