//-----------------------------------------------------------------------
// <copyright file="Material_req_master.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - material_req_master
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("material_req_master")]
    public sealed class Material_req_master : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the Mr_no value.
        /// </summary>
        public string Mr_no { get; set; }

        /// <summary>
        /// Gets or sets the Mr_date value.
        /// </summary>
        public DateTime? Mr_date { get; set; }

        /// <summary>
        /// Gets or sets the Order_date value.
        /// </summary>
        public DateTime? Order_date { get; set; }

        /// <summary>
        /// Gets or sets the Status_id value.
        /// </summary>
        public int? Status_id { get; set; }

        /// <summary>
        /// Gets or sets the Auth_status value.
        /// </summary>
        public int? Auth_status { get; set; }

        /// <summary>
        /// Gets or sets the Req_by value.
        /// </summary>
        public int? Req_by { get; set; }

        /// <summary>
        /// Gets or sets the Supplier_Id value.
        /// </summary>
        public int? Supplier_Id { get; set; }

        /// <summary>
        /// Gets or sets the Requiredby_date value.
        /// </summary>
        public DateTime? Requiredby_date { get; set; }

        /// <summary>
        /// Gets or sets the Mr_notes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Mr_notes { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_date value.
        /// </summary>
        public DateTime? Cancel_date { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_by value.
        /// </summary>
        public int? Cancel_by { get; set; }

        /// <summary>
        /// Gets or sets the Cancel_notes value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Cancel_notes { get; set; }

        /// <summary>
        /// Gets or sets the Po_no value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Po_no { get; set; }

        /// <summary>
        /// Gets or sets the Pr_no value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Pr_no { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the MaintdeptId value.
        /// </summary>
        public int? MaintdeptId { get; set; }

        /// <summary>
        /// Gets or sets the MainSubDeptId value.
        /// </summary>
        public int? MainSubDeptId { get; set; }

        /// <summary>
        /// Gets or sets the MaintdivId value.
        /// </summary>
        public int? MaintdivId { get; set; }

        /// <summary>
        /// Gets or sets the CostCenterID value.
        /// </summary>
        public int? CostCenterID { get; set; }

        /// <summary>
        /// Gets or sets the Accountcodeid value.
        /// </summary>
        public int? Accountcodeid { get; set; }

        /// <summary>
        /// Gets or sets the SectionSupId value.
        /// </summary>
        public int? SectionSupId { get; set; }

        /// <summary>
        /// Gets or sets the StoreSupId value.
        /// </summary>
        public int? StoreSupId { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreId value.
        /// </summary>
        public int? SubStoreId { get; set; }

        /// <summary>
        /// Gets or sets the PrintDate value.
        /// </summary>
        public DateTime? PrintDate { get; set; }

        /// <summary>
        /// Gets or sets the Documentstatusid value.
        /// </summary>
        public int? Documentstatusid { get; set; }

        [NotMapped]
        public string L2Code { get; set; }


        [NotMapped]
        public string MaintDivisionCode { get; set; }


        [NotMapped]
        public string MaintDeptCode { get; set; }

        [NotMapped]
        public string MaintSubDeptCode { get; set; }

        [NotMapped]
        public string status_desc { get; set; }

        [NotMapped]
        public string auth_status_desc { get; set; }

        [NotMapped]
        public string RequestedByName { get; set; }

        [NotMapped]
        public string RequestedByNo { get; set; }

        [NotMapped]
        public string SupplierNo { get; set; }

        [NotMapped]
        public string SupplierName { get; set; }

        [NotMapped]
        public string AltSupplierName { get; set; }

        [NotMapped]
        public string SectionSupName { get; set; }

        [NotMapped]
        public string SectionSupNo { get; set; }

        [NotMapped]
        public string StoreSupName { get; set; }

        [NotMapped]
        public string StoreSupNo { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the PrintedBy value.
        /// </summary>
        public int? PrintedBy { get; set; }

        [NotMapped]
        public string JsonlstItemModel { get; set; }

        [NotMapped]
        public bool IsAnyPOItemReceivedOrCancelled { get; set; }

        [NotMapped]
        public List<Material_req_detail> lstMaterialreqdetail { get; set; }

        [NotMapped]
        public string workOrderNo { get; set; }

        #endregion
    }
}
