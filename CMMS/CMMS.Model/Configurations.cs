﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - Configurations
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Configurations")]
    public sealed class Configurations
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ConfigID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)] 
        public int ConfigID { get; set; }

        /// <summary>
        /// Gets or sets the ConfigKey value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ConfigKey { get; set; }

        /// <summary>
        /// Gets or sets the ConfigValue value.
        /// </summary>
        [StringLength(300, ErrorMessage = "*")]
        public string ConfigValue { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        [StringLength(800, ErrorMessage = "*")]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the job order assigned to status.
        /// </summary>
        /// <value>
        /// The job order assigned to status.
        /// </value>
        [NotMapped]
        public string JobOrderAssignedToStatus { get; set; }

        /// <summary>
        /// Gets or sets the smtp_server_port.
        /// </summary>
        /// <value>
        /// The smtp_server_port.
        /// </value>
        [NotMapped]
        public string TechnicianCategory { get; set; }

        /// <summary>
        /// Gets or sets the sub store consume.
        /// </summary>
        /// <value>
        /// The sub store consume.
        /// </value>
        [NotMapped]
        public string SubStoreConsume { get; set; }

        /// <summary>
        /// Gets or sets the sub store personal.
        /// </summary>
        /// <value>
        /// The sub store personal.
        /// </value>
        [NotMapped]
        public string SubStorePersonal { get; set; }

        /// <summary>
        /// Gets or sets the city store.
        /// </summary>
        /// <value>
        /// The city store.
        /// </value>
        [NotMapped]
        public string CityStore { get; set; }

        /// <summary>
        /// Gets or sets the job order material requested status.
        /// </summary>
        /// <value>
        /// The job order material requested status.
        /// </value>
        [NotMapped]
        public string JobOrderMaterialRequestedStatus { get; set; }

        #endregion
    }
}
