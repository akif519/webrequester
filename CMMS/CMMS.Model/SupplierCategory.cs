﻿//-----------------------------------------------------------------------
// <copyright file="SupplierCategory.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - SupplierCategory
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("SupplierCategory")]
    public sealed class SupplierCategory : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CategoryID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int CategoryID { get; set; }

        /// <summary>
        /// Gets or sets the SupplierCategoryName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string SupplierCategoryName { get; set; }

        /// <summary>
        /// Gets or sets the SupplierCategoryAltName value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string SupplierCategoryAltName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(10, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
