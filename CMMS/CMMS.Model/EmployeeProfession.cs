﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - EmployeeProfession
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("EmployeeProfession")]
    public sealed class EmployeeProfession : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the EmployeeProfID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployeeProfID { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeProfCode value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(100, ErrorMessage = "*")]
        public string EmployeeProfCode { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeProfName value.
        /// </summary>
        [StringLength(300, ErrorMessage = "*")]
        public string EmployeeProfName { get; set; }

        /// <summary>
        /// Gets or sets the EmployeeProfNameArabic value.
        /// </summary>
        [StringLength(300, ErrorMessage = "*")]
        public string EmployeeProfNameArabic { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }
}
