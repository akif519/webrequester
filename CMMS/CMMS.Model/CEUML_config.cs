﻿//-----------------------------------------------------------------------
// <copyright file="CEUML_config.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - CEUML_config
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>14-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("CEUML_config")]
    public sealed class CEUML_config : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the CEUML_configId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int CEUML_configId { get; set; }

        /// <summary>
        /// Gets or sets the ClientID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int ClientID { get; set; }

        /// <summary>
        /// Gets or sets the CeUser value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string CeUser { get; set; }

        /// <summary>
        /// Gets or sets the Token value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string Token { get; set; }

        #endregion
    }
}
