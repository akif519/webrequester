//-----------------------------------------------------------------------
// <copyright file="HelpChapter.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - HelpChapter
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>20-Dec-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("HelpChapter")]
	public sealed class HelpChapter : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the HelpChapterID value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int HelpChapterID { get; set; }

		/// <summary>
		/// Gets or sets the HelpModuleDetailID value.
		/// </summary>
		public int? HelpModuleDetailID { get; set; }

		/// <summary>
		/// Gets or sets the HelpChapterTitle value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string HelpChapterTitle { get; set; }

		/// <summary>
		/// Gets or sets the HelpChapterDescription value.
		/// </summary>
		[StringLength(10000, ErrorMessage = "*")]
		public string HelpChapterDescription { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedOn value.
		/// </summary>
		public DateTime? CreatedOn { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedOn value.
		/// </summary>
		public DateTime? ModifiedOn { get; set; }

		#endregion
	}
}
