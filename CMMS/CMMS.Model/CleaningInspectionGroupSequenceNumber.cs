//-----------------------------------------------------------------------
// <copyright file="CleaningInspectionGroupSequenceNumber.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - cleaningInspectionGroupSequenceNumber
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cleaningInspectionGroupSequenceNumber")]
    public sealed class CleaningInspectionGroupSequenceNumber : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the SequenceId value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SequenceId { get; set; }

        /// <summary>
        /// Gets or sets the CleaningInspectionGroupId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int CleaningInspectionGroupId { get; set; }

        /// <summary>
        /// Gets or sets the SequenceNumber value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int SequenceNumber { get; set; }

        /// <summary>
        /// Gets or sets the LocationId value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int LocationId { get; set; }

        /// <summary>
        /// Gets or sets the TaskDesc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string TaskDesc { get; set; }

        /// <summary>
        /// Gets or sets the AltTaskDesc value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string AltTaskDesc { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        [NotMapped]
        public string LocationNo { get; set; }

        [NotMapped]
        public string LocationDescription { get; set; }

        [NotMapped]
        public string LocationAltDescription { get; set; }

        #endregion
    }
}
