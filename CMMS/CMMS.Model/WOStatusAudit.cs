//-----------------------------------------------------------------------
// <copyright file="WOStatusAudit.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - WOStatusAudit
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>24-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("WOStatusAudit")]
	public sealed class WOStatusAudit : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Gets or sets the WONo value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string WONo { get; set; }

		/// <summary>
		/// Gets or sets the WOStatus value.
		/// </summary>
		public int? WOStatus { get; set; }

		/// <summary>
		/// Gets or sets the Date value.
		/// </summary>
		public DateTime? Date { get; set; }

		/// <summary>
		/// Gets or sets the EmpID value.
		/// </summary>
		public int? EmpID { get; set; }

		/// <summary>
		/// Gets or sets the Source value.
		/// </summary>
		[StringLength(30, ErrorMessage = "*")]
		public string Source { get; set; }

		/// <summary>
		/// Gets or sets the Latitude value.
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the Longitude value.
		/// </summary>
		public float? Longitude { get; set; }

		#endregion
	}
}
