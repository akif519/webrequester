﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    public class AssetListFilterModel
    {
        public int L2ID { get; set; }
    }

    public class AssetMaintenanceHistoryListFilterModel
    {
        public int L2ID { get; set; }
        public int AssetId { get; set; }
    }

    public class AssetMaintenanceHistoryNoCostListFilterModel
    {
        public int L2ID { get; set; }
        public int AssetId { get; set; }
    }

    public class Top10AssetDownTimeFilterModel
    {
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }

        public DateTime joDatefrom { get; set; }
        public DateTime joDateTo { get; set; }
    }

    public class Top10AssetBreakDownFilterModel
    {
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }

        public DateTime joDatefrom { get; set; }
        public DateTime joDateTo { get; set; }
    }

    public class JobOrderListFilterModel
    {
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int maintDivisionId { get; set; }
        public string maintDeptId { get; set; }
        public string maintSubDeptId { get; set; }
        public int locationId { get; set; }
        public DateTime joDatefrom { get; set; }
        public DateTime joDateTo { get; set; }
    }

    public class PMJobOrderListFilterModel
    {
        public int L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public int maintDivisionId { get; set; }
        public string maintDeptId { get; set; }
        public string maintSubDeptId { get; set; }
        public int locationId { get; set; }
        public DateTime joDatefrom { get; set; }
        public DateTime joDateTo { get; set; }
    }

    public class ReportSearchModel
    {
        public int PageId { get; set; }
        public string PageName { get; set; }
        public int L1ID { get; set; }
        public string L2ID { get; set; }
        public string L3ID { get; set; }
        public string L4ID { get; set; }
        public string L5ID { get; set; }
        public string MaintDivID { get; set; }
        public string MaintDeptID { get; set; }
        public string MaintSubDeptID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int AssetId { get; set; }
        public int LocationId { get; set; }
        public string AssetCategoryId { get; set; }
        public string WorkTypeId { get; set; }
        public string WorkTradeId { get; set; }
        public string WorkPriorityId { get; set; }
        public string WorkStatusId { get; set; }
        public string CostCenterId { get; set; }
        public int Aging { get; set; }
        public bool IncludeLabourCost { get; set; }
        public int ChecklistId { get; set; }
        public int ForecastOption { get; set; }
        public string EmployeeCategoryId { get; set; }
        public string EmployeeStatusId { get; set; }
        public string RequestStatusId { get; set; }
        public string ApprovalStatusId { get; set; }
        public string OrderStatusId { get; set; }
        public string DeliveryStatusId { get; set; }
        public int SupplierId { get; set; }
        public int CleaningGroupId { get; set; }
        public int InspectedBy{ get; set; }
        public int ItemTypeId { get; set; }
        public int ItemSubTypeId { get; set; }
        public int ItemStatusId { get; set; }
        public int SubStoreId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string JobOrderNo { get; set; }
    }

    public class ReportSearchPageModel
    {
        public int PageId { get; set; }
        public string PageName { get; set; }
        public string ReloadAction { get; set; }
        public string ReloadController { get; set; }
    }
}
