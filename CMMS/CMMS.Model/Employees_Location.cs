﻿namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Employees_Location
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Employees_Location")]
    public sealed class Employees_Location : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int LocationID { get; set; }

        /// <summary>
        /// Gets or sets the LocationNo value.
        /// </summary>
        [NotMapped]
        public string LocationNo { get; set; }

        /// <summary>
        /// Gets or sets the LocationDescription value.
        /// </summary>
        [NotMapped]
        public string LocationDescription { get; set; }

        /// <summary>
        /// Gets or sets the LocationAltDescription value.
        /// </summary>
        [NotMapped]
        public string LocationAltDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public bool ChkLocation { get; set; }

        /// <summary>
        /// Gets or sets the IsInserted value. If Value is to be inserted then 
        /// it will be true or value is to be deleted then it will be false
        /// </summary>
        [NotMapped]
        public bool IsInserted { get; set; }

        /// <summary>
        /// Gets or sets the EmpID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int EmpID { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the L2Code value.
        /// </summary>
        [NotMapped]
        public string L2Code { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the L2Altname value.
        /// </summary>
        [NotMapped]
        public string L2Altname { get; set; }

        /// <summary>
        /// Gets or sets the L1No value.
        /// </summary>
        [NotMapped]
        public string L1No { get; set; }

        /// <summary>
        /// Gets or sets the L1Name value.
        /// </summary>
        [NotMapped]
        public string L1Name { get; set; }

        /// <summary>
        /// Gets or sets the L1AltName value.
        /// </summary>
        [NotMapped]
        public string L1AltName { get; set; }

        /// <summary>
        /// Gets or sets the L5No value.
        /// </summary>
        [NotMapped]
        public string L5No { get; set; }

        /// <summary>
        /// Gets or sets the L5Description value.
        /// </summary>
        [NotMapped]
        public string L5Description { get; set; }

        [NotMapped]
        public string L5AltDescription { get; set; }
        #endregion
    }
}
