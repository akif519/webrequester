//-----------------------------------------------------------------------
// <copyright file="Dbo_PurchaseOrderItems_CT.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - dbo_PurchaseOrderItems_CT
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>07-Apr-2017</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("dbo_PurchaseOrderItems_CT")]
    public sealed class Dbo_PurchaseOrderItems_CT : BaseModel
    {
        #region Properties

        ///// <summary>
        ///// Gets or sets the __$start_lsn value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$start_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$end_lsn value.
        ///// </summary>
        //public byte[] __$end_lsn { get; set; }

        ///// <summary>
        ///// Gets or sets the __$seqval value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public byte[] __$seqval { get; set; }

        ///// <summary>
        ///// Gets or sets the __$operation value.
        ///// </summary>
        //[Required(ErrorMessage = "*")]
        //public int __$operation { get; set; }

        /// <summary>
        /// Gets or sets the __$update_mask value.
        /// </summary>
        public byte[] update_mask { get; set; }

        [NotMapped]
        public string PurchaseOrderNo { get; set; }       

        /// <summary>
        /// Gets or sets the PartDescription value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string PartDescription { get; set; }

        /// <summary>
        /// Gets or sets the JobOrderID value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string JobOrderID { get; set; }

        /// <summary>
        /// Gets or sets the UoM value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string UoM { get; set; }

        /// <summary>
        /// Gets or sets the Quantity value.
        /// </summary>
        public decimal? Quantity { get; set; }

        /// <summary>
        /// Gets or sets the UnitPrice value.
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Gets or sets the Discount value.
        /// </summary>
        public decimal? Discount { get; set; }

        /// <summary>
        /// Gets or sets the Tax value.
        /// </summary>
        public decimal? Tax { get; set; }

        /// <summary>
        /// Gets or sets the LineCost value.
        /// </summary>
        public decimal? LineCost { get; set; }

        /// <summary>
        /// Gets or sets the BaseCost value.
        /// </summary>
        public decimal? BaseCost { get; set; }

        /// <summary>
        /// Gets or sets the Source value.
        /// </summary>
        [StringLength(1, ErrorMessage = "*")]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the PurchaseRequestNo value.
        /// </summary>
        [StringLength(20, ErrorMessage = "*")]
        public string PurchaseRequestNo { get; set; }


        /// <summary>
        /// Gets or sets the altaprovel status value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the alt stock description.
        /// </summary>
        /// <value>
        /// The alt stock description.
        /// </value>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the __$operation value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int operation { get; set; }

        public string ModifiedBy { get; set; }

        #endregion
    }
}
