//-----------------------------------------------------------------------
// <copyright file="LoginLog.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - LoginLog
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("LoginLog")]
	public sealed class LoginLog : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the LoginLogIn value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int LoginLogIn { get; set; }

		/// <summary>
		/// Gets or sets the EmployeeId value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		public int EmployeeId { get; set; }

		/// <summary>
		/// Gets or sets the LoginDateTime value.
		/// </summary>
		public DateTime? LoginDateTime { get; set; }

		/// <summary>
		/// Gets or sets the LogoutDateTime value.
		/// </summary>
		public DateTime? LogoutDateTime { get; set; }

		/// <summary>
		/// Gets or sets the IPAddress value.
		/// </summary>
		[StringLength(50, ErrorMessage = "*")]
		public string IPAddress { get; set; }

		/// <summary>
		/// Gets or sets the Latitude value.
		/// </summary>
		public float? Latitude { get; set; }

		/// <summary>
		/// Gets or sets the Longitude value.
		/// </summary>
		public float? Longitude { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [NotMapped]
        public string NAME { get; set; }

		#endregion
	}
}
