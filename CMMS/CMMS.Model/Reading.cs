﻿//-----------------------------------------------------------------------
// <copyright file="Reading.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    /// <summary>
    /// This class is used to Define Model for Table - Reading
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("Reading")]
    public sealed class Reading : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ReadingID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReadingID { get; set; }

        /// <summary>
        /// Gets or sets the MeterMasterID value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        public int MeterMasterID { get; set; }

        /// <summary>
        /// Gets or sets the ReadingDate value.
        /// </summary>
        public DateTime? ReadingDate { get; set; }

        /// <summary>
        /// Gets or sets the CurrentReading value.
        /// </summary>
        public float? CurrentReading { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the meter no.
        /// </summary>
        /// <value>
        /// The meter no.
        /// </value>
        [NotMapped]
        public string MeterNo { get; set; }


        /// <summary>
        /// Gets or sets the meter no.
        /// </summary>
        /// <value>
        /// The meter no.
        /// </value>
        [NotMapped]
        public int? AssetID { get; set; }

        #endregion
    }
}
