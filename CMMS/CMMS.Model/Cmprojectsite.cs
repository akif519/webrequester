//-----------------------------------------------------------------------
// <copyright file="Cmprojectsite.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - cmprojectsite
	/// </summary>
	/// <CreatedBy></CreatedBy>
	/// <CreatedDate>24-May-2017</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("cmprojectsite")]
	public sealed class Cmprojectsite : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the Id value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the ProjectReqNo value.
		/// </summary>
		[Required(ErrorMessage = "*")]
		[StringLength(50, ErrorMessage = "*")]
		public string ProjectReqNo { get; set; }

		/// <summary>
		/// Gets or sets the SectorID value.
		/// </summary>
		public int? SectorID { get; set; }

		/// <summary>
		/// Gets or sets the L2Id value.
		/// </summary>
		public int? L2Id { get; set; }

		#endregion
	}
}
