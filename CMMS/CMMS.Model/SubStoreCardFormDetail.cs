//-----------------------------------------------------------------------
// <copyright file="SubStoreCardFormDetail.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Model
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	/// <summary>
	/// This class is used to Define Model for Table - SubStoreCardFormDetail
	/// </summary>
	/// <CreatedBy>Nikhil Rupada</CreatedBy>
	/// <CreatedDate>22-Aug-2016</CreatedDate>
	/// <ModifiedBy></ModifiedBy>
	/// <ModifiedDate></ModifiedDate>
	/// <ReviewBy></ReviewBy>
	/// <ReviewDate></ReviewDate>
	[Table("SubStoreCardFormDetail")]
	public sealed class SubStoreCardFormDetail : BaseModel
	{
		#region Properties

		/// <summary>
		/// Gets or sets the CardIssueId value.
		/// </summary>
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int CardIssueId { get; set; }

		/// <summary>
		/// Gets or sets the StockId value.
		/// </summary>
		public int? StockId { get; set; }

		/// <summary>
		/// Gets or sets the CardIRef value.
		/// </summary>
		public int? CardIRef { get; set; }

		/// <summary>
		/// Gets or sets the QtyIssue value.
		/// </summary>
		public decimal? QtyIssue { get; set; }

		/// <summary>
		/// Gets or sets the Remarks value.
		/// </summary>
		[StringLength(500, ErrorMessage = "*")]
		public string Remarks { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy value.
		/// </summary>
		public int? CreatedBy { get; set; }

		/// <summary>
		/// Gets or sets the CreatedBy Name value.
		/// </summary>
		[NotMapped]
		public string CreatedByName { get; set; }

		/// <summary>
		/// Gets or sets the CreatedDate value.
		/// </summary>
		public DateTime? CreatedDate { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy value.
		/// </summary>
		public int? ModifiedBy { get; set; }

		/// <summary>
		/// Gets or sets the ModifiedBy Name value.
		/// </summary>
		[NotMapped]
		public string ModifiedByName { get; set; }

		/// <summary>
		/// Gets or sets the ModfiedDate value.
		/// </summary>
		public DateTime? ModfiedDate { get; set; }

        /// <summary>
        /// Gets or sets the stock no.
        /// </summary>
        /// <value>
        /// The stock no.
        /// </value>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the stock description.
        /// </summary>
        /// <value>
        /// The stock description.
        /// </value>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the alt stock description.
        /// </summary>
        /// <value>
        /// The alt stock description.
        /// </value>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the uom.
        /// </summary>
        /// <value>
        /// The uom.
        /// </value>
        [NotMapped]
        public string UOM { get; set; }

        /// <summary>
        /// Gets or sets the card identifier.
        /// </summary>
        /// <value>
        /// The card identifier.
        /// </value>
        [NotMapped]
        public int CardId { get; set; }

        /// <summary>
        /// Gets or sets the sub store identifier reference.
        /// </summary>
        /// <value>
        /// The sub store identifier reference.
        /// </value>
        [NotMapped]
        public int SubStoreIdRef { get; set; }

        /// <summary>
        /// Gets or sets the date value.
        /// </summary>
        /// <value>
        /// The date value.
        /// </value>
        [NotMapped]
        public DateTime DateValue { get; set; }

        /// <summary>
        /// Gets or sets the price2.
        /// </summary>
        /// <value>
        /// The price2.
        /// </value>
        [NotMapped]
        public decimal Price2 { get; set; }

        /// <summary>
        /// Gets or sets the qty needed.
        /// </summary>
        /// <value>
        /// The qty needed.
        /// </value>
        [NotMapped]
        public decimal QtyNeeded { get; set; }

        /// <summary>
        /// Gets or sets the LST sub store card form detail.
        /// </summary>
        /// <value>
        /// The LST sub store card form detail.
        /// </value>
        [NotMapped]
        public List<SubStoreCardFormDetail> lstSubStoreCardFormDetail { get; set; }

        /// <summary>
        /// Gets or sets the type of the is.
        /// </summary>
        /// <value>
        /// The type of the is.
        /// </value>
        [NotMapped]
        public int isType { get; set; }


		#endregion
	}

    public class SubStoreCardTransferDetail
    {
        public int FromEmployeeID { get; set; }

        public int FromCardID { get; set; }

        public int ToEmployeeID { get; set; }

        public int ToCardID { get; set; }

        [NotMapped]
        public List<SubStoreCardFormDetail> lstSubStoreCardFormDetailForTransfer { get; set; }
    }
}
