﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - cmprojectrequest
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>22-Aug-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("cmprojectrequest")]
    public sealed class Cmprojectrequest : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the ProjectReqNo value.
        /// </summary>
        [Required(ErrorMessage = "*")]
        [StringLength(50, ErrorMessage = "*")]
        public string ProjectReqNo { get; set; }

        /// <summary>
        /// Gets or sets the ProjectName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ProjectName { get; set; }

        /// <summary>
        /// Gets or sets the AltProjectName value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string AltProjectName { get; set; }

        /// <summary>
        /// Gets or sets the ProjectNo value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string ProjectNo { get; set; }

        /// <summary>
        /// Gets or sets the ReqDate value.
        /// </summary>
        public DateTime? ReqDate { get; set; }

        /// <summary>
        /// Gets or sets the RequestedAmount value.
        /// </summary>
        public decimal? RequestedAmount { get; set; }

        /// <summary>
        /// Gets or sets the TechnicalReport value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string TechnicalReport { get; set; }

        /// <summary>
        /// Gets or sets the Remarks value.
        /// </summary>
        [StringLength(-1, ErrorMessage = "*")]
        public string Remarks { get; set; }

        /// <summary>
        /// Gets or sets the ProjectTypeID value.
        /// </summary>
        public int? ProjectTypeID { get; set; }

        /// <summary>
        /// Gets or sets the DurationInMonth value.
        /// </summary>
        public int? DurationInMonth { get; set; }

        /// <summary>
        /// Gets or sets the ProjectStaus value.
        /// </summary>
        public int? ProjectStaus { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        public int? CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        public int? ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        #endregion
    }

}
