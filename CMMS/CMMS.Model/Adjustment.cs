﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CMMS.Model
{
    /// <summary>
    /// This class is used to Define Model for Table - adjustment
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>24-Nov-2016</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    [Table("adjustment")]
    public sealed class Adjustment : BaseModel
    {
        #region Properties

        /// <summary>
        /// Gets or sets the AdjustmentID value.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AdjustmentID { get; set; }

        /// <summary>
        /// Gets or sets the StockID value.
        /// </summary>
        public int? StockID { get; set; }

        /// <summary>
        /// Gets or sets the GUID value.
        /// </summary>
        [NotMapped]
        public string GUID { get; set; }

        /// <summary>
        /// Gets or sets the StockNo value.
        /// </summary>
        [NotMapped]
        public string StockNo { get; set; }

        /// <summary>
        /// Gets or sets the StockDescription value.
        /// </summary>
        [NotMapped]
        public string StockDescription { get; set; }

        /// <summary>
        /// Gets or sets the AltStockDescription value.
        /// </summary>
        [NotMapped]
        public string AltStockDescription { get; set; }

        /// <summary>
        /// Gets or sets the L2ID value.
        /// </summary>
        public int? L2ID { get; set; }

        /// <summary>
        /// Gets or sets the SectorName value.
        /// </summary>
        [NotMapped]
        public string SectorName { get; set; }

        /// <summary>
        /// Gets or sets the L2name value.
        /// </summary>
        [NotMapped]
        public string L2name { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreDesc value.
        /// </summary>
        [NotMapped]
        public string SubStoreDesc { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreID value.
        /// </summary>
        public int? SubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the Date value.
        /// </summary>
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or sets the Person value.
        /// </summary>
        [StringLength(50, ErrorMessage = "*")]
        public string Person { get; set; }

        /// <summary>
        /// Gets or sets the Person value.
        /// </summary>
        [NotMapped]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Person value.
        /// </summary>
        [NotMapped]
        public string AltName { get; set; }

        /// <summary>
        /// Gets or sets the QtyAdj value.
        /// </summary>
        public decimal? QtyAdj { get; set; }

        /// <summary>
        /// Gets or sets the Comment value.
        /// </summary>
        [StringLength(4000, ErrorMessage = "*")]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the Price4 value.
        /// </summary>
        public decimal? Price4 { get; set; }

        /// <summary>
        /// Gets or sets the SubStoreCode value.
        /// </summary>
        [NotMapped]
        public string SubStoreCode { get; set; }

        /// <summary>
        /// Gets or sets the TotalPrice value.
        /// </summary>
        [NotMapped]
        public decimal? TotalPrice { get; set; }

        /// <summary>
        /// Gets or sets the HdnL2ID value.
        /// </summary>
        [NotMapped]
        public int? HdnL2ID { get; set; }

        /// <summary>
        /// Gets or sets the HdnSubStoreID value.
        /// </summary>
        [NotMapped]
        public int? HdnSubStoreID { get; set; }

        /// <summary>
        /// Gets or sets the BatchNo value.
        /// </summary>
        [StringLength(30, ErrorMessage = "*")]
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the CreatedBy Name value.
        /// </summary>
        [NotMapped]
        public string CreatedByName { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate value.
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy value.
        /// </summary>
        [StringLength(100, ErrorMessage = "*")]
        public string ModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedBy Name value.
        /// </summary>
        [NotMapped]
        public string ModifiedByName { get; set; }

        /// <summary>
        /// Gets or sets the ModifiedDate value.
        /// </summary>
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the List of Issue value.
        /// </summary>
        [NotMapped]
        public string lstAdj { get; set; }

        /// <summary>
        /// Gets or sets the TransactionID value.
        /// </summary>
        public long? TransactionID { get; set; }

        #endregion
    }
}
