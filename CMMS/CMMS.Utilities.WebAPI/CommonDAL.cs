﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Model;
using CMMS.Service;
using CMMS.Infrastructure;
using CMMS.DAL;


namespace CMMS.Utilities.WebAPI
{
    public static class CommonDAL
    {
        public static Employee GetEmployeeByUserIdPassword(string clientCode, string UserID, string Password, string IPAddress, double? Latitude, double? Longitude)
        {
            try
            {
                Employee model = EmployeeDetailService.GetEmployeeByUserIdPassword(clientCode, UserID, Password).FirstOrDefault();
                if (model.EmployeeID > 0)
                {
                    Account objAC = EmployeeDetailService.GetAccountType(clientCode);
                    int strHeartbeatValInSec = string.IsNullOrEmpty(Convert.ToString(objAC.SessionTimeOutMobApp)) ? 300 : (Convert.ToInt32(Convert.ToString(objAC.SessionTimeOutMobApp)) * 60);

                    model.ClientCode = clientCode;
                    string queryEmpStatusCheck = EmployeeDetailService.GetEmployeeStatus(clientCode, UserID, Password);
                    if (!string.IsNullOrWhiteSpace(queryEmpStatusCheck))
                    {
                        if ((queryEmpStatusCheck.ToLower() == "terminated") || (queryEmpStatusCheck.ToLower() == "in-active"))
                        {
                            model.IPAddress = IPAddress;
                            model.Latitude = Latitude;
                            model.Longitude = Longitude;
                            //model.HeartbeatValInSec = DBExecute.GetHeratBeatValue();

                            model.HeartbeatValInSec = strHeartbeatValInSec;
                            model.UTCDateTime = DateTime.UtcNow;
                            model.InActiveUserMessage = ConstatntMessages.InActiveUserMessage;
                            return model;
                        }
                    }

                    string strTime = DateTime.Now.AddSeconds(-(strHeartbeatValInSec)).ToString("yyyy/MM/dd hh:mm:ss tt", new System.Globalization.CultureInfo("en-US").DateTimeFormat);

                    EmployeeDetailService.DeleteMobileSession(strTime, clientCode);

                    //Check if Same User already logged in or not

                    List<mobilesession> lst = EmployeeDetailService.GetSessionsByEmployeeId(model.EmployeeID, clientCode);

                    if (lst.Any())
                    {
                        if (lst[0].Lastheartbeat.AddSeconds((strHeartbeatValInSec)) < System.DateTime.Now)
                        {
                            model.IsLogIn = false;
                        }
                        else
                        {
                            model.IsLogIn = true;
                        }
                    }
                    else
                    {
                        model.IsLogIn = false;
                    }

                    //Proceed further if logged in User do not exist in MobileSessions table
                    if (model.IsLogIn == false)
                    {
                        //Check for Connection limit
                        model.ExceedConnectionLimitMessage = EmployeeDetailService.CheckConlimits(clientCode, lst);

                        //If Connection limit matches its value then proceed further
                        if (string.IsNullOrEmpty(model.ExceedConnectionLimitMessage))
                        {

                            //Insert record into MobileSessions table
                            int SavedInMobileSession = EmployeeDetailService.InsertMobileSession(model.EmployeeID, clientCode);
                            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                            //{
                            //Insert record into LoginLogs table
                            //if (SavedInMobileSession == 1)
                            //{
                            LoginLog newLogin = new LoginLog();
                            newLogin.EmployeeId = model.EmployeeID;
                            newLogin.LoginDateTime = System.DateTime.Now;
                            newLogin.IPAddress = IPAddress;
                            if (Latitude.HasValue)
                                newLogin.Latitude = (float)Latitude;
                            if (Longitude.HasValue)
                                newLogin.Longitude = (float)Longitude;
                            int result = EmployeeDetailService.InsertLoginLog(newLogin, clientCode);
                            //}
                            //  scope.Complete();
                            //}
                        }
                    }
                    else
                    {
                        model.SameUserAlreadyLoginMessage = ConstatntMessages.SameUserAlreadyLoginMessage;
                    }
                    model.IPAddress = IPAddress;
                    model.Latitude = Latitude;
                    model.Longitude = Longitude;
                    model.HeartbeatValInSec = strHeartbeatValInSec;
                    model.UTCDateTime = DateTime.UtcNow;
                    return model;
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
