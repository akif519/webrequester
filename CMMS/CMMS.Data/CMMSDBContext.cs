﻿using CMMS.Model;
using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace CMMS.Data
{

    public partial class CMMSDBContext : DbContext
    {
        public CMMSDBContext()
            : base("name=CMMSDBContext")
        {
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public virtual DbSet<cmBank> cmBank { get; set; }

       
    }

}