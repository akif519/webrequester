﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for AssetList
/// </summary>
public class PurchaseRequestPrint : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand pageHeaderBand1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell6;
    private XRControlStyle Title;
    private XRControlStyle FieldCaption;
    private XRControlStyle PageInfo;
    private XRControlStyle DataField;
    private XRTable xrTable1;
    private XRTableRow xrTableRow3;
    private XRTableCell lblAssetDescription;
    private XRTableCell lblAssetNo;
    private XRTableCell lblL2Code;
    private XRTableCell lbll5;
    private XRTableCell lblSrNo;
    private XRControlStyle EvenStyle;
    private XRControlStyle OddStyle;
    private XRControlStyle GridHeader;
    private DevExpress.XtraReports.Parameters.Parameter l2Id;
    private DevExpress.XtraReports.Parameters.Parameter lang;
    private ReportHeaderBand reportHeaderBand1;
    private XRSubreport xrSubreport1;
    private DevExpress.XtraReports.Parameters.Parameter accountId;
    private XRTableCell lblL4no;
    private XRTable xrTable2;
    private XRTableRow xrTableRow4;
    private XRTableCell AssetNoValue;
    private XRTableCell assetDescValue;
    private XRTableCell L3Value;
    private XRTableCell xrTableCell7;
    private XRTableCell location;
    private DevExpress.XtraReports.Parameters.Parameter isCentral;
    private DevExpress.XtraReports.Parameters.Parameter employeeId;
    private DevExpress.XtraReports.Parameters.Parameter poid;
    private XRTableCell xrTableCell14;
    private XRTableCell xrTableCell18;
    private XRTableCell xrTableCell19;
    private DevExpress.XtraReports.Parameters.Parameter header;
    private DevExpress.XtraReports.Parameters.Parameter l1Id;
    private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    private XRLabel lblReportHeader;
    private XRSubreport xrSubreport3;
    private XRPageInfo xrPageInfo2;
    private XRPageInfo xrPageInfo1;
    private XRSubreport xrSubreport2;
    private PageFooterBand pageFooterBand1;
    private XRTableCell xrTableCell9;
    private XRTableCell xrTableCell8;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public PurchaseRequestPrint()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
        DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
        this.AssetNoValue = new DevExpress.XtraReports.UI.XRTableCell();
        this.assetDescValue = new DevExpress.XtraReports.UI.XRTableCell();
        this.L3Value = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
        this.location = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.pageHeaderBand1 = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.l2Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.accountId = new DevExpress.XtraReports.Parameters.Parameter();
        this.employeeId = new DevExpress.XtraReports.Parameters.Parameter();
        this.lang = new DevExpress.XtraReports.Parameters.Parameter();
        this.poid = new DevExpress.XtraReports.Parameters.Parameter();
        this.lblReportHeader = new DevExpress.XtraReports.UI.XRLabel();
        this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
        this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
        this.lblSrNo = new DevExpress.XtraReports.UI.XRTableCell();
        this.lblAssetNo = new DevExpress.XtraReports.UI.XRTableCell();
        this.lblAssetDescription = new DevExpress.XtraReports.UI.XRTableCell();
        this.lblL2Code = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
        this.lblL4no = new DevExpress.XtraReports.UI.XRTableCell();
        this.lbll5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
        this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
        this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
        this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
        this.FieldCaption = new DevExpress.XtraReports.UI.XRControlStyle();
        this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
        this.DataField = new DevExpress.XtraReports.UI.XRControlStyle();
        this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
        this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
        this.GridHeader = new DevExpress.XtraReports.UI.XRControlStyle();
        this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.header = new DevExpress.XtraReports.Parameters.Parameter();
        this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.isCentral = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.pageFooterBand1 = new DevExpress.XtraReports.UI.PageFooterBand();
        this.xrSubreport3 = new DevExpress.XtraReports.UI.XRSubreport();
        this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
        this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
        this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
        this.Detail.Dpi = 100F;
        this.Detail.HeightF = 17.74998F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrTable2
        // 
        this.xrTable2.BorderColor = System.Drawing.Color.LightGray;
        this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable2.Dpi = 100F;
        this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.059906E-06F, 0F);
        this.xrTable2.Name = "xrTable2";
        this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
        this.xrTable2.SizeF = new System.Drawing.SizeF(1058.921F, 17.74998F);
        this.xrTable2.StylePriority.UseBorderColor = false;
        this.xrTable2.StylePriority.UseBorders = false;
        this.xrTable2.StylePriority.UsePadding = false;
        // 
        // xrTableRow4
        // 
        this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.AssetNoValue,
            this.assetDescValue,
            this.L3Value,
            this.xrTableCell7,
            this.location,
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell9});
        this.xrTableRow4.Dpi = 100F;
        this.xrTableRow4.EvenStyleName = "EvenStyle";
        this.xrTableRow4.Name = "xrTableRow4";
        this.xrTableRow4.OddStyleName = "OddStyle";
        this.xrTableRow4.Weight = 1D;
        // 
        // AssetNoValue
        // 
        this.AssetNoValue.Dpi = 100F;
        this.AssetNoValue.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.AssetNoValue.Multiline = true;
        this.AssetNoValue.Name = "AssetNoValue";
        this.AssetNoValue.StylePriority.UseFont = false;
        this.AssetNoValue.StylePriority.UseTextAlignment = false;
        xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
        xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
        this.AssetNoValue.Summary = xrSummary1;
        this.AssetNoValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
        this.AssetNoValue.Weight = 21.783233290225432D;
        // 
        // assetDescValue
        // 
        this.assetDescValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "StockNo")});
        this.assetDescValue.Dpi = 100F;
        this.assetDescValue.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.assetDescValue.Multiline = true;
        this.assetDescValue.Name = "assetDescValue";
        this.assetDescValue.StylePriority.UseFont = false;
        this.assetDescValue.Weight = 80.953707766273652D;
        // 
        // L3Value
        // 
        this.L3Value.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PartDescription")});
        this.L3Value.Dpi = 100F;
        this.L3Value.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.L3Value.Multiline = true;
        this.L3Value.Name = "L3Value";
        this.L3Value.StylePriority.UseFont = false;
        this.L3Value.Weight = 139.10853517077473D;
        // 
        // xrTableCell7
        // 
        this.xrTableCell7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "UoM")});
        this.xrTableCell7.Dpi = 100F;
        this.xrTableCell7.EvenStyleName = "EvenStyle";
        this.xrTableCell7.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell7.Multiline = true;
        this.xrTableCell7.Name = "xrTableCell7";
        this.xrTableCell7.OddStyleName = "OddStyle";
        this.xrTableCell7.StylePriority.UseFont = false;
        this.xrTableCell7.StylePriority.UseTextAlignment = false;
        this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrTableCell7.Weight = 58.298435056971044D;
        // 
        // location
        // 
        this.location.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "JobOrderID")});
        this.location.Dpi = 100F;
        this.location.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.location.Multiline = true;
        this.location.Name = "location";
        this.location.StylePriority.UseFont = false;
        this.location.Weight = 60.135083813667485D;
        // 
        // xrTableCell18
        // 
        this.xrTableCell18.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Quantity")});
        this.xrTableCell18.Dpi = 100F;
        this.xrTableCell18.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell18.Multiline = true;
        this.xrTableCell18.Name = "xrTableCell18";
        this.xrTableCell18.StylePriority.UseFont = false;
        this.xrTableCell18.Weight = 53.386207868880234D;
        // 
        // xrTableCell19
        // 
        this.xrTableCell19.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "RefPrice", "{0:#.00}")});
        this.xrTableCell19.Dpi = 100F;
        this.xrTableCell19.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell19.Multiline = true;
        this.xrTableCell19.Name = "xrTableCell19";
        this.xrTableCell19.StylePriority.UseFont = false;
        this.xrTableCell19.Text = "W";
        this.xrTableCell19.Weight = 55.860764929022828D;
        // 
        // xrTableCell9
        // 
        this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Source")});
        this.xrTableCell9.Dpi = 100F;
        this.xrTableCell9.Font = new System.Drawing.Font("Calibri", 8.25F);
        this.xrTableCell9.Name = "xrTableCell9";
        this.xrTableCell9.StylePriority.UseFont = false;
        this.xrTableCell9.Weight = 55.860764929022828D;
        // 
        // TopMargin
        // 
        this.TopMargin.Dpi = 100F;
        this.TopMargin.HeightF = 0.2083302F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Dpi = 100F;
        this.BottomMargin.HeightF = 20F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // pageHeaderBand1
        // 
        this.pageHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblReportHeader,
            this.xrTable1});
        this.pageHeaderBand1.Dpi = 100F;
        this.pageHeaderBand1.HeightF = 56.25006F;
        this.pageHeaderBand1.Name = "pageHeaderBand1";
        // 
        // l2Id
        // 
        this.l2Id.Description = "l2Id";
        this.l2Id.Name = "l2Id";
        // 
        // accountId
        // 
        this.accountId.Description = "accountId";
        this.accountId.Name = "accountId";
        this.accountId.Type = typeof(int);
        this.accountId.ValueInfo = "0";
        // 
        // employeeId
        // 
        this.employeeId.Description = "employeeId";
        this.employeeId.Name = "employeeId";
        this.employeeId.Type = typeof(int);
        this.employeeId.ValueInfo = "0";
        // 
        // lang
        // 
        this.lang.Description = "lang";
        this.lang.Name = "lang";
        this.lang.Type = typeof(bool);
        this.lang.ValueInfo = "True";
        // 
        // poid
        // 
        this.poid.Description = "poid";
        this.poid.Name = "poid";
        this.poid.Type = typeof(int);
        this.poid.ValueInfo = "0";
        // 
        // lblReportHeader
        // 
        this.lblReportHeader.Dpi = 100F;
        this.lblReportHeader.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
        this.lblReportHeader.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.lblReportHeader.Multiline = true;
        this.lblReportHeader.Name = "lblReportHeader";
        this.lblReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblReportHeader.SizeF = new System.Drawing.SizeF(1058.921F, 25.70833F);
        this.lblReportHeader.StyleName = "Title";
        this.lblReportHeader.StylePriority.UseFont = false;
        this.lblReportHeader.Text = "PR Item List";
        // 
        // xrTable1
        // 
        this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
        this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
        this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTable1.Dpi = 100F;
        this.xrTable1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.08335F);
        this.xrTable1.Name = "xrTable1";
        this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
        this.xrTable1.SizeF = new System.Drawing.SizeF(1058.921F, 29.16671F);
        this.xrTable1.StylePriority.UseBorderColor = false;
        this.xrTable1.StylePriority.UseBorders = false;
        this.xrTable1.StylePriority.UseFont = false;
        this.xrTable1.StylePriority.UsePadding = false;
        this.xrTable1.StylePriority.UseTextAlignment = false;
        // 
        // xrTableRow3
        // 
        this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSrNo,
            this.lblAssetNo,
            this.lblAssetDescription,
            this.lblL2Code,
            this.xrTableCell14,
            this.lblL4no,
            this.lbll5,
            this.xrTableCell8});
        this.xrTableRow3.Dpi = 100F;
        this.xrTableRow3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrTableRow3.Name = "xrTableRow3";
        this.xrTableRow3.StylePriority.UseBorders = false;
        this.xrTableRow3.StylePriority.UseFont = false;
        this.xrTableRow3.Weight = 1D;
        // 
        // lblSrNo
        // 
        this.lblSrNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lblSrNo.CanGrow = false;
        this.lblSrNo.Dpi = 100F;
        this.lblSrNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lblSrNo.Name = "lblSrNo";
        this.lblSrNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblSrNo.StyleName = "GridHeader";
        this.lblSrNo.StylePriority.UseBorders = false;
        this.lblSrNo.StylePriority.UseFont = false;
        this.lblSrNo.StylePriority.UsePadding = false;
        this.lblSrNo.StylePriority.UseTextAlignment = false;
        this.lblSrNo.Text = "#";
        this.lblSrNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        this.lblSrNo.Weight = 19.649299467386836D;
        // 
        // lblAssetNo
        // 
        this.lblAssetNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lblAssetNo.CanGrow = false;
        this.lblAssetNo.Dpi = 100F;
        this.lblAssetNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lblAssetNo.Name = "lblAssetNo";
        this.lblAssetNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetNo.StyleName = "GridHeader";
        this.lblAssetNo.StylePriority.UseBorders = false;
        this.lblAssetNo.StylePriority.UseFont = false;
        this.lblAssetNo.StylePriority.UsePadding = false;
        this.lblAssetNo.StylePriority.UseTextAlignment = false;
        this.lblAssetNo.Text = "Item No";
        this.lblAssetNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lblAssetNo.Weight = 73.023292628299387D;
        // 
        // lblAssetDescription
        // 
        this.lblAssetDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lblAssetDescription.CanGrow = false;
        this.lblAssetDescription.Dpi = 100F;
        this.lblAssetDescription.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lblAssetDescription.Multiline = true;
        this.lblAssetDescription.Name = "lblAssetDescription";
        this.lblAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetDescription.StyleName = "GridHeader";
        this.lblAssetDescription.StylePriority.UseBorders = false;
        this.lblAssetDescription.StylePriority.UseFont = false;
        this.lblAssetDescription.StylePriority.UsePadding = false;
        this.lblAssetDescription.StylePriority.UseTextAlignment = false;
        this.lblAssetDescription.Text = "Item Desc";
        this.lblAssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lblAssetDescription.Weight = 125.48114055456574D;
        // 
        // lblL2Code
        // 
        this.lblL2Code.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lblL2Code.CanGrow = false;
        this.lblL2Code.Dpi = 100F;
        this.lblL2Code.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lblL2Code.Multiline = true;
        this.lblL2Code.Name = "lblL2Code";
        this.lblL2Code.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblL2Code.StyleName = "GridHeader";
        this.lblL2Code.StylePriority.UseBorders = false;
        this.lblL2Code.StylePriority.UseFont = false;
        this.lblL2Code.StylePriority.UsePadding = false;
        this.lblL2Code.StylePriority.UseTextAlignment = false;
        this.lblL2Code.Text = "UOM";
        this.lblL2Code.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lblL2Code.Weight = 52.58738015752693D;
        // 
        // xrTableCell14
        // 
        this.xrTableCell14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
        this.xrTableCell14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
        this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell14.CanGrow = false;
        this.xrTableCell14.Dpi = 100F;
        this.xrTableCell14.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableCell14.Name = "xrTableCell14";
        this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrTableCell14.StylePriority.UseBackColor = false;
        this.xrTableCell14.StylePriority.UseBorderColor = false;
        this.xrTableCell14.StylePriority.UseBorders = false;
        this.xrTableCell14.StylePriority.UseFont = false;
        this.xrTableCell14.StylePriority.UsePadding = false;
        this.xrTableCell14.StylePriority.UseTextAlignment = false;
        this.xrTableCell14.Text = "JO No";
        this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell14.Weight = 54.244083991059895D;
        // 
        // lblL4no
        // 
        this.lblL4no.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lblL4no.CanGrow = false;
        this.lblL4no.Dpi = 100F;
        this.lblL4no.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lblL4no.Multiline = true;
        this.lblL4no.Name = "lblL4no";
        this.lblL4no.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblL4no.StyleName = "GridHeader";
        this.lblL4no.StylePriority.UseBorders = false;
        this.lblL4no.StylePriority.UseFont = false;
        this.lblL4no.StylePriority.UsePadding = false;
        this.lblL4no.StylePriority.UseTextAlignment = false;
        this.lblL4no.Text = "Qty";
        this.lblL4no.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lblL4no.Weight = 48.156367312330069D;
        // 
        // lbll5
        // 
        this.lbll5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.lbll5.CanGrow = false;
        this.lbll5.Dpi = 100F;
        this.lbll5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.lbll5.Multiline = true;
        this.lbll5.Name = "lbll5";
        this.lbll5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lbll5.StyleName = "GridHeader";
        this.lbll5.StylePriority.UseBorders = false;
        this.lbll5.StylePriority.UseFont = false;
        this.lbll5.StylePriority.UsePadding = false;
        this.lbll5.StylePriority.UseTextAlignment = false;
        this.lbll5.Text = "Ref Price";
        this.lbll5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.lbll5.Weight = 50.388516239008858D;
        // 
        // xrTableCell8
        // 
        this.xrTableCell8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
        this.xrTableCell8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
        this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.xrTableCell8.CanGrow = false;
        this.xrTableCell8.Dpi = 100F;
        this.xrTableCell8.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
        this.xrTableCell8.Name = "xrTableCell8";
        this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrTableCell8.StylePriority.UseBackColor = false;
        this.xrTableCell8.StylePriority.UseBorderColor = false;
        this.xrTableCell8.StylePriority.UseBorders = false;
        this.xrTableCell8.StylePriority.UseFont = false;
        this.xrTableCell8.StylePriority.UsePadding = false;
        this.xrTableCell8.StylePriority.UseTextAlignment = false;
        this.xrTableCell8.Text = "Source";
        this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrTableCell8.Weight = 50.388516239008858D;
        // 
        // xrTableRow1
        // 
        this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
        this.xrTableRow1.Dpi = 100F;
        this.xrTableRow1.Name = "xrTableRow1";
        this.xrTableRow1.Weight = 1D;
        // 
        // xrTableCell1
        // 
        this.xrTableCell1.Dpi = 100F;
        this.xrTableCell1.Name = "xrTableCell1";
        this.xrTableCell1.Text = "xrTableCell1";
        this.xrTableCell1.Weight = 1D;
        // 
        // xrTableCell2
        // 
        this.xrTableCell2.Dpi = 100F;
        this.xrTableCell2.Name = "xrTableCell2";
        this.xrTableCell2.Text = "xrTableCell2";
        this.xrTableCell2.Weight = 1D;
        // 
        // xrTableCell3
        // 
        this.xrTableCell3.Dpi = 100F;
        this.xrTableCell3.Name = "xrTableCell3";
        this.xrTableCell3.Text = "xrTableCell3";
        this.xrTableCell3.Weight = 1D;
        // 
        // xrTableRow2
        // 
        this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
        this.xrTableRow2.Dpi = 100F;
        this.xrTableRow2.Name = "xrTableRow2";
        this.xrTableRow2.Weight = 1D;
        // 
        // xrTableCell4
        // 
        this.xrTableCell4.Dpi = 100F;
        this.xrTableCell4.Name = "xrTableCell4";
        this.xrTableCell4.Text = "xrTableCell4";
        this.xrTableCell4.Weight = 1D;
        // 
        // xrTableCell5
        // 
        this.xrTableCell5.Dpi = 100F;
        this.xrTableCell5.Name = "xrTableCell5";
        this.xrTableCell5.Text = "xrTableCell5";
        this.xrTableCell5.Weight = 1D;
        // 
        // xrTableCell6
        // 
        this.xrTableCell6.Dpi = 100F;
        this.xrTableCell6.Name = "xrTableCell6";
        this.xrTableCell6.Text = "xrTableCell6";
        this.xrTableCell6.Weight = 1D;
        // 
        // Title
        // 
        this.Title.BackColor = System.Drawing.Color.Transparent;
        this.Title.BorderColor = System.Drawing.Color.Black;
        this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.Title.BorderWidth = 1F;
        this.Title.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold);
        this.Title.ForeColor = System.Drawing.Color.Maroon;
        this.Title.Name = "Title";
        // 
        // FieldCaption
        // 
        this.FieldCaption.BackColor = System.Drawing.Color.Transparent;
        this.FieldCaption.BorderColor = System.Drawing.Color.Black;
        this.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.FieldCaption.BorderWidth = 1F;
        this.FieldCaption.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
        this.FieldCaption.ForeColor = System.Drawing.Color.Maroon;
        this.FieldCaption.Name = "FieldCaption";
        // 
        // PageInfo
        // 
        this.PageInfo.BackColor = System.Drawing.Color.Transparent;
        this.PageInfo.BorderColor = System.Drawing.Color.Black;
        this.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.PageInfo.BorderWidth = 1F;
        this.PageInfo.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
        this.PageInfo.ForeColor = System.Drawing.Color.Black;
        this.PageInfo.Name = "PageInfo";
        // 
        // DataField
        // 
        this.DataField.BackColor = System.Drawing.Color.Transparent;
        this.DataField.BorderColor = System.Drawing.Color.Black;
        this.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.DataField.BorderWidth = 1F;
        this.DataField.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.DataField.ForeColor = System.Drawing.Color.Black;
        this.DataField.Name = "DataField";
        this.DataField.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        // 
        // EvenStyle
        // 
        this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
        this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
        this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.EvenStyle.Name = "EvenStyle";
        // 
        // OddStyle
        // 
        this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
        this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.OddStyle.Name = "OddStyle";
        // 
        // GridHeader
        // 
        this.GridHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
        this.GridHeader.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
        this.GridHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
        | DevExpress.XtraPrinting.BorderSide.Right)
        | DevExpress.XtraPrinting.BorderSide.Bottom)));
        this.GridHeader.BorderWidth = 1F;
        this.GridHeader.Name = "GridHeader";
        // 
        // reportHeaderBand1
        // 
        this.reportHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1,
            this.xrSubreport3});
        this.reportHeaderBand1.Dpi = 100F;
        this.reportHeaderBand1.HeightF = 210.4167F;
        this.reportHeaderBand1.Name = "reportHeaderBand1";
        // 
        // header
        // 
        this.header.Name = "header";
        // 
        // l1Id
        // 
        this.l1Id.Name = "l1Id";
        this.l1Id.Type = typeof(int);
        this.l1Id.ValueInfo = "0";
        // 
        // isCentral
        // 
        this.isCentral.Description = "isCentral";
        this.isCentral.Name = "isCentral";
        this.isCentral.Type = typeof(bool);
        this.isCentral.ValueInfo = "False";
        // 
        // xrPageInfo2
        // 
        this.xrPageInfo2.Dpi = 100F;
        this.xrPageInfo2.Format = "Page {0} of {1}";
        this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(741F, 80.37497F);
        this.xrPageInfo2.Name = "xrPageInfo2";
        this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo2.SizeF = new System.Drawing.SizeF(313F, 23F);
        this.xrPageInfo2.StyleName = "PageInfo";
        this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        // 
        // xrPageInfo1
        // 
        this.xrPageInfo1.Dpi = 100F;
        this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 80.37497F);
        this.xrPageInfo1.Name = "xrPageInfo1";
        this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo1.SizeF = new System.Drawing.SizeF(313F, 23F);
        this.xrPageInfo1.StyleName = "PageInfo";
        // 
        // pageFooterBand1
        // 
        this.pageFooterBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPageInfo2});
        this.pageFooterBand1.Dpi = 100F;
        this.pageFooterBand1.HeightF = 106.0833F;
        this.pageFooterBand1.Name = "pageFooterBand1";
        // 
        // xrSubreport3
        // 
        this.xrSubreport3.Dpi = 100F;
        this.xrSubreport3.LocationFloat = new DevExpress.Utils.PointFloat(9.059906E-06F, 98.62496F);
        this.xrSubreport3.Name = "xrSubreport3";
        this.xrSubreport3.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l2Id", this.l2Id));
        this.xrSubreport3.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("accountId", this.accountId));
        this.xrSubreport3.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("employeeId", this.employeeId));
        this.xrSubreport3.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("lang", this.lang));
        this.xrSubreport3.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("poid", this.poid));
        this.xrSubreport3.ReportSource = new PurchaseRequestPrintHeader();
        this.xrSubreport3.SizeF = new System.Drawing.SizeF(1058.921F, 111.7917F);
        // 
        // xrSubreport2
        // 
        this.xrSubreport2.Dpi = 100F;
        this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
        this.xrSubreport2.Name = "xrSubreport2";
        this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
        this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
        this.xrSubreport2.ReportSource = new FooterSubReport();
        this.xrSubreport2.SizeF = new System.Drawing.SizeF(1058.921F, 80.37497F);
        // 
        // xrSubreport1
        // 
        this.xrSubreport1.Dpi = 100F;
        this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(5.999986F, 0F);
        this.xrSubreport1.Name = "xrSubreport1";
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("header", this.header));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l1Id", this.l1Id));
        this.xrSubreport1.ReportSource = new HeaderSubReport();
        this.xrSubreport1.SizeF = new System.Drawing.SizeF(1052.921F, 90.625F);
        // 
        // objectDataSource1
        // 
        this.objectDataSource1.Constructor = objectConstructorInfo1;
        this.objectDataSource1.DataMember = "GetPRItems";
        this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.PurchasingReportService);
        this.objectDataSource1.Name = "objectDataSource1";
        parameter1.Name = "accountId";
        parameter1.Type = typeof(DevExpress.DataAccess.Expression);
        parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.accountId]", typeof(int));
        parameter2.Name = "isCentral";
        parameter2.Type = typeof(DevExpress.DataAccess.Expression);
        parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.isCentral]", typeof(bool));
        parameter3.Name = "lang";
        parameter3.Type = typeof(DevExpress.DataAccess.Expression);
        parameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.lang]", typeof(bool));
        parameter4.Name = "employeeId";
        parameter4.Type = typeof(DevExpress.DataAccess.Expression);
        parameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.employeeId]", typeof(int));
        parameter5.Name = "POId";
        parameter5.Type = typeof(DevExpress.DataAccess.Expression);
        parameter5.Value = new DevExpress.DataAccess.Expression("[Parameters.poid]", typeof(int));
        this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2,
            parameter3,
            parameter4,
            parameter5});
        // 
        // PurchaseRequestPrint
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.pageHeaderBand1,
            this.pageFooterBand1,
            this.reportHeaderBand1});
        this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
        this.DataSource = this.objectDataSource1;
        this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(10, 26, 0, 20);
        this.PageHeight = 850;
        this.PageWidth = 1100;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.l2Id,
            this.lang,
            this.accountId,
            this.isCentral,
            this.employeeId,
            this.poid,
            this.header,
            this.l1Id});
        this.RequestParameters = false;
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.FieldCaption,
            this.PageInfo,
            this.DataField,
            this.EvenStyle,
            this.OddStyle,
            this.GridHeader});
        this.Version = "16.2";
        ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion
}
