﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for AssetDetails
/// </summary>
public class AssetDetails : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageFooterBand pageFooterBand1;
    private XRPageInfo xrPageInfo1;
    private XRPageInfo xrPageInfo2;
    private ReportHeaderBand reportHeaderBand1;
    private XRControlStyle Title;
    private XRControlStyle FieldCaption;
    private XRControlStyle PageInfo;
    private XRControlStyle DataField;
    private XRSubreport xrSubreport1;
    private DevExpress.XtraReports.Parameters.Parameter accountId;
    private DevExpress.XtraReports.Parameters.Parameter lang;
    private XRSubreport xrSubreport2;
    private XRLabel xrLabel5;
    private XRLabel lblArAssetDescription;
    private XRLabel xrLabel2;
    private XRLabel lblCityCode;
    private XRLabel xrLabel4;
    private XRLabel lblAssetNo;
    private XRLabel lblAssetDescription;
    private XRLabel xrLabel45;
    private XRLabel xrLabel46;
    private XRLabel xrLabel6;
    private XRLabel xrLabel7;
    private XRLabel lblZoneCode;
    private XRLabel lblLocationNo;
    private XRLabel lblAssetCategory;
    private XRLabel xrLabel12;
    private XRLabel lblBuildingCode;
    private XRLabel xrLabel16;
    private XRLabel xrLabel15;
    private XRLabel xrLabel14;
    private XRLabel xrLabel10;
    private XRLabel lblManufactuer;
    private XRLabel xrLabel31;
    private XRLabel xrLabel28;
    private XRLabel lblSerialNo;
    private XRLabel lblAuthEmp;
    private XRLabel xrLabel21;
    private XRLabel lblModelNo;
    private XRLabel lblAssetStatus;
    private XRLabel lblTrade;
    private XRLabel xrLabel25;
    private XRLabel xrLabel26;
    private XRLabel xrLabel27;
    private XRLabel xrLabel58;
    private XRLabel lblAssetCritcality;
    private XRLabel xrLabel32;
    private XRLabel xrLabel33;
    private XRLabel lblWarranty;
    private XRLabel lblContractor;
    private XRLabel xrLabel36;
    private XRLabel lblWarrantyStatus;
    private XRLabel xrLabel38;
    private XRLabel lblWarrantyExpiryDate;
    private XRLabel lblSupplier;
    private XRLabel xrlblAssetNotes;
    private XRLabel lblPurchasePrice;
    private XRLabel lblContractNotes;
    private XRLabel lblAssetNotes;
    private XRLabel xrLabel47;
    private XRLabel xrlblManufacturer;
    private XRLabel xrLabel49;
    private XRLabel xrLabel50;
    private XRLabel xrLabel51;
    private XRLabel lblDateDisposed;
    private XRLabel lblDateCommissioned;
    private XRLabel xrLabel54;
    private XRLabel lblEstimatedLife;
    private XRLabel lblNotesToTechnician;
    private XRLabel xrLabel62;
    private XRLabel lblCurrentValue;
    private XRLabel xrLabel60;
    private PageHeaderBand PageHeader;
    private ReportFooterBand ReportFooter;
    private DevExpress.XtraReports.Parameters.Parameter employeeId;
    private DevExpress.XtraReports.Parameters.Parameter isCentral;
    private DevExpress.XtraReports.Parameters.Parameter l2Id;
    private DevExpress.XtraReports.Parameters.Parameter l3Id;
    private DevExpress.XtraReports.Parameters.Parameter l4Id;
    private DevExpress.XtraReports.Parameters.Parameter l5Id;
    private DevExpress.XtraReports.Parameters.Parameter locationId;
    private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    private DevExpress.XtraReports.Parameters.Parameter header;
    private DevExpress.XtraReports.Parameters.Parameter l1Id;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public AssetDetails()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter6 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter7 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter8 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter9 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.lblNotesToTechnician = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblCurrentValue = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetCritcality = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblWarranty = new DevExpress.XtraReports.UI.XRLabel();
        this.lblContractor = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblWarrantyStatus = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblWarrantyExpiryDate = new DevExpress.XtraReports.UI.XRLabel();
        this.lblSupplier = new DevExpress.XtraReports.UI.XRLabel();
        this.xrlblAssetNotes = new DevExpress.XtraReports.UI.XRLabel();
        this.lblPurchasePrice = new DevExpress.XtraReports.UI.XRLabel();
        this.lblContractNotes = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetNotes = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrlblManufacturer = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblDateDisposed = new DevExpress.XtraReports.UI.XRLabel();
        this.lblDateCommissioned = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblEstimatedLife = new DevExpress.XtraReports.UI.XRLabel();
        this.lblManufactuer = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblSerialNo = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAuthEmp = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblModelNo = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetStatus = new DevExpress.XtraReports.UI.XRLabel();
        this.lblTrade = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblZoneCode = new DevExpress.XtraReports.UI.XRLabel();
        this.lblLocationNo = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetCategory = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblBuildingCode = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblArAssetDescription = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblCityCode = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetNo = new DevExpress.XtraReports.UI.XRLabel();
        this.lblAssetDescription = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.pageFooterBand1 = new DevExpress.XtraReports.UI.PageFooterBand();
        this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
        this.accountId = new DevExpress.XtraReports.Parameters.Parameter();
        this.lang = new DevExpress.XtraReports.Parameters.Parameter();
        this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
        this.FieldCaption = new DevExpress.XtraReports.UI.XRControlStyle();
        this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
        this.DataField = new DevExpress.XtraReports.UI.XRControlStyle();
        this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
        this.header = new DevExpress.XtraReports.Parameters.Parameter();
        this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
        this.employeeId = new DevExpress.XtraReports.Parameters.Parameter();
        this.isCentral = new DevExpress.XtraReports.Parameters.Parameter();
        this.l2Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.l3Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.l4Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.l5Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.locationId = new DevExpress.XtraReports.Parameters.Parameter();
        this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
        this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
        this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNotesToTechnician,
            this.xrLabel62,
            this.lblCurrentValue,
            this.xrLabel60,
            this.xrLabel58,
            this.lblAssetCritcality,
            this.xrLabel32,
            this.xrLabel33,
            this.lblWarranty,
            this.lblContractor,
            this.xrLabel36,
            this.lblWarrantyStatus,
            this.xrLabel38,
            this.lblWarrantyExpiryDate,
            this.lblSupplier,
            this.xrlblAssetNotes,
            this.lblPurchasePrice,
            this.lblContractNotes,
            this.lblAssetNotes,
            this.xrLabel47,
            this.xrlblManufacturer,
            this.xrLabel49,
            this.xrLabel50,
            this.xrLabel51,
            this.lblDateDisposed,
            this.lblDateCommissioned,
            this.xrLabel54,
            this.lblEstimatedLife,
            this.lblManufactuer,
            this.xrLabel31,
            this.xrLabel28,
            this.lblSerialNo,
            this.lblAuthEmp,
            this.xrLabel21,
            this.lblModelNo,
            this.lblAssetStatus,
            this.lblTrade,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel27,
            this.xrLabel10,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel6,
            this.xrLabel7,
            this.lblZoneCode,
            this.lblLocationNo,
            this.lblAssetCategory,
            this.xrLabel12,
            this.lblBuildingCode,
            this.xrLabel5,
            this.lblArAssetDescription,
            this.xrLabel2,
            this.lblCityCode,
            this.xrLabel4,
            this.lblAssetNo,
            this.lblAssetDescription,
            this.xrLabel45,
            this.xrLabel46});
        this.Detail.Dpi = 100F;
        this.Detail.HeightF = 412.25F;
        this.Detail.KeepTogether = true;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // lblNotesToTechnician
        // 
        this.lblNotesToTechnician.Dpi = 100F;
        this.lblNotesToTechnician.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblNotesToTechnician.ForeColor = System.Drawing.Color.Black;
        this.lblNotesToTechnician.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 327.625F);
        this.lblNotesToTechnician.Name = "lblNotesToTechnician";
        this.lblNotesToTechnician.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblNotesToTechnician.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblNotesToTechnician.StyleName = "FieldCaption";
        this.lblNotesToTechnician.StylePriority.UseFont = false;
        this.lblNotesToTechnician.StylePriority.UseForeColor = false;
        this.lblNotesToTechnician.StylePriority.UsePadding = false;
        this.lblNotesToTechnician.Text = "Notes To Technician :";
        // 
        // xrLabel62
        // 
        this.xrLabel62.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NotesToTech")});
        this.xrLabel62.Dpi = 100F;
        this.xrLabel62.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(716.7501F, 327.625F);
        this.xrLabel62.Name = "xrLabel62";
        this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel62.SizeF = new System.Drawing.SizeF(180F, 35.70834F);
        this.xrLabel62.StyleName = "DataField";
        this.xrLabel62.StylePriority.UseFont = false;
        this.xrLabel62.StylePriority.UsePadding = false;
        // 
        // lblCurrentValue
        // 
        this.lblCurrentValue.Dpi = 100F;
        this.lblCurrentValue.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblCurrentValue.ForeColor = System.Drawing.Color.Black;
        this.lblCurrentValue.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 300.625F);
        this.lblCurrentValue.Name = "lblCurrentValue";
        this.lblCurrentValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblCurrentValue.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblCurrentValue.StyleName = "FieldCaption";
        this.lblCurrentValue.StylePriority.UseFont = false;
        this.lblCurrentValue.StylePriority.UseForeColor = false;
        this.lblCurrentValue.StylePriority.UsePadding = false;
        this.lblCurrentValue.Text = "Current Value :";
        // 
        // xrLabel60
        // 
        this.xrLabel60.Dpi = 100F;
        this.xrLabel60.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(716.7501F, 300.625F);
        this.xrLabel60.Name = "xrLabel60";
        this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel60.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel60.StyleName = "DataField";
        this.xrLabel60.StylePriority.UseFont = false;
        this.xrLabel60.StylePriority.UsePadding = false;
        // 
        // xrLabel58
        // 
        this.xrLabel58.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Criticality")});
        this.xrLabel58.Dpi = 100F;
        this.xrLabel58.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 246.625F);
        this.xrLabel58.Name = "xrLabel58";
        this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel58.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel58.StyleName = "DataField";
        this.xrLabel58.StylePriority.UseFont = false;
        this.xrLabel58.StylePriority.UsePadding = false;
        // 
        // lblAssetCritcality
        // 
        this.lblAssetCritcality.Dpi = 100F;
        this.lblAssetCritcality.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetCritcality.ForeColor = System.Drawing.Color.Black;
        this.lblAssetCritcality.LocationFloat = new DevExpress.Utils.PointFloat(6.000026F, 246.625F);
        this.lblAssetCritcality.Name = "lblAssetCritcality";
        this.lblAssetCritcality.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetCritcality.SizeF = new System.Drawing.SizeF(147.7917F, 17.99998F);
        this.lblAssetCritcality.StyleName = "FieldCaption";
        this.lblAssetCritcality.StylePriority.UseFont = false;
        this.lblAssetCritcality.StylePriority.UseForeColor = false;
        this.lblAssetCritcality.StylePriority.UsePadding = false;
        this.lblAssetCritcality.Text = "Asset Criticality :";
        // 
        // xrLabel32
        // 
        this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Warranty_contract")});
        this.xrLabel32.Dpi = 100F;
        this.xrLabel32.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 34.00002F);
        this.xrLabel32.Name = "xrLabel32";
        this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel32.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel32.StyleName = "DataField";
        this.xrLabel32.StylePriority.UseFont = false;
        this.xrLabel32.StylePriority.UsePadding = false;
        // 
        // xrLabel33
        // 
        this.xrLabel33.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SupplierNo")});
        this.xrLabel33.Dpi = 100F;
        this.xrLabel33.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 10.00001F);
        this.xrLabel33.Name = "xrLabel33";
        this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel33.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel33.StyleName = "DataField";
        this.xrLabel33.StylePriority.UseFont = false;
        this.xrLabel33.StylePriority.UsePadding = false;
        // 
        // lblWarranty
        // 
        this.lblWarranty.Dpi = 100F;
        this.lblWarranty.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblWarranty.ForeColor = System.Drawing.Color.Black;
        this.lblWarranty.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 34.00002F);
        this.lblWarranty.Name = "lblWarranty";
        this.lblWarranty.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblWarranty.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblWarranty.StyleName = "FieldCaption";
        this.lblWarranty.StylePriority.UseFont = false;
        this.lblWarranty.StylePriority.UseForeColor = false;
        this.lblWarranty.StylePriority.UsePadding = false;
        this.lblWarranty.Text = "Warranty / Contract :";
        // 
        // lblContractor
        // 
        this.lblContractor.Dpi = 100F;
        this.lblContractor.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblContractor.ForeColor = System.Drawing.Color.Black;
        this.lblContractor.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 10.00001F);
        this.lblContractor.Name = "lblContractor";
        this.lblContractor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblContractor.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblContractor.StyleName = "FieldCaption";
        this.lblContractor.StylePriority.UseFont = false;
        this.lblContractor.StylePriority.UseForeColor = false;
        this.lblContractor.StylePriority.UsePadding = false;
        this.lblContractor.Text = "Contractor :";
        // 
        // xrLabel36
        // 
        this.xrLabel36.Dpi = 100F;
        this.xrLabel36.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 87.99998F);
        this.xrLabel36.Name = "xrLabel36";
        this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel36.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel36.StyleName = "DataField";
        this.xrLabel36.StylePriority.UseFont = false;
        this.xrLabel36.StylePriority.UsePadding = false;
        // 
        // lblWarrantyStatus
        // 
        this.lblWarrantyStatus.Dpi = 100F;
        this.lblWarrantyStatus.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblWarrantyStatus.ForeColor = System.Drawing.Color.Black;
        this.lblWarrantyStatus.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 87.99998F);
        this.lblWarrantyStatus.Name = "lblWarrantyStatus";
        this.lblWarrantyStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblWarrantyStatus.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblWarrantyStatus.StyleName = "FieldCaption";
        this.lblWarrantyStatus.StylePriority.UseFont = false;
        this.lblWarrantyStatus.StylePriority.UseForeColor = false;
        this.lblWarrantyStatus.StylePriority.UsePadding = false;
        this.lblWarrantyStatus.Text = "Warranty Status :";
        // 
        // xrLabel38
        // 
        this.xrLabel38.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Warranty_ContractExpiry")});
        this.xrLabel38.Dpi = 100F;
        this.xrLabel38.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 61F);
        this.xrLabel38.Name = "xrLabel38";
        this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel38.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel38.StyleName = "DataField";
        this.xrLabel38.StylePriority.UseFont = false;
        this.xrLabel38.StylePriority.UsePadding = false;
        // 
        // lblWarrantyExpiryDate
        // 
        this.lblWarrantyExpiryDate.Dpi = 100F;
        this.lblWarrantyExpiryDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblWarrantyExpiryDate.ForeColor = System.Drawing.Color.Black;
        this.lblWarrantyExpiryDate.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 61F);
        this.lblWarrantyExpiryDate.Name = "lblWarrantyExpiryDate";
        this.lblWarrantyExpiryDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblWarrantyExpiryDate.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblWarrantyExpiryDate.StyleName = "FieldCaption";
        this.lblWarrantyExpiryDate.StylePriority.UseFont = false;
        this.lblWarrantyExpiryDate.StylePriority.UseForeColor = false;
        this.lblWarrantyExpiryDate.StylePriority.UsePadding = false;
        this.lblWarrantyExpiryDate.Text = "Warranty Expiry Date :";
        // 
        // lblSupplier
        // 
        this.lblSupplier.Dpi = 100F;
        this.lblSupplier.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblSupplier.ForeColor = System.Drawing.Color.Black;
        this.lblSupplier.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 164.5833F);
        this.lblSupplier.Name = "lblSupplier";
        this.lblSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblSupplier.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblSupplier.StyleName = "FieldCaption";
        this.lblSupplier.StylePriority.UseFont = false;
        this.lblSupplier.StylePriority.UseForeColor = false;
        this.lblSupplier.StylePriority.UsePadding = false;
        this.lblSupplier.Text = "Supplier :";
        // 
        // xrlblAssetNotes
        // 
        this.xrlblAssetNotes.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SupplierNo")});
        this.xrlblAssetNotes.Dpi = 100F;
        this.xrlblAssetNotes.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrlblAssetNotes.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 164.5833F);
        this.xrlblAssetNotes.Name = "xrlblAssetNotes";
        this.xrlblAssetNotes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrlblAssetNotes.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrlblAssetNotes.StyleName = "DataField";
        this.xrlblAssetNotes.StylePriority.UseFont = false;
        this.xrlblAssetNotes.StylePriority.UsePadding = false;
        // 
        // lblPurchasePrice
        // 
        this.lblPurchasePrice.Dpi = 100F;
        this.lblPurchasePrice.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblPurchasePrice.ForeColor = System.Drawing.Color.Black;
        this.lblPurchasePrice.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 191.5833F);
        this.lblPurchasePrice.Name = "lblPurchasePrice";
        this.lblPurchasePrice.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblPurchasePrice.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblPurchasePrice.StyleName = "FieldCaption";
        this.lblPurchasePrice.StylePriority.UseFont = false;
        this.lblPurchasePrice.StylePriority.UseForeColor = false;
        this.lblPurchasePrice.StylePriority.UsePadding = false;
        this.lblPurchasePrice.Text = "Purchase Price :";
        // 
        // lblContractNotes
        // 
        this.lblContractNotes.Dpi = 100F;
        this.lblContractNotes.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblContractNotes.ForeColor = System.Drawing.Color.Black;
        this.lblContractNotes.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 113.5833F);
        this.lblContractNotes.Name = "lblContractNotes";
        this.lblContractNotes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblContractNotes.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblContractNotes.StyleName = "FieldCaption";
        this.lblContractNotes.StylePriority.UseFont = false;
        this.lblContractNotes.StylePriority.UseForeColor = false;
        this.lblContractNotes.StylePriority.UsePadding = false;
        this.lblContractNotes.Text = "Warranty / Contract Notes :";
        // 
        // lblAssetNotes
        // 
        this.lblAssetNotes.Dpi = 100F;
        this.lblAssetNotes.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetNotes.ForeColor = System.Drawing.Color.Black;
        this.lblAssetNotes.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 137.5833F);
        this.lblAssetNotes.Name = "lblAssetNotes";
        this.lblAssetNotes.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetNotes.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblAssetNotes.StyleName = "FieldCaption";
        this.lblAssetNotes.StylePriority.UseFont = false;
        this.lblAssetNotes.StylePriority.UseForeColor = false;
        this.lblAssetNotes.StylePriority.UsePadding = false;
        this.lblAssetNotes.Text = "Asset Notes :";
        // 
        // xrLabel47
        // 
        this.xrLabel47.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Warranty_ContractNotes")});
        this.xrLabel47.Dpi = 100F;
        this.xrLabel47.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 113.5833F);
        this.xrLabel47.Name = "xrLabel47";
        this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel47.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel47.StyleName = "DataField";
        this.xrLabel47.StylePriority.UseFont = false;
        this.xrLabel47.StylePriority.UsePadding = false;
        // 
        // xrlblManufacturer
        // 
        this.xrlblManufacturer.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Comments")});
        this.xrlblManufacturer.Dpi = 100F;
        this.xrlblManufacturer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrlblManufacturer.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 137.5833F);
        this.xrlblManufacturer.Name = "xrlblManufacturer";
        this.xrlblManufacturer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrlblManufacturer.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrlblManufacturer.StyleName = "DataField";
        this.xrlblManufacturer.StylePriority.UseFont = false;
        this.xrlblManufacturer.StylePriority.UsePadding = false;
        // 
        // xrLabel49
        // 
        this.xrLabel49.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PurchasePrice", "{0:0.00}")});
        this.xrLabel49.Dpi = 100F;
        this.xrLabel49.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 191.5833F);
        this.xrLabel49.Name = "xrLabel49";
        this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel49.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel49.StyleName = "DataField";
        this.xrLabel49.StylePriority.UseFont = false;
        this.xrLabel49.StylePriority.UsePadding = false;
        // 
        // xrLabel50
        // 
        this.xrLabel50.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DataDisposed")});
        this.xrLabel50.Dpi = 100F;
        this.xrLabel50.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 244.625F);
        this.xrLabel50.Name = "xrLabel50";
        this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel50.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel50.StyleName = "DataField";
        this.xrLabel50.StylePriority.UseFont = false;
        this.xrLabel50.StylePriority.UsePadding = false;
        // 
        // xrLabel51
        // 
        this.xrLabel51.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DateAcquired")});
        this.xrLabel51.Dpi = 100F;
        this.xrLabel51.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 220.625F);
        this.xrLabel51.Name = "xrLabel51";
        this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel51.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel51.StyleName = "DataField";
        this.xrLabel51.StylePriority.UseFont = false;
        this.xrLabel51.StylePriority.UsePadding = false;
        // 
        // lblDateDisposed
        // 
        this.lblDateDisposed.Dpi = 100F;
        this.lblDateDisposed.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblDateDisposed.ForeColor = System.Drawing.Color.Black;
        this.lblDateDisposed.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 244.625F);
        this.lblDateDisposed.Name = "lblDateDisposed";
        this.lblDateDisposed.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblDateDisposed.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblDateDisposed.StyleName = "FieldCaption";
        this.lblDateDisposed.StylePriority.UseFont = false;
        this.lblDateDisposed.StylePriority.UseForeColor = false;
        this.lblDateDisposed.StylePriority.UsePadding = false;
        this.lblDateDisposed.Text = "Date Disposed :";
        // 
        // lblDateCommissioned
        // 
        this.lblDateCommissioned.Dpi = 100F;
        this.lblDateCommissioned.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblDateCommissioned.ForeColor = System.Drawing.Color.Black;
        this.lblDateCommissioned.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 220.625F);
        this.lblDateCommissioned.Name = "lblDateCommissioned";
        this.lblDateCommissioned.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblDateCommissioned.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblDateCommissioned.StyleName = "FieldCaption";
        this.lblDateCommissioned.StylePriority.UseFont = false;
        this.lblDateCommissioned.StylePriority.UseForeColor = false;
        this.lblDateCommissioned.StylePriority.UsePadding = false;
        this.lblDateCommissioned.Text = "Date Comissioned :";
        // 
        // xrLabel54
        // 
        this.xrLabel54.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "EstLife")});
        this.xrLabel54.Dpi = 100F;
        this.xrLabel54.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(716.75F, 271.625F);
        this.xrLabel54.Name = "xrLabel54";
        this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel54.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel54.StyleName = "DataField";
        this.xrLabel54.StylePriority.UseFont = false;
        this.xrLabel54.StylePriority.UsePadding = false;
        // 
        // lblEstimatedLife
        // 
        this.lblEstimatedLife.Dpi = 100F;
        this.lblEstimatedLife.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblEstimatedLife.ForeColor = System.Drawing.Color.Black;
        this.lblEstimatedLife.LocationFloat = new DevExpress.Utils.PointFloat(525.75F, 271.625F);
        this.lblEstimatedLife.Name = "lblEstimatedLife";
        this.lblEstimatedLife.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblEstimatedLife.SizeF = new System.Drawing.SizeF(184F, 18F);
        this.lblEstimatedLife.StyleName = "FieldCaption";
        this.lblEstimatedLife.StylePriority.UseFont = false;
        this.lblEstimatedLife.StylePriority.UseForeColor = false;
        this.lblEstimatedLife.StylePriority.UsePadding = false;
        this.lblEstimatedLife.Text = "Estimated Life(Year) :";
        // 
        // lblManufactuer
        // 
        this.lblManufactuer.Dpi = 100F;
        this.lblManufactuer.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblManufactuer.ForeColor = System.Drawing.Color.Black;
        this.lblManufactuer.LocationFloat = new DevExpress.Utils.PointFloat(6.000026F, 383.4167F);
        this.lblManufactuer.Name = "lblManufactuer";
        this.lblManufactuer.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblManufactuer.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblManufactuer.StyleName = "FieldCaption";
        this.lblManufactuer.StylePriority.UseFont = false;
        this.lblManufactuer.StylePriority.UseForeColor = false;
        this.lblManufactuer.StylePriority.UsePadding = false;
        this.lblManufactuer.Text = "Manufacturer :";
        // 
        // xrLabel31
        // 
        this.xrLabel31.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Manufacturer")});
        this.xrLabel31.Dpi = 100F;
        this.xrLabel31.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 383.4167F);
        this.xrLabel31.Name = "xrLabel31";
        this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel31.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel31.StyleName = "DataField";
        this.xrLabel31.StylePriority.UseFont = false;
        this.xrLabel31.StylePriority.UsePadding = false;
        // 
        // xrLabel28
        // 
        this.xrLabel28.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SerialNumber")});
        this.xrLabel28.Dpi = 100F;
        this.xrLabel28.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 355.375F);
        this.xrLabel28.Name = "xrLabel28";
        this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel28.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel28.StyleName = "DataField";
        this.xrLabel28.StylePriority.UseFont = false;
        this.xrLabel28.StylePriority.UsePadding = false;
        // 
        // lblSerialNo
        // 
        this.lblSerialNo.Dpi = 100F;
        this.lblSerialNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblSerialNo.ForeColor = System.Drawing.Color.Black;
        this.lblSerialNo.LocationFloat = new DevExpress.Utils.PointFloat(5.999947F, 355.375F);
        this.lblSerialNo.Name = "lblSerialNo";
        this.lblSerialNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblSerialNo.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblSerialNo.StyleName = "FieldCaption";
        this.lblSerialNo.StylePriority.UseFont = false;
        this.lblSerialNo.StylePriority.UseForeColor = false;
        this.lblSerialNo.StylePriority.UsePadding = false;
        this.lblSerialNo.Text = "Serial No :";
        // 
        // lblAuthEmp
        // 
        this.lblAuthEmp.Dpi = 100F;
        this.lblAuthEmp.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAuthEmp.ForeColor = System.Drawing.Color.Black;
        this.lblAuthEmp.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 300.625F);
        this.lblAuthEmp.Name = "lblAuthEmp";
        this.lblAuthEmp.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAuthEmp.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblAuthEmp.StyleName = "FieldCaption";
        this.lblAuthEmp.StylePriority.UseFont = false;
        this.lblAuthEmp.StylePriority.UseForeColor = false;
        this.lblAuthEmp.StylePriority.UsePadding = false;
        this.lblAuthEmp.Text = "Auth. Employee :";
        // 
        // xrLabel21
        // 
        this.xrLabel21.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "EmployeeNO")});
        this.xrLabel21.Dpi = 100F;
        this.xrLabel21.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 300.625F);
        this.xrLabel21.Name = "xrLabel21";
        this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel21.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel21.StyleName = "DataField";
        this.xrLabel21.StylePriority.UseFont = false;
        this.xrLabel21.StylePriority.UsePadding = false;
        // 
        // lblModelNo
        // 
        this.lblModelNo.Dpi = 100F;
        this.lblModelNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblModelNo.ForeColor = System.Drawing.Color.Black;
        this.lblModelNo.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 327.625F);
        this.lblModelNo.Name = "lblModelNo";
        this.lblModelNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblModelNo.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblModelNo.StyleName = "FieldCaption";
        this.lblModelNo.StylePriority.UseFont = false;
        this.lblModelNo.StylePriority.UseForeColor = false;
        this.lblModelNo.StylePriority.UsePadding = false;
        this.lblModelNo.Text = "Model No :";
        // 
        // lblAssetStatus
        // 
        this.lblAssetStatus.Dpi = 100F;
        this.lblAssetStatus.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetStatus.ForeColor = System.Drawing.Color.Black;
        this.lblAssetStatus.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 220.625F);
        this.lblAssetStatus.Name = "lblAssetStatus";
        this.lblAssetStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetStatus.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblAssetStatus.StyleName = "FieldCaption";
        this.lblAssetStatus.StylePriority.UseFont = false;
        this.lblAssetStatus.StylePriority.UseForeColor = false;
        this.lblAssetStatus.StylePriority.UsePadding = false;
        this.lblAssetStatus.Text = "Asset Status :";
        // 
        // lblTrade
        // 
        this.lblTrade.Dpi = 100F;
        this.lblTrade.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblTrade.ForeColor = System.Drawing.Color.Black;
        this.lblTrade.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 273.625F);
        this.lblTrade.Name = "lblTrade";
        this.lblTrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblTrade.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblTrade.StyleName = "FieldCaption";
        this.lblTrade.StylePriority.UseFont = false;
        this.lblTrade.StylePriority.UseForeColor = false;
        this.lblTrade.StylePriority.UsePadding = false;
        this.lblTrade.Text = "Trade :";
        // 
        // xrLabel25
        // 
        this.xrLabel25.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetStatusDesc")});
        this.xrLabel25.Dpi = 100F;
        this.xrLabel25.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 220.625F);
        this.xrLabel25.Name = "xrLabel25";
        this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel25.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel25.StyleName = "DataField";
        this.xrLabel25.StylePriority.UseFont = false;
        this.xrLabel25.StylePriority.UsePadding = false;
        // 
        // xrLabel26
        // 
        this.xrLabel26.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkTrade")});
        this.xrLabel26.Dpi = 100F;
        this.xrLabel26.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 273.625F);
        this.xrLabel26.Name = "xrLabel26";
        this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel26.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel26.StyleName = "DataField";
        this.xrLabel26.StylePriority.UseFont = false;
        this.xrLabel26.StylePriority.UsePadding = false;
        // 
        // xrLabel27
        // 
        this.xrLabel27.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ModelNumber")});
        this.xrLabel27.Dpi = 100F;
        this.xrLabel27.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 327.625F);
        this.xrLabel27.Name = "xrLabel27";
        this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel27.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel27.StyleName = "DataField";
        this.xrLabel27.StylePriority.UseFont = false;
        this.xrLabel27.StylePriority.UsePadding = false;
        // 
        // xrLabel10
        // 
        this.xrLabel10.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetCatCode")});
        this.xrLabel10.Dpi = 100F;
        this.xrLabel10.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 191.5833F);
        this.xrLabel10.Name = "xrLabel10";
        this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel10.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel10.StyleName = "DataField";
        this.xrLabel10.StylePriority.UseFont = false;
        this.xrLabel10.StylePriority.UsePadding = false;
        // 
        // xrLabel16
        // 
        this.xrLabel16.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L5Description")});
        this.xrLabel16.Dpi = 100F;
        this.xrLabel16.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(340.7918F, 164.5833F);
        this.xrLabel16.Name = "xrLabel16";
        this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel16.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel16.StyleName = "DataField";
        this.xrLabel16.StylePriority.UseFont = false;
        this.xrLabel16.StylePriority.UsePadding = false;
        // 
        // xrLabel15
        // 
        this.xrLabel15.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L4Description")});
        this.xrLabel15.Dpi = 100F;
        this.xrLabel15.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(340.7918F, 137.5833F);
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel15.StyleName = "DataField";
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UsePadding = false;
        // 
        // xrLabel14
        // 
        this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LocationDescription")});
        this.xrLabel14.Dpi = 100F;
        this.xrLabel14.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(340.7918F, 113.5833F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel14.StyleName = "DataField";
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UsePadding = false;
        // 
        // xrLabel6
        // 
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L4No")});
        this.xrLabel6.Dpi = 100F;
        this.xrLabel6.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 137.5833F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel6.StyleName = "DataField";
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UsePadding = false;
        // 
        // xrLabel7
        // 
        this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LocationNo")});
        this.xrLabel7.Dpi = 100F;
        this.xrLabel7.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 113.5833F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel7.StyleName = "DataField";
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UsePadding = false;
        // 
        // lblZoneCode
        // 
        this.lblZoneCode.Dpi = 100F;
        this.lblZoneCode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblZoneCode.ForeColor = System.Drawing.Color.Black;
        this.lblZoneCode.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 137.5833F);
        this.lblZoneCode.Name = "lblZoneCode";
        this.lblZoneCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblZoneCode.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblZoneCode.StyleName = "FieldCaption";
        this.lblZoneCode.StylePriority.UseFont = false;
        this.lblZoneCode.StylePriority.UseForeColor = false;
        this.lblZoneCode.StylePriority.UsePadding = false;
        this.lblZoneCode.Text = "Zone Code :";
        // 
        // lblLocationNo
        // 
        this.lblLocationNo.Dpi = 100F;
        this.lblLocationNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblLocationNo.ForeColor = System.Drawing.Color.Black;
        this.lblLocationNo.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 113.5833F);
        this.lblLocationNo.Name = "lblLocationNo";
        this.lblLocationNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblLocationNo.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblLocationNo.StyleName = "FieldCaption";
        this.lblLocationNo.StylePriority.UseFont = false;
        this.lblLocationNo.StylePriority.UseForeColor = false;
        this.lblLocationNo.StylePriority.UsePadding = false;
        this.lblLocationNo.Text = "Location No :";
        // 
        // lblAssetCategory
        // 
        this.lblAssetCategory.Dpi = 100F;
        this.lblAssetCategory.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetCategory.ForeColor = System.Drawing.Color.Black;
        this.lblAssetCategory.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 191.5833F);
        this.lblAssetCategory.Name = "lblAssetCategory";
        this.lblAssetCategory.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetCategory.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblAssetCategory.StyleName = "FieldCaption";
        this.lblAssetCategory.StylePriority.UseFont = false;
        this.lblAssetCategory.StylePriority.UseForeColor = false;
        this.lblAssetCategory.StylePriority.UsePadding = false;
        this.lblAssetCategory.Text = "Asset Category :";
        // 
        // xrLabel12
        // 
        this.xrLabel12.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L5No")});
        this.xrLabel12.Dpi = 100F;
        this.xrLabel12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 164.5833F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel12.StyleName = "DataField";
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UsePadding = false;
        // 
        // lblBuildingCode
        // 
        this.lblBuildingCode.Dpi = 100F;
        this.lblBuildingCode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblBuildingCode.ForeColor = System.Drawing.Color.Black;
        this.lblBuildingCode.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 164.5833F);
        this.lblBuildingCode.Name = "lblBuildingCode";
        this.lblBuildingCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblBuildingCode.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblBuildingCode.StyleName = "FieldCaption";
        this.lblBuildingCode.StylePriority.UseFont = false;
        this.lblBuildingCode.StylePriority.UseForeColor = false;
        this.lblBuildingCode.StylePriority.UsePadding = false;
        this.lblBuildingCode.Text = "Building Code :";
        // 
        // xrLabel5
        // 
        this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L2name")});
        this.xrLabel5.Dpi = 100F;
        this.xrLabel5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(340.7917F, 86.99999F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel5.StyleName = "DataField";
        this.xrLabel5.StylePriority.UseFont = false;
        this.xrLabel5.StylePriority.UsePadding = false;
        // 
        // lblArAssetDescription
        // 
        this.lblArAssetDescription.Dpi = 100F;
        this.lblArAssetDescription.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblArAssetDescription.ForeColor = System.Drawing.Color.Black;
        this.lblArAssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(5.999994F, 61F);
        this.lblArAssetDescription.Name = "lblArAssetDescription";
        this.lblArAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblArAssetDescription.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblArAssetDescription.StyleName = "FieldCaption";
        this.lblArAssetDescription.StylePriority.UseFont = false;
        this.lblArAssetDescription.StylePriority.UseForeColor = false;
        this.lblArAssetDescription.StylePriority.UsePadding = false;
        this.lblArAssetDescription.Text = "Arabic Asset Description :";
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetAltDescription")});
        this.xrLabel2.Dpi = 100F;
        this.xrLabel2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 61F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel2.StyleName = "DataField";
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UsePadding = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        // 
        // lblCityCode
        // 
        this.lblCityCode.Dpi = 100F;
        this.lblCityCode.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblCityCode.ForeColor = System.Drawing.Color.Black;
        this.lblCityCode.LocationFloat = new DevExpress.Utils.PointFloat(5.999994F, 88.00002F);
        this.lblCityCode.Name = "lblCityCode";
        this.lblCityCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblCityCode.SizeF = new System.Drawing.SizeF(147.7917F, 17.99999F);
        this.lblCityCode.StyleName = "FieldCaption";
        this.lblCityCode.StylePriority.UseFont = false;
        this.lblCityCode.StylePriority.UseForeColor = false;
        this.lblCityCode.StylePriority.UsePadding = false;
        this.lblCityCode.Text = "City Code :";
        // 
        // xrLabel4
        // 
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L2Code")});
        this.xrLabel4.Dpi = 100F;
        this.xrLabel4.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 88F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel4.StyleName = "DataField";
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UsePadding = false;
        // 
        // lblAssetNo
        // 
        this.lblAssetNo.Dpi = 100F;
        this.lblAssetNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetNo.ForeColor = System.Drawing.Color.Black;
        this.lblAssetNo.LocationFloat = new DevExpress.Utils.PointFloat(6.000002F, 10.00001F);
        this.lblAssetNo.Name = "lblAssetNo";
        this.lblAssetNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetNo.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblAssetNo.StyleName = "FieldCaption";
        this.lblAssetNo.StylePriority.UseFont = false;
        this.lblAssetNo.StylePriority.UseForeColor = false;
        this.lblAssetNo.StylePriority.UsePadding = false;
        this.lblAssetNo.Text = "Asset No. :";
        // 
        // lblAssetDescription
        // 
        this.lblAssetDescription.Dpi = 100F;
        this.lblAssetDescription.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.lblAssetDescription.ForeColor = System.Drawing.Color.Black;
        this.lblAssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(6.000002F, 34F);
        this.lblAssetDescription.Name = "lblAssetDescription";
        this.lblAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.lblAssetDescription.SizeF = new System.Drawing.SizeF(147.7917F, 18F);
        this.lblAssetDescription.StyleName = "FieldCaption";
        this.lblAssetDescription.StylePriority.UseFont = false;
        this.lblAssetDescription.StylePriority.UseForeColor = false;
        this.lblAssetDescription.StylePriority.UsePadding = false;
        this.lblAssetDescription.Text = "Asset Description :";
        // 
        // xrLabel45
        // 
        this.xrLabel45.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetNumber")});
        this.xrLabel45.Dpi = 100F;
        this.xrLabel45.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 10F);
        this.xrLabel45.Name = "xrLabel45";
        this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel45.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel45.StyleName = "DataField";
        this.xrLabel45.StylePriority.UseFont = false;
        this.xrLabel45.StylePriority.UsePadding = false;
        // 
        // xrLabel46
        // 
        this.xrLabel46.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetDescription")});
        this.xrLabel46.Dpi = 100F;
        this.xrLabel46.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(153.7917F, 34F);
        this.xrLabel46.Name = "xrLabel46";
        this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
        this.xrLabel46.SizeF = new System.Drawing.SizeF(180F, 18F);
        this.xrLabel46.StyleName = "DataField";
        this.xrLabel46.StylePriority.UseFont = false;
        this.xrLabel46.StylePriority.UsePadding = false;
        // 
        // TopMargin
        // 
        this.TopMargin.Dpi = 100F;
        this.TopMargin.HeightF = 1F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Dpi = 100F;
        this.BottomMargin.HeightF = 0F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // pageFooterBand1
        // 
        this.pageFooterBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo2,
            this.xrSubreport2});
        this.pageFooterBand1.Dpi = 100F;
        this.pageFooterBand1.HeightF = 132.125F;
        this.pageFooterBand1.Name = "pageFooterBand1";
        // 
        // xrPageInfo1
        // 
        this.xrPageInfo1.Dpi = 100F;
        this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 109.1249F);
        this.xrPageInfo1.Name = "xrPageInfo1";
        this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
        this.xrPageInfo1.SizeF = new System.Drawing.SizeF(313F, 23F);
        this.xrPageInfo1.StyleName = "PageInfo";
        // 
        // xrPageInfo2
        // 
        this.xrPageInfo2.Dpi = 100F;
        this.xrPageInfo2.Format = "Page {0} of {1}";
        this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(670.7501F, 109.1249F);
        this.xrPageInfo2.Name = "xrPageInfo2";
        this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrPageInfo2.SizeF = new System.Drawing.SizeF(226F, 23F);
        this.xrPageInfo2.StyleName = "PageInfo";
        this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        // 
        // accountId
        // 
        this.accountId.Description = "accountId";
        this.accountId.Name = "accountId";
        this.accountId.Type = typeof(int);
        this.accountId.ValueInfo = "0";
        // 
        // lang
        // 
        this.lang.Description = "lang";
        this.lang.Name = "lang";
        this.lang.Type = typeof(bool);
        this.lang.ValueInfo = "False";
        // 
        // reportHeaderBand1
        // 
        this.reportHeaderBand1.Dpi = 100F;
        this.reportHeaderBand1.HeightF = 0F;
        this.reportHeaderBand1.Name = "reportHeaderBand1";
        // 
        // Title
        // 
        this.Title.BackColor = System.Drawing.Color.Transparent;
        this.Title.BorderColor = System.Drawing.Color.Black;
        this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.Title.BorderWidth = 1F;
        this.Title.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold);
        this.Title.ForeColor = System.Drawing.Color.Maroon;
        this.Title.Name = "Title";
        // 
        // FieldCaption
        // 
        this.FieldCaption.BackColor = System.Drawing.Color.Transparent;
        this.FieldCaption.BorderColor = System.Drawing.Color.Black;
        this.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.FieldCaption.BorderWidth = 1F;
        this.FieldCaption.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
        this.FieldCaption.ForeColor = System.Drawing.Color.Maroon;
        this.FieldCaption.Name = "FieldCaption";
        // 
        // PageInfo
        // 
        this.PageInfo.BackColor = System.Drawing.Color.Transparent;
        this.PageInfo.BorderColor = System.Drawing.Color.Black;
        this.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.PageInfo.BorderWidth = 1F;
        this.PageInfo.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
        this.PageInfo.ForeColor = System.Drawing.Color.Black;
        this.PageInfo.Name = "PageInfo";
        // 
        // DataField
        // 
        this.DataField.BackColor = System.Drawing.Color.Transparent;
        this.DataField.BorderColor = System.Drawing.Color.Black;
        this.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.DataField.BorderWidth = 1F;
        this.DataField.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.DataField.ForeColor = System.Drawing.Color.Black;
        this.DataField.Name = "DataField";
        this.DataField.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        // 
        // PageHeader
        // 
        this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
        this.PageHeader.Dpi = 100F;
        this.PageHeader.HeightF = 87.88044F;
        this.PageHeader.Name = "PageHeader";
        // 
        // header
        // 
        this.header.Name = "header";
        // 
        // l1Id
        // 
        this.l1Id.Name = "l1Id";
        this.l1Id.Type = typeof(int);
        this.l1Id.ValueInfo = "0";
        // 
        // ReportFooter
        // 
        this.ReportFooter.Dpi = 100F;
        this.ReportFooter.HeightF = 0F;
        this.ReportFooter.Name = "ReportFooter";
        // 
        // employeeId
        // 
        this.employeeId.Description = "employeeId";
        this.employeeId.Name = "employeeId";
        this.employeeId.Type = typeof(int);
        this.employeeId.ValueInfo = "0";
        // 
        // isCentral
        // 
        this.isCentral.Description = "isCentral";
        this.isCentral.Name = "isCentral";
        this.isCentral.Type = typeof(bool);
        this.isCentral.ValueInfo = "False";
        // 
        // l2Id
        // 
        this.l2Id.Description = "l2Id";
        this.l2Id.Name = "l2Id";
        // 
        // l3Id
        // 
        this.l3Id.Description = "l3Id";
        this.l3Id.Name = "l3Id";
        // 
        // l4Id
        // 
        this.l4Id.Description = "l4Id";
        this.l4Id.Name = "l4Id";
        // 
        // l5Id
        // 
        this.l5Id.Description = "l5Id";
        this.l5Id.Name = "l5Id";
        // 
        // locationId
        // 
        this.locationId.Description = "locationId";
        this.locationId.Name = "locationId";
        this.locationId.Type = typeof(int);
        this.locationId.ValueInfo = "0";
        // 
        // objectDataSource1
        // 
        this.objectDataSource1.Constructor = objectConstructorInfo1;
        this.objectDataSource1.DataMember = "GetAssetDetails";
        this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.AssetReportService);
        this.objectDataSource1.Name = "objectDataSource1";
        parameter1.Name = "accountId";
        parameter1.Type = typeof(DevExpress.DataAccess.Expression);
        parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.accountId]", typeof(int));
        parameter2.Name = "isCentral";
        parameter2.Type = typeof(DevExpress.DataAccess.Expression);
        parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.isCentral]", typeof(bool));
        parameter3.Name = "lang";
        parameter3.Type = typeof(DevExpress.DataAccess.Expression);
        parameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.lang]", typeof(bool));
        parameter4.Name = "employeeId";
        parameter4.Type = typeof(DevExpress.DataAccess.Expression);
        parameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.employeeId]", typeof(int));
        parameter5.Name = "l2Id";
        parameter5.Type = typeof(DevExpress.DataAccess.Expression);
        parameter5.Value = new DevExpress.DataAccess.Expression("[Parameters.l2Id]", typeof(string));
        parameter6.Name = "l3Id";
        parameter6.Type = typeof(DevExpress.DataAccess.Expression);
        parameter6.Value = new DevExpress.DataAccess.Expression("[Parameters.l3Id]", typeof(string));
        parameter7.Name = "l4Id";
        parameter7.Type = typeof(DevExpress.DataAccess.Expression);
        parameter7.Value = new DevExpress.DataAccess.Expression("[Parameters.l4Id]", typeof(string));
        parameter8.Name = "l5Id";
        parameter8.Type = typeof(DevExpress.DataAccess.Expression);
        parameter8.Value = new DevExpress.DataAccess.Expression("[Parameters.l5Id]", typeof(string));
        parameter9.Name = "locationId";
        parameter9.Type = typeof(DevExpress.DataAccess.Expression);
        parameter9.Value = new DevExpress.DataAccess.Expression("[Parameters.locationId]", typeof(int));
        this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2,
            parameter3,
            parameter4,
            parameter5,
            parameter6,
            parameter7,
            parameter8,
            parameter9});
        // 
        // xrSubreport2
        // 
        this.xrSubreport2.Dpi = 100F;
        this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(2.750127F, 0F);
        this.xrSubreport2.Name = "xrSubreport2";
        this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
        this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
        this.xrSubreport2.ReportSource = new FooterSubReport();
        this.xrSubreport2.SizeF = new System.Drawing.SizeF(891.2499F, 87.24995F);
        // 
        // xrSubreport1
        // 
        this.xrSubreport1.Dpi = 100F;
        this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(4.750005F, 0F);
        this.xrSubreport1.Name = "xrSubreport1";
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("header", this.header));
        this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l1Id", this.l1Id));
        this.xrSubreport1.ReportSource = new HeaderSubReport();
        this.xrSubreport1.SizeF = new System.Drawing.SizeF(892.0001F, 83.41669F);
        // 
        // AssetDetails
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.pageFooterBand1,
            this.reportHeaderBand1,
            this.PageHeader,
            this.ReportFooter});
        this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
        this.DataSource = this.objectDataSource1;
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(10, 186, 1, 0);
        this.PageHeight = 850;
        this.PageWidth = 1100;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.accountId,
            this.lang,
            this.employeeId,
            this.isCentral,
            this.l2Id,
            this.l3Id,
            this.l4Id,
            this.l5Id,
            this.locationId,
            this.header,
            this.l1Id});
        this.RequestParameters = false;
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.FieldCaption,
            this.PageInfo,
            this.DataField});
        this.Version = "16.2";
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion
}
