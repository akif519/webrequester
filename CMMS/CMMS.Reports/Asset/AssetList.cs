﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for AssetList
/// </summary>
public class AssetList : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private PageHeaderBand pageHeaderBand1;
    private XRTableRow xrTableRow1;
    private XRTableCell xrTableCell1;
    private XRTableCell xrTableCell2;
    private XRTableCell xrTableCell3;
    private XRTableRow xrTableRow2;
    private XRTableCell xrTableCell4;
    private XRTableCell xrTableCell5;
    private XRTableCell xrTableCell6;
    private PageFooterBand pageFooterBand1;
    private XRPageInfo xrPageInfo1;
    private XRPageInfo xrPageInfo2;
    private XRControlStyle Title;
    private XRControlStyle FieldCaption;
    private XRControlStyle PageInfo;
    private XRControlStyle DataField;
    private XRTable xrTable1;
    private XRTableRow xrTableRow3;
    private XRTableCell lblAssetCatCode;
    private XRTableCell lblAssetDescription;
    private XRTableCell lblAssetNo;
    private XRTableCell lblL3;
    private XRTableCell lblL2Code;
    private XRTableCell lbll5;
    private XRTableCell lblSrNo;
    private XRTable xrTable2;
    private XRTableRow xrTableRow4;
    private XRTableCell SrNoValue;
    private XRTableCell assetCatCodeValue;
    private XRTableCell assetDescValue;
    private XRTableCell AssetNoValue;
    private XRTableCell L3Value;
    private XRTableCell L5value;
    private XRTableCell L2CodeValue;
    private XRTableCell ModelNoValue;
    private XRTableCell lblModelNo;
    private XRTableCell lblSerialNo;
    private XRTableCell SerailNoValue;
    private XRTableCell location;
    private XRTableCell lbllocation;
    private XRControlStyle EvenStyle;
    private XRControlStyle OddStyle;
    private XRControlStyle GridHeader;
    private DevExpress.XtraReports.Parameters.Parameter l2Id;
    private DevExpress.XtraReports.Parameters.Parameter lang;
    private ReportHeaderBand reportHeaderBand1;
    private XRSubreport xrSubreport1;
    private XRSubreport xrSubreport2;
    private DevExpress.XtraReports.Parameters.Parameter accountId;
    private DevExpress.XtraReports.Parameters.Parameter isCentral;
    private DevExpress.XtraReports.Parameters.Parameter employeeId;
    private DevExpress.XtraReports.Parameters.Parameter l3Id;
    private DevExpress.XtraReports.Parameters.Parameter l4Id;
    private DevExpress.XtraReports.Parameters.Parameter l5Id;
    private DevExpress.XtraReports.Parameters.Parameter locationId;
    private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    private XRTableCell xrTableCell8;
    private XRTableCell xrTableCell7;
    private DevExpress.XtraReports.Parameters.Parameter header;
    private DevExpress.XtraReports.Parameters.Parameter l1Id;
    private DevExpress.XtraReports.Parameters.Parameter reportBy;
    private DevExpress.XtraReports.Parameters.Parameter dateRange;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public AssetList()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter6 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter7 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter8 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter9 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.SrNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.AssetNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.assetDescValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.ModelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.SerailNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.assetCatCodeValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.L2CodeValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.L3Value = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.L5value = new DevExpress.XtraReports.UI.XRTableCell();
            this.location = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.pageHeaderBand1 = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSrNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAssetNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAssetDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblModelNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSerialNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAssetCatCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblL2Code = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblL3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbll5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lbllocation = new DevExpress.XtraReports.UI.XRTableCell();
            this.lang = new DevExpress.XtraReports.Parameters.Parameter();
            this.header = new DevExpress.XtraReports.Parameters.Parameter();
            this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.accountId = new DevExpress.XtraReports.Parameters.Parameter();
            this.reportBy = new DevExpress.XtraReports.Parameters.Parameter();
            this.dateRange = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.pageFooterBand1 = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.FieldCaption = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DataField = new DevExpress.XtraReports.UI.XRControlStyle();
            this.EvenStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.OddStyle = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GridHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            this.l2Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.isCentral = new DevExpress.XtraReports.Parameters.Parameter();
            this.employeeId = new DevExpress.XtraReports.Parameters.Parameter();
            this.l3Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.l4Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.l5Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.locationId = new DevExpress.XtraReports.Parameters.Parameter();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.reportBy = new DevExpress.XtraReports.Parameters.Parameter();
            this.dateRange = new DevExpress.XtraReports.Parameters.Parameter();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 29.20831F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1063F, 29.20831F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.SrNoValue,
            this.AssetNoValue,
            this.assetDescValue,
            this.ModelNoValue,
            this.SerailNoValue,
            this.assetCatCodeValue,
            this.L2CodeValue,
            this.L3Value,
            this.xrTableCell8,
            this.L5value,
            this.location});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.EvenStyleName = "EvenStyle";
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.OddStyleName = "OddStyle";
            this.xrTableRow4.Weight = 1D;
            // 
            // SrNoValue
            // 
            this.SrNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SrNoValue.Dpi = 100F;
            this.SrNoValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrNoValue.Multiline = true;
            this.SrNoValue.Name = "SrNoValue";
            this.SrNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.SrNoValue.StylePriority.UseBorders = false;
            this.SrNoValue.StylePriority.UseFont = false;
            this.SrNoValue.StylePriority.UsePadding = false;
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.SrNoValue.Summary = xrSummary1;
            this.SrNoValue.Weight = 22.429356426454603D;
            // 
            // AssetNoValue
            // 
            this.AssetNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AssetNoValue.CanShrink = true;
            this.AssetNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetNumber")});
            this.AssetNoValue.Dpi = 100F;
            this.AssetNoValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssetNoValue.Multiline = true;
            this.AssetNoValue.Name = "AssetNoValue";
            this.AssetNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.AssetNoValue.StylePriority.UseBorders = false;
            this.AssetNoValue.StylePriority.UseFont = false;
            this.AssetNoValue.StylePriority.UsePadding = false;
            this.AssetNoValue.Weight = 39.874411064744649D;
            // 
            // assetDescValue
            // 
            this.assetDescValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.assetDescValue.CanShrink = true;
            this.assetDescValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetDesc")});
            this.assetDescValue.Dpi = 100F;
            this.assetDescValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assetDescValue.Multiline = true;
            this.assetDescValue.Name = "assetDescValue";
            this.assetDescValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.assetDescValue.StylePriority.UseBorders = false;
            this.assetDescValue.StylePriority.UseFont = false;
            this.assetDescValue.StylePriority.UsePadding = false;
            this.assetDescValue.Weight = 74.764520518371853D;
            // 
            // ModelNoValue
            // 
            this.ModelNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ModelNoValue.CanShrink = true;
            this.ModelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ModelNumber")});
            this.ModelNoValue.Dpi = 100F;
            this.ModelNoValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ModelNoValue.Multiline = true;
            this.ModelNoValue.Name = "ModelNoValue";
            this.ModelNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.ModelNoValue.StylePriority.UseBorders = false;
            this.ModelNoValue.StylePriority.UseFont = false;
            this.ModelNoValue.StylePriority.UsePadding = false;
            this.ModelNoValue.Weight = 49.843013451081532D;
            // 
            // SerailNoValue
            // 
            this.SerailNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SerailNoValue.CanShrink = true;
            this.SerailNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SerialNumber")});
            this.SerailNoValue.Dpi = 100F;
            this.SerailNoValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SerailNoValue.Multiline = true;
            this.SerailNoValue.Name = "SerailNoValue";
            this.SerailNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.SerailNoValue.StylePriority.UseBorders = false;
            this.SerailNoValue.StylePriority.UseFont = false;
            this.SerailNoValue.StylePriority.UsePadding = false;
            this.SerailNoValue.Weight = 49.843013375739091D;
            // 
            // assetCatCodeValue
            // 
            this.assetCatCodeValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.assetCatCodeValue.CanShrink = true;
            this.assetCatCodeValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetCatCode")});
            this.assetCatCodeValue.Dpi = 100F;
            this.assetCatCodeValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assetCatCodeValue.Multiline = true;
            this.assetCatCodeValue.Name = "assetCatCodeValue";
            this.assetCatCodeValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.assetCatCodeValue.StylePriority.UseBorders = false;
            this.assetCatCodeValue.StylePriority.UseFont = false;
            this.assetCatCodeValue.StylePriority.UsePadding = false;
            this.assetCatCodeValue.Weight = 39.874411471945756D;
            // 
            // L2CodeValue
            // 
            this.L2CodeValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.L2CodeValue.CanShrink = true;
            this.L2CodeValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L2Code")});
            this.L2CodeValue.Dpi = 100F;
            this.L2CodeValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.L2CodeValue.Multiline = true;
            this.L2CodeValue.Name = "L2CodeValue";
            this.L2CodeValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.L2CodeValue.StylePriority.UseBorders = false;
            this.L2CodeValue.StylePriority.UseFont = false;
            this.L2CodeValue.StylePriority.UsePadding = false;
            this.L2CodeValue.Weight = 29.905808800336754D;
            // 
            // L3Value
            // 
            this.L3Value.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.L3Value.CanShrink = true;
            this.L3Value.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L3Desc")});
            this.L3Value.Dpi = 100F;
            this.L3Value.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.L3Value.Multiline = true;
            this.L3Value.Name = "L3Value";
            this.L3Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.L3Value.StylePriority.UseBorders = false;
            this.L3Value.StylePriority.UseFont = false;
            this.L3Value.StylePriority.UsePadding = false;
            this.L3Value.Weight = 74.764521680522563D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L4Desc")});
            this.xrTableCell8.Dpi = 100F;
            this.xrTableCell8.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.Weight = 74.764521680522563D;
            // 
            // L5value
            // 
            this.L5value.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.L5value.CanShrink = true;
            this.L5value.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L5Desc")});
            this.L5value.Dpi = 100F;
            this.L5value.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.L5value.Multiline = true;
            this.L5value.Name = "L5value";
            this.L5value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.L5value.StylePriority.UseBorders = false;
            this.L5value.StylePriority.UseFont = false;
            this.L5value.StylePriority.UsePadding = false;
            this.L5value.Weight = 74.764519631856786D;
            // 
            // location
            // 
            this.location.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.location.CanShrink = true;
            this.location.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LocationDesc")});
            this.location.Dpi = 100F;
            this.location.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.location.Multiline = true;
            this.location.Name = "location";
            this.location.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.location.StylePriority.UseBorders = false;
            this.location.StylePriority.UseFont = false;
            this.location.StylePriority.UsePadding = false;
            this.location.Weight = 74.76452246421438D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // pageHeaderBand1
            // 
            this.pageHeaderBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrSubreport1});
            this.pageHeaderBand1.Dpi = 100F;
            this.pageHeaderBand1.HeightF = 135.7501F;
            this.pageHeaderBand1.Name = "pageHeaderBand1";
            this.pageHeaderBand1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.pageHeaderBand1_BeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 99.75008F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1063F, 36F);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSrNo,
            this.lblAssetNo,
            this.lblAssetDescription,
            this.lblModelNo,
            this.lblSerialNo,
            this.lblAssetCatCode,
            this.lblL2Code,
            this.lblL3,
            this.xrTableCell7,
            this.lbll5,
            this.lbllocation});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.StylePriority.UseFont = false;
            this.xrTableRow3.Weight = 1D;
            // 
            // lblSrNo
            // 
            this.lblSrNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSrNo.CanGrow = false;
            this.lblSrNo.Dpi = 100F;
            this.lblSrNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSrNo.Name = "lblSrNo";
            this.lblSrNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblSrNo.StyleName = "GridHeader";
            this.lblSrNo.StylePriority.UseBorders = false;
            this.lblSrNo.StylePriority.UseFont = false;
            this.lblSrNo.StylePriority.UsePadding = false;
            this.lblSrNo.StylePriority.UseTextAlignment = false;
            this.lblSrNo.Text = "#";
            this.lblSrNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSrNo.Weight = 22.429354995670824D;
            // 
            // lblAssetNo
            // 
            this.lblAssetNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAssetNo.CanGrow = false;
            this.lblAssetNo.Dpi = 100F;
            this.lblAssetNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssetNo.Name = "lblAssetNo";
            this.lblAssetNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAssetNo.StyleName = "GridHeader";
            this.lblAssetNo.StylePriority.UseBorders = false;
            this.lblAssetNo.StylePriority.UseFont = false;
            this.lblAssetNo.StylePriority.UsePadding = false;
            this.lblAssetNo.StylePriority.UseTextAlignment = false;
            this.lblAssetNo.Text = "Asset No";
            this.lblAssetNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAssetNo.Weight = 39.874408197652357D;
            // 
            // lblAssetDescription
            // 
            this.lblAssetDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAssetDescription.CanGrow = false;
            this.lblAssetDescription.Dpi = 100F;
            this.lblAssetDescription.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssetDescription.Name = "lblAssetDescription";
            this.lblAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAssetDescription.StyleName = "GridHeader";
            this.lblAssetDescription.StylePriority.UseBorders = false;
            this.lblAssetDescription.StylePriority.UseFont = false;
            this.lblAssetDescription.StylePriority.UsePadding = false;
            this.lblAssetDescription.StylePriority.UseTextAlignment = false;
            this.lblAssetDescription.Text = "Asset Description";
            this.lblAssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAssetDescription.Weight = 74.764516068473114D;
            // 
            // lblModelNo
            // 
            this.lblModelNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblModelNo.CanGrow = false;
            this.lblModelNo.Dpi = 100F;
            this.lblModelNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModelNo.Name = "lblModelNo";
            this.lblModelNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblModelNo.StyleName = "GridHeader";
            this.lblModelNo.StylePriority.UseBorders = false;
            this.lblModelNo.StylePriority.UseFont = false;
            this.lblModelNo.StylePriority.UsePadding = false;
            this.lblModelNo.StylePriority.UseTextAlignment = false;
            this.lblModelNo.Text = "Model No";
            this.lblModelNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblModelNo.Weight = 49.843011654885927D;
            // 
            // lblSerialNo
            // 
            this.lblSerialNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSerialNo.CanGrow = false;
            this.lblSerialNo.Dpi = 100F;
            this.lblSerialNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSerialNo.Name = "lblSerialNo";
            this.lblSerialNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblSerialNo.StyleName = "GridHeader";
            this.lblSerialNo.StylePriority.UseBorders = false;
            this.lblSerialNo.StylePriority.UseFont = false;
            this.lblSerialNo.StylePriority.UsePadding = false;
            this.lblSerialNo.StylePriority.UseTextAlignment = false;
            this.lblSerialNo.Text = "Serial No.";
            this.lblSerialNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSerialNo.Weight = 49.843011621893631D;
            // 
            // lblAssetCatCode
            // 
            this.lblAssetCatCode.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAssetCatCode.CanGrow = false;
            this.lblAssetCatCode.Dpi = 100F;
            this.lblAssetCatCode.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssetCatCode.Name = "lblAssetCatCode";
            this.lblAssetCatCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAssetCatCode.StyleName = "GridHeader";
            this.lblAssetCatCode.StylePriority.UseBorders = false;
            this.lblAssetCatCode.StylePriority.UseFont = false;
            this.lblAssetCatCode.StylePriority.UsePadding = false;
            this.lblAssetCatCode.StylePriority.UseTextAlignment = false;
            this.lblAssetCatCode.Text = "Asset Cat.";
            this.lblAssetCatCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAssetCatCode.Weight = 39.874409455503596D;
            // 
            // lblL2Code
            // 
            this.lblL2Code.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblL2Code.CanGrow = false;
            this.lblL2Code.Dpi = 100F;
            this.lblL2Code.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL2Code.Name = "lblL2Code";
            this.lblL2Code.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblL2Code.StyleName = "GridHeader";
            this.lblL2Code.StylePriority.UseBorders = false;
            this.lblL2Code.StylePriority.UseFont = false;
            this.lblL2Code.StylePriority.UsePadding = false;
            this.lblL2Code.StylePriority.UseTextAlignment = false;
            this.lblL2Code.Text = "City Code";
            this.lblL2Code.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblL2Code.Weight = 29.90580727427513D;
            // 
            // lblL3
            // 
            this.lblL3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblL3.CanGrow = false;
            this.lblL3.Dpi = 100F;
            this.lblL3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL3.Multiline = true;
            this.lblL3.Name = "lblL3";
            this.lblL3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblL3.StyleName = "GridHeader";
            this.lblL3.StylePriority.UseBorders = false;
            this.lblL3.StylePriority.UseFont = false;
            this.lblL3.StylePriority.UsePadding = false;
            this.lblL3.StylePriority.UseTextAlignment = false;
            this.lblL3.Text = "Area Code\r\nArea Desc";
            this.lblL3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblL3.Weight = 74.764516725310969D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.xrTableCell7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.CanGrow = false;
            this.xrTableCell7.Dpi = 100F;
            this.xrTableCell7.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Multiline = true;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTableCell7.StylePriority.UseBackColor = false;
            this.xrTableCell7.StylePriority.UseBorderColor = false;
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "Zone Code\r\nZone Desc";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 74.764516725310969D;
            // 
            // lbll5
            // 
            this.lbll5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lbll5.CanGrow = false;
            this.lbll5.Dpi = 100F;
            this.lbll5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbll5.Multiline = true;
            this.lbll5.Name = "lbll5";
            this.lbll5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lbll5.StyleName = "GridHeader";
            this.lbll5.StylePriority.UseBorders = false;
            this.lbll5.StylePriority.UseFont = false;
            this.lbll5.StylePriority.UsePadding = false;
            this.lbll5.StylePriority.UseTextAlignment = false;
            this.lbll5.Text = "Building No\r\nBuilding Desc";
            this.lbll5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbll5.Weight = 74.764515324481124D;
            // 
            // lbllocation
            // 
            this.lbllocation.CanGrow = false;
            this.lbllocation.Dpi = 100F;
            this.lbllocation.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllocation.Multiline = true;
            this.lbllocation.Name = "lbllocation";
            this.lbllocation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lbllocation.StyleName = "GridHeader";
            this.lbllocation.StylePriority.UseFont = false;
            this.lbllocation.StylePriority.UsePadding = false;
            this.lbllocation.StylePriority.UseTextAlignment = false;
            this.lbllocation.Text = "Location No\r\nLocation Desc";
            this.lbllocation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbllocation.Weight = 74.764520089528943D;
            // 
            // lang
            // 
            this.lang.Description = "lang";
            this.lang.Name = "lang";
            this.lang.Type = typeof(bool);
            this.lang.ValueInfo = "True";
            // 
            // header
            // 
            this.header.Description = "header";
            this.header.Name = "header";
            // 
            // l1Id
            // 
            this.l1Id.Name = "l1Id";
            this.l1Id.Type = typeof(int);
            this.l1Id.ValueInfo = "0";
            // 
            // accountId
            // 
            this.accountId.Description = "accountId";
            this.accountId.Name = "accountId";
            this.accountId.Type = typeof(int);
            this.accountId.ValueInfo = "0";
            // 
            // reportBy
            // 
            this.reportBy.Description = "reportBy";
            this.reportBy.Name = "reportBy";
            // 
            // dateRange
            // 
            this.dateRange.Description = "dateRange";
            this.dateRange.Name = "dateRange";
            // 
            // header
            // 
            this.header.Description = "header";
            this.header.Name = "header";
            // 
            // l1Id
            // 
            this.l1Id.Name = "l1Id";
            this.l1Id.Type = typeof(int);
            this.l1Id.ValueInfo = "0";
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "xrTableCell1";
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Dpi = 100F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "xrTableCell2";
            this.xrTableCell2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.Weight = 1D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Dpi = 100F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "xrTableCell4";
            this.xrTableCell4.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Dpi = 100F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "xrTableCell5";
            this.xrTableCell5.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Dpi = 100F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 1D;
            // 
            // pageFooterBand1
            // 
            this.pageFooterBand1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPageInfo2});
            this.pageFooterBand1.Dpi = 100F;
            this.pageFooterBand1.HeightF = 113.375F;
            this.pageFooterBand1.Name = "pageFooterBand1";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 80.37497F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(313F, 23F);
            this.xrPageInfo1.StyleName = "PageInfo";
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(752F, 80.37497F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(313F, 23F);
            this.xrPageInfo2.StyleName = "PageInfo";
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BorderColor = System.Drawing.Color.Black;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1F;
            this.Title.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.Maroon;
            this.Title.Name = "Title";
            // 
            // FieldCaption
            // 
            this.FieldCaption.BackColor = System.Drawing.Color.Transparent;
            this.FieldCaption.BorderColor = System.Drawing.Color.Black;
            this.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.FieldCaption.BorderWidth = 1F;
            this.FieldCaption.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FieldCaption.ForeColor = System.Drawing.Color.Maroon;
            this.FieldCaption.Name = "FieldCaption";
            // 
            // PageInfo
            // 
            this.PageInfo.BackColor = System.Drawing.Color.Transparent;
            this.PageInfo.BorderColor = System.Drawing.Color.Black;
            this.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.PageInfo.BorderWidth = 1F;
            this.PageInfo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PageInfo.ForeColor = System.Drawing.Color.Black;
            this.PageInfo.Name = "PageInfo";
            // 
            // DataField
            // 
            this.DataField.BackColor = System.Drawing.Color.Transparent;
            this.DataField.BorderColor = System.Drawing.Color.Black;
            this.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DataField.BorderWidth = 1F;
            this.DataField.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataField.ForeColor = System.Drawing.Color.Black;
            this.DataField.Name = "DataField";
            this.DataField.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            // 
            // EvenStyle
            // 
            this.EvenStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(243)))), ((int)(((byte)(250)))));
            this.EvenStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.EvenStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.EvenStyle.Name = "EvenStyle";
            // 
            // OddStyle
            // 
            this.OddStyle.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(209)))), ((int)(((byte)(228)))));
            this.OddStyle.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.OddStyle.Name = "OddStyle";
            // 
            // GridHeader
            // 
            this.GridHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.GridHeader.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.GridHeader.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.GridHeader.BorderWidth = 1F;
            this.GridHeader.Name = "GridHeader";
            // 
            // l2Id
            // 
            this.l2Id.Description = "l2Id";
            this.l2Id.Name = "l2Id";
            // 
            // reportHeaderBand1
            // 
            this.reportHeaderBand1.Dpi = 100F;
            this.reportHeaderBand1.HeightF = 0F;
            this.reportHeaderBand1.Name = "reportHeaderBand1";
            // 
            // isCentral
            // 
            this.isCentral.Description = "isCentral";
            this.isCentral.Name = "isCentral";
            this.isCentral.Type = typeof(bool);
            this.isCentral.ValueInfo = "False";
            // 
            // employeeId
            // 
            this.employeeId.Description = "employeeId";
            this.employeeId.Name = "employeeId";
            this.employeeId.Type = typeof(int);
            this.employeeId.ValueInfo = "0";
            // 
            // l3Id
            // 
            this.l3Id.Description = "l3Id";
            this.l3Id.Name = "l3Id";
            // 
            // l4Id
            // 
            this.l4Id.Description = "l4Id";
            this.l4Id.Name = "l4Id";
            // 
            // l5Id
            // 
            this.l5Id.Description = "l5Id";
            this.l5Id.Name = "l5Id";
            // 
            // locationId
            // 
            this.locationId.Description = "locationId";
            this.locationId.Name = "locationId";
            this.locationId.Type = typeof(int);
            this.locationId.ValueInfo = "0";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.Constructor = objectConstructorInfo1;
            this.objectDataSource1.DataMember = "GetAssetList";
            this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.AssetReportService);
            this.objectDataSource1.Name = "objectDataSource1";
            parameter1.Name = "accountId";
            parameter1.Type = typeof(DevExpress.DataAccess.Expression);
            parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.accountId]", typeof(int));
            parameter2.Name = "isCentral";
            parameter2.Type = typeof(DevExpress.DataAccess.Expression);
            parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.isCentral]", typeof(bool));
            parameter3.Name = "lang";
            parameter3.Type = typeof(DevExpress.DataAccess.Expression);
            parameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.lang]", typeof(bool));
            parameter4.Name = "employeeId";
            parameter4.Type = typeof(DevExpress.DataAccess.Expression);
            parameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.employeeId]", typeof(int));
            parameter5.Name = "l2Id";
            parameter5.Type = typeof(DevExpress.DataAccess.Expression);
            parameter5.Value = new DevExpress.DataAccess.Expression("[Parameters.l2Id]", typeof(string));
            parameter6.Name = "l3Id";
            parameter6.Type = typeof(DevExpress.DataAccess.Expression);
            parameter6.Value = new DevExpress.DataAccess.Expression("[Parameters.l3Id]", typeof(string));
            parameter7.Name = "l4Id";
            parameter7.Type = typeof(DevExpress.DataAccess.Expression);
            parameter7.Value = new DevExpress.DataAccess.Expression("[Parameters.l4Id]", typeof(string));
            parameter8.Name = "l5Id";
            parameter8.Type = typeof(DevExpress.DataAccess.Expression);
            parameter8.Value = new DevExpress.DataAccess.Expression("[Parameters.l5Id]", typeof(string));
            parameter9.Name = "locationId";
            parameter9.Type = typeof(DevExpress.DataAccess.Expression);
            parameter9.Value = new DevExpress.DataAccess.Expression("[Parameters.locationId]", typeof(int));
            this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2,
            parameter3,
            parameter4,
            parameter5,
            parameter6,
            parameter7,
            parameter8,
            parameter9});
            // 
            // reportBy
            // 
            this.reportBy.Description = "reportBy";
            this.reportBy.Name = "reportBy";
            // 
            // dateRange
            // 
            this.dateRange.Description = "dateRange";
            this.dateRange.Name = "dateRange";
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 100F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(3.973643E-05F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("header", this.header));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l1Id", this.l1Id));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("reportBy", this.reportBy));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("dateRange", this.dateRange));
            this.xrSubreport1.ReportSource = new HeaderSubReport();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(1063F, 86F);
            this.xrSubreport1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrSubreport1_BeforePrint);
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 100F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport2.ReportSource = new FooterSubReport();
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1063F, 80.37497F);
            // 
            // AssetList
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.pageHeaderBand1,
            this.pageFooterBand1,
            this.reportHeaderBand1});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(10, 17, 0, 0);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.l2Id,
            this.lang,
            this.accountId,
            this.isCentral,
            this.employeeId,
            this.l3Id,
            this.l4Id,
            this.l5Id,
            this.locationId,
            this.header,
            this.l1Id,
            this.reportBy,
            this.dateRange});
            this.RequestParameters = false;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.FieldCaption,
            this.PageInfo,
            this.DataField,
            this.EvenStyle,
            this.OddStyle,
            this.GridHeader});
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        //HeaderSubReport  subreport = (HeaderSubReport)(sender as XRSubreport).ReportSource;
        //subreport.Parameters["AccountId"].Value = this.Parameters["accountId"].Value;
    }

    private void pageHeaderBand1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        //HeaderSubReport subreport = (HeaderSubReport)(sender as XRSubreport).ReportSource;
        //subreport.Parameters["AccountId"].Value = this.Parameters["accountId"].Value;
    }
}
