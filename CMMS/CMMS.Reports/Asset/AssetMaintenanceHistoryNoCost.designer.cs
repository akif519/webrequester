﻿namespace CMMS.Reports.Asset
{
    partial class AssetMaintenanceHistoryNoCost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter6 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.SrNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.AssetNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.assetDescValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.ModelNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.SerailNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.assetCatCodeValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.L2CodeValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.L3Value = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.accountId = new DevExpress.XtraReports.Parameters.Parameter();
            this.lang = new DevExpress.XtraReports.Parameters.Parameter();
            this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.header = new DevExpress.XtraReports.Parameters.Parameter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblWorkorderNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblProblemDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblActionTaken = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCityCodeTable = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblAssetDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblWorkTypeDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaintDivisionCode = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDateReceived = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.l2Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.isCentral = new DevExpress.XtraReports.Parameters.Parameter();
            this.assetId = new DevExpress.XtraReports.Parameters.Parameter();
            this.employeeId = new DevExpress.XtraReports.Parameters.Parameter();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 20.83333F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(1059F, 20.83333F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.SrNoValue,
            this.AssetNoValue,
            this.assetDescValue,
            this.ModelNoValue,
            this.SerailNoValue,
            this.assetCatCodeValue,
            this.L2CodeValue,
            this.L3Value});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // SrNoValue
            // 
            this.SrNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SrNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkorderNo")});
            this.SrNoValue.Dpi = 100F;
            this.SrNoValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.SrNoValue.Multiline = true;
            this.SrNoValue.Name = "SrNoValue";
            this.SrNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.SrNoValue.StylePriority.UseBorders = false;
            this.SrNoValue.StylePriority.UseFont = false;
            this.SrNoValue.StylePriority.UsePadding = false;
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            this.SrNoValue.Summary = xrSummary1;
            this.SrNoValue.Weight = 36.023716686142826D;
            // 
            // AssetNoValue
            // 
            this.AssetNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.AssetNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ProblemDescription")});
            this.AssetNoValue.Dpi = 100F;
            this.AssetNoValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.AssetNoValue.Multiline = true;
            this.AssetNoValue.Name = "AssetNoValue";
            this.AssetNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.AssetNoValue.StylePriority.UseBorders = false;
            this.AssetNoValue.StylePriority.UseFont = false;
            this.AssetNoValue.StylePriority.UsePadding = false;
            this.AssetNoValue.Weight = 79.761970377450908D;
            // 
            // assetDescValue
            // 
            this.assetDescValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.assetDescValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ActionTaken")});
            this.assetDescValue.Dpi = 100F;
            this.assetDescValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.assetDescValue.Multiline = true;
            this.assetDescValue.Name = "assetDescValue";
            this.assetDescValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.assetDescValue.StylePriority.UseBorders = false;
            this.assetDescValue.StylePriority.UseFont = false;
            this.assetDescValue.StylePriority.UsePadding = false;
            this.assetDescValue.Weight = 39.48071273058514D;
            // 
            // ModelNoValue
            // 
            this.ModelNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.ModelNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LDesc")});
            this.ModelNoValue.Dpi = 100F;
            this.ModelNoValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.ModelNoValue.Multiline = true;
            this.ModelNoValue.Name = "ModelNoValue";
            this.ModelNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.ModelNoValue.StylePriority.UseBorders = false;
            this.ModelNoValue.StylePriority.UseFont = false;
            this.ModelNoValue.StylePriority.UsePadding = false;
            this.ModelNoValue.Weight = 56.243035032850571D;
            // 
            // SerailNoValue
            // 
            this.SerailNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SerailNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetDesc")});
            this.SerailNoValue.Dpi = 100F;
            this.SerailNoValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.SerailNoValue.Multiline = true;
            this.SerailNoValue.Name = "SerailNoValue";
            this.SerailNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.SerailNoValue.StylePriority.UseBorders = false;
            this.SerailNoValue.StylePriority.UseFont = false;
            this.SerailNoValue.StylePriority.UsePadding = false;
            this.SerailNoValue.Weight = 56.020522499693435D;
            // 
            // assetCatCodeValue
            // 
            this.assetCatCodeValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.assetCatCodeValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkDesc")});
            this.assetCatCodeValue.Dpi = 100F;
            this.assetCatCodeValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.assetCatCodeValue.Multiline = true;
            this.assetCatCodeValue.Name = "assetCatCodeValue";
            this.assetCatCodeValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.assetCatCodeValue.StylePriority.UseBorders = false;
            this.assetCatCodeValue.StylePriority.UseFont = false;
            this.assetCatCodeValue.StylePriority.UsePadding = false;
            this.assetCatCodeValue.Weight = 53.581070873962069D;
            // 
            // L2CodeValue
            // 
            this.L2CodeValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.L2CodeValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaintDesc")});
            this.L2CodeValue.Dpi = 100F;
            this.L2CodeValue.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.L2CodeValue.Multiline = true;
            this.L2CodeValue.Name = "L2CodeValue";
            this.L2CodeValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.L2CodeValue.StylePriority.UseBorders = false;
            this.L2CodeValue.StylePriority.UseFont = false;
            this.L2CodeValue.StylePriority.UsePadding = false;
            this.L2CodeValue.Weight = 49.573268853698892D;
            // 
            // L3Value
            // 
            this.L3Value.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.L3Value.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DateRange")});
            this.L3Value.Dpi = 100F;
            this.L3Value.Font = new System.Drawing.Font("Calibri", 8.25F);
            this.L3Value.Multiline = true;
            this.L3Value.Name = "L3Value";
            this.L3Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.L3Value.StylePriority.UseBorders = false;
            this.L3Value.StylePriority.UseFont = false;
            this.L3Value.StylePriority.UsePadding = false;
            this.L3Value.Weight = 42.6273131311817D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 2.291679F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 100F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l1Id", this.l1Id));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("header", this.header));
            this.xrSubreport1.ReportSource = new HeaderSubReport();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(1059F, 76.66667F);
            // 
            // accountId
            // 
            this.accountId.Description = "accountId";
            this.accountId.Name = "accountId";
            this.accountId.Type = typeof(int);
            this.accountId.ValueInfo = "0";
            // 
            // lang
            // 
            this.lang.Description = "lang";
            this.lang.Name = "lang";
            this.lang.Type = typeof(bool);
            this.lang.ValueInfo = "True";
            // 
            // l1Id
            // 
            this.l1Id.Name = "l1Id";
            this.l1Id.Type = typeof(int);
            this.l1Id.ValueInfo = "0";
            // 
            // header
            // 
            this.header.Name = "header";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrSubreport1});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 139.3749F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 78.83331F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(1059F, 60.5416F);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblWorkorderNo,
            this.lblProblemDesc,
            this.lblActionTaken,
            this.lblCityCodeTable,
            this.lblAssetDesc,
            this.lblWorkTypeDesc,
            this.lblMaintDivisionCode,
            this.lblDateReceived});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.StylePriority.UseFont = false;
            this.xrTableRow3.Weight = 1D;
            // 
            // lblWorkorderNo
            // 
            this.lblWorkorderNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblWorkorderNo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblWorkorderNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblWorkorderNo.CanGrow = false;
            this.lblWorkorderNo.Dpi = 100F;
            this.lblWorkorderNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblWorkorderNo.Name = "lblWorkorderNo";
            this.lblWorkorderNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblWorkorderNo.StylePriority.UseBackColor = false;
            this.lblWorkorderNo.StylePriority.UseBorderColor = false;
            this.lblWorkorderNo.StylePriority.UseBorders = false;
            this.lblWorkorderNo.StylePriority.UseFont = false;
            this.lblWorkorderNo.StylePriority.UsePadding = false;
            this.lblWorkorderNo.StylePriority.UseTextAlignment = false;
            this.lblWorkorderNo.Text = "Workorder No";
            this.lblWorkorderNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblWorkorderNo.Weight = 29.39612258687384D;
            // 
            // lblProblemDesc
            // 
            this.lblProblemDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblProblemDesc.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblProblemDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblProblemDesc.CanGrow = false;
            this.lblProblemDesc.Dpi = 100F;
            this.lblProblemDesc.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblProblemDesc.Name = "lblProblemDesc";
            this.lblProblemDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblProblemDesc.StylePriority.UseBackColor = false;
            this.lblProblemDesc.StylePriority.UseBorderColor = false;
            this.lblProblemDesc.StylePriority.UseBorders = false;
            this.lblProblemDesc.StylePriority.UseFont = false;
            this.lblProblemDesc.StylePriority.UsePadding = false;
            this.lblProblemDesc.StylePriority.UseTextAlignment = false;
            this.lblProblemDesc.Text = "Problem Desc";
            this.lblProblemDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblProblemDesc.Weight = 65.087459827684938D;
            // 
            // lblActionTaken
            // 
            this.lblActionTaken.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblActionTaken.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblActionTaken.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblActionTaken.CanGrow = false;
            this.lblActionTaken.Dpi = 100F;
            this.lblActionTaken.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblActionTaken.Name = "lblActionTaken";
            this.lblActionTaken.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblActionTaken.StylePriority.UseBackColor = false;
            this.lblActionTaken.StylePriority.UseBorderColor = false;
            this.lblActionTaken.StylePriority.UseBorders = false;
            this.lblActionTaken.StylePriority.UseFont = false;
            this.lblActionTaken.StylePriority.UsePadding = false;
            this.lblActionTaken.StylePriority.UseTextAlignment = false;
            this.lblActionTaken.Text = "Action Taken";
            this.lblActionTaken.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblActionTaken.Weight = 32.2170799244186D;
            // 
            // lblCityCodeTable
            // 
            this.lblCityCodeTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblCityCodeTable.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblCityCodeTable.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCityCodeTable.CanGrow = false;
            this.lblCityCodeTable.Dpi = 100F;
            this.lblCityCodeTable.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblCityCodeTable.Multiline = true;
            this.lblCityCodeTable.Name = "lblCityCodeTable";
            this.lblCityCodeTable.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblCityCodeTable.StylePriority.UseBackColor = false;
            this.lblCityCodeTable.StylePriority.UseBorderColor = false;
            this.lblCityCodeTable.StylePriority.UseBorders = false;
            this.lblCityCodeTable.StylePriority.UseFont = false;
            this.lblCityCodeTable.StylePriority.UsePadding = false;
            this.lblCityCodeTable.StylePriority.UseTextAlignment = false;
            this.lblCityCodeTable.Text = "City Code\r\nZone Code\r\nBuilding Code";
            this.lblCityCodeTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCityCodeTable.Weight = 45.895500848136543D;
            // 
            // lblAssetDesc
            // 
            this.lblAssetDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblAssetDesc.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblAssetDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAssetDesc.CanGrow = false;
            this.lblAssetDesc.Dpi = 100F;
            this.lblAssetDesc.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblAssetDesc.Multiline = true;
            this.lblAssetDesc.Name = "lblAssetDesc";
            this.lblAssetDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAssetDesc.StylePriority.UseBackColor = false;
            this.lblAssetDesc.StylePriority.UseBorderColor = false;
            this.lblAssetDesc.StylePriority.UseBorders = false;
            this.lblAssetDesc.StylePriority.UseFont = false;
            this.lblAssetDesc.StylePriority.UsePadding = false;
            this.lblAssetDesc.StylePriority.UseTextAlignment = false;
            this.lblAssetDesc.Text = "Asset No\r\nAsset Desc\r\nLocation No";
            this.lblAssetDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAssetDesc.Weight = 45.713955504238342D;
            // 
            // lblWorkTypeDesc
            // 
            this.lblWorkTypeDesc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblWorkTypeDesc.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblWorkTypeDesc.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblWorkTypeDesc.CanGrow = false;
            this.lblWorkTypeDesc.Dpi = 100F;
            this.lblWorkTypeDesc.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblWorkTypeDesc.Multiline = true;
            this.lblWorkTypeDesc.Name = "lblWorkTypeDesc";
            this.lblWorkTypeDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblWorkTypeDesc.StylePriority.UseBackColor = false;
            this.lblWorkTypeDesc.StylePriority.UseBorderColor = false;
            this.lblWorkTypeDesc.StylePriority.UseBorders = false;
            this.lblWorkTypeDesc.StylePriority.UseFont = false;
            this.lblWorkTypeDesc.StylePriority.UsePadding = false;
            this.lblWorkTypeDesc.StylePriority.UseTextAlignment = false;
            this.lblWorkTypeDesc.Text = "Job Type\r\nJob Status\r\nJob Priority\r\nJob Trade";
            this.lblWorkTypeDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblWorkTypeDesc.Weight = 43.723295481852745D;
            // 
            // lblMaintDivisionCode
            // 
            this.lblMaintDivisionCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblMaintDivisionCode.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblMaintDivisionCode.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaintDivisionCode.CanGrow = false;
            this.lblMaintDivisionCode.Dpi = 100F;
            this.lblMaintDivisionCode.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMaintDivisionCode.Multiline = true;
            this.lblMaintDivisionCode.Name = "lblMaintDivisionCode";
            this.lblMaintDivisionCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblMaintDivisionCode.StylePriority.UseBackColor = false;
            this.lblMaintDivisionCode.StylePriority.UseBorderColor = false;
            this.lblMaintDivisionCode.StylePriority.UseBorders = false;
            this.lblMaintDivisionCode.StylePriority.UseFont = false;
            this.lblMaintDivisionCode.StylePriority.UsePadding = false;
            this.lblMaintDivisionCode.StylePriority.UseTextAlignment = false;
            this.lblMaintDivisionCode.Text = "Maint. Div\r\nMaint. Dept\r\nMaint. Sub Dept";
            this.lblMaintDivisionCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMaintDivisionCode.Weight = 40.452839625054317D;
            // 
            // lblDateReceived
            // 
            this.lblDateReceived.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblDateReceived.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblDateReceived.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDateReceived.CanGrow = false;
            this.lblDateReceived.Dpi = 100F;
            this.lblDateReceived.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblDateReceived.Multiline = true;
            this.lblDateReceived.Name = "lblDateReceived";
            this.lblDateReceived.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblDateReceived.StylePriority.UseBackColor = false;
            this.lblDateReceived.StylePriority.UseBorderColor = false;
            this.lblDateReceived.StylePriority.UseBorders = false;
            this.lblDateReceived.StylePriority.UseFont = false;
            this.lblDateReceived.StylePriority.UsePadding = false;
            this.lblDateReceived.StylePriority.UseTextAlignment = false;
            this.lblDateReceived.Text = "Date Received\r\nEnd Date";
            this.lblDateReceived.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDateReceived.Weight = 34.7848001862006D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPageInfo2});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 87.5F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 100F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport2.ReportSource = new FooterSubReport();
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(1059F, 53.75004F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 54.5F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(313F, 23F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(724.3399F, 54.50001F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(344.6601F, 23F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // l2Id
            // 
            this.l2Id.Description = "l2Id";
            this.l2Id.Name = "l2Id";
            // 
            // isCentral
            // 
            this.isCentral.Description = "isCentral";
            this.isCentral.Name = "isCentral";
            this.isCentral.Type = typeof(bool);
            this.isCentral.ValueInfo = "False";
            // 
            // assetId
            // 
            this.assetId.Description = "assetId";
            this.assetId.Name = "assetId";
            this.assetId.Type = typeof(int);
            this.assetId.ValueInfo = "0";
            // 
            // employeeId
            // 
            this.employeeId.Description = "employeeId";
            this.employeeId.Name = "employeeId";
            this.employeeId.Type = typeof(int);
            this.employeeId.ValueInfo = "0";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.Constructor = objectConstructorInfo1;
            this.objectDataSource1.DataMember = "GetAssetMaintenanceHistoryNoCost";
            this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.AssetReportService);
            this.objectDataSource1.Name = "objectDataSource1";
            parameter1.Name = "accountId";
            parameter1.Type = typeof(DevExpress.DataAccess.Expression);
            parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.accountId]", typeof(int));
            parameter2.Name = "isCentral";
            parameter2.Type = typeof(DevExpress.DataAccess.Expression);
            parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.isCentral]", typeof(bool));
            parameter3.Name = "lang";
            parameter3.Type = typeof(DevExpress.DataAccess.Expression);
            parameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.lang]", typeof(bool));
            parameter4.Name = "employeeId";
            parameter4.Type = typeof(DevExpress.DataAccess.Expression);
            parameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.employeeId]", typeof(int));
            parameter5.Name = "l2Id";
            parameter5.Type = typeof(DevExpress.DataAccess.Expression);
            parameter5.Value = new DevExpress.DataAccess.Expression("[Parameters.l2Id]", typeof(string));
            parameter6.Name = "assetId";
            parameter6.Type = typeof(DevExpress.DataAccess.Expression);
            parameter6.Value = new DevExpress.DataAccess.Expression("[Parameters.assetId]", typeof(int));
            this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2,
            parameter3,
            parameter4,
            parameter5,
            parameter6});
            // 
            // AssetMaintenanceHistoryNoCost
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(8, 0, 0, 2);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.l2Id,
            this.isCentral,
            this.assetId,
            this.employeeId,
            this.lang,
            this.accountId,
            this.l1Id,
            this.header});
            this.RequestParameters = false;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkorderNo;
        private DevExpress.XtraReports.UI.XRTableCell lblProblemDesc;
        private DevExpress.XtraReports.UI.XRTableCell lblActionTaken;
        private DevExpress.XtraReports.UI.XRTableCell lblCityCodeTable;
        private DevExpress.XtraReports.UI.XRTableCell lblAssetDesc;
        private DevExpress.XtraReports.UI.XRTableCell lblWorkTypeDesc;
        private DevExpress.XtraReports.UI.XRTableCell lblMaintDivisionCode;
        private DevExpress.XtraReports.UI.XRTableCell lblDateReceived;
        private DevExpress.XtraReports.Parameters.Parameter l2Id;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell SrNoValue;
        private DevExpress.XtraReports.UI.XRTableCell AssetNoValue;
        private DevExpress.XtraReports.UI.XRTableCell assetDescValue;
        private DevExpress.XtraReports.UI.XRTableCell ModelNoValue;
        private DevExpress.XtraReports.UI.XRTableCell SerailNoValue;
        private DevExpress.XtraReports.UI.XRTableCell assetCatCodeValue;
        private DevExpress.XtraReports.UI.XRTableCell L2CodeValue;
        private DevExpress.XtraReports.UI.XRTableCell L3Value;
        private DevExpress.XtraReports.Parameters.Parameter isCentral;
        private DevExpress.XtraReports.Parameters.Parameter assetId;
        private DevExpress.XtraReports.Parameters.Parameter employeeId;
        private DevExpress.XtraReports.Parameters.Parameter lang;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.Parameters.Parameter accountId;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.XtraReports.Parameters.Parameter l1Id;
        private DevExpress.XtraReports.Parameters.Parameter header;
    }
}
