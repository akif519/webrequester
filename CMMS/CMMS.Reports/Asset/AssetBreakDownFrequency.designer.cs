﻿
    partial class AssetBreakDownFrequency
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter6 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter7 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter8 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter9 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter10 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter11 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter12 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.SrNoValue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrSubreport1 = new DevExpress.XtraReports.UI.XRSubreport();
            this.accountId = new DevExpress.XtraReports.Parameters.Parameter();
            this.lang = new DevExpress.XtraReports.Parameters.Parameter();
            this.header = new DevExpress.XtraReports.Parameters.Parameter();
            this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblAssetNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDownTime = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrSubreport2 = new DevExpress.XtraReports.UI.XRSubreport();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.l2Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.isCentral = new DevExpress.XtraReports.Parameters.Parameter();
            this.employeeId = new DevExpress.XtraReports.Parameters.Parameter();
            this.l3Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.l4Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.l5Id = new DevExpress.XtraReports.Parameters.Parameter();
            this.locationId = new DevExpress.XtraReports.Parameters.Parameter();
            this.assetCategoryId = new DevExpress.XtraReports.Parameters.Parameter();
            this.dateFrom = new DevExpress.XtraReports.Parameters.Parameter();
            this.dateTo = new DevExpress.XtraReports.Parameters.Parameter();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 21.25002F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Dpi = 100F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable1.SizeF = new System.Drawing.SizeF(432.4919F, 21.25002F);
            this.xrTable1.StylePriority.UseBorderColor = false;
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UsePadding = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.SrNoValue,
            this.xrTableCell9});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // SrNoValue
            // 
            this.SrNoValue.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.SrNoValue.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetNumber")});
            this.SrNoValue.Dpi = 100F;
            this.SrNoValue.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SrNoValue.Multiline = true;
            this.SrNoValue.Name = "SrNoValue";
            this.SrNoValue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.SrNoValue.StylePriority.UseBorders = false;
            this.SrNoValue.StylePriority.UseFont = false;
            this.SrNoValue.StylePriority.UsePadding = false;
            this.SrNoValue.StylePriority.UseTextAlignment = false;
            xrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.RecordNumber;
            this.SrNoValue.Summary = xrSummary1;
            this.SrNoValue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.SrNoValue.Weight = 168.11629602122383D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Total")});
            this.xrTableCell9.Dpi = 100F;
            this.xrTableCell9.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Multiline = true;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.StylePriority.UsePadding = false;
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell9.Weight = 47.450689849571035D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 20F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 20F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 79.16666F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrSubreport1
            // 
            this.xrSubreport1.Dpi = 100F;
            this.xrSubreport1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrSubreport1.Name = "xrSubreport1";
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("header", this.header));
            this.xrSubreport1.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("l1Id", this.l1Id));
            this.xrSubreport1.ReportSource = new HeaderSubReport();
            this.xrSubreport1.SizeF = new System.Drawing.SizeF(819.9999F, 76.66667F);
            // 
            // accountId
            // 
            this.accountId.Description = "accountId";
            this.accountId.Name = "accountId";
            this.accountId.Type = typeof(int);
            this.accountId.ValueInfo = "0";
            // 
            // lang
            // 
            this.lang.Description = "lang";
            this.lang.Name = "lang";
            this.lang.Type = typeof(bool);
            this.lang.ValueInfo = "True";
            // 
            // header
            // 
            this.header.Name = "header";
            // 
            // l1Id
            // 
            this.l1Id.Name = "l1Id";
            this.l1Id.Type = typeof(int);
            this.l1Id.ValueInfo = "0";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PageHeader.Dpi = 100F;
            this.PageHeader.HeightF = 36F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrTable2
            // 
            this.xrTable2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
            this.xrTable2.BorderColor = System.Drawing.Color.LightGray;
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 5, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable2.SizeF = new System.Drawing.SizeF(432.4919F, 36F);
            this.xrTable2.StylePriority.UseBorderColor = false;
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UsePadding = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblAssetNo,
            this.lblDownTime});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.Weight = 1D;
            // 
            // lblAssetNo
            // 
            this.lblAssetNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblAssetNo.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblAssetNo.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblAssetNo.CanGrow = false;
            this.lblAssetNo.Dpi = 100F;
            this.lblAssetNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAssetNo.Name = "lblAssetNo";
            this.lblAssetNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblAssetNo.StylePriority.UseBackColor = false;
            this.lblAssetNo.StylePriority.UseBorderColor = false;
            this.lblAssetNo.StylePriority.UseBorders = false;
            this.lblAssetNo.StylePriority.UseFont = false;
            this.lblAssetNo.StylePriority.UsePadding = false;
            this.lblAssetNo.StylePriority.UseTextAlignment = false;
            this.lblAssetNo.Text = "Asset No";
            this.lblAssetNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblAssetNo.Weight = 168.11630381659592D;
            // 
            // lblDownTime
            // 
            this.lblDownTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(214)))), ((int)(((byte)(237)))));
            this.lblDownTime.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(190)))), ((int)(((byte)(216)))));
            this.lblDownTime.CanGrow = false;
            this.lblDownTime.Dpi = 100F;
            this.lblDownTime.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownTime.Name = "lblDownTime";
            this.lblDownTime.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblDownTime.StylePriority.UseBackColor = false;
            this.lblDownTime.StylePriority.UseBorderColor = false;
            this.lblDownTime.StylePriority.UseFont = false;
            this.lblDownTime.StylePriority.UsePadding = false;
            this.lblDownTime.StylePriority.UseTextAlignment = false;
            this.lblDownTime.Text = "Total";
            this.lblDownTime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblDownTime.Weight = 47.450676331118785D;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubreport2,
            this.xrPageInfo1,
            this.xrPageInfo2});
            this.PageFooter.Dpi = 100F;
            this.PageFooter.HeightF = 87.5F;
            this.PageFooter.Name = "PageFooter";
            // 
            // xrSubreport2
            // 
            this.xrSubreport2.Dpi = 100F;
            this.xrSubreport2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
            this.xrSubreport2.Name = "xrSubreport2";
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("AccountId", this.accountId));
            this.xrSubreport2.ParameterBindings.Add(new DevExpress.XtraReports.UI.ParameterBinding("Lang", this.lang));
            this.xrSubreport2.ReportSource = new FooterSubReport();
            this.xrSubreport2.SizeF = new System.Drawing.SizeF(792.4998F, 53.75004F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 54.5F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(313F, 23F);
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(489.4998F, 53.75004F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(313F, 23F);
            this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // l2Id
            // 
            this.l2Id.Description = "l2Id";
            this.l2Id.Name = "l2Id";
            // 
            // isCentral
            // 
            this.isCentral.Description = "isCentral";
            this.isCentral.Name = "isCentral";
            this.isCentral.Type = typeof(bool);
            this.isCentral.ValueInfo = "False";
            // 
            // employeeId
            // 
            this.employeeId.Description = "employeeId";
            this.employeeId.Name = "employeeId";
            this.employeeId.Type = typeof(int);
            this.employeeId.ValueInfo = "0";
            // 
            // l3Id
            // 
            this.l3Id.Description = "l3Id";
            this.l3Id.Name = "l3Id";
            // 
            // l4Id
            // 
            this.l4Id.Description = "l4Id";
            this.l4Id.Name = "l4Id";
            // 
            // l5Id
            // 
            this.l5Id.Description = "l5Id";
            this.l5Id.Name = "l5Id";
            // 
            // locationId
            // 
            this.locationId.Description = "locationId";
            this.locationId.Name = "locationId";
            this.locationId.Type = typeof(int);
            this.locationId.ValueInfo = "0";
            // 
            // assetCategoryId
            // 
            this.assetCategoryId.Description = "assetCategoryId";
            this.assetCategoryId.Name = "assetCategoryId";
            // 
            // dateFrom
            // 
            this.dateFrom.Description = "dateFrom";
            this.dateFrom.Name = "dateFrom";
            this.dateFrom.Type = typeof(System.DateTime);
            // 
            // dateTo
            // 
            this.dateTo.Description = "dateTo";
            this.dateTo.Name = "dateTo";
            this.dateTo.Type = typeof(System.DateTime);
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.Constructor = objectConstructorInfo1;
            this.objectDataSource1.DataMember = "GetAssetBreakdownFrequency";
            this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.AssetReportService);
            this.objectDataSource1.Name = "objectDataSource1";
            parameter1.Name = "accountId";
            parameter1.Type = typeof(DevExpress.DataAccess.Expression);
            parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.accountId]", typeof(int));
            parameter2.Name = "isCentral";
            parameter2.Type = typeof(DevExpress.DataAccess.Expression);
            parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.isCentral]", typeof(bool));
            parameter3.Name = "lang";
            parameter3.Type = typeof(DevExpress.DataAccess.Expression);
            parameter3.Value = new DevExpress.DataAccess.Expression("[Parameters.lang]", typeof(bool));
            parameter4.Name = "employeeId";
            parameter4.Type = typeof(DevExpress.DataAccess.Expression);
            parameter4.Value = new DevExpress.DataAccess.Expression("[Parameters.employeeId]", typeof(int));
            parameter5.Name = "l2Id";
            parameter5.Type = typeof(DevExpress.DataAccess.Expression);
            parameter5.Value = new DevExpress.DataAccess.Expression("[Parameters.l2Id]", typeof(string));
            parameter6.Name = "l3Id";
            parameter6.Type = typeof(DevExpress.DataAccess.Expression);
            parameter6.Value = new DevExpress.DataAccess.Expression("[Parameters.l3Id]", typeof(string));
            parameter7.Name = "l4Id";
            parameter7.Type = typeof(DevExpress.DataAccess.Expression);
            parameter7.Value = new DevExpress.DataAccess.Expression("[Parameters.l4Id]", typeof(string));
            parameter8.Name = "l5Id";
            parameter8.Type = typeof(DevExpress.DataAccess.Expression);
            parameter8.Value = new DevExpress.DataAccess.Expression("[Parameters.l5Id]", typeof(string));
            parameter9.Name = "locationId";
            parameter9.Type = typeof(DevExpress.DataAccess.Expression);
            parameter9.Value = new DevExpress.DataAccess.Expression("[Parameters.locationId]", typeof(int));
            parameter10.Name = "dateFrom";
            parameter10.Type = typeof(DevExpress.DataAccess.Expression);
            parameter10.Value = new DevExpress.DataAccess.Expression("[Parameters.dateFrom]", typeof(System.DateTime));
            parameter11.Name = "dateTo";
            parameter11.Type = typeof(DevExpress.DataAccess.Expression);
            parameter11.Value = new DevExpress.DataAccess.Expression("[Parameters.dateTo]", typeof(System.DateTime));
            parameter12.Name = "assetCategoryId";
            parameter12.Type = typeof(DevExpress.DataAccess.Expression);
            parameter12.Value = new DevExpress.DataAccess.Expression("[Parameters.assetCategoryId]", typeof(string));
            this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2,
            parameter3,
            parameter4,
            parameter5,
            parameter6,
            parameter7,
            parameter8,
            parameter9,
            parameter10,
            parameter11,
            parameter12});
            // 
            // AssetBreakDownFrequency
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
            this.DataSource = this.objectDataSource1;
            this.Margins = new System.Drawing.Printing.Margins(10, 0, 20, 20);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.l2Id,
            this.isCentral,
            this.employeeId,
            this.lang,
            this.accountId,
            this.l3Id,
            this.l4Id,
            this.l5Id,
            this.locationId,
            this.assetCategoryId,
            this.dateFrom,
            this.dateTo,
            this.l1Id,
            this.header});
            this.RequestParameters = false;
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.Parameters.Parameter l2Id;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.Parameters.Parameter isCentral;
        private DevExpress.XtraReports.Parameters.Parameter employeeId;
        private DevExpress.XtraReports.Parameters.Parameter lang;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport1;
        private DevExpress.XtraReports.Parameters.Parameter accountId;
        private DevExpress.XtraReports.UI.XRSubreport xrSubreport2;
        private DevExpress.XtraReports.Parameters.Parameter l3Id;
        private DevExpress.XtraReports.Parameters.Parameter l4Id;
        private DevExpress.XtraReports.Parameters.Parameter l5Id;
        private DevExpress.XtraReports.Parameters.Parameter locationId;
        private DevExpress.XtraReports.Parameters.Parameter assetCategoryId;
        private DevExpress.XtraReports.Parameters.Parameter dateFrom;
        private DevExpress.XtraReports.Parameters.Parameter dateTo;
    private DevExpress.XtraReports.UI.XRTable xrTable2;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
    private DevExpress.XtraReports.UI.XRTableCell lblAssetNo;
    private DevExpress.XtraReports.UI.XRTableCell lblDownTime;
    private DevExpress.XtraReports.UI.XRTable xrTable1;
    private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
    private DevExpress.XtraReports.UI.XRTableCell SrNoValue;
    private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
    private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    private DevExpress.XtraReports.Parameters.Parameter l1Id;
    private DevExpress.XtraReports.Parameters.Parameter header;
}
