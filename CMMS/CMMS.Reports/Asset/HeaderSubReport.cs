﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Globalization;

/// <summary>
/// Summary description for HeaderSubReport
/// </summary>
public class HeaderSubReport : DevExpress.XtraReports.UI.XtraReport
{
    private DevExpress.XtraReports.UI.DetailBand Detail;
    private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private XRLabel xrLabel1;
    private XRLabel xrLabel2;
    private XRLabel xrLabel3;
    private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
    private PageFooterBand pageFooterBand1;
    private ReportHeaderBand reportHeaderBand1;
    private XRControlStyle Title;
    private XRControlStyle FieldCaption;
    private XRControlStyle PageInfo;
    private XRControlStyle DataField;
    private DevExpress.XtraReports.Parameters.Parameter Lang;
    private DevExpress.XtraReports.Parameters.Parameter AccountId;
    private XRLabel xrLabel4;
    private XRPictureBox xrPictureBox1;
    private XRLine xrLine1;
    private XRLabel xrLabel8;
    private XRLabel xrLabel5;
    private XRLabel xrLabel6;
    private XRLabel xrLabel7;
    private XRLabel lblReportHeader;
    private XRLabel xrLabel12;
    private XRLabel xrLabel11;
    private XRLine xrLine2;
    private DevExpress.XtraReports.Parameters.Parameter header;
    private DevExpress.XtraReports.Parameters.Parameter l1Id;
    private DevExpress.XtraReports.Parameters.Parameter reportBy;
    private DevExpress.XtraReports.Parameters.Parameter dateRange;
    private XRLabel xrLabel13;
    private XRLabel xrLabel9;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    public HeaderSubReport()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo objectConstructorInfo1 = new DevExpress.DataAccess.ObjectBinding.ObjectConstructorInfo();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.dateRange = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.lblReportHeader = new DevExpress.XtraReports.UI.XRLabel();
        this.header = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
        this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.reportBy = new DevExpress.XtraReports.Parameters.Parameter();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.AccountId = new DevExpress.XtraReports.Parameters.Parameter();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.pageFooterBand1 = new DevExpress.XtraReports.UI.PageFooterBand();
        this.reportHeaderBand1 = new DevExpress.XtraReports.UI.ReportHeaderBand();
        this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
        this.FieldCaption = new DevExpress.XtraReports.UI.XRControlStyle();
        this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
        this.DataField = new DevExpress.XtraReports.UI.XRControlStyle();
        this.Lang = new DevExpress.XtraReports.Parameters.Parameter();
        this.l1Id = new DevExpress.XtraReports.Parameters.Parameter();
        this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel9,
            this.xrLabel13,
            this.xrLine2,
            this.xrLabel12,
            this.xrLabel11,
            this.lblReportHeader,
            this.xrLabel8,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLine1,
            this.xrPictureBox1,
            this.xrLabel4,
            this.xrLabel1,
            this.xrLabel2,
            this.xrLabel3});
        this.Detail.Dpi = 100F;
        this.Detail.HeightF = 86.00004F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
        // 
        // xrLabel9
        // 
        this.xrLabel9.Dpi = 100F;
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(727.2501F, 39.7083F);
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel9.SizeF = new System.Drawing.SizeF(164.3749F, 18F);
        this.xrLabel9.StyleName = "FieldCaption";
        this.xrLabel9.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel9_BeforePrint);
        // 
        // xrLabel13
        // 
        this.xrLabel13.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.dateRange, "Text", "")});
        this.xrLabel13.Dpi = 100F;
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(516.7549F, 57.70829F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(318.3284F, 18F);
        this.xrLabel13.StyleName = "FieldCaption";
        this.xrLabel13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // dateRange
        // 
        this.dateRange.Description = "dateRange";
        this.dateRange.Name = "dateRange";
        // 
        // xrLine2
        // 
        this.xrLine2.Dpi = 100F;
        this.xrLine2.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
        this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(508.3749F, 5.208302F);
        this.xrLine2.Name = "xrLine2";
        this.xrLine2.SizeF = new System.Drawing.SizeF(2.083374F, 73.50002F);
        // 
        // xrLabel12
        // 
        this.xrLabel12.Dpi = 100F;
        this.xrLabel12.ForeColor = System.Drawing.Color.Black;
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(700.7084F, 39.7083F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(26.54175F, 18F);
        this.xrLabel12.StyleName = "FieldCaption";
        this.xrLabel12.StylePriority.UseForeColor = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "On:";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
        this.xrLabel12.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel12_BeforePrint);
        // 
        // xrLabel11
        // 
        this.xrLabel11.Dpi = 100F;
        this.xrLabel11.ForeColor = System.Drawing.Color.Black;
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(516.7549F, 39.7083F);
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(49.4585F, 18F);
        this.xrLabel11.StyleName = "FieldCaption";
        this.xrLabel11.StylePriority.UseForeColor = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "By:";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.xrLabel11.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel11_BeforePrint);
        // 
        // lblReportHeader
        // 
        this.lblReportHeader.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.header, "Text", "")});
        this.lblReportHeader.Dpi = 100F;
        this.lblReportHeader.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Bold);
        this.lblReportHeader.LocationFloat = new DevExpress.Utils.PointFloat(516.7549F, 0F);
        this.lblReportHeader.Multiline = true;
        this.lblReportHeader.Name = "lblReportHeader";
        this.lblReportHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.lblReportHeader.SizeF = new System.Drawing.SizeF(374.8702F, 39.7083F);
        this.lblReportHeader.StyleName = "Title";
        this.lblReportHeader.StylePriority.UseFont = false;
        this.lblReportHeader.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // header
        // 
        this.header.Description = "header";
        this.header.Name = "header";
        // 
        // xrLabel8
        // 
        this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MODA")});
        this.xrLabel8.Dpi = 100F;
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(363.1667F, 42.7084F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(140.125F, 18F);
        this.xrLabel8.StyleName = "FieldCaption";
        this.xrLabel8.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLabel5
        // 
        this.xrLabel5.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "GDMW")});
        this.xrLabel5.Dpi = 100F;
        this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(363.1667F, 24.70837F);
        this.xrLabel5.Name = "xrLabel5";
        this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel5.SizeF = new System.Drawing.SizeF(140.125F, 18F);
        this.xrLabel5.StyleName = "FieldCaption";
        this.xrLabel5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLabel6
        // 
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaintDept")});
        this.xrLabel6.Dpi = 100F;
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(363.1667F, 6.708336F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(140.125F, 18F);
        this.xrLabel6.StyleName = "FieldCaption";
        this.xrLabel6.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLabel7
        // 
        this.xrLabel7.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "OMName")});
        this.xrLabel7.Dpi = 100F;
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(139.1667F, 60.70833F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(224F, 18F);
        this.xrLabel7.StyleName = "FieldCaption";
        this.xrLabel7.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLine1
        // 
        this.xrLine1.BorderColor = System.Drawing.Color.Maroon;
        this.xrLine1.Dpi = 100F;
        this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(2.384186E-05F, 78.70836F);
        this.xrLine1.Name = "xrLine1";
        this.xrLine1.SizeF = new System.Drawing.SizeF(1088F, 7.291672F);
        this.xrLine1.StylePriority.UseBorderColor = false;
        // 
        // xrPictureBox1
        // 
        this.xrPictureBox1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("ImageUrl", null, "BranchLogo")});
        this.xrPictureBox1.Dpi = 100F;
        this.xrPictureBox1.ImageAlignment = DevExpress.XtraPrinting.ImageAlignment.MiddleCenter;
        this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 0F);
        this.xrPictureBox1.Name = "xrPictureBox1";
        this.xrPictureBox1.SizeF = new System.Drawing.SizeF(116.6667F, 78.70833F);
        this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
        // 
        // xrLabel4
        // 
        this.xrLabel4.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding(this.reportBy, "Text", "")});
        this.xrLabel4.Dpi = 100F;
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(566.2134F, 39.7083F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(134.495F, 18F);
        this.xrLabel4.StyleName = "FieldCaption";
        this.xrLabel4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // reportBy
        // 
        this.reportBy.Description = "reportBy";
        this.reportBy.Name = "reportBy";
        // 
        // xrLabel1
        // 
        this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Country")});
        this.xrLabel1.Dpi = 100F;
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(139.1667F, 6.708336F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(224F, 18F);
        this.xrLabel1.StyleName = "FieldCaption";
        this.xrLabel1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLabel2
        // 
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "District")});
        this.xrLabel2.Dpi = 100F;
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(139.1667F, 24.70833F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(224F, 18F);
        this.xrLabel2.StyleName = "FieldCaption";
        this.xrLabel2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // xrLabel3
        // 
        this.xrLabel3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "BranchName")});
        this.xrLabel3.Dpi = 100F;
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(139.1667F, 42.70833F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(224F, 18F);
        this.xrLabel3.StyleName = "FieldCaption";
        this.xrLabel3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel1_BeforePrint);
        // 
        // AccountId
        // 
        this.AccountId.Description = "Accountid";
        this.AccountId.Name = "AccountId";
        this.AccountId.Type = typeof(int);
        this.AccountId.ValueInfo = "0";
        // 
        // TopMargin
        // 
        this.TopMargin.Dpi = 100F;
        this.TopMargin.HeightF = 1F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.TopMargin.Visible = false;
        // 
        // BottomMargin
        // 
        this.BottomMargin.Dpi = 100F;
        this.BottomMargin.HeightF = 0F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        this.BottomMargin.Visible = false;
        // 
        // pageFooterBand1
        // 
        this.pageFooterBand1.Dpi = 100F;
        this.pageFooterBand1.HeightF = 0F;
        this.pageFooterBand1.Name = "pageFooterBand1";
        // 
        // reportHeaderBand1
        // 
        this.reportHeaderBand1.Dpi = 100F;
        this.reportHeaderBand1.HeightF = 0F;
        this.reportHeaderBand1.Name = "reportHeaderBand1";
        this.reportHeaderBand1.Visible = false;
        // 
        // Title
        // 
        this.Title.BackColor = System.Drawing.Color.Transparent;
        this.Title.BorderColor = System.Drawing.Color.Black;
        this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.Title.BorderWidth = 1F;
        this.Title.Font = new System.Drawing.Font("Times New Roman", 20F, System.Drawing.FontStyle.Bold);
        this.Title.ForeColor = System.Drawing.Color.Maroon;
        this.Title.Name = "Title";
        // 
        // FieldCaption
        // 
        this.FieldCaption.BackColor = System.Drawing.Color.Transparent;
        this.FieldCaption.BorderColor = System.Drawing.Color.Black;
        this.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.FieldCaption.BorderWidth = 1F;
        this.FieldCaption.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.FieldCaption.ForeColor = System.Drawing.Color.Maroon;
        this.FieldCaption.Name = "FieldCaption";
        // 
        // PageInfo
        // 
        this.PageInfo.BackColor = System.Drawing.Color.Transparent;
        this.PageInfo.BorderColor = System.Drawing.Color.Black;
        this.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.PageInfo.BorderWidth = 1F;
        this.PageInfo.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
        this.PageInfo.ForeColor = System.Drawing.Color.Black;
        this.PageInfo.Name = "PageInfo";
        // 
        // DataField
        // 
        this.DataField.BackColor = System.Drawing.Color.Transparent;
        this.DataField.BorderColor = System.Drawing.Color.Black;
        this.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None;
        this.DataField.BorderWidth = 1F;
        this.DataField.Font = new System.Drawing.Font("Times New Roman", 10F);
        this.DataField.ForeColor = System.Drawing.Color.Black;
        this.DataField.Name = "DataField";
        this.DataField.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        // 
        // Lang
        // 
        this.Lang.Description = "Lang";
        this.Lang.Name = "Lang";
        this.Lang.Type = typeof(bool);
        this.Lang.ValueInfo = "False";
        // 
        // l1Id
        // 
        this.l1Id.Name = "l1Id";
        this.l1Id.Type = typeof(int);
        this.l1Id.ValueInfo = "0";
        // 
        // objectDataSource1
        // 
        this.objectDataSource1.Constructor = objectConstructorInfo1;
        this.objectDataSource1.DataMember = "getHeaderFooter";
        this.objectDataSource1.DataSource = typeof(CMMS.Service.ReportService.HeaderFooterService);
        this.objectDataSource1.Name = "objectDataSource1";
        parameter1.Name = "AccountId";
        parameter1.Type = typeof(DevExpress.DataAccess.Expression);
        parameter1.Value = new DevExpress.DataAccess.Expression("[Parameters.AccountId]", typeof(int));
        parameter2.Name = "Lang";
        parameter2.Type = typeof(DevExpress.DataAccess.Expression);
        parameter2.Value = new DevExpress.DataAccess.Expression("[Parameters.Lang]", typeof(bool));
        this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2});
        // 
        // HeaderSubReport
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.pageFooterBand1,
            this.reportHeaderBand1});
        this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1});
        this.DataSource = this.objectDataSource1;
        this.Landscape = true;
        this.Margins = new System.Drawing.Printing.Margins(1, 1, 1, 0);
        this.PageHeight = 850;
        this.PageWidth = 1100;
        this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.Lang,
            this.AccountId,
            this.header,
            this.l1Id,
            this.reportBy,
            this.dateRange});
        this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.FieldCaption,
            this.PageInfo,
            this.DataField});
        this.Version = "16.2";
        ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        CMMS.Service.ReportService.HeaderFooterService._AccountId = (int)this.AccountId.Value;
        //        this.objectDataSource1.Parameters[0] = this.AccountId.Value;
    }

    private void xrLabel1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        if (!Convert.ToBoolean(Lang.Value))
        {
            ((XRLabel)sender).RightToLeft = RightToLeft.Yes;
        }
    }

    private void xrPageInfo1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        if (!Convert.ToBoolean(Lang.Value))
        {
            ((XRPageInfo)sender).RightToLeft = RightToLeft.Yes;
            //XRLabel label = sender as XRLabel;
            //CultureInfo culture = new CultureInfo("ar-SA");
            //xrPageInfo1.Text = String.Format(culture, "Date of creation: {0:dd MMM yyyy hh:mm}", DateTime.Now);
        }
    }

    private void xrLabel9_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        if (!Convert.ToBoolean(Lang.Value))
        {
            ((XRLabel)sender).RightToLeft = RightToLeft.Yes;
            XRLabel label = sender as XRLabel;
            CultureInfo culture = new CultureInfo("ar-SA");
            label.Text = String.Format(culture, "{0:MM/dd/yyyy hh:mm tt}", DateTime.Now);

        }
        else
        {

            XRLabel label = sender as XRLabel;
            CultureInfo culture = new CultureInfo("en-US");
            label.Text = String.Format(culture, "{0:MM/dd/yyyy hh:mm tt}", DateTime.Now);
        }
    }

    private void xrLabel11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        if (!Convert.ToBoolean(Lang.Value))
        {
            ((XRLabel)sender).RightToLeft = RightToLeft.Yes;
            XRLabel label = sender as XRLabel;
            label.Text = "بواسطة";

        }
    }

    private void xrLabel12_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        if (!Convert.ToBoolean(Lang.Value))
        {
            ((XRLabel)sender).RightToLeft = RightToLeft.Yes;
            XRLabel label = sender as XRLabel;
            label.Text = "على";

        }

    }
}
