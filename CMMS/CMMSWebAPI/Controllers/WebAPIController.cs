﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Utilities.Common;
using System.Web.Http.Description;
using CMMS.Utilities.WebAPI;
using CMMS.Service;
using System.Data.SqlClient;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Converters;

namespace CMMSWebAPI.Controllers
{
    public class OrderedContractResolver : DefaultContractResolver
    {
        protected override System.Collections.Generic.IList<JsonProperty> CreateProperties(System.Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(type, memberSerialization).OrderBy(p => p.PropertyName).ToList();
        }
    }

    public class WebAPIController : ApiController
    {
        #region Validate Login
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage ValidateLogin(Employee model, string clientCode)
        {
            //int ClientId = 1;
            //Employee model = new Employee();
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);      

            List<Employee> list = new List<Employee>();
            try
            {
                if (model.UserID != null && model.Password != null)
                {
                    Employee employeeModel = CommonDAL.GetEmployeeByUserIdPassword(clientCode, model.UserID, model.Password, model.IPAddress, model.Latitude, model.Longitude);

                    //Employee employeeModel = Loginservices.GetEmployeeByUserIdPassword(model.UserID, model.Password, model.IPAddress, model.Latitude, model.Longitude);
                    //Employee employeeModel = GetEmployeeDetail(1);

                    if (employeeModel != null && string.IsNullOrEmpty(employeeModel.ExceedConnectionLimitMessage) && string.IsNullOrEmpty(employeeModel.SameUserAlreadyLoginMessage) && string.IsNullOrEmpty(employeeModel.InActiveUserMessage))
                    {
                        list.Add(employeeModel);
                        //return new DataResult(list, 1, ConstatntMessages.LoginSuccess);                                                
                        string JsonString = JsonConvert.SerializeObject(new DataResult(list, 1, ConstatntMessages.LoginSuccess), Formatting.Indented,settings);
                        response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");

                    }
                    else if (employeeModel != null && !string.IsNullOrEmpty(employeeModel.ExceedConnectionLimitMessage))
                    {
                        //list.Add(new EmployeeModel());
                        //return new DataResult(null, 0, employeeModel.ExceedConnectionLimitMessage);
                        string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, employeeModel.ExceedConnectionLimitMessage), Formatting.Indented, settings);
                        response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
                    }
                    else if (employeeModel != null && !string.IsNullOrEmpty(employeeModel.SameUserAlreadyLoginMessage))
                    {
                        //return new DataResult(null, 0, employeeModel.SameUserAlreadyLoginMessage);
                        string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, employeeModel.SameUserAlreadyLoginMessage), Formatting.Indented, settings);
                        response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
                    }
                    else if (employeeModel != null && !string.IsNullOrEmpty(employeeModel.InActiveUserMessage))
                    {
                        //return new DataResult(null, 0, employeeModel.InActiveUserMessage);
                        string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, employeeModel.InActiveUserMessage), Formatting.Indented, settings);
                        response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
                    }
                    else
                    {
                        //return new DataResult(null, 0, ConstatntMessages.IncorrectUsernamePassword);
                        string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, ConstatntMessages.IncorrectUsernamePassword), Formatting.Indented, settings);
                        response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
                    }
                }
                else
                {
                    //return new DataResult(null, 0, ConstatntMessages.IncorrectParameter);
                    string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, ConstatntMessages.IncorrectParameter), Formatting.Indented, settings);
                    response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
                }
            }
            catch (Exception ex)
            {
                //return new DataResult(null, 0, ConstatntMessages.LoginFail);
                string JsonString = JsonConvert.SerializeObject(new DataResult(null, 0, ConstatntMessages.LoginFail), Formatting.Indented, settings);
                response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            }
            return response;       
        }

        #endregion

        #region Logout Functionality
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage Logout(LogoutResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;
            try
            {
                if (model.EmployeeID > 0)
                {
                    EmployeeDetailService obj = new EmployeeDetailService();
                    int LogoutResult = obj.Logout(model, clientCode);
                    if (LogoutResult > 0)
                    {
                         JsonString = JsonConvert.SerializeObject(
                            (new LogoutResult
                            {
                                Success = 1,
                                Message = ConstatntMessages.LogoutSuccessMessage
                            })                            
                            , Formatting.Indented, settings);
                        

                        //return (new LogoutResult
                        //{
                        //    Success = 1,
                        //    Message = ConstatntMessages.LogoutSuccessMessage
                        //});
                    }
                    else
                    {

                         JsonString = JsonConvert.SerializeObject(
                            (new LogoutResult
                            {
                                Success = 0,
                                Message = ConstatntMessages.LogoutFailMessage
                            })
                            , Formatting.Indented, settings);


                        //return (new LogoutResult
                        //{
                        //    Success = 0,
                        //    Message = ConstatntMessages.LogoutFailMessage
                        //});
                    }
                }
                else
                {
                    //return (new LogoutResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});
                    
                     JsonString = JsonConvert.SerializeObject(
                        (new LogoutResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);

                }
            }
            catch (Exception ex)
            {
                //return (new LogoutResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                 JsonString = JsonConvert.SerializeObject(
                       (new LogoutResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;


        }

        #endregion

        #region Change Password
        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage ChangePassword(ChangePasswordModel model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);            
            string JsonString = string.Empty;

            try
            {
                if (model.EmployeeID != 0 && !string.IsNullOrEmpty(model.NewPassword) && !string.IsNullOrEmpty(model.OldPassword))
                {
                    ChangePasswordModel returnChangePasswordObject = WorkRequestService.ChangePassword(model, clientCode);
                    returnChangePasswordObject.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    //return returnChangePasswordObject;

                    JsonString = JsonConvert.SerializeObject(
                            returnChangePasswordObject
                            , Formatting.Indented, settings);
                        

                }
                else
                {
                    //return (new ChangePasswordModel
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});


                    JsonString = JsonConvert.SerializeObject(
                          (new ChangePasswordModel
                            {
                                Success = 0,
                                Message = ConstatntMessages.IncorrectParameter
                            })
                          , Formatting.Indented, settings);
                        
                }
            }
            catch (Exception ex)
            {
                //return (new ChangePasswordModel
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new ChangePasswordModel
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Work Request Master For Basic Info

        /// <summary>
        /// Ws for Master records for Basic Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    //DateTime? currentTimeStamp;
                    //currentTimeStamp = BaseService.ConvertStringToDataTime(model.TimeStamp);

                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestMasterResult objWorkRequestMasterResult = obj.GetWorkRequestMaster(model.TimeStamp, clientCode);

                    if (objWorkRequestMasterResult != null)
                    {
                        objWorkRequestMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestMasterResult.Success = 0;
                        objWorkRequestMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestMasterResult;

                    JsonString = JsonConvert.SerializeObject(
                         objWorkRequestMasterResult
                         , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkRequestMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkRequestMasterResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                         , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});
                
                JsonString = JsonConvert.SerializeObject(
                         (new GetWorkRequestMasterResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.ErrorMessage
                         })
                         , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }
        #endregion

        #region Work Request Master For Asset Info

        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestAssetInfoMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {                    
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestAssetInfoMasterResult objWorkRequestAssetInfoMasterResult = obj.GetWorkRequestAssetInfoMaster(model.TimeStamp, clientCode, Convert.ToInt32(model.EmployeeID));

                    if (objWorkRequestAssetInfoMasterResult != null)
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 0;
                        objWorkRequestAssetInfoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestAssetInfoMasterResult;
                    JsonString = JsonConvert.SerializeObject(
                        objWorkRequestAssetInfoMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new GetWorkRequestAssetInfoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);


                }
            }
            catch (Exception ex)
            {
                //return (new GetWorkRequestAssetInfoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }


        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestAssetInfoMasterAssetTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestAssetInfoMasteAssetTablerResult objWorkRequestAssetInfoMasterResult = obj.GetWorkRequestAssetInfoMasterAssetTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkRequestAssetInfoMasterResult != null)
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 0;
                        objWorkRequestAssetInfoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestAssetInfoMasterResult;
                    JsonString = JsonConvert.SerializeObject(
                        objWorkRequestAssetInfoMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new GetWorkRequestAssetInfoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasteAssetTablerResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);


                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestAssetInfoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasteAssetTablerResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestAssetInfoMasterEmployeesL2Table(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestAssetInfoMasterEmployeesL2Result objWorkRequestAssetInfoMasterResult = obj.GetWorkRequestAssetInfoEmployeesL2Master(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkRequestAssetInfoMasterResult != null)
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 0;
                        objWorkRequestAssetInfoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestAssetInfoMasterResult;
                    JsonString = JsonConvert.SerializeObject(
                        objWorkRequestAssetInfoMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new GetWorkRequestAssetInfoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterEmployeesL2Result
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);


                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestAssetInfoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterEmployeesL2Result
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }



        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetEmployeesLocationMasterTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetEmployeesLocationResult objMasterResult = obj.GetEmployeesLocationMasterTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize, model.EmployeeID);

                    if (objMasterResult != null)
                    {
                        objMasterResult.Success = 1;
                    }
                    else
                    {
                        objMasterResult.Success = 0;
                        objMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }                    
                    JsonString = JsonConvert.SerializeObject(
                        objMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterEmployeesL2Result
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);
                }
            }
            catch (Exception)
            {               
                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterEmployeesL2Result
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }





        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestAssetInfoLocationTableMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestAssetInfoMasterLocationResult objWorkRequestAssetInfoMasterResult = obj.GetWorkRequestAssetInfoLocationTableMaster(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkRequestAssetInfoMasterResult != null)
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 0;
                        objWorkRequestAssetInfoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestAssetInfoMasterResult;
                    JsonString = JsonConvert.SerializeObject(
                        objWorkRequestAssetInfoMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new GetWorkRequestAssetInfoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterLocationResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);


                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestAssetInfoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterLocationResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

            
        /// <summary>
        /// Ws for Master records for Asset Info in WR
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestAssetInfoTranslationTableMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestAssetInfoMasterTranslationTableResult objWorkRequestAssetInfoMasterResult = obj.GetWorkRequestAssetInfoTranslationTableMaster(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkRequestAssetInfoMasterResult != null)
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkRequestAssetInfoMasterResult.Success = 0;
                        objWorkRequestAssetInfoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkRequestAssetInfoMasterResult;
                    JsonString = JsonConvert.SerializeObject(
                        objWorkRequestAssetInfoMasterResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new GetWorkRequestAssetInfoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterTranslationTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.IncorrectParameter
                        })
                        , Formatting.Indented, settings);


                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestAssetInfoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkRequestAssetInfoMasterTranslationTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                        , Formatting.Indented, settings);


            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }



        #endregion

        #region Existing WorkRequest Master IDs For Basic Info

        /// <summary>
        /// WS for Existing Master IDs for Basic Info
        /// </summary>
        /// <returns></returns>        
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkRequestMasterIDs(string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkRequestService obj = new WorkRequestService();
                GetExistingWorkRequestMasterIDsResult objExistingWorkRequestMasterIDsResult = obj.GetExistingWorkRequestMasterIDs(clientCode);

                if (objExistingWorkRequestMasterIDsResult != null)
                {
                    objExistingWorkRequestMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkRequestMasterIDsResult.Success = 0;
                    objExistingWorkRequestMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkRequestMasterIDsResult;
                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkRequestMasterIDsResult
                       , Formatting.Indented, settings);
            }
            catch (Exception)
            {
                //return (new GetExistingWorkRequestMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkRequestMasterIDsResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Existing WorkRequest Master IDs For Asset Info

        /// <summary>
        /// WS for Existing Master IDs for Asset Info
        /// </summary>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkRequestAssetInfoMasterIDs(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkRequestService obj = new WorkRequestService();
                GetExistingWorkRequestAssetInfoMasterIDsResult objExistingWorkRequestMasterIDsResult = obj.GetExistingWorkRequestAssetInfoMasterIDs(Convert.ToInt32(model.EmployeeID), clientCode);

                if (objExistingWorkRequestMasterIDsResult != null)
                {
                    objExistingWorkRequestMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkRequestMasterIDsResult.Success = 0;
                    objExistingWorkRequestMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkRequestMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkRequestMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkRequestAssetInfoMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkRequestAssetInfoMasterIDsResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }
        
        /// <summary>
        /// WS for Existing Master IDs for Asset Info
        /// </summary>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2Table(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkRequestService obj = new WorkRequestService();
                GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult objExistingWorkRequestMasterIDsResult = obj.GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2Table(clientCode, model.PageIndex, model.PageSize);

                if (objExistingWorkRequestMasterIDsResult != null)
                {
                    objExistingWorkRequestMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkRequestMasterIDsResult.Success = 0;
                    objExistingWorkRequestMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkRequestMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkRequestMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkRequestAssetInfoMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }
        
        /// <summary>
        /// WS for Existing Master IDs for Asset Info
        /// </summary>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkRequestAssetInfoMasterIDsLstAssetID(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkRequestService obj = new WorkRequestService();
                GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult objExistingWorkRequestMasterIDsResult = obj.GetExistingWorkRequestAssetInfoMasterIDsLstAssetID(clientCode, model.PageIndex, model.PageSize);

                if (objExistingWorkRequestMasterIDsResult != null)
                {
                    objExistingWorkRequestMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkRequestMasterIDsResult.Success = 0;
                    objExistingWorkRequestMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkRequestMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkRequestMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkRequestAssetInfoMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }
        
        /// <summary>
        /// WS for Existing Master IDs for Asset Info
        /// </summary>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkRequestAssetInfoMasterIDsLstLocationID(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkRequestService obj = new WorkRequestService();
                GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult objExistingWorkRequestMasterIDsResult = obj.GetExistingWorkRequestAssetInfoMasterIDsLstLocationID(clientCode, model.PageIndex, model.PageSize);

                if (objExistingWorkRequestMasterIDsResult != null)
                {
                    objExistingWorkRequestMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkRequestMasterIDsResult.Success = 0;
                    objExistingWorkRequestMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkRequestMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkRequestMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkRequestAssetInfoMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }




        #endregion
        
        #region Work Request List With Assosiated Attachment

        /// <summary>
        /// Work Request List with Attachment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkRequestList(GetWorkRequestListResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {   
                if (model != null)
                {
                    WorkRequestService obj = new WorkRequestService();
                    GetWorkRequestListResult objGetWorkRequestListResult = obj.GetWorkRequestList(model.IsCentral, model.EmployeeID, model.RequestStatusIds, model.PageIndex, model.PageSize, model.RequestNoToRefresh, model.SynchTime, clientCode);
                    if (objGetWorkRequestListResult != null)
                    {
                        objGetWorkRequestListResult.Success = 1;
                    }
                    else
                    {
                        objGetWorkRequestListResult.Success = 0;
                        objGetWorkRequestListResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objGetWorkRequestListResult;

                    JsonString = JsonConvert.SerializeObject(
                       objGetWorkRequestListResult
                       , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkRequestListResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                       (new GetWorkRequestListResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.IncorrectParameter
                       })
                       , Formatting.Indented, settings);
                }
            }
            catch (Exception)
            {
                //return (new GetWorkRequestListResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetWorkRequestListResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                       , Formatting.Indented, settings);
            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion
        
        #region Insert/Update Work Request

        /// <summary>
        /// Insert/Update Work Request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage AddUpdateWorkRequest(Worequest model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model != null && model.L2ID > 0)
                {
                    //string attachmentPath = HttpContext.Current.Server.MapPath("~/AttachmentTest/");
                    //string attachmentPath = AppDomain.CurrentDomain.BaseDirectory;
                    string attachmentPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]);

                    WorkRequestService objWorkRequestService = new WorkRequestService();
                    AddUpdateWorkRequestResult objAddUpdateWorkRequestResult = objWorkRequestService.AddUpdateWorkRequest(model, attachmentPath, clientCode);
                    if (objAddUpdateWorkRequestResult != null)
                    {
                        objAddUpdateWorkRequestResult.Success = 1;
                        objAddUpdateWorkRequestResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    else
                    {
                        objAddUpdateWorkRequestResult.Success = 0;
                        objAddUpdateWorkRequestResult.Message = ConstatntMessages.ErrorMessage;
                        objAddUpdateWorkRequestResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    //return objAddUpdateWorkRequestResult;
                    JsonString = JsonConvert.SerializeObject(
                       objAddUpdateWorkRequestResult
                       , Formatting.Indented, settings);
                }
                else
                {
                    //return (new AddUpdateWorkRequestResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});


                    JsonString = JsonConvert.SerializeObject(
                       (new AddUpdateWorkRequestResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.IncorrectParameter
                       })
                       , Formatting.Indented, settings);

                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new AddUpdateWorkRequestResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); // Foreign Key violation

                            JsonString = JsonConvert.SerializeObject(
                                         (new AddUpdateWorkRequestResult
                                         {
                                             Success = 1,
                                             IsUserActive = 1,
                                             IsFKViolation = 1,
                                             Message = ConstatntMessages.ForeignKeyViolation
                                         })
                                        , Formatting.Indented, settings);

                        }
                        else
                        {
                            JsonString = JsonConvert.SerializeObject(
                                               (new AddUpdateWorkRequestResult
                                               {
                                                   Success = 0,
                                                   Message = ConstatntMessages.ErrorMessage
                                               })
                                               , Formatting.Indented, settings);
                        }
                    }
                }
                else
                {
                    //return (new AddUpdateWorkRequestResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                       (new AddUpdateWorkRequestResult
                                       {
                                           Success = 0,
                                           Message = ConstatntMessages.ErrorMessage
                                       })
                                       , Formatting.Indented, settings);


                }
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Add File Attachment

        /// <summary>
        /// Add File Attachment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage AddFileAttachment(AddAttachmentResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model != null)
                {
                    //string attachmentPath = HttpContext.Current.Server.MapPath("~/AttachmentTest/");
                    string attachmentPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]);
                    //string attachmentPath = AppDomain.CurrentDomain.BaseDirectory;
                    WorkRequestService objWorkRequestService = new WorkRequestService();
                    AddAttachmentResult objAddAttachmentResult = objWorkRequestService.AddFileAttachment(model.ExtassetfileTable, attachmentPath, model.EmployeeID, clientCode);
                    if (objAddAttachmentResult != null)
                    {
                        objAddAttachmentResult.Success = 1;
                        objAddAttachmentResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    else
                    {
                        objAddAttachmentResult.Success = 0;
                        objAddAttachmentResult.Message = ConstatntMessages.ErrorMessage;
                        objAddAttachmentResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    //return objAddAttachmentResult;

                    JsonString = JsonConvert.SerializeObject(
                      objAddAttachmentResult
                      , Formatting.Indented, settings);

                }
                else
                {
                    //return (new AddAttachmentResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                      (new AddAttachmentResult
                      {
                          Success = 0,
                          Message = ConstatntMessages.IncorrectParameter
                      })
                      , Formatting.Indented, settings);

                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new AddAttachmentResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); 

                            // Foreign Key violation
                            JsonString = JsonConvert.SerializeObject(
                              (new AddAttachmentResult
                              {
                                  Success = 1,
                                  IsUserActive = 1,
                                  IsFKViolation = 1,
                                  Message = ConstatntMessages.ForeignKeyViolation
                              })
                              , Formatting.Indented, settings);


                        }
                    }
                }
                else
                {
                    //return (new AddAttachmentResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                              (new AddAttachmentResult
                              {
                                  Success = 0,
                                  Message = ConstatntMessages.ErrorMessage
                              })
                              , Formatting.Indented, settings);

                }
            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        #endregion

        #region Delete File Attachment
               
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage DeleteFileAttachment(DeleteAttachmentResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model != null)
                {
                    //string attachmentPath = AppDomain.CurrentDomain.BaseDirectory;
                    string attachmentPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]);
                    
                    WorkRequestService objWorkRequestService = new WorkRequestService();
                    DeleteAttachmentResult objDeleteAttachmentResult = objWorkRequestService.DeleteFileAttachment(model.ExtassetfileTable, attachmentPath, model.EmployeeID, clientCode);
                    if (objDeleteAttachmentResult != null)
                    {
                        objDeleteAttachmentResult.Success = 1;
                        objDeleteAttachmentResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    else
                    {
                        objDeleteAttachmentResult.Success = 0;
                        objDeleteAttachmentResult.Message = ConstatntMessages.ErrorMessage;
                        objDeleteAttachmentResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    //return objDeleteAttachmentResult;
                    JsonString = JsonConvert.SerializeObject(
                             objDeleteAttachmentResult
                             , Formatting.Indented, settings);
                }
                else
                {
                    //return (new DeleteAttachmentResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                             (new DeleteAttachmentResult
                             {
                                 Success = 0,
                                 Message = ConstatntMessages.IncorrectParameter
                             })
                             , Formatting.Indented, settings);

                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new DeleteAttachmentResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); 
                            
                            // Foreign Key violation
                            JsonString = JsonConvert.SerializeObject(
                            (new DeleteAttachmentResult
                            {
                                Success = 1,
                                IsUserActive = 1,
                                IsFKViolation = 1,
                                Message = ConstatntMessages.ForeignKeyViolation
                            })
                            , Formatting.Indented, settings);


                        }
                    }
                }
                else
                {
                    //return (new DeleteAttachmentResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                           (new DeleteAttachmentResult
                           {
                               Success = 0,
                               Message = ConstatntMessages.ErrorMessage
                           })
                           , Formatting.Indented, settings);

                }
            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        #endregion
        
        #region Change Work Request Status

        /// <summary>
        /// Change Work Request Master
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage ChangeWorkRequestStatus(ChangeWorkRequestStatusResult model, string clientCode)
        {

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model != null)
                {
                    WorkRequestService objWorkRequestService = new WorkRequestService();
                    ChangeWorkRequestStatusResult returnChangeWorkRequestStatusResultObject = objWorkRequestService.ChangeWorkRequestStatus(model, clientCode);
                    returnChangeWorkRequestStatusResultObject.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    //return returnChangeWorkRequestStatusResultObject;
                    JsonString = JsonConvert.SerializeObject(
                         returnChangeWorkRequestStatusResultObject
                         , Formatting.Indented, settings);
                }
                else
                {
                    //return (new ChangeWorkRequestStatusResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new ChangeWorkRequestStatusResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                         , Formatting.Indented, settings);

                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new ChangeWorkRequestStatusResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); 
                            
                            // Foreign Key violation

                            JsonString = JsonConvert.SerializeObject(
                            (new ChangeWorkRequestStatusResult
                            {
                                Success = 1,
                                IsUserActive = 1,
                                IsFKViolation = 1,
                                Message = ConstatntMessages.ForeignKeyViolation
                            })
                            , Formatting.Indented, settings);

                        }
                    }
                }
                else
                {
                    //return (new ChangeWorkRequestStatusResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                           (new ChangeWorkRequestStatusResult
                           {
                               Success = 0,
                               Message = ConstatntMessages.ErrorMessage
                           })
                            , Formatting.Indented, settings);

                }
            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        #endregion

        #region Work Order Master

        /// <summary>
        /// Get Work Order Master Tables
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkOrderMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model.TimeStamp != null)
                {
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetWorkOrderMasterResult objWorkOrderMasterResult = objWorkOrderService.GetWorkOrderMaster(model.TimeStamp, clientCode);

                    if (objWorkOrderMasterResult != null)
                    {
                        objWorkOrderMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkOrderMasterResult.Success = 0;
                        objWorkOrderMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkOrderMasterResult;


                    JsonString = JsonConvert.SerializeObject(
                     objWorkOrderMasterResult
                     , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkOrderMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkOrderMasterResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                           , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkOrderMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkOrderMasterResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                          , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;            
        }
        
        /// <summary>
        /// Get Work Order Master checklistelementsTable Tables
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkOrderMasterChecklistelementsTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model.TimeStamp != null)
                {
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetWorkOrderMasterChecklistelementsTableResult objWorkOrderMasterResult = objWorkOrderService.GetWorkOrderMasterChecklistelementsTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkOrderMasterResult != null)
                    {
                        objWorkOrderMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkOrderMasterResult.Success = 0;
                        objWorkOrderMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkOrderMasterResult;


                    JsonString = JsonConvert.SerializeObject(
                     objWorkOrderMasterResult
                     , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkOrderMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkOrderMasterChecklistelementsTableResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                           , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkOrderMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkOrderMasterChecklistelementsTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                          , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Get Work Order Master Tables
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkOrderMasterChecklistToolsTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model.TimeStamp != null)
                {
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetWorkOrderMasterChecklistToolsTableResult objWorkOrderMasterResult = objWorkOrderService.GetWorkOrderMasterChecklistToolsTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkOrderMasterResult != null)
                    {
                        objWorkOrderMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkOrderMasterResult.Success = 0;
                        objWorkOrderMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkOrderMasterResult;


                    JsonString = JsonConvert.SerializeObject(
                     objWorkOrderMasterResult
                     , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkOrderMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkOrderMasterChecklistToolsTableResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                           , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkOrderMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkOrderMasterChecklistToolsTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                          , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        /// <summary>
        /// Get Work Order Master PMchecklist Table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkOrderMasterPMchecklistTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model.TimeStamp != null)
                {
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetWorkOrderMasterPMChecklistTableResult objWorkOrderMasterResult = objWorkOrderService.GetWorkOrderMasterPMchecklistTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkOrderMasterResult != null)
                    {
                        objWorkOrderMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkOrderMasterResult.Success = 0;
                        objWorkOrderMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkOrderMasterResult;


                    JsonString = JsonConvert.SerializeObject(
                     objWorkOrderMasterResult
                     , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkOrderMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkOrderMasterPMChecklistTableResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                           , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkOrderMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkOrderMasterPMChecklistTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                          , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }
                
        /// <summary>
        /// Get Work Order Master Tables Stockcode Table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>       
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetWorkOrderMasterStockcodeTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                if (model.TimeStamp != null)
                {
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetWorkOrderMasterStockcodeTableResult objWorkOrderMasterResult = objWorkOrderService.GetWorkOrderMasterStockcodeTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objWorkOrderMasterResult != null)
                    {
                        objWorkOrderMasterResult.Success = 1;
                    }
                    else
                    {
                        objWorkOrderMasterResult.Success = 0;
                        objWorkOrderMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objWorkOrderMasterResult;


                    JsonString = JsonConvert.SerializeObject(
                     objWorkOrderMasterResult
                     , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetWorkOrderMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new GetWorkOrderMasterStockcodeTableResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         })
                           , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetWorkOrderMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                        (new GetWorkOrderMasterStockcodeTableResult
                        {
                            Success = 0,
                            Message = ConstatntMessages.ErrorMessage
                        })
                          , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;            
        }


        #endregion

        #region Existing Workorder Master IDS

        /// <summary>
        /// Get Existing WorkOrder MasterIDs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkOrderMasterIDs(string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                WorkOrderService objWorkOrderService = new WorkOrderService();
                GetExistingWorkOrderMasterIDsResult objExistingWorkOrderMasterIDsResult = objWorkOrderService.GetExistingWorkOrdertMasterIDs(clientCode);

                if (objExistingWorkOrderMasterIDsResult != null)
                {
                    objExistingWorkOrderMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkOrderMasterIDsResult.Success = 0;
                    objExistingWorkOrderMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkOrderMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkOrderMasterIDsResult
                         , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkOrderMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkOrderMasterIDsResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                         , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }


        /// <summary>
        /// Get Existing WorkOrder MasterIDs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingWorkOrderMasterIDsLstStockID(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;


            try
            {
                WorkOrderService objWorkOrderService = new WorkOrderService();
                GetExistingWorkOrderMasterIDsLstStockIDResult objExistingWorkOrderMasterIDsResult = objWorkOrderService.GetExistingWorkOrderMasterIDsLstStockID(clientCode, model.PageIndex, model.PageSize);

                if (objExistingWorkOrderMasterIDsResult != null)
                {
                    objExistingWorkOrderMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingWorkOrderMasterIDsResult.Success = 0;
                    objExistingWorkOrderMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objExistingWorkOrderMasterIDsResult;

                JsonString = JsonConvert.SerializeObject(
                       objExistingWorkOrderMasterIDsResult
                         , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingWorkOrderMasterIDsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                       (new GetExistingWorkOrderMasterIDsLstStockIDResult
                       {
                           Success = 0,
                           Message = ConstatntMessages.ErrorMessage
                       })
                         , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }


        #endregion

        #region Permission Mapping

        /// <summary>
        /// User Permission as per EmployeeId and UserGroup Id
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetPermissionMapping(PermissionMappingResult model, string clientCode)
        {

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.EmployeeId > 0 && model.UserGroupId > 0)
                {
                    PermissionWebService objPermissionService = new PermissionWebService();
                    PermissionMappingResult objPermissionMappingResult = objPermissionService.GetPermissionMapping(model,clientCode);
                    objPermissionMappingResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    if (objPermissionMappingResult != null)
                    {
                        objPermissionMappingResult.Success = 1;
                    }
                    else
                    {
                        objPermissionMappingResult.Success = 0;
                        objPermissionMappingResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objPermissionMappingResult;
                    JsonString = JsonConvert.SerializeObject(
                     objPermissionMappingResult
                        , Formatting.Indented, settings);
                }
                else
                {
                    //return (new PermissionMappingResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                     (new PermissionMappingResult
                     {
                         Success = 0,
                         Message = ConstatntMessages.IncorrectParameter
                     })
                        , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new PermissionMappingResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                     (new PermissionMappingResult
                     {
                         Success = 0,
                         Message = ConstatntMessages.ErrorMessage
                     })
                        , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Project No Masters

        /// <summary>
        /// Masters for Project No
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetProjectNoMaster(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;
            
            try
            {
                if (model.TimeStamp != null)
                {                    
                    
                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetProjectNoMasterResult objProjectNoMasterResult = objWorkOrderService.GetProjectNoMaster(model.TimeStamp,clientCode);

                    if (objProjectNoMasterResult != null)
                    {
                        objProjectNoMasterResult.Success = 1;
                    }
                    else
                    {
                        objProjectNoMasterResult.Success = 0;
                        objProjectNoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                   // return objProjectNoMasterResult;

                    JsonString = JsonConvert.SerializeObject(
                    objProjectNoMasterResult
                       , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetProjectNoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                    (new GetProjectNoMasterResult
                    {
                        Success = 0,
                        Message = ConstatntMessages.IncorrectParameter
                    })
                       , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetProjectNoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                    (new GetProjectNoMasterResult
                    {
                        Success = 0,
                        Message = ConstatntMessages.ErrorMessage
                    })
                       , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;


        }


        /// <summary>
        /// Masters for Project No
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetProjectNoMasterCMProjPerformanceEvalutionTable(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.TimeStamp != null)
                {

                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    GetProjectNoMasterCMProjectPerformanceEvaluationResult objProjectNoMasterResult = objWorkOrderService.GetProjectNoMasterCMProjPerformanceEvalutionTable(model.TimeStamp, clientCode, model.PageIndex, model.PageSize);

                    if (objProjectNoMasterResult != null)
                    {
                        objProjectNoMasterResult.Success = 1;
                    }
                    else
                    {
                        objProjectNoMasterResult.Success = 0;
                        objProjectNoMasterResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    // return objProjectNoMasterResult;

                    JsonString = JsonConvert.SerializeObject(
                    objProjectNoMasterResult
                       , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetProjectNoMasterResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                    (new GetProjectNoMasterCMProjectPerformanceEvaluationResult
                    {
                        Success = 0,
                        Message = ConstatntMessages.IncorrectParameter
                    })
                       , Formatting.Indented, settings);

                }
            }
            catch (Exception)
            {
                //return (new GetProjectNoMasterResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                    (new GetProjectNoMasterResult
                    {
                        Success = 0,
                        Message = ConstatntMessages.ErrorMessage
                    })
                       , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;


        }


        #region Existing Project No Ids

        /// <summary>
        /// Get Existing Project-No MasterIDs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingProjectNoMasterIds(string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkOrderService objWorkOrderService = new WorkOrderService();
                GetExistingProjectNoMasterIdsResult objExistingProjectNoMasterIDsResult = objWorkOrderService.GetExistingProjectNoMasterIds(clientCode);

                if (objExistingProjectNoMasterIDsResult != null)
                {
                    objExistingProjectNoMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingProjectNoMasterIDsResult.Success = 0;
                    objExistingProjectNoMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
               // return objExistingProjectNoMasterIDsResult;


                JsonString = JsonConvert.SerializeObject(
                   objExistingProjectNoMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingProjectNoMasterIdsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                   (new GetExistingProjectNoMasterIdsResult
                   {
                       Success = 0,
                       Message = ConstatntMessages.ErrorMessage
                   })
                       , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;


        }


        /// <summary>
        /// Get Existing Project-No MasterIDs
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIds(GetCurrentTimeStamp model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                WorkOrderService objWorkOrderService = new WorkOrderService();
                GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult objExistingProjectNoMasterIDsResult = objWorkOrderService.GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIds(clientCode, model.PageIndex, model.PageSize);

                if (objExistingProjectNoMasterIDsResult != null)
                {
                    objExistingProjectNoMasterIDsResult.Success = 1;
                }
                else
                {
                    objExistingProjectNoMasterIDsResult.Success = 0;
                    objExistingProjectNoMasterIDsResult.Message = ConstatntMessages.ErrorMessage;
                }
                // return objExistingProjectNoMasterIDsResult;


                JsonString = JsonConvert.SerializeObject(
                   objExistingProjectNoMasterIDsResult
                       , Formatting.Indented, settings);

            }
            catch (Exception)
            {
                //return (new GetExistingProjectNoMasterIdsResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                   (new GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult
                   {
                       Success = 0,
                       Message = ConstatntMessages.ErrorMessage
                   })
                       , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;


        }


        #endregion

        #endregion

        #region Test Service

        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage test(Employee model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            JsonString = JsonConvert.SerializeObject(
                  "مااتتتتخختخهةهةخخوخووت"
                   , Formatting.Indented, settings);

            //return "مااتتتتخختخهةهةخخوخووت";
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        #endregion

        #region Job/Work Order List

        /// <summary>
        /// Work order List with Assosiated Tab Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetJobOrderList(GetJobOrderListResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model == null)
                {
                    //return (new GetJobOrderListResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});
                    JsonString = JsonConvert.SerializeObject(
                                      (new GetJobOrderListResult
                                      {
                                          Success = 0,
                                          Message = ConstatntMessages.IncorrectParameter
                                      })
                                      , Formatting.Indented, settings);

                }

                WorkOrderService obj = new WorkOrderService();
                var objGetJobOrderListResult =
                    obj.GetJobOrderList(model.IsCentral, model.EmployeeID, model.PageIndex, model.PageSize, model.WorkOrderNoToRefresh, model.FromJoHistory, model.SynchTime, clientCode);
                if (objGetJobOrderListResult != null)
                {
                    objGetJobOrderListResult.Success = 1;
                }
                else
                {
                    objGetJobOrderListResult.Success = 0;
                    objGetJobOrderListResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objGetJobOrderListResult;

                JsonString = JsonConvert.SerializeObject(
                                      objGetJobOrderListResult
                                      , Formatting.Indented, settings);


            }
            catch (Exception)
            {
                //return (new GetJobOrderListResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});

                JsonString = JsonConvert.SerializeObject(
                                      (new GetJobOrderListResult
                                      {
                                          Success = 0,
                                          Message = ConstatntMessages.ErrorMessage
                                      })
                                      , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }
        #endregion

        #region Job/Work Order Add/Update/Delete

        /// <summary>
        /// Add/Update/Delete Work order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage AddUpdateWorkOrder(WorkOrder model, string clientCode)
        {

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model == null)
                {
                    //return (new AddUpdateWorkOrderResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                         (new AddUpdateWorkOrderResult
                         {
                             Success = 0,
                             Message = ConstatntMessages.IncorrectParameter
                         }), Formatting.Indented, settings);

                }
                //var attachmentPath = AppDomain.CurrentDomain.BaseDirectory;
                var attachmentPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]);

                WorkOrderService objWorkOrderService = new WorkOrderService();
                var objAddUpdateJobOrderResult = objWorkOrderService.AddUpdateWorkOrder(model, attachmentPath, clientCode);
                if (objAddUpdateJobOrderResult != null)
                {
                    objAddUpdateJobOrderResult.Success = 1;
                }
                else
                {
                    objAddUpdateJobOrderResult.Success = 0;
                    objAddUpdateJobOrderResult.Message = ConstatntMessages.ErrorMessage;
                }
                //return objAddUpdateJobOrderResult;

                JsonString = JsonConvert.SerializeObject(
                         objAddUpdateJobOrderResult
                         , Formatting.Indented, settings);

            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new AddUpdateWorkOrderResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); 
                            // Foreign Key violation

                            JsonString = JsonConvert.SerializeObject(
                             (new AddUpdateWorkOrderResult
                             {
                                 Success = 1,
                                 IsUserActive = 1,
                                 IsFKViolation = 1,
                                 Message = ConstatntMessages.ForeignKeyViolation
                             })
                             , Formatting.Indented, settings);

                        }
                    }
                }
                else
                {
                    //return (new AddUpdateWorkOrderResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                           (new AddUpdateWorkOrderResult
                           {
                               Success = 0,
                               Message = ConstatntMessages.ErrorMessage
                           })
                            , Formatting.Indented, settings);

                }
            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion

        #region Asset List

        /// <summary>
        /// Get Asset List with Associated Tab Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetAssetList(GetAssetListResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                //DateTime? currentTimeStamp;
                //currentTimeStamp = BaseService.ConvertStringToDataTime(model.SynchTime);

                if (model != null)
                {
                    AssetWebService objAssetService = new AssetWebService();
                    GetAssetListResult objGetAssetListResult = objAssetService.GetAssetList(model.IsCentral, model.EmployeeID, model.PageIndex, model.PageSize, model.AssetIdToRefresh, model.JobOrderHistoryCount, model.SynchTime,clientCode);
                    if (objGetAssetListResult != null)
                    {
                        objGetAssetListResult.Success = 1;
                    }
                    else
                    {
                        objGetAssetListResult.Success = 0;
                        objGetAssetListResult.Message = ConstatntMessages.ErrorMessage;
                    }
                    //return objGetAssetListResult;
                    JsonString = JsonConvert.SerializeObject(
                                      objGetAssetListResult
                                      , Formatting.Indented, settings);

                }
                else
                {
                    //return (new GetAssetListResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});
                    JsonString = JsonConvert.SerializeObject(
                                      (new GetAssetListResult
                                      {
                                          Success = 0,
                                          Message = ConstatntMessages.IncorrectParameter
                                      })
                                      , Formatting.Indented, settings);

                }
            }
            catch (Exception ex)
            {
                //return (new GetAssetListResult
                //{
                //    Success = 0,
                //    Message = ConstatntMessages.ErrorMessage
                //});
                JsonString = JsonConvert.SerializeObject(
                                      (new GetAssetListResult
                                      {
                                          Success = 0,
                                          Message = ConstatntMessages.ErrorMessage
                                      })
                                      , Formatting.Indented, settings);

            }
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Generate Work Order from Work Request

        /// <summary>
        /// Generate Work Order from Work Request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GenerateWorkOrdreFromWorkRequest(Worequest model, string clientCode)
        {

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model != null)
                {
                    WorkRequestService objWorkRequestService = new WorkRequestService();
                    GenerateWorkOrderResult objGenerateWorkOrderResult = objWorkRequestService.GenerateWorkOrdreFromWorkRequest(model, clientCode);
                    if (objGenerateWorkOrderResult != null)
                    {
                        objGenerateWorkOrderResult.Success = 1;
                        objGenerateWorkOrderResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    else
                    {
                        objGenerateWorkOrderResult.Success = 0;
                        objGenerateWorkOrderResult.Message = ConstatntMessages.ErrorMessage;
                        objGenerateWorkOrderResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    //return objGenerateWorkOrderResult;

                    JsonString = JsonConvert.SerializeObject(
                                objGenerateWorkOrderResult
                                , Formatting.Indented, settings);


                }
                else
                {
                    //return (new GenerateWorkOrderResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                (new GenerateWorkOrderResult
                                {
                                    Success = 0,
                                    Message = ConstatntMessages.IncorrectParameter
                                })
                                , Formatting.Indented, settings);

                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new GenerateWorkOrderResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); 
                            
                            // Foreign Key violation
                            JsonString = JsonConvert.SerializeObject(
                                (new GenerateWorkOrderResult
                                {
                                    Success = 1,
                                    IsUserActive = 1,
                                    IsFKViolation = 1,
                                    Message = ConstatntMessages.ForeignKeyViolation
                                })
                                , Formatting.Indented, settings);

                        }
                    }
                }
                else
                {
                    //return (new GenerateWorkOrderResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                (new GenerateWorkOrderResult
                                {
                                    Success = 0,
                                    Message = ConstatntMessages.ErrorMessage
                                })
                                , Formatting.Indented, settings);

                }
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }
        #endregion
        
        #region Work Order UploadSignature

        /// <summary>
        /// Insert/Update Work Order UploadSignature
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage UploadSignature(WorkOrderUploadSignatureResult model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model != null)
                {
                    //string attachmentPath = HttpContext.Current.Server.MapPath("~/AttachmentTest/");
                    //string attachmentPath = AppDomain.CurrentDomain.BaseDirectory;
                    //string attachmentPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]);

                    WorkOrderService objWorkOrderService = new WorkOrderService();
                    WorkOrderUploadSignatureResult objAddUpdateWorkRequestResult = objWorkOrderService.AddUpdateWorkOrderUploadSignature(model,clientCode);
                    if (objAddUpdateWorkRequestResult != null)
                    {
                        if (objAddUpdateWorkRequestResult.Success == 1)
                        {
                            objAddUpdateWorkRequestResult.Success = 1;
                            objAddUpdateWorkRequestResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                        }
                        else
                        {
                        objAddUpdateWorkRequestResult.Success = 0;
                        if (string.IsNullOrWhiteSpace(objAddUpdateWorkRequestResult.Message))
                            objAddUpdateWorkRequestResult.Message = ConstatntMessages.ErrorMessage;
                        objAddUpdateWorkRequestResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                        }
                    }
                    else
                    {
                        objAddUpdateWorkRequestResult.Success = 0;
                        objAddUpdateWorkRequestResult.Message = ConstatntMessages.ErrorMessage;
                        objAddUpdateWorkRequestResult.CurrentTimeStamp = DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss");
                    }
                    //return objAddUpdateWorkRequestResult;
                    JsonString = JsonConvert.SerializeObject(
                                     objAddUpdateWorkRequestResult
                                     , Formatting.Indented, settings);
                }
                else
                {
                    //return (new WorkOrderUploadSignatureResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.IncorrectParameter
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                     (new WorkOrderUploadSignatureResult
                                     {
                                         Success = 0,
                                         Message = ConstatntMessages.IncorrectParameter
                                     })
                                     , Formatting.Indented, settings);
                }
            }
            catch (Exception e)
            {
                var sqlException = e.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    if (sqlException.Errors.Count > 0)
                    {
                        if (sqlException.Errors[0].Number == 547)
                        {
                            //return (new WorkOrderUploadSignatureResult
                            //{
                            //    Success = 1,
                            //    IsUserActive = 1,
                            //    IsFKViolation = 1,
                            //    Message = ConstatntMessages.ForeignKeyViolation
                            //}); // Foreign Key violation

                            JsonString = JsonConvert.SerializeObject(
                                     (new WorkOrderUploadSignatureResult
                                     {
                                         Success = 1,
                                         IsUserActive = 1,
                                         IsFKViolation = 1,
                                         Message = ConstatntMessages.ForeignKeyViolation
                                     })
                                     , Formatting.Indented, settings);
                        }
                    }
                }
                else
                {
                    //return (new WorkOrderUploadSignatureResult
                    //{
                    //    Success = 0,
                    //    Message = ConstatntMessages.ErrorMessage
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                     (new WorkOrderUploadSignatureResult
                                     {
                                         Success = 0,
                                         Message = ConstatntMessages.ErrorMessage + e.InnerException.ToString() + e.Message.ToString()  
                                     })
                                     , Formatting.Indented, settings);
                }
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Keep Last heart beat updated

        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage KeepLastHeartBeatUpdated(KeepLastHeartBeatUpdated model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {
                if (model.employeeId > 0)
                {
                    EmployeeDetailService objEmployeeDetailService = new EmployeeDetailService();
                    KeepLastHeartBeatUpdated ObjHeartbeatUpdated = objEmployeeDetailService.KeepLastHeartBeatUpdated(model.employeeId, clientCode);
                    //return ObjHeartbeatUpdated;

                    JsonString = JsonConvert.SerializeObject(
                                      ObjHeartbeatUpdated
                                      , Formatting.Indented, settings);

                }
                else
                {
                    //return (new KeepLastHeartBeatUpdated
                    //{
                    //    HeartbeatUpdated = 0
                    //});

                    JsonString = JsonConvert.SerializeObject(
                                      (new KeepLastHeartBeatUpdated
                                      {
                                          HeartbeatUpdated = 0
                                      })
                                      , Formatting.Indented, settings);

                }
            }
            catch (Exception ex)
            {
                //return (new KeepLastHeartBeatUpdated
                //{
                //    HeartbeatUpdated = 0
                //});

                JsonString = JsonConvert.SerializeObject(
                     (new KeepLastHeartBeatUpdated
                     {
                        HeartbeatUpdated = 0
                     })
                     , Formatting.Indented, settings);

            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;

        }

        #endregion

        #region Get Server UTC DateTime

        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage GetServerUTCDateTime(KeepLastHeartBeatUpdated model, string clientCode)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            try
            {              
               // return DateTime.UtcNow;
                JsonString = JsonConvert.SerializeObject(
                  DateTime.UtcNow, Formatting.Indented, settings);

            }
            catch (Exception ex)
            {
                //return DateTime.UtcNow;
                JsonString = JsonConvert.SerializeObject(
                  DateTime.UtcNow
                   , Formatting.Indented);
            }

            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        #endregion
        
        [HttpGet]
        public string TestAPI2()
        {
            try
            {
                return "Your Name : 2";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        [HttpPost]
        [ResponseType(typeof(HttpResponseMessage))]
        public HttpResponseMessage testreturnvalue()
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver(),
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat
            };
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            string JsonString = string.Empty;

            EmployeeDetailService obj = new EmployeeDetailService();
            int LogoutResult = obj.testreturnvalue();

            JsonString = JsonConvert.SerializeObject(
                  "مااتتتتخختخهةهةخخوخووت"
                   , Formatting.Indented, settings);
            
            response.Content = new StringContent(JsonString, Encoding.UTF8, "application/json");
            return response;
        }

        
        //[HttpGet]
        //[ResponseType(typeof(HttpResponseMessage))]        
        //public HttpResponseMessage TestAPI()
        //{
        //    List<Employee> list = new List<Employee>();
        //    var response = this.Request.CreateResponse(HttpStatusCode.OK);
        //    try
        //    {
        //        Employee employeeModel = new Employee();
        //        employeeModel.Accessibility = "1";
        //        list.Add(employeeModel);


        //         //var settings = new JsonSerializerSettings()
        //         //       {
        //         //           ContractResolver = new OrderedContractResolver()
        //         //       };

        //        // JsonConvert.SerializeObject(new DataResult(list, 1, ConstatntMessages.LoginSuccess), Formatting.Indented, settings);



        //        string yourJson = ""; //JsonConvert.SerializeObject(new DataResult(list, 1, ConstatntMessages.LoginSuccess), Formatting.Indented, settings);
                                                
                
        //        response.Content = new StringContent(yourJson, Encoding.UTF8, "application/json");
                

        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //    return response;
        //}
    }
}
