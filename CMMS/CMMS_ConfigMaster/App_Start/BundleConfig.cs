﻿using System.Web;
using System.Web.Optimization;

namespace CMMS_ConfigMaster
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //            "~/Content/themes/base/jquery.ui.core.css",
            //            "~/Content/themes/base/jquery.ui.resizable.css",
            //            "~/Content/themes/base/jquery.ui.selectable.css",
            //            "~/Content/themes/base/jquery.ui.accordion.css",
            //            "~/Content/themes/base/jquery.ui.autocomplete.css",
            //            "~/Content/themes/base/jquery.ui.button.css",
            //            "~/Content/themes/base/jquery.ui.dialog.css",
            //            "~/Content/themes/base/jquery.ui.slider.css",
            //            "~/Content/themes/base/jquery.ui.tabs.css",
            //            "~/Content/themes/base/jquery.ui.datepicker.css",
            //            "~/Content/themes/base/jquery.ui.progressbar.css",
            //            "~/Content/themes/base/jquery.ui.theme.css"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
                        "~/Scripts/jquery-ui.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrapDeleteConform").Include(
                      "~/Scripts/bootstrap-tooltip.js",
                      "~/Scripts/bootstrap-confirmation.js"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
                      "~/Scripts/fastclick.min.js",
                      "~/Scripts/custom-tabs.js",
                      "~/Scripts/general.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                    "~/Scripts/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/advancedSearch").Include(
                   "~/Scripts/AdvancedSearch.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/kendo.all.min.js",
                ////"~/Scripts/kendo/kendo.timezones.min.js", // uncomment if using the Scheduler
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/jszip.min.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.plus.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.picker.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.ummalqura.js",
                "~/Scripts/moment.js",
                "~/Scripts/moment-with-locales.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrapCSS").Include(
                      "~/Content/css/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/BundleCSS").Include(
                     "~/Content/css/custom-tabs.css",
                     "~/Content/css/style.css",
                     "~/Content/css/font.css",
                     "~/Content/css/theme.css",
                      "~/Content/css/custom.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/kendocss").Include(
                        "~/Content/kendo/kendo.common-bootstrap.min.css",
                        "~/Content/kendo/kendo.bootstrap.min.css",
                        "~/Content/kendo/kendo.bootstrap.mobile.min.css",
                        "~/Content/kendo/kendo.rtl.min.css",
                        "~/Content/JqueryCalendar/jquery.calendars.picker.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}