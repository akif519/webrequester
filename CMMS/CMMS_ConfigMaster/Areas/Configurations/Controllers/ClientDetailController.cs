﻿using CMMS.Controllers;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Service.ConfigMaster;
using Kendo.Mvc.UI;
using Microsoft.SqlServer.Management.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;
using System.IO;
using CMMS.Service.UserAuthentication;
using System.Configuration;
using System.Text.RegularExpressions;
using System.DirectoryServices;
using Microsoft.Web.Administration;


namespace CMMS_ConfigMaster.Areas.Configurations.Controllers
{
    //[Authorize]
    public partial class ConfigurationsController : BaseController
    {
        [HttpGet]
        [ActionName(Actions.ClientList)]
        public ActionResult ClientList()
        {
            return View(Views.ClientList);
        }

        [HttpPost]
        [ActionName(Actions.GetClientList)]
        //[ValidateAntiForgeryToken]
        public ActionResult GetClientList([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;
                string strWhere = string.Empty;
                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                AuthenticationData objService = new AuthenticationData();

                var result = new DataSourceResult()
                {
                    Data = objService.GetAccountList(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
                //CreateDatabaseBackup();
                //CreateDatabase();
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.ClientDetail)]
        public ActionResult ClientDetail(int ClientID)
        {
            //Account objAccount = UserAuthenticationData.GetAccountWithClientCode(clientCode);
            //int ClientID = objAccount.ClientID;

            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            ViewBag.meterMasterID = ClientID;
            string strWhere = string.Empty;

            Account objClient = new Account();
            if (ClientID == 0)
            {
                objClient = new Account();
            }

            if (ClientID > 0)
            {
                objClient = ClientData.GetClientDetailByID(ClientID);
            }

            //return Json(objClient, JsonRequestBehavior.AllowGet);
            return this.View(Views.ClientDetail, objClient);

        }
        [ActionName(Actions.GetAllDatabaseType)]
        public ActionResult GetAllDatabaseType()
        {
            DatabaseType objDbType = new DatabaseType();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            IList<DatabaseType> lstDbType = new List<DatabaseType>();
            //query += @"USE master;";
            query += @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;";
            query += @"SELECT * FROM [dbo].[DatabaseType]";

            using (DapperContext objContext = new DapperContext())
            {
                lstDbType = objContext.ExecuteQuery<DatabaseType>(query, parameters);
                return Json(lstDbType, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.GetAllSubCycleType)]
        public ActionResult GetAllSubCycleType()
        {
            SubCycle objSubCycle = new SubCycle();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            IList<SubCycle> lstSubCycle = new List<SubCycle>();
            //query += @"USE master;";
            query += @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;";
            query += @"SELECT * FROM [dbo].[SubCycleTbl]";

            using (DapperContext objContext = new DapperContext())
            {
                lstSubCycle = objContext.ExecuteQuery<SubCycle>(query, parameters);
                return Json(lstSubCycle, JsonRequestBehavior.AllowGet);
            }
        }
        [ActionName(Actions.ManageClientDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageClientDetail(Account objAccount, HttpPostedFileBase LogoImage, HttpPostedFileBase ReportImage, string btnSave)
        {
            string validations = string.Empty;
            int newAdminID = objAccount.ClientID;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            // HttpPostedFileBase fileUserImage;
            //objAccount.ExpDate = objAccount.DateCreated.Value.AddDays(Convert.ToDouble(objAccount.SubCycle));

            #region AccountParameters

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objAccount.ClientID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "UserName",
                Value = objAccount.UserName,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "Password",
                Value = objAccount.Password,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "ClientCode",
                Value = objAccount.ClientCode,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "HdrNameAr",
                Value = objAccount.HdrNameAr,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "HdrNameEn",
                Value = objAccount.HdrNameEn,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "SessionTimeOut",
                Value = objAccount.SessionTimeOut,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "CreatedBy",
                Value = Session["UserName"].ToString(),
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "ModifiedDate",
                Value = objAccount.ModifiedDate,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "IsNamedUser",
                Value = objAccount.IsNamedUser,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "ConCurrentUsers",
                Value = objAccount.ConCurrentUsers,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "MobAppLicCon",
                Value = objAccount.MobAppLicCon,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "MobAppLicNamed",
                Value = objAccount.MobAppLicNamed,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "SubCycle",
                Value = objAccount.SubCycle,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "ExpDate",
                Value = objAccount.ExpDate,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "DateCreated",
                Value = objAccount.DateCreated,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "AccStatus",
                Value = objAccount.AccStatus,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "DbType",
                Value = objAccount.DbType,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "DbName",
                Value = objAccount.DbName,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "DbConString",
                Value = objAccount.DbConString,
                DBType = DbType.String
            });
           
            parameters.Add(new DBParameters()
            {
                Name = "NoOfLicenses",
                Value = objAccount.NoOfLicenses,
                DBType = DbType.Int32
            });

            #endregion

            if (ModelState.IsValid)
            {

               
                using (DapperContext context = new DapperContext())
                {
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',CONVERT(nvarchar(50), DecryptByKey(ClientCode)) AS 'ClientCode',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp]  FROM [dbo].[Accounts] WHERE ClientID = @ClientID";
                    var oldResult = context.ExecuteQuery<Account>(filterQuery, parameters);
                    string totalRecords = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',CONVERT(nvarchar(50), DecryptByKey(ClientCode)) AS 'ClientCode',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp]  FROM [dbo].[Accounts]";
                    var count = context.ExecuteQuery<Account>(totalRecords, parameters).Count();
                    if (oldResult.Count == 0)
                    {

                        var ClientID = count + 1;
                        parameters.Add(new DBParameters()
                        {
                            Name = "ClientID",
                            Value = ClientID,
                            DBType = DbType.Int32
                        });

                        List<Account> acc = context.ExecuteQuery<Account>(totalRecords, parameters).ToList();

                        //AutoGenerate ClientCode & Database Name     
                        var clientCodeCount = acc.Select(a => a.ClientCode).ToList();

                        string clientCode = string.Empty;
                        string DbName = string.Empty;

                        if (clientCodeCount.Count > 0)
                        {
                            var noarray = new int[clientCodeCount.ToList().Count];
                            for (int i = 0; i < noarray.Length; i++)
                            {
                                int no = Convert.ToInt32(Regex.Replace(clientCodeCount[i], "[^0-9]+", string.Empty));
                                noarray[i] = no;
                            }

                            string clientcodeMaxRecord =Convert.ToString((noarray.Max() + 1).ToString("{0}").Trim('{').Trim('}'));
                            clientcodeMaxRecord = clientcodeMaxRecord.ToString().PadLeft(4, '0');
                            clientCode = "N" + clientcodeMaxRecord;
                            string dbName = string.Empty;
                            //string searchdbWord = "Initial Catalog";
                            //string dbName = objAccount.DbConString.Substring(objAccount.DbConString.IndexOf(searchdbWord)).Split('=')[1].Split(';')[0];
                            DbName = "Database" + "_" + clientCode;
                        }
                        else
                            clientCode = "N0101";

                        Random generator = new Random();
                        string strLicenseNo = generator.Next(0, 1000000).ToString("D6");
                        strLicenseNo = clientCode + "-" + DateTime.Now.ToString("MMyydd") + '-' + strLicenseNo;
                        parameters.Add(new DBParameters()
                        {
                            Name = "LicenseNo",
                            Value = strLicenseNo,
                            DBType = DbType.String
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "ClientCode",
                            Value = clientCode,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "dbName",
                            Value = DbName,
                            DBType = DbType.String
                        });


                        parameters.Add(new DBParameters()
                        {
                            Name = "IsItemNoAutoIncrement",
                            Value = objAccount.IsItemNoAutoIncrement,
                            DBType = DbType.Boolean 
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SharedFolderPath",
                            Value = objAccount.SharedFolderPath,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "AllowedFileTypes",
                            Value = objAccount.AllowedFileTypes,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "AllowedMaxFilesize",
                            Value = objAccount.AllowedMaxFilesize,
                            DBType = DbType.Int32  
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "DashboardRefreshTime",
                            Value = objAccount.DashboardRefreshTime,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SessionTimeOutMobApp",
                            Value = objAccount.SessionTimeOutMobApp,
                            DBType = DbType.Int32
                        });






                        if (LogoImage != null && LogoImage.ContentLength > 0)
                        {
                            string strFolderPath = Path.Combine(objAccount.SharedFolderPath, "");
                            strFolderPath = Path.Combine(strFolderPath, clientCode.ToString());
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            strFolderPath = Path.Combine(strFolderPath, "HeaderImages");
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            string filePath = Path.Combine(strFolderPath, clientCode.ToString() + "_" + LogoImage.FileName);
                            LogoImage.SaveAs(filePath);
                            objAccount.HdrLogo = clientCode.ToString() + "_" + LogoImage.FileName;
                        }
                        else
                        {
                            if (objAccount.ClientID > 0)
                            {
                                Collection<DBParameters> parameters2 = new Collection<DBParameters>();
                                parameters2.Add(new DBParameters()
                                {
                                    Name = "ClientID",
                                    Value = objAccount.ClientID,
                                    DBType = DbType.Int32
                                });

                                using (DapperContext context2 = new DapperContext())
                                {
                                    string filterQuery2 = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],[HdrLogo],[RprtLogo] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";
                                    List<Account> acc2 = context.ExecuteQuery<Account>(filterQuery2, parameters2).ToList();
                                    objAccount.HdrLogo = acc2.Select(a => a.HdrLogo).FirstOrDefault();
                                }
                            }
                            else
                            {
                                objAccount.HdrLogo = string.Empty;
                            }
                        }

                        if (ReportImage != null && ReportImage.ContentLength > 0)
                        {

                            string strFolderPath = Path.Combine(objAccount.SharedFolderPath, "");
                            strFolderPath = Path.Combine(strFolderPath, clientCode.ToString());
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            strFolderPath = Path.Combine(strFolderPath, "ReportImages");
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            string filePath = Path.Combine(strFolderPath, clientCode.ToString() + "_" + ReportImage.FileName);
                            ReportImage.SaveAs(filePath);
                            objAccount.RprtLogo = clientCode.ToString() + "_" + ReportImage.FileName;
                        }
                        else
                        {
                            if (objAccount.ClientID > 0)
                            {
                                Collection<DBParameters> parameters2 = new Collection<DBParameters>();
                                parameters2.Add(new DBParameters()
                                {
                                    Name = "ClientID",
                                    Value = objAccount.ClientID,
                                    DBType = DbType.Int32
                                });
                                using (DapperContext context2 = new DapperContext())
                                {
                                    string filterQuery2 = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],[HdrLogo],[RprtLogo] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";
                                    List<Account> acc2 = context.ExecuteQuery<Account>(filterQuery2, parameters2).ToList();
                                    objAccount.RprtLogo = acc2.Select(a => a.RprtLogo).FirstOrDefault();
                                }
                            }
                            else
                            {
                                objAccount.RprtLogo = string.Empty;
                            }
                        }

                        parameters.Add(new DBParameters()
                        {
                            Name = "HdrLogo",
                            Value = objAccount.HdrLogo,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "RprtLogo",
                            Value = objAccount.RprtLogo,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SessionTimeOut",
                            Value = objAccount.SessionTimeOut,
                            DBType = DbType.Int32
                        });


                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[Accounts]([ClientID],[UserName],[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],[Password],[IsNamedUser],[ConCurrentUsers],[MobAppLicCon],[MobAppLicNamed],[DateCreated],[SubCycle],[ExpDate],[AccStatus],[dbType],[dbName],[dbConString],[LicenseNo],[NoOfLicenses],[ClientCode],[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp])VALUES(@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@UserName)),@HdrLogo,@RprtLogo,@HdrNameEn,@HdrNameAr,@CreatedBy,GetDate(),@SessionTimeOut,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Password)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@IsNamedUser)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ConCurrentUsers)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicCon)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicNamed)),@DateCreated,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@SubCycle)),@ExpDate,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AccStatus)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbType)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbName)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbConString)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@LicenseNo)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@NoOfLicenses)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ClientCode)),@IsItemNoAutoIncrement,@SharedFolderPath,@AllowedFileTypes,@AllowedMaxFilesize,@DashboardRefreshTime,@SessionTimeOutMobApp)";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = ClientID });
                    }
                    else
                    {

                        if (btnSave == "dbConfig" && ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            bool dbExistOrNot;
                            using (SqlConnection backupConn = new SqlConnection())
                            {
                                backupConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["LocalContext"].ConnectionString;
                                backupConn.Open();
                                dbExistOrNot = CheckDatabaseExists(backupConn, objAccount.DbName);
                            }
                            if (!dbExistOrNot)
                            {
                                CreateDatabaseBackup(objAccount.ClientID);
                                TempData["Message"] = "Database Configured Successfully";
                                TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                            }
                            else
                            {
                                TempData["Message"] = "Database Having Same Name Already Exist";
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            }

                            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
                        }


                        if (objAccount.ClientID > 0)
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = "ClientID",
                                Value = objAccount.ClientID,
                                DBType = DbType.Int32
                            });
                            using (DapperContext context2 = new DapperContext())
                            {
                                string filterQuery2 = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],[HdrLogo],[RprtLogo] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";
                                List<Account> acc2 = context.ExecuteQuery<Account>(filterQuery2, parameters).ToList();
                                objAccount.RprtLogo = acc2.Select(a => a.RprtLogo).FirstOrDefault();
                                objAccount.HdrLogo = acc2.Select(a => a.HdrLogo).FirstOrDefault();
                            }
                        }
                        else
                        {
                            objAccount.HdrLogo = string.Empty;
                            objAccount.RprtLogo = string.Empty;
                        }


                        if (LogoImage != null && LogoImage.ContentLength > 0)
                        {
                            string strFolderPath = Path.Combine(objAccount.SharedFolderPath, "");
                            strFolderPath = Path.Combine(strFolderPath, objAccount.ClientCode.ToString());
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            strFolderPath = Path.Combine(strFolderPath, "HeaderImages");
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            if (!string.IsNullOrWhiteSpace(objAccount.HdrLogo))
                            {
                                string ExistingfilePath = Path.Combine(strFolderPath, objAccount.HdrLogo);
                                if (System.IO.File.Exists(ExistingfilePath))
                                {
                                    System.IO.File.Delete(ExistingfilePath);
                                }
                            }

                            string filePath = Path.Combine(strFolderPath, objAccount.ClientCode.ToString() + "_" + LogoImage.FileName);
                            LogoImage.SaveAs(filePath);
                            objAccount.HdrLogo = objAccount.ClientCode.ToString() + "_" + LogoImage.FileName;
                        }

                        if (ReportImage != null && ReportImage.ContentLength > 0)
                        {

                            string strFolderPath = Path.Combine(objAccount.SharedFolderPath, "");
                            strFolderPath = Path.Combine(strFolderPath, objAccount.ClientCode.ToString());
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            strFolderPath = Path.Combine(strFolderPath, "ReportImages");
                            if (!Directory.Exists(strFolderPath))
                            {
                                Directory.CreateDirectory(strFolderPath);
                            }

                            if (!string.IsNullOrWhiteSpace(objAccount.RprtLogo))
                            {
                                string ExistingfilePath = Path.Combine(strFolderPath, objAccount.RprtLogo);
                                if (System.IO.File.Exists(ExistingfilePath))
                                {
                                    System.IO.File.Delete(ExistingfilePath);
                                }
                            }

                            string filePath = Path.Combine(strFolderPath, objAccount.ClientCode.ToString() + "_" + ReportImage.FileName);
                            ReportImage.SaveAs(filePath);
                            objAccount.RprtLogo = objAccount.ClientCode.ToString() + "_" + ReportImage.FileName;
                        }

                        parameters.Add(new DBParameters()
                        {
                            Name = "HdrLogo",
                            Value = objAccount.HdrLogo,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "RprtLogo",
                            Value = objAccount.RprtLogo,
                            DBType = DbType.String
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "SessionTimeOut",
                            Value = objAccount.SessionTimeOut,
                            DBType = DbType.Int32 
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "IsItemNoAutoIncrement",
                            Value = objAccount.IsItemNoAutoIncrement,
                            DBType = DbType.Boolean
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SharedFolderPath",
                            Value = objAccount.SharedFolderPath,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "AllowedFileTypes",
                            Value = objAccount.AllowedFileTypes,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "AllowedMaxFilesize",
                            Value = objAccount.AllowedMaxFilesize,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "DashboardRefreshTime",
                            Value = objAccount.DashboardRefreshTime,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SessionTimeOutMobApp",
                            Value = objAccount.SessionTimeOutMobApp,
                            DBType = DbType.Int32
                        });


                        //strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Accounts]SET [UserName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@UserName)),[HdrLogo] = @HdrLogo,[RprtLogo] = @RprtLogo,[HdrNameEn] = @HdrNameEn,[HdrNameAr] = @HdrNameAr,[CreatedBy] = @CreatedBy,[ModifiedDate] = GetDate(),[SessionTimeOut] = @SessionTimeOut,[Password] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Password)),[IsNamedUser] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@IsNamedUser)),[ConCurrentUsers] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ConCurrentUsers)),[MobAppLicCon] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicCon)),[MobAppLicNamed] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicNamed)), [DateCreated]=@DateCreated, [SubCycle] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@SubCycle)),[ExpDate] = @ExpDate,[AccStatus] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AccStatus)),[dbType] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbType)),[dbName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbName)),[dbConString] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbConString)),[LicenseNo] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@LicenseNo)),[NoOfLicenses] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@NoOfLicenses)),[ClientCode] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ClientCode)),[IsItemNoAutoIncrement]=@IsItemNoAutoIncrement,[SharedFolderPath]=@SharedFolderPath,[AllowedFileTypes]=@AllowedFileTypes,[AllowedMaxFilesize]=@AllowedMaxFilesize,[DashboardRefreshTime]=@DashboardRefreshTime,[SessionTimeOutMobApp]=@SessionTimeOutMobApp WHERE [ClientID] = @ClientID";
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Accounts] SET [UserName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@UserName)),[HdrLogo] = @HdrLogo,[RprtLogo] = @RprtLogo,[HdrNameEn] = @HdrNameEn,[HdrNameAr] = @HdrNameAr,[CreatedBy] = @CreatedBy,[ModifiedDate] = GetDate(),[SessionTimeOut] = @SessionTimeOut,[Password] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Password)),[IsNamedUser] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@IsNamedUser)),[ConCurrentUsers] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ConCurrentUsers)),[MobAppLicCon] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicCon)),[MobAppLicNamed] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@MobAppLicNamed)), [DateCreated]=@DateCreated, [SubCycle] =	EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@SubCycle)),[ExpDate] = @ExpDate,[AccStatus] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AccStatus)),[dbType] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbType)),[dbName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbName)),[dbConString] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@dbConString)),[NoOfLicenses] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@NoOfLicenses)),[ClientCode] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ClientCode)),[IsItemNoAutoIncrement]=@IsItemNoAutoIncrement,[SharedFolderPath]=@SharedFolderPath,[AllowedFileTypes]=@AllowedFileTypes,[AllowedMaxFilesize]=@AllowedMaxFilesize,[DashboardRefreshTime]=@DashboardRefreshTime,[SessionTimeOutMobApp]=@SessionTimeOutMobApp WHERE [ClientID] = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        ViewBag.Message = TempData["Message"].ToString();
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    
                    ServerManager iisManager = new ServerManager();
                    Application app;
                    if (ProjectConfiguration.TestMode)
                    {
                        app = iisManager.Sites[0].Applications["/CMMS_Dev"];
                    }
                    else
                    {
                        app = iisManager.Sites[ProjectConfiguration.IISSiteName].Applications[0];
                    }
                    if (string.IsNullOrWhiteSpace(Convert.ToString(app.VirtualDirectories["/" + objAccount.ClientCode])))
                    {
                        if (!Directory.Exists(objAccount.SharedFolderPath))
                        {
                            Directory.CreateDirectory(objAccount.SharedFolderPath);
                        }
                        app.VirtualDirectories.Add("/" + objAccount.ClientCode, objAccount.SharedFolderPath);
                    }
                    else
                    {
                        if (!Directory.Exists(objAccount.SharedFolderPath))
                        {
                            Directory.CreateDirectory(objAccount.SharedFolderPath);
                        }
                        app.VirtualDirectories["/" + objAccount.ClientCode].PhysicalPath = objAccount.SharedFolderPath;
                    }                    
                    iisManager.CommitChanges();
                }
            }

           

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }





        #region DatabaseCreateBackup&Restore
        public void CreateDatabaseBackup(int clientId)
        {
            Account clientDetail = ClientData.CreateNewDBByClientID(clientId);
            string dbExistOrNot = string.Empty;
            using (SqlConnection backupConn = new SqlConnection())
            {
                backupConn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["LocalContext"].ConnectionString;
                backupConn.Open();

                using (SqlCommand cmd = new SqlCommand("targetnet_copyNewDatabase", backupConn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@TestDB", SqlDbType.VarChar).Value = clientDetail.DbName;
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                }

            }
        }

        public bool CheckDatabaseExists(SqlConnection tmpConn, string databaseName)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);
                using (tmpConn)
                {
                    using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                    {
                        object resultObj = sqlCmd.ExecuteScalar();
                        int databaseID = 0;
                        if (resultObj != null)
                        {
                            int.TryParse(resultObj.ToString(), out databaseID);
                        }
                        tmpConn.Close();
                        result = (databaseID > 0);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }
        #endregion



        [ActionName(Actions.StatusChangeClient)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult StatusChangeClient(int clientID, int bitStatus)
        {
            try
            {
                using (DapperContext context = new DapperContext())
                {
                    string strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Accounts] SET [AccStatus] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AccStatus)) WHERE [ClientID] = @ClientID";
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "ClientID",
                        Value = clientID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "AccStatus",
                        Value = bitStatus,
                        DBType = DbType.Boolean
                    });

                    context.ExecuteQuery(strQuery, parameters);
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Record Updated Successfully" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ErrorContactAdmin" }, JsonRequestBehavior.AllowGet);
            }
        }


        [ActionName(Actions.DeleteClient)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult DeleteClient(int clientID)
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;

            try
            {
                using (ServiceContext adminService = new ServiceContext())
                {
                    string strQueryFeatureTable = "DELETE FROM [dbo].[Tbl_Feature] WHERE ClientID =" + clientID + " ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[ModulesAccess] WHERE ClientID = " + clientID + " ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[TabAccess] WHERE ClientID = " + clientID + " ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[ReportAccess] WHERE ClientID = " + clientID + " ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[CEUML_config] WHERE ClientID = " + clientID + " ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[LevelNamesTbl] WHERE L1_ID = (select L1_ID FROM [dbo].[FirstLevelTbl] WHERE ClientID = " + clientID + ") ";
                    strQueryFeatureTable = strQueryFeatureTable + "DELETE FROM [dbo].[FirstLevelTbl] WHERE ClientID = " + clientID + " ";
                    adminService.ExecuteQuery(strQueryFeatureTable, new Collection<DBParameters>());

                    CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                    int returnValue = adminService.Delete<Account>(clientID, true);
                    if (returnValue == 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), "RecordDeletedSuccessfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else if (returnValue == -2)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ReferencedInOtherModules" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ErrorContactAdmin" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ErrorContactAdmin" }, JsonRequestBehavior.AllowGet);
            }




            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;                
            //using (ServiceContext objContext = new ServiceContext(0))
            //{
            //    string strQueryobjModulesAccess = "DELETE FROM [dbo].[ModulesAccess] WHERE ClientID = " + clientID;                    
            //    objContext.ExecuteQuery(strQueryobjModulesAccess, new Collection<DBParameters>());
            //}

            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;                
            //using (ServiceContext objContext = new ServiceContext(0))
            //{
            //    string strQueryobjModulesAccess = "DELETE FROM [dbo].[TabAccess] WHERE ClientID = @ClientID";
            //    Collection<DBParameters> parameters = new Collection<DBParameters>();
            //    parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
            //    objContext.ExecuteQuery(strQueryobjModulesAccess, parameters);
            //}


            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            //using (ServiceContext objContext = new ServiceContext(0))
            //{
            //    string strQueryobjModulesAccess = "DELETE FROM [dbo].[ReportAccess] WHERE ClientID = @ClientID";
            //    Collection<DBParameters> parameters = new Collection<DBParameters>();
            //    parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
            //    objContext.ExecuteQuery(strQueryobjModulesAccess, parameters);
            //}

            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            //using (ServiceContext objContext = new ServiceContext(0))
            //{
            //    string strQueryobjModulesAccess = "DELETE FROM [dbo].[CEUML_config] WHERE ClientID = @ClientID";
            //    Collection<DBParameters> parameters = new Collection<DBParameters>();
            //    parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
            //    objContext.ExecuteQuery(strQueryobjModulesAccess, parameters);
            //}


            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            //int returnValue = adminService.Delete<Account>(clientID, true);               

        }

        #region AccessTabs


        //ReportAccess Tab
        [ActionName(Actions.ManageReportAccess)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageReportAccess(Account objReportAccess)
        {
            string validations = string.Empty;
            int newAdminID = objReportAccess.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objReportAccess.ClientID,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "Assets",
                Value = objReportAccess.ReportsAccess.Assets,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "JobOrder",
                Value = objReportAccess.ReportsAccess.JobOrder,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "Preventive",
                Value = objReportAccess.ReportsAccess.Preventive,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "Store",
                Value = objReportAccess.ReportsAccess.Store,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "Employee",
                Value = objReportAccess.ReportsAccess.Employee,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "Setups",
                Value = objReportAccess.ReportsAccess.Setups,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "Purchasing",
                Value = objReportAccess.ReportsAccess.Purchasing,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "Cleaning",
                Value = objReportAccess.ReportsAccess.Cleaning,
                DBType = DbType.DateTime
            });

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(Assets)) AS 'Assets',CONVERT(bit, DecryptByKey(Preventive)) AS 'Preventive',CONVERT(bit, DecryptByKey(Store)) AS 'Store',CONVERT(bit, DecryptByKey(Employee)) AS 'Employee',CONVERT(bit, DecryptByKey(Setups)) AS 'Setups',CONVERT(bit, DecryptByKey(Purchasing)) AS 'Purchasing',CONVERT(bit, DecryptByKey(Cleaning)) AS 'Cleaning',CONVERT(bit, DecryptByKey([JobOrder])) AS 'JobOrder'FROM dbo.ReportAccess where ClientID = @ClientID;CLOSE SYMMETRIC KEY CMMSMasterEncryptKey ";
                    var oldResult = context.ExecuteQuery<ReportAccess>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[ReportAccess]([ClientID],[Assets],[JobOrder],[Preventive],[Store],[Employee],[Setups],[Purchasing],[Cleaning])VALUES(@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Assets)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobOrder)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Preventive)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Store)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Employee)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Setups)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Purchasing)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Cleaning)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[ReportAccess]SET [Assets] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Assets)),[Preventive] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Preventive)),[Store] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Store)),[Employee] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Employee)),[Setups] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Setups)),[Purchasing] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Purchasing)),[Cleaning] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Cleaning)),[JobOrder] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobOrder))WHERE ClientID = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }


        //FeatureAccess Tab
        [ActionName(Actions.ManageFeatureAccess)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageFeatureAccess(Account objFeatureAccess)
        {
            string validations = string.Empty;
            int newAdminID = objFeatureAccess.ClientID;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objFeatureAccess.ClientID,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "Area",
                Value = objFeatureAccess.FeatureAccess.Area,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "CMProject",
                Value = objFeatureAccess.FeatureAccess.CMProject,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "CMProjectNoMandatory",
                Value = objFeatureAccess.FeatureAccess.CMProjectNoMandatory,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "WebRequesterLicence",
                Value = objFeatureAccess.FeatureAccess.WebRequesterLicence,
                DBType = DbType.String
            });

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebREquesterLicence)) AS 'WebRequesterLicence'FROM dbo.Tbl_Feature WHERE ClientID = @ClientID;CLOSE SYMMETRIC KEY CMMSMasterEncryptKey";
                    var oldResult = context.ExecuteQuery<Tbl_Feature>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[Tbl_Feature]([ClientID],[Area],[CMProject],[CMProjectNoMandatory],[WebRequesterLicence])VALUES(@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Area)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@CMProject)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@CMProjectNoMandatory)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@WebRequesterLicence)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Tbl_Feature]SET [Area] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Area)),[CMProject] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@CMProject)),[CMProjectNoMandatory] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CMProjectNoMandatory)),[WebRequesterLicence] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@WebRequesterLicence))WHERE [ClientID] = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }

        //TabAccess Tab
        [ActionName(Actions.ManageTabAccess)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageTabAccess(Account objTabAccess)
        {
            string validations = string.Empty;
            int newAdminID = objTabAccess.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            TabAccess tabAccess = new TabAccess();
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objTabAccess.ClientID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "Transactions",
                Value = objTabAccess.TabAccess.Transactions,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "Setup",
                Value = objTabAccess.TabAccess.Setup,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "Reports",
                Value = objTabAccess.TabAccess.Reports,
                DBType = DbType.Int32
            }); parameters.Add(new DBParameters()
            {
                Name = "Dashboard",
                Value = objTabAccess.TabAccess.Dashboard,
                DBType = DbType.Int32
            });

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    var filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(Transactions)) AS 'Transactions',CONVERT(bit, DecryptByKey(Setup)) AS 'Setup',CONVERT(bit, DecryptByKey(Reports)) AS 'Reports',CONVERT(bit, DecryptByKey(Dashboard)) AS 'Dashboard'FROM dbo.TabAccess WHERE ClientID = @ClientID;CLOSE SYMMETRIC KEY CMMSMasterEncryptKey";
                    var oldResult = context.ExecuteQuery<TabAccess>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[TabAccess]([ClientID],[Transactions],[Setup],[Reports],[Dashboard])VALUES(@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Transactions)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Setup)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Reports)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Dashboard)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[TabAccess] SET [Transactions] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Transactions)),[Setup] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Setup)),[Reports] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Reports)),[Dashboard] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Dashboard))WHERE [ClientID] = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }

        //ModuleAccess Tab
        [ActionName(Actions.ManageModuleAccess)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageModuleAccess(Account objModuleAccess)
        {
            //string[] p = ModuleAccessType[0].Split(',').ToArray();

            //var output = Enumerable.Range(0, p.ToList().Count / 2)
            //           .Select(i => Tuple.Create(p.ToList()[i * 2], p.ToList()[i * 2 + 1]))
            //           .ToList();
            string validations = string.Empty;
            int newAdminID = objModuleAccess.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            #region ModuleAccessParameter

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objModuleAccess.ClientID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "JobRequest",
                Value = objModuleAccess.ModulesAccess.JobRequest,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "JobOrder",
                Value = objModuleAccess.ModulesAccess.JobOrder,
                DBType = DbType.Boolean
            });

            parameters.Add(new DBParameters()
            {
                Name = "Assets",
                Value = objModuleAccess.ModulesAccess.Assets,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Locations",
                Value = objModuleAccess.ModulesAccess.Locations,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "PreventiveMaintenance",
                Value = objModuleAccess.ModulesAccess.PreventiveMaintenance,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Store",
                Value = objModuleAccess.ModulesAccess.Store,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Purchasing",
                Value = objModuleAccess.ModulesAccess.Purchasing,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Employee",
                Value = objModuleAccess.ModulesAccess.Employee,
                DBType = DbType.Boolean
            });

            parameters.Add(new DBParameters()
            {
                Name = "Cleaning",
                Value = objModuleAccess.ModulesAccess.Cleaning,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "CEUML",
                Value = objModuleAccess.ModulesAccess.CEUML,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Sector",
                Value = objModuleAccess.ModulesAccess.Sector,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Zone",
                Value = objModuleAccess.ModulesAccess.Zone,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Building",
                Value = objModuleAccess.ModulesAccess.Building,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Division",
                Value = objModuleAccess.ModulesAccess.Division,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Department",
                Value = objModuleAccess.ModulesAccess.Department,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "SubDept",
                Value = objModuleAccess.ModulesAccess.SubDept,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Groups",
                Value = objModuleAccess.ModulesAccess.Groups,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "FailureCodes",
                Value = objModuleAccess.ModulesAccess.FailureCodes,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Suppliers_Contractors",
                Value = objModuleAccess.ModulesAccess.Suppliers_Contractors,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "BoMList",
                Value = objModuleAccess.ModulesAccess.BoMList,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "CostCenter",
                Value = objModuleAccess.ModulesAccess.CostCenter,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "AccountCode",
                Value = objModuleAccess.ModulesAccess.AccountCode,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "JobPlans",
                Value = objModuleAccess.ModulesAccess.JobPlans,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "SafetyInstructions",
                Value = objModuleAccess.ModulesAccess.SafetyInstructions,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "SiteSetups",
                Value = objModuleAccess.ModulesAccess.SiteSetups,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "StoreSetups",
                Value = objModuleAccess.ModulesAccess.StoreSetups,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "PurchaseRequisition",
                Value = objModuleAccess.ModulesAccess.PurchaseRequisition,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "JobManagement",
                Value = objModuleAccess.ModulesAccess.JobManagement,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "AssetSetup",
                Value = objModuleAccess.ModulesAccess.AssetSetup,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Employee_User",
                Value = objModuleAccess.ModulesAccess.Employee_User,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "CleaningSetup",
                Value = objModuleAccess.ModulesAccess.CleaningSetup,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "PurchaseSetup",
                Value = objModuleAccess.ModulesAccess.PurchaseSetup,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "NotificationSetup",
                Value = objModuleAccess.ModulesAccess.NotificationSetup,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "ItemRequisition",
                Value = objModuleAccess.ModulesAccess.ItemRequisition,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "RequestCard",
                Value = objModuleAccess.ModulesAccess.RequestCard,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "Help",
                Value = objModuleAccess.ModulesAccess.Help,
                DBType = DbType.Boolean
            });
            parameters.Add(new DBParameters()
            {
                Name = "SLA",
                Value = objModuleAccess.ModulesAccess.SLA,
                DBType = DbType.Boolean
            });


            parameters.Add(new DBParameters()
            {
                Name = "AuditTrail",
                Value = objModuleAccess.ModulesAccess.AuditTrail,
                DBType = DbType.Boolean
            });


            #endregion


            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    ModulesAccess moduleAccess = new ModulesAccess();
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(AuditTrail)) AS 'AuditTrail',CONVERT(bit, DecryptByKey(JobOrder)) AS 'JobOrder',CONVERT(bit, DecryptByKey(JobRequest)) AS 'JobRequest',CONVERT(bit, DecryptByKey(Assets)) AS 'Assets',CONVERT(bit, DecryptByKey(Locations)) AS 'Locations',CONVERT(bit, DecryptByKey(PreventiveMaintenance)) AS 'PreventiveMaintenance',CONVERT(bit, DecryptByKey(Store)) AS 'Store',CONVERT(bit, DecryptByKey(Purchasing)) AS 'Purchasing',CONVERT(bit, DecryptByKey(Employee)) AS 'Employee',CONVERT(bit, DecryptByKey(Cleaning)) AS 'Cleaning',CONVERT(bit, DecryptByKey(CEUML)) AS 'CEUML',CONVERT(bit, DecryptByKey(Sector)) AS 'Sector',CONVERT(bit, DecryptByKey(Zone)) AS 'Zone',CONVERT(bit, DecryptByKey(Building)) AS 'Building',CONVERT(bit, DecryptByKey(Division)) AS 'Division',CONVERT(bit, DecryptByKey(Department)) AS 'Department',CONVERT(bit, DecryptByKey(SubDept)) AS 'SubDept',CONVERT(bit, DecryptByKey(Groups)) AS 'Groups',CONVERT(bit, DecryptByKey(FailureCodes)) AS 'FailureCodes',CONVERT(bit, DecryptByKey(Suppliers_Contractors)) AS 'Suppliers_Contractors',CONVERT(bit, DecryptByKey(BoMList)) AS 'BoMList',CONVERT(bit, DecryptByKey(CostCenter)) AS 'CostCenter',CONVERT(bit, DecryptByKey(AccountCode)) AS 'AccountCode',CONVERT(bit, DecryptByKey(JobPlans)) AS 'JobPlans',CONVERT(bit, DecryptByKey(SafetyInstructions)) AS 'SafetyInstructions',CONVERT(bit, DecryptByKey(SiteSetups)) AS 'SiteSetups',CONVERT(bit, DecryptByKey(StoreSetups)) AS 'StoreSetups',CONVERT(bit, DecryptByKey(PurchaseRequisition)) AS 'PurchaseRequisition',CONVERT(bit, DecryptByKey(JobManagement)) AS 'JobManagement',CONVERT(bit, DecryptByKey(AssetSetup)) AS 'AssetSetup',CONVERT(bit, DecryptByKey(Employee_User)) AS 'Employee_User',CONVERT(bit, DecryptByKey(CleaningSetup)) AS 'CleaningSetup',CONVERT(bit, DecryptByKey(PurchaseSetup)) AS 'PurchaseSetup',CONVERT(bit, DecryptByKey(NotificationSetup)) AS 'NotificationSetup',CONVERT(bit, DecryptByKey(ItemRequisition)) AS 'ItemRequisition',CONVERT(bit, DecryptByKey(RequestCard)) AS 'RequestCard',CONVERT(bit, DecryptByKey(Help)) AS 'Help',CONVERT(bit, DecryptByKey(SLA)) AS 'SLA'  FROM dbo.ModulesAccess where ClientID = @ClientID;CLOSE SYMMETRIC KEY CMMSMasterEncryptKey";
                    var oldResult = context.ExecuteQuery<ModulesAccess>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[ModulesAccess]([AuditTrail],[ClientID],[JobRequest],[JobOrder],[Assets],[Locations],[PreventiveMaintenance],[Store],[Purchasing],[Employee],[Cleaning],[CEUML],[Sector],[Zone],[Building],[Division],[Department],[SubDept],[Groups],[FailureCodes],[Suppliers_Contractors],[BoMList],[CostCenter],[AccountCode],[JobPlans],[SafetyInstructions],[SiteSetups],[StoreSetups],[PurchaseRequisition],[JobManagement],[AssetSetup],[Employee_User],[CleaningSetup],[PurchaseSetup],[NotificationSetup],[ItemRequisition],[RequestCard],[Help],[SLA])VALUES(EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AuditTrail)),@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobRequest)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobOrder)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Assets)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Locations)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PreventiveMaintenance)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Store)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Purchasing)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Employee)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Cleaning)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CEUML)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Sector)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Zone)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Building)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Division)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Department)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SubDept)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Groups)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@FailureCodes)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Suppliers_Contractors)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@BoMList)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CostCenter)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@AccountCode)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@JobPlans)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SafetyInstructions)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SiteSetups)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@StoreSetups)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PurchaseRequisition)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@JobManagement)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@AssetSetup)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Employee_User)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CleaningSetup)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PurchaseSetup)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@NotificationSetup)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@ItemRequisition)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@RequestCard)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Help)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SLA))  )";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[ModulesAccess] SET [AuditTrail]=EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@AuditTrail)), [JobRequest] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobRequest)),[JobOrder] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@JobOrder)),[Assets] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Assets)),[Locations] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Locations)),[PreventiveMaintenance] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PreventiveMaintenance)),[Store] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Store)),[Purchasing] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Purchasing)),[Employee] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Employee)),[Cleaning] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Cleaning)),[CEUML] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CEUML)),[Sector] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Sector)),[Zone] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Zone)),[Building] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Building)),[Division] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Division)),[Department] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Department)),[SubDept] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SubDept)),[Groups] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Groups)),[FailureCodes] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@FailureCodes)),[Suppliers_Contractors] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Suppliers_Contractors)),[BoMList] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@BoMList)),[CostCenter] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CostCenter)),[AccountCode] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@AccountCode)),[JobPlans] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@JobPlans)),[SafetyInstructions] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SafetyInstructions)),[SiteSetups] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SiteSetups)),[StoreSetups] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@StoreSetups)),[PurchaseRequisition] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PurchaseRequisition)),[JobManagement] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@JobManagement)),[AssetSetup] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@AssetSetup)),[Employee_User] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Employee_User)),[CleaningSetup] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@CleaningSetup)),[PurchaseSetup] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@PurchaseSetup)),[NotificationSetup] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@NotificationSetup)),[ItemRequisition] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@ItemRequisition)),[RequestCard] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@RequestCard)),[Help] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@Help)), [SLA]=EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@SLA)) WHERE [ClientID] = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }

                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });


        }


        [ActionName(Actions.ManageFirstLevelTblAccess)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageFirstLevelTblAccess(Account objFirstLevelTbl)
        {
            string validations = string.Empty;
            int newAdminID = objFirstLevelTbl.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "L1_ID",
                Value = objFirstLevelTbl.ClientID,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objFirstLevelTbl.ClientID,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "L1_en",
                Value = objFirstLevelTbl.FirstLevelTbl.L1_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L1_ar",
                Value = objFirstLevelTbl.FirstLevelTbl.L1_ar,
                DBType = DbType.String
            });


            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    LevelNamesTbl levelNameAccess = new LevelNamesTbl();
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(nvarchar(max), DecryptByKey(L1_en)) AS 'L1_en',CONVERT(nvarchar(max), DecryptByKey(L1_ar)) AS 'L1_ar'FROM [dbo].[FirstLevelTbl] Where ClientID = @ClientID";
                    var oldResult = context.ExecuteQuery<Account>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[FirstLevelTbl]([L1_ID],ClientID,[L1_en],[L1_ar])VALUES(@ClientID,@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_ar)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[FirstLevelTbl]SET [L1_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_en)),[L1_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_ar)) WHERE ClientID = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }

                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }


        [ActionName(Actions.ManageLevelNamesAccess)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageLevelNamesAccess(Account objLevelNameAccess)
        {
            string validations = string.Empty;
            int newAdminID = objLevelNameAccess.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "L1_ID",
                Value = objLevelNameAccess.ClientID,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objLevelNameAccess.ClientID,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "L1_en",
                Value = objLevelNameAccess.FirstLevelTbl.L1_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L1_ar",
                Value = objLevelNameAccess.FirstLevelTbl.L1_ar,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L2_en",
                Value = objLevelNameAccess.LevelNames.L2_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L2_ar",
                Value = objLevelNameAccess.LevelNames.L2_ar,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "L3_en",
                Value = objLevelNameAccess.LevelNames.L3_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L3_ar",
                Value = objLevelNameAccess.LevelNames.L3_ar,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L4_en",
                Value = objLevelNameAccess.LevelNames.L4_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L4_ar",
                Value = objLevelNameAccess.LevelNames.L4_ar,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L5_en",
                Value = objLevelNameAccess.LevelNames.L5_en,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "L5_ar",
                Value = objLevelNameAccess.LevelNames.L5_ar,
                DBType = DbType.String
            });

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {

                    string filterFirstTbl = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(nvarchar(max), DecryptByKey(L1_en)) AS 'L1_en',CONVERT(nvarchar(max), DecryptByKey(L1_ar)) AS 'L1_ar'FROM [dbo].[FirstLevelTbl] Where ClientID = @ClientID";
                    var oldResultFirstTbl = context.ExecuteQuery<Account>(filterFirstTbl, parameters);
                    if (oldResultFirstTbl.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[FirstLevelTbl]([L1_ID],ClientID,[L1_en],[L1_ar])VALUES(@ClientID,@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_ar)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[FirstLevelTbl]SET [L1_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_en)),[L1_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L1_ar)) WHERE ClientID = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }

                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(varchar(max), DecryptByKey(L2_en)) AS 'L2_en',CONVERT(nvarchar(max), DecryptByKey(L2_ar)) AS 'L2_ar',CONVERT(nvarchar(max), DecryptByKey(L3_en)) AS 'L3_en',CONVERT(nvarchar(max), DecryptByKey(L3_ar)) AS 'L3_ar',CONVERT(nvarchar(max), DecryptByKey(L4_en)) AS 'L4_en',CONVERT(nvarchar(max), DecryptByKey(L4_ar)) AS 'L4_ar',CONVERT(nvarchar(max), DecryptByKey(L5_en)) AS 'L5_en',CONVERT(nvarchar(max), DecryptByKey(L5_ar)) AS 'L5_ar'FROM [dbo].[LevelNamesTbl] Where L1_ID = @L1_ID";
                    var oldResult = context.ExecuteQuery<Account>(filterQuery, parameters);
                    if (oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[LevelNamesTbl]([L1_ID],[L2_en],[L2_ar],[L3_en],[L3_ar],[L4_en],[L4_ar],[L5_en],[L5_ar])VALUES(@L1_ID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L2_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L2_ar)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L3_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L3_ar)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L4_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L4_ar)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L5_en)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L5_ar)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[LevelNamesTbl]SET [L2_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L2_en)),[L2_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L2_ar)),[L3_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'), CONVERT(varbinary(max),@L3_en)),[L3_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L3_ar)),[L4_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L4_en)),[L4_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L4_ar)),[L5_en] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L5_en)),[L5_ar] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@L5_ar)) WHERE L1_ID = @L1_ID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }



                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }


        [ActionName(Actions.ManageCEUMLConfig)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageCEUMLConfig(Account objCEUMLConfig)
        {
            string validations = string.Empty;
            int newAdminID = objCEUMLConfig.ClientID;
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strQuery = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = objCEUMLConfig.ClientID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "CEUML_configId",
                Value = objCEUMLConfig.CEUML_config.CEUML_configId,
                DBType = DbType.Int32
            });
            parameters.Add(new DBParameters()
            {
                Name = "CeUser",
                Value = objCEUMLConfig.CEUML_config.CeUser,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "Token",
                Value = objCEUMLConfig.CEUML_config.Token,
                DBType = DbType.String
            });
            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    CEUML_config CEUMLConfig = new CEUML_config();
                    string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(int, DecryptByKey(CEUML_configId)) AS 'CEUML_configId',CONVERT(Nvarchar(50), DecryptByKey(ceUser)) AS 'ceUser',CONVERT(Nvarchar(max), DecryptByKey(Token)) AS 'Token'FROM dbo.[CEUML_config]  where ClientID = @ClientID CLOSE SYMMETRIC KEY CMMSMasterEncryptKey";
                    //string filterQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(Nvarchar(50), DecryptByKey(CEUML_configId)) AS 'CEUML_configId',CONVERT(Nvarchar(50), DecryptByKey(ceUser)) AS 'ceUser',CONVERT(Nvarchar(50), DecryptByKey(Token)) AS 'Token'FROM dbo.[CEUML_config] where ClientID = @ClientID CLOSE SYMMETRIC KEY CMMSMasterEncryptKey";
                    var oldResult = context.ExecuteQuery<CEUML_config>(filterQuery, parameters);
                    if (oldResult == null && oldResult.Count == 0)
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[CEUML_config]([CEUML_configId],[ClientID],[ceUser],[Token])VALUES(EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@CEUML_configId)),@ClientID,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ceUser)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Token)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[CEUML_config]SET [CEUML_configId] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@CEUML_configId)),[ceUser] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@ceUser)),[Token] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@Token))WHERE [ClientID] = @ClientID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }

                }
            }

            return RedirectToAction(Actions.ClientDetail, CMMS.Pages.Controllers.Configurations, new { clientId = newAdminID });
        }
        
        #endregion

    }
}
