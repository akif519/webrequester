﻿using CMMS.Controllers;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Service.ConfigMaster;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMMS_ConfigMaster.Areas.Configurations.Controllers
{
    //[Authorize]
    public partial class ConfigurationsController : BaseController
    {
        // GET: Configurations/AdminUser
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [ActionName(Actions.AdminList)]
        public ActionResult AdminList()
        {

            return View(Views.AdminList);

        }

        [HttpPost]
        [ActionName(Actions.GetAdminList)]
        //[ValidateAntiForgeryToken]
        public ActionResult GetAdminList([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;
                string strWhere = string.Empty;
                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                AdminUserData objService = new AdminUserData();

                var result = new DataSourceResult()
                {
                    Data = objService.GetAdminUserList(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
       
        [ActionName(Actions.AdminUserEdit)]
        public ActionResult AdminUserEdit(int UserId)
        {

            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            ViewBag.meterMasterID = UserId;
            string strWhere = string.Empty;

            Tbl_AdminUser objAdminUser = new Tbl_AdminUser();
            if (UserId == 0)
            {
                objAdminUser = new Tbl_AdminUser();
            }

            if (UserId > 0)
            {
                objAdminUser = AdminUserData.GetAdminUserEditID(UserId);
            }

            return Json(objAdminUser, JsonRequestBehavior.AllowGet);

        }

        [ActionName(Actions.ManageAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageAdmin(Tbl_AdminUser objAdminUser)
        {
            string validations = string.Empty;
            int newAdminID = objAdminUser.UserID;
            
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            ReportAccess reportAccess = new ReportAccess();
            parameters.Add(new DBParameters()
            {
                Name = "UserID",
                Value = objAdminUser.UserID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "FirstName",
                Value = objAdminUser.FirstName,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "LastName",
                Value = objAdminUser.LastName,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "UserName",
                Value = objAdminUser.UserName,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "Password",
                Value = objAdminUser.Password,
                DBType = DbType.String
            });
            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    string strQuery = string.Empty;
                    
                    string userName = Session["UserName"].ToString();
                    Tbl_AdminUser objAdminList = AdminUserData.CheckAccountExistOrNot(objAdminUser.UserName).FirstOrDefault();
                    if (objAdminList == null)
                    {
                        //context.Save(objAdminUser);
                        //TempData["Message"] = "MessageSavedSuccessfully";
                        //TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[Tbl_AdminUser]([FirsName],[LastName],[UserName],[Password])VALUES(EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@FirstName)) ,EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@LastName)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@UserName)),EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@PassWord)))";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Saved Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Record Saved Successfully" });

                    }
                    else
                    {
                        //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                        strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Tbl_AdminUser]SET [FirsName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@FirstName)),[LastName] =EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@LastName)),[UserName] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@UserName)),[Password] = EncryptByKey (Key_GUID('CMMSMasterEncryptKey'),CONVERT(varbinary(max),@PassWord))WHERE UserID = @UserID";
                        context.ExecuteQuery(strQuery, parameters);
                        TempData["Message"] = "Record Updated Successfully";
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Record Updated Successfully" });
                    }
                    //For Encrypted Table
                    //if (objAdminList == null)
                    //{
                    //    strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;INSERT INTO [dbo].[Tbl_AdminUser]([FirstName],[LastName],[UserName],[Password])VALUES( EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@FirstName)), EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@LastName)), EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@UserName)), EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@Password)))";
                    //    context.ExecuteQuery(strQuery, parameters);
                    //    TempData["Message"] = "MessageSavedSuccessfully";
                    //    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    //    return Json(new object[] { SystemEnum.MessageType.success.ToString(), "MessageSavedSuccessfully" });
                    //}
                    //else
                    //{

                    //    strQuery = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;UPDATE [dbo].[Tbl_AdminUser]SET [FirstName] = EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@FirstName)),[LastName] = EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@LastName)),[UserName] = EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@UserName)),[Password] = EncryptByKey (Key_GUID('smCMMS_NUB_Key'),CONVERT(varbinary(max),@Password))WHERE UserID = @UserID";
                    //    context.ExecuteQuery(strQuery, parameters);
                    //    TempData["Message"] = "AdminAlreadyExists&UpdatedSuccessfully";
                    //    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    //    return Json(new object[] { SystemEnum.MessageType.success.ToString(), "AdminAlreadyExists&UpdatedSuccessfully" });
                    //}
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "Contact Admin" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
            //return RedirectToAction(Actions.AdminList, CMMS.Pages.Controllers.Configurations);
        }

        [ActionName(Actions.DeleteAdminUser)]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult DeleteAdminUser(int userId)
        {
            using (ServiceContext adminService = new ServiceContext())
            {
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                int returnValue = adminService.Delete<Tbl_AdminUser>(userId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), "RecordDeletedSuccessfully" }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ReferencedInOtherModules" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ErrorContactAdmin" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

    }
}
