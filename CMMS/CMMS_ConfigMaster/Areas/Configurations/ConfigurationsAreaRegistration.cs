﻿using System.Web.Mvc;

namespace CMMS_ConfigMaster.Areas.Configurations
{
    public class ConfigurationsAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return CMMS.Pages.Areas.Configurations;
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
            name: "Configurations_default",
            url: CMMS.Pages.Areas.Configurations + "/{action}",
            defaults: new { controller = CMMS.Pages.Controllers.Configurations, action = "Index" });

            context.MapRoute(
                name: "Configurations",
                url: CMMS.Pages.Areas.Configurations + "/{controller}/{action}/{id}",
                defaults: new { controller = CMMS.Pages.Controllers.Configurations, action = "Index", id = UrlParameter.Optional });
        }
    }
}
