﻿$(document).ready(function () {
    CreateCalendarsPicker();
});
function KendoDDLTemplate(code, enName, arName) {
    var htmlForDDLTemplate = '';

    if (code != 'undefined' && code != null && $.trim(code) != '') { ///Three Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table'><span class='code-no'>{0}</span><span class='english'>{1}</span><span class='arabic'>{2}</span></div>",
              code == null ? '' : code,
              enName == null ? '' : enName,
              arName == null ? '' : arName
          );
    }
    else {
        ///Two Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table'><span class='english'>{0}</span><span class='arabic'>{1}</span></div>",
              enName == null ? '' : enName,
              arName == null ? '' : arName
          );
    }

    return htmlForDDLTemplate;
}

function ShowMessage(type, message, divId, fadOutTime) {
    debugger

    fadOutTime = typeof fadOutTime !== 'undefined' ? fadOutTime : 5000;

    var imagepath = imageServerPath;

    var mainDiv = ' <div class="alert alert-' + type + ' alert-dismissible" role="alert">';
    mainDiv = mainDiv + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    mainDiv = mainDiv + ('<img src="' + imagepath + '" title="' + type + '"/>').replace("#type#", type) + '&nbsp;' + message;

    mainDiv = mainDiv + '</div>';

    $(divId).html(mainDiv);
    $(divId).slideDown('slow');

    if (type == dangerType && fadOutTime == 5000) {
        fadOutTime = 6000;
    }
    window.scrollTo(0, 0);
   setTimeout(function () { $(divId).slideUp('slow'); $(divId).html(''); }, fadOutTime);

}


function CreateCalendarsPicker() {

    var myculture = $('#hiddenDateCulture').val();
    if (myculture != 'undefined') {
        $(".calendarsPicker").each(function () {
            if ($(this).has("maxdate")) {
                if (!$(this).attr("readonly")) {
                    $(this).calendarsPicker({
                        calendar: $.calendars.instance(myculture),
                        //onClose: calendarsPickerOnClose,
                        onSelect: function (date) { CalculateTargetDate(date[0].formatDate('yyyy/mm/dd'), $(this).attr("id")) },
                        dateFormat: $("#hiddenDefaultDateFormat").val().toLowerCase(),
                        maxDate: $(this).attr("maxdate")
                    });
                }
            }
            else {
                $(this).calendarsPicker({
                    calendar: $.calendars.instance(myculture),
                    //onClose: calendarsPickerOnClose,
                    // onSelect: function (date) { CalculateTargetDate(); },
                    dateFormat: $("#hiddenDefaultDateFormat").val().toLowerCase()
                });
            }


        });

        $(".calendarsPicker").attr("maxlength", $("#hiddenDefaultDateFormat").val().length)// Maxlength
        $(".calendarsPicker").each(function () {
            if (!$(this).next().next().hasClass("date-conversation")) {
                $(this).parent().append('<span id="ApprovalDate" class="k-button date-conversation k-state-disabled" style="" type="text" readonly="ReadOnly" placeholder="yyy/mm/dd"/>');
                $(this).bind("change", function () {
                    DateConversation($(this).val(), this);
                });
                setInterval('$(".calendarsPicker").trigger("change");', 500);
                //$(".calendarsPicker").trigger("change");
            }
        });

        $(".calendarsPicker").each(function () {
            $(this).on('focusout', function () {
                if ($(this).val() != "") {

                    var a = moment($(this).val(), 'YYYY/MM/DD', true).isValid();
                    //var a = moment.lang('ar_SA')($('#CR_ExpiryDate').val()).format('YYYY/MM/DD').isValid();
                    if (!a) {
                        $(this).val("");
                    }
                }
            });
        });

    }
}


// set converted date
function DateConversation(dates, calendarPiker) {

    try {

        if ($(calendarPiker).val().length > 0) {

            var culture = $('#hiddenDateCulture').val() != 'ummalqura' ? 'ummalqura' : 'gregorian';
            var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
            dates = new Date(dates);
            var calendar = $.calendars.instance($('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian');
            var date = calendar.parseDate(format, dates.getFullYear() + "/" + Number(dates.getMonth() + 1) + "/" + dates.getDate());

            calendar = $.calendars.instance(culture);
            date = calendar.fromJD(date.toJD());

            date = calendar.formatDate(format, date);
            $(calendarPiker).parent().find(".date-conversation").html(date);
        }
        else {
            $(calendarPiker).parent().find(".date-conversation").html("");
        }

    }
    catch (ex) {
    }
}
function GetCultureDateWithFormant(objectdate) {
    if (objectdate != undefined && objectdate != null && objectdate != 'Invalid Date') {
        var culture = $('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian';
        var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
        var date = objectdate;
        var calendar = $.calendars.instance('gregorian');
        var date = calendar.parseDate(format, date.getFullYear() + '/' + Number(date.getMonth() + 1) + '/' + date.getDate());
        calendar = $.calendars.instance(culture);
        date = calendar.fromJD(date.toJD());
        date = calendar.formatDate(format, date);
        return date;
    }
    else {
        return "";
    }
}

function DateTemplate(date) {

    if (date != undefined && date != null) {
        try {
            if (date.indexOf('/Date(') > -1) {
                date = new Date(parseInt(date.substr(6)));
            }
        } catch (err) { //do not throw err here
        }
        return GetCultureDateWithFormant(date);
    }
    return "";
}

function DateTemplateWithTime(date) {


    if (date != undefined && date != null) {
        try {
            if (date.indexOf('/Date(') > -1) {
                date = new Date(parseInt(date.substr(6)));
            }
        } catch (err) { //do not throw err here
        }
        var a = GetCultureDateWithFormant(date);

        var b = formatAMPM(date)
        return a + " " + b;
    }
    return "";
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function SetJsondate(jsondate) {
    var culture = $('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian';
    var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
    var date;
    if (jsondate != null && jsondate.toString().indexOf("Date") != -1) {
        date = eval(jsondate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
    }
    else {
        date = eval(jsondate);
    }
    //var date = eval(jsondate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
    var calendar = $.calendars.instance('gregorian');
    var date = calendar.parseDate(format, date.getFullYear() + '/' + Number(date.getMonth() + 1) + '/' + date.getDate());
    calendar = $.calendars.instance(culture);
    date = calendar.fromJD(date.toJD());
    date = calendar.formatDate(format, date);
    //alert(date);
    //$('#formatDate').val(calendar.formatDate(formats[$('#formatFormat').val()], date));
    return date;
}
//------------------------------------------------------End Date Picker--------------------------------------//

$(".allowIntegerOnly").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }

});

$(".allowNumberOnly").keydown(function (e) {
    // allow: backspace, delete, tab, escape, enter and .
    //var arr = [46, 8, 9, 27, 13, 110, 190];
    var arr = [46, 8, 9, 27, 13];

    //if ($(this).val().indexOf(".") >= 0) {
    //    arr = [46, 8, 9, 27, 13]
    //}

    if ($.inArray(e.keyCode, arr) !== -1 ||
        // allow: ctrl+a, command+a
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }

    // ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }

});