﻿using CMMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMMS_ConfigMaster.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public JsonResult SelectTheme(string theme)
        {
            if (!string.IsNullOrEmpty(theme))
            {
                ProjectSession.UserTheme = theme;
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
