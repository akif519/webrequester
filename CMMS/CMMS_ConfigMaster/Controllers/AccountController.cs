﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.ConfigMaster;
using System.Web.Security;
using Kendo.Mvc.UI;
using CMMS.Controllers;

namespace CMMS_ConfigMaster.Controllers
{
    //[Authorize]
    //[InitializeSimpleMembership]
    public class AccountController : BaseController
    {
        //
        // GET: /Account/Login

        [HttpGet]
        [ActionName(Actions.Login)]
        public ActionResult Login()
        {
            return View(CMMS.Pages.Views.Login);
        }

        //
        // POST: /Account/Login

        //[AllowAnonymous]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ActionName(Actions.Login)]
        public ActionResult Login(string username, string password, bool isRememberMe = false, bool isAlreadyChecked = false, [DataSourceRequest]DataSourceRequest request = null)
        {
            
            string message = string.Empty;
            string errMessage = string.Empty;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                Tbl_AdminUser objAdminUser = AdminUserData.GetLoginAccount(username, password).FirstOrDefault();

                if (objAdminUser != null)
                {
                    Session["UserID"] = Convert.ToString(objAdminUser.UserID);
                    ProjectSession.SessionID = Convert.ToInt32(Session["UserID"].ToString());
                    Session["UserName"] = Convert.ToString(objAdminUser.UserName);

                    HttpCookie cookie = new HttpCookie("UserLogin");
                    if (Convert.ToBoolean(isRememberMe))
                    {
                        cookie.Expires = DateTime.Now.AddDays(ProjectConfiguration.CookiesValidity);
                    }
                    else
                    {
                        cookie.Expires = DateTime.Now.AddDays(-1);
                    }

                    cookie.Values.Add("UserName", username.Trim());
                    cookie.Values.Add("Password", password);
                    //cookie.Values.Add("clientID", Convert.ToString(clientId));
                    cookie.Values.Add("IsRememberMe", isRememberMe.ToString());
                    Response.Cookies.Add(cookie);

                    return RedirectToAction(Actions.ClientList, CMMS.Pages.Controllers.Configurations);
                }
                else
                {

                    ViewBag.InvalidCredentials = "UserName or Password is Invalid";
                    return View("Login");

                }
            }
            else
            {
                return View("Login");
            }
            
        }
        [HttpGet]
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Clear();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction("Login","Account");
        }
        

       
       
    }
}
