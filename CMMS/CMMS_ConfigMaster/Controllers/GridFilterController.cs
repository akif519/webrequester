﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Controllers
{
    /// <summary>
    /// Grid Filter
    /// </summary>
    public class GridFilterController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Filters the multiple.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterMultiple()
        {
            Sector sectorSearch = new Sector();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(sectorSearch).Select(c => new
                  {
                      SectorID = c.SectorID,
                      SectorName = c.SectorName
                  });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the status.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterStatus()
        {
            Sector sectorSearch = new Sector();

            DapperContext objContext = new DapperContext();

            List<L2> lstL2 = new List<L2>();

            IEnumerable<SelectListItem> status = Enum.GetValues(typeof(SystemEnum.Status)).Cast<SystemEnum.Status>().Select(x => new SelectListItem()
            {
                Text = SystemEnum.GetEnumName(typeof(SystemEnum.Status), (int)x),
                Value = ((int)x).ToString()
            }).ToList();

            foreach (var lst in status)
            {
                L2 objl2 = new L2();
                objl2.Status = ConvertTo.Integer(lst.Value);
                lstL2.Add(objl2);
            }

            ////var result = Enum.GetValues(typeof(SystemEnum.Status)).Cast<SystemEnum.Status>().Select(x => new SelectListItem()
            ////{
            ////    Text = SystemEnum.GetEnumName(typeof(SystemEnum.Status), (int)x),
            ////    TimeZone = ((int)x).ToString()
            ////});
            return Json(lstL2, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the organization.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterOrganisation()
        {
            L1 objOrganisation = new L1();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objOrganisation).Select(c => new
            {
                L1ID = c.L1ID,
                L1No = c.L1No
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the sector.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSector()
        {
            Sector sectorSearch = new Sector();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(sectorSearch).Select(c => new
            {
                SectorID = c.SectorID,
                SectorCode = c.SectorCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the name of the sector by.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSectorByName()
        {
            Sector sectorSearch = new Sector();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(sectorSearch).Select(c => new
            {
                SectorID = c.SectorID,
                SectorName = c.SectorName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the site class.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSiteClass()
        {
            L2Classe objSearch = new L2Classe();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                L2ClassID = c.L2ClassID,
                L2ClassCode = c.L2ClassCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the site.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSite()
        {
            L2 objSearch = new L2();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                L2ID = c.L2ID,
                L2Code = c.L2Code
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the area.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterArea()
        {
            L3 objSearch = new L3();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                L3ID = c.L3ID,
                L3No = c.L3No
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the zone.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterZone()
        {
            L4 objSearch = new L4();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                L4ID = c.L4ID,
                L4No = c.L4No
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the building.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterBuilding()
        {
            L5 objSearch = new L5();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                L5ID = c.L5ID,
                L5No = c.L5No
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the maintenance division.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterMaintenanceDivision()
        {
            MainenanceDivision objSearch = new MainenanceDivision();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                MaintDivisionID = c.MaintDivisionID,
                MaintDivisionCode = c.MaintDivisionCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the maintenance department.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterMaintenanceDepartment()
        {
            MaintenanceDepartment objSearch = new MaintenanceDepartment();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                MaintDeptID = c.MaintDeptID,
                MaintDeptCode = c.MaintDeptCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the maintenance sub department.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterMaintenanceSubDepartment()
        {
            MaintSubDept objSearch = new MaintSubDept();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                MaintSubDeptID = c.MaintSubDeptID,
                MaintSubDeptCode = c.MaintSubDeptCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the cost center.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterCostCenter()
        {
            CostCenter objSearch = new CostCenter();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                CostCenterId = c.CostCenterId,
                CostCenterNo = c.CostCenterNo
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the sub store.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSubStore()
        {
            Substore objSearch = new Substore();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                SubStoreID = c.SubStoreID,
                SubStoreCode = c.SubStoreCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the type of the store item.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterStoreItemType()
        {
            Attrgroup objSearch = new Attrgroup();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AttrID = c.AttrID,
                AttrName = c.AttrName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the type of the store item sub.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterStoreItemSubType()
        {
            Attrsubgroup objSearch = new Attrsubgroup();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AttrSubID = c.AttrSubID,
                AttrSubName = c.AttrSubName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the name of the approval level.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterIRApprovalLevelName()
        {
            IRApprovalLevel objSearch = new IRApprovalLevel();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                IRApprovalLevelId = c.IRApprovalLevelId,
                LevelName = c.LevelName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the approval level no.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterIRApprovalLevelNo()
        {
            IRApprovalLevel objSearch = new IRApprovalLevel();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                IRApprovalLevelId = c.IRApprovalLevelId,
                LevelNo = c.LevelNo
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the status.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterIRStatus()
        {
            Mr_authorisation_statu objSearch = new Mr_authorisation_statu();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                Auth_status_id = c.Auth_status_id,
                auth_status_desc = c.Auth_status_desc
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the Request status.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterReqStatus()
        {
            Material_req_statu objSearch = new Material_req_statu();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                Mr_status_id = c.Mr_status_id,
                status_desc = c.Status_desc
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the name of the approval level.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterPRApprovalLevelName()
        {
            PRApprovalLevel objSearch = new PRApprovalLevel();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                PRApprovalLevelId = c.PRApprovalLevelId,
                LevelName = c.LevelName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the approval level no.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterPRApprovalLevelNo()
        {
            PRApprovalLevel objSearch = new PRApprovalLevel();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                PRApprovalLevelId = c.PRApprovalLevelId,
                LevelNo = c.LevelNo
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the status.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterPRStatus()
        {
            Pr_authorisation_statu objSearch = new Pr_authorisation_statu();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                Auth_status_id = c.Auth_status_id,
                auth_status_desc = c.Auth_status_desc
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the asset category.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterAssetCategory()
        {
            Assetcategorie objSearch = new Assetcategorie();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AssetCatID = c.AssetCatID,
                AssetCatCode = c.AssetCatCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the asset sub category.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterAssetSubCategory()
        {
            AssetSubCategorie objSearch = new AssetSubCategorie();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AssetSubCatID = c.AssetSubCatID,
                AssetSubCatCode = c.AssetSubCatCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the asset class.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterAssetClass()
        {
            AssetClasse objSearch = new AssetClasse();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AssetClassID = c.AssetClassID,
                AssetClassCode = c.AssetClassCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the asset condition.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterAssetCondition()
        {
            Assetcondition objSearch = new Assetcondition();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                AssetConditionID = c.AssetConditionID,
                AssetConditionCode = c.AssetConditionCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the cleaning element.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterCleaningElement(int id)
        {
            Cleaningelement objSearch = new Cleaningelement();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    AssetCatID = c.CleaningElementId,
                    Element = c.Element
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    AssetCatID = c.CleaningElementId,
                    AltElement = c.AltElement
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the tool category.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterToolCategory()
        {
            ToolCategory objSearch = new ToolCategory();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                ToolCategoryID = c.ToolCategoryID,
                ToolCategoryCode = c.ToolCategoryCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the tool sub category.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterToolSubCategory()
        {
            ToolSubCategory objSearch = new ToolSubCategory();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                ToolSubCategoryID = c.ToolSubCategoryID,
                ToolSubCategoryCode = c.ToolSubCategoryCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #region "Employee"

        /// <summary>
        /// Filters the employee category.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterEmpCategory(int id)
        {
            Employeecategorie objSearch = new Employeecategorie();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    CategoryID = c.CategoryID,
                    CategoryName = c.CategoryName
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    CategoryID = c.CategoryID,
                    AltCategoryName = c.AltCategoryName
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the employee status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterEmpStatus(int id)
        {
            EmployeeStatuse objSearch = new EmployeeStatuse();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    EmployeeStatusId = c.EmployeeStatusId,
                    EmployeeStatus = c.EmployeeStatus
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    EmployeeStatusId = c.EmployeeStatusId,
                    EmployeeAltStatus = c.EmployeeAltStatus
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        /// <summary>
        /// Filters the location.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterLocation()
        {
            Location objSearch = new Location();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                LocationID = c.LocationID,
                LocationNo = c.LocationNo
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the failure.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterFailurecause()
        {
            Failurecause objSearch = new Failurecause();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                FailureCauseID = c.FailureCauseID,
                FailureCauseCode = c.FailureCauseCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the group.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterMaintGroup()
        {
            MaintGroup objSearch = new MaintGroup();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                GroupID = c.GroupID,
                GroupCode = c.GroupCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the asset status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterAssetStatus(int id)
        {
            AssetStatus objSearch = new AssetStatus();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    AssetStatusID = c.AssetStatusID,
                    AssetStatusDesc = c.AssetStatusDesc
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    AssetStatusID = c.AssetStatusID,
                    AltAssetStatusDesc = c.AltAssetStatusDesc
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #region "Job Request"

        /// <summary>
        /// Filters the job status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterJobStatus(int id)
        {
            Workstatu objSearch = new Workstatu();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkStatusID = c.WorkStatusID,
                    JobStatus = c.WorkStatus
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (id == 2)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkStatusID = c.WorkStatusID,
                    AltWorkStatus = c.AltWorkStatus
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkStatusID = c.WorkStatusID,
                    WorkStatus = c.WorkStatus
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the type of the job.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterJobType(int id)
        {
            Worktype objSearch = new Worktype();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkTypeID = c.WorkTypeID,
                    WorkTypeDescription = c.WorkTypeDescription
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkTypeID = c.WorkTypeID,
                    AltWorkTypeDescription = c.AltWorkTypeDescription
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the job trade.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterJobTrade(int id)
        {
            Worktrade objSearch = new Worktrade();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkTradeID = c.WorkTradeID,
                    WorkTrade = c.WorkTrade
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkTradeID = c.WorkTradeID,
                    AltWorkTrade = c.AltWorkTrade
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the job priority.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterJobPriority(int id)
        {
            Workpriority objSearch = new Workpriority();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkPriorityID = c.WorkPriorityID,
                    WorkPriorityName = c.WorkPriority
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (id == 2)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkPriorityID = c.WorkPriorityID,
                    AltWorkPriority = c.AltWorkPriority
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    WorkPriorityID = c.WorkPriorityID,
                    WorkPriority = c.WorkPriority
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the name of the employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterEmployeeName(int id)
        {
            employees objSearch = new employees();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    EmployeeID = c.EmployeeID,
                    Name = c.Name
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else if (id == 2)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    EmployeeID = c.EmployeeID,
                    AltName = c.AltName
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    EmployeeID = c.EmployeeID,
                    AssignTo = c.Name
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        /// <summary>
        /// Filters the location type code.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterLocationTypeCode()
        {
            LocationType objSearch = new LocationType();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                LocationTypeID = c.LocationTypeID,
                LocationTypeCode = c.LocationTypeCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the criticality.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterCriticality()
        {
            Criticalities objSearch = new Criticalities();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                Id = c.Id,
                Criticality = c.Criticality
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the pm check list.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterPMCheckList()
        {
            PMCheckList objSearch = new PMCheckList();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                ChecklistID = c.ChecklistID,
                ChecklistNo = c.ChecklistNo
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the store card.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterStore()
        {
            Substore objSearch = new Substore();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(objSearch).Select(c => new
            {
                SubStoreID = c.SubStoreID,
                SubStoreCode = c.SubStoreCode
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Filters the sub store status.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterSubStoreStatus(int id)
        {
            SubStoreStatu objSearch = new SubStoreStatu();

            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    SubStoreStatusId = c.SubStoreStatusId,
                    StatusName = c.StatusName
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                {
                    SubStoreStatusId = c.SubStoreStatusId,
                    StatusAltName = c.StatusAltName
                });
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the type of Transaction.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterTransactionsType(int id)
        {
            TransactionTypes objSearch = new TransactionTypes();
            DapperContext objContext = new DapperContext();

            if (id == 1)
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                 {
                     TransactionID = c.TransactionTypeID,
                     TypeName = c.TypeName
                 });

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objContext.SearchAll(objSearch).Select(c => new
                 {
                     TransactionID = c.TransactionTypeID,
                     AltTypeName = c.AltTypeName
                 });

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Filters the multiple.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterUserName()
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            Tbl_AdminUser usernameSearch = new Tbl_AdminUser();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(usernameSearch).Select(c => new
            {
                UserID = c.UserID,
                UserName = c.UserName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Filters the multiple.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterFirstName()
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            Tbl_AdminUser firstNameSearch = new Tbl_AdminUser();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(firstNameSearch).Select(c => new
            {
                UserID = c.UserID,
                FirstName = c.FirstName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Filters the multiple.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilterLastName()
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            Tbl_AdminUser lastnameSearch = new Tbl_AdminUser();

            DapperContext objContext = new DapperContext();

            var result = objContext.SearchAll(lastnameSearch).Select(c => new
            {
                UserID = c.UserID,
                LastName = c.LastName
            });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}