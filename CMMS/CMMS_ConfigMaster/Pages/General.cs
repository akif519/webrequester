﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    /// General Method
    /// </summary>
    public class General
    {
        #region Property and Const

        /// <summary>
        /// Gets Grid page size 
        /// </summary>
        public static int[] PageSizes
        {
            get
            {
                return new int[] { 10, 20, 50, 100 };
            }
        }

        #endregion
    }
}