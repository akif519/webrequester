﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    ///  Declare Areas names
    /// </summary>
    public class Areas
    {
        #region '"Configurations"

        /// <summary>
        /// The configurations
        /// </summary>
        public const string Configurations = "configurations";

        #endregion

        #region '"Management"

        /// <summary>
        /// The Management
        /// </summary>
        public const string Management = "management";

        #endregion

        #region "Transaction"

        /// <summary>
        /// The Management
        /// </summary>
        public const string Transaction = "transaction";

        #endregion

        #region "Help"

        /// <summary>
        /// The Help
        /// </summary>
        public const string Help = "Help";

        #endregion

        #region '"Reports"

        /// <summary>
        /// The Management
        /// </summary>
        public const string Report = "reports";

        #endregion

        #region "ManageReportSetup"

        /// <summary>
        /// The manage report setup
        /// </summary>
        public const string ManageReportSetup = "managereportsetup";

        #endregion
     }
}