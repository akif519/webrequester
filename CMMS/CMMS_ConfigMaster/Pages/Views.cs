﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    /// Declare view names
    /// </summary>
    public class Views
    {
        #region "Account"

        /// <summary>
        /// The login
        /// </summary>
        public const string Login = "login";

        /// <summary>
        /// The access denied
        /// </summary>
        public const string AccessDenied = "accessdenied";

        /// <summary>
        /// The Error
        /// </summary>
        public const string DisplayError = "displayerror";

        /// <summary>
        /// The page not found
        /// </summary>
        public const string PageNotFound = "pagenotfound";

        /// <summary>
        /// The error
        /// </summary>
        public const string Error = "error";

        #endregion

        #region "Home"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string Dashboard = "dashboard";

        #endregion

        #region "Configurations"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string SectorList = "sectorlist";

        /// <summary>
        /// The area
        /// </summary>
        public const string Area = "area";

        /// <summary>
        /// The Org
        /// </summary>
        public const string Organisation = "organisation";

        /// <summary>
        /// The failure code list
        /// </summary>
        public const string FailureCauseList = "failurecauselist";

        /// <summary>
        /// The specification list
        /// </summary>
        public const string SpecificationList = "Specificationlist";

        /// <summary>
        /// The asset categories list
        /// </summary>
        public const string AssetCategoriesList = "assetcategorieslist";

        /// <summary>
        /// The asset sub categories list
        /// </summary>
        public const string AssetSubCategoriesList = "assetsubcategorieslist";

        /// <summary>
        /// The asset conditions list
        /// </summary>
        public const string AssetConditionsList = "assetconditionslist";

        /// <summary>
        /// The asset class list
        /// </summary>
        public const string AssetClassList = "assetclasslist";

        /// <summary>
        /// The asset status list
        /// </summary>
        public const string AssetStatusList = "assetstatuslist";

        /// <summary>
        /// The criticality list
        /// </summary>
        public const string CriticalityList = "criticalitylist";

        /// <summary>
        /// The warranty contract list
        /// </summary>
        public const string WarrantyContractList = "warrantycontractlist";

        /// <summary>
        /// The maintenance division
        /// </summary>
        public const string MaintenanceDivision = "maintenancedivision";

        /// <summary>
        /// The maintenance department
        /// </summary>
        public const string MaintenanceDepartment = "maintenancedepartment";

        /// <summary>
        /// The maintenance sub department
        /// </summary>
        public const string MaintenanceSubDepartment = "maintenancesubdepartment";

        /// <summary>
        /// The suppliers
        /// </summary>
        public const string Suppliers = "suppliers";

        /// <summary>
        /// The maintenance group
        /// </summary>
        public const string MaintenanceGroup = "maintenancegroup";

        /// <summary>
        /// The zone
        /// </summary>
        public const string Zone = "zone";

        /// <summary>
        /// The building
        /// </summary>
        public const string Building = "building";

        /// <summary>
        /// The cost center
        /// </summary>
        public const string CostCenter = "costcenter";

        /// <summary>
        /// The employee category
        /// </summary>
        public const string EmployeeCategory = "employeecategory";

        /// <summary>
        /// The site class
        /// </summary>
        public const string SiteClass = "siteclass";

        /// <summary>
        /// The site
        /// </summary>
        public const string Site = "site";

        /// <summary>
        /// The employee status
        /// </summary>
        public const string EmployeeStatus = "employeestatus";

        /// <summary>
        /// The item type
        /// </summary>
        public const string ItemType = "itemtype";

        /// <summary>
        /// The item sub type
        /// </summary>
        public const string ItemSubType = "itemsubtype";

        /// <summary>
        /// The stores
        /// </summary>
        public const string Stores = "stores";

        /// <summary>
        /// The item section
        /// </summary>
        public const string ItemSection = "itemsection";

        /// <summary>
        /// The job priority
        /// </summary>
        public const string JobPriority = "jobpriority";

        /// <summary>
        /// The job status
        /// </summary>
        public const string JobStatus = "jobstatus";

        /// <summary>
        /// The job type
        /// </summary>
        public const string JobType = "jobtype";

        /// <summary>
        /// The job trade
        /// </summary>
        public const string JobTrade = "jobtrade";

        /// <summary>
        /// The item requisition status
        /// </summary>
        public const string IRStatus = "irstatus";

        /// <summary>
        /// The item requisition request status
        /// </summary>
        public const string IRReqStatus = "irreqstatus";

        /// <summary>
        /// The item requisition approval levels
        /// </summary>
        public const string IRApprovalLevels = "irapprovallevels";

        /// <summary>
        /// The item requisition work flow
        /// </summary>
        public const string IRWorkFlow = "irworkflow";

        /// <summary>
        /// The Purchase requisition list
        /// </summary>
        public const string PRStatusList = "prstatuslist";

        /// <summary>
        /// The Purchase requisition Authorization Status list
        /// </summary>
        public const string PRAuthStatusList = "prauthstatuslist";

        /// <summary>
        /// The Purchase requisition Approval Level list
        /// </summary>
        public const string PRApprovalLevel = "prapprovallevel";

        /// <summary>
        /// The purchase requisition work flow
        /// </summary>
        public const string PRWorkFlow = "prworkflow";

        /// <summary>
        /// The cleaning group
        /// </summary>
        public const string CleaningGroup = "cleaninggroup";

        /// <summary>
        /// The cleaning weightage
        /// </summary>
        public const string CleaningWeightage = "cleaningweightage";

        /// <summary>
        /// The cleaning classification
        /// </summary>
        public const string CleaningClassification = "cleaningclassification";

        /// <summary>
        /// The job plan
        /// </summary>
        public const string JobPlan = "jobplan";

        /// <summary>
        /// The safety instruction
        /// </summary>
        public const string SafetyInstruction = "safetyinstruction";

        /// <summary>
        /// The email server configuration
        /// </summary>
        public const string EmailServerConfiguration = "emailserverconfiguration";

        /// <summary>
        /// The message server configuration
        /// </summary>
        public const string SMSServerConfiguration = "smsconfiguration";

        /// <summary>
        /// The account code
        /// </summary>
        public const string AccountCode = "accountcode";

        /// <summary>
        /// The Email Templates
        /// </summary>
        public const string EmailTemplates = "emailtemplates";

        /// <summary>
        /// The Email Rules
        /// </summary>
        public const string EmailRules = "emailrules";

        /// <summary>
        /// The bo m list
        /// </summary>
        public const string BoMList = "bomlist";

        /// <summary>
        /// The purchasing currency
        /// </summary>
        public const string PurchasingCurrency = "purchasingcurrency";

        /// <summary>
        /// The purchasing term of sale
        /// </summary>
        public const string PurchasingTermofSale = "purchasingtermofsale";

        /// <summary>
        /// The purchasing shipped via
        /// </summary>
        public const string PurchasingShippedVia = "purchasingshippedvia";

        /// <summary>
        /// The purchasing payment terms
        /// </summary>
        public const string PurchasingPaymentTerms = "purchasingpaymentterms";

        /// <summary>
        /// The default setups
        /// </summary>
        public const string DefaultSetups = "defaultsetups";

        /// <summary>
        /// The tool category
        /// </summary>
        public const string ToolCategory = "toolcategory";

        /// <summary>
        /// The tool sub category
        /// </summary>
        public const string ToolSubCategory = "toolsubcategory";

        /// <summary>
        /// The tool
        /// </summary>
        public const string Tool = "tool";

        /// <summary>
        /// The location type
        /// </summary>
        public const string LocationType = "locationtype";

        /// <summary>
        /// The error log
        /// </summary>
        public const string ErrorLog = "errorlog";

        /// <summary>
        /// The help list
        /// </summary>
        public const string HelpList = "helplist";

        /// <summary>
        /// The help detail
        /// </summary>
        public const string HelpDetail = "helpdetail";

        /// <summary>
        /// The general configuration
        /// </summary>
        public const string GeneralConfiguration = "generalconfiguration";

        /// <summary>
        /// The holiday setting
        /// </summary>
        public const string HolidaySetting = "holidaysetting";

        #endregion

        #region "Management"

        #region "User List"
        /// <summary>
        /// The user list
        /// </summary>
        public const string UserList = "UsersList";

        /// <summary>
        /// The Employee
        /// </summary>
        public const string Employee = "Employee";

        #endregion

        /// <summary>
        /// The active session
        /// </summary>
        public const string ActiveSession = "activesession";

        /// <summary>
        /// The login history
        /// </summary>
        public const string LoginHistory = "loginhistory";

        /// <summary>
        /// The language translation
        /// </summary>
        public const string LanguageTranslation = "languagetranslation";

        #region "User Group"

        /// <summary>
        /// The User Group List
        /// </summary>
        public const string UserGroupList = "UserGroupList";
        #endregion

        #region "Permissions"

        /// <summary>
        /// Permission View
        /// </summary>
        public const string Permissions = "Permissions";

        #endregion

        #region "Database Audit"

        /// <summary>
        /// Database Audit Trail
        /// </summary>
        public const string DatabaseAuditTrail = "databaseaudittrail";

        #endregion

        #endregion

        #region "Transaction"

        /// <summary>
        /// The asset list
        /// </summary>
        public const string AssetList = "assetlist";

        /// <summary>
        /// The asset detail
        /// </summary>
        public const string AssetDetail = "assetdetail";

        /// <summary>
        /// The asset tree
        /// </summary>
        public const string AssetTree = "AssetTree";

        /// <summary>
        /// The job request detail
        /// </summary>
        public const string JobRequestList = "JobRequestList";

        /// <summary>
        /// The job request add/edit
        /// </summary>
        public const string JobRequests = "JobRequests";

        /// <summary>
        /// The job order add/edit
        /// </summary>
        public const string JobOrders = "JobOrders";

        /// <summary>
        /// The Section List 
        /// </summary>
        public const string SectionList = "SectionList";

        /// <summary>
        /// The Section Details 
        /// </summary>
        public const string SectionDetail = "SectionDetail";

        /// <summary>
        /// The pm schedule list
        /// </summary>
        public const string PMSchedule = "PMSchedule";

        /// <summary>
        /// The pm schedule by asset
        /// </summary>
        public const string PMScheduleByAsset = "PMScheduleByAsset";

        /// <summary>
        /// The cleaning schedule
        /// </summary>
        public const string CleaningSchedule = "CleaningSchedule";

        /// <summary>
        /// The cleaning schedule detail
        /// </summary>
        public const string CleaningScheduleDetail = "CleaningScheduleDetail";

        /// <summary>
        /// The pm schedule detail
        /// </summary>
        public const string PMScheduleDetail = "PMScheduleDetail";

        /// <summary>
        /// The pm schedule by asset detail
        /// </summary>
        public const string PMScheduleByAssetDetail = "PMScheduleByAssetDetail";

        /// <summary>
        /// The Job Order List
        /// </summary>
        public const string JobOrderList = "JobOrderList";

        /// <summary>
        /// The print view
        /// </summary>
        public const string PrintView = "PrintView";

        /// <summary>
        /// The CEMUL 
        /// </summary>
        public const string CEMULPage = "cemul";

        /// <summary>
        /// The PM CheckList 
        /// </summary>
        public const string PMCheckList = "pmchecklist";

        /// <summary>
        /// The pm check list for cleaning
        /// </summary>
        public const string PMCheckListForCleaning = "pmchecklistforcleaning";

        /// <summary>
        /// The PM CheckList Details
        /// </summary>
        public const string PMCheckListDetails = "pmchecklistdetails";

        /// <summary>
        /// The pm check list for cleaning details
        /// </summary>
        public const string PMCheckListForCleaningDetails = "pmchecklistforcleaningdetails";

        /// <summary>
        /// The pm jo generation
        /// </summary>
        public const string PMJoGeneration = "pmjogeneration";

        /// <summary>
        /// The Purchase Request View
        /// </summary>
        public const string PurchaseRequestDetails = "PurchaseRequestDetails";

        /// <summary>
        /// The Purchase Order View
        /// </summary>
        public const string PurchaseOrderDetails = "PurchaseOrderDetails";

        /// <summary>
        /// The purchase request list
        /// </summary>
        public const string PurchaseRequestList = "purchaserequestlist";

        /// <summary>
        /// The purchase order list
        /// </summary>
        public const string PurchaseOrderList = "purchaseorderlist";

        /// <summary>
        /// The good receipt list
        /// </summary>
        public const string GoodReceiptList = "goodreceiptlist";

        /// <summary>
        /// The good receipt detail
        /// </summary>
        public const string GoodReceiptDetail = "goodreceiptdetail";

        /// <summary>
        /// The consumables list
        /// </summary>
        public const string ConsumablesList = "consumableslist";

        /// <summary>
        /// The personal list
        /// </summary>
        public const string PersonalList = "personallist";

        /// <summary>
        /// The Items List
        /// </summary>
        public const string ItemsList = "ItemsList";

        /// <summary>
        /// The consumables detail
        /// </summary>
        public const string ConsumablesDetail = "consumablesdetail";

        /// <summary>
        /// The Item Detail
        /// </summary>
        public const string ItemDetail = "ItemDetail";

        /// <summary>
        /// The personal details
        /// </summary>
        public const string PersonalDetails = "personaldetails";

        /// <summary>
        /// The Transaction List
        /// </summary>
        public const string TransactionList = "TransactionList";

        /// <summary>
        /// Add/Edit Transaction Issue Items 
        /// </summary>
        public const string TransactionIssue = "TransactionIssue";

        /// <summary>
        /// Add/Edit Transaction Receive Items 
        /// </summary>
        public const string TransactionReceive = "TransactionReceive";

        /// <summary>
        /// Add/Edit Transaction Return Items 
        /// </summary>
        public const string TransactionReturn = "TransactionReturn";

        /// <summary>
        /// Add/Edit Transaction Adjustment Items 
        /// </summary>
        public const string TransactionAdjustment = "TransactionAdjustment";

        /// <summary>
        /// Add/Edit Transaction Transfer Items 
        /// </summary>
        public const string TransactionTransfer = "TransactionTransfer";

        /// <summary>
        /// Add/Edit Transaction Purchase Proposal
        /// </summary>
        public const string TransactionPurchaseProposal = "TransactionPurchaseProposal";

        /// <summary>
        /// The Transaction Detail
        /// </summary>
        public const string TransactionDetail = "TransactionDetail";

        public const string ClientList = "ClientList";

        public const string AdminList = "AdminList";

        #endregion

        #region "PMMeterMasterList"
        /// <summary>
        /// The PM Meter Master List
        /// </summary>
        public const string PMMeterMasterList = "PMMeterMasterList";

        /// <summary>
        /// The PM Meter Master Edit
        /// </summary>
        public const string PMMeterMasterEdit = "PMMeterMasterEdit";

        /// <summary>
        /// The PM Meter Edit
        /// </summary>
        public const string PMMeterEdit = "PMMeterEdit";

        public const string AdminUserEdit = "AdminUserEdit";

        public const string ClientDetail = "ClientDetail";
        #endregion

        #region "Reading"
        /// <summary>
        /// the Reading page
        /// </summary>
        public const string Reading = "Reading";
        #endregion

        #region "PM Meter Jo Generation"
        /// <summary>
        /// PM Meter Jo Generation
        /// </summary>
        public const string PMMeterJoGeneration = "PMMeterJoGeneration";
        #endregion

        #region "Cleaning Inspection"

        /// <summary>
        /// Cleaning Inspection Page
        /// </summary>
        public const string CleaningInspection = "CleaningInspection";

        /// <summary>
        /// Cleaning Inspection add/edit Page
        /// </summary>
        public const string CleaningInspectionEdit = "CleaningInspectionEdit";

        /// <summary>
        /// Cleaning Inspection Group Page
        /// </summary>
        public const string CleaningInspectionGroup = "CleaningInspectionGroup";

        /// <summary>
        /// Cleaning Inspection Group add/edit Page
        /// </summary>
        public const string CleaningInspectionGroupEdit = "CleaningInspectionGroupEdit";

        #endregion

        #region "Item Requisition"

        /// <summary>
        /// Item Requisition List Page
        /// </summary>
        public const string ItemRequisitionList = "ItemRequisitionList";

        /// <summary>
        /// Item Requisition List Page
        /// </summary>
        public const string ItemRequisitionDetails = "ItemRequisitionDetails";

        #endregion

        #region "Help"
        /// <summary>
        /// load Help Page
        /// </summary>
        public const string HelpPage = "HelpDetail";

        #endregion
    }
}