﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    ///  Declare Controller names
    /// </summary>
    public class Controllers
    {
        #region "Front-Side"

        /// <summary>
        /// The account
        /// </summary>
        public const string Account = "account";

        /// <summary>
        /// The home
        /// </summary>
        public const string Home = "home";

        #endregion 

        #region "Configurations"

        /// <summary>
        /// The configurations
        /// </summary>
        public const string Configurations = "configurations";

        /// <summary>
        /// The error log
        /// </summary>
        public const string ErrorLog = "errorlog"; 
        
        #endregion

        #region "Management"

        /// <summary>
        /// The Management
        /// </summary>
        public const string Management = "management";

        #endregion

        #region "Transaction"

        /// <summary>
        /// The Transaction
        /// </summary>
        public const string Transaction = "transaction";

        #endregion

        #region "Help"

        /// <summary>
        /// The Help
        /// </summary>
        public const string Help = "Help";

        #endregion

        #region "Auto Complete"

        /// <summary>
        /// The AutoComplete
        /// </summary>
        public const string AutoComplete = "AutoComplete";

        #endregion

        #region "Report"

        public const string Report = "Report";
        public const string ReportAsset = "Asset";
        public const string ReportJobOrder = "JobOrderReport";
        public const string ReportPreventive = "PreventiveReport";
        public const string ReportEmployee = "EmployeeReport";
        public const string ReportSetups = "SetupsReport";
        public const string ReportCleaning = "CleaningReport";
        public const string ReportPurchasing = "PurchasingReport";
        public const string ReportStore = "StoreReport";

        #endregion

        #region Setup Report 

        public const string SetupReport = "SetupReport";

        #endregion

        #region "ClientDetailCont"
        public const string ClientDetail = "clientdetail";

        public const string AdminUser = "adminuser";
        #endregion
    }
}