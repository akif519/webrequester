﻿using CMMS.Infrastructure;
using CMMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CMMS.Model;
using CMMS.DAL;
using System.Data;
using System.Collections.ObjectModel;

namespace CMMS.Service
{
    public class SectorService : DBExecute
    {
        public SectorService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public bool UpdateL1InL2BySectorID(int sectorID, int orgID)
        {
            string strSQl = string.Empty;
            int result = -1;

            try
            {
                strSQl = "Update L2 Set L1ID = " + orgID + " Where L2.SectorID = " + sectorID;

                using (ServiceContext objService = new ServiceContext())
                {
                    result = objService.ExecuteQuery(strSQl, new Collection<DBParameters>());
                }

                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
