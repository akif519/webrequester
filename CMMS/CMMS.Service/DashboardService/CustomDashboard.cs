﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.DashboardService
{
    public class CustomDashboard : DBExecute
    {
        #region "Constructor"

        public CustomDashboard()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        #region "Method"

        private string[] colours1 = { "#42a7ff", "#666666", "#999999", "#cccccc", "#004d38", "#033939", "#033939" };

        private string[] theme4 = { "#fcc761", "#f89021", "#da5527", "#a72221", "#d0215f", "#64217a", "#412177", "#296b80", "#0097d7", "#7abb4d", "#d1e7a6" };

        private string[] theme2 = { "#412177", "#296b80", "#0097d7", "#7abb4d", "#d1e7a6", "#fcc761", "#f89021", "#da5527", "#a72221", "#d0215f", "#64217a" };

        private string[] theme5 = { "#f89021", "#ae421e", "#a72221", "#d0215f", "#64217a", "#296b80", "#538131", "#0000e6" };

        private string[] colours = { "#578ebe", "#ce3434", "#7bd2f6", "#ffb800", "#ff8517", "#e34a00", "#d49fdd", "#c17969", "#7fc896", "#d9d86f", "#d59a8d", "#70b7c7", "#c788d2", "#e18345", "#71b586", "#0069a5", "#0098ee", "#4e9fb1", "#af6dbb", "#d06c2a" };

        private string[] theme3 = { "#578ebe", "#ce3434", "#7bd2f6", "#ffb800", "#ff8517", "#e34a00", "#d49fdd", "#c17969", "#7fc896", "#d9d86f", "#d59a8d", "#70b7c7", "#c788d2", "#e18345", "#71b586", "#0069a5", "#0098ee", "#4e9fb1", "#af6dbb", "#d06c2a" };

        #region "Job Order Type Distribution"

        public IList<PIChartModel> GetJobOrderTypeDistributionChartDetail(bool IsCentral, bool IsOutStandingReport, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<DashboardChart> list = new List<DashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.PIChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate )) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND  ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                        else
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND  ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            query += "(workorders.WorkStatusID <> 3) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND  ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                        else
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND  ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND  ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                        else
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND  ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            query += "(workorders.WorkStatusID <> 3) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND  ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                        else
                        {
                            query += "select worktype.WorkTypeDescription As CategoryEnglish,worktype.AltWorkTypeDescription As CategoryArabic,count(*)  As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND  ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) ";
                            query += "Group By worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,worktype.WorkTypeID ";
                            query += "Order By worktype.WorkTypeID";
                        }
                    }
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new PIChartModel { category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.TotalCount, Pervalue = 100 * x.TotalCount / list.Sum(d => d.TotalCount), color = colours[idx] });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Order Status"

        public IList<PIChartModel> GetJobOrderStatusChartDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate, string WorkTypeID)
        {
            try
            {
                IList<DashboardChart> list = new List<DashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.PIChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (IsCentral)
                    {
                        query += "select workstatus.WorkStatus As CategoryEnglish ,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        if (WorkTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                        }
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                        query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)))";
                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                    else
                    {
                        query += "select workstatus.WorkStatus As CategoryEnglish ,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        if (WorkTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                        }
                        query += " (( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                        query += " OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) ";
                        query += " AND (workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += " Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += " Order By workstatus.WorkStatusID";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (IsCentral)
                    {
                        query += "select workstatus.WorkStatus As CategoryEnglish ,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        if (WorkTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                        }
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                        query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";

                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                    else
                    {
                        query += "select workstatus.WorkStatus As CategoryEnglish ,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        if (WorkTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                        }
                        query += " (( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                        query += " OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";
                        query += " and (workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += " Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += " Order By workstatus.WorkStatusID";
                    }
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new PIChartModel { category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.TotalCount, Pervalue = 100 * x.TotalCount / list.Sum(d => d.TotalCount), color = colours[idx] });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "PM Job Order Status"

        public IList<PIChartModel> GetPMJobOrderStatusChartDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<DashboardChart> list = new List<DashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.PIChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute

                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "select workstatus.WorkStatus AS CategoryEnglish,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join  ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate ) AND ";
                        query += "(workorders.WorkTypeID = 2 OR workorders.WorkTypeID = 7) AND ";
                        query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                    else
                    {
                        query += "select workstatus.WorkStatus AS CategoryEnglish,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join  ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkTypeID = 2 OR workorders.WorkTypeID = 7) ";
                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "select workstatus.WorkStatus AS CategoryEnglish,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join  ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkTypeID = 2 OR workorders.WorkTypeID = 7) AND ";
                        query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                    else
                    {
                        query += "select workstatus.WorkStatus AS CategoryEnglish,workstatus.AltWorkStatus AS CategoryArabic,count(*) As TotalCount ";
                        query += "from workorders inner join  ";
                        query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkTypeID = 2 OR workorders.WorkTypeID = 7) ";
                        query += "Group By workstatus.WorkStatus,workstatus.AltWorkStatus,workstatus.WorkStatusID ";
                        query += "Order By workstatus.WorkStatusID";
                    }
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new PIChartModel { category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.TotalCount, Pervalue = 100 * x.TotalCount / list.Sum(d => d.TotalCount), color = theme3[idx] });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Priority Distribution"

        public IList<PIChartModel> GetJobPriorityDistributionChartDetail(bool IsCentral, bool IsOutStandingReport, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate, string WorkTypeID)
        {
            try
            {
                IList<DashboardChart> list = new List<DashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.PIChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute

                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between  @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between  @JOStartDate and @JOCompletedDate))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                        else
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between  @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between  @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between  @JOStartDate and @JOCompletedDate))) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between  @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between  @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between  @JOStartDate and @JOCompletedDate))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                        else
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between  @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between  @JOStartDate and @JOCompletedDate )) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between  @JOStartDate and @JOCompletedDate ))) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                        else
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";

                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                        else
                        {
                            query += "SELECT workpriority.WorkPriority AS CategoryEnglish, workpriority.AltWorkPriority  AS CategoryArabic, count(*) As TotalCount ";
                            query += "from workorders inner join  ";
                            query += "workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";
                            query += "Group By workpriority.WorkPriority, workpriority.AltWorkPriority, workpriority.WorkPriorityID ";
                            query += "Order By workpriority.WorkPriorityID";
                        }
                    }
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new PIChartModel { category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.TotalCount, Pervalue = 100 * x.TotalCount / list.Sum(d => d.TotalCount), color = theme3[idx] });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Trade Distribution"

        public IList<PIChartModel> GetJobTradeDistributionChartDetail(bool IsCentral, bool IsOutStandingReport, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate, string WorkTypeID)
        {
            try
            {
                IList<DashboardChart> list = new List<DashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.PIChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                        else
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                        else
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate))) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                    }
                }

               //if database is SQL this query will be execute
                else
                {
                    if (!IsOutStandingReport)
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                        else
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID <> 3) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                    }
                    else
                    {
                        if (!IsCentral)
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) AND ";
                            query += "(workorders.L2ID in (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                        else
                        {
                            query += "SELECT worktrade.WorkTrade  AS CategoryEnglish,worktrade.AltWorkTrade AS CategoryArabic,count(*) As TotalCount ";
                            query += "from workorders inner join ";
                            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID ";
                            query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                            query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                            query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                            if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                            {
                                query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                            }
                            if (L4ID.Length > 0)
                            {
                                query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                            }
                            if (L5ID.Length > 0)
                            {
                                query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                            }
                            query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                            if (MaintDeptID.Length > 0)
                            {
                                query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                            }
                            if (MaintSubDeptID.Length > 0)
                            {
                                query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                            }
                            query += "(workorders.WorkStatusID NOT IN (2,3,7)) AND ";
                            query += "(workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                            if (WorkTypeID.Length > 0)
                            {
                                query += "(workorders.WorkTypeID in (" + WorkTypeID + ")) AND ";
                            }
                            query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                            query += "OR (Workorders.worktypeId not in (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "'))) ";
                            query += "Group By worktrade.WorkTrade, worktrade.AltWorkTrade, worktrade.WorkTradeID ";
                            query += "Order By worktrade.WorkTradeID";
                        }
                    }
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new PIChartModel { category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.TotalCount, Pervalue = 100 * x.TotalCount / list.Sum(d => d.TotalCount), color = theme3[idx] });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "PM Compliance"

        public IList<BARChartModel> GetPMComplianceChartDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTradeID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<BARChartModel> list = new List<BARChartModel>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                var viewModel = new List<CMMS.Model.BARChartModel>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle this query will be execute
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "select ";
                        query += "(ROW_NUMBER() Over (Order by WorkTypeID)) As [Year],";
                        query += " (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND  (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ") ColA";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColB";

                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColC";


                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColD";

                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders  inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColE";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColF ";
                        query += " from dbo.workorders  w";
                        query += " where ROWNUM = 1";
                    }
                    else
                    {
                        query += "select ";
                        query += "(ROW_NUMBER() Over (Order by WorkTypeID)) As [Year],";
                        query += " (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkTypeID in (2,7))";
                        query += ")ColA ";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColB";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColC";


                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColD";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders  inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColE ";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColF ";
                        query += " from workorders  w";
                        query += " where ROWNUM = 1";
                    }
                }
                //if database is MYSQL this query will be execute
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "select ";
                        query += "@rownum := @rownum + 1 AS[Year],";
                        query += " (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ") ColA";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColB";

                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColC";


                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColD";

                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColE";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")ColF ";
                        query += " from dbo.workorders  w";
                        query += " ,(SELECT @rownum := 0) r  Order by w.WorkTypeID";
                        query += " limit 1";
                    }
                    else
                    {
                        query += "select ";
                        query += " @rownum := @rownum + 1 As [Year],";
                        query += " (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkTypeID in (2,7))";
                        query += ")ColA ";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColB";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColC";


                        query += ",(select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColD";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColE ";

                        query += ", (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")ColF ";
                        query += " from workorders  w";
                        query += " ,(SELECT @rownum := 0) r  Order by w.WorkTypeID";
                        query += "  limit 1";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "select TOP 1 ";
                        query += "convert(varchar(10),ROW_NUMBER() Over (Order by WorkTypeID)) As [Year],";
                        query += "ColA = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";

                        query += ",ColB = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";

                        query += ",ColC = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";


                        query += ",ColD = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";

                        query += ",ColE = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";

                        query += ",ColF = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) AND ";
                        query += "(workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += ")";
                        query += "from dbo.workorders  w";
                    }
                    else
                    {
                        query += "select TOP 1 ";
                        query += "convert(varchar(10),ROW_NUMBER() Over (Order by WorkTypeID)) As [Year],";
                        query += "ColA = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkTypeID in (2,7))";
                        query += ")";

                        query += ",ColB = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")";

                        query += ",ColC = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID = 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")";


                        query += ",ColD = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")";

                        query += ",ColE = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
                        query += "(workorders.WorkStatusID != 2) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")";

                        query += ",ColF = (select cast(count(1) as decimal(10,2)) from dbo.workorders workorders inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "where  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(workorders.WorkStatusID = 3) AND ";
                        query += "(workorders.WorkTypeID in (2,7)) ";
                        query += ")";
                        query += "from workorders  w";
                    }
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<BARChartModel>(query, parameters, 1);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new BARChartModel { Year = x.Year.ToString(), ColA = x.ColA, ColB = x.ColB, ColC = x.ColC, ColD = x.ColD, ColE = x.ColE, ColF = x.ColF });
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "No Of Job Order By Type"

        public IList<ColumnChartModel> GetNoOfJobOrderByTypeDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTradeID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<NoOfJobOrderByTypeDashboardChart> list = new List<NoOfJobOrderByTypeDashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.ColumnChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is ORACLE this query will be execute
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.DateReceived, 'Mon')||' - '||to_char(workorders.DateReceived,'yyyy') AS MonthYear ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        //query += ", to_char(workorders.DateReceived, 'mm') AS DateMonth, to_char(workorders.DateReceived, 'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.DateReceived,to_char(workorders.DateReceived,'mm'), to_char(workorders.DateReceived,'yyyy')";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.PMTarCompDate, 'Mon')||' - '||to_char(workorders.PMTarCompDate,'yyyy') AS MonthYear ";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", to_char(workorders.PMTarCompDate,'mm') AS DateMonth, to_char(workorders.PMTarCompDate,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate,to_char(workorders.PMTarCompDate,'mm'),to_char(workorders.PMTarCompDate,'yyyy') ";
                        query += ")";
                        query += ")";
                        query += "  JobTrending ORDER BY MonthYear asc";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.DateReceived, 'Mon')||' - '||to_char(workorders.DateReceived,'yyyy') AS MonthYear ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        //query += ", to_char(workorders.DateReceived, 'mm') AS DateMonth, to_char(workorders.DateReceived,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.DateReceived,to_char(workorders.DateReceived,'mm'),to_char(workorders.DateReceived,'yyyy') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.PMTarCompDate, 'Mon')||' - '||to_char(workorders.PMTarCompDate,'yyyy') AS MonthYear ";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", to_char(workorders.PMTarCompDate,'mm') AS DateMonth, to_char(workorders.PMTarCompDate,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate )) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate,to_char(workorders.PMTarCompDate,'mm'),to_char(workorders.PMTarCompDate,'yyyy') ";
                        query += ")";
                        query += ")";
                        query += " JobTrending ORDER BY MonthYear asc";
                    }
                }
                //if database is MYSQL this query will be execute
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CONCAT(DATE_FORMAT(workorders.DateReceived,'%b'), ' - ', DATE_FORMAT(workorders.DateReceived,'%Y'))  as MonthYear ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,DATE_FORMAT(workorders.DateReceived,'%m') ,DATE_FORMAT(workorders.DateReceived,'%Y') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CONCAT(DATE_FORMAT(workorders.PMTarCompDate,'%b'), ' - ', DATE_FORMAT(workorders.PMTarCompDate,'%Y'))  as MonthYear ";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,DATE_FORMAT(workorders.PMTarCompDate,'%m') ,DATE_FORMAT(workorders.PMTarCompDate,'%Y') ";
                        query += ")";
                        query += ")";
                        query += " AS JobTrending ORDER BY MonthYear asc";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CONCAT(DATE_FORMAT(workorders.DateReceived,'%b'), ' - ', DATE_FORMAT(workorders.DateReceived,'%Y'))  as MonthYear ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        //query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,DATE_FORMAT(workorders.DateReceived,'%m'),DATE_FORMAT(workorders.DateReceived,'%Y') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CONCAT(DATE_FORMAT(workorders.PMTarCompDate,'%b'), ' - ', DATE_FORMAT(workorders.PMTarCompDate,'%Y'))  as MonthYear ";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,DATE_FORMAT(workorders.PMTarCompDate,'%m') ,DATE_FORMAT(workorders.PMTarCompDate,'%Y') ";
                        query += ")";
                        query += ")";
                        query += " JobTrending ORDER BY MonthYear asc";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.DateReceived) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.DateReceived) AS VARCHAR(4)) AS [MonthYear] ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.DateReceived), YEAR(workorders.DateReceived)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.PMTarCompDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.PMTarCompDate) AS VARCHAR(4)) AS [MonthYear] ";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                       // query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + "))";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.PMTarCompDate), YEAR(workorders.PMTarCompDate)";
                        query += ")";
                        query += ")";
                        query += " AS JobTrending ORDER BY MonthYear asc";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.DateReceived) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.DateReceived) AS VARCHAR(4)) AS [MonthYear] ";
                        //query += "workorders.DateReceived AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        //query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.DateReceived), YEAR(workorders.DateReceived)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.PMTarCompDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.PMTarCompDate) AS VARCHAR(4)) AS [MonthYear]";
                        //query += "workorders.PMTarCompDate AS [MonthYear] ";
                        query += ", COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription As CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        //query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.PMTarCompDate), YEAR(workorders.PMTarCompDate)";
                        query += ")";
                        query += ")";
                        query += " JobTrending ORDER BY MonthYear asc";
                    }
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<NoOfJobOrderByTypeDashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new ColumnChartModel { MonthYear = x.MonthYear, category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.JOCount });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Order Status By Site"

        public IList<JobOrderStatusBySite> GetJobOrderStatusBySiteDetail(bool IsCentral, int L1ID, string L2ID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<JobOrderStatusBySite> list = new List<JobOrderStatusBySite>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT workorders.L2ID, L2.L2name, L2.L2Altname ";
                        query += ", worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID NOT IN (2,3,7) THEN 1 ELSE 0 END) AS [CountOutstanding]  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID IN (2,7) THEN 1 ELSE 0 END) AS [CountClosed]  ";
                        query += "FROM workorders INNER JOIN worktype on workorders.WorkTypeID = worktype.WorkTypeID  ";
                        query += "INNER JOIN L2 ON L2.L2ID = workorders.L2ID  Where ";
                        if (L2ID.Length > 0)
                        {
                            query += "(workorders.L2ID IN (" + L2ID + ") ) AND ";
                        }
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate BETWEEN @JOStartDate and @JOCompletedDate)) ";
                        query += "OR (Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived BETWEEN @JOStartDate and @JOCompletedDate)))";
                        query += "GROUP BY workorders.L2ID,L2.L2name, L2.L2Altname, worktype.WorkTypeID , worktype.WorkTypeDescription, worktype.AltWorkTypeDescription ";
                    }
                    else
                    {
                        query += "SELECT workorders.L2ID, L2.L2name, L2.L2Altname ";
                        query += ", worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID NOT IN (2,3,7) THEN 1 ELSE 0 END) AS [CountOutstanding]  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID IN (2,7) THEN 1 ELSE 0 END) AS [CountClosed]  ";
                        query += "FROM workorders INNER JOIN worktype on workorders.WorkTypeID = worktype.WorkTypeID  ";
                        query += "INNER JOIN L2 ON L2.L2ID = workorders.L2ID  Where ";
                        if (L2ID.Length > 0)
                        {
                            query += "(workorders.L2ID IN (" + L2ID + ") ) AND ";
                        }
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate BETWEEN @JOStartDate and @JOCompletedDate)) ";
                        query += "OR (Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived BETWEEN @JOStartDate and @JOCompletedDate)))";
                        query += "GROUP BY workorders.L2ID,L2.L2name, L2.L2Altname, worktype.WorkTypeID , worktype.WorkTypeDescription, worktype.AltWorkTypeDescription ";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT workorders.L2ID, L2.L2name, L2.L2Altname ";
                        query += ", worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID NOT IN (2,3,7) THEN 1 ELSE 0 END) AS [CountOutstanding]  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID IN (2,7) THEN 1 ELSE 0 END) AS [CountClosed]  ";
                        query += "FROM workorders INNER JOIN worktype on workorders.WorkTypeID = worktype.WorkTypeID  ";
                        query += "INNER JOIN L2 ON L2.L2ID = workorders.L2ID  Where ";
                        if (L2ID.Length > 0)
                        {
                            query += "(workorders.L2ID IN (" + L2ID + ") ) AND ";
                        }
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                        query += "OR (Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')))";
                        query += "GROUP BY workorders.L2ID,L2.L2name, L2.L2Altname, worktype.WorkTypeID , worktype.WorkTypeDescription, worktype.AltWorkTypeDescription ";
                    }
                    else
                    {
                        query += "SELECT workorders.L2ID, L2.L2name, L2.L2Altname ";
                        query += ", worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID NOT IN (2,3,7) THEN 1 ELSE 0 END) AS [CountOutstanding]  ";
                        query += ", SUM(CASE WHEN workorders.WorkStatusID IN (2,7) THEN 1 ELSE 0 END) AS [CountClosed]  ";
                        query += "FROM workorders INNER JOIN worktype on workorders.WorkTypeID = worktype.WorkTypeID  ";
                        query += "INNER JOIN L2 ON L2.L2ID = workorders.L2ID  Where ";
                        if (L2ID.Length > 0)
                        {
                            query += "(workorders.L2ID IN (" + L2ID + ") ) AND ";
                        }
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(( workorders.WorkTypeID IN (2,7) AND (workorders.PMTarCompDate BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) ";
                        query += "OR (Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')))";
                        query += "GROUP BY workorders.L2ID,L2.L2name, L2.L2Altname, worktype.WorkTypeID , worktype.WorkTypeDescription, worktype.AltWorkTypeDescription ";
                    }
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.ExecuteQuery<JobOrderStatusBySite>(query, parameters);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JobOrderStatusBySiteSearchData GetJobOrderBySiteReportData(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                JobOrderStatusBySiteSearchData objJobOrderStatusBySiteSearchData = new JobOrderStatusBySiteSearchData();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objJobOrderStatusBySiteSearchData.reportID = reportId;
                        objJobOrderStatusBySiteSearchData.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objJobOrderStatusBySiteSearchData.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiCityUserControl"))
                            {
                                objJobOrderStatusBySiteSearchData.L2ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objJobOrderStatusBySiteSearchData.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objJobOrderStatusBySiteSearchData.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objJobOrderStatusBySiteSearchData;
            }

            finally
            {

            }
        }

        public bool SaveJobOrderStatusBySiteReport(JobOrderStatusBySiteSearchData obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.JobOrderStatusBySiteCriteria>)Enum.GetValues(typeof(SystemEnum.JobOrderStatusBySiteCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 2)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusBySiteCriteria), 4))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        #endregion

        #region "Top 10 Asset Breakdowns"

        public IList<Top10AssetBreakdowns> GetTop10AssetBreakdownsDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, string LocationID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Top10AssetBreakdowns> list = new List<Top10AssetBreakdowns>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle this query will be execute
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "select (ROW_NUMBER() OVER (ORDER BY TotalCount desc)) AS RowNo,assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";

                        query += " AND  ROWNUM <= 10";
                    }
                    else
                    {
                        query += "select (ROW_NUMBER() OVER (ORDER BY TotalCount desc)) AS RowNo,assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0)  ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }
                        query += " AND  ROWNUM <= 10";
                    }
                }
                //if database is mysql this query will be execute
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "select @rownum := @rownum + 1 AS RowNo ,assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += " ,(SELECT @rownum := 0) r ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";

                        query += "  Order by TotalCount desc LIMIT 10";
                    }
                    else
                    {
                        query += "select @rownum := @rownum + 1 AS RowNo , assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += " ,(SELECT @rownum := 0) r ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }
                        query += "  Order by TotalCount desc LIMIT 10";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "select Top 10 Convert(int,ROW_NUMBER() OVER (ORDER BY TotalCount desc)) AS RowNo,assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";
                    }
                    else
                    {
                        query += "select Top 10 Convert(int,ROW_NUMBER() OVER (ORDER BY TotalCount desc)) AS RowNo,assets.AssetDescription,assets.AssetAltDescription, ";
                        query += "assets.AssetNumber,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,location.LocationNo,TotalCount,assets.AssetID   ";
                        query += "FROM (Select isnull(AssetID,0) as AssetID,COUNT(*) as TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' AND  workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0)  ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }
                    }
                }



                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Top10AssetBreakdowns>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Top 10 Asset Downtime"

        public IList<Top10AssetBreakdowns> GetTop10AssetDowntimeDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, string LocationID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Top10AssetBreakdowns> list = new List<Top10AssetBreakdowns>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle this query will be execute
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT (ROW_NUMBER() OVER(ORDER BY TotalCount desc)) AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";

                        query += " AND  ROWNUM <= 10";
                    }
                    else
                    {
                        query += "SELECT (ROW_NUMBER() OVER(ORDER BY TotalCount desc)) AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }

                        query += " AND  ROWNUM <= 10";
                    }
                }
                //if database is MYSQL this query will be execute
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT @rownum := @rownum + 1 AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += " ,(SELECT @rownum := 0) r ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";

                        query += "  Order by TotalCount LIMIT 10";
                    }
                    else
                    {
                        query += "SELECT @rownum := @rownum + 1 AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between @JOStartDate and @JOCompletedDate AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += " ,(SELECT @rownum := 0) r ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0)  ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }

                        query += "  Order by TotalCount LIMIT 10";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT Top 10 Convert(int,ROW_NUMBER() OVER(ORDER BY TotalCount desc)) AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) AND";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(location.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(location.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(location.L5ID in (" + L5ID + ")) AND ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "(assets.LocationID in (" + LocationID + ")) AND ";
                        }
                        query += "(location.L2ID in (select L2Id from employees_L2 where empID = '" + ProjectSession.EmployeeID + "' union Select L2Id from employees where employeeId = '" + ProjectSession.EmployeeID + "'))";
                    }
                    else
                    {
                        query += "SELECT Top 10 Convert(int,ROW_NUMBER() OVER(ORDER BY TotalCount desc)) AS RowNo, assets.AssetDescription, assets.AssetAltDescription, assets.AssetNumber, ";
                        query += "L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, TotalCount, assets.AssetID   ";
                        query += "FROM   (SELECT isnull(AssetID, 0) AS AssetID, SUM(workorders.DownTime) AS TotalCount FROM   workorders   ";
                        query += "WHERE  DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3   ";
                        query += "GROUP BY AssetID ) T ";
                        query += "inner JOIN assets ON  assets.AssetID = T.AssetID ";
                        query += "inner JOIN location ON  assets.LocationID = location.LocationID ";
                        query += "left outer JOIN L2 ON  location.L2ID = L2.L2ID  ";
                        query += "left outer JOIN L1 ON  L1.L1ID = L2.L1ID  ";
                        query += "left outer JOIN L3 ON  location.L3ID = L3.L3ID  ";
                        query += "left outer JOIN L4 ON  location.L4ID = L4.L4ID  ";
                        query += "left outer JOIN L5 ON  location.L5ID = L5.L5ID  ";
                        query += "WHERE (location.L2ID = '" + L2ID + "' OR ISNULL('" + L2ID + "', 0) = 0) ";
                        query += " AND (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0)  ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "AND (location.L3ID in (" + L3ID + ")) ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "AND (location.L4ID in (" + L4ID + ")) ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "AND (location.L5ID in (" + L5ID + ")) ";
                        }
                        if (LocationID.Length > 0)
                        {
                            query += "AND (assets.LocationID in (" + LocationID + ")) ";
                        }
                    }
                }



                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Top10AssetBreakdowns>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Order And Request Trending"

        public IList<ColumnChartModel> GetJobOrderAndRequestTrendingDetail(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTypeID, string JobTradeID, DateTime JOStartDate, DateTime JOCompletedDate)
        {
            try
            {
                IList<JobTradingDashboardChart> list = new List<JobTradingDashboardChart>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                int idx = 0;
                var viewModel = new List<CMMS.Model.ColumnChartModel>();

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                //if database is oracle this query will be execute
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.DateReceived, 'Mon')||' - '||to_char(workorders.DateReceived,'yyyy')  MonthYear ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ", to_char(workorders.DateReceived,'mm') AS DateMonth, to_char(workorders.DateReceived,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.DateReceived,to_char(workorders.DateReceived,'mm'), to_char(workorders.DateReceived,'yyyy')";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.PMTarCompDate, 'Mon')||' - '||to_char(workorders.PMTarCompDate,'yyyy')  MonthYear ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ", to_char(workorders.PMTarCompDate,'mm') AS DateMonth, to_char(workorders.PMTarCompDate,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate,to_char(workorders.PMTarCompDate,'mm'), to_char(workorders.PMTarCompDate,'yyyy')";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(worequest.ReceivedDate, 'Mon')||' - '||to_char(worequest.ReceivedDate,'yyyy')  \"MONTH-YEAR\" ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, N'Job Request' AS CategoryEnglish, N'Job Request' AS CategoryArabic ";
                        query += ", to_char(worequest.ReceivedDate,'mm') AS DateMonth, to_char(worequest.ReceivedDate,'yyyy') AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN @JOStartDate and @JOCompletedDate) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,worequest.ReceivedDate,to_char(worequest.ReceivedDate,'mm'), to_char(worequest.ReceivedDate,'yyyy'))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateYear,JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(workorders.DateReceived, 'Mon')||' - '||to_char(workorders.DateReceived,'yyyy')  MonthYear ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ", to_char(workorders.DateReceived,'mm') AS DateMonth, to_char(workorders.DateReceived,'yyyy') ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.DateReceived,to_char(workorders.DateReceived,'mm'), to_char(workorders.DateReceived,'yyyy') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += " to_char(workorders.PMTarCompDate, 'Mon')||' - '||to_char(workorders.PMTarCompDate,'yyyy')  \"[MONTH-YEAR]\" ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ", to_char(workorders.PMTarCompDate,'mm') AS DateMonth, to_char(workorders.PMTarCompDate,'yyyy') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate, to_char(workorders.PMTarCompDate,'mm'), to_char(workorders.PMTarCompDate,'yyyy')";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "to_char(worequest.ReceivedDate, 'Mon')||' - '||to_char(worequest.ReceivedDate,'yyyy')  \"MONTH-YEAR\" ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, N'Job Request' AS CategoryEnglish, N'Job Request' AS CategoryArabic ";
                        query += ",to_char(worequest.ReceivedDate,'mm') AS DateMonth,to_char(worequest.ReceivedDate,'yyyy') AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN @JOStartDate and @JOCompletedDate ) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,worequest.ReceivedDate, to_char(worequest.ReceivedDate,'mm'), to_char(worequest.ReceivedDate,'yyyy'))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                }
                //if database is MYSQL this query will be execute
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = Convert.ToDateTime(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = Convert.ToDateTime(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(workorders.DateReceived,'%b'), ' - ', DATE_FORMAT(workorders.DateReceived,'%Y'))  as MonthYear  ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ",DATE_FORMAT(workorders.DateReceived,'%m') AS DateMonth, DATE_FORMAT(workorders.DateReceived,'%Y') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,DATE_FORMAT(workorders.DateReceived,'%m'),DATE_FORMAT(workorders.DateReceived,'%Y') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(workorders.PMTarCompDate,'%b'), ' - ', DATE_FORMAT(workorders.PMTarCompDate,'%Y'))  as MonthYear  ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ",DATE_FORMAT(workorders.PMTarCompDate,'%m') AS DateMonth,DATE_FORMAT(workorders.PMTarCompDate,'%Y') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate,DATE_FORMAT(workorders.PMTarCompDate,'%m'),DATE_FORMAT(workorders.PMTarCompDate,'%Y') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(worequest.ReceivedDate,'%b'), ' - ', DATE_FORMAT(worequest.ReceivedDate,'%Y'))  as `MONTH-YEAR`  ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, N'Job Request' AS CategoryEnglish, N'Job Request' AS CategoryArabic ";
                        query += ",DATE_FORMAT(worequest.ReceivedDate,'%m') AS DateMonth,DATE_FORMAT(worequest.ReceivedDate,'%Y') AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN @JOStartDate and @JOCompletedDate) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,worequest.ReceivedDate,DATE_FORMAT(worequest.ReceivedDate,'%m'),DATE_FORMAT(worequest.ReceivedDate,'%Y'))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateYear,JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(workorders.DateReceived,'%b'), ' - ', DATE_FORMAT(workorders.DateReceived,'%Y'))  as MonthYear  ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ",DATE_FORMAT(workorders.DateReceived,'%m') AS DateMonth,DATE_FORMAT(workorders.DateReceived,'%Y') ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.DateReceived,DATE_FORMAT(workorders.DateReceived,'%m'),DATE_FORMAT(workorders.DateReceived,'%Y') ";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(workorders.PMTarCompDate,'%b'), ' - ', DATE_FORMAT(workorders.PMTarCompDate,'%Y'))  as `MONTH-YEAR`  ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ", DATE_FORMAT(workorders.PMTarCompDate,'%m') AS DateMonth, DATE_FORMAT(workorders.PMTarCompDate,'%Y') AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between @JOStartDate and @JOCompletedDate)) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,workorders.PMTarCompDate, DATE_FORMAT(workorders.PMTarCompDate,'%m'), tDATE_FORMAT(workorders.PMTarCompDate,'%Y')";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += " CONCAT(DATE_FORMAT(worequest.ReceivedDate,'%b'), ' - ', DATE_FORMAT(worequest.ReceivedDate,'%Y'))  as `MONTH-YEAR`  ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, N'Job Request' AS CategoryEnglish, N'Job Request' AS CategoryArabic ";
                        query += ",DATE_FORMAT(worequest.ReceivedDate,'%m') AS DateMonth,DATE_FORMAT(worequest.ReceivedDate,'%Y') AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN @JOStartDate and @JOCompletedDate ) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription,worequest.ReceivedDate, DATE_FORMAT(worequest.ReceivedDate,'%m'), DATE_FORMAT(worequest.ReceivedDate,'%Y'))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateYear,JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.DateReceived) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.DateReceived) AS VARCHAR(4)) AS [MonthYear] ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.DateReceived), YEAR(workorders.DateReceived)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.PMTarCompDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.PMTarCompDate) AS VARCHAR(4)) AS [MonthYear] ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) AND ";
                        query += "(workorders.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.PMTarCompDate), YEAR(workorders.PMTarCompDate)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(worequest.ReceivedDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(worequest.ReceivedDate) AS VARCHAR(4)) AS [MONTH-YEAR] ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, 'Job Request' AS CategoryEnglish, 'Job Request' AS CategoryArabic ";
                        query += ", MONTH(worequest.ReceivedDate) AS DateMonth, YEAR(worequest.ReceivedDate) AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' ) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.L2ID IN (select L2ID from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2ID from employees where employeeId = " + ProjectSession.EmployeeID + ")) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(worequest.ReceivedDate), YEAR(worequest.ReceivedDate))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateYear,JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                    else
                    {
                        query += "SELECT * FROM (";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.DateReceived) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.DateReceived) AS VARCHAR(4)) AS [MonthYear] ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish, worktype.AltWorkTypeDescription As CategoryArabic ";
                        query += ", MONTH(workorders.DateReceived) AS DateMonth, YEAR(workorders.DateReceived) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId NOT IN (2,7) AND (workorders.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.DateReceived), YEAR(workorders.DateReceived)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(workorders.PMTarCompDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(workorders.PMTarCompDate) AS VARCHAR(4)) AS [MonthYear] ";
                        query += ", worktype.WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, worktype.WorkTypeDescription AS CategoryEnglish , worktype.AltWorkTypeDescription  As CategoryArabic ";
                        query += ", MONTH(workorders.PMTarCompDate) AS DateMonth, YEAR(workorders.PMTarCompDate) AS DateYear ";
                        query += "FROM workorders  ";
                        query += "INNER JOIN worktype ON workorders.WorkTypeID = worktype.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = workorders.WOTradeID ";
                        query += " inner join L2 on L2.L2ID = workorders.L2ID ";
                        query += "WHERE  (workorders.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(workorders.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(workorders.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(workorders.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(workorders.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(workorders.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(workorders.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(workorders.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        query += "(Workorders.worktypeId IN (2,7) AND (workorders.PMTarCompDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "')) AND ";
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(workorders.WorkStatusID != 3) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(workorders.PMTarCompDate), YEAR(workorders.PMTarCompDate)";
                        query += ")";
                        query += "UNION ALL ";
                        query += "(";
                        query += "SELECT ";
                        query += "CAST(DATENAME( MONTH , DATEADD( MONTH , MONTH(worequest.ReceivedDate) , -1 ) ) AS VARCHAR(3)) + ' - ' + CAST(YEAR(worequest.ReceivedDate) AS VARCHAR(4)) AS [MONTH-YEAR] ";
                        query += ", 0 AS WorkTypeID, COUNT(worktype.WorkTypeDescription) AS JOCount, 'Job Request' AS CategoryEnglish, 'Job Request' AS CategoryArabic ";
                        query += ", MONTH(worequest.ReceivedDate) AS DateMonth, YEAR(worequest.ReceivedDate) AS DateYear ";
                        query += "FROM worequest ";
                        query += "INNER JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID ";
                        query += "INNER JOIN worktrade w ON w.WorkTradeID = worequest.worktradeid ";
                        query += " inner join L2 on L2.L2ID = worequest.L2ID ";
                        query += "WHERE  (worequest.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(worequest.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(worequest.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(worequest.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(worequest.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(worequest.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(worequest.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(worequest.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(workorders.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(worequest.ReceivedDate BETWEEN '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "' ) ";
                        query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, MONTH(worequest.ReceivedDate), YEAR(worequest.ReceivedDate))";
                        query += ")";
                        query += " JobTrending ORDER BY JobTrending.DateYear,JobTrending.DateMonth,JobTrending.WorkTypeID";
                    }
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<JobTradingDashboardChart>(query, parameters, 0);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new ColumnChartModel { MonthYear = x.MonthYear, category = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.CategoryArabic : x.CategoryEnglish, value = x.JOCount , DateMonth =x.DateMonth ,DateYear =x.DateYear });
                    idx++;
                    idx = (idx % colours.Length);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Dashboard Module Permission"

        public DashboardModule GetDashboardChartPermission()
        {
            string query = string.Empty;
            try
            {
                DashboardReportMenu objDashboardReportMenu = new DashboardReportMenu();
                DashboardModule objDashboardModule = new DashboardModule();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    var result = objDapperContext.SearchAll(objDashboardReportMenu).Where(o => o.UserID == ProjectSession.EmployeeID).ToList();
                    if (result != null)
                    {
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 1)))
                            {
                                objDashboardModule.PMJobOrderStatus = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 2)))
                            {
                                objDashboardModule.JobOrderStatus = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 3)))
                            {
                                objDashboardModule.JobOrderTypeDistribution = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 4)))
                            {
                                objDashboardModule.PMCompilance = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 5)))
                            {
                                objDashboardModule.Top10AssetBreakdown = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 6)))
                            {
                                objDashboardModule.Top10AssetDowntime = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 7)))
                            {
                                objDashboardModule.JobOrderAndRequestTrending = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 8)))
                            {
                                objDashboardModule.NoOfJobOrdersByType = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 9)))
                            {
                                objDashboardModule.JobOrderStatusBySite = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 10)))
                            {
                                objDashboardModule.JobPriorityDistribution = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 11)))
                            {
                                objDashboardModule.JobTradeDistribution = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 12)))
                            {
                                objDashboardModule.OutstandingJobsByPriority = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 13)))
                            {
                                objDashboardModule.OutstandingJobsByTrade = true;
                            }
                            if (result[i].ReportUniqueName.Equals(@SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 14)))
                            {
                                objDashboardModule.OutstandingJobsByType = true;
                            }
                        }
                    }
                }
                return objDashboardModule;
            }

            finally
            {

            }
        }

        public bool InsertWidet(bool isChecked, int reportId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                DashboardReportMenu objDashboardReportMenu = new DashboardReportMenu();
                int displayOrder = 1;
                DashboardReportMenu objOld = new DashboardReportMenu();
                var resultDisplayOrder = objDapperContext.SearchAll(objOld).Where(o => o.UserID == ProjectSession.EmployeeID).ToList();
                if (resultDisplayOrder.Count > 0)
                {
                    displayOrder = resultDisplayOrder.Max(o => o.DisplayOrder) + 1;
                }

                if (isChecked)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "UserID",
                        Value = ProjectSession.EmployeeID,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ReportUniqueName",
                        Value = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), reportId),
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = ProjectSession.EmployeeID,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ChartType",
                        Value = 1,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "DisplayOrder",
                        Value = displayOrder,
                        DBType = DbType.Int32
                    });

                    strQuery = @"INSERT INTO DashboardReportMenu(ReportUniqueName,UserID,CreatedBy,CreatedDate,ChartType,DisplayOrder) VALUES(@ReportUniqueName,@UserID,@CreatedBy,@CreatedDate,@ChartType,@DisplayOrder)";
                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }
                else
                {
                    var result = objDapperContext.SearchAll(objDashboardReportMenu).Where(o => o.UserID == ProjectSession.EmployeeID).ToList();
                    if (result != null)
                    {
                        objDashboardReportMenu = result.Where(o => o.ReportUniqueName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), reportId))).FirstOrDefault();
                        parameters.Add(new DBParameters()
                        {
                            Name = "DashboardRepotMenuID",
                            Value = objDashboardReportMenu.DashboardRepotMenuID,
                            DBType = DbType.Int32
                        });
                        strQuery = @"Delete From DashboardReportMenu where DashboardRepotMenuID = @DashboardRepotMenuID";
                        objDapperContext.ExecuteQuery(strQuery, parameters);
                    }
                }

            }
            return true;
        }

        public IList<DashboardReportMenu> GetDashBoardReportMenu()
        {
            try
            {
                IList<DashboardReportMenu> list = new List<DashboardReportMenu>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                query += "SELECT drf.*,drf2.FieldName, drf2.FieldValue,";
                query += "ISNULL(l.L2Code,'All') AS FilterBy ";
                query += ",drf3.FieldValue AS JOStartDate,drf4.FieldValue AS JOEndDate,drf5.FieldValue AS DatePickerID ";
                query += "FROM DashboardReportMenu drf ";
                query += "LEFT JOIN DashboardReportFilter drf2 ON drf2.DashboardRepotMenuID = drf.DashboardRepotMenuID AND drf2.FieldName = 'ComboBoxCityCode' ";
                query += "LEFT JOIN DashboardReportFilter drf3 ON drf3.DashboardRepotMenuID = drf.DashboardRepotMenuID AND drf3.FieldName = 'DatePickerJODateFrom' ";
                query += "LEFT JOIN DashboardReportFilter drf4 ON drf4.DashboardRepotMenuID = drf.DashboardRepotMenuID AND drf4.FieldName = 'DatePickerJODateTo' ";
                query += "LEFT JOIN DashboardReportFilter drf5 ON drf5.DashboardRepotMenuID = drf.DashboardRepotMenuID AND drf5.FieldName = 'ComboBoxDatePicker' ";
                query += "LEFT JOIN L2 l ON l.L2ID = drf2.FieldValue ";
                query += "WHERE drf.UserID = " + ProjectSession.EmployeeID;
                query += " ORDER BY DisplayOrder ";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardReportMenu>(query, parameters, 0);
                    foreach (var item in list.Where(w => w.FilterBy == "All"))
                    {
                        if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                        {
                            item.FilterBy = "جميع";
                        }
                    }

                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<DashboardReportMenu> GetDashBoardReportMenuForGrid(int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<DashboardReportMenu> list = new List<DashboardReportMenu>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT * FROM DashboardReportMenu drf WHERE drf.UserID = " + ProjectSession.EmployeeID;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<DashboardReportMenu>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Report Filter Data"

        public ReportBindJobOrder GetReportFilterData(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objReportBindJobOrder.reportID = reportId;
                        objReportBindJobOrder.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objReportBindJobOrder.L2ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objReportBindJobOrder.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objReportBindJobOrder.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objReportBindJobOrder.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objReportBindJobOrder.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objReportBindJobOrder.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objReportBindJobOrder.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTypeUserControl"))
                            {
                                objReportBindJobOrder.JobTypeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objReportBindJobOrder.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objReportBindJobOrder.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objReportBindJobOrder.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objReportBindJobOrder;
            }

            finally
            {

            }
        }

        public bool SaveReportData(ReportBindJobOrder obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.JobOrderStatusCriteria>)Enum.GetValues(typeof(SystemEnum.JobOrderStatusCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2)) && ProjectSession.IsAreaFieldsVisible)
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 10)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 10);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTypeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 10))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public JobOrderTypeChart GetReportFilterDataForJobOrderType(int reportId)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                JobOrderTypeChart objJobOrderTypeChart = new JobOrderTypeChart();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    var result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.ReportUniqueName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), reportId))).ToList();
                    if (result != null)
                    {
                        objJobOrderTypeChart.reportID = reportId;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objJobOrderTypeChart.L2ID = ConvertTo.Integer(result[i].FieldValue);
                                if (objJobOrderTypeChart.L2ID != 0)
                                {
                                    L2 l2 = objDapperContext.SelectObject<L2>(objJobOrderTypeChart.L2ID);
                                    if (l2 != null)
                                    {
                                        objJobOrderTypeChart.L2Code = l2.L2Code;
                                    }
                                }
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl"))
                            {
                                objJobOrderTypeChart.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objJobOrderTypeChart.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objJobOrderTypeChart.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objJobOrderTypeChart.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objJobOrderTypeChart.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objJobOrderTypeChart.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objJobOrderTypeChart.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objJobOrderTypeChart.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objJobOrderTypeChart;
            }

            finally
            {

            }
        }

        public bool SaveReporForJobOrderTypetData(JobOrderTypeChart obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.JobOrderStatusCriteria>)Enum.GetValues(typeof(SystemEnum.JobOrderStatusCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.ReportUniqueName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID))).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public TopTenAssetModal GetReportFilterDataForTopTenAsset(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                TopTenAssetModal objTopTenAssetModal = new TopTenAssetModal();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objTopTenAssetModal.reportID = reportId;
                        objTopTenAssetModal.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objTopTenAssetModal.L2ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objTopTenAssetModal.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objTopTenAssetModal.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objTopTenAssetModal.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("TextBoxLocationNo"))
                            {
                                objTopTenAssetModal.LocationID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objTopTenAssetModal.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objTopTenAssetModal.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objTopTenAssetModal.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objTopTenAssetModal;
            }

            finally
            {

            }
        }

        public bool SaveReporForTopTenAssetData(TopTenAssetModal obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.TopTenAssetCriteria>)Enum.GetValues(typeof(SystemEnum.TopTenAssetCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 2)) && ProjectSession.IsAreaFieldsVisible)
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.LocationID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.TopTenAssetCriteria), 8))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public PMComplianceModal GetReportFilterDataForPMComplianceModal(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                PMComplianceModal objPMComplianceModal = new PMComplianceModal();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objPMComplianceModal.reportID = reportId;
                        objPMComplianceModal.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objPMComplianceModal.L2ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objPMComplianceModal.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objPMComplianceModal.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objPMComplianceModal.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objPMComplianceModal.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objPMComplianceModal.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objPMComplianceModal.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objPMComplianceModal.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objPMComplianceModal.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTradeUserControl"))
                            {
                                objPMComplianceModal.JobTradeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objPMComplianceModal.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objPMComplianceModal;
            }

            finally
            {

            }
        }

        public bool SavePMComplianceData(PMComplianceModal obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.PMComplianceCriteria>)Enum.GetValues(typeof(SystemEnum.PMComplianceCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 2)) && ProjectSession.IsAreaFieldsVisible)
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 9))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 10)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 10);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTradeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 10))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 11)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 11);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.PMComplianceCriteria), 11))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public JobTradeingModal GetReportFilterDataForJobTradingModal(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                JobTradeingModal objJobTradeingModal = new JobTradeingModal();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objJobTradeingModal.reportID = reportId;
                        objJobTradeingModal.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objJobTradeingModal.L2ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objJobTradeingModal.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objJobTradeingModal.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objJobTradeingModal.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objJobTradeingModal.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objJobTradeingModal.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objJobTradeingModal.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objJobTradeingModal.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objJobTradeingModal.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTradeUserControl"))
                            {
                                objJobTradeingModal.JobTradeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTypeUserControl"))
                            {
                                objJobTradeingModal.JobTypeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objJobTradeingModal.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objJobTradeingModal;
            }

            finally
            {

            }
        }

        public bool SaveJobTradingData(JobTradeingModal obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.JobTradingCriteria>)Enum.GetValues(typeof(SystemEnum.JobTradingCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 2)) && ProjectSession.IsAreaFieldsVisible)
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 9))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 10)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 10);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTradeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 10))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 11)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 11);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTypeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 11))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 12)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 12);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobTradingCriteria), 12))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        #endregion

        #region "Method"

        public string GetLocationNoByID(string id)
        {
            try
            {
                IList<Location> list = new List<Location>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                query += "SELECT l.LocationID,l.LocationNo,l.LocationDescription,l.LocationAltDescription FROM location l ";
                query += "Where ";
                if (id.Length > 0)
                {
                    query += "l.LocationID in (" + id + ") ";
                }
                else
                {
                    query += "l.LocationID =  " + id;
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.ExecuteQuery<Location>(query, null);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
                var str = String.Join(",", list.Select(o => o.LocationNo));

                return str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JobOrderTypeChartFilterModal GetJobOrderTypeFilterSearch(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal = new JobOrderTypeChartFilterModal();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objJobOrderTypeChartFilterModal.reportID = reportId;
                        objJobOrderTypeChartFilterModal.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objJobOrderTypeChartFilterModal.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objJobOrderTypeChartFilterModal.L2ID = ConvertTo.Integer(result[i].FieldValue);
                                if (objJobOrderTypeChartFilterModal.L2ID != 0)
                                {
                                    L2 l2 = objDapperContext.SelectObject<L2>(objJobOrderTypeChartFilterModal.L2ID);
                                    if (l2 != null)
                                    {
                                        objJobOrderTypeChartFilterModal.L2Code = l2.L2Code;
                                    }
                                }
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objJobOrderTypeChartFilterModal.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objJobOrderTypeChartFilterModal.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objJobOrderTypeChartFilterModal.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objJobOrderTypeChartFilterModal.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objJobOrderTypeChartFilterModal.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objJobOrderTypeChartFilterModal.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objJobOrderTypeChartFilterModal.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objJobOrderTypeChartFilterModal.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objJobOrderTypeChartFilterModal;
            }

            finally
            {

            }
        }

        public bool SaveReporForJobOrderTypeCriteria(JobOrderTypeChartFilterModal obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.JobOrderStatusCriteria>)Enum.GetValues(typeof(SystemEnum.JobOrderStatusCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2)) && ProjectSession.IsAreaFieldsVisible)
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateFrom.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JoDateTo.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 9))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.JobOrderStatusCriteria), 11))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            Collection<DBParameters> parameters = new Collection<DBParameters>();

                            parameters.Add(new DBParameters()
                            {
                                Name = "CreatedDate",
                                Value = objDashboardReportFilter.CreatedDate,
                                DBType = DbType.DateTime
                            });

                            strQuery = @"INSERT INTO DashboardReportFilter(FieldName,FieldValue,ReportUniqueName,UserID,CreatedBy,CreatedDate,DashboardRepotMenuID) VALUES('" + objDashboardReportFilter.FieldName + "','" + objDashboardReportFilter.FieldValue + "','" + objDashboardReportFilter.ReportUniqueName + "','" + objDashboardReportFilter.UserID + "','" + objDashboardReportFilter.CreatedBy + "',@CreatedDate,'" + objDashboardReportFilter.DashboardRepotMenuID + "')";
                            objDapperContext.ExecuteQuery(strQuery, parameters);
                        }
                        else
                        {
                            strQuery = @"INSERT INTO DashboardReportFilter(FieldName,FieldValue,ReportUniqueName,UserID,CreatedBy,CreatedDate,DashboardRepotMenuID) VALUES('" + objDashboardReportFilter.FieldName + "','" + objDashboardReportFilter.FieldValue + "','" + objDashboardReportFilter.ReportUniqueName + "','" + objDashboardReportFilter.UserID + "','" + objDashboardReportFilter.CreatedBy + "','" + objDashboardReportFilter.CreatedDate + "','" + objDashboardReportFilter.DashboardRepotMenuID + "')";
                            objDapperContext.ExecuteQuery(strQuery, null);
                        }

                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;

                        if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            Collection<DBParameters> parameters = new Collection<DBParameters>();

                            parameters.Add(new DBParameters()
                            {
                                Name = "ModifiedDate",
                                Value = objDashboardReportFilter.ModifiedDate,
                                DBType = DbType.DateTime
                            });

                            strQuery = @"UPDATE DashboardReportFilter SET FieldValue = '" + objDashboardReportFilter.FieldValue + "', ModifiedBy = '" + objDashboardReportFilter.ModifiedBy + "' , ModifiedDate = @ModifiedDate  where DashboardReportFilterID = '" + objDashboardReportFilter.DashboardReportFilterID + "' and UserID = '" + objDashboardReportFilter.UserID + "'";
                            objDapperContext.ExecuteQuery(strQuery, parameters);
                        }
                        else
                        {
                            strQuery = @"UPDATE DashboardReportFilter SET FieldValue = '" + objDashboardReportFilter.FieldValue + "', ModifiedBy = '" + objDashboardReportFilter.ModifiedBy + "' , ModifiedDate = '" + objDashboardReportFilter.ModifiedDate + "'  where DashboardReportFilterID = '" + objDashboardReportFilter.DashboardReportFilterID + "' and UserID = '" + objDashboardReportFilter.UserID + "'";
                            objDapperContext.ExecuteQuery(strQuery, null);
                        }


                    }
                }
            }
            return true;
        }

        public bool DeleteChartByDashboardMenuID(int reportID, int dashboardReportMenuID)
        {
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "DashboardRepotMenuID",
                    Value = dashboardReportMenuID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "UserID",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From DashboardReportFilter where DashboardRepotMenuID = @DashboardRepotMenuID and UserID = @UserID";
                objDapperContext.ExecuteQuery(strQuery, parameters);

                strQuery = @"Delete From DashboardReportMenu where DashboardRepotMenuID = @DashboardRepotMenuID and UserID = @UserID";
                objDapperContext.ExecuteQuery(strQuery, parameters);

                UpdateDisplayOrder();
            }
            return true;
        }

        public bool UpdateDisplayOrder()
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    strQuery = @";WITH cte AS(SELECT *, ROW_NUMBER() OVER(ORDER BY DisplayOrder) Rno FROM dbo.DashboardReportMenu WHERE UserID = " + ProjectSession.EmployeeID + ")" +
                                "UPDATE cte SET DisplayOrder = cte.Rno";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    strQuery = @"UPDATE DashboardReportMenu as c inner JOIN(SELECT *,  @rownum := @rownum + 1 AS rank FROM DashboardReportMenu,(SELECT @rownum := 0) r WHERE UserID = " + ProjectSession.EmployeeID + ") as cte " +
                               "on cte.UserID = c.UserID and cte.DisplayOrder = c.DisplayOrder SET c.DisplayOrder = cte.rank";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = @"UPDATE DashboardReportMenu c SET(c.DisplayOrder) = (SELECT ROW_NUMBER() OVER ( ORDER BY DisplayOrder) rno FROM   DashboardReportMenu cte WHERE  cte.UserID = c.UserID and cte.DisplayOrder = c.DisplayOrder) " +
                                "Where c.UserID = " + ProjectSession.EmployeeID;
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        public static List<MaintSubDept> GetSubDeptByDeptsId(string DeptID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "select maintSubDept.* from MaintSubDept maintSubDept  ";
                if (DeptID.Length > 0)
                {
                    query += "Where maintSubDept.MaintDeptID in (" + DeptID + ") ";
                }

                return context.ExecuteQuery<MaintSubDept>(query, parameters).ToList();
            }
        }

        public static List<MaintenanceDepartment> GetMaintDeptByDivisionsId(int DivisionID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT md.* FROM MaintenanceDepartment md  ";
                if (DivisionID > 0)
                {
                    query += "WHERE md.MaintDivisionID in (" + DivisionID + ") ";
                }

                return context.ExecuteQuery<MaintenanceDepartment>(query, parameters).ToList();
            }
        }

        public static List<L2> GetCityByOrgID(int orgID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT L2.* FROM L2  ";
                if (orgID > 0)
                {
                    query += "Where L2.L1ID in (" + orgID + ") ";
                }

                return context.ExecuteQuery<L2>(query, parameters).ToList();
            }
        }

        public static List<L3> GetAreaByCityID(int cityID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT L3.* FROM L3  ";
                if (cityID > 0)
                {
                    query += "Where L3.L2ID in (" + cityID + ") ";
                }

                return context.ExecuteQuery<L3>(query, parameters).ToList();
            }
        }

        public static List<L4> GetZoneByCityID(int cityID, string areaId, int? orgID = 0)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT L4.* FROM L4 inner join L2 on L4.L2ID = L2.L2ID Where 1 = 1 ";
                if (cityID > 0)
                {
                    query += "AND L4.L2ID in (" + cityID + ") ";
                }

                if (orgID > 0)
                {
                    query += "AND L2.L1ID =" + orgID;
                }

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    if (areaId.Length > 0)
                    {
                        query += "AND L4.L3ID in (" + areaId + ") ";
                    }
                }

                if (!ProjectSession.IsCentral)
                {
                    query += " and L4.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";

                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        query += " and (L4.L3ID in ( " + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (L4.L3ID IS NULL))";
                    }
                }

                return context.ExecuteQuery<L4>(query, parameters).ToList();
            }
        }

        public static List<L5> GetBuildingByCityID(int cityID, string areaId, string zoneId, int? orgID = 0)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT L5.* FROM L5 inner join L2 on L5.L2Id = L2.L2Id Where 1 = 1 ";
                if (cityID > 0)
                {
                    query += "AND L5.L2ID in (" + cityID + ") ";
                }

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    if (areaId.Length > 0)
                    {
                        query += "AND L5.L3ID in (" + areaId + ") ";
                    }
                }
                if (zoneId.Length > 0)
                {
                    query += "AND L5.L4ID in (" + zoneId + ") ";
                }

                if (orgID > 0)
                {
                    query += "AND L2.L1Id in (" + orgID + ") ";
                }

                if (!ProjectSession.IsCentral)
                {
                    query += " and L5.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";

                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        query += " and (L5.L3ID in ( " + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (L5.L3ID IS NULL))";
                    }
                }

                return context.ExecuteQuery<L5>(query, parameters).ToList();
            }
        }

        public static List<Location> GetLocationByCityID(int cityID, string areaId, string zoneId, string buildingId)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT location.* FROM location  Where 1 = 1 ";
                if (cityID > 0)
                {
                    query += "AND location.L2ID in (" + cityID + ") ";
                }
                if (ProjectSession.IsAreaFieldsVisible)
                {
                    if (areaId.Length > 0)
                    {
                        query += "AND location.L3ID in (" + areaId + ") ";
                    }
                }
                if (zoneId.Length > 0)
                {
                    query += "AND location.L4ID in (" + zoneId + ") ";
                }
                if (buildingId.Length > 0)
                {
                    query += "AND location.L5ID in (" + buildingId + ") ";
                }

                return context.ExecuteQuery<Location>(query, parameters).ToList();
            }
        }

        public static List<Asset> GetAssetByCityID(int cityID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT ass.AssetID, ass.AssetNumber, ass.AssetDescription, ass.AssetAltDescription ";
                query += "FROM assets ass inner join ";
                query += "location l on l.LocationID = ass.LocationID ";
                query += "Where 1 = 1 ";

                if (cityID > 0)
                {
                    query += "AND l.L2ID in (" + cityID + ") ";
                }

                return context.ExecuteQuery<Asset>(query, parameters).ToList();
            }
        }

        public static List<Asset> GetEmployeeByCityID(int cityID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "SELECT emp.EmployeeID, emp.EmployeeNO, emp.Name, emp.AltName ";
                query += "FROM employees emp ";
                query += "Where 1 = 1 ";

                if (cityID > 0)
                {
                    query += "AND emp.L2ID in (" + cityID + ") ";
                }

                return context.ExecuteQuery<Asset>(query, parameters).ToList();
            }
        }

        public bool ChangeChartTypeByDashboardMenuID(int chartTypeID, int dashboardReportMenuID)
        {
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "DashboardRepotMenuID",
                    Value = dashboardReportMenuID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ChartType",
                    Value = chartTypeID,
                    DBType = DbType.Int32
                });

                strQuery = @"UPDATE DashboardReportMenu SET ChartType = @ChartType Where DashboardRepotMenuID = @DashboardRepotMenuID";
                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        #endregion

        #region "Work Report Status"

        public WorkReportFilterModal GetReportFilterDataForWorkStatusModal(int reportId, int dashboardMenuID)
        {
            string query = string.Empty;
            try
            {
                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<DashboardReportFilter> result = objDapperContext.SearchAll(objDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardMenuID).ToList();
                    if (result.Count > 0)
                    {
                        objWorkReportFilterModal.reportID = reportId;
                        objWorkReportFilterModal.DashboardRepotMenuID = dashboardMenuID;
                        for (int i = 0; i < result.Count; i++)
                        {
                            if (result[i].FieldName.Equals("ComboBoxCityCode"))
                            {
                                objWorkReportFilterModal.L2ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiAreaUserControl") && ProjectSession.IsAreaFieldsVisible)
                            {
                                objWorkReportFilterModal.L3ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiStreetUserControl"))
                            {
                                objWorkReportFilterModal.L4ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiBuildingUserControl"))
                            {
                                objWorkReportFilterModal.L5ID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxMaintDiv"))
                            {
                                objWorkReportFilterModal.MaintDivID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintDeptUserControl"))
                            {
                                objWorkReportFilterModal.MaintDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiMaintSubDeptUserControl"))
                            {
                                objWorkReportFilterModal.MaintSubDeptID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTradeUserControl"))
                            {
                                objWorkReportFilterModal.JobTradeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("MultiJobTypeUserControl"))
                            {
                                objWorkReportFilterModal.JobTypeID = ConvertTo.String(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateFrom"))
                            {
                                objWorkReportFilterModal.JoDateFrom = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("DatePickerJODateTo"))
                            {
                                objWorkReportFilterModal.JoDateTo = Convert.ToDateTime(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxDatePicker"))
                            {
                                objWorkReportFilterModal.DatePickerID = ConvertTo.Integer(result[i].FieldValue);
                            }
                            if (result[i].FieldName.Equals("ComboBoxOrg"))
                            {
                                objWorkReportFilterModal.L1ID = ConvertTo.Integer(result[i].FieldValue);
                            }
                        }
                    }
                }
                return objWorkReportFilterModal;
            }

            finally
            {

            }
        }

        public bool SaveWorkStatusReportData(WorkReportFilterModal obj)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.WorkStatusReportCriteria>)Enum.GetValues(typeof(SystemEnum.WorkStatusReportCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).ToList();
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == obj.DashboardRepotMenuID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), obj.reportID);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = obj.DashboardRepotMenuID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 1)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 1);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L2ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 1))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 2)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 2);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L3ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 2))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 3)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 3);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L4ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 3))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 4)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 4);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L5ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 4))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 5)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 5);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDivID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 5))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 6)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 6);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 6))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 7)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 7);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.MaintSubDeptID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 7))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 8)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 8);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTradeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 8))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 9)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 9);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.JobTypeID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 9))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 13)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 13);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(obj.L1ID);
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 13))).FirstOrDefault();
                    }

                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public bool UpdateDatePickerID(int datePickerID, int dashboardID)
        {
            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardID).ToList();

                DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 15);
                objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                objDashboardReportFilter.DashboardRepotMenuID = dashboardID;
                DashboardReportFilter objCurrent = new DashboardReportFilter();

                objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 12);
                objDashboardReportFilter.FieldValue = ConvertTo.String(datePickerID);
                objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 12))).FirstOrDefault();

                if (objCurrent == null)
                {
                    objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                    objDashboardReportFilter.CreatedDate = DateTime.Now;
                    objDapperContext.Save(objDashboardReportFilter);
                }
                else
                {
                    objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                    objDashboardReportFilter.ModifiedDate = DateTime.Now;
                    objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                    objDapperContext.Save(objDashboardReportFilter);
                }

                if (datePickerID != 4)
                {
                    objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 10))).FirstOrDefault();
                    if (objCurrent != null && objCurrent.DashboardReportFilterID > 0)
                    {
                        objDapperContext.Delete<DashboardReportFilter>(objCurrent.DashboardReportFilterID);
                    }
                    objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 11))).FirstOrDefault();
                    if (objCurrent != null && objCurrent.DashboardReportFilterID > 0)
                    {
                        objDapperContext.Delete<DashboardReportFilter>(objCurrent.DashboardReportFilterID);
                    }
                }
                else
                {
                    ChangeDateRangeSelection(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1), DateTime.Now, dashboardID);
                }
            }
            return true;
        }

        public bool ChangeDateRangeSelection(DateTime JOStartDate, DateTime JOEndDate, int dashboardID)
        {
            List<SystemEnum> lstElements = ((IEnumerable<SystemEnum.WorkStatusReportCriteria>)Enum.GetValues(typeof(SystemEnum.WorkStatusReportCriteria))).Select(o => new SystemEnum { ID = (int)o, Name = o.ToString() }).Where(p => p.ID == 10 || p.ID == 11).ToList();

            DashboardReportFilter ObjectListDashboardReportFilter = new DashboardReportFilter();
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                var result = objDapperContext.SearchAll(ObjectListDashboardReportFilter).Where(o => o.UserID == ProjectSession.EmployeeID && o.DashboardRepotMenuID == dashboardID).ToList();

                foreach (var lst in lstElements)
                {
                    DashboardReportFilter objDashboardReportFilter = new DashboardReportFilter();
                    objDashboardReportFilter.ReportUniqueName = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), 15);
                    objDashboardReportFilter.UserID = ProjectSession.EmployeeID;
                    objDashboardReportFilter.DashboardRepotMenuID = dashboardID;
                    DashboardReportFilter objCurrent = new DashboardReportFilter();
                    if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 10)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 10);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(JOStartDate.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 10))).FirstOrDefault();
                    }
                    else if (lst.Name.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 11)))
                    {
                        objDashboardReportFilter.FieldName = SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 11);
                        objDashboardReportFilter.FieldValue = ConvertTo.String(JOEndDate.ToShortDateString());
                        objCurrent = result.Where(o => o.FieldName.Equals(SystemEnum.GetEnumName(typeof(SystemEnum.WorkStatusReportCriteria), 11))).FirstOrDefault();
                    }
                    if (objCurrent == null)
                    {
                        objDashboardReportFilter.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.CreatedDate = DateTime.Now;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                    else
                    {
                        objDashboardReportFilter.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                        objDashboardReportFilter.ModifiedDate = DateTime.Now;
                        objDashboardReportFilter.DashboardReportFilterID = objCurrent.DashboardReportFilterID;
                        objDapperContext.Save(objDashboardReportFilter);
                    }
                }
            }
            return true;
        }

        public IList<WorkReportModal> GetWorkStatusReport(bool IsCentral, int L1ID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTradeID, string JobTypeID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<WorkReportModal> list = new List<WorkReportModal>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                var viewModel = new List<WorkReportModal>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    if (!IsCentral)
                    {
                        query += "SELECT 1 AS ID,'Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 1 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 2 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 3 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] From dual ";
                        query += "UNION ALL ";
                        query += "SELECT 2 AS ID,'Work Request' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 1 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 2 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 3 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Cancel] From dual ";
                        query += "UNION ALL ";
                        query += "SELECT 3 AS ID,'PM Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 1 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 2 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 3 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) Cancel From dual ";
                        query += " UNION ALL ";
                        query += "SELECT 4 AS ID,'Purchase Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += " (po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised') ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "po.OrderStatus = 'Closed'";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "po.OrderStatus = 'Cancelled'";
                        query += "),0) AS [Cancel] From dual  ";
                        query += " UNION ALL ";
                        query += "SELECT 5 AS ID,'Purchase Request' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 1 ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 2 ";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 3 ";
                        query += "),0) Cancel From dual ";

                    }
                    else
                    {
                        query += "SELECT 1 AS ID,'Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 1 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0)  [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 2 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 3 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0)  [Cancel] From dual ";
                        query += "UNION ALL ";
                        query += "SELECT 2 AS ID,'Work Request' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.RequestStatusID = 1 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0)  [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.RequestStatusID = 2 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.RequestStatusID = 3 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Cancel] From dual ";
                        query += "UNION ALL ";
                        query += "SELECT 3 AS ID,'PM Work Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 1 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 2 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = 3 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] From dual ";

                        query += " UNION ALL ";
                        query += "SELECT 4 AS ID,'Purchase Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += " (po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised') ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "po.OrderStatus = 'Closed'";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "po.OrderStatus = 'Cancelled'";
                        query += "),0) AS [Cancel] From dual ";
                        query += " UNION ALL ";
                        query += "SELECT 5 AS ID,'Purchase Request' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "pr.status_id = 1 ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "pr.status_id = 2 ";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "pr.status_id = 3 ";
                        query += "),0) AS [Cancel] From dual ";

                    }

                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT 1 AS ID,'Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 1 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 2 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 3 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] ";
                        query += "UNION ALL ";
                        query += "SELECT 2 AS ID,'Work Request' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 1 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 2 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = 3 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Cancel] ";
                        query += "UNION ALL ";
                        query += "SELECT 3 AS ID,'PM Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 1 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 2 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = 3 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] ";
                        query += " UNION ALL ";
                        query += "SELECT 4 AS ID,'Purchase Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += " (po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised') ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "po.OrderStatus = 'Closed'";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "po.OrderStatus = 'Cancelled'";
                        query += "),0) AS [Cancel] ";
                        query += " UNION ALL ";
                        query += "SELECT 5 AS ID,'Purchase Request' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 1 ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 2 ";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = 3 ";
                        query += "),0) AS [Cancel] ";

                    }
                    else
                    {
                        query += "SELECT 1 AS ID,'Work Order' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 1 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 2 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 3 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] ";
                        query += "UNION ALL ";
                        query += "SELECT 2 AS ID,'Work Request' AS Tasks";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.RequestStatusID = 1 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.RequestStatusID = 2 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM worequest w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.RequestStatusID = 3 ";
                        query += "GROUP BY w.RequestStatusID";
                        query += "),0) AS [Cancel] ";
                        query += "UNION ALL ";
                        query += "SELECT 3 AS ID,'PM Work Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 1 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 2 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM workorders w ";
                        query += " inner join L2 on L2.L2ID = w.L2ID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = 3 AND w.PMID > 0 ";
                        query += "GROUP BY w.WorkStatusID";
                        query += "),0) AS [Cancel] ";

                        query += " UNION ALL ";
                        query += "SELECT 4 AS ID,'Purchase Order' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += " (po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised') ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "po.OrderStatus = 'Closed'";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseOrder po ";
                        query += " inner join L2 on L2.L2ID = po.L2ID ";
                        query += "WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "po.OrderStatus = 'Cancelled'";
                        query += "),0) AS [Cancel] ";
                        query += " UNION ALL ";
                        query += "SELECT 5 AS ID,'Purchase Request' AS Tasks ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "pr.status_id = 1 ";
                        query += "),0) AS [Open] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "pr.status_id = 2 ";
                        query += "),0) AS [Close] ";
                        query += ",ISNULL((SELECT COUNT(*) FROM PurchaseRequest pr ";
                        query += " inner join L2 on L2.L2ID = pr.L2ID ";
                        query += "WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += " (L2.L1ID = '" + L1ID + "' or ISNULL('" + L1ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "pr.status_id = 3 ";
                        query += "),0) AS [Cancel] ";


                    }
                }



                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkReportModal>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                foreach (var x in list)
                {
                    viewModel.Add(new WorkReportModal { ID = x.ID, Tasks = x.Tasks, Open = x.Open, Close = x.Close, Cancel = x.Cancel, Total = x.Open + x.Close + x.Cancel, OpenP = ConvertTo.Decimal(x.Open * 100.00 / (x.Open + x.Close + x.Cancel)), CloseP = ConvertTo.Decimal(x.Close * 100.00 / (x.Open + x.Close + x.Cancel)), CancelP = ConvertTo.Decimal(x.Cancel * 100.00 / (x.Open + x.Close + x.Cancel)) });
                }
                return viewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkStatusDetailModal> GetWorkOrderStatus(bool IsCentral, int workStatusID, int pmId, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTradeID, string JobTypeID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkStatusDetailModal> list = new List<WorkStatusDetailModal>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });
                    if (!IsCentral)
                    {
                        query += "SELECT w.WorkorderNo AS Number,e.Name AS Name,w.DateReceived  AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus ";
                        query += "FROM workorders w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.WorkStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequestorID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        if (pmId > 0)
                        {
                            query += "w.PMID > 0 AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = " + workStatusID;
                    }
                    else
                    {
                        query += "SELECT w.WorkorderNo AS Number,e.Name AS Name,w.DateReceived  AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus ";
                        query += "FROM workorders w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.WorkStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequestorID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        if (pmId > 0)
                        {
                            query += "w.PMID > 0 AND ";
                        }
                        query += "(w.DateReceived between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.WorkStatusID = " + workStatusID;
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT w.WorkorderNo AS Number,e.Name AS Name,w.DateReceived  AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus ";
                        query += "FROM workorders w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.WorkStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequestorID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        if (pmId > 0)
                        {
                            query += "w.PMID > 0 AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.WorkStatusID = " + workStatusID;
                    }
                    else
                    {
                        query += "SELECT w.WorkorderNo AS Number,e.Name AS Name,w.DateReceived  AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus ";
                        query += "FROM workorders w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.WorkStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequestorID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.maintdeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.WOTradeID in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        if (pmId > 0)
                        {
                            query += "w.PMID > 0 AND ";
                        }
                        query += "(w.DateReceived between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.WorkStatusID = " + workStatusID;
                    }
                }



                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query = "Select * from (" + query + ") A Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkStatusDetailModal>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkStatusDetailModal> GetWorkRequestStatus(bool IsCentral, int workStatusID, int L2ID, string L3ID, string L4ID, string L5ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, string JobTradeID, string JobTypeID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkStatusDetailModal> list = new List<WorkStatusDetailModal>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });
                    if (!IsCentral)
                    {
                        query += "SELECT w.RequestNo AS Number,e.Name AS Name,w.ReceivedDate AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus  ";
                        query += "FROM worequest w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.RequestStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequesterID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate ) AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = " + workStatusID;
                    }
                    else
                    {
                        query += "SELECT w.RequestNo AS Number,e.Name AS Name,w.ReceivedDate AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus  ";
                        query += "FROM worequest w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.RequestStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequesterID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "w.RequestStatusID = " + workStatusID;
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT w.RequestNo AS Number,e.Name AS Name,w.ReceivedDate AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus  ";
                        query += "FROM worequest w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.RequestStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequesterID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(w.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "w.RequestStatusID = " + workStatusID;
                    }
                    else
                    {
                        query += "SELECT w.RequestNo AS Number,e.Name AS Name,w.ReceivedDate AS ReceiveDate,l.L2Code AS L2Code,w2.WorkStatus AS WorkStatus  ";
                        query += "FROM worequest w ";
                        query += "INNER JOIN workstatus w2 ON w2.WorkStatusID = w.RequestStatusID  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = w.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = w.RequesterID ";
                        query += " WHERE (w.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        if (L3ID.Length > 0 && ProjectSession.IsAreaFieldsVisible)
                        {
                            query += "(w.L3ID in (" + L3ID + ")) AND ";
                        }
                        if (L4ID.Length > 0)
                        {
                            query += "(w.L4ID in (" + L4ID + ")) AND ";
                        }
                        if (L5ID.Length > 0)
                        {
                            query += "(w.L5ID in (" + L5ID + ")) AND ";
                        }
                        query += "(w.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(w.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(w.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        if (JobTradeID.Length > 0)
                        {
                            query += "(w.worktradeid in (" + JobTradeID + ")) AND ";
                        }
                        if (JobTypeID.Length > 0)
                        {
                            query += "(w.WorkTypeID in (" + JobTypeID + ")) AND ";
                        }
                        query += "(w.ReceivedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "w.RequestStatusID = " + workStatusID;
                    }
                }


                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query = "Select * from (" + query + ") A Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkStatusDetailModal>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkStatusDetailModal> GetPurchaseOrderStatus(bool IsCentral, int workStatusID, int L2ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkStatusDetailModal> list = new List<WorkStatusDetailModal>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });
                    if (!IsCentral)
                    {
                        query += "SELECT po.PurchaseOrderNo AS Number,e.Name AS Name,po.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,po.OrderStatus AS WorkStatus   ";
                        query += "FROM PurchaseOrder po ";
                        query += "LEFT JOIN L2 l ON l.L2ID = po.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = po.OrderBy ";
                        query += " WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        if (workStatusID == 1)
                        {
                            query += "po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised' ";
                        }
                        else if (workStatusID == 2)
                        {
                            query += "po.OrderStatus = 'Closed' ";
                        }
                        else
                        {
                            query += " po.OrderStatus = 'Cancelled' ";
                        }
                    }
                    else
                    {
                        query += "SELECT po.PurchaseOrderNo AS Number,e.Name AS Name,po.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,po.OrderStatus AS WorkStatus   ";
                        query += "FROM PurchaseOrder po ";
                        query += "LEFT JOIN L2 l ON l.L2ID = po.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = po.OrderBy ";
                        query += " WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between @JOStartDate and @JOCompletedDate) AND ";
                        if (workStatusID == 1)
                        {
                            query += "po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised' ";
                        }
                        else if (workStatusID == 2)
                        {
                            query += "po.OrderStatus = 'Closed' ";
                        }
                        else
                        {
                            query += " po.OrderStatus = 'Cancelled' ";
                        }
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT po.PurchaseOrderNo AS Number,e.Name AS Name,po.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,po.OrderStatus AS WorkStatus   ";
                        query += "FROM PurchaseOrder po ";
                        query += "LEFT JOIN L2 l ON l.L2ID = po.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = po.OrderBy ";
                        query += " WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(po.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        if (workStatusID == 1)
                        {
                            query += "po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised' ";
                        }
                        else if (workStatusID == 2)
                        {
                            query += "po.OrderStatus = 'Closed' ";
                        }
                        else
                        {
                            query += " po.OrderStatus = 'Cancelled' ";
                        }
                    }
                    else
                    {
                        query += "SELECT po.PurchaseOrderNo AS Number,e.Name AS Name,po.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,po.OrderStatus AS WorkStatus   ";
                        query += "FROM PurchaseOrder po ";
                        query += "LEFT JOIN L2 l ON l.L2ID = po.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = po.OrderBy ";
                        query += " WHERE (po.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(po.MaintDivisionID = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(po.maintDeptID in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(po.maintsubdeptID in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(po.QuotationDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        if (workStatusID == 1)
                        {
                            query += "po.OrderStatus = 'Authorised' OR po.OrderStatus = 'Not Authorised' ";
                        }
                        else if (workStatusID == 2)
                        {
                            query += "po.OrderStatus = 'Closed' ";
                        }
                        else
                        {
                            query += " po.OrderStatus = 'Cancelled' ";
                        }
                    }
                }



                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query = "Select * from (" + query + ") A Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkStatusDetailModal>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkStatusDetailModal> GetPurchaseRequestStatus(bool IsCentral, int workStatusID, int L2ID, int MaintDivisionID, string MaintDeptID, string MaintSubDeptID, DateTime JOStartDate, DateTime JOCompletedDate, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkStatusDetailModal> list = new List<WorkStatusDetailModal>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if database is oracle or MySql this query will be execute
                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "JOStartDate",
                        Value = ConvertTo.Date(JOStartDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "JOCompletedDate",
                        Value = ConvertTo.Date(JOCompletedDate.ToShortDateString()),
                        DBType = DbType.DateTime
                    });
                    if (!IsCentral)
                    {
                        query += "SELECT pr.PurchaseRequestID AS Number,e.Name AS Name,pr.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,prs.status_desc AS WorkStatus    ";
                        query += "FROM PurchaseRequest pr ";
                        query += "INNER JOIN purchase_req_status prs ON prs.pr_status_id = pr.status_id  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = pr.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = pr.RequestedBy  ";
                        query += " WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = " + workStatusID;


                    }
                    else
                    {
                        query += "SELECT pr.PurchaseRequestID AS Number,e.Name AS Name,pr.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,prs.status_desc AS WorkStatus    ";
                        query += "FROM PurchaseRequest pr ";
                        query += "INNER JOIN purchase_req_status prs ON prs.pr_status_id = pr.status_id  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = pr.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = pr.RequestedBy  ";
                        query += " WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between @JOStartDate and @JOCompletedDate) AND ";
                        query += "pr.status_id = " + workStatusID;
                    }
                }
                //if database is SQL this query will be execute
                else
                {
                    if (!IsCentral)
                    {
                        query += "SELECT pr.PurchaseRequestID AS Number,e.Name AS Name,pr.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,prs.status_desc AS WorkStatus    ";
                        query += "FROM PurchaseRequest pr ";
                        query += "INNER JOIN purchase_req_status prs ON prs.pr_status_id = pr.status_id  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = pr.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = pr.RequestedBy  ";
                        query += " WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "(pr.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + ")) AND ";
                        query += "pr.status_id = " + workStatusID;


                    }
                    else
                    {
                        query += "SELECT pr.PurchaseRequestID AS Number,e.Name AS Name,pr.CreatedDate AS ReceiveDate,l.L2Code AS L2Code,prs.status_desc AS WorkStatus    ";
                        query += "FROM PurchaseRequest pr ";
                        query += "INNER JOIN purchase_req_status prs ON prs.pr_status_id = pr.status_id  ";
                        query += "LEFT JOIN L2 l ON l.L2ID = pr.L2ID ";
                        query += "LEFT JOIN employees e ON e.EmployeeID = pr.RequestedBy  ";
                        query += " WHERE (pr.L2ID = '" + L2ID + "' or ISNULL('" + L2ID + "', 0) = 0) AND ";
                        query += "(pr.MaintdivId = '" + MaintDivisionID + "' or ISNULL('" + MaintDivisionID + "' , 0) = 0) AND ";
                        if (MaintDeptID.Length > 0)
                        {
                            query += "(pr.MaintdeptId in (" + MaintDeptID + ")) AND ";
                        }
                        if (MaintSubDeptID.Length > 0)
                        {
                            query += "(pr.MainSubDeptId in (" + MaintSubDeptID + ")) AND ";
                        }
                        query += "(pr.CreatedDate between '" + JOStartDate.ToShortDateString() + "' and '" + JOCompletedDate.ToShortDateString() + "') AND ";
                        query += "pr.status_id = " + workStatusID;
                    }
                }

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query = "Select * from (" + query + ") A Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkStatusDetailModal>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public bool UpdateDisplayOrderByWidget(List<DashboardReportMenu> lstMenu)
        {
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    if (lstMenu.Count > 0)
                    {
                        string strQuery = string.Empty;
                        foreach (var lst in lstMenu)
                        {
                            strQuery = "Update DashboardReportMenu SET DisplayOrder = " + lst.DisplayOrder + " WHERE DashboardRepotMenuID = " + lst.DashboardRepotMenuID;
                            objDapperContext.ExecuteQuery(strQuery, null);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
