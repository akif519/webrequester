﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CMMS.Service.UserAuthentication
{
    public class UserAuthenticationData : DBExecute
    {
        public UserAuthenticationData()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Get Account
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public static Account GetAccount(int clientId)
        {
            
                //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                #region OldLogic
                //using (ServiceContext context = new ServiceContext())
                //{
                //    Account objAccount = new Account();
                //    objAccount.ClientID = clientId;
                //    return context.Search(objAccount, null, null, null).FirstOrDefault();
                //}
                #endregion

                try
                {
                    string strQuery = string.Empty;
                    Account objAccount;
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                    using (DapperContext objDapperContext = new DapperContext())
                    {

                        strQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";

                        Collection<DBParameters> parameters = new Collection<DBParameters>();
                        parameters.Add(new DBParameters()
                        {
                            Name = "ClientID",
                            Value = clientId,
                            DBType = DbType.Int32
                        });

                        objAccount = objDapperContext.ExecuteQuery<Account>(strQuery, parameters).FirstOrDefault();

                        //objDapperContext.ex
                    }
                    return objAccount;
                }
                catch (Exception ex)
                {
                    throw ex;
                }           
        }

        public static Account GetAccountWithClientCode(string ClientCode)
        {
            try
            {
                //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                #region OldLogic
                //using (ServiceContext context = new ServiceContext())
                //{
                //    Account objAccount = new Account();
                //    objAccount.ClientID = clientID;
                //    return context.Search(objAccount, null, null, null).FirstOrDefault();
                //}
                #endregion

                #region NewLogic
                Account objAdminUser = new Account();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "ClientCode",
                    Value = ClientCode,
                    DBType = DbType.String
                });

                string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] where CONVERT(varchar(max), DecryptByKey(ClientCode)) = @ClientCode ";
                //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName  and CONVERT(varchar(max), DecryptByKey([Password])) = @Password
                using (DapperDBContext context = new DapperDBContext())
                {
                    objAdminUser = context.ExecuteQuery<Account>(query, parameters).FirstOrDefault();
                }
                return objAdminUser;
                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ModulesAccess GetAccountWithModuleAccess(int ClientID)
        {
            try
            {
                //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
               

                #region NewLogic
                ModulesAccess objModuleAccess = new ModulesAccess();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "ClientID",
                    Value = ClientID,
                    DBType = DbType.Int32
                });

                string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(JobOrder)) AS 'JobOrder',CONVERT(bit, DecryptByKey(JobRequest)) AS 'JobRequest',CONVERT(bit, DecryptByKey(Assets)) AS 'Assets',CONVERT(bit, DecryptByKey(Locations)) AS 'Locations',CONVERT(bit, DecryptByKey(PreventiveMaintenance)) AS 'PreventiveMaintenance',CONVERT(bit, DecryptByKey(Store)) AS 'Store',CONVERT(bit, DecryptByKey(Purchasing)) AS 'Purchasing',CONVERT(bit, DecryptByKey(Employee)) AS 'Employee',CONVERT(bit, DecryptByKey(Cleaning)) AS 'Cleaning',CONVERT(bit, DecryptByKey(CEUML)) AS 'CEUML',CONVERT(bit, DecryptByKey(Sector)) AS 'Sector',CONVERT(bit, DecryptByKey(Zone)) AS 'Zone',CONVERT(bit, DecryptByKey(Building)) AS 'Building',CONVERT(bit, DecryptByKey(Division)) AS 'Division',CONVERT(bit, DecryptByKey(Department)) AS 'Department',CONVERT(bit, DecryptByKey(SubDept)) AS 'SubDept',CONVERT(bit, DecryptByKey(Groups)) AS 'Groups',CONVERT(bit, DecryptByKey(FailureCodes)) AS 'FailureCodes',CONVERT(bit, DecryptByKey(Suppliers_Contractors)) AS 'Suppliers_Contractors',CONVERT(bit, DecryptByKey(BoMList)) AS 'BoMList',CONVERT(bit, DecryptByKey(CostCenter)) AS 'CostCenter',CONVERT(bit, DecryptByKey(AccountCode)) AS 'AccountCode',CONVERT(bit, DecryptByKey(JobPlans)) AS 'JobPlans',CONVERT(bit, DecryptByKey(SafetyInstructions)) AS 'SafetyInstructions',CONVERT(bit, DecryptByKey(SiteSetups)) AS 'SiteSetups',CONVERT(bit, DecryptByKey(StoreSetups)) AS 'StoreSetups',CONVERT(bit, DecryptByKey(PurchaseRequisition)) AS 'PurchaseRequisition',CONVERT(bit, DecryptByKey(JobManagement)) AS 'JobManagement',CONVERT(bit, DecryptByKey(AssetSetup)) AS 'AssetSetup',CONVERT(bit, DecryptByKey(Employee_User)) AS 'Employee_User',CONVERT(bit, DecryptByKey(CleaningSetup)) AS 'CleaningSetup',CONVERT(bit, DecryptByKey(PurchaseSetup)) AS 'PurchaseSetup',CONVERT(bit, DecryptByKey(NotificationSetup)) AS 'NotificationSetup',CONVERT(bit, DecryptByKey(ItemRequisition)) AS 'ItemRequisition', CONVERT(bit, DecryptByKey(RequestCard)) AS 'RequestCard',CONVERT(bit, DecryptByKey(SLA)) AS 'SLA' FROM dbo.ModulesAccess where ClientID = @ClientID ";
                //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName  and CONVERT(varchar(max), DecryptByKey([Password])) = @Password
                using (DapperDBContext context = new DapperDBContext())
                {
                    objModuleAccess = context.ExecuteQuery<ModulesAccess>(query, parameters).FirstOrDefault();
                }
                return objModuleAccess;
                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportAccess GetAccountWithReportAccess(int ClientID)
        {
            try
            {
                //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;

                #region NewLogic
                ReportAccess objReportAccess = new ReportAccess();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "ClientID",
                    Value = ClientID,
                    DBType = DbType.Int32
                });
                
                string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(int, DecryptByKey(Assets)) AS 'Assets',CONVERT(int, DecryptByKey(Preventive)) AS 'Preventive',CONVERT(int, DecryptByKey(Store)) AS 'Store',CONVERT(int, DecryptByKey(Employee)) AS 'Employee',CONVERT(int, DecryptByKey(Setups)) AS 'Setups',CONVERT(int, DecryptByKey(Purchasing)) AS 'Purchasing',CONVERT(int, DecryptByKey(Cleaning)) AS 'Cleaning',CONVERT(int, DecryptByKey([JobOrder])) AS 'JobOrder' FROM [dbo].[ReportAccess]  WHERE ClientID = @ClientID";
                using (DapperDBContext context = new DapperDBContext())
                {
                    objReportAccess = context.ExecuteQuery<ReportAccess>(query, parameters).FirstOrDefault();
                }
                return objReportAccess;
                #endregion


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Account GetAccountWithConnection(int clientCode, int DBType)
        {
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            try
            {
                #region Oldlogic
                //using (ServiceContext context = new ServiceContext())
                //{
                //    Account objAccount = new Account();
                //    objAccount.ClientCode = clientCode;
                //    return context.SearchWithConnectionString(objAccount, null, null, null, DBType, "").FirstOrDefault();
                //}
                #endregion

                #region NewLogic
                Account objAdminUser = new Account();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "ClientId",
                    Value = clientCode,
                    DBType = DbType.Int32
                });

                string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] where ClientId = @ClientId ";
                //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName  and CONVERT(varchar(max), DecryptByKey([Password])) = @Password
                using (DapperDBContext context = new DapperDBContext())
                {
                    context.PagingInformation =  new Pagination { PageSize =10, PagerSize = 10 };
                    string Connectionsting = System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString;
                    objAdminUser = context.ExecuteQuery<Account>(query, parameters, 0, DBType, Connectionsting).FirstOrDefault();
                    //objAdminUser = context.ExecuteQuery<Account>(query, parameters).FirstOrDefault();
                }
                return objAdminUser;
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     


        /// <summary>
        /// Get Tab Access by Client ID
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public static TabAccess GetTabAccess(string clientCode)
        {
            try
            {
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                TabAccess objTabAccessClientWise = new TabAccess();
                TabAccess objTabAccessEmployeeWise = new TabAccess();
                TabAccess objTabAccessFinal = new TabAccess();

                Account objAccount = UserAuthenticationData.GetAccountWithClientCode(clientCode);
                int clientID = objAccount.ClientID;

                using (ServiceContext context = new ServiceContext())
                {
                    TabAccess objTabAccess = new TabAccess();
                    objTabAccess.ClientID = clientID;
                    objTabAccessClientWise = context.Search(objTabAccess, null, null, null).FirstOrDefault();
                }

                EmployeeService objService = new EmployeeService();
                objTabAccessEmployeeWise = objService.GetTabRightsOfEmployee(ProjectSession.EmployeeID);

                if (objTabAccessClientWise != null && objTabAccessEmployeeWise != null)
                {
                    objTabAccessFinal = new TabAccess()
                    {
                        Transactions = Convert.ToBoolean(objTabAccessClientWise.Transactions) && Convert.ToBoolean(objTabAccessEmployeeWise.Transactions),
                        Dashboard = Convert.ToBoolean(objTabAccessClientWise.Dashboard) && Convert.ToBoolean(objTabAccessEmployeeWise.Dashboard),
                        Reports = Convert.ToBoolean(objTabAccessClientWise.Reports) && Convert.ToBoolean(objTabAccessEmployeeWise.Reports),
                        Setup = Convert.ToBoolean(objTabAccessClientWise.Setup) && Convert.ToBoolean(objTabAccessEmployeeWise.Setup)
                    };
                }
                else
                {
                    objTabAccessFinal = new TabAccess()
                    {
                        Transactions = false,
                        Dashboard = false,
                        Reports = false,
                        Setup = false
                    };
                }

                return objTabAccessFinal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static TabAccess GetAccountWithTabAccess(int ClientID)
        {
            try
            {
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                TabAccess objTabAccessClientWise = new TabAccess();
                TabAccess objTabAccessEmployeeWise = new TabAccess();
                TabAccess objTabAccessFinal = new TabAccess();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "ClientID",
                    Value = ClientID,
                    DBType = DbType.String
                });

                string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(Transactions)) AS 'Transactions',CONVERT(bit, DecryptByKey(Setup)) AS 'Setup',CONVERT(bit, DecryptByKey(Reports)) AS 'Reports',CONVERT(bit, DecryptByKey(Dashboard)) AS 'Dashboard'FROM dbo.TabAccess WHERE ClientID = @ClientID";
                
                using (DapperDBContext context = new DapperDBContext())
                {
                    objTabAccessClientWise = context.ExecuteQuery<TabAccess>(query, parameters).FirstOrDefault();
                }

                EmployeeService objService = new EmployeeService();
                objTabAccessEmployeeWise = objService.GetTabRightsOfEmployee(ProjectSession.EmployeeID);

                if (objTabAccessClientWise != null && objTabAccessEmployeeWise != null)
                {
                    objTabAccessFinal = new TabAccess()
                    {
                        Transactions = Convert.ToBoolean(objTabAccessClientWise.Transactions) && Convert.ToBoolean(objTabAccessEmployeeWise.Transactions),
                        Dashboard = Convert.ToBoolean(objTabAccessClientWise.Dashboard) && Convert.ToBoolean(objTabAccessEmployeeWise.Dashboard),
                        Reports = Convert.ToBoolean(objTabAccessClientWise.Reports) && Convert.ToBoolean(objTabAccessEmployeeWise.Reports),
                        Setup = Convert.ToBoolean(objTabAccessClientWise.Setup) && Convert.ToBoolean(objTabAccessEmployeeWise.Setup)
                    };
                }
                else
                {
                    objTabAccessFinal = new TabAccess()
                    {
                        Transactions = false,
                        Dashboard = false,
                        Reports = false,
                        Setup = false
                    };
                }

                return objTabAccessFinal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Module Access by Client ID
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public static ModulesAccess GetModulesAccess(string clientCode)
        {
            try
            {

                Account objAccount = GetAccountWithClientCode(clientCode);
                int clientID = objAccount.ClientID;

                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (ServiceContext context = new ServiceContext())
                {
                    ModulesAccess objModulesAccess = new ModulesAccess();
                    objModulesAccess.ClientID = clientID;
                    return context.Search(objModulesAccess, null, null, null).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get First Level Table Name
        /// </summary>
        /// <param name="clientID"></param>
        /// <returns></returns>
        public static FirstLevelTbl GetFirstLevelTbl(int clientID)
        {
            try
            {
                string strQuery;
                FirstLevelTbl objFirstLevelTbl;
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (DapperContext objDapperContext = new DapperContext())
                {

                    strQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,L1_ID,CONVERT(nvarchar(max), DecryptByKey(L1_en)) AS 'L1_en',CONVERT(nvarchar(max), DecryptByKey(L1_ar)) AS 'L1_ar' FROM dbo.FirstLevelTbl WHERE ClientID = @clientID";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });

                    objFirstLevelTbl = objDapperContext.ExecuteQuery<FirstLevelTbl>(strQuery, parameters).FirstOrDefault();

                    //objDapperContext.ex
                }
                return objFirstLevelTbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Level Names by L1_ID
        /// </summary>
        /// <param name="L1_ID"></param>
        /// <returns></returns>
        public static LevelNamesTbl GetLevelNamesTbl(int L1_ID)
        {
            try
            {
                string strQuery;
                LevelNamesTbl objLevelNamesTbl;
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (DapperContext objDapperContext = new DapperContext())
                {

                    strQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(nvarchar(max), DecryptByKey(L2_en)) AS 'L2_en',CONVERT(nvarchar(max), DecryptByKey(L2_ar)) AS 'L2_ar',CONVERT(nvarchar(max), DecryptByKey(L3_en)) AS 'L3_en',CONVERT(nvarchar(max), DecryptByKey(L3_ar)) AS 'L3_ar',CONVERT(nvarchar(max), DecryptByKey(L4_en)) AS 'L4_en',CONVERT(nvarchar(max), DecryptByKey(L4_ar)) AS 'L4_ar',CONVERT(nvarchar(max), DecryptByKey(L5_en)) AS 'L5_en',CONVERT(nvarchar(max), DecryptByKey(L5_ar)) AS 'L5_ar'FROM [dbo].[LevelNamesTbl] Where L1_ID = @L1_ID";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "L1_ID", Value = L1_ID, DBType = DbType.Int32 });

                    objLevelNamesTbl = objDapperContext.ExecuteQuery<LevelNamesTbl>(strQuery, parameters).FirstOrDefault();

                    //objDapperContext.ex
                }
                return objLevelNamesTbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Tbl_Feature GetTblFeatureData(int ClientID)
        {
            try
            {
                string strQuery;
                Tbl_Feature objTblFeature;
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (DapperContext objDapperContext = new DapperContext())
                {

                    strQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebREquesterLicence)) AS 'WebRequesterLicence'FROM dbo.Tbl_Feature WHERE ClientID = @ClientID";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "ClientID", Value = ClientID, DBType = DbType.Int32 });

                    objTblFeature = objDapperContext.ExecuteQuery<Tbl_Feature>(strQuery, parameters).FirstOrDefault();
                }
                return objTblFeature;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static CEUML_config GetCemulData(int ClientID)
        {
            try
            {
                string strQuery;
                CEUML_config objCEUML_config;
                //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (DapperContext objDapperContext = new DapperContext())
                {

                    strQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(int, DecryptByKey(CEUML_configId)) AS 'CEUML_configId',CONVERT(Nvarchar(50), DecryptByKey(ceUser)) AS 'ceUser',CONVERT(Nvarchar(max), DecryptByKey(Token)) AS 'Token'FROM dbo.[CEUML_config] where ClientID = @ClientID";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "ClientID", Value = ClientID, DBType = DbType.Int32 });

                    objCEUML_config = objDapperContext.ExecuteQuery<CEUML_config>(strQuery, parameters).FirstOrDefault();
                }
                return objCEUML_config;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check User SignIn
        /// </summary>
        /// <param name="strUsername"></param>
        /// <param name="strPassword"></param>
        /// <returns></returns>
        public employees CheckUserSignIn(string strUsername, string strPassword)
        {
            try
            {
                employees objemployees;
                string strQuery;
                using (DapperContext objDapperContext = new DapperContext())
                {

                    strQuery = "Select EmployeeID, EmployeeNo,Name,UserID, password, LanguageCode, ModifiedDate";
                    strQuery += " from employees ";
                    strQuery += " Where UserID = @UserID and password = @password ";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "UserID", Value = strUsername, DBType = DbType.String });
                    parameters.Add(new DBParameters() { Name = "password", Value = strPassword, DBType = DbType.String });


                    objemployees = objDapperContext.ExecuteQuery<employees>(strQuery, parameters).FirstOrDefault();

                    //objDapperContext.ex
                }

                // employees objemployees = this.ExecuteProcedure<employees>("UspLogin", parameters).FirstOrDefault();
                return objemployees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates the login log.
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        public static void UpdateLoginLog(int employeeID, DateTime time)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "LogoutDateTime",
                    Value = time,
                    DBType = DbType.DateTime
                });

                string query = "Update LoginLog set LogoutDateTime = @LogoutDateTime where employeeId = " + employeeID + " and LogoutDateTime is null";

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void DeleteSession(int employeeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = "Delete from sessions where EmployeeId = " + employeeID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteSessions(int SessionTimeOut)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = "Delete from sessions where lastheartbeat < '" + Common.setQuote(Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Minute, -SessionTimeOut, Convert.ToDateTime(Common.GetEnglishDateSecond(DateTime.Now))))) + "'";

                if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                      parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Minute, -SessionTimeOut, Convert.ToDateTime(Common.GetEnglishDateSecond(DateTime.Now))),
                        DBType = DbType.DateTime
                    });

                      query = "Delete from sessions where lastheartbeat < @CurrentDate";
                }

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateSessionsLastHeartBeat(int employeeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "CurrentDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });

                string query = "Update Sessions set lastheartbeat = @CurrentDate where employeeId = " + employeeID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteErrorLog(string errorLogID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = "Delete from ErrorLog where ErrorLogId IN (" + errorLogID + ") ";

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query, parameters);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
