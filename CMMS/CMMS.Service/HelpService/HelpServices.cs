﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CMMS.Service
{
    public class HelpServices : DBExecute
    {
        public HelpServices()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public static IList<HelpModule> GetTopics(int ParentID, int Lavel, int EmployeeID, int LanguageID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "ParentID",
                Value = ParentID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = EmployeeID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = ProjectSession.AccountID,
                DBType = DbType.Int32
            });

            HelpModule objL1 = new HelpModule();

            var query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master; DECLARE @ModuleAccess AS TABLE" +
   "( " +
"PropertyName VARCHAR(50)," +
"Value bit" +
")" +
"INSERT INTO @ModuleAccess" +
       "( PropertyName, Value )" +      
" SELECT  Property ," +
       "Value " +
"FROM    ( " +
@"
SELECT    ClientID , 
[AccountCode] = CONVERT(bit, DecryptByKey([AccountCode])), 
[Assets] = CONVERT(bit, DecryptByKey( [Assets])) , 
[AssetSetup] = CONVERT(bit, DecryptByKey( [AssetSetup])), 
[AuditTrail] = CONVERT(bit, DecryptByKey( [AuditTrail])), 
[BoMList] = CONVERT(bit, DecryptByKey( [BoMList])) , 
[Building] = CONVERT(bit, DecryptByKey( [Building])) , 
[CEUML] = CONVERT(bit, DecryptByKey( [CEUML])) , 
[Cleaning] = CONVERT(bit, DecryptByKey( [Cleaning])) , 
[CleaningSetup] = CONVERT(bit, DecryptByKey([CleaningSetup])) , 
[CostCenter] = CONVERT(bit, DecryptByKey( [CostCenter])) , 
[Department] = CONVERT(bit, DecryptByKey( [Department])) , 
[Division] = CONVERT(bit, DecryptByKey( [Division])) , 
[Employee] = CONVERT(bit, DecryptByKey( [Employee])) , 
[Employee_User] = CONVERT(bit, DecryptByKey( [Employee_User]) ), 
[FailureCodes] = CONVERT(bit, DecryptByKey( [FailureCodes])) , 
[Groups] = CONVERT(bit, DecryptByKey( [Groups])) , 
[JobManagement] = CONVERT(bit, DecryptByKey( [JobManagement])) , 
[JobOrder] = CONVERT(bit, DecryptByKey( [JobOrder])) , 
[JobPlans] = CONVERT(bit, DecryptByKey( [JobPlans])) , 
[JobRequest] = CONVERT(bit, DecryptByKey( [JobRequest])) , 
[Locations] = CONVERT(bit, DecryptByKey( [Locations])) , 
[NotificationSetup] = CONVERT(bit, DecryptByKey( [NotificationSetup])) , 
[ItemRequisition] = CONVERT(bit, DecryptByKey( [ItemRequisition])) , 
[PreventiveMaintenance] = CONVERT(bit, DecryptByKey( [PreventiveMaintenance])) , 
[PurchaseRequisition] = CONVERT(bit, DecryptByKey( [PurchaseRequisition])) , 
[PurchaseSetup] = CONVERT(bit, DecryptByKey( [PurchaseSetup])) , 
[Purchasing] = CONVERT(bit, DecryptByKey( [Purchasing])) , 
[SafetyInstructions] = CONVERT(bit, DecryptByKey( [SafetyInstructions])) , 
[Sector] = CONVERT(bit, DecryptByKey( [Sector])) , 
[SiteSetups] = CONVERT(bit, DecryptByKey( [SiteSetups])) , 
[Store] = CONVERT(bit, DecryptByKey( [Store])) , 
[StoreSetups] = CONVERT(bit, DecryptByKey( [StoreSetups])) , 
[SubDept] = CONVERT(bit, DecryptByKey( [SubDept])) , 
[Suppliers_Contractors] = CONVERT(bit, DecryptByKey([Suppliers_Contractors])) , 
[Zone] = CONVERT(bit, DecryptByKey( [Zone]) ) 
FROM dbo.ModulesAccess " +
         "WHERE     ClientID = @ClientID " +
       ") AS t UNPIVOT " +
"( Value FOR Property IN ( [AccountCode], [Assets], [AssetSetup], [AuditTrail], " +
                         "[BoMList], [Building], [CEUML], [Cleaning], " +
                         "[CleaningSetup], [CostCenter], [Department], " +
                         "[Division], [Employee], [Employee_User], " +
                         "[FailureCodes], [Groups], [JobManagement], " +
                         "[JobOrder], [JobPlans], [JobRequest], [Locations], " +
                         "[NotificationSetup],[ItemRequisition], [PreventiveMaintenance], " +
                         "[PurchaseRequisition], [PurchaseSetup], [Purchasing], " +
                         "[SafetyInstructions], [Sector], [SiteSetups], " +
                         "[Store], [StoreSetups], [SubDept], " +
                         "[Suppliers_Contractors], [Zone] ) ) AS up;  Select * from @ModuleAccess";


            ////            query +=
            ////"SELECT h.*,(select count(1) CNt from helpModule h1 where parentid = h.HelpModuleID) As ChildCount, hmd.HelpTitle,hmd.HelpModuelDetailID " +
            ////"FROM helpModule H " +
            ////"INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1 AND H.ParentID = @ParentID " +
            ////" INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and LanguageID = @LanguageID order by h.HelpModuleID";


            using (ServiceContext context = new ServiceContext())
            {
                ProjectSession.UseMasterDB = true;
                List<HelpModule> lst = context.ExecuteQuery<HelpModule>(query, parameters).ToList();

                HelpModule objHelpModule = new HelpModule();
                HelpModule_Detail objHelpModule_Detail = new HelpModule_Detail();
                ProjectSession.UseHelpDB = true;
                List<HelpModule> lstHelpModule = context.SearchAll<HelpModule>(objHelpModule).ToList();

                ProjectSession.UseHelpDB = true;
                List<HelpModule_Detail> lstHelpModule_Detail = context.SearchAll<HelpModule_Detail>(objHelpModule_Detail).ToList();

                List<HelpModule> lstResult = (from H in lstHelpModule.Where(x => x.ParentID == ParentID)
                                              join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                              join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                              select new HelpModule()
                                              {
                                                  HelpTitle = hmd.HelpTitle,
                                                  HelpModuelDetailID = hmd.HelpModuelDetailID,
                                                  HelpModuleID = H.HelpModuleID,
                                                  ModuleName = H.ModuleName,
                                                  ParentID = H.ParentID,
                                                  DisplayOrder = H.DisplayOrder,
                                                  HasHelpDescription = H.HasHelpDescription,
                                                  ModuleAccess = H.ModuleAccess,
                                                  PageName = H.PageName,
                                                  LanguageID = H.LanguageID,
                                                  ChildCount = lstHelpModule.Where(y => y.ParentID == H.HelpModuleID).ToList().Count
                                              }).OrderBy(x => x.HelpModuleID).ToList();

                return lstResult;

            }
        }

        public static IList<HelpModule> GetTopicsByIndex(string Index, int Lavel, int EmployeeID, int LanguageID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "Index",
                Value = Index,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = EmployeeID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = ProjectSession.AccountID,
                DBType = DbType.Int32
            });

            HelpModule objL1 = new HelpModule();


            var query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master; DECLARE @ModuleAccess AS TABLE" +
   "( " +
"PropertyName VARCHAR(50)," +
"Value bit" +
")" +
"INSERT INTO @ModuleAccess" +
       "( PropertyName, Value )" +
" SELECT  Property ," +
       "Value " +
"FROM    ( " +
@"
SELECT    ClientID , 
[AccountCode] = CONVERT(bit, DecryptByKey([AccountCode])), 
[Assets] = CONVERT(bit, DecryptByKey( [Assets])) , 
[AssetSetup] = CONVERT(bit, DecryptByKey( [AssetSetup])), 
[AuditTrail] = CONVERT(bit, DecryptByKey(  [AuditTrail])) , 
[BoMList] = CONVERT(bit, DecryptByKey( [BoMList])) , 
[Building] = CONVERT(bit, DecryptByKey( [Building])) , 
[CEUML] = CONVERT(bit, DecryptByKey( [CEUML])) , 
[Cleaning] = CONVERT(bit, DecryptByKey( [Cleaning])) , 
[CleaningSetup] = CONVERT(bit, DecryptByKey([CleaningSetup])) , 
[CostCenter] = CONVERT(bit, DecryptByKey( [CostCenter])) , 
[Department] = CONVERT(bit, DecryptByKey( [Department])) , 
[Division] = CONVERT(bit, DecryptByKey( [Division])) , 
[Employee] = CONVERT(bit, DecryptByKey( [Employee])) , 
[Employee_User] = CONVERT(bit, DecryptByKey( [Employee_User]) ), 
[FailureCodes] = CONVERT(bit, DecryptByKey( [FailureCodes])) , 
[Groups] = CONVERT(bit, DecryptByKey( [Groups])) , 
[JobManagement] = CONVERT(bit, DecryptByKey( [JobManagement])) , 
[JobOrder] = CONVERT(bit, DecryptByKey( [JobOrder])) , 
[JobPlans] = CONVERT(bit, DecryptByKey( [JobPlans])) , 
[JobRequest] = CONVERT(bit, DecryptByKey( [JobRequest])) , 
[Locations] = CONVERT(bit, DecryptByKey( [Locations])) , 
[NotificationSetup] = CONVERT(bit, DecryptByKey( [NotificationSetup])) , 
[ItemRequisition] = CONVERT(bit, DecryptByKey( [ItemRequisition])) , 
[PreventiveMaintenance] = CONVERT(bit, DecryptByKey( [PreventiveMaintenance])) , 
[PurchaseRequisition] = CONVERT(bit, DecryptByKey( [PurchaseRequisition])) , 
[PurchaseSetup] = CONVERT(bit, DecryptByKey( [PurchaseSetup])) , 
[Purchasing] = CONVERT(bit, DecryptByKey( [Purchasing])) , 
[SafetyInstructions] = CONVERT(bit, DecryptByKey( [SafetyInstructions])) , 
[Sector] = CONVERT(bit, DecryptByKey( [Sector])) , 
[SiteSetups] = CONVERT(bit, DecryptByKey( [SiteSetups])) , 
[Store] = CONVERT(bit, DecryptByKey( [Store])) , 
[StoreSetups] = CONVERT(bit, DecryptByKey( [StoreSetups])) , 
[SubDept] = CONVERT(bit, DecryptByKey( [SubDept])) , 
[Suppliers_Contractors] = CONVERT(bit, DecryptByKey([Suppliers_Contractors])) , 
[Zone] = CONVERT(bit, DecryptByKey( [Zone]) ) " +
         " FROM      dbo.ModulesAccess " +
         "WHERE     ClientID = @ClientID " +
       ") AS t UNPIVOT " +
"( Value FOR Property IN ( [AccountCode], [Assets], [AssetSetup], [AuditTrail], " +
                         "[BoMList], [Building], [CEUML], [Cleaning], " +
                         "[CleaningSetup], [CostCenter], [Department], " +
                         "[Division], [Employee], [Employee_User], " +
                         "[FailureCodes], [Groups], [JobManagement], " +
                         "[JobOrder], [JobPlans], [JobRequest], [Locations], " +
                         "[NotificationSetup],[ItemRequisition], [PreventiveMaintenance], " +
                         "[PurchaseRequisition], [PurchaseSetup], [Purchasing], " +
                         "[SafetyInstructions], [Sector], [SiteSetups], " +
                         "[Store], [StoreSetups], [SubDept], " +
                         "[Suppliers_Contractors], [Zone] ) ) AS up; Select * from @ModuleAccess";


            using (ServiceContext context = new ServiceContext())
            {
                ProjectSession.UseMasterDB = true;
                IList<HelpModule> lst = context.ExecuteQuery<HelpModule>(query, parameters);

                HelpModule objHelpModule = new HelpModule();
                HelpModule_Detail objHelpModule_Detail = new HelpModule_Detail();
                ProjectSession.UseHelpDB = true;
                List<HelpModule> lstHelpModule = context.SearchAll<HelpModule>(objHelpModule).ToList();

                ProjectSession.UseHelpDB = true;
                List<HelpModule_Detail> lstHelpModule_Detail = context.SearchAll<HelpModule_Detail>(objHelpModule_Detail).ToList();

                if (Lavel == 1)
                {
                    //query += "SELECT Distinct SUBSTRING (hmd.HelpTitle,1,1) as HelpTitle FROM helpModule H " +
                    //         " INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1 and H.HasHelpDescription = 1 " +
                    //        "INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and hmd.LanguageID = @LanguageID order by HelpTitle";


                    List<HelpModule> lstResult1 = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                                   join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                                   join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                                   select new HelpModule()
                                                   {
                                                       HelpTitle = hmd.HelpTitle.Substring(0, 1)
                                                   }).GroupBy(x => x.HelpTitle).Select(x => new HelpModule()
                                                   {
                                                       HelpTitle = x.Key
                                                   }).OrderBy(x => x.HelpTitle).ToList();

                    List<HelpModule> lstResult = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                                  join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                                  join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                                  select new HelpModule()
                                                  {
                                                      HelpTitle = hmd.HelpTitle.Substring(0, 1)
                                                  }).Distinct().OrderBy(x => x.HelpTitle).ToList();

                    return lstResult1;
                }
                else if (Lavel == 2 && Index != null)
                {
                    //query += "SELECT h.*,hmd.HelpTitle,hmd.HelpModuelDetailID " +
                    //         " FROM helpModule H " +
                    //         "INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1 AND H.HasHelpDescription = 1 " +
                    //          "INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and LanguageID = @LanguageID where HelpTitle like @Index +'%' ";


                    List<HelpModule> lstResult = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                                  join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                                  join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID && x.HelpTitle.StartsWith(Index)) on H.HelpModuleID equals hmd.HelpModuleID
                                                  select new HelpModule()
                                                  {
                                                      HelpTitle = hmd.HelpTitle,
                                                      HelpModuelDetailID = hmd.HelpModuelDetailID,
                                                      HelpModuleID = H.HelpModuleID,
                                                      ModuleName = H.ModuleName,
                                                      ParentID = H.ParentID,
                                                      DisplayOrder = H.DisplayOrder,
                                                      HasHelpDescription = H.HasHelpDescription,
                                                      ModuleAccess = H.ModuleAccess,
                                                      PageName = H.PageName,
                                                      LanguageID = H.LanguageID
                                                  }).OrderBy(x => x.HelpTitle).ToList();

                    return lstResult;
                }

                return lst;
            }
        }

        public static List<HelpBookmarks> GetHelpBookmarks(int EmployeeID, int LanguageID)
        {
            string query = string.Empty;
            List<HelpBookmarks> listFromMaster = new List<HelpBookmarks>();
            List<HelpBookmarks> listFromHBTable = new List<HelpBookmarks>();

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = EmployeeID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = ProjectSession.AccountID,
                DBType = DbType.Int32
            });

            query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master; DECLARE @ModuleAccess AS TABLE" +
   "( " +
"PropertyName VARCHAR(50)," +
"Value bit" +
")" +
"INSERT INTO @ModuleAccess" +
       "( PropertyName, Value )" +
" SELECT  Property ," +
       "Value " +
"FROM    ( " +
@"
SELECT    ClientID , 
[AccountCode] = CONVERT(bit, DecryptByKey([AccountCode])), 
[Assets] = CONVERT(bit, DecryptByKey( [Assets])) , 
[AssetSetup] = CONVERT(bit, DecryptByKey( [AssetSetup])), 
[AuditTrail] = CONVERT(bit, DecryptByKey(  [AuditTrail])) , 
[BoMList] = CONVERT(bit, DecryptByKey( [BoMList])) , 
[Building] = CONVERT(bit, DecryptByKey( [Building])) , 
[CEUML] = CONVERT(bit, DecryptByKey( [CEUML])) , 
[Cleaning] = CONVERT(bit, DecryptByKey( [Cleaning])) , 
[CleaningSetup] = CONVERT(bit, DecryptByKey([CleaningSetup])) , 
[CostCenter] = CONVERT(bit, DecryptByKey( [CostCenter])) , 
[Department] = CONVERT(bit, DecryptByKey( [Department])) , 
[Division] = CONVERT(bit, DecryptByKey( [Division])) , 
[Employee] = CONVERT(bit, DecryptByKey( [Employee])) , 
[Employee_User] = CONVERT(bit, DecryptByKey( [Employee_User]) ), 
[FailureCodes] = CONVERT(bit, DecryptByKey( [FailureCodes])) , 
[Groups] = CONVERT(bit, DecryptByKey( [Groups])) , 
[JobManagement] = CONVERT(bit, DecryptByKey( [JobManagement])) , 
[JobOrder] = CONVERT(bit, DecryptByKey( [JobOrder])) , 
[JobPlans] = CONVERT(bit, DecryptByKey( [JobPlans])) , 
[JobRequest] = CONVERT(bit, DecryptByKey( [JobRequest])) , 
[Locations] = CONVERT(bit, DecryptByKey( [Locations])) , 
[NotificationSetup] = CONVERT(bit, DecryptByKey( [NotificationSetup])) , 
[ItemRequisition] = CONVERT(bit, DecryptByKey( [ItemRequisition])) , 
[PreventiveMaintenance] = CONVERT(bit, DecryptByKey( [PreventiveMaintenance])) , 
[PurchaseRequisition] = CONVERT(bit, DecryptByKey( [PurchaseRequisition])) , 
[PurchaseSetup] = CONVERT(bit, DecryptByKey( [PurchaseSetup])) , 
[Purchasing] = CONVERT(bit, DecryptByKey( [Purchasing])) , 
[SafetyInstructions] = CONVERT(bit, DecryptByKey( [SafetyInstructions])) , 
[Sector] = CONVERT(bit, DecryptByKey( [Sector])) , 
[SiteSetups] = CONVERT(bit, DecryptByKey( [SiteSetups])) , 
[Store] = CONVERT(bit, DecryptByKey( [Store])) , 
[StoreSetups] = CONVERT(bit, DecryptByKey( [StoreSetups])) , 
[SubDept] = CONVERT(bit, DecryptByKey( [SubDept])) , 
[Suppliers_Contractors] = CONVERT(bit, DecryptByKey([Suppliers_Contractors])) , 
[Zone] = CONVERT(bit, DecryptByKey( [Zone]) )  " +
         " FROM      dbo.ModulesAccess " +
         "WHERE     ClientID = @ClientID " +
       ") AS t UNPIVOT " +
"( Value FOR Property IN ( [AccountCode], [Assets], [AssetSetup], [AuditTrail], " +
                         "[BoMList], [Building], [CEUML], [Cleaning], " +
                         "[CleaningSetup], [CostCenter], [Department], " +
                         "[Division], [Employee], [Employee_User], " +
                         "[FailureCodes], [Groups], [JobManagement], " +
                         "[JobOrder], [JobPlans], [JobRequest], [Locations], " +
                         "[NotificationSetup], [ItemRequisition],[PreventiveMaintenance], " +
                         "[PurchaseRequisition], [PurchaseSetup], [Purchasing], " +
                         "[SafetyInstructions], [Sector], [SiteSetups], " +
                         "[Store], [StoreSetups], [SubDept], " +
                         "[Suppliers_Contractors], [Zone] ) ) AS up; Select * from @ModuleAccess";

            //" SELECT H.HelpModuleID,hmd.HelpTitle,hmd.HelpModuelDetailID " +
            // "FROM helpModule H " +
            // "INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1 AND H.HasHelpDescription = 1 " +
            // "INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and LanguageID = @LanguageID ";           



            using (DapperDBContext context = new ServiceContext())
            {
                ProjectSession.UseMasterDB = true;
                IList<HelpModule> lst = context.ExecuteQuery<HelpModule>(query, parameters).ToList();

                HelpModule objHelpModule = new HelpModule();
                HelpModule_Detail objHelpModule_Detail = new HelpModule_Detail();
                ProjectSession.UseHelpDB = true;
                List<HelpModule> lstHelpModule = context.SearchAll<HelpModule>(objHelpModule).ToList();

                ProjectSession.UseHelpDB = true;
                List<HelpModule_Detail> lstHelpModule_Detail = context.SearchAll<HelpModule_Detail>(objHelpModule_Detail).ToList();

                listFromMaster = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                  join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                  join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                  select new HelpBookmarks()
                                  {
                                      HelpTitle = hmd.HelpTitle,
                                      HelpModuelDetailID = hmd.HelpModuelDetailID,
                                      HelpModuleID = H.HelpModuleID
                                  }).OrderBy(x => x.HelpTitle).ToList();
            }

            string query2 = @"select HelpModuleID from HelpBookmarks where EmployeeID = @EmployeeID ";
            using (DapperDBContext context = new ServiceContext())
            {
                listFromHBTable = context.ExecuteQuery<HelpBookmarks>(query2, parameters).ToList();
            }

            List<HelpBookmarks> joinedList = new List<HelpBookmarks>();
            joinedList = (from lstmaster in listFromMaster join lstord in listFromHBTable on lstmaster.HelpModuleID equals lstord.HelpModuleID select lstmaster).ToList();

            return joinedList;
        }

        public static List<HelpModule> GetSearchHelp(string SearchText, int LanguageID)
        {
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "SearchText",
                Value = SearchText,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = ProjectSession.EmployeeID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = ProjectSession.AccountID,
                DBType = DbType.Int32
            });

            query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master; DECLARE @ModuleAccess AS TABLE" +
   "( " +
"PropertyName VARCHAR(50)," +
"Value bit" +
")" +
"INSERT INTO @ModuleAccess" +
       "( PropertyName, Value )" +
" SELECT  Property ," +
       "Value " +
"FROM    ( " +
@"
SELECT    ClientID , 
[AccountCode] = CONVERT(bit, DecryptByKey([AccountCode])), 
[Assets] = CONVERT(bit, DecryptByKey( [Assets])) , 
[AssetSetup] = CONVERT(bit, DecryptByKey( [AssetSetup])), 
[AuditTrail] = CONVERT(bit, DecryptByKey(  [AuditTrail])) , 
[BoMList] = CONVERT(bit, DecryptByKey( [BoMList])) , 
[Building] = CONVERT(bit, DecryptByKey( [Building])) , 
[CEUML] = CONVERT(bit, DecryptByKey( [CEUML])) , 
[Cleaning] = CONVERT(bit, DecryptByKey( [Cleaning])) , 
[CleaningSetup] = CONVERT(bit, DecryptByKey([CleaningSetup])) , 
[CostCenter] = CONVERT(bit, DecryptByKey( [CostCenter])) , 
[Department] = CONVERT(bit, DecryptByKey( [Department])) , 
[Division] = CONVERT(bit, DecryptByKey( [Division])) , 
[Employee] = CONVERT(bit, DecryptByKey( [Employee])) , 
[Employee_User] = CONVERT(bit, DecryptByKey( [Employee_User]) ), 
[FailureCodes] = CONVERT(bit, DecryptByKey( [FailureCodes])) , 
[Groups] = CONVERT(bit, DecryptByKey( [Groups])) , 
[JobManagement] = CONVERT(bit, DecryptByKey( [JobManagement])) , 
[JobOrder] = CONVERT(bit, DecryptByKey( [JobOrder])) , 
[JobPlans] = CONVERT(bit, DecryptByKey( [JobPlans])) , 
[JobRequest] = CONVERT(bit, DecryptByKey( [JobRequest])) , 
[Locations] = CONVERT(bit, DecryptByKey( [Locations])) , 
[NotificationSetup] = CONVERT(bit, DecryptByKey( [NotificationSetup])) , 
[ItemRequisition] = CONVERT(bit, DecryptByKey( [ItemRequisition])) , 
[PreventiveMaintenance] = CONVERT(bit, DecryptByKey( [PreventiveMaintenance])) , 
[PurchaseRequisition] = CONVERT(bit, DecryptByKey( [PurchaseRequisition])) , 
[PurchaseSetup] = CONVERT(bit, DecryptByKey( [PurchaseSetup])) , 
[Purchasing] = CONVERT(bit, DecryptByKey( [Purchasing])) , 
[SafetyInstructions] = CONVERT(bit, DecryptByKey( [SafetyInstructions])) , 
[Sector] = CONVERT(bit, DecryptByKey( [Sector])) , 
[SiteSetups] = CONVERT(bit, DecryptByKey( [SiteSetups])) , 
[Store] = CONVERT(bit, DecryptByKey( [Store])) , 
[StoreSetups] = CONVERT(bit, DecryptByKey( [StoreSetups])) , 
[SubDept] = CONVERT(bit, DecryptByKey( [SubDept])) , 
[Suppliers_Contractors] = CONVERT(bit, DecryptByKey([Suppliers_Contractors])) , 
[Zone] = CONVERT(bit, DecryptByKey( [Zone]) ) " +
         " FROM      dbo.ModulesAccess " +
         "WHERE     ClientID = @ClientID " +
       ") AS t UNPIVOT " +
"( Value FOR Property IN ( [AccountCode], [Assets], [AssetSetup], [AuditTrail], " +
                         "[BoMList], [Building], [CEUML], [Cleaning], " +
                         "[CleaningSetup], [CostCenter], [Department], " +
                         "[Division], [Employee], [Employee_User], " +
                         "[FailureCodes], [Groups], [JobManagement], " +
                         "[JobOrder], [JobPlans], [JobRequest], [Locations], " +
                         "[NotificationSetup],[ItemRequisition], [PreventiveMaintenance], " +
                         "[PurchaseRequisition], [PurchaseSetup], [Purchasing], " +
                         "[SafetyInstructions], [Sector], [SiteSetups], " +
                         "[Store], [StoreSetups], [SubDept], " +
                         "[Suppliers_Contractors], [Zone] ) ) AS up; Select * from @ModuleAccess ";


            //" SELECT h.HelpModuleID,H.ModuleName,h.ParentID,H.HasHelpDescription,hmd.HelpModuelDetailID,hmd.HelpTitle,hmd.HelpDescription,hmd.LanguageID " +
            //" FROM helpModule H " +
            //" INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1  " +
            // " INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and LanguageID = @LanguageID " +
            // " where H.HasHelpDescription = 1 and (hmd.HelpTitle like '%' + @SearchText + '%' or hmd.HelpDescription like '%'  +@SearchText +'%') ";


            using (ServiceContext context = new ServiceContext())
            {
                ProjectSession.UseMasterDB = true;
                List<HelpModule> lst = context.ExecuteQuery<HelpModule>(query, parameters).ToList();

                HelpModule objHelpModule = new HelpModule();
                HelpModule_Detail objHelpModule_Detail = new HelpModule_Detail();
                ProjectSession.UseHelpDB = true;
                List<HelpModule> lstHelpModule = context.SearchAll<HelpModule>(objHelpModule).ToList();

                ProjectSession.UseHelpDB = true;
                List<HelpModule_Detail> lstHelpModule_Detail = context.SearchAll<HelpModule_Detail>(objHelpModule_Detail).ToList();


                List<HelpModule> lstResult = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                              join m in lst.Where(y => y.Value == true) on H.ModuleAccess equals m.PropertyName
                                              join hmd in lstHelpModule_Detail.Where(z => z.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                              where ((hmd.HelpTitle != null && hmd.HelpTitle.Contains(SearchText)) || (hmd.HelpDescription != null && hmd.HelpDescription.Contains(SearchText)))
                                              select new HelpModule()
                                              {
                                                  HelpTitle = hmd.HelpTitle,
                                                  HelpModuelDetailID = hmd.HelpModuelDetailID,
                                                  HelpModuleID = H.HelpModuleID,
                                                  ModuleName = H.ModuleName,
                                                  ParentID = H.ParentID,
                                                  HasHelpDescription = H.HasHelpDescription,
                                                  LanguageID = H.LanguageID
                                              }).OrderBy(x => x.HelpModuleID).ToList();

                return lstResult;
            }
        }

        public static HelpModule_Detail GetHelpDetails(int HelpModuelDetailID, int LanguageID)
        {
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "HelpModuelDetailID",
                Value = HelpModuelDetailID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            query = "select * from helpModule_detail where HelpModuelDetailID = @HelpModuelDetailID AND LanguageID = @LanguageID";

            using (DapperDBContext context = new ServiceContext())
            {
                ProjectSession.UseHelpDB = true;
                return context.ExecuteQuery<HelpModule_Detail>(query, parameters).FirstOrDefault();
            }
        }

        public static List<CustomHelpRelatedItem> GetHelpRelatedItem(int HelpModuelDetailID)
        {
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "HelpModuelDetailID",
                Value = HelpModuelDetailID,
                DBType = DbType.Int32
            });

            query = "SELECT HELPRELATEDTOPICS.RELATEDTOPICID, HELPRELATEDTOPICS.PARENTHELPMODULEDETAILID,HELPRELATEDTOPICS.ChildHelpModuleDetailID,HELPMODULE_DETAIL.HelpTitle As RelatedHelpTitle " +
                    " FROM HELPRELATEDTOPICS INNER JOIN  HELPMODULE_DETAIL ON HELPRELATEDTOPICS.ChildHelpModuleDetailID = HELPMODULE_DETAIL.HelpModuelDetailID " +
                    "where HELPRELATEDTOPICS.PARENTHELPMODULEDETAILID = @HelpModuelDetailID";

            using (DapperDBContext context = new ServiceContext())
            {
                ProjectSession.UseHelpDB = true;
                return context.ExecuteQuery<CustomHelpRelatedItem>(query, parameters).ToList();
            }
        }

        public static bool AddRemoveBookmark(int HelpID, int EmployeeID, bool Add)
        {
            try
            {
                if (HelpID > 0 && EmployeeID > 0)
                {
                    string strQuery = string.Empty;
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "HelpID",
                        Value = HelpID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = EmployeeID,
                        DBType = DbType.Int32
                    });
                    if (Add)
                    {
                        strQuery = @"insert into HelpBookmarks (HelpModuleID,EmployeeID) values(@HelpID,@EmployeeID)";
                    }
                    else
                    {
                        strQuery = @"Delete From HelpBookmarks where HelpModuleID = @HelpID and EmployeeID = @EmployeeID ";
                    }

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        objDapperContext.ExecuteQuery(strQuery, parameters);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static IList<HelpModule> GetNextPrev(int EmployeeID, int LanguageID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = EmployeeID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "LanguageID",
                Value = LanguageID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "ClientID",
                Value = ProjectSession.AccountID,
                DBType = DbType.Int32
            });

            HelpModule objL1 = new HelpModule();


            var query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master; DECLARE @ModuleAccess AS TABLE" +
   "( " +
"PropertyName VARCHAR(50)," +
"Value bit" +
")" +
"INSERT INTO @ModuleAccess" +
       "( PropertyName, Value )" +
" SELECT  Property ," +
       "Value " +
"FROM    ( " +
@"
SELECT    ClientID , 
[AccountCode] = CONVERT(bit, DecryptByKey([AccountCode])), 
[Assets] = CONVERT(bit, DecryptByKey( [Assets])) , 
[AssetSetup] = CONVERT(bit, DecryptByKey( [AssetSetup])), 
[AuditTrail] = CONVERT(bit, DecryptByKey(  [AuditTrail])) , 
[BoMList] = CONVERT(bit, DecryptByKey( [BoMList])) , 
[Building] = CONVERT(bit, DecryptByKey( [Building])) , 
[CEUML] = CONVERT(bit, DecryptByKey( [CEUML])) , 
[Cleaning] = CONVERT(bit, DecryptByKey( [Cleaning])) , 
[CleaningSetup] = CONVERT(bit, DecryptByKey([CleaningSetup])) , 
[CostCenter] = CONVERT(bit, DecryptByKey( [CostCenter])) , 
[Department] = CONVERT(bit, DecryptByKey( [Department])) , 
[Division] = CONVERT(bit, DecryptByKey( [Division])) , 
[Employee] = CONVERT(bit, DecryptByKey( [Employee])) , 
[Employee_User] = CONVERT(bit, DecryptByKey( [Employee_User]) ), 
[FailureCodes] = CONVERT(bit, DecryptByKey( [FailureCodes])) , 
[Groups] = CONVERT(bit, DecryptByKey( [Groups])) , 
[JobManagement] = CONVERT(bit, DecryptByKey( [JobManagement])) , 
[JobOrder] = CONVERT(bit, DecryptByKey( [JobOrder])) , 
[JobPlans] = CONVERT(bit, DecryptByKey( [JobPlans])) , 
[JobRequest] = CONVERT(bit, DecryptByKey( [JobRequest])) , 
[Locations] = CONVERT(bit, DecryptByKey( [Locations])) , 
[NotificationSetup] = CONVERT(bit, DecryptByKey( [NotificationSetup])) , 
[ItemRequisition] = CONVERT(bit, DecryptByKey( [ItemRequisition])) , 
[PreventiveMaintenance] = CONVERT(bit, DecryptByKey( [PreventiveMaintenance])) , 
[PurchaseRequisition] = CONVERT(bit, DecryptByKey( [PurchaseRequisition])) , 
[PurchaseSetup] = CONVERT(bit, DecryptByKey( [PurchaseSetup])) , 
[Purchasing] = CONVERT(bit, DecryptByKey( [Purchasing])) , 
[SafetyInstructions] = CONVERT(bit, DecryptByKey( [SafetyInstructions])) , 
[Sector] = CONVERT(bit, DecryptByKey( [Sector])) , 
[SiteSetups] = CONVERT(bit, DecryptByKey( [SiteSetups])) , 
[Store] = CONVERT(bit, DecryptByKey( [Store])) , 
[StoreSetups] = CONVERT(bit, DecryptByKey( [StoreSetups])) , 
[SubDept] = CONVERT(bit, DecryptByKey( [SubDept])) , 
[Suppliers_Contractors] = CONVERT(bit, DecryptByKey([Suppliers_Contractors])) , 
[Zone] = CONVERT(bit, DecryptByKey( [Zone]) ) " +
         " FROM      dbo.ModulesAccess " +
         "WHERE     ClientID = @ClientID " +
       ") AS t UNPIVOT " +
"( Value FOR Property IN ( [AccountCode], [Assets], [AssetSetup], [AuditTrail], " +
                         "[BoMList], [Building], [CEUML], [Cleaning], " +
                         "[CleaningSetup], [CostCenter], [Department], " +
                         "[Division], [Employee], [Employee_User], " +
                         "[FailureCodes], [Groups], [JobManagement], " +
                         "[JobOrder], [JobPlans], [JobRequest], [Locations], " +
                         "[NotificationSetup], [ItemRequisition],[PreventiveMaintenance], " +
                         "[PurchaseRequisition], [PurchaseSetup], [Purchasing], " +
                         "[SafetyInstructions], [Sector], [SiteSetups], " +
                         "[Store], [StoreSetups], [SubDept], " +
                         "[Suppliers_Contractors], [Zone] ) ) AS up; Select * from @ModuleAccess";


            //            query +=
            //"SELECT h.*,hmd.HelpTitle,hmd.HelpModuelDetailID FROM helpModule H " +
            //" INNER JOIN @ModuleAccess m ON m.PropertyName = H.ModuleAccess AND m.Value = 1 and H.hasHelpDescription = 1 " +
            //" INNER JOIN helpModule_detail hmd on H.HelpModuleID = hmd.HelpModuleID  and LanguageID = @LanguageID order by h.HelpModuleID ";


            using (ServiceContext context = new ServiceContext())
            {
                ProjectSession.UseMasterDB = true;
                List<HelpModule> lst = context.ExecuteQuery<HelpModule>(query, parameters).ToList();

                HelpModule objHelpModule = new HelpModule();
                HelpModule_Detail objHelpModule_Detail = new HelpModule_Detail();
                ProjectSession.UseHelpDB = true;
                List<HelpModule> lstHelpModule = context.SearchAll<HelpModule>(objHelpModule).ToList();

                ProjectSession.UseHelpDB = true;
                List<HelpModule_Detail> lstHelpModule_Detail = context.SearchAll<HelpModule_Detail>(objHelpModule_Detail).ToList();

                List<HelpModule> lstResult = (from H in lstHelpModule.Where(x => x.HasHelpDescription == true)
                                              join m in lst.Where(x => x.Value == true) on H.ModuleAccess equals m.PropertyName
                                              join hmd in lstHelpModule_Detail.Where(x => x.LanguageID == LanguageID) on H.HelpModuleID equals hmd.HelpModuleID
                                              select new HelpModule()
                                              {
                                                  HelpTitle = hmd.HelpTitle,
                                                  HelpModuelDetailID = hmd.HelpModuelDetailID,
                                                  HelpModuleID = H.HelpModuleID,
                                                  ModuleName = H.ModuleName,
                                                  ParentID = H.ParentID,
                                                  DisplayOrder = H.DisplayOrder,
                                                  HasHelpDescription = H.HasHelpDescription,
                                                  ModuleAccess = H.ModuleAccess,
                                                  PageName = H.PageName,
                                                  LanguageID = H.LanguageID
                                              }).OrderBy(x => x.HelpModuleID).ToList();

                return lstResult;
            }
        }
    }
}

