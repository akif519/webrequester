﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class StoreTransactionService : DBExecute
    {
        public StoreTransactionService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Issue"

        public IList<Issue> GetIssueByStockCodeandSubStore(int stockID, int subStoreID, int tranId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Issue> list = new List<Issue>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockID > 0)
                {
                    strWhere = " And  i.StockID = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockID,
                        DBType = DbType.Int32
                    });
                }

                if (subStoreID > 0)
                {
                    strWhere += " And  i.SubStoreID = @SubStoreID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreID",
                        Value = subStoreID,
                        DBType = DbType.Int32
                    });

                }

                if (tranId > 0)
                {
                    strWhere += " And  i.TransactionID = @TransactionID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "TransactionID",
                        Value = tranId,
                        DBType = DbType.Int32
                    });
                }

                string query = "SELECT i.IssueID, i.StockID,sc.StockNo, sc.StockDescription, i.L2ID, l2r.L2name, i.SubStoreID, ss.SubStoreDesc, i.ToL2ID, tol2r.L2name AS ToL2Name, i.ToSubStoreID, ";
                query += "toss.SubStoreDesc AS ToSubStoreDesc, i.WONo, i.Description, i.DateIssue, i.IWTNo, i.Warehouse, i.UoM, i.QtyIssue, i.Price3, (isnull(i.QtyIssue,0) * isnull(i.Price3,0)) AS TotalPrice,";
                query += "i.BatchNo, i.isTransfer, Case When i.isTransfer = 1 Then 'Yes' When i.isTransfer = 0 Then 'No' Else '' End As TransferText, i.CreatedBy, i.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode, fs.SectorID AS ToSectorID, ";
                query += " fs.SectorName AS ToSectorName, fs.SectorAltname AS ToSectorAltname, fs.SectorCode AS ToSectorCode, sdept.MaintSubDeptID, sdept.MaintSubDeptDesc, ";
                query += " sdept.MaintSubDeptAltDesc, sdept.MaintSubDeptCode, dept.maintDeptID, dept.MaintDeptdesc, dept.MaintDeptAltdesc, dept.MaintDeptCode, ";
                query += " div.MaintDivisionID, div.MaintDivisionName, div.MaintDivisionAltName, div.MaintDivisionCode, l.LocationID, l.LocationNo";
                query += " FROM issue i LEFT OUTER JOIN ";
                query += "  stockcode sc ON i.StockID = sc.StockID LEFT OUTER JOIN ";
                query += " L2 l2r ON i.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                query += " substore  ss ON i.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                query += " L2 tol2r ON i.ToL2ID = tol2r.L2ID LEFT OUTER JOIN";
                query += " substore  toss ON i.ToSubStoreID = toss.SubStoreID LEFT OUTER JOIN ";
                query += " sector  rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN ";
                query += " sector  fs ON tol2r.SectorID = fs.SectorID LEFT OUTER JOIN";
                query += " workorders  wo ON wo.WorkorderNo = i.WONo LEFT OUTER JOIN ";
                query += " MaintSubDept  sdept ON sdept.MaintSubDeptID = wo.maintsubdeptID LEFT OUTER JOIN ";
                query += " MaintenanceDepartment  dept ON dept.maintDeptID = sdept.MaintDeptID LEFT OUTER JOIN ";
                query += " MainenanceDivision div ON div.MaintDivisionID = dept.MaintDivisionID LEFT OUTER JOIN ";
                query += " location l ON l.LocationID = i.LocationID ";
                query += " where 1=1 " + strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Issue>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public string GetlastTransactionNoByType(int type)
        {
            try
            {
                string strSQl = "Select TransactionNO from Transactions where Type = " + type + " order by TransactionID desc";
                string lastTransactionNo = string.Empty;
                IList<string> listTranNo = new List<string>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    listTranNo = objContext.ExecuteQuery<string>(strSQl, new Collection<DBParameters>());
                }

                if (listTranNo != null && listTranNo.Count > 0)
                {
                    lastTransactionNo = listTranNo.FirstOrDefault();
                }

                return lastTransactionNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GenerateNewTransactionNo(int type)
        {
            try
            {
                string lastTranNo = string.Empty;
                string typePrefix = string.Empty;

                lastTranNo = GetlastTransactionNoByType(type);

                if (string.IsNullOrEmpty(lastTranNo))
                {
                    lastTranNo = "1";
                }

                if (type == SystemEnum.TransactionType.Receive.GetHashCode())
                {
                    typePrefix = "REC-";
                }
                else if (type == SystemEnum.TransactionType.Issue.GetHashCode())
                {
                    typePrefix = "ISS-";
                }
                else if (type == SystemEnum.TransactionType.Return.GetHashCode())
                {
                    typePrefix = "RET-";
                }
                else if (type == SystemEnum.TransactionType.Adjustment.GetHashCode())
                {
                    typePrefix = "ADJ-";
                }
                else if (type == SystemEnum.TransactionType.Transfer.GetHashCode())
                {
                    typePrefix = "TRA-";
                }

                lastTranNo = lastTranNo.Replace(typePrefix, "");

                lastTranNo = typePrefix + Convert.ToString(Convert.ToInt32(lastTranNo) + 1).PadLeft(8, '0');

                return lastTranNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Receive"
        public IList<Receive> GetReceiveListByItemCityStore(int stockID, int subStoreID, int tranId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Receive> list = new List<Receive>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockID > 0)
                {
                    strWhere = " And  r.StockID = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockID,
                        DBType = DbType.Int32
                    });
                }

                if (subStoreID > 0)
                {
                    strWhere += " And  r.SubStoreID = @SubStoreID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreID",
                        Value = subStoreID,
                        DBType = DbType.Int32
                    });

                }

                if (tranId > 0)
                {
                    strWhere += " And  r.TransactionID = @TransactionID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "TransactionID",
                        Value = tranId,
                        DBType = DbType.Int32
                    });
                }

                string query = string.Empty;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "SELECT  r.ReceiveID, r.StockID,sc.StockNo, sc.StockDescription, sc.AltStockDescription, r.SupplierID, sp.SupplierName, sp.AltSupplierName, r.L2ID, l2r.L2name, l2r.L2Altname,  ";
                    query += "r.SubStoreID, ss.SubStoreDesc, ss.AltSubStoreDesc, r.FromL2ID, fl2r.L2name AS FromL2Name, fl2r.L2Altname AS FromL2AltName, r.FromSubStoreID, ";
                    query += " fss.SubStoreDesc AS FromSubStoreDesc, fss.AltSubStoreDesc AS FromAltSubStoreDesc, r.date_ as \"date\" , r.MRNNo, r.QtyRec, r.PONo, r.Price, (isnull( r.QtyRec,0) * isnull(r.Price,0)) AS TotalPrice,  ";
                    query += " r.QtyRec * r.Price AS Total, r.BatchNo, r.BatchExpiryDate, r.Comments, r.Transfer, Case When r.Transfer = 1 Then 'Yes' When r.Transfer = 0 Then 'No' Else '' End As TransferText, r.CreatedBy, r.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname,  ";
                    query += " rs.SectorCode, fs.SectorID AS FromSectorID, fs.SectorName AS FromSectorName, fs.SectorAltname AS FromSectorAltname,  ";
                    query += " fs.SectorCode AS FromSectorCode ";
                    query += " From receive r LEFT OUTER JOIN ";
                    query += "  stockcode sc ON r.StockID = sc.StockID LEFT OUTER JOIN ";
                    query += " suppliers sp ON r.SupplierID = sp.SupplierID LEFT OUTER JOIN ";
                    query += " L2 l2r ON r.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore ss ON r.SubStoreID = ss.SubStoreID LEFT OUTER JOIN";
                    query += " L2 fl2r ON r.FromL2ID = fl2r.L2ID LEFT OUTER JOIN ";
                    query += " substore  fss ON r.FromSubStoreID = fss.SubStoreID LEFT OUTER JOIN ";
                    query += " sector rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN";
                    query += " sector fs ON fl2r.SectorID = fs.SectorID ";
                    query += " where 1=1 " + strWhere;
                }
                else
                {
                    query = "SELECT        r.ReceiveID, r.StockID,sc.StockNo, sc.StockDescription, sc.AltStockDescription, r.SupplierID, sp.SupplierName, sp.AltSupplierName, r.L2ID, l2r.L2name, l2r.L2Altname,  ";
                    query += "r.SubStoreID, ss.SubStoreDesc, ss.AltSubStoreDesc, r.FromL2ID, fl2r.L2name AS FromL2Name, fl2r.L2Altname AS FromL2AltName, r.FromSubStoreID, ";
                    query += " fss.SubStoreDesc AS FromSubStoreDesc, fss.AltSubStoreDesc AS FromAltSubStoreDesc, r.Date, r.MRNNo, r.QtyRec, r.PONo, r.Price, (isnull( r.QtyRec,0) * isnull(r.Price,0)) AS TotalPrice,  ";
                    query += " r.QtyRec * r.Price AS Total, r.BatchNo, r.BatchExpiryDate, r.Comments, r.Transfer, Case When r.Transfer = 1 Then 'Yes' When r.Transfer = 0 Then 'No' Else '' End As TransferText, r.CreatedBy, r.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname,  ";
                    query += " rs.SectorCode, fs.SectorID AS FromSectorID, fs.SectorName AS FromSectorName, fs.SectorAltname AS FromSectorAltname,  ";
                    query += " fs.SectorCode AS FromSectorCode ";
                    query += " From receive  r LEFT OUTER JOIN ";
                    query += "  stockcode  sc ON r.StockID = sc.StockID LEFT OUTER JOIN ";
                    query += " suppliers  sp ON r.SupplierID = sp.SupplierID LEFT OUTER JOIN ";
                    query += " L2  l2r ON r.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore  ss ON r.SubStoreID = ss.SubStoreID LEFT OUTER JOIN";
                    query += " L2  fl2r ON r.FromL2ID = fl2r.L2ID LEFT OUTER JOIN ";
                    query += " substore  fss ON r.FromSubStoreID = fss.SubStoreID LEFT OUTER JOIN ";
                    query += " sector  rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN";
                    query += " sector  fs ON fl2r.SectorID = fs.SectorID ";
                    query += " where 1=1 " + strWhere;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Receive>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        #region "Return"

        public Single GetReturnBalance(int stockID, int storeID, string woNO, int locationID)
        {
            try
            {
                Single balance = 0;
                string strtotalIssue = "0";
                string strtotalReturn = "0";

                string strQueryIssue = string.Empty;
                string strQueryReturn = string.Empty;

                if (woNO.Trim() == string.Empty)
                {
                    strQueryIssue = "Select sum(QtyIssue) from Issue where StockID = " + stockID + " And SubStoreID = " + storeID + " And LocationID = " + locationID;
                    if(ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                    {
                        strQueryReturn = "Select sum(Qty) from `return`  where StockID = " + stockID + " And SubStoreID = " + storeID + " And LocationID = " + locationID;
                    }
                    else
                    {
                        strQueryReturn = "Select sum(Qty) from \"RETURN\"  where StockID = " + stockID + " And SubStoreID = " + storeID + " And LocationID = " + locationID;
                    }
                    
                }
                else
                {
                    strQueryIssue = "Select sum(QtyIssue) from Issue where StockID = " + stockID + " And SubStoreID = " + storeID + " And WONo = '" + Common.setQuote(woNO) + "'";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                    {
                        strQueryReturn = "Select sum(Qty) from `return`  where StockID = " + stockID + " And SubStoreID = " + storeID + " And WONo = '" + Common.setQuote(woNO) + "'";
                    }
                    else
                    {
                        strQueryReturn = "Select sum(Qty) from \"RETURN\"  where StockID = " + stockID + " And SubStoreID = " + storeID + " And WONo = '" + Common.setQuote(woNO) + "'";
                    }
                    
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    strtotalIssue = objContext.ExecuteScalar(strQueryIssue);
                    strtotalReturn = objContext.ExecuteScalar(strQueryReturn);
                }

                strtotalIssue = !string.IsNullOrEmpty(strtotalIssue) ? strtotalIssue : "0";
                strtotalReturn = !string.IsNullOrEmpty(strtotalReturn) ? strtotalReturn : "0";

                balance = Convert.ToSingle(strtotalIssue) - Convert.ToSingle(strtotalReturn);
                return balance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Issue> GetIssueUnitPriceByStockStoreWOandLOC(int stockID, int storeID, string woNO, int locationID)
        {
            try
            {
                IList<Issue> strList = new List<Issue>();

                string strQueryIssue = string.Empty;

                if (woNO.Trim() == string.Empty)
                {
                    strQueryIssue = "Select distinct Price3 from Issue where StockID = " + stockID + " And SubStoreID = " + storeID + " And LocationID = " + locationID;
                }
                else
                {
                    strQueryIssue = "Select distinct Price3 from Issue where StockID = " + stockID + " And SubStoreID = " + storeID + " And WONo = '" + Common.setQuote(woNO) + "'";
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    strList = objContext.ExecuteQuery<Issue>(strQueryIssue, new Collection<DBParameters>());
                }

                return strList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Return> GetReturnByStockCodeandSubStore(int stockID, int subStoreID, int tranId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Return> list = new List<Return>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockID > 0)
                {
                    strWhere = " And  r.StockID = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockID,
                        DBType = DbType.Int32
                    });
                }

                if (subStoreID > 0)
                {
                    strWhere += " And  r.SubStoreID = @SubStoreID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreID",
                        Value = subStoreID,
                        DBType = DbType.Int32
                    });

                }

                if (tranId > 0)
                {
                    strWhere += " And  r.TransactionID = @TransactionID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "TransactionID",
                        Value = tranId,
                        DBType = DbType.Int32
                    });
                }
                string query = string.Empty;
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "SELECT r.ReturnID, r.StockID,sc.StockNo, sc.StockDescription, r.L2ID, l2r.L2name, r.SubStoreID, ss.SubStoreDesc, r.WONo, r.Qty, r.DATE_ as \"Date\", r.Person, r.Comment_, r.RetPrice, ";
                    query += "(isnull(r.Qty,0) * isnull(r.RetPrice,0)) AS TotalPrice, r.BatchNo, r.CreatedBy, r.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode, sdept.MaintSubDeptID, ";
                    query += " sdept.MaintSubDeptDesc, sdept.MaintSubDeptAltDesc, sdept.MaintSubDeptCode, dept.maintDeptID, dept.MaintDeptdesc, dept.MaintDeptAltdesc, ";
                    query += " dept.MaintDeptCode, div.MaintDivisionID, div.MaintDivisionName, div.MaintDivisionAltName, div.MaintDivisionCode, l.LocationID, l.LocationNo ";
                    query += " FROM \"RETURN\" r LEFT OUTER JOIN ";
                    query += " stockcode sc ON r.StockID = sc.StockID LEFT OUTER JOIN ";
                    query += " L2 l2r ON r.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore ss ON r.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                    query += " sector rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN";
                    query += " workorders wo ON wo.WorkorderNo = r.WONo LEFT OUTER JOIN";
                    query += " MaintSubDept sdept ON sdept.MaintSubDeptID = wo.maintsubdeptID LEFT OUTER JOIN";
                    query += "  MaintenanceDepartment dept ON dept.maintDeptID = sdept.MaintDeptID LEFT OUTER JOIN";
                    query += "  MainenanceDivision div ON div.MaintDivisionID = dept.MaintDivisionID LEFT OUTER JOIN";
                    query += "  location l ON l.LocationID = r.LocationID ";
                    query += " where 1=1 " + strWhere;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    query = " SELECT r.ReturnID, r.StockID,sc.StockNo, sc.StockDescription, r.L2ID, l2r.L2name, r.SubStoreID, ss.SubStoreDesc, r.WONo, r.Qty, r.Date, r.Person, r.Comment, r.RetPrice, ";
                    query += " (isnull(r.Qty,0) * isnull(r.RetPrice,0)) AS TotalPrice, r.BatchNo, r.CreatedBy, r.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode, sdept.MaintSubDeptID, ";
                    query += " sdept.MaintSubDeptDesc, sdept.MaintSubDeptAltDesc, sdept.MaintSubDeptCode, dept.maintDeptID, dept.MaintDeptdesc, dept.MaintDeptAltdesc, ";
                    query += " dept.MaintDeptCode, divi.MaintDivisionID, divi.MaintDivisionName, divi.MaintDivisionAltName, divi.MaintDivisionCode, l.LocationID, l.LocationNo ";
                    query += " FROM `return`  r LEFT OUTER JOIN ";
                    query += " stockcode  sc ON r.StockID = sc.StockID LEFT OUTER JOIN ";
                    query += " L2  l2r ON r.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore ss ON r.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                    query += " sector rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN";
                    query += " workorders wo ON wo.WorkorderNo = r.WONo LEFT OUTER JOIN";
                    query += " MaintSubDept sdept ON sdept.MaintSubDeptID = wo.maintsubdeptID LEFT OUTER JOIN";
                    query += "  MaintenanceDepartment dept ON dept.maintDeptID = sdept.MaintDeptID LEFT OUTER JOIN";
                    query += "  MainenanceDivision divi ON divi.MaintDivisionID = dept.MaintDivisionID LEFT OUTER JOIN";
                    query += "  location l ON l.LocationID = r.LocationID ";
                    query += " where 1=1 " + strWhere;
                }
                else
                {
                query = "SELECT r.ReturnID, r.StockID,sc.StockNo, sc.StockDescription, r.L2ID, l2r.L2name, r.SubStoreID, ss.SubStoreDesc, r.WONo, r.Qty, r.Date, r.Person, r.Comment, r.RetPrice, ";
                query += "(isnull(r.Qty,0) * isnull(r.RetPrice,0)) AS TotalPrice, r.BatchNo, r.CreatedBy, r.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode, sdept.MaintSubDeptID, ";
                query += " sdept.MaintSubDeptDesc, sdept.MaintSubDeptAltDesc, sdept.MaintSubDeptCode, dept.maintDeptID, dept.MaintDeptdesc, dept.MaintDeptAltdesc, ";
                query += " dept.MaintDeptCode, div.MaintDivisionID, div.MaintDivisionName, div.MaintDivisionAltName, div.MaintDivisionCode, l.LocationID, l.LocationNo ";
                query += " FROM \"return\"  r LEFT OUTER JOIN ";
                query += " stockcode  sc ON r.StockID = sc.StockID LEFT OUTER JOIN ";
                query += " L2  l2r ON r.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                query += " substore ss ON r.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                query += " sector rs ON l2r.SectorID = rs.SectorID LEFT OUTER JOIN";
                query += " workorders wo ON wo.WorkorderNo = r.WONo LEFT OUTER JOIN";
                query += " MaintSubDept sdept ON sdept.MaintSubDeptID = wo.maintsubdeptID LEFT OUTER JOIN";
                query += "  MaintenanceDepartment dept ON dept.maintDeptID = sdept.MaintDeptID LEFT OUTER JOIN";
                query += "  MainenanceDivision div ON div.MaintDivisionID = dept.MaintDivisionID LEFT OUTER JOIN";
                query += "  location l ON l.LocationID = r.LocationID ";
                query += " where 1=1 " + strWhere;
                }
               

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Return>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion

        #region "Adjustment"

        public IList<Adjustment> GetAdjustmentByStockCodeandSubStore(int stockID, int subStoreID, int tranId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Adjustment> list = new List<Adjustment>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockID > 0)
                {
                    strWhere = " And  a.StockID = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockID,
                        DBType = DbType.Int32
                    });
                }

                if (subStoreID > 0)
                {
                    strWhere += " And  a.SubStoreID = @SubStoreID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreID",
                        Value = subStoreID,
                        DBType = DbType.Int32
                    });
                }

                if (tranId > 0)
                {
                    strWhere += " And  a.TransactionID = @TransactionID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "TransactionID",
                        Value = tranId,
                        DBType = DbType.Int32
                    });
                }
                string query = string.Empty;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "SELECT  a.AdjustmentID, a.StockID,sc.StockNo, sc.StockDescription, a.L2ID, l2r.L2name, a.SubStoreID, ss.SubStoreDesc, a.DATE_ as \"Date\" , a.Person, a.QtyAdj, a.Comment_, a.Price4, ";
                    query += "(isnull( a.Price4,0) * isnull(a.Price4,0)) AS TotalPrice, ";
                    query += " a.BatchNo, a.CreatedBy, a.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode  ";
                    query += " FROM  adjustment a LEFT OUTER JOIN  ";
                    query += " stockcode sc ON a.StockID = sc.StockID LEFT OUTER JOIN  ";
                    query += " L2 l2r ON a.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore ss ON a.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                    query += "  sector rs ON l2r.SectorID = rs.SectorID  ";
                    query += " where 1=1 " + strWhere;
                }
                else
                {
                    query = "SELECT  a.AdjustmentID, a.StockID,sc.StockNo, sc.StockDescription, a.L2ID, l2r.L2name, a.SubStoreID, ss.SubStoreDesc, a.Date, a.Person, a.QtyAdj, a.Comment, a.Price4, ";
                    query += "(isnull( a.Price4,0) * isnull(a.Price4,0)) AS TotalPrice, ";
                    query += " a.BatchNo, a.CreatedBy, a.CreatedDate, rs.SectorID, rs.SectorName, rs.SectorAltname, rs.SectorCode  ";
                    query += " FROM  adjustment a LEFT OUTER JOIN  ";
                    query += " stockcode  sc ON a.StockID = sc.StockID LEFT OUTER JOIN  ";
                    query += " L2  l2r ON a.L2ID = l2r.L2ID LEFT OUTER JOIN ";
                    query += " substore  ss ON a.SubStoreID = ss.SubStoreID LEFT OUTER JOIN ";
                    query += "  sector  rs ON l2r.SectorID = rs.SectorID  ";
                    query += " where 1=1 " + strWhere;
                }

              

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Adjustment>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        #region "Purchase Proposal"

        public IList<Stockcode_level> GetPurchaseProposalNormalpriority(int storeID, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                string query = string.Empty;

                IList<Stockcode_level> list = new List<Stockcode_level>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "SubStoreID",
                    Value = storeID,
                    DBType = DbType.Int32
                });

                query = @"Select * from (
                        SELECT        ROW_NUMBER() OVER (ORDER BY sc.StockNo) AS RowNo, sc.StockID, sc.StockNo, sc.StockDescription, sc.AltStockDescription, sc.Manufacturer,
                        ss.SubStoreID, ss.SubStoreDesc, ss.AltSubStoreDesc, sl.max_level, sl.re_order_level, sl.min_level, sl.reorder_qty, sl.Balance, ss.L2Id
                        FROM stockcode  sc LEFT JOIN stockcode_levels  sl ON sc.StockID = sl.stock_id LEFT JOIN  substore  ss ON sl.sub_store_id = ss.SubStoreID)
                         vw
                        Where vw.SubStoreID = @SubStoreID And vw.Balance < vw.re_order_level And vw.Balance >= vw.min_level";

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stockcode_level>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Stockcode_level> GetPurchaseProposalHighpriority(int storeID, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                string query = string.Empty;

                IList<Stockcode_level> list = new List<Stockcode_level>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "SubStoreID",
                    Value = storeID,
                    DBType = DbType.Int32
                });

                query = @"Select * from (
                        SELECT        ROW_NUMBER() OVER (ORDER BY sc.StockNo) AS RowNo, sc.StockID, sc.StockNo, sc.StockDescription, sc.AltStockDescription, sc.Manufacturer,
                        ss.SubStoreID, ss.SubStoreDesc, ss.AltSubStoreDesc, sl.max_level, sl.re_order_level, sl.min_level, sl.reorder_qty, sl.Balance, ss.L2Id
                        FROM stockcode sc LEFT JOIN stockcode_levels sl ON sc.StockID = sl.stock_id LEFT JOIN  substore ss ON sl.sub_store_id = ss.SubStoreID)
                         vw
                        Where vw.SubStoreID = @SubStoreID And vw.Balance < vw.min_level";

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stockcode_level>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Stock Level"

        public bool UpdateBalance(int stockID, int subStoreID, string balance)
        {
            try
            {
                int result = 0;
                string strSQL = "Update Stockcode_levels set Balance = @Balance Where stock_id = @StockID And sub_store_id = @SubStoreID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "Balance",
                    Value = Convert.ToDecimal(balance),
                    DBType = DbType.Decimal
                });

                parameters.Add(new DBParameters()
                {
                    Name = "StockID",
                    Value = stockID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "SubStoreID",
                    Value = subStoreID,
                    DBType = DbType.Int32
                });

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQL, parameters);
                }

                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Stock Code"

        public bool UpdateItemAvgPrice(int stockID, decimal avgPrice)
        {
            try
            {
                int result = -1;
                string strSQl = "Update StockCode Set AvgPrice = @AvgPrice, ModifiedBy = @ModifiedBy, ModifiedDate = @ModifiedDate  Where StockID = @StockID ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AvgPrice",
                    Value = avgPrice,
                    DBType = DbType.Decimal
                });

                parameters.Add(new DBParameters()
                {
                    Name = "StockID",
                    Value = stockID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedBy",
                    Value = ProjectSession.EmployeeID.ToString(),
                    DBType = DbType.String
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

               

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, parameters);
                }

                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Item Requisition"

        public static Material_req_master GetItemRequisitionDetail(string mrno)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "mrno",
                Value = mrno,
                DBType = DbType.String,
            });

            string query = " select material_req_master.mr_no, material_req_master.mr_date, " +
        " material_req_master.order_date, material_req_master.status_id, " +
        " material_req_status.status_desc, material_req_master.auth_status,mr_authorisation_status.auth_status_desc, " +
        " material_req_master.req_by,employees.employeeno as RequestedByNo, employees.Name as RequestedByName, " +
        " material_req_master.SubStoreId,material_req_master.requiredby_date, MainenanceDivision.MaintDivisionCode, " +
        " material_req_master.MaintdeptId,MaintenanceDepartment.MaintDeptCode, material_req_master.MainSubDeptId, " +
        " MaintSubDept.MaintSubDeptCode,material_req_master.MaintdivId,material_req_master.costCenterID, " +
        " material_req_master.accountcodeid,material_req_master.supplier_Id,suppliers.SupplierNo,suppliers.SupplierName, " +
        " suppliers.AltSupplierName,material_req_master.SectionSupId,SectionSup.Name as SectionSupName,SectionSup.Employeeno as SectionSupNo, " +
        " material_req_master.StoreSupId,StoreSup.Name as StoreSupName,StoreSup.Employeeno as StoreSupNo,material_req_master.CreatedBy, " +
        " emp_createby.Name as CreatedByName,material_req_master.ModifiedBy, emp_modifiedby.Name as ModifiedByName, " +
        " material_req_master.ModifiedDate, material_req_master.mr_notes, material_req_master.cancel_date, " +
        " material_req_master.cancel_by, material_req_master.cancel_notes, material_req_master.po_no, material_req_master.pr_no, material_req_master.L2ID,L2.L2Code, " +
        " material_req_master.PrintedBy, material_req_master.PrintDate, material_req_master.Documentstatusid " +
        " FROM  material_req_master " +
        " inner join material_req_status on material_req_master.status_id=material_req_status.mr_status_id " +
        " left outer join mr_authorisation_status on mr_authorisation_status.auth_status_id = material_req_master.auth_status " +
        " left outer join Employees employees  on employees.EmployeeId = material_req_master.req_by " +
        " inner join employees emp_createby on emp_createby.EmployeeId = material_req_master.CreatedBy " +
        " inner join L2 on material_req_master.L2ID = L2.L2ID " +
        " left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = material_req_master.MaintdivId " +
        " left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = material_req_master.MaintdeptId " +
        " left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = material_req_master.MainSubDeptId " +
        " left outer join suppliers on suppliers.SupplierID = material_req_master.supplier_Id " +
        " left outer join Employees  SectionSup  on SectionSup.EmployeeId = material_req_master.SectionSupId " +
        " left outer join Employees  StoreSup  on StoreSup.EmployeeId = material_req_master.StoreSupId " +
        " left outer join Employees  emp_modifiedby on emp_modifiedby.EmployeeId = material_req_master.ModifiedBy " +
         " where material_req_master.mr_no = @mrno ";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<Material_req_master>(query, parameters).FirstOrDefault();
            }
        }

        public IList<Material_req_detail> GetItemRequisitionByIRID(string mrno, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Material_req_detail> list = new List<Material_req_detail>();

            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters()
            {
                Name = "mrno",
                Value = mrno,
                DBType = DbType.String,
            });

            string strWhereClause = string.Empty;
            string query = " Select mrd.mr_id,mrd.PartId, case when mrd.PartId > 0 then 1 else 0 End as  isStock, " +
                 " mrd.mr_no,s.StockNo,mrd.part_desc,mrd.Altpart_desc,mrd.workorderno,mrd.uom,mrd.qty,mrd.ReceivedQty, " +
                 " mrd.CanceledQty,mrd.LocationId,location.LocationNo,location.LocationDescription, " +
                 " mrd.AssetId,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription, " +
                 " mrd.SubWHSupId,mrd.SubWHIssueTechId,sup.Employeeno as supNo,sup.Name as supName,issue.Employeeno as issueNo,issue.Name as issueName, " +
                 " (mrd.qty - (mrd.ReceivedQty + mrd.CanceledQty)) as AvailableQty " +
                 " from material_req_details  mrd left outer join StockCode s on mrd.PartId=s.StockID left outer join location  on location.locationId=mrd.locationid left outer join assets on assets.AssetId=mrd.AssetId " +
                 " left outer join employees sup on sup.employeeid=mrd.SubWHSupId " +
                 " left outer join employees issue on issue.employeeid=mrd.SubWHIssueTechId " +
                             " where 1=1 and mrd.mr_no = @mrno ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Material_req_detail>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            IList<Material_req_detail> listFinal = new List<Material_req_detail>();
            if (list.Count > 0)
            {
                foreach (Material_req_detail obj in list)
                {
                    obj.Guid_ItemID = System.Guid.NewGuid();
                    listFinal.Add(obj);
                }
            }

            return listFinal;
        }

        public IList<Material_req_detail> GetItemRequisitionByWoNo(string woNO, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Material_req_detail> list = new List<Material_req_detail>();

            Collection<DBParameters> Parameters = new Collection<DBParameters>();

            Parameters.Add(new DBParameters()
            {
                Name = "WoNo",
                Value = woNO,
                DBType = DbType.String,
            });

            string strWhereClause = string.Empty;
            string query = " Select mrd.mr_id,mrd.PartId, case when mrd.PartId > 0 then 1 else 0 End as  isStock, " +
                 " mrd.mr_no,s.StockNo,mrd.part_desc,mrd.Altpart_desc,mrd.workorderno,mrd.uom,mrd.qty,mrd.ReceivedQty, " +
                 " mrd.CanceledQty,mrd.LocationId,location.LocationNo,location.LocationDescription, " +
                 " mrd.AssetId,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription, " +
                 " mrd.SubWHSupId,mrd.SubWHIssueTechId,sup.Employeeno as supNo,sup.Name as supName,issue.Employeeno as issueNo,issue.Name as issueName, " +
                 " (mrd.qty - mrd.ReceivedQty + mrd.CanceledQty) as AvailableQty " +
                 " from material_req_details  mrd left outer join StockCode s on mrd.PartId=s.StockID left outer join location  on location.locationId=mrd.locationid left outer join assets on assets.AssetId=mrd.AssetId " +
                 " left outer join employees sup on sup.employeeid=mrd.SubWHSupId " +
                 " left outer join employees issue on issue.employeeid=mrd.SubWHSupId " +
                             " where 1=1 and mrd.WorkOrderNo = @WoNo ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, Parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Material_req_detail>(query, Parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static bool InsertMaterial_req(Material_req_master IRDetails)
        {
            int employeeID = ProjectSession.EmployeeID;
            int result = -1;

            try
            {
                if (IRDetails != null)
                {
                    string strInsertColumn = "";
                    string strInsertValues = "";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    if ((IRDetails.Mr_date != null))
                    {
                        strInsertColumn = strInsertColumn + " Mr_date, ";
                        strInsertValues = strInsertValues + "@Mr_date, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Mr_date",
                            Value = IRDetails.Mr_date,
                            DBType = DbType.DateTime
                        });
                    }

                    if ((IRDetails.Order_date != null))
                    {
                        strInsertColumn = strInsertColumn + " Order_date, ";
                        strInsertValues = strInsertValues + "@Order_date, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Order_date",
                            Value = IRDetails.Order_date,
                            DBType = DbType.DateTime
                        });
                    }

                    if ((IRDetails.Status_id != null))
                    {
                        strInsertColumn = strInsertColumn + " Status_id, ";
                        strInsertValues = strInsertValues + "@Status_id, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Status_id",
                            Value = IRDetails.Status_id,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Auth_status != null))
                    {
                        strInsertColumn = strInsertColumn + " Auth_status, ";
                        strInsertValues = strInsertValues + "@Auth_status, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Auth_status",
                            Value = IRDetails.Auth_status,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Req_by != null))
                    {
                        strInsertColumn = strInsertColumn + " Req_by, ";
                        strInsertValues = strInsertValues + "@Req_by, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Req_by",
                            Value = IRDetails.Req_by,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Supplier_Id != null))
                    {
                        strInsertColumn = strInsertColumn + " Supplier_Id, ";
                        strInsertValues = strInsertValues + "@Supplier_Id, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Supplier_Id",
                            Value = IRDetails.Supplier_Id,
                            DBType = DbType.Int32
                        });
                    }


                    if ((IRDetails.Requiredby_date != null))
                    {
                        strInsertColumn = strInsertColumn + " Requiredby_date, ";
                        strInsertValues = strInsertValues + "@Requiredby_date, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Requiredby_date",
                            Value = IRDetails.Requiredby_date,
                            DBType = DbType.DateTime
                        });
                    }


                    if ((IRDetails.Mr_notes != null))
                    {
                        strInsertColumn = strInsertColumn + " Mr_notes, ";
                        strInsertValues = strInsertValues + "@Mr_notes, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Mr_notes",
                            Value = IRDetails.Mr_notes,
                            DBType = DbType.String
                        });
                    }


                    if ((IRDetails.Cancel_date != null))
                    {
                        strInsertColumn = strInsertColumn + " Cancel_date, ";
                        strInsertValues = strInsertValues + "@Cancel_date, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Cancel_date",
                            Value = IRDetails.Cancel_date,
                            DBType = DbType.DateTime
                        });
                    }

                    if ((IRDetails.Cancel_by != null))
                    {
                        strInsertColumn = strInsertColumn + " Cancel_by, ";
                        strInsertValues = strInsertValues + "@Cancel_by, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Cancel_by",
                            Value = IRDetails.Cancel_by,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Cancel_notes != null))
                    {
                        strInsertColumn = strInsertColumn + " Cancel_notes, ";
                        strInsertValues = strInsertValues + "@Cancel_notes, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Cancel_notes",
                            Value = IRDetails.Cancel_notes,
                            DBType = DbType.String
                        });
                    }

                    if ((IRDetails.Po_no != null))
                    {
                        strInsertColumn = strInsertColumn + " Po_no, ";
                        strInsertValues = strInsertValues + "@Po_no, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Po_no",
                            Value = IRDetails.Po_no,
                            DBType = DbType.String
                        });
                    }

                    if ((IRDetails.Pr_no != null))
                    {
                        strInsertColumn = strInsertColumn + " Pr_no, ";
                        strInsertValues = strInsertValues + "@Pr_no, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Pr_no",
                            Value = IRDetails.Pr_no,
                            DBType = DbType.String
                        });
                    }

                    if ((IRDetails.L2ID != null))
                    {
                        strInsertColumn = strInsertColumn + " L2ID, ";
                        strInsertValues = strInsertValues + "@L2ID, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "L2ID",
                            Value = IRDetails.L2ID,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.MaintdeptId != null))
                    {
                        strInsertColumn = strInsertColumn + " MaintdeptId, ";
                        strInsertValues = strInsertValues + "@MaintdeptId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "MaintdeptId",
                            Value = IRDetails.MaintdeptId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.MainSubDeptId != null))
                    {
                        strInsertColumn = strInsertColumn + " MainSubDeptId, ";
                        strInsertValues = strInsertValues + "@MainSubDeptId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "MainSubDeptId",
                            Value = IRDetails.MainSubDeptId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.MaintdivId != null))
                    {
                        strInsertColumn = strInsertColumn + " MaintdivId, ";
                        strInsertValues = strInsertValues + "@MaintdivId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "MaintdivId",
                            Value = IRDetails.MaintdivId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.CostCenterID != null))
                    {
                        strInsertColumn = strInsertColumn + " CostCenterID, ";
                        strInsertValues = strInsertValues + "@CostCenterID, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "CostCenterID",
                            Value = IRDetails.CostCenterID,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Accountcodeid != null))
                    {
                        strInsertColumn = strInsertColumn + " Accountcodeid, ";
                        strInsertValues = strInsertValues + "@Accountcodeid, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Accountcodeid",
                            Value = IRDetails.Accountcodeid,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.SectionSupId != null))
                    {
                        strInsertColumn = strInsertColumn + " SectionSupId, ";
                        strInsertValues = strInsertValues + "@SectionSupId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "SectionSupId",
                            Value = IRDetails.SectionSupId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.StoreSupId != null))
                    {
                        strInsertColumn = strInsertColumn + " StoreSupId, ";
                        strInsertValues = strInsertValues + "@StoreSupId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "StoreSupId",
                            Value = IRDetails.StoreSupId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.SubStoreId != null))
                    {
                        strInsertColumn = strInsertColumn + " SubStoreId, ";
                        strInsertValues = strInsertValues + "@SubStoreId, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "SubStoreId",
                            Value = IRDetails.SubStoreId,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.PrintDate != null))
                    {
                        strInsertColumn = strInsertColumn + " PrintDate, ";
                        strInsertValues = strInsertValues + "@PrintDate, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "PrintDate",
                            Value = IRDetails.PrintDate,
                            DBType = DbType.DateTime
                        });
                    }

                    if ((IRDetails.Documentstatusid != null))
                    {
                        strInsertColumn = strInsertColumn + " Documentstatusid, ";
                        strInsertValues = strInsertValues + "@Documentstatusid, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Documentstatusid",
                            Value = IRDetails.Documentstatusid,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.CreatedBy != null))
                    {
                        strInsertColumn = strInsertColumn + " CreatedBy, ";
                        strInsertValues = strInsertValues + "@CreatedBy, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "CreatedBy",
                            Value = IRDetails.CreatedBy,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.ModifiedBy != null))
                    {
                        strInsertColumn = strInsertColumn + " ModifiedBy, ";
                        strInsertValues = strInsertValues + "@ModifiedBy, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedBy",
                            Value = IRDetails.ModifiedBy,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.ModifiedDate != null))
                    {
                        strInsertColumn = strInsertColumn + " ModifiedDate, ";
                        strInsertValues = strInsertValues + "@ModifiedDate, ";

                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = IRDetails.ModifiedDate,
                            DBType = DbType.DateTime
                        });
                    }

                    if ((IRDetails.PrintedBy != null))
                    {
                        strInsertColumn = strInsertColumn + " PrintedBy, ";
                        strInsertValues = strInsertValues + "@PrintedBy,";

                        parameters.Add(new DBParameters()
                        {
                            Name = "PrintedBy",
                            Value = IRDetails.PrintedBy,
                            DBType = DbType.Int32
                        });
                    }

                    if ((IRDetails.Mr_no != null))
                    {
                        strInsertColumn = strInsertColumn + " Mr_no ";
                        strInsertValues = strInsertValues + "@Mr_no";

                        parameters.Add(new DBParameters()
                        {
                            Name = "Mr_no",
                            Value = IRDetails.Mr_no,
                            DBType = DbType.String
                        });
                    }

                    string strQuery = "INSERT into Material_req_master(" + strInsertColumn + ")  " + " VALUES(" + strInsertValues + ")";

                    using (ServiceContext context = new ServiceContext())
                    {
                        context.ExecuteQuery<Material_req_master>(strQuery, parameters);
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void NewItemRequisitionItemDetails(Material_req_master model)
        {
            try
            {
                int result = -1;
                bool isSuccess = false;

                if (model.lstMaterialreqdetail != null && model.lstMaterialreqdetail.Count() > 0)
                {
                    string queryDelete = "Delete from Material_req_details Where mr_no = '" + Common.setQuote(model.Mr_no) + "'";

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(queryDelete, new Collection<DBParameters>());
                    }

                    if (result == 0)
                    {
                        isSuccess = InsertMaterial_req_detail(model.lstMaterialreqdetail, model.Mr_no);
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool InsertMaterial_req_detail(List<Material_req_detail> lstIRDetails, string mr_no)
        {
            string strSQL = " insert into Material_req_details (mr_no,partid,part_desc,Altpart_desc,Qty,uom,WorkorderNo,AssetId,LocationId,ReceivedQty,CanceledQty,SubWHSupId,SubWHIssueTechId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int result = -1;

            string oraFrom = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";

                parameters.Add(new DBParameters()
                {
                    Name = "CurrentDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });
            }
            else
            {
                parameters.Add(new DBParameters()
                {
                    Name = "CurrentDate",
                    Value = Common.GetEnglishDate(DateTime.Now),
                    DBType = DbType.DateTime
                });
            }

            try
            {
                if (!String.IsNullOrEmpty(mr_no))
                {
                    foreach (Material_req_detail item in lstIRDetails)
                    {
                        if (strInsertValues.Length == 0)
                        {
                            strInsertValues.Append(" Select '" + mr_no + "'," + (item.Partid == null ? "NULL" : item.Partid.ToString()) + "," + (item.Part_desc == null || item.Part_desc == "NULL" ? "NULL" : "'" + item.Part_desc.ToString() + "'") +
                            "," + (item.Altpart_desc == null || item.Altpart_desc == "NULL" ? "NULL" : "N'" + item.Altpart_desc.ToString() + "'") + "," + (item.Qty == null ? "NULL" : item.Qty.ToString()) + "," + (item.Uom == null || item.Uom == "NULL" ? "NULL" : "'" + item.Uom.ToString() + "'") +
                            "," + (item.WorkorderNo == null || item.WorkorderNo == "NULL" ? "NULL" : "'" + item.WorkorderNo.ToString() + "'") + "," + (item.AssetId == null ? "NULL" : item.AssetId.ToString()) + "," + (item.LocationId == null ? "NULL" : item.LocationId.ToString()) +
                            "," + (item.ReceivedQty == null ? "0.00" : item.ReceivedQty.ToString()) + "," + (item.CanceledQty == null ? "0.00" : item.CanceledQty.ToString()) + "," + (item.SubWHSupId == null ? "NULL" : item.SubWHSupId.ToString()) +
                            "," + (item.SubWHIssueTechId == null ? "NULL" : item.SubWHIssueTechId.ToString()) + "," + "NULL" + ",@CurrentDate ,"
                             + (item.ModifiedBy == null || item.ModifiedBy == "NULL" ? "NULL" : item.ModifiedBy.ToString()) + "," + (item.ModifiedDate == null ? "NULL" : item.ModifiedDate.ToString()) + "" + oraFrom);
                        }
                        else
                        {
                            strInsertValues.Append("  Union All Select '" + mr_no + "'," + (item.Partid == null ? "NULL" : item.Partid.ToString()) + "," + (item.Part_desc == null || item.Part_desc == "NULL" ? "NULL" : "'" + item.Part_desc.ToString() + "'") +
                            "," + (item.Altpart_desc == null || item.Altpart_desc == "NULL" ? "NULL" : "N'" + item.Altpart_desc.ToString() + "'") + "," + (item.Qty == null ? "NULL" : item.Qty.ToString()) + "," + (item.Uom == null || item.Uom == "NULL" ? "NULL" : "'" + item.Uom.ToString() + "'") +
                            "," + (item.WorkorderNo == null || item.WorkorderNo == "NULL" ? "NULL" : "'" + item.WorkorderNo.ToString() + "'") + "," + (item.AssetId == null ? "NULL" : item.AssetId.ToString()) + "," + (item.LocationId == null ? "NULL" : item.LocationId.ToString()) +
                            "," + (item.ReceivedQty == null ? "0.00" : item.ReceivedQty.ToString()) + "," + (item.CanceledQty == null ? "0.00" : item.CanceledQty.ToString()) + "," + (item.SubWHSupId == null ? "NULL" : item.SubWHSupId.ToString()) +
                            "," + (item.SubWHIssueTechId == null ? "NULL" : item.SubWHIssueTechId.ToString()) + "," + "NULL" + ",@CurrentDate ,"
                             + (item.ModifiedBy == null || item.ModifiedBy == "NULL" ? "NULL" : item.ModifiedBy.ToString()) + "," + (item.ModifiedDate == null ? "NULL" : item.ModifiedDate.ToString()) + "" + oraFrom);
                        }
                    }
                }
                else
                {
                    return false;
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CancelIR(string mrno, int deliveryStatus, string cancle_note)
        {
            try
            {
                int result = -1;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "mrno",
                    Value = mrno,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "deliveryStatus",
                    Value = deliveryStatus,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "cancle_note",
                    Value = cancle_note,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedBy",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "cancel_date",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });


                parameters.Add(new DBParameters()
                {
                    Name = "cancel_by",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });


                string updateQuery = @"UPDATE Material_req_master SET status_id = @deliveryStatus, cancel_by = @cancel_by, cancel_notes = @cancle_note, cancel_date = @cancel_date, ModifiedBy = @ModifiedBy, ModifiedDate = @ModifiedDate Where mr_no = @mrno";
                using (DapperDBContext context = new DapperDBContext())
                {
                    result = context.ExecuteQuery(updateQuery, parameters);
                }
                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ReopenIR(string mrno)
        {
            try
            {
                int result = -1;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "mrno",
                    Value = mrno,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "deliveryStatus",
                    Value = 1,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedBy",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });

                string updateQuery = @"UPDATE Material_req_master SET status_id = @deliveryStatus, ModifiedBy = @ModifiedBy, ModifiedDate = @ModifiedDate Where mr_no = @mrno";
                using (DapperDBContext context = new DapperDBContext())
                {
                    result = context.ExecuteQuery(updateQuery, parameters);
                }
                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Material_req_auth> GetApproveIRBymr_no(int pageNo, string sortExpression, string sortDirection, string mr_no, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Material_req_auth> list = new List<Material_req_auth>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = " Select material_req_auth.*, " +
                            " employees.EmployeeNO,employees.Name, " +
                            " IRApprovalLevels.LevelNo,IRApprovalLevels.LevelName,IRApprovalLevelMappings.SubStoreId " +
                             " from material_req_auth " +
                            " inner join employees on employees.EmployeeID = material_req_auth.EmployeeID " +
                            " inner join IRApprovalLevelMappings on IRApprovalLevelMappings.AutoId = material_req_auth.IRApprovalLevelMappingId " +
                            " inner join IRApprovalLevels on IRApprovalLevels.IRApprovalLevelId = IRApprovalLevelMappings.IRApprovalLevelId " +
                             " where material_req_auth.mr_no = " + mr_no;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Material_req_auth>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static IList<IRApprovalLevelMapping> GetPRApprovalLevelsBySubStore_IR_ID(int SubStoreId, string mrno)
        {
            List<IRApprovalLevelMapping> lstIRApprovalLevel = new List<IRApprovalLevelMapping>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "SubStoreId",
                Value = SubStoreId,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "mrno",
                Value = mrno,
                DBType = DbType.String
            });

            int LastLevel = 0;

            //using (ServiceContext context = new ServiceContext())
            //{
            //    IRApprovalLevel objIRApprovalLevels = new IRApprovalLevel();
            //    IRApprovalLevelMapping objIRApprovalLevelMappings = new IRApprovalLevelMapping();
            //    Material_req_auth objMaterial_req_auth = new Material_req_auth();

            //    var query = (from c in context.SearchAll<IRApprovalLevel>(objIRApprovalLevels)
            //                 join d in context.SearchAll<IRApprovalLevelMapping>(objIRApprovalLevelMappings)
            //                on c.IRApprovalLevelId equals d.IRApprovalLevelId
            //                 where d.SubStoreId == SubStoreId &&
            //                !(from e in context.SearchAll<Material_req_auth>(objMaterial_req_auth) where e.Mr_no == mr_no select e.IRApprovalLevelMappingId).Contains(d.AutoId)
            //                 select new { c, d }).ToList().OrderBy(o => o.c.LevelNo);
            //}



            string query = " select IRApprovalLevels.*,IRApprovalLevelMappings.Mandatory,IRApprovalLevelMappings.AutoId from " +
                " IRApprovalLevelMappings inner join IRApprovalLevels  on IRApprovalLevelMappings.IRApprovalLevelId = IRApprovalLevels.IRApprovalLevelId " +
                " where SubStoreId = @SubStoreId and IRApprovalLevelMappings.AutoId not in(select material_req_auth.IRApprovalLevelMappingId from material_req_auth where material_req_auth.mr_no = @mrno) order by IRApprovalLevels.LevelNo ";

            using (ServiceContext context = new ServiceContext())
            {
                lstIRApprovalLevel = context.ExecuteQuery<IRApprovalLevelMapping>(query, parameters).ToList();
            }

            List<IRApprovalLevelMapping> finallstIRApprovalLevel = new List<IRApprovalLevelMapping>();

            foreach (IRApprovalLevelMapping item in lstIRApprovalLevel)
            {
                if (item.Mandatory == false)
                {
                    finallstIRApprovalLevel.Add(item);
                }
                else
                {
                    finallstIRApprovalLevel.Add(item);
                    break;
                }
            }
            return finallstIRApprovalLevel;
        }

        public static IList<employees> GetEmployeesByPRApprovalLevelId_L2ID(int SubStoreid, int IRApprovalLevelId)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();


            parameters.Add(new DBParameters()
            {
                Name = "SubStoreid",
                Value = SubStoreid,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "IRApprovalLevelId",
                Value = IRApprovalLevelId,
                DBType = DbType.Int32
            });

            string query1 = " select employees.* from employees where EmployeeID " +
                            " in (select EmployeeId from IRApprovalLevelMappings_employees where AutoId in (select AutoId from IRApprovalLevelMappings where IRApprovalLevelId = @IRApprovalLevelId and SubStoreId = @SubStoreid ))";

            using (ServiceContext context = new ServiceContext())
            {
                List<employees> lstemployees = context.ExecuteQuery<employees>(query1, parameters).ToList();
                return lstemployees;
            }
        }

        public static bool UpdateIRAuthStatus(string mr_no, int auth_status_id)
        {
            string strSQL = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            int result = -1;

            parameters.Add(new DBParameters()
            {
                Name = "AuthStatusID",
                Value = auth_status_id,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "MrNo",
                Value = mr_no,
                DBType = DbType.String
            });

            try
            {
                strSQL = "Update material_req_master Set auth_status= @AuthStatusID  Where mr_no = @MrNo";

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQL, parameters);
                }

                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Material_req_detail GetIRIssueDetail(Int32 mr_id)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "mr_id",
                    Value = mr_id,
                    DBType = DbType.Int32,
                });

                string query = @"Select material_req_details.mr_id, material_req_details.mr_no, 
                            material_req_details.Qty,material_req_details.ReceivedQty,material_req_details.CanceledQty,
                            (material_req_details.qty - (material_req_details.ReceivedQty + material_req_details.CanceledQty)) as AvailableQty,
                            material_req_details.SubWHSupId,e1.Name As supName, e1.EmployeeNO As supNo, material_req_details.SubWHIssueTechId , e2.Name As issueName ,e2.EmployeeNO As issueNo
                            from material_req_details Left Outer Join employees e1
                            on material_req_details.SubWHSupId = e1.EmployeeID
                            Left Outer Join employees e2
                            on material_req_details.SubWHIssueTechId = e2.EmployeeID
                             where mr_id = @mr_id";

                using (DapperDBContext context = new DapperDBContext())
                {
                    return context.ExecuteQuery<Material_req_detail>(query, parameters).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateMaterialReqDetailIssue(Material_req_detail objDetail)
        {
            try
            {
                int result = -1;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "Mr_id",
                    Value = objDetail.Mr_id,
                    DBType = DbType.Int32,
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ReceivedQty",
                    Value = objDetail.ReceivedQty,
                    DBType = DbType.Decimal,
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CancelledQty",
                    Value = objDetail.CanceledQty,
                    DBType = DbType.Decimal,
                });

                parameters.Add(new DBParameters()
                {
                    Name = "SubWHSupID",
                    Value = objDetail.SubWHSupId,
                    DBType = DbType.Int32,
                });

                parameters.Add(new DBParameters()
                {
                    Name = "SubWHIssueTechID",
                    Value = objDetail.SubWHIssueTechId,
                    DBType = DbType.Int32,
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModifiedBy",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32,
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime,
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime,
                    });

                }

               
                string query = @"Update material_req_details Set ReceivedQty = @ReceivedQty,
                                                                    CanceledQty = @CancelledQty,
                                                                    SubWHSupId = @SubWHSupID,
                                                                    SubWHIssueTechId = @SubWHIssueTechID,
                                                                    ModifiedBy = @ModifiedBy,
                                                                    ModifiedDate = @ModifiedDate
                                 where mr_id = @Mr_id";

                using (ServiceContext objContext = new ServiceContext(true))
                {
                    try
                    {
                        objContext.BeginTransaction();
                        result = objContext.ExecuteQuery(query, objContext.Connection, parameters);

                        if (result == 0)
                        {
                            List<Material_req_detail> lstmaterialDetail = new List<Material_req_detail>();
                            bool closeIR = true;
                            query = @"Select * from material_req_details where mr_no = @Mr_No";
                             parameters = new Collection<DBParameters>();
                             parameters.Add(new DBParameters()
                             {
                                 Name = "Mr_No",
                                 Value = objDetail.Mr_no,
                                 DBType = DbType.String,
                             });

                             lstmaterialDetail = objContext.ExecuteQuery<Material_req_detail>(query, objContext.Connection, parameters).ToList();

                            if (lstmaterialDetail != null && lstmaterialDetail.Count > 0)
                            {
                                foreach (Material_req_detail obj in lstmaterialDetail)
                                {
                                    if ((obj.Qty != null ? obj.Qty : 0) != ((obj.ReceivedQty != null ? obj.ReceivedQty : 0) + (obj.CanceledQty != null ? obj.CanceledQty : 0)))
                                    { 
                                        closeIR = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                closeIR = false;
                            }

                            if(closeIR)
                            {
                                parameters = new Collection<DBParameters>();

                                 parameters.Add(new DBParameters()
                                {
                                    Name = "MrNo",
                                    Value = objDetail.Mr_no, 
                                    DBType = DbType.String,
                                });

                                //Status ID = 2 Means Closed

                                parameters.Add(new DBParameters()
                                {
                                    Name = "statusID",
                                    Value = 2, 
                                    DBType = DbType.Int32,
                                });

                                parameters.Add(new DBParameters()
                                {
                                    Name = "ModifiedBy",
                                    Value = ProjectSession.EmployeeID,
                                    DBType = DbType.Int32,
                                });

                                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "ModifiedDate",
                                        Value = DateTime.Now,
                                        DBType = DbType.DateTime,
                                    });

                                }
                                else
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "ModifiedDate",
                                        Value = Common.GetEnglishDate(DateTime.Now),
                                        DBType = DbType.DateTime,
                                    });

                                }

                               
                                query = @"Update material_req_master Set status_id = @statusID,
                                                                    ModifiedBy = @ModifiedBy,
                                                                    ModifiedDate = @ModifiedDate
                                 where mr_no = @MrNo";

                                result = -1;
                                 result = objContext.ExecuteQuery(query, objContext.Connection, parameters);

                                 if (result == 0)
                                 {
                                     objContext.CommitTransaction();
                                     return true;
                                 }
                                 else
                                 {
                                    objContext.RollBackTransaction();
                                    return false;
                                 }
                            }
                            else
                            {
                                objContext.CommitTransaction();
                                return true;
                            }
                        }
                        else
                        {
                            objContext.RollBackTransaction();
                            return false;
                        }
                    }
                    catch (Exception ex1)
                    {
                        objContext.RollBackTransaction();
                        throw ex1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Transaction"

        public IList<Transactions> GetTransactionbyTransactionID(int transactionID)
        {
            try
            {
                string strWhere = string.Empty;
                IList<Transactions> lstTransactions = new List<Transactions>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strSQl = @"Select Transactions.TransactionID,Transactions.TransactionNO,Transactions.Type,TransactionTypes.TypeName,TransactionTypes.AltTypeName,Transactions.Status,
                                Transactions.CreatedBy,Employees.Name As CreatedByName ,Transactions.CreatedDate,Transactions.ModifiedBy,e2.Name As ModifiedByName ,Transactions.ModifiedDate
                                FROM  Transactions Inner Join Employees ON Transactions.CreatedBy = Employees.EmployeeID Left Outer Join Employees e2 On Transactions.ModifiedBy = e2.EmployeeID 
                                Inner Join TransactionTypes On Transactions.Type = TransactionTypes.TransactionTypeID";

                if (transactionID > 0)
                {
                    strWhere += " AND Transactions.TransactionID = @TransactionID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "TransactionID",
                        Value = transactionID,
                        DBType = DbType.Int32
                    });
                }

                strSQl += strWhere;

                using (DapperContext objDapperContext = new DapperContext())
                {
                    lstTransactions = objDapperContext.ExecuteQuery<Transactions>(strSQl, parameters);
                }

                return lstTransactions;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
