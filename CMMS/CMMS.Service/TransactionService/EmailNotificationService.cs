﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using CMMS.Model;
using System.Collections.ObjectModel;
using CMMS.Infrastructure;
using System.Data;

namespace CMMS.Service
{
    public class EmailNotificationService : DBExecute
    {
        public EmailNotificationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Methods for table emailnotificationrules_processedrule"

        public static List<EmailNotificationProcessedRule> GetListEmailNotificationRule_ProceedRules(string clientCode)
        {

            Emailnotificationrules_processedrule objEmailNotificationProcessedRule = null;
            EmployeeService objEmpService = null;
            employees objEmp = new employees();
            employees ObjectTempEmployee = new employees();
            try
            {
                objEmailNotificationProcessedRule = new Emailnotificationrules_processedrule();
                objEmpService = new EmployeeService();
                objEmailNotificationProcessedRule.IsMailSent = false;

                List<EmailNotificationProcessedRule> ListEmailNotificationProcessedRule = new List<EmailNotificationProcessedRule>();
                List<Emailnotificationrules_processedrule> ListEmailNotificationRule_ProceedRules = new List<Emailnotificationrules_processedrule>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    ListEmailNotificationRule_ProceedRules = GetListEmailNotificationRule_ProceedRulesWithEmailIsSentFalse(clientCode);
                }

                foreach (Emailnotificationrules_processedrule ObjectEmailNotificationRule_ProceedRules in ListEmailNotificationRule_ProceedRules)
                {
                    EmailNotificationProcessedRule ObjectEmailNotificationProcessedRule = new EmailNotificationProcessedRule();
                    Nullable<int> IntRequestorID = default(Nullable<int>);

                    //Assign emailnotificationrules object to ObjectEmailNotificationRule
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule = objContext.SearchWithClientID<Emailnotificationrule>(new Emailnotificationrule(), 0, string.Empty, string.Empty, clientCode).ToList().FirstOrDefault(Item => Item.NotificationRuleID == ObjectEmailNotificationRule_ProceedRules.NotificationRuleID);
                    }

                    //Assign WorkRequest OR WorkOrder object
                    if (ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode())
                    {
                        ObjectEmailNotificationProcessedRule.ObjectWorkRequest = GetEmailTemplateWorkRequestByWorkRequestNo(ObjectEmailNotificationRule_ProceedRules.ReferenceID, clientCode);
                        IntRequestorID = ObjectEmailNotificationProcessedRule.ObjectWorkRequest.RequestorID;
                    }
                    else if (ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode())
                    {
                        ObjectEmailNotificationProcessedRule.ObjectWorkOrder = GetEmailTemplateWorkOrderByWorkOrderNo(ObjectEmailNotificationRule_ProceedRules.ReferenceID, clientCode);
                        IntRequestorID = ObjectEmailNotificationProcessedRule.ObjectWorkOrder.RequestorID;
                    }
                    else if (ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.Supplier.GetHashCode())
                    {
                        ObjectEmailNotificationProcessedRule.ObjectSupplier = GetEmailTemplateSuppliersByWorkOrderNo(ObjectEmailNotificationRule_ProceedRules.ReferenceID, clientCode);
                        if (ObjectEmailNotificationProcessedRule.ObjectSupplier.SupplierID > 0)
                        {
                            //Get List of Supplier
                            ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee = new List<EmailReceiptant>();

                            EmailReceiptant ObjectEmailReceiptant = new EmailReceiptant();
                            ObjectEmailReceiptant.ReceiptantID = ObjectEmailNotificationProcessedRule.ObjectSupplier.SupplierID;
                            ObjectEmailReceiptant.ReceiptantName = ObjectEmailNotificationProcessedRule.ObjectSupplier.SupplierName;
                            ObjectEmailReceiptant.ReceiptantAltName = ObjectEmailNotificationProcessedRule.ObjectSupplier.AltSupplierName;
                            ObjectEmailReceiptant.ReceiptantNo = ObjectEmailNotificationProcessedRule.ObjectSupplier.SupplierNo;
                            ObjectEmailReceiptant.ReceiptantEmail = ObjectEmailNotificationProcessedRule.ObjectSupplier.Email;
                            ObjectEmailReceiptant.LanguageCode = "en-us";
                            ObjectEmailReceiptant.HandPhone = string.Empty;
                            ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee.Add(ObjectEmailReceiptant);
                        }
                    }

                    //Get list of employee
                    if ((ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.IsSendToRequester != null) && ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID != SystemEnum.ModuleCode.Supplier.GetHashCode())
                    {
                        ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee = new List<EmailReceiptant>();
                        if (!ObjectEmailNotificationProcessedRule.ObjectEmailNotificationRule.IsSendToRequester.Value)
                        {
                            List<Emailnotificationrules_employee> ListEmailNotificationRule_Employee = new List<Emailnotificationrules_employee>();
                            Emailnotificationrules_employee objEmailNotification_Employee = new Emailnotificationrules_employee();
                            objEmailNotification_Employee.NotificationRuleID = ObjectEmailNotificationRule_ProceedRules.NotificationRuleID;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                ListEmailNotificationRule_Employee = GetEmailnotificationrules_employeeByNotificationRuleID(clientCode, objEmailNotification_Employee.NotificationRuleID); //objContext.SearchWithClientID<Emailnotificationrules_employee>(objEmailNotification_Employee, 0, string.Empty, string.Empty, clientCode).ToList();
                            }

                            foreach (Emailnotificationrules_employee ObjectEmailNotificationRule_Employee in ListEmailNotificationRule_Employee)
                            {
                                objEmp = new employees();
                                objEmp.EmployeeID = ObjectEmailNotificationRule_Employee.EmployeeID;
                                using (ServiceContext objContext = new ServiceContext())
                                {
                                    ObjectTempEmployee = objContext.SearchWithClientID<employees>(objEmp, 0, string.Empty, string.Empty, clientCode).FirstOrDefault();
                                }

                                EmailReceiptant ObjectEmailReceiptant = new EmailReceiptant();
                                ObjectEmailReceiptant.ReceiptantID = ObjectTempEmployee.EmployeeID;
                                ObjectEmailReceiptant.ReceiptantName = ObjectTempEmployee.Name;
                                ObjectEmailReceiptant.ReceiptantAltName = ObjectTempEmployee.AltName;
                                ObjectEmailReceiptant.ReceiptantNo = ObjectTempEmployee.EmployeeNO;
                                ObjectEmailReceiptant.ReceiptantEmail = ObjectTempEmployee.Email;
                                ObjectEmailReceiptant.LanguageCode = ObjectTempEmployee.LanguageCode;
                                ObjectEmailReceiptant.HandPhone = ObjectTempEmployee.HandPhone;
                                ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee.Add(ObjectEmailReceiptant);
                            }
                        }
                        else
                        {
                            if (IntRequestorID.HasValue)
                            {
                                objEmp = new employees();
                                objEmp.EmployeeID = IntRequestorID.Value;
                                using (ServiceContext objContext = new ServiceContext())
                                {
                                    ObjectTempEmployee = objContext.SearchWithClientID<employees>(objEmp, 0, string.Empty, string.Empty, clientCode).FirstOrDefault();
                                }

                                EmailReceiptant ObjectEmailReceiptant = new EmailReceiptant();
                                ObjectEmailReceiptant.ReceiptantID = ObjectTempEmployee.EmployeeID;
                                ObjectEmailReceiptant.ReceiptantName = ObjectTempEmployee.Name;
                                ObjectEmailReceiptant.ReceiptantAltName = ObjectTempEmployee.AltName;
                                ObjectEmailReceiptant.ReceiptantNo = ObjectTempEmployee.EmployeeNO;
                                ObjectEmailReceiptant.ReceiptantEmail = ObjectTempEmployee.Email;
                                ObjectEmailReceiptant.LanguageCode = ObjectTempEmployee.LanguageCode;
                                ObjectEmailReceiptant.HandPhone = ObjectTempEmployee.HandPhone;
                                ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee.Add(ObjectEmailReceiptant);
                            }
                        }
                    }

                    if (ObjectEmailNotificationProcessedRule.ListEmailReceiptant_Employee.Count > 0)
                    {
                        ListEmailNotificationProcessedRule.Add(ObjectEmailNotificationProcessedRule);
                    }
                }

                return ListEmailNotificationProcessedRule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateEmailSentStatus_ProceedRules(List<EmailNotificationProcessedRule> ListEmailNotificationProcessedRule, string clientCode)
        {
            try
            {
                string StringEmailNotificationProcessedRuleIDs = string.Join(",", ListEmailNotificationProcessedRule.Select(Item => Item.ObjectEmailNotificationRule.NotificationRuleID.ToString()).ToList());
                string Query = "UPDATE emailnotificationrules_processedrule SET IsMailSent = 1 WHERE NotificationRuleID IN (" + StringEmailNotificationProcessedRuleIDs + ")";

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Emailnotificationrules_processedrule> GetListEmailNotificationRule_ProceedRulesWithEmailIsSentFalse(string clientCode)
        {

            try
            {

                string Query = "Select * from emailnotificationrules_processedrule Where IsMailSent = 0";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailnotificationrules_processedrule>(Query, parameters, clientCode).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Emailnotificationrules_employee> GetEmailnotificationrules_employeeByNotificationRuleID(string clientCode, int notificationRuleID)
        {

            try
            {

                string Query = "Select * from Emailnotificationrules_employees Where NotificationRuleID = @NotificationRuleID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailnotificationrules_employee>(Query, parameters, clientCode).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Implementation of EmailNotificationRule Escalation"

        public static List<EmailNotificationEscalationRule> GetListEmailNotificationRule_EscalationRules(string clientCode)
        {
            try
            {
                List<EmailNotificationEscalationRule> ListEmailNotificationEscalationRule = new List<EmailNotificationEscalationRule>();
                ListEmailNotificationEscalationRule.AddRange(ProcessWorkRequest_EscalationEmailNotificationRule(clientCode));
                ListEmailNotificationEscalationRule.AddRange(ProcessWorkOrder_EscalationEmailNotificationRule(clientCode));

                return ListEmailNotificationEscalationRule;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<EmailNotificationEscalationRule> ProcessWorkRequest_EscalationEmailNotificationRule(string clientCode)
        {
            try
            {
                List<EmailNotificationEscalationRule> ListEmailNotificationEscalationRule = new List<EmailNotificationEscalationRule>();
                List<Emailnotificationrule> ListEmailNotificationRule = new List<Emailnotificationrule>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    ListEmailNotificationRule = objContext.SearchWithClientID<Emailnotificationrule>(new Emailnotificationrule(), 0, string.Empty, string.Empty, clientCode).Where(item => item.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode() && item.NotificationRuleEventID == SystemEnum.ModuleEvent.JobRequestOpenStatusEscalation.GetHashCode() && Convert.ToBoolean(item.IsActive)).ToList();
                }

                foreach (Emailnotificationrule ObjectEmailNotificationRule in ListEmailNotificationRule)
                {
                    dynamic StringL2IDs = string.Empty;
                    dynamic StringWorkPriorityIDs = string.Empty;
                    dynamic StringWorkTypeIDs = string.Empty;
                    dynamic StringWorkTradeIDs = string.Empty;
                    dynamic StringBuildingIDs = string.Empty;
                    dynamic StringLocationIDs = string.Empty;
                    dynamic StringAssetIDs = string.Empty;
                    //dynamic StringLocationTypeIDs = string.Empty;
                    //dynamic StringCriticalityIDs = string.Empty;

                    List<Emailnotificationrules_L2> lstEmailNotificationL2 = null;
                    Emailnotificationrules_L2 objEmailNotificationL2 = new Emailnotificationrules_L2();
                    objEmailNotificationL2.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_workpriority> lstEmailNotificationWp = null;
                    Emailnotificationrules_workpriority objEmailNotificationWp = new Emailnotificationrules_workpriority();
                    objEmailNotificationWp.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_worktype> lstEmailNotificationWtype = null;
                    Emailnotificationrules_worktype objEmailNotificationWtype = new Emailnotificationrules_worktype();
                    objEmailNotificationWtype.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_worktrade> lstEmailNotificationWtrade = null;
                    Emailnotificationrules_worktrade objEmailNotificationWtrade = new Emailnotificationrules_worktrade();
                    objEmailNotificationWtrade.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_L5> lstEmailNotificationL5 = null;
                    Emailnotificationrules_L5 objEmailNotificationL5 = new Emailnotificationrules_L5();
                    objEmailNotificationL5.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_location> lstEmailNotificationLocation = null;
                    Emailnotificationrules_location objEmailNotificationLocation = new Emailnotificationrules_location();
                    objEmailNotificationLocation.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    //List<EmailNotification_LocationType> lstEmailNotificationLocationType = null;
                    //EmailNotification_LocationType objEmailNotificationLocationType = new EmailNotification_LocationType();
                    //objEmailNotificationLocationType.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    //List<EmailNotification_Criticality> lstEmailNotificationCriticality = null;
                    //EmailNotification_Criticality objEmailNotificationCriticality = new EmailNotification_Criticality();
                    //objEmailNotificationCriticality.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_asset> lstEmailNotificationAsset = null;
                    Emailnotificationrules_asset objEmailNotificationAsset = new Emailnotificationrules_asset();
                    objEmailNotificationAsset.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        lstEmailNotificationL2 = objContext.SearchWithClientID<Emailnotificationrules_L2>(objEmailNotificationL2, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWp = objContext.SearchWithClientID<Emailnotificationrules_workpriority>(objEmailNotificationWp, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWtype = objContext.SearchWithClientID<Emailnotificationrules_worktype>(objEmailNotificationWtype, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWtrade = objContext.SearchWithClientID<Emailnotificationrules_worktrade>(objEmailNotificationWtrade, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationL5 = objContext.SearchWithClientID<Emailnotificationrules_L5>(objEmailNotificationL5, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationLocation = objContext.SearchWithClientID<Emailnotificationrules_location>(objEmailNotificationLocation, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationAsset = objContext.SearchWithClientID<Emailnotificationrules_asset>(objEmailNotificationAsset, 0, string.Empty, string.Empty, clientCode).ToList();
                        //lstEmailNotificationLocationType = objContext.SearchWithClientID<EmailNotification_LocationType>(objEmailNotificationLocationType, 0, string.Empty, string.Empty, clientCode).ToList();
                        //lstEmailNotificationCriticality = objContext.SearchWithClientID<EmailNotification_Criticality>(objEmailNotificationCriticality, 0, string.Empty, string.Empty, clientCode).ToList();
                    }

                    if (lstEmailNotificationL2 != null && lstEmailNotificationL2.Count > 0)
                    {
                        StringL2IDs = string.Join(",", lstEmailNotificationL2.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.L2ID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWp != null && lstEmailNotificationWp.Count > 0)
                    {
                        StringWorkPriorityIDs = string.Join(",", lstEmailNotificationWp.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkPriorityID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWtype != null && lstEmailNotificationWtype.Count > 0)
                    {
                        StringWorkTypeIDs = string.Join(",", lstEmailNotificationWtype.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkTypeID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWtrade != null && lstEmailNotificationWtrade.Count > 0)
                    {
                        StringWorkTradeIDs = string.Join(",", lstEmailNotificationWtrade.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkTradeID.ToString()).ToList());
                    }

                    if (lstEmailNotificationL5 != null && lstEmailNotificationL5.Count > 0)
                    {
                        StringBuildingIDs = string.Join(",", lstEmailNotificationL5.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.L5ID.ToString()).ToList());
                    }

                    if (lstEmailNotificationLocation != null && lstEmailNotificationLocation.Count > 0)
                    {
                        StringLocationIDs = string.Join(",", lstEmailNotificationLocation.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.LocationID.ToString()).ToList());
                    }

                    if (lstEmailNotificationAsset != null && lstEmailNotificationAsset.Count > 0)
                    {
                        StringAssetIDs = string.Join(",", lstEmailNotificationAsset.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.AssetID.ToString()).ToList());
                    }

                    //if (lstEmailNotificationLocationType != null && lstEmailNotificationLocationType.Count > 0)
                    //{
                    //    StringLocationTypeIDs = string.Join(",", lstEmailNotificationLocationType.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.LocationTypeID.ToString()).ToList());
                    //}

                    //if (lstEmailNotificationCriticality != null && lstEmailNotificationCriticality.Count > 0)
                    //{
                    //    StringCriticalityIDs = string.Join(",", lstEmailNotificationCriticality.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.CriticalityID.ToString()).ToList());
                    //}


                    string Query = "SELECT "
                    + "worequest.RequestNo AS RequestNo, worequest.WorkorderNo AS WorkorderNo, worequest.ProblemDesc AS ProblemDesc, worequest.ReceivedDate AS ReceivedDate, worequest.RequiredDate AS RequiredDate, worequest.Remarks AS Remarks,  "
                    + "L2.L2ID AS L2ID, L2.L2Code AS L2Code, L2.L2name AS L2name, L2.L2Altname AS L2Altname, "
                    + "L3.L3ID AS L3ID, L3.L3No AS L3No, L3.L3Desc AS L3Desc, L3.L3AltDesc AS L3AltDesc, "
                    + "L4.L4ID AS L4ID, L4.L4No AS L4No, L4.L4Description AS L4Description, L4.L4AltDescription AS L4AltDescription, "
                    + "L5.L5ID AS L5ID, L5.L5No AS L5No, L5.L5Description AS L5Description, L5.L5AltDescription AS L5AltDescription, "
                    + "location.LocationID AS LocationNo, location.LocationNo AS LocationNo, location.LocationDescription AS LocationDescription, location.LocationAltDescription AS LocationAltDescription, "
                    + "assets.AssetID AS AssetID, assets.AssetNumber AS AssetNumber, assets.AssetDescription AS AssetDescription, assets.AssetAltDescription AS AssetAltDescription, "
                    + "MainenanceDivision.MaintDivisionID AS MaintDivisionID,MainenanceDivision.MaintDivisionCode AS MaintDivisionCode, MainenanceDivision.MaintDivisionName AS MaintDivisionName,MainenanceDivision.MaintDivisionAltName AS MaintDivisionAltName, "
                    + "MaintenanceDepartment.maintDeptID AS MaintDeptID,MaintenanceDepartment.MaintDeptCode AS MaintDeptCode, MaintenanceDepartment.MaintDeptdesc AS MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc AS MaintDeptAltdesc,  "
                    + "MaintSubDept.MaintSubDeptID AS MaintSubDeptID,MaintSubDept.MaintSubDeptCode AS MaintSubDeptCode, MaintSubDept.MaintSubDeptDesc AS MaintSubDeptDesc,MaintSubDept.MaintSubDeptAltDesc AS MaintSubDeptAltDesc,  "
                    + "worktrade.WorkTradeID AS WorkTradeID, worktrade.WorkTrade AS WorkTrade,worktrade.AltWorkTrade AS AltWorkTrade,  "
                    + "worktype.WorkTypeID AS WorkTypeID,worktype.WorkTypeDescription AS WorkTypeDescription, worktype.AltWorkTypeDescription AS AltWorkTypeDescription, "
                    + "workstatus.WorkStatusID AS RequestStatusID, workstatus.WorkStatus AS RequestStatus, workstatus.AltWorkStatus AS AltRequestStatus,  "
                    + "workpriority.WorkPriorityID AS WorkPriorityID, workpriority.WorkPriority AS WorkPriority, workpriority.AltWorkPriority AS AltWorkPriority, "
                    + "Requestor.EmployeeID AS RequestorID, Requestor.EmployeeNO AS RequestorNo, Requestor.Name AS RequestorName,Requestor.AltName AS RequestorAltName,Requestor.Email AS RequestorEmail, "
                    + "Created.EmployeeID AS CreatedID, Created.EmployeeNO AS CreatedNo, Created.Name AS CreatedName,Created.AltName AS CreatedAltName,Created.Email AS CreatedEmail, "
                    + "Cancelled.EmployeeID AS CancelledID, Cancelled.EmployeeNO AS CancelledNo, Cancelled.Name AS CancelledName,Cancelled.AltName AS CancelledAltName,Cancelled.Email AS CancelledEmail "
                    + "FROM worequest "
                    + "LEFT JOIN L2 ON L2.L2ID = worequest.L2ID "
                    + "LEFT JOIN L3 ON L3.L3ID = worequest.L3ID "
                    + "LEFT JOIN L4 ON L4.L4ID = worequest.L4ID "
                    + "LEFT JOIN L5 ON L5.L5ID = worequest.L5ID "
                    + "LEFT JOIN location ON location.LocationID = worequest.LocationID "
                        //+ "LEFT JOIN LocationType ON LocationType.LocationTypeID = location.LocationTypeID"
                    + "LEFT JOIN assets ON assets.AssetID = worequest.AssetID "
                        //+ "LEFT JOIN Criticality ON Criticality.Id = assets.CriticalityId"
                    + "LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = worequest.MaintdivId "
                    + "LEFT JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = worequest.MaintdeptId "
                    + "LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = worequest.MainSubDeptId "
                    + "LEFT JOIN worktrade ON worktrade.WorkTradeID = worequest.worktradeid "
                    + "LEFT JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID "
                    + "LEFT JOIN workstatus ON workstatus.WorkStatusID = worequest.RequestStatusID "
                    + "LEFT JOIN workpriority ON workpriority.WorkPriorityID = worequest.workpriorityid "
                    + "LEFT JOIN employees Requestor ON Requestor.EmployeeID = worequest.RequesterID "
                    + "LEFT JOIN employees Created ON Created.EmployeeID = worequest.CreatedByID "
                    + "LEFT JOIN employees Cancelled ON Cancelled.EmployeeID = worequest.CancelledByID "
                    + "WHERE worequest.L2ID IN (" + StringL2IDs + ") ";

                    if ((ObjectEmailNotificationRule.MaintDivisionID != null))
                    {
                        Query = Query + " AND worequest.MaintdivId = " + ObjectEmailNotificationRule.MaintDivisionID.ToString() + " ";
                    }
                    if ((ObjectEmailNotificationRule.MaintDeptID != null))
                    {
                        Query = Query + " AND worequest.MaintdeptId = " + ObjectEmailNotificationRule.MaintDeptID.ToString() + " ";
                    }
                    if ((ObjectEmailNotificationRule.MaintSubDeptID != null))
                    {
                        Query = Query + " AND worequest.MainSubDeptId = " + ObjectEmailNotificationRule.MaintSubDeptID.ToString() + " ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkPriorityIDs))
                    {
                        Query = Query + " AND worequest.workpriorityid IN (" + StringWorkPriorityIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkTypeIDs))
                    {
                        Query = Query + " AND worequest.WorkTypeID IN (" + StringWorkTypeIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkTradeIDs))
                    {
                        Query = Query + " AND worequest.worktradeid IN (" + StringWorkTradeIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringBuildingIDs))
                    {
                        Query = Query + " AND ( worequest.L5ID IN (" + StringBuildingIDs + ") OR worequest.L5ID IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringLocationIDs))
                    {
                        Query = Query + " AND ( worequest.LocationID IN (" + StringLocationIDs + ") OR worequest.LocationID IS NULL) ";
                    }
                    //if (!string.IsNullOrEmpty(StringLocationTypeIDs))
                    //{
                    //    Query = Query + " AND ( LocationType.LocationTypeID IN (" + StringLocationTypeIDs + ") OR LocationType.LocationTypeID IS NULL) ";
                    //}
                    //if (!string.IsNullOrEmpty(StringCriticalityIDs))
                    //{
                    //    Query = Query + " AND ( Criticality.Id IN (" + StringCriticalityIDs + ") OR Criticality.Id IS NULL) ";
                    //}
                    if (!string.IsNullOrEmpty(StringAssetIDs))
                    {
                        Query = Query + " AND ( worequest.AssetID IN (" + StringAssetIDs + ") OR worequest.AssetID IS NULL) ";
                    }
                    Query = Query + " AND worequest.RequestStatusID = 1 ";

                    List<EmailTemplateWorkRequest> ListEmailTemplateWorkRequest = new List<EmailTemplateWorkRequest>();
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        ListEmailTemplateWorkRequest = objContext.ExecuteQueryWithClientID<EmailTemplateWorkRequest>(Query, new Collection<DBParameters>(), clientCode).ToList();
                    }

                    if (ListEmailTemplateWorkRequest != null && ListEmailTemplateWorkRequest.Count > 0)
                    {
                        EmailNotificationEscalationRule ObjectEmailNotificationEscalationRule = new EmailNotificationEscalationRule();
                        ObjectEmailNotificationEscalationRule.ObjectEmailNotificationRule = ObjectEmailNotificationRule;
                        ObjectEmailNotificationEscalationRule.ListWorkRequest = ListEmailTemplateWorkRequest;
                        ObjectEmailNotificationEscalationRule.ListEmailReceiptant_Employee = GetListEmailEscalationEmployeeByNotificationRuleID(ObjectEmailNotificationRule.NotificationRuleID, clientCode);

                        ListEmailNotificationEscalationRule.Add(ObjectEmailNotificationEscalationRule);
                    }
                }

                return ListEmailNotificationEscalationRule;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<EmailNotificationEscalationRule> ProcessWorkOrder_EscalationEmailNotificationRule(string clientCode)
        {
            try
            {
                List<EmailNotificationEscalationRule> ListEmailNotificationEscalationRule = new List<EmailNotificationEscalationRule>();
                List<Emailnotificationrule> ListEmailNotificationRule = new List<Emailnotificationrule>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    ListEmailNotificationRule = objContext.SearchWithClientID<Emailnotificationrule>(new Emailnotificationrule(), 0, string.Empty, string.Empty, clientCode).Where(item => item.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode() && item.NotificationRuleEventID == SystemEnum.ModuleEvent.JobOrderOpenStatusEscalation.GetHashCode() && Convert.ToBoolean(item.IsActive)).ToList();
                }

                foreach (Emailnotificationrule ObjectEmailNotificationRule in ListEmailNotificationRule)
                {
                    dynamic StringL2IDs = string.Empty;
                    dynamic StringWorkPriorityIDs = string.Empty;
                    dynamic StringWorkTypeIDs = string.Empty;
                    dynamic StringWorkTradeIDs = string.Empty;
                    dynamic StringBuildingIDs = string.Empty;
                    dynamic StringLocationIDs = string.Empty;
                    dynamic StringAssetIDs = string.Empty;
                    dynamic StringLocationTypeIDs = string.Empty;
                    dynamic StringSubCategoryIDs = string.Empty;
                    dynamic StringCriticalityIDs = string.Empty;

                    List<Emailnotificationrules_L2> lstEmailNotificationL2 = null;
                    Emailnotificationrules_L2 objEmailNotificationL2 = new Emailnotificationrules_L2();
                    objEmailNotificationL2.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_workpriority> lstEmailNotificationWp = null;
                    Emailnotificationrules_workpriority objEmailNotificationWp = new Emailnotificationrules_workpriority();
                    objEmailNotificationWp.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_worktype> lstEmailNotificationWtype = null;
                    Emailnotificationrules_worktype objEmailNotificationWtype = new Emailnotificationrules_worktype();
                    objEmailNotificationWtype.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_worktrade> lstEmailNotificationWtrade = null;
                    Emailnotificationrules_worktrade objEmailNotificationWtrade = new Emailnotificationrules_worktrade();
                    objEmailNotificationWtrade.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_L5> lstEmailNotificationL5 = null;
                    Emailnotificationrules_L5 objEmailNotificationL5 = new Emailnotificationrules_L5();
                    objEmailNotificationL5.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_location> lstEmailNotificationLocation = null;
                    Emailnotificationrules_location objEmailNotificationLocation = new Emailnotificationrules_location();
                    objEmailNotificationLocation.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<Emailnotificationrules_asset> lstEmailNotificationAsset = null;
                    Emailnotificationrules_asset objEmailNotificationAsset = new Emailnotificationrules_asset();
                    objEmailNotificationAsset.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<EmailNotification_SubCategory> lstEmailNotificationSubCategory = null;
                    EmailNotification_SubCategory objEmailNotificationSubCategory = new EmailNotification_SubCategory();
                    objEmailNotificationSubCategory.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<EmailNotification_LocationType> lstEmailNotificationLocationType = null;
                    EmailNotification_LocationType objEmailNotificationLocationType = new EmailNotification_LocationType();
                    objEmailNotificationLocationType.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    List<EmailNotification_Criticality> lstEmailNotificationCriticality = null;
                    EmailNotification_Criticality objEmailNotificationCriticality = new EmailNotification_Criticality();
                    objEmailNotificationCriticality.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        lstEmailNotificationL2 = objContext.SearchWithClientID<Emailnotificationrules_L2>(objEmailNotificationL2, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWp = objContext.SearchWithClientID<Emailnotificationrules_workpriority>(objEmailNotificationWp, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWtype = objContext.SearchWithClientID<Emailnotificationrules_worktype>(objEmailNotificationWtype, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationWtrade = objContext.SearchWithClientID<Emailnotificationrules_worktrade>(objEmailNotificationWtrade, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationL5 = objContext.SearchWithClientID<Emailnotificationrules_L5>(objEmailNotificationL5, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationLocation = objContext.SearchWithClientID<Emailnotificationrules_location>(objEmailNotificationLocation, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationAsset = objContext.SearchWithClientID<Emailnotificationrules_asset>(objEmailNotificationAsset, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationLocationType = objContext.SearchWithClientID<EmailNotification_LocationType>(objEmailNotificationLocationType, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationSubCategory = objContext.SearchWithClientID<EmailNotification_SubCategory>(objEmailNotificationSubCategory, 0, string.Empty, string.Empty, clientCode).ToList();
                        lstEmailNotificationCriticality = objContext.SearchWithClientID<EmailNotification_Criticality>(objEmailNotificationCriticality, 0, string.Empty, string.Empty, clientCode).ToList();
                    }

                    if (lstEmailNotificationL2 != null && lstEmailNotificationL2.Count > 0)
                    {
                        StringL2IDs = string.Join(",", lstEmailNotificationL2.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.L2ID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWp != null && lstEmailNotificationWp.Count > 0)
                    {
                        StringWorkPriorityIDs = string.Join(",", lstEmailNotificationWp.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkPriorityID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWtype != null && lstEmailNotificationWtype.Count > 0)
                    {
                        StringWorkTypeIDs = string.Join(",", lstEmailNotificationWtype.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkTypeID.ToString()).ToList());
                    }

                    if (lstEmailNotificationWtrade != null && lstEmailNotificationWtrade.Count > 0)
                    {
                        StringWorkTradeIDs = string.Join(",", lstEmailNotificationWtrade.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.WorkTradeID.ToString()).ToList());
                    }

                    if (lstEmailNotificationL5 != null && lstEmailNotificationL5.Count > 0)
                    {
                        StringBuildingIDs = string.Join(",", lstEmailNotificationL5.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.L5ID.ToString()).ToList());
                    }

                    if (lstEmailNotificationLocation != null && lstEmailNotificationLocation.Count > 0)
                    {
                        StringLocationIDs = string.Join(",", lstEmailNotificationLocation.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.LocationID.ToString()).ToList());
                    }

                    if (lstEmailNotificationLocationType != null && lstEmailNotificationLocationType.Count > 0)
                    {
                        StringLocationTypeIDs = string.Join(",", lstEmailNotificationLocationType.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.LocationTypeID.ToString()).ToList());
                    }

                    if (lstEmailNotificationSubCategory != null && lstEmailNotificationSubCategory.Count > 0)
                    {
                        StringSubCategoryIDs = string.Join(",", lstEmailNotificationSubCategory.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.AssetSubCatID.ToString()).ToList());
                    }

                    if (lstEmailNotificationCriticality != null && lstEmailNotificationCriticality.Count > 0)
                    {
                        StringCriticalityIDs = string.Join(",", lstEmailNotificationCriticality.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.CriticalityID.ToString()).ToList());
                    }

                    if (lstEmailNotificationAsset != null && lstEmailNotificationAsset.Count > 0)
                    {
                        StringAssetIDs = string.Join(",", lstEmailNotificationAsset.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.AssetID.ToString()).ToList());
                    }

                    string Query = "SELECT  " +
                    "workorders.WorkorderNo AS WorkorderNo, workorders.ProblemDescription AS ProblemDescription, workorders.DateReceived AS DateReceived, workorders.DateRequired AS DateRequired, " +
                    "L2.L2ID AS L2ID, L2.L2Code AS L2Code, L2.L2name AS L2name, L2.L2Altname AS L2Altname, " +
                    "L3.L3ID AS L3ID, L3.L3No AS L3No, L3.L3Desc AS L3Desc, L3.L3AltDesc AS L3AltDesc, " +
                    "L4.L4ID AS L4ID, L4.L4No AS L4No, L4.L4Description AS L4Description, L4.L4AltDescription AS L4AltDescription, " +
                    "L5.L5ID AS L5ID, L5.L5No AS L5No, L5.L5Description AS L5Description, L5.L5AltDescription AS L5AltDescription, " +
                    "location.LocationID AS LocationNo, location.LocationNo AS LocationNo, location.LocationDescription AS LocationDescription, location.LocationAltDescription AS LocationAltDescription, " +
                    "assets.AssetID AS AssetID, assets.AssetNumber AS AssetNumber, assets.AssetDescription AS AssetDescription, assets.AssetAltDescription AS AssetAltDescription, " +
                    "MainenanceDivision.MaintDivisionID AS MaintDivisionID,MainenanceDivision.MaintDivisionCode AS MaintDivisionCode, MainenanceDivision.MaintDivisionName AS MaintDivisionName,MainenanceDivision.MaintDivisionAltName AS MaintDivisionAltName, " +
                    "MaintenanceDepartment.maintDeptID AS MaintDeptID,MaintenanceDepartment.MaintDeptCode AS MaintDeptCode, MaintenanceDepartment.MaintDeptdesc AS MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc AS MaintDeptAltdesc,  " +
                    "MaintSubDept.MaintSubDeptID AS MaintSubDeptID,MaintSubDept.MaintSubDeptCode AS MaintSubDeptCode, MaintSubDept.MaintSubDeptDesc AS MaintSubDeptDesc,MaintSubDept.MaintSubDeptAltDesc AS MaintSubDeptAltDesc,  " +
                    "worktrade.WorkTradeID AS WorkTradeID, worktrade.WorkTrade AS WorkTrade,worktrade.AltWorkTrade AS AltWorkTrade,  " +
                    "worktype.WorkTypeID AS WorkTypeID,worktype.WorkTypeDescription AS WorkTypeDescription, worktype.AltWorkTypeDescription AS AltWorkTypeDescription, " +
                    "workstatus.WorkStatusID AS WorkStatusID, workstatus.WorkStatus AS WorkStatus, workstatus.AltWorkStatus AS AltWorkStatus,  " +
                    "workpriority.WorkPriorityID AS WorkPriorityID, workpriority.WorkPriority AS WorkPriority, workpriority.AltWorkPriority AS AltWorkPriority, " +
                    "Requestor.EmployeeID AS RequestorID, Requestor.EmployeeNO AS RequestorNo, Requestor.Name AS RequestorName,Requestor.AltName AS RequestorAltName,Requestor.Email AS RequestorEmail, " +
                    "Created.EmployeeID AS CreatedID, Created.EmployeeNO AS CreatedNo, Created.Name AS CreatedName,Created.AltName AS CreatedAltName,Created.Email AS CreatedEmail " +
                    "FROM workorders " + "LEFT JOIN L2 ON L2.L2ID = workorders.L2ID " +
                    "LEFT JOIN L3 ON L3.L3ID = workorders.L3ID " +
                    "LEFT JOIN L4 ON L4.L4ID = workorders.L4ID " +
                    "LEFT JOIN L5 ON L5.L5ID = workorders.L5ID " +
                    "LEFT JOIN location ON location.LocationID = workorders.LocationID " +
                    "LEFT JOIN LocationType ON LocationType.LocationTypeID = location.LocationTypeID " +
                    "LEFT JOIN assets ON assets.AssetID = workorders.AssetID " +
                    "LEFT JOIN assetsubcategories ON assetsubcategories.AssetSubCatID = assets.AssetSubCatID " +
                    "LEFT JOIN Criticality ON Criticality.id = assets.CriticalityId " +
                    "LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = workorders.MaintDivisionID " +
                    "LEFT JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID =workorders.maintDeptID " +
                    "LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = workorders.maintsubdeptID " +
                    "LEFT JOIN worktrade ON worktrade.WorkTradeID = workorders.WOTradeID " +
                    "LEFT JOIN worktype ON worktype.WorkTypeID = workorders.WorkTypeID " +
                    "LEFT JOIN workstatus ON workstatus.WorkStatusID = workorders.WorkStatusID " +
                    "LEFT JOIN workpriority ON workpriority.WorkPriorityID = workorders.workpriorityid " +
                    "LEFT JOIN employees  Requestor ON Requestor.EmployeeID = workorders.RequestorID " +
                    "LEFT JOIN employees  Created ON Created.EmployeeID = workorders.EmployeeID " +
                    "WHERE workorders.L2ID IN (" + StringL2IDs + ") ";

                    if ((ObjectEmailNotificationRule.MaintDivisionID != null))
                    {
                        Query = Query + " AND workorders.MaintDivisionID = " + ObjectEmailNotificationRule.MaintDivisionID.ToString() + " ";
                    }
                    if ((ObjectEmailNotificationRule.MaintDeptID != null))
                    {
                        Query = Query + " AND workorders.maintdeptID = " + ObjectEmailNotificationRule.MaintDeptID.ToString() + " ";
                    }
                    if ((ObjectEmailNotificationRule.MaintSubDeptID != null))
                    {
                        Query = Query + " AND workorders.maintsubdeptID = " + ObjectEmailNotificationRule.MaintSubDeptID.ToString() + " ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkPriorityIDs))
                    {
                        Query = Query + " AND workorders.WorkPriorityID IN (" + StringWorkPriorityIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkTypeIDs))
                    {
                        Query = Query + " AND workorders.WorkTypeID IN (" + StringWorkTypeIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringWorkTradeIDs))
                    {
                        Query = Query + " AND workorders.WOTradeID IN (" + StringWorkTradeIDs + ") ";
                    }
                    if (!string.IsNullOrEmpty(StringBuildingIDs))
                    {
                        Query = Query + " AND ( workorders.L5ID IN (" + StringBuildingIDs + ") OR workorders.L5ID IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringLocationIDs))
                    {
                        Query = Query + " AND ( workorders.LocationID IN (" + StringLocationIDs + ") OR workorders.LocationID IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringLocationTypeIDs))
                    {
                        Query = Query + " AND ( LocationType.LocationTypeID IN (" + StringLocationTypeIDs + ") OR LocationType.LocationTypeID IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringSubCategoryIDs))
                    {
                        Query = Query + " AND ( assetsubcategories.AssetSubCatID IN (" + StringSubCategoryIDs + ") OR assetsubcategories.AssetSubCatID IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringCriticalityIDs))
                    {
                        Query = Query + " AND ( Criticality.id IN (" + StringCriticalityIDs + ") OR Criticality.id IS NULL) ";
                    }
                    if (!string.IsNullOrEmpty(StringAssetIDs))
                    {
                        Query = Query + " AND ( workorders.AssetID IN (" + StringAssetIDs + ") OR workorders.AssetID IS NULL) ";
                    }
                    Query = Query + " AND workorders.WorkStatusID = 1 ";

                    List<EmailTemplateWorkOrder> ListEmailTemplateWorkOrder = new List<EmailTemplateWorkOrder>();
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        ListEmailTemplateWorkOrder = objContext.ExecuteQueryWithClientID<EmailTemplateWorkOrder>(Query, new Collection<DBParameters>(), clientCode).ToList();
                    }

                    if (ListEmailTemplateWorkOrder != null && ListEmailTemplateWorkOrder.Count > 0)
                    {
                        EmailNotificationEscalationRule ObjectEmailNotificationEscalationRule = new EmailNotificationEscalationRule();
                        ObjectEmailNotificationEscalationRule.ObjectEmailNotificationRule = ObjectEmailNotificationRule;
                        ObjectEmailNotificationEscalationRule.ListWorkOrder = ListEmailTemplateWorkOrder;
                        ObjectEmailNotificationEscalationRule.ListEmailReceiptant_Employee = GetListEmailEscalationEmployeeByNotificationRuleID(ObjectEmailNotificationRule.NotificationRuleID, clientCode);

                        ListEmailNotificationEscalationRule.Add(ObjectEmailNotificationEscalationRule);
                    }
                }

                return ListEmailNotificationEscalationRule;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<WorkOrderEscalation> GetWorkOrderEscalation(WorkOrderEscalation obj, string clientCode)
        {
            try
            {
                using (ServiceContext objContext = new ServiceContext())
                {
                    string Query = "Select * from WorkOrderEscalation where  NotificationRuleID =" + obj.NotificationRuleID + "and WorkOrderNo ='" + obj.WorkOrderNo + "'";
                    List<WorkOrderEscalation> lst = objContext.ExecuteQueryWithClientID<WorkOrderEscalation>(Query, new Collection<DBParameters>(), clientCode).ToList();

                    return lst;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertWorkOrderEscalation(WorkOrderEscalation obj, string clientCode, int dbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = obj.CreatedDate,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = Common.GetEnglishDate(obj.CreatedDate),
                        DBType = DbType.DateTime
                    });
                }

                string Query = "insert into WorkOrderEscalation(NotificationRuleID,WorkOrderNo,EscalationTimeInMin,EscalationLevel,CreatedDate,ModifiedDate) Values (" + obj.NotificationRuleID + ",'" + obj.WorkOrderNo + "'," + obj.EscalationTimeInMin + "," + obj.EscalationLevel +
                                ",@CreatedDate,null)";

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateWorkOrderEscalation(WorkOrderEscalation obj, string clientCode, int dbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = obj.ModifiedDate,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(obj.ModifiedDate),
                        DBType = DbType.DateTime
                    });
                }

                string Query = "UPDATE WorkOrderEscalation Set NotificationRuleID =" + obj.NotificationRuleID +
                                                               ",WorkOrderNo = '" + obj.WorkOrderNo +
                                                               "',EscalationTimeInMin = " + obj.EscalationTimeInMin +
                                                               ",EscalationLevel = " + obj.EscalationLevel +
                                                               ",ModifiedDate = @ModifiedDate" +
                                                               " Where WorkOrderEscalationID = " + obj.WorkOrderEscalationID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static List<WorkRequestEscalation> GetWorkRequestEscalation(WorkRequestEscalation obj, string clientCode)
        {
            try
            {
                using (ServiceContext objContext = new ServiceContext())
                {
                    string Query = "Select * from WorkRequestEscalation where  NotificationRuleID =" + obj.NotificationRuleID + "and RequestNo ='" + obj.RequestNo + "'";
                    List<WorkRequestEscalation> lst = objContext.ExecuteQueryWithClientID<WorkRequestEscalation>(Query, new Collection<DBParameters>(), clientCode).ToList();

                    return lst;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void InsertWorkRequestEscalation(WorkRequestEscalation obj, string clientCode, int dbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = obj.CreatedDate,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = Common.GetEnglishDate(obj.CreatedDate),
                        DBType = DbType.DateTime
                    });
                }

                string Query = "insert into WorkRequestEscalation(NotificationRuleID,RequestNo,EscalationTimeInMin,EscalationLevel,CreatedDate,ModifiedDate) Values (" + obj.NotificationRuleID + ",'" + obj.RequestNo + "'," + obj.EscalationTimeInMin + "," + obj.EscalationLevel +
                                ",@CreatedDate,null)";

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static void UpdateWorkRequestEscalation(WorkRequestEscalation obj, string clientCode, int dbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = obj.ModifiedDate,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(obj.ModifiedDate),
                        DBType = DbType.DateTime
                    });
                }

                string Query = "UPDATE WorkRequestEscalation Set NotificationRuleID =" + obj.NotificationRuleID +
                                                               ",RequestNo = '" + obj.RequestNo +
                                                               "',EscalationTimeInMin = " + obj.EscalationTimeInMin +
                                                               ",EscalationLevel = " + obj.EscalationLevel +
                                                               ",ModifiedDate = @ModifiedDate" +
                                                               " Where WorkRequestEscalationID = " + obj.WorkRequestEscalationID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static List<EmailReceiptant> GetListEmailEscalationEmployeeByNotificationRuleID(int EmailNotificationRuleID, string clientCode)
        {
            try
            {
                List<EmailReceiptant> ListEmailReceiptant = new List<EmailReceiptant>();
                List<employees> ListEmployees = new List<employees>();

                string Query = "SELECT * FROM emailnotificationrules_escalation WHERE NotificationRuleID = " + EmailNotificationRuleID;
                List<Emailnotificationrules_escalation> ListEmailNotificationRuleEscalations = new List<Emailnotificationrules_escalation>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    ListEmailNotificationRuleEscalations = objContext.ExecuteQueryWithClientID<Emailnotificationrules_escalation>(Query, new Collection<DBParameters>(), clientCode).ToList();
                    ListEmployees = objContext.SearchWithClientID<employees>(new employees(), 0, string.Empty, string.Empty, clientCode).ToList();
                }

                foreach (Emailnotificationrules_escalation ObjectEmailNotificationRuleEscalations in ListEmailNotificationRuleEscalations)
                {

                    dynamic ObjectTempEmployee = ListEmployees.FirstOrDefault(Item => Item.EmployeeID == ObjectEmailNotificationRuleEscalations.EmployeeID);

                    EmailReceiptant ObjectEmailReceiptant = new EmailReceiptant();
                    ObjectEmailReceiptant.ReceiptantID = ObjectTempEmployee.EmployeeID;
                    ObjectEmailReceiptant.ReceiptantName = ObjectTempEmployee.Name;
                    ObjectEmailReceiptant.ReceiptantAltName = ObjectTempEmployee.AltName;
                    ObjectEmailReceiptant.ReceiptantNo = ObjectTempEmployee.EmployeeNO;
                    ObjectEmailReceiptant.ReceiptantEmail = ObjectTempEmployee.Email;
                    ObjectEmailReceiptant.LanguageCode = ObjectTempEmployee.LanguageCode;
                    ObjectEmailReceiptant.EscalationTimeInMin = ObjectEmailNotificationRuleEscalations.EscalationTimeInMin;
                    ObjectEmailReceiptant.LastEmailSentTime = ObjectEmailNotificationRuleEscalations.LastEmailSentTime;
                    ObjectEmailReceiptant.HandPhone = ObjectTempEmployee.HandPhone;

                    ListEmailReceiptant.Add(ObjectEmailReceiptant);

                }

                return ListEmailReceiptant;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateLastEmailSentTime_EscalationEmailNotificationRule(int EmailNotificationRuleID, int EmployeeID, string clientCode, int dbType)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                parameters.Add(new DBParameters()
                {
                    Name = "LastEmailSentTime",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });
            }
            else
            {
                parameters.Add(new DBParameters()
                {
                    Name = "LastEmailSentTime",
                    Value = Common.GetEnglishDateSecond(DateTime.Now),
                    DBType = DbType.DateTime
                });
            }

            string Query = "UPDATE emailnotificationrules_escalation SET LastEmailSentTime = @LastEmailSentTime WHERE NotificationRuleID = " + EmailNotificationRuleID + " AND EmployeeID = " + EmployeeID;

            using (ServiceContext objContext = new ServiceContext())
            {
                objContext.ExecuteWithClientID(Query, parameters, clientCode);
            }
        }

        #endregion

        #region "Implementation of Material module"

        public static List<Emailnotificationrule> GetListEmailNotificationRule_Material(string clientCode)
        {
            try
            {
                string Query = "SELECT * FROM emailnotificationrules WHERE NotificationRuleModuleID = " + SystemEnum.ModuleCode.Materials.GetHashCode() + " AND isActive = 1";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailnotificationrule>(Query, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<EmailReceiptant> GetListEmployeeByNotificationRuleID(int EmailNotificationRuleID, string clientCode)
        {
            try
            {
                List<EmailReceiptant> ListEmailReceiptant = new List<EmailReceiptant>();
                List<employees> ListEmployees = new List<employees>();

                List<Emailnotificationrules_employee> ListEmailNotificationRules_Employee = GetListEmailNotificationRuleEmployeesByNotificationRuleID(EmailNotificationRuleID, clientCode);

                using (ServiceContext objContext = new ServiceContext())
                {
                    ListEmployees = objContext.SearchWithClientID<employees>(new employees(), 0, string.Empty, string.Empty, clientCode).ToList();
                }

                foreach (Emailnotificationrules_employee ObjectEmailNotificationRules_Employee in ListEmailNotificationRules_Employee)
                {
                    dynamic ObjectTempEmployee = ListEmployees.FirstOrDefault(Item => Item.EmployeeID == ObjectEmailNotificationRules_Employee.EmployeeID);
                    EmailReceiptant ObjectEmailReceiptant = new EmailReceiptant();
                    ObjectEmailReceiptant.ReceiptantID = ObjectTempEmployee.EmployeeID;
                    ObjectEmailReceiptant.ReceiptantName = ObjectTempEmployee.Name;
                    ObjectEmailReceiptant.ReceiptantAltName = ObjectTempEmployee.AltName;
                    ObjectEmailReceiptant.ReceiptantNo = ObjectTempEmployee.EmployeeNO;
                    ObjectEmailReceiptant.ReceiptantEmail = ObjectTempEmployee.Email;
                    ObjectEmailReceiptant.LanguageCode = ObjectTempEmployee.LanguageCode;
                    ObjectEmailReceiptant.LastEmailSentTime = ObjectEmailNotificationRules_Employee.LastEmailSentTime;
                    ObjectEmailReceiptant.IsSelectedCriticalProposal = ObjectEmailNotificationRules_Employee.IsSelectedCriticalProposal;
                    ObjectEmailReceiptant.IsSelectedNormalProposal = ObjectEmailNotificationRules_Employee.IsSelectedNormalProposal;
                    ObjectEmailReceiptant.HandPhone = ObjectTempEmployee.HandPhone;

                    ListEmailReceiptant.Add(ObjectEmailReceiptant);

                }

                return ListEmailReceiptant;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Stockcode_level> GetPurchaseProposalCritical(string clientCode, int dbType)
        {
            string Query = string.Empty;
            try
            {
                if (dbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vwStockBalanceQueryForPurchaseProposal.StockID ,
                                            vwStockBalanceQueryForPurchaseProposal.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vwStockBalanceQueryForPurchaseProposal.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL([return].Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) AS vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN [return] ON vwSubStoreANDStock.StockID = [return].StockID
                                                                                                AND vwSubStoreANDStock.L2ID = [return].L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = [return].SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vwStockBalanceQueryForPurchaseProposal
                                            LEFT JOIN stockcode_levels ON vwStockBalanceQueryForPurchaseProposal.StockID = stockcode_levels.stock_id
                                                                          AND vwStockBalanceQueryForPurchaseProposal.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vwStockBalanceQueryForPurchaseProposal.BalanceStock <= stockcode_levels.min_level) vw";
                }

                else if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vws.StockID ,
                                            vws.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vws.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL(""RETURN"".Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN ""RETURN"" ON vwSubStoreANDStock.StockID = ""RETURN"".StockID
                                                                                                AND vwSubStoreANDStock.L2ID = ""RETURN"".L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = ""RETURN"".SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vws
                                            LEFT JOIN stockcode_levels ON vws.StockID = stockcode_levels.stock_id
                                                                          AND vws.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vws.BalanceStock <= stockcode_levels.min_level) vw";
                }
                else if (dbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vws.StockID ,
                                            vws.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vws.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL(`RETURN`.Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN `RETURN` ON vwSubStoreANDStock.StockID = `RETURN`.StockID
                                                                                                AND vwSubStoreANDStock.L2ID = `RETURN`.L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = `RETURN`.SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vws
                                            LEFT JOIN stockcode_levels ON vws.StockID = stockcode_levels.stock_id
                                                                          AND vws.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vws.BalanceStock <= stockcode_levels.min_level) vw";
                }
                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Stockcode_level>(Query, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Stockcode_level> GetPurchaseProposalNormal(string clientCode, int dbType)
        {
            string Query = string.Empty;
            try
            {
                if (dbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vwStockBalanceQueryForPurchaseProposal.StockID ,
                                            vwStockBalanceQueryForPurchaseProposal.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vwStockBalanceQueryForPurchaseProposal.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL([return].Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) AS vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN [return] ON vwSubStoreANDStock.StockID = [return].StockID
                                                                                                AND vwSubStoreANDStock.L2ID = [return].L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = [return].SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vwStockBalanceQueryForPurchaseProposal
                                            LEFT JOIN stockcode_levels ON vwStockBalanceQueryForPurchaseProposal.StockID = stockcode_levels.stock_id
                                                                          AND vwStockBalanceQueryForPurchaseProposal.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vwStockBalanceQueryForPurchaseProposal.BalanceStock > stockcode_levels.min_level) vw";
                }
                else if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vws.StockID ,
                                            vws.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vws.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL(""RETURN"".Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN ""RETURN"" ON vwSubStoreANDStock.StockID = ""RETURN"".StockID
                                                                                                AND vwSubStoreANDStock.L2ID = ""RETURN"".L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = ""RETURN"".SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vws
                                            LEFT JOIN stockcode_levels ON vws.StockID = stockcode_levels.stock_id
                                                                          AND vws.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vws.BalanceStock > stockcode_levels.min_level) vw";
                }
                else if (dbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    Query = @"Select * from (
                                    SELECT vwStockBalanceQueryForPurchaseProposal.StockID ,
                                            vwStockBalanceQueryForPurchaseProposal.L2ID ,
                                            substore.SubStoreID ,
                                            stockcode.StockNo ,
                                            stockcode.StockDescription ,
                                            stockcode.Specification ,
                                            substore.SubStoreCode ,
                                            vwStockBalanceQueryForPurchaseProposal.BalanceStock ,
                                            stockcode_levels.max_level ,
                                            stockcode_levels.re_order_level ,
                                            stockcode_levels.min_level ,
                                            stockcode_levels.reorder_qty
                                     FROM   ( SELECT    StockID ,
                                                        L2ID ,
                                                        SUM(TotalReceive) AS TRec ,
                                                        SUM(TotalIssue) AS TI ,
                                                        SUM(TotalAdj) AS TAdj ,
                                                        SUM(TotalReturn) AS TReturn ,
                                                        SUM(BalanceStock) AS BalanceStock ,
                                                        SubStoreID
                                              FROM      ( SELECT    vwSubStoreANDStock.StockID ,
                                                                    vwSubStoreANDStock.L2ID ,
                                                                    vwSubStoreANDStock.SubStoreID ,
                                                                    ISNULL(receive.QtyRec, 0) AS TotalReceive ,
                                                                    ISNULL(issue.QtyIssue, 0) AS TotalIssue ,
                                                                    ISNULL([return].Qty, 0) AS TotalReturn ,
                                                                    ISNULL(adjustment.QtyAdj, 0) AS TotalAdj ,
                                                                    ISNULL(receive.QtyRec, 0)
                                                                    + ISNULL(`return`.Qty, 0)
                                                                    + ISNULL(adjustment.QtyAdj, 0)
                                                                    - ISNULL(issue.QtyIssue, 0) AS BalanceStock ,
                                                                    adjustment.L2ID AS AdjStCode ,
                                                                    adjustment.SubStoreID AS AdjSSC ,
                                                                    receive.L2ID AS RecStCode ,
                                                                    receive.SubStoreID AS RecSSC
                                                          FROM      ( SELECT    substore.SubStoreID ,
                                                                                substore.L2ID ,
                                                                                stockcode.StockID
                                                                      FROM      stockcode_levels
                                                                                INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                                                                INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                                                    ) As vwSubStoreANDStock
                                                                    LEFT OUTER JOIN adjustment ON vwSubStoreANDStock.StockID = adjustment.StockID
                                                                                                  AND vwSubStoreANDStock.L2ID = adjustment.L2ID
                                                                                                  AND vwSubStoreANDStock.SubStoreID = adjustment.SubStoreID
                                                                    LEFT OUTER JOIN `return` ON vwSubStoreANDStock.StockID = `return`.StockID
                                                                                                AND vwSubStoreANDStock.L2ID = `return`.L2ID
                                                                                                AND vwSubStoreANDStock.SubStoreID = `return`.SubStoreID
                                                                    LEFT OUTER JOIN issue ON vwSubStoreANDStock.StockID = issue.StockID
                                                                                             AND vwSubStoreANDStock.L2ID = issue.L2ID
                                                                                             AND vwSubStoreANDStock.SubStoreID = issue.SubStoreID
                                                                    LEFT OUTER JOIN receive ON vwSubStoreANDStock.StockID = receive.StockID
                                                                                               AND vwSubStoreANDStock.L2ID = receive.L2ID
                                                                                               AND vwSubStoreANDStock.SubStoreID = receive.SubStoreID
                                                        ) vwStockBalanceBySubStore
                                              GROUP BY  StockID ,
                                                        L2ID ,
                                                        SubStoreID
                                            ) vwStockBalanceQueryForPurchaseProposal
                                            LEFT JOIN stockcode_levels ON vwStockBalanceQueryForPurchaseProposal.StockID = stockcode_levels.stock_id
                                                                          AND vwStockBalanceQueryForPurchaseProposal.SubStoreID = stockcode_levels.sub_store_id
                                            INNER JOIN stockcode ON stockcode.StockID = stockcode_levels.stock_id
                                            INNER JOIN substore ON substore.SubStoreID = stockcode_levels.sub_store_id
                                     WHERE  vwStockBalanceQueryForPurchaseProposal.BalanceStock > stockcode_levels.min_level) vw";
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Stockcode_level>(Query, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void UpdateLastEmailSentTime_EmployeeEmailNotificationRule(int EmailNotificationRuleID, int EmployeeID, string clientCode, int dbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LastEmailSentTime",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LastEmailSentTime",
                        Value = Common.GetEnglishDateSecond(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }
                string Query = "UPDATE emailnotificationrules_employees SET LastEmailSentTime = '" + Common.GetEnglishDateSecond(DateTime.Now) + "' WHERE NotificationRuleID = " + EmailNotificationRuleID + " AND EmployeeID = " + EmployeeID;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private static EmailTemplateWorkRequest GetEmailTemplateWorkRequestByWorkRequestNo(string WorkRequestNo, string clientCode)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "RequestNo",
                    Value = WorkRequestNo,
                    DBType = DbType.String
                });

                string Query = "SELECT " +
                "worequest.RequestNo AS RequestNo, worequest.WorkorderNo AS WorkorderNo, worequest.ProblemDesc AS ProblemDesc, worequest.ReceivedDate AS ReceivedDate, worequest.RequiredDate AS RequiredDate, worequest.Remarks AS Remarks,  " +
                "L2.L2ID AS L2ID, L2.L2Code AS L2Code, L2.L2name AS L2name, L2.L2Altname AS L2Altname, " +
                "L3.L3ID AS L3ID, L3.L3No AS L3No, L3.L3Desc AS L3Desc, L3.L3AltDesc AS L3AltDesc, " +
                "L4.L4ID AS L4ID, L4.L4No AS L4No, L4.L4Description AS L4Description, L4.L4AltDescription AS L4AltDescription, " +
                "L5.L5ID AS L5ID, L5.L5No AS L5No, L5.L5Description AS L5Description, L5.L5AltDescription AS L5AltDescription, " +
                "location.LocationID AS LocationNo, location.LocationNo AS LocationNo, location.LocationDescription AS LocationDescription, location.LocationAltDescription AS LocationAltDescription, " +
                "assets.AssetID AS AssetID, assets.AssetNumber AS AssetNumber, assets.AssetDescription AS AssetDescription, assets.AssetAltDescription AS AssetAltDescription, " + "MainenanceDivision.MaintDivisionID AS MaintDivisionID,MainenanceDivision.MaintDivisionCode AS MaintDivisionCode, MainenanceDivision.MaintDivisionName AS MaintDivisionName,MainenanceDivision.MaintDivisionAltName AS MaintDivisionAltName, " +
                "MaintenanceDepartment.maintDeptID AS MaintDeptID,MaintenanceDepartment.MaintDeptCode AS MaintDeptCode, MaintenanceDepartment.MaintDeptdesc AS MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc AS MaintDeptAltdesc,  " +
                "MaintSubDept.MaintSubDeptID AS MaintSubDeptID,MaintSubDept.MaintSubDeptCode AS MaintSubDeptCode, MaintSubDept.MaintSubDeptDesc AS MaintSubDeptDesc,MaintSubDept.MaintSubDeptAltDesc AS MaintSubDeptAltDesc,  " +
                "worktrade.WorkTradeID AS WorkTradeID, worktrade.WorkTrade AS WorkTrade,worktrade.AltWorkTrade AS AltWorkTrade,  " +
                "worktype.WorkTypeID AS WorkTypeID,worktype.WorkTypeDescription AS WorkTypeDescription, worktype.AltWorkTypeDescription AS AltWorkTypeDescription, " +
                "workstatus.WorkStatusID AS RequestStatusID, workstatus.WorkStatus AS RequestStatus, workstatus.AltWorkStatus AS AltRequestStatus,  " +
                "workpriority.WorkPriorityID AS WorkPriorityID, workpriority.WorkPriority AS WorkPriority, workpriority.AltWorkPriority AS AltWorkPriority, " +
                "Requestor.EmployeeID AS RequestorID, Requestor.EmployeeNO AS RequestorNo, Requestor.Name AS RequestorName,Requestor.AltName AS RequestorAltName,Requestor.Email AS RequestorEmail, " +
                "Created.EmployeeID AS CreatedID, Created.EmployeeNO AS CreatedNo, Created.Name AS CreatedName,Created.AltName AS CreatedAltName,Created.Email AS CreatedEmail, " +
                "Cancelled.EmployeeID AS CancelledID, Cancelled.EmployeeNO AS CancelledNo, Cancelled.Name AS CancelledName,Cancelled.AltName AS CancelledAltName,Cancelled.Email AS CancelledEmail " +
                "FROM worequest " +
                "LEFT JOIN L2 ON L2.L2ID = worequest.L2ID " +
                "LEFT JOIN L3 ON L3.L3ID = worequest.L3ID " +
                "LEFT JOIN L4 ON L4.L4ID = worequest.L4ID " +
                "LEFT JOIN L5 ON L5.L5ID = worequest.L5ID " +
                "LEFT JOIN location ON location.LocationID = worequest.LocationID " +
                "LEFT JOIN assets ON assets.AssetID = worequest.AssetID " +
                "LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = worequest.MaintdivId " +
                "LEFT JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = worequest.MaintdeptId " +
                "LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = worequest.MainSubDeptId " +
                "LEFT JOIN worktrade ON worktrade.WorkTradeID = worequest.worktradeid " +
                "LEFT JOIN worktype ON worktype.WorkTypeID = worequest.WorkTypeID " +
                "LEFT JOIN workstatus ON workstatus.WorkStatusID = worequest.RequestStatusID " +
                "LEFT JOIN workpriority ON workpriority.WorkPriorityID = worequest.workpriorityid " +
                "LEFT JOIN employees Requestor ON Requestor.EmployeeID = worequest.RequesterID " +
                "LEFT JOIN employees Created ON Created.EmployeeID = worequest.CreatedByID " +
                "LEFT JOIN employees Cancelled ON Cancelled.EmployeeID = worequest.CancelledByID " +
                "WHERE worequest.RequestNo = @RequestNo";

                using (ServiceContext context = new ServiceContext())
                {
                    return context.ExecuteQueryWithClientID<EmailTemplateWorkRequest>(Query, parameters, clientCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static EmailTemplateWorkOrder GetEmailTemplateWorkOrderByWorkOrderNo(string WorkOrderNo, string clientCode)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = WorkOrderNo,
                    DBType = DbType.String
                });

                string Query = "SELECT  "
                    + "workorders.WorkorderNo AS WorkorderNo, workorders.ProblemDescription AS ProblemDescription, workorders.DateReceived AS DateReceived, workorders.DateRequired AS DateRequired, "
                    + "L2.L2ID AS L2ID, L2.L2Code AS L2Code, L2.L2name AS L2name, L2.L2Altname AS L2Altname, "
                    + "L3.L3ID AS L3ID, L3.L3No AS L3No, L3.L3Desc AS L3Desc, L3.L3AltDesc AS L3AltDesc, "
                    + "L4.L4ID AS L4ID, L4.L4No AS L4No, L4.L4Description AS L4Description, L4.L4AltDescription AS L4AltDescription, "
                    + "L5.L5ID AS L5ID, L5.L5No AS L5No, L5.L5Description AS L5Description, L5.L5AltDescription AS L5AltDescription, "
                    + "location.LocationID AS LocationNo, location.LocationNo AS LocationNo, location.LocationDescription AS LocationDescription, location.LocationAltDescription AS LocationAltDescription, "
                    + "assets.AssetID AS AssetID, assets.AssetNumber AS AssetNumber, assets.AssetDescription AS AssetDescription, assets.AssetAltDescription AS AssetAltDescription, "
                    + "MainenanceDivision.MaintDivisionID AS MaintDivisionID,MainenanceDivision.MaintDivisionCode AS MaintDivisionCode, MainenanceDivision.MaintDivisionName AS MaintDivisionName,MainenanceDivision.MaintDivisionAltName AS MaintDivisionAltName, "
                    + "MaintenanceDepartment.maintDeptID AS MaintDeptID,MaintenanceDepartment.MaintDeptCode AS MaintDeptCode, MaintenanceDepartment.MaintDeptdesc AS MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc AS MaintDeptAltdesc,  "
                    + "MaintSubDept.MaintSubDeptID AS MaintSubDeptID,MaintSubDept.MaintSubDeptCode AS MaintSubDeptCode, MaintSubDept.MaintSubDeptDesc AS MaintSubDeptDesc,MaintSubDept.MaintSubDeptAltDesc AS MaintSubDeptAltDesc,  "
                    + "worktrade.WorkTradeID AS WorkTradeID, worktrade.WorkTrade AS WorkTrade,worktrade.AltWorkTrade AS AltWorkTrade,  "
                    + "worktype.WorkTypeID AS WorkTypeID,worktype.WorkTypeDescription AS WorkTypeDescription, worktype.AltWorkTypeDescription AS AltWorkTypeDescription, "
                    + "workstatus.WorkStatusID AS WorkStatusID, workstatus.WorkStatus AS WorkStatus, workstatus.AltWorkStatus AS AltWorkStatus,  "
                    + "workpriority.WorkPriorityID AS WorkPriorityID, workpriority.WorkPriority AS WorkPriority, workpriority.AltWorkPriority AS AltWorkPriority, "
                    + "Requestor.EmployeeID AS RequestorID, Requestor.EmployeeNO AS RequestorNo, Requestor.Name AS RequestorName,Requestor.AltName AS RequestorAltName,Requestor.Email AS RequestorEmail, "
                    + "Created.EmployeeID AS CreatedID, Created.EmployeeNO AS CreatedNo, Created.Name AS CreatedName,Created.AltName AS CreatedAltName,Created.Email AS CreatedEmail "
                    + "FROM workorders "
                    + "LEFT JOIN L2 ON L2.L2ID = workorders.L2ID "
                    + "LEFT JOIN L3 ON L3.L3ID = workorders.L3ID "
                    + "LEFT JOIN L4 ON L4.L4ID = workorders.L4ID "
                    + "LEFT JOIN L5 ON L5.L5ID = workorders.L5ID "
                    + "LEFT JOIN location ON location.LocationID = workorders.LocationID " + "LEFT JOIN assets ON assets.AssetID = workorders.AssetID "
                    + "LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = workorders.MaintDivisionID "
                    + "LEFT JOIN MaintenanceDepartment ON MaintenanceDepartment.MaintDivisionID = MainenanceDivision.MaintDivisionID "
                    + "LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = workorders.maintsubdeptID "
                    + "LEFT JOIN worktrade ON worktrade.WorkTradeID = workorders.WOTradeID "
                    + "LEFT JOIN worktype ON worktype.WorkTypeID = workorders.WorkTypeID "
                    + "LEFT JOIN workstatus ON workstatus.WorkStatusID = workorders.WorkStatusID "
                    + "LEFT JOIN workpriority ON workpriority.WorkPriorityID = workorders.workpriorityid "
                    + "LEFT JOIN employees Requestor ON Requestor.EmployeeID = workorders.RequestorID "
                    + "LEFT JOIN employees Created ON Created.EmployeeID = workorders.EmployeeID "
                    + "WHERE workorders.WorkorderNo = @WorkorderNo";

                using (ServiceContext context = new ServiceContext())
                {
                    return context.ExecuteQueryWithClientID<EmailTemplateWorkOrder>(Query, parameters, clientCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static EmailTemplateSupplier GetEmailTemplateSuppliersByWorkOrderNo(string WorkOrderNo, string clientCode)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = WorkOrderNo,
                    DBType = DbType.String
                });

                string Query = "Select "
                               + " suppliers.SupplierID,suppliers.SupplierNo,suppliers.SupplierName,suppliers.AltSupplierName,suppliers.ContactName,suppliers.Address,suppliers.City,suppliers.PhoneNumber,suppliers.Email, "
                               + " WorkOrders.WorkorderNo,workorders.ProblemDescription "
                               + " from suppliers "
                               + " inner join WorkOrders on WorkOrders.SupplierID= suppliers.SupplierID "
                               + "WHERE workorders.WorkorderNo = @WorkorderNo";

                using (ServiceContext context = new ServiceContext())
                {
                    return context.ExecuteQueryWithClientID<EmailTemplateSupplier>(Query, parameters, clientCode).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "Email Template Methods"

        public static List<Emailtemplate> GetEmailTemplates()
        {
            try
            {
                string strQuery = "SELECT * FROM emailtemplate";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQuery<Emailtemplate>(strQuery, new Collection<DBParameters>()).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Emailtemplate GetEmailTemplateByEmailTemplateID(int EmailTemplateID, string clientCode)
        {
            try
            {
                string strQuery = "SELECT * FROM emailtemplate WHERE EmailTemplateID = " + EmailTemplateID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailtemplate>(strQuery, new Collection<DBParameters>(), clientCode).ToList().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Emailtemplate> GetEmailTemplateByEmailTemplateName(string EmailTemplateName, string clientCode)
        {
            try
            {
                string strQuery = "SELECT * FROM emailtemplate WHERE EmailTemplateName = '" + Common.setQuote(EmailTemplateName) + "'";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailtemplate>(strQuery, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Emailtemplatefield> GetEmailTemplateFields(string FieldType, string clientCode)
        {
            try
            {
                string strQuery = "SELECT * FROM emailtemplatefield WHERE FieldType = '" + Common.setQuote(FieldType) + "'";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailtemplatefield>(strQuery, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Private Methods for table emailnotificationrules_employees"

        private static List<Emailnotificationrules_employee> GetListEmailNotificationRuleEmployeesByNotificationRuleID(int EmailNotificationRuleID, string clientCode)
        {
            try
            {
                string Query = "SELECT * FROM emailnotificationrules_employees WHERE NotificationRuleID = " + EmailNotificationRuleID;
                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQueryWithClientID<Emailnotificationrules_employee>(Query, new Collection<DBParameters>(), clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Client Wise Methods"

        public static List<Account> GetAllClientsFromMasterDB(int clientStatus, string clientCode)
        {
            try
            {                
                //string Query = "Select * from Accounts ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (clientStatus > -1)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "AccStatus",
                        Value = clientStatus,
                        DBType = DbType.Int32
                    });

                //    Query += " Where AccStatus = @AccStatus";
                }

                string Query = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] where CONVERT(int, DecryptByKey(AccStatus)) = @AccStatus";

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    return objContext.ExecuteQueryWithClientID<Account>(Query, parameters, clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Notification - Scheduler"

        public static void InsertWOWRNotification(int NotificationInterval, string clientCode, int dbType)
        {
            try
            {
                string QueryWR = string.Empty;
                string QueryWO = string.Empty;
                if (SystemEnum.DBTYPE.SQL.GetHashCode() == dbType)
                {
                    QueryWR = @"
                insert into WorkOrderNotification(WOWRNo,WRWO,NotificationTypeID,RelevantTypeID,Title,Description,IsActive,EmployeeID,CreatedDate) 
                Select * from(
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 1  As NotificationTypeID,  AssetCategoryNotification.AssetCategoryNotificationID As  RelevantTypeID , NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive ,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join Assets on Assets.AssetID = WoRequest.AssetID
                left join AssetCategoryNotification on AssetCategoryNotification.AssetCatID = Assets.AssetCatID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 1
                Left Join NotificationType on NotificationType.NotificationTypeID = 1
                where  
                WoRequest.RequestStatusID = 1 and WoRequest.L2Id = AssetCategoryNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) >= AssetCategoryNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < AssetCategoryNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 2  As NotificationTypeID,  CriticalityNotification.CriticalityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join Assets on Assets.AssetID = WoRequest.AssetID
                left join CriticalityNotification on CriticalityNotification.CriticalityID = Assets.CriticalityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 2
                Left Join NotificationType on NotificationType.NotificationTypeID = 2
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = CriticalityNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) > CriticalityNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < CriticalityNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 3  As NotificationTypeID,  LocationTypeNotification.LocationTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join location on location.LocationID = WoRequest.LocationID
                left join LocationTypeNotification on LocationTypeNotification.LocationTypeID = location.LocationTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 3
                Left Join NotificationType on NotificationType.NotificationTypeID = 3
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = LocationTypeNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) > LocationTypeNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < LocationTypeNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 4  As NotificationTypeID,  WorkPriorityNotification.WorkPriorityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join WorkPriorityNotification on WorkPriorityNotification.WorkPriorityID = WoRequest.WorkPriorityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 4
                Left Join NotificationType on NotificationType.NotificationTypeID = 4
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = WorkPriorityNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) > WorkPriorityNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < WorkPriorityNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 5  As NotificationTypeID,  WorkTypeNotification.WorkTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join WorkTypeNotification on WorkTypeNotification.WorkTypeID = WoRequest.WorkTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 5
                Left Join NotificationType on NotificationType.NotificationTypeID = 5
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = WorkTypeNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) > WorkTypeNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < WorkTypeNotification.Response + @NotificationInterval
                
                UNION
                
                --Remaining for JOStatusNotification
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 7  As NotificationTypeID,  JRStatusNotification.JRStatusNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title , NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WoRequest
                left join JRStatusNotification on JRStatusNotification.WorkStatusID = WoRequest.RequestStatusID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 6
                Left Join NotificationType on NotificationType.NotificationTypeID = 6
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = JRStatusNotification.L2ID
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) > JRStatusNotification.Response
                and DATEDIFF(mi,WoRequest.CreatedDate,GETDATE()) < JRStatusNotification.Response + @NotificationInterval
                ) Temp
                ";

                    QueryWO = @"
                insert into WorkOrderNotification(WOWRNo,WRWO,NotificationTypeID,RelevantTypeID,Title,Description,IsActive,EmployeeID,CreatedDate) 
                Select * from(
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 1  As NotificationTypeID,  AssetCategoryNotification.AssetCategoryNotificationID As  RelevantTypeID , NotificationType.NotificationTitle As Title,NotificationType.NotificationDescription As Description, 1 As IsActive ,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join Assets on Assets.AssetID = workorders.AssetID
                left join AssetCategoryNotification on AssetCategoryNotification.AssetCatID = Assets.AssetCatID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 1
                Left Join NotificationType on NotificationType.NotificationTypeID = 1
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3 and WorkOrders.L2Id = AssetCategoryNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) >= AssetCategoryNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < AssetCategoryNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 2  As NotificationTypeID,  CriticalityNotification.CriticalityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title,NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join Assets on Assets.AssetID = workorders.AssetID
                left join CriticalityNotification on CriticalityNotification.CriticalityID = Assets.CriticalityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 2
                Left Join NotificationType on NotificationType.NotificationTypeID = 2
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = CriticalityNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) > CriticalityNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < CriticalityNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 3  As NotificationTypeID,  LocationTypeNotification.LocationTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join location on location.LocationID = WorkOrders.LocationID
                left join LocationTypeNotification on LocationTypeNotification.LocationTypeID = location.LocationTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 3
                Left Join NotificationType on NotificationType.NotificationTypeID = 3
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = LocationTypeNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) > LocationTypeNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < LocationTypeNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 4  As NotificationTypeID,  WorkPriorityNotification.WorkPriorityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join WorkPriorityNotification on WorkPriorityNotification.WorkPriorityID = WorkOrders.WorkPriorityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 4
                Left Join NotificationType on NotificationType.NotificationTypeID = 4
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = WorkPriorityNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) > WorkPriorityNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < WorkPriorityNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 5  As NotificationTypeID,  WorkTypeNotification.WorkTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join WorkTypeNotification on WorkTypeNotification.WorkTypeID = WorkOrders.WorkTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 5
                Left Join NotificationType on NotificationType.NotificationTypeID = 5
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = WorkTypeNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) > WorkTypeNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < WorkTypeNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 6  As NotificationTypeID,  JOStatusNotification.JOStatusNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title , NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, GETDATE() As CreatedDate
                from WorkOrders
                left join JOStatusNotification on JOStatusNotification.WorkStatusID = WorkOrders.WorkStatusID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 6
                Left Join NotificationType on NotificationType.NotificationTypeID = 6
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = JOStatusNotification.L2ID
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) > JOStatusNotification.Resolution
                and DATEDIFF(mi,WorkOrders.CreatedDate,GETDATE()) < JOStatusNotification.Resolution + @NotificationInterval
                ) Temp";
                }
                else
                {

                    QueryWR = @"
                insert into WorkOrderNotification(WOWRNo,WRWO,NotificationTypeID,RelevantTypeID,Title,Description,IsActive,EmployeeID,CreatedDate) 
                Select * from(
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 1  As NotificationTypeID,  AssetCategoryNotification.AssetCategoryNotificationID As  RelevantTypeID , NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive ,NotificationEmployee.EmployeeID AS EmployeeID,Sysdate As CreatedDate
                from WoRequest
                left join Assets on Assets.AssetID = WoRequest.AssetID
                left join AssetCategoryNotification on AssetCategoryNotification.AssetCatID = Assets.AssetCatID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 1
                Left Join NotificationType on NotificationType.NotificationTypeID = 1
                where  
                WoRequest.RequestStatusID = 1 and WoRequest.L2Id = AssetCategoryNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 >= AssetCategoryNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < AssetCategoryNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 2  As NotificationTypeID,  CriticalityNotification.CriticalityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WoRequest
                left join Assets on Assets.AssetID = WoRequest.AssetID
                left join CriticalityNotification on CriticalityNotification.CriticalityID = Assets.CriticalityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 2
                Left Join NotificationType on NotificationType.NotificationTypeID = 2
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = CriticalityNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 > CriticalityNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < CriticalityNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 3  As NotificationTypeID,  LocationTypeNotification.LocationTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WoRequest
                left join location on location.LocationID = WoRequest.LocationID
                left join LocationTypeNotification on LocationTypeNotification.LocationTypeID = location.LocationTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 3
                Left Join NotificationType on NotificationType.NotificationTypeID = 3
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = LocationTypeNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 > LocationTypeNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < LocationTypeNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 4  As NotificationTypeID,  WorkPriorityNotification.WorkPriorityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WoRequest
                left join WorkPriorityNotification on WorkPriorityNotification.WorkPriorityID = WoRequest.WorkPriorityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 4
                Left Join NotificationType on NotificationType.NotificationTypeID = 4
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = WorkPriorityNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 > WorkPriorityNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < WorkPriorityNotification.Response + @NotificationInterval
                
                UNION
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 5  As NotificationTypeID,  WorkTypeNotification.WorkTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WoRequest
                left join WorkTypeNotification on WorkTypeNotification.WorkTypeID = WoRequest.WorkTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 5
                Left Join NotificationType on NotificationType.NotificationTypeID = 5
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = WorkTypeNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 > WorkTypeNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < WorkTypeNotification.Response + @NotificationInterval
                
                UNION
                
                --Remaining for JOStatusNotification
                
                Select WoRequest.RequestNo As WOWRNo,1 As WRWO, 7  As NotificationTypeID,  JRStatusNotification.JRStatusNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title , NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WoRequest
                left join JRStatusNotification on JRStatusNotification.WorkStatusID = WoRequest.RequestStatusID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 6
                Left Join NotificationType on NotificationType.NotificationTypeID = 6
                where  
                WoRequest.RequestStatusID = 1  and WoRequest.L2Id = JRStatusNotification.L2ID
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 > JRStatusNotification.Response
                and (Sysdate - WoRequest.CreatedDate) * 24 * 60 < JRStatusNotification.Response + @NotificationInterval
                ) dual
                ";

                    QueryWO = @"
                insert into WorkOrderNotification(WOWRNo,WRWO,NotificationTypeID,RelevantTypeID,Title,Description,IsActive,EmployeeID,CreatedDate) 
                Select * from(
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 1  As NotificationTypeID,  AssetCategoryNotification.AssetCategoryNotificationID As  RelevantTypeID , NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive ,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WorkOrders
                left join Assets on Assets.AssetID = workorders.AssetID
                left join AssetCategoryNotification on AssetCategoryNotification.AssetCatID = Assets.AssetCatID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 1
                Left Join NotificationType on NotificationType.NotificationTypeID = 1
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3 and WorkOrders.L2Id = AssetCategoryNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 >= AssetCategoryNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < AssetCategoryNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 2  As NotificationTypeID,  CriticalityNotification.CriticalityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID, Sysdate As CreatedDate
                from WorkOrders
                left join Assets on Assets.AssetID = workorders.AssetID
                left join CriticalityNotification on CriticalityNotification.CriticalityID = Assets.CriticalityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 2
                Left Join NotificationType on NotificationType.NotificationTypeID = 2
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = CriticalityNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 > CriticalityNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < CriticalityNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 3  As NotificationTypeID,  LocationTypeNotification.LocationTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID,Sysdate As CreatedDate
                from WorkOrders
                left join location on location.LocationID = WorkOrders.LocationID
                left join LocationTypeNotification on LocationTypeNotification.LocationTypeID = location.LocationTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 3
                Left Join NotificationType on NotificationType.NotificationTypeID = 3
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = LocationTypeNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 > LocationTypeNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < LocationTypeNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 4  As NotificationTypeID,  WorkPriorityNotification.WorkPriorityNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID,Sysdate As CreatedDate
                from WorkOrders
                left join WorkPriorityNotification on WorkPriorityNotification.WorkPriorityID = WorkOrders.WorkPriorityID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 4
                Left Join NotificationType on NotificationType.NotificationTypeID = 4
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = WorkPriorityNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 > WorkPriorityNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < WorkPriorityNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 5  As NotificationTypeID,  WorkTypeNotification.WorkTypeNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title, NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID,Sysdate As CreatedDate
                from WorkOrders
                left join WorkTypeNotification on WorkTypeNotification.WorkTypeID = WorkOrders.WorkTypeID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 5
                Left Join NotificationType on NotificationType.NotificationTypeID = 5
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = WorkTypeNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 > WorkTypeNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < WorkTypeNotification.Resolution + @NotificationInterval
                
                UNION
                
                Select WorkOrders.WorkorderNo As WOWRNo,2 As WRWO, 6  As NotificationTypeID,  JOStatusNotification.JOStatusNotificationID As  RelevantTypeID, NotificationType.NotificationTitle As Title , NotificationType.NotificationDescription As Description, 1 As IsActive,NotificationEmployee.EmployeeID AS EmployeeID,Sysdate As CreatedDate
                from WorkOrders
                left join JOStatusNotification on JOStatusNotification.WorkStatusID = WorkOrders.WorkStatusID
                left join NotificationEmployee on NotificationEmployee.NotificationTypeID = 6
                Left Join NotificationType on NotificationType.NotificationTypeID = 6
                where  
                WorkOrders.WorkStatusID != 2 and WorkOrders.WorkStatusID != 3  and WorkOrders.L2Id = JOStatusNotification.L2ID
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60  > JOStatusNotification.Resolution
                and (Sysdate - WorkOrders.CreatedDate) * 24 * 60 < JOStatusNotification.Resolution + @NotificationInterval
                ) dual";
                }

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationInterval",
                    Value = NotificationInterval,
                    DBType = DbType.Int32
                });

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteWithClientID(QueryWR, parameters, clientCode);
                    objContext.ExecuteWithClientID(QueryWO, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
