﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.TransactionService
{
    public class GoodReceiptService : DBExecute
    {
        public GoodReceiptService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Good Receipt"

        public static PurchaseOrder GetGoodReceiptDetail(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " Select PurchaseOrder.*, " +
                            " OrderByEmp.EmployeeNO As OrderByEmployeeNO,OrderByEmp.Name As OrderByEmployeeName,OrderByEmp.AltName As OrderByAltEmployeeName, " +
                            " InvoiceToEmp.EmployeeNO As InvoiceToEmployeeNO,InvoiceToEmp.Name As InvoiceToEmployeeName,InvoiceToEmp.AltName As InvoiceToAltEmployeeName, " +
                            " Accountcode.AccountCode,Accountcode.AccountCodeDesc,Accountcode.AltAccountCodeDesc,  " +
                            " CostCenter.CostCenterNo,CostCenter.CostCenterName,CostCenter.AltCostCenterName, " +
                            " suppliers.SupplierNo,suppliers.SupplierName,suppliers.AltSupplierName, " +
                            " Currency.CurrencyCode,Currency.CurrencyDescription,Currency.AltCurrencyDescription,Currency.Rate as CurrencyRate, L2.L2Code,L2.L2name,L2.L2Altname, " +
                            " Pt.PaymentTermsDesc, Pt.AltPaymentTermsDesc,sv.ShippedViaDesc,sv.AltShippedViaDesc,ts.TermofSaleDesc, ts.AltTermofSaleDesc " +
                            " FROM PurchaseOrder  " +
                            " INNER JOIN employees OrderByEmp on OrderByEmp.EmployeeID = PurchaseOrder.OrderBy " +
                            " INNER JOIN employees InvoiceToEmp on InvoiceToEmp.EmployeeID = PurchaseOrder.InvoiceTo " +
                            " INNER Join suppliers on suppliers.SupplierID = PurchaseOrder.SupplierID " +
                            " LEFT JOIN Accountcode on Accountcode.AccCodeid = PurchaseOrder.AccountCodeID " +
                            " LEFT JOIN CostCenter on CostCenter.CostCenterId = accountcode.CostCenterId " +
                            " LEFT JOIN Currency on Currency.CurrencyID = PurchaseOrder.CurrencyID " +
                            " LEFT JOIN L2 on L2.L2ID = PurchaseOrder.L2ID " +
                            " LEFT JOIN PaymentTerms pt ON pt.ID = PurchaseOrder.PaymentTermsID " +
                            " LEFT JOIN ShippedVia sv ON sv.ID = PurchaseOrder.ShippedViaID " +
                            " LEFT JOIN TermofSale ts ON ts.ID = PurchaseOrder.TermOfSaleID " +
                            " Where PurchaseOrder.ID = @ID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PurchaseOrder>(query, parameters).FirstOrDefault();
            }
        }

        public IList<Tran> GetPOItemTransactions(int pageNo, string sortExpression, string sortDirection, int id, string allItemdone, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Tran> list = new List<Tran>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereClause = string.Empty;

            query += "SELECT t.TransactionID,s.StockNo,s.StockDescription,s.AltStockDescription,poi.PartDescription,t.TransactionType,t.Quantity,poi.UnitPrice,t.TransDate,t.DeliveryOrderNo,t.InvoiceNo,t.InvoiceDate ";
            query += "FROM trans t ";
            query += "LEFT JOIN PurchaseOrderItems poi ON poi.ID = t.PurchaseOrderItemsId ";
            query += "LEFT JOIN stockcode s ON s.StockID = poi.PartNumber ";
            query += "LEFT JOIN PurchaseOrder po ON po.ID = poi.PurchaseOrderID ";
            query += "WHERE ";
            if (allItemdone == "1")
            {
                query += " t.Quantity > 0  AND ";
            }
            query += " po.ID = " + id;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Tran>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public PoItemDetailModal GetTranDetailByPOItemID(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            IList<Tran> list = new List<Tran>();

            PoItemDetailModal objPoItemDetailModal = new PoItemDetailModal();

            PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();

            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " SELECT poi.Quantity AS TotalQuantity,t.* FROM trans t " +
                            " LEFT JOIN PurchaseOrderItems poi ON poi.ID = t.PurchaseOrderItemsId " +
                            "Where poi.ID = @ID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Tran>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;
            }
            objPoItemDetailModal.TotalOrdered = ConvertTo.Decimal(new DapperContext().SelectObject<PurchaseOrderItem>(ID).Quantity);
            if (list.Count > 0)
            {
                objPoItemDetailModal.LastReceivedDate = list.Max(o => o.TransDate);
                objPoItemDetailModal.TotalReceived = ConvertTo.Decimal(list.Where(o => o.TransactionType == 1).Sum(o => o.Quantity));
                objPoItemDetailModal.TotalCancelled = ConvertTo.Decimal(list.Where(o => o.TransactionType == 2).Sum(o => o.Quantity));
                objPoItemDetailModal.Balance = ConvertTo.Decimal((objPoItemDetailModal.TotalOrdered - objPoItemDetailModal.TotalReceived - objPoItemDetailModal.TotalCancelled));
            }
            else
            {
                objPoItemDetailModal.LastReceivedDate = null;
                objPoItemDetailModal.TotalReceived = ConvertTo.Decimal(0.00);
                objPoItemDetailModal.TotalCancelled = ConvertTo.Decimal(0.00);
                objPoItemDetailModal.Balance = ConvertTo.Decimal((objPoItemDetailModal.TotalOrdered - objPoItemDetailModal.TotalReceived - objPoItemDetailModal.TotalCancelled));
            }
            return objPoItemDetailModal;
        }

        public PoItemDetailModal CalculateAllItemsSummary(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            IList<Tran> list = new List<Tran>();

            PoItemDetailModal objPoItemDetailModal = new PoItemDetailModal();

            PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();

            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " SELECT t.* FROM trans t " +
                            " LEFT JOIN PurchaseOrderItems poi ON poi.ID = t.PurchaseOrderItemsId " +
                            "Where poi.PurchaseOrderID = @ID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Tran>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;
            }
            objPoItemDetailModal.TotalOrdered = ConvertTo.Decimal(new DapperContext().SearchAll(objPurchaseOrderItem).Where(o => o.PurchaseOrderID == ID).Sum(o => o.Quantity));
            if (list.Count > 0)
            {
                objPoItemDetailModal.LastReceivedDate = list.Max(o => o.TransDate);
                objPoItemDetailModal.TotalReceived = ConvertTo.Decimal(list.Where(o => o.TransactionType == 1).Sum(o => o.Quantity));
                objPoItemDetailModal.TotalCancelled = ConvertTo.Decimal(list.Where(o => o.TransactionType == 2).Sum(o => o.Quantity));
                objPoItemDetailModal.Balance = ConvertTo.Decimal((objPoItemDetailModal.TotalOrdered - objPoItemDetailModal.TotalReceived - objPoItemDetailModal.TotalCancelled));
            }
            else
            {
                objPoItemDetailModal.LastReceivedDate = null;
                objPoItemDetailModal.TotalReceived = ConvertTo.Decimal(0.00);
                objPoItemDetailModal.TotalCancelled = ConvertTo.Decimal(0.00);
                objPoItemDetailModal.Balance = ConvertTo.Decimal((objPoItemDetailModal.TotalOrdered - objPoItemDetailModal.TotalReceived - objPoItemDetailModal.TotalCancelled));
            }
            return objPoItemDetailModal;
        }

        public IList<Substore> GetPOStoreByPOID(int id)
        {
            IList<Substore> list = new List<Substore>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "SELECT s.SubStoreID,s.SubStoreCode,s.SubStoreDesc,s.AltSubStoreDesc FROM substore s " +
                            "INNER JOIN PurchaseOrder po ON po.L2ID = s.L2ID " +
                            "WHERE po.ID = " + id;

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Substore>(query, outParameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public bool UpdatePurchaseOrder(PurchaseOrder ObjOldPurchaseOrder)
        {
            try
            {
                int id = 0;
                if (ObjOldPurchaseOrder != null && ObjOldPurchaseOrder.ID > 0)
                {
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        id = objDapperContext.Save(ObjOldPurchaseOrder);

                        if (ObjOldPurchaseOrder.DeliveryStatus == "Full Delivery")
                        {
                            string query = "Update PurchaseOrder set orderstatus = 'Closed' where Id= " + ObjOldPurchaseOrder.ID;
                            objDapperContext.ExecuteQuery(query, null);
                            string strQuery = "Update PurchaseRequest set status_id = 2 where Id in (Select PurchaseRequestItemID from PurchaseOrderItems Where PurchaseOrderID = " + ObjOldPurchaseOrder.ID + " )";
                            objDapperContext.ExecuteQuery(strQuery, null);
                        }
                    }
                }
                if (id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region "Save PO Item"

        public bool ReceiveAllPOItems(Receive objReceive)
        {
            try
            {
                PurchaseOrder objPurchaseOrder = new PurchaseOrder();
                List<PurchaseOrderItem> lstPurchaseOrderItem = new List<PurchaseOrderItem>();
                List<Tran> lstTran = new List<Tran>();
                PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();
                Tran objTran = new Tran();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    lstPurchaseOrderItem = objDapperContext.SearchAll(objPurchaseOrderItem).Where(o => o.PurchaseOrderID == objReceive.ID).ToList();
                    objPurchaseOrder = objDapperContext.SelectObject<PurchaseOrder>(ConvertTo.Integer(objReceive.ID));

                    foreach (var lst in lstPurchaseOrderItem)
                    {
                        Receive objNewReceive = new Receive();

                        decimal? Ttl = objDapperContext.SearchAll(objTran).Where(o => o.PurchaseOrderItemsId == lst.ID).Sum(o => o.Quantity);
                        decimal? Qty = lst.Quantity - Ttl;
                        decimal? Price = lst.UnitPrice * Qty;

                        if (lst.PartNumber > 0 || ConvertTo.Integer(objReceive.SubStoreID) != 0)
                        {
                            objNewReceive.CreatedDate = objReceive.CreatedDate;
                            objNewReceive.CreatedBy = objReceive.CreatedBy;
                            objNewReceive.InvoiceDate = objReceive.InvoiceDate;
                            objNewReceive.DeliveryOrderNo = objReceive.DeliveryOrderNo;
                            objNewReceive.SubStoreID = objReceive.SubStoreID;
                            objNewReceive.MRNNo = objReceive.MRNNo;
                            objNewReceive.Date = objReceive.Date;

                            objNewReceive.L2ID = objPurchaseOrder.L2ID;
                            objNewReceive.PONo = objPurchaseOrder.PurchaseOrderNo;
                            objNewReceive.SupplierID = objPurchaseOrder.SupplierID;
                            objNewReceive.StockID = lst.PartNumber;

                            objNewReceive.QtyRec = Qty;
                            objNewReceive.Price = Price;
                            objDapperContext.Save(objNewReceive);
                        }
                        else if (String.IsNullOrEmpty(lst.PartNumber.ToString()) && ConvertTo.Integer(objReceive.SubStoreID) == 0)
                        {
                            Direct objDirect = new Direct();
                            objDirect.CreatedDate = objReceive.CreatedDate;
                            objDirect.CreatedBy = objReceive.CreatedBy;
                            objDirect.Date = objReceive.Date;
                            objDirect.Invoicenumber = objReceive.MRNNo;
                            objDirect.L2ID = objPurchaseOrder.L2ID;
                            objDirect.Qty = Qty;
                            objDirect.Price = Price;
                            objDirect.StockDesc = lst.PartDescription;
                            objDirect.Supplierid = objPurchaseOrder.SupplierID;
                            objDirect.WONo = lst.JobOrderID;
                            objDirect.PRNo = lst.PurchaseRequestNo;
                            objDapperContext.Save(objDirect);
                        }

                        Tran objNewTran = new Tran();
                        objNewTran.PurchaseOrderItemsId = lst.ID;
                        objNewTran.TransactionType = 1; //'1 = Received Transaction; 2 = Cancelled Transaction
                        objNewTran.TransDate = objReceive.Date;
                        objNewTran.InvoiceNo = objReceive.MRNNo;
                        objNewTran.InvoiceDate = objReceive.InvoiceDate;
                        objNewTran.DeliveryOrderNo = objReceive.DeliveryOrderNo;
                        objNewTran.Quantity = Qty;
                        objDapperContext.Save(objNewTran);
                    }

                    //Where Received All items PO and PR order status should be closed
                    string query = "Update PurchaseOrder set orderstatus = 'Closed' " +
                                   "where Id= " + objReceive.ID;

                    objDapperContext.ExecuteQuery(query, null);

                    string strQuery = "Update PurchaseRequest set status_id = 2 where Id in (Select PurchaseRequestItemID from PurchaseOrderItems Where PurchaseOrderID = " + objReceive.ID + " )";

                    objDapperContext.ExecuteQuery(strQuery, null);

                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CancelPOItem(Tran objTran)
        {
            try
            {
                objTran.TransactionType = 2;
                objTran.PurchaseOrderItemsId = objTran.PurchaseOrderItemsId;
                using (DapperContext objDapperContext = new DapperContext())
                {
                    int id = objDapperContext.Save(objTran);
                    if (id > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool NewReceiveItem(Receive objReceive)
        {
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    if (ConvertTo.Integer(objReceive.SubStoreID) != 0 || ConvertTo.Integer(objReceive.StockID) != 0) // If PartID and/or SubstoreID is/are available, make an entry into 'receive' table
                    {
                        objDapperContext.Save(objReceive);
                    }
                    else if (ConvertTo.Integer(objReceive.SubStoreID) == 0 && ConvertTo.Integer(objReceive.StockID) == 0) //If PartID and SubstoreID both are not available, make an entry into 'direct' table
                    {
                        Direct objDirect = new Direct();
                        objDirect.CreatedDate = objReceive.CreatedDate;
                        objDirect.CreatedBy = objReceive.CreatedBy;
                        objDirect.Date = objReceive.Date;
                        objDirect.Invoicenumber = objReceive.MRNNo;
                        objDirect.L2ID = objReceive.L2ID;
                        objDirect.Qty = objReceive.QtyRec;
                        objDirect.Price = objReceive.Price;
                        objDirect.StockDesc = objReceive.StockDesc;
                        objDirect.Supplierid = objReceive.SupplierID;
                        objDirect.WONo = objReceive.WONo;
                        objDirect.PRNo = objReceive.PRNo;
                        objDapperContext.Save(objDirect);
                    }

                    //New entry in trans table
                    Tran objNewTran = new Tran();
                    objNewTran.PurchaseOrderItemsId = objReceive.POItemID;
                    objNewTran.TransactionType = 1; //'1 = Received Transaction; 2 = Cancelled Transaction
                    objNewTran.TransDate = objReceive.Date;
                    objNewTran.InvoiceNo = objReceive.MRNNo;
                    objNewTran.InvoiceDate = objReceive.InvoiceDate;
                    objNewTran.DeliveryOrderNo = objReceive.DeliveryOrderNo;
                    objNewTran.Quantity = objReceive.QtyRec;
                    objDapperContext.Save(objNewTran);

                    // item has been received then we need to set the purchase request to closed.
                    string query = "Update PurchaseRequest set status_id = 2 where Id = " + objReceive.POItemID;

                    objDapperContext.ExecuteQuery(query, null);
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
