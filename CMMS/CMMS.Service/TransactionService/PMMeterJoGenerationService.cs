﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.IO;

namespace CMMS.Service
{
    public class PMMeterJoGenerationService : DBExecute
    {
        public PMMeterJoGenerationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public void ClearPMLockMetersIfStuck(int userID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "userID",
                    Value = userID,
                    DBType = DbType.Int32
                });

                string query = "Delete from PMLockMeters where  UserId = @userID";

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveMeterIntermediateData(int Userid, int L2ID, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query1 = "Delete from intermediate_pmmetersgenerate where Userid = " + Userid;
                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query1, parameters);
                }

                string strWhere = "";
                if (MaintDivisionID > 0)
                {
                    strWhere = strWhere + " and pmmeters.MaintDivisionID = " + MaintDivisionID;
                }
                if (maintDeptID > 0)
                {
                    strWhere = strWhere + " and pmmeters.maintDeptID = " + maintDeptID;
                }
                if (MaintSubDeptID > 0)
                {
                    strWhere = strWhere + " and pmmeters.MaintSubDeptID = " + MaintSubDeptID;
                }
                else if (Userid > 0)
                {
                    if (!ProjectSession.IsCentral)
                    {
                        strWhere = strWhere + " and pmmeters.MaintSubDeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + Userid +
                                    " union Select MaintSubDeptID from employees where employeeId = " + Userid + " ) ";
                    }
                }

                string query2 = "INSERT INTO intermediate_pmmetersgenerate (ReadingScheduleID, MeterID, MeterNo, MeterDescription, MeterType," +
                               " AssetID, AssetNumber, AssetDescription, AssetAltDescription, LocationID, LocationNo, " +
                               " LocationDescription, LocationAltDescription, L2ID, L2Code, L3ID, L3No, L4ID, L4No, L5ID, L5No, " +
                               " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                               " Lowerlimit, PMLowTaskID, PMLowTaskNo, UpperLimit, PMUppTaskID, " +
                               " PMUppTaskNo, PMIncTaskID, PMIncTaskNo, Reading, MeterUnit, LastReading, LastPMDue, " + " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, Userid) " +
                               " select ReadingSchedule.ReadingScheduleID, pmmeters.MeterID, pmmeters.MeterNo, pmmeters.MeterDescription, PMMeterMaster.MeterType, " +
                               " assets.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " +
                               " location.LocationID,location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                               " L2.L2ID, L2.L2Code, L3.L3ID, L3.L3No, L4.L4ID, L4.L4No, L5.L5ID, L5.L5No, " +
                               " MainenanceDivision.MaintDivisionID, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.maintDeptID, " +
                               " MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " +
                               " ReadingSchedule.Lowerlimit, PMLowTaskID, p1.ChecklistNo as PMLowTaskNo, ReadingSchedule.UpperLimit, " +
                               " PMUppTaskID, p2.ChecklistNo as PMUppTaskNo, PMIncTaskID, p3.ChecklistNo as PMIncTaskNo, Reading, MeterUnit, " +
                               " ReadingSchedule.LastReading, LastPMDue, worktrade.WorkTradeID, worktrade.WorkTrade, worktrade.AltWorkTrade, " + " 2 , " +
                               Userid + " as Userid from ReadingSchedule " + " inner join pmmeters on ReadingSchedule.MeterID = pmmeters.MeterID " +
                               " inner join PMMeterMaster on PMMeterMaster.MeterMasterID = pmmeters.MeterMasterID " +
                               " inner join assets on assets.AssetID = PMMeterMaster.AssetID " +
                               " inner join location on assets.LocationID = location.LocationID " +
                               " inner join L2 on L2.L2ID = location.L2ID " +
                               " left outer join L3 on L3.L3ID = location.L3ID " +
                               " left outer join L4 on L4.L4ID = location.L4ID " +
                               " left outer join L5 on L5.L5ID = location.L5ID " + " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = pmmeters.MaintDivisionID " +
                               " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = pmmeters.maintDeptID " +
                               " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = pmmeters.MaintSubDeptID " +
                               " inner join worktrade on worktrade.WorkTradeID = pmmeters.worktradeid " +
                               " left outer join pmchecklist p1 on p1.ChecklistID = pmmeters.PMLowTaskID " +
                               " left outer join pmchecklist p2 on p2.ChecklistID = pmmeters.PMUppTaskID " +
                               " left outer join pmchecklist p3 on p3.ChecklistID = pmmeters.PMIncTaskID " +
                               " where GenStatus='Yes' " +
                    //juhi
                               "and PMMeterMaster.L2ID = " + L2ID +
                    //End juhi
                               strWhere;


                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query2, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SetPMLockMeters(int Userid, int L2ID, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            int RecordCount = 0;

            if (!(MaintDivisionID > 0))
            {
                string query1 = "Select count(1) As noOfData from PMLockMeters " + " where L2ID = " + L2ID;
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query1));
                }
            }
            else if (!(maintDeptID > 0))
            {
                string query2 = "Select count(1) As noOfData from PMLockMeters " + " where L2ID = " + L2ID + " and (MaintDivisionID = 0 or MaintDivisionID = " + MaintDivisionID + ")";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query2));
                }

            }
            else if (!(MaintSubDeptID > 0))
            {
                string query3 = "Select count(1) As noOfData from PMLockMeters " + " where L2ID = " + L2ID + " and ((MaintDivisionID = " + MaintDivisionID + " and maintDeptID = " + maintDeptID +
                                ") or" + " (MaintDivisionID = 0) or (MaintDivisionID = " + MaintDivisionID + " and maintDeptID = 0))";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query3));
                }

            }
            else
            {
                string query4 = "Select count(1) As noOfData from PMLockMeters " + " where L2ID = " + L2ID + " and ((MaintDivisionID = " + MaintDivisionID + " and " +
                                " maintDeptID = " + maintDeptID + " and MaintSubDeptID = " + MaintSubDeptID + ") " + " or (MaintDivisionID = 0) " +
                                " or (MaintDivisionID = " + MaintDivisionID + " and maintDeptID = 0 and MaintSubDeptID = 0) " + " or (MaintDivisionID = " + MaintDivisionID +
                                " and maintDeptID = " + maintDeptID + " and MaintSubDeptID = 0)) ";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query4));
                }

            }
            if (RecordCount > 0)
            {
                return false;
            }
            else
            {
                string query5 = "INSERT INTO PMLockMeters (L2ID, MaintDivisionID, maintDeptID, MaintSubDeptID, Userid) values " + " (" + L2ID + ", " + MaintDivisionID + ", " +
                                maintDeptID + ", " + MaintSubDeptID + ", " + Userid + ")";
                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query5, parameters);
                }

                return true;
            }

        }

        public void GeneratePMMeterJO(int Userid, int L2ID, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = DateTime.Now,
                            DBType = DbType.DateTime
                        });

                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = Common.GetEnglishDate(DateTime.Now),
                            DBType = DbType.DateTime
                        });

                    }


                    //Region WOPM Meter Start
                    Int64 workordernumber = default(Int64);
                    string WONo = null;

                    string query1 = "Select * from intermediate_pmmetersgenerate " + " where Userid = " + Userid;
                    List<Intermediate_pmmetersgenerate> ObjectListPMMeterGen = context.ExecuteQuery<Intermediate_pmmetersgenerate>(query1, parameters).ToList();

                    dynamic ListMeterID = ObjectListPMMeterGen.Select(Item => Item.MeterID).Distinct().ToList();
                    PmMeter objPmMeter = new PmMeter();
                    List<PmMeter> ListObjectPMMeter = context.SearchAll<PmMeter>(objPmMeter).Where(Item => ListMeterID.Contains(Item.MeterID)).ToList();

                    Int64 CurrentMinNo = Convert.ToInt64(L2ID.ToString().PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));

                    WorkOrder objWorkOrder = new WorkOrder();
                    List<WorkOrder> TempListJO = (from c in context.SearchAll<WorkOrder>(objWorkOrder) select c).Where(x => x.WorkorderNo.Contains("PMM") & x.L2ID == L2ID).ToList();

                    foreach (WorkOrder item in TempListJO)
                    {
                        if (Convert.ToInt64(item.WorkorderNo.Replace("PMM", "")) > CurrentMinNo)
                        {
                            CurrentMinNo = Convert.ToInt64(item.WorkorderNo.Replace("PMM", ""));
                        }
                    }
                    workordernumber = CurrentMinNo + 1;
                    WONo = "PMM" + workordernumber.ToString().PadLeft(11, '0');

                    List<string> lstwoForNotication = new List<string>();

                    Collection<DBParameters> parameters1 = new Collection<DBParameters>();
                    foreach (Intermediate_pmmetersgenerate ObjPMMeterGen in ObjectListPMMeterGen)
                    {

                        //unable to find supplierid in pmchecklist table for CPMMS
                        //![EmployeeID2] = GetSuppFromCheckList(rsPMGen![ChecklistNo]) - in CworksSQL

                        string strInsertColumn = "";
                        string strInsertValues = "";
                        int PMChecklistID = 0;

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {

                            parameters1.Add(new DBParameters()
                            {
                                Name = "PMTarStartDate",
                                Value = DateTime.Now,
                                DBType = DbType.DateTime
                            });
                        }
                        else
                        {

                            parameters1.Add(new DBParameters()
                            {
                                Name = "PMTarStartDate",
                                Value = Common.GetEnglishDate(DateTime.Now),
                                DBType = DbType.DateTime
                            });
                        }

                        strInsertColumn = strInsertColumn + " PMTarStartDate, ";
                        strInsertValues = strInsertValues + "@PMTarStartDate, ";

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "PMTarCompDate",
                                Value = DateTime.Now.AddDays(7),
                                DBType = DbType.DateTime
                            });
                        }
                        else
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "PMTarCompDate",
                                Value = Common.GetEnglishDate(DateTime.Now.AddDays(7)),
                                DBType = DbType.DateTime
                            });
                        }

                        strInsertColumn = strInsertColumn + " PMTarCompDate, ";
                        strInsertValues = strInsertValues + "@PMTarCompDate, ";

                        if (ObjPMMeterGen.MeterType.ToLower() == "incremental")
                        {
                            if ((ObjPMMeterGen.PMIncTaskID != null))
                            {
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "PMIncTaskID",
                                    Value = ObjPMMeterGen.PMIncTaskID,
                                    DBType = DbType.Int32
                                });

                                strInsertColumn = strInsertColumn + " PMChecklistID, ";
                                strInsertValues = strInsertValues + "@PMIncTaskID, ";
                                PMChecklistID = Convert.ToInt32(ObjPMMeterGen.PMIncTaskID);
                            }
                        }
                        else if (ObjPMMeterGen.MeterType.ToLower() == "threshold")
                        {
                            if ((ObjPMMeterGen.PMLowTaskID != null))
                            {
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "PMLowTaskID",
                                    Value = ObjPMMeterGen.PMLowTaskID,
                                    DBType = DbType.Int32
                                });

                                strInsertColumn = strInsertColumn + " PMChecklistID, ";
                                strInsertValues = strInsertValues + "@PMLowTaskID , ";
                                PMChecklistID = Convert.ToInt32(ObjPMMeterGen.PMLowTaskID);
                            }
                        }
                        if ((ObjPMMeterGen.WOTradeID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "WOTradeID",
                                Value = ObjPMMeterGen.WOTradeID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " WOTradeID, ";
                            strInsertValues = strInsertValues + "@WOTradeID , ";
                        }
                        if ((ObjPMMeterGen.L3ID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "L3ID",
                                Value = ObjPMMeterGen.L3ID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " L3ID, ";
                            strInsertValues = strInsertValues + "@L3ID , ";
                        }
                        if ((ObjPMMeterGen.L4ID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "L4ID",
                                Value = ObjPMMeterGen.L4ID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " L4ID, ";
                            strInsertValues = strInsertValues + "@L4ID , ";
                        }
                        if ((ObjPMMeterGen.L5ID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "L5ID",
                                Value = ObjPMMeterGen.L5ID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " L5ID, ";
                            strInsertValues = strInsertValues + "@L5ID , ";
                        }
                        if ((ObjPMMeterGen.LocationID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "LocationID",
                                Value = ObjPMMeterGen.LocationID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " LocationID, ";
                            strInsertValues = strInsertValues + " @LocationID , ";
                        }
                        if ((ObjPMMeterGen.MaintDivisionID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "MaintDivisionID",
                                Value = ObjPMMeterGen.MaintDivisionID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " MaintDivisionID, ";
                            strInsertValues = strInsertValues + "@MaintDivisionID , ";
                        }
                        if ((ObjPMMeterGen.MaintDeptID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "MaintDeptID",
                                Value = ObjPMMeterGen.MaintDeptID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " maintdeptID, ";
                            strInsertValues = strInsertValues + "@MaintDeptID , ";
                        }
                        if ((ObjPMMeterGen.MaintSubDeptID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "MaintSubDeptID",
                                Value = ObjPMMeterGen.MaintSubDeptID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " maintsubdeptID, ";
                            strInsertValues = strInsertValues + "@MaintSubDeptID , ";
                        }
                        if ((ObjPMMeterGen.AssetID != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "AssetID",
                                Value = ObjPMMeterGen.AssetID,
                                DBType = DbType.Int32
                            });

                            strInsertColumn = strInsertColumn + " assetid, ";
                            strInsertValues = strInsertValues + "@AssetID, ";
                        }

                        if ((ObjPMMeterGen.AssetID != null))
                        {
                            Asset objAsset = new Asset();
                            objAsset.AssetID = Convert.ToInt32(ObjPMMeterGen.AssetID);
                            Nullable<int> AuthEmpId = context.Search<Asset>(objAsset).FirstOrDefault().EmployeeID;


                            if ((AuthEmpId != null))
                            {
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "AuthEmpId",
                                    Value = AuthEmpId,
                                    DBType = DbType.Int32
                                });

                                strInsertColumn = strInsertColumn + " AuthorisedEmployeeID, ";
                                strInsertValues = strInsertValues + "@AuthEmpId , ";
                            }
                        }

                        PmMeter ObjectPMMeter = ListObjectPMMeter.FirstOrDefault(Item => Item.MeterID == ObjPMMeterGen.MeterID && (!string.IsNullOrEmpty(Item.ProjectNumber)));
                        if ((ObjectPMMeter != null))
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "ProjectNumber",
                                Value = ObjectPMMeter.ProjectNumber,
                                DBType = DbType.String
                            });

                            strInsertColumn = strInsertColumn + " ProjectNumber, ";
                            strInsertValues = strInsertValues + "@ProjectNumber, ";
                        }

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "CurrentDate",
                                Value = DateTime.Now,
                                DBType = DbType.DateTime
                            });
                        }
                        else
                        {
                            parameters1.Add(new DBParameters()
                            {
                                Name = "CurrentDate",
                                Value = Common.GetEnglishDate(DateTime.Now),
                                DBType = DbType.DateTime
                            });
                        }

                        parameters1.Add(new DBParameters()
                        {
                            Name = "ProblemDescription",
                            Value = ObjPMMeterGen.MeterNo + " - " + ObjPMMeterGen.MeterDescription + Environment.NewLine + ObjPMMeterGen.Reading + " " + ObjPMMeterGen.MeterUnit,
                            DBType = DbType.String
                        });

                        string query2 = "INSERT INTO workorders ( " + strInsertColumn + " WorkorderNo, DateReceived, ProblemDescription, " +
                                        " WOPMtype, WorkTypeID, WorkStatusID, WorkPriorityID, MeterID, PMJOGeneratedReading, EmployeeID, " + " L2ID, CreatedBy, CreatedDate) values " +
                                        " ( " + strInsertValues + "'" + WONo + "', @CurrentDate, " + " @ProblemDescription , " + " '2', 2, 1, 1, " + ObjPMMeterGen.MeterID + ", " + ObjPMMeterGen.Reading + ", " +
                                        ObjPMMeterGen.Userid + ", " + ObjPMMeterGen.L2ID + ", " + " " + ObjPMMeterGen.Userid + ", @CurrentDate)";
                        context.ExecuteQuery(query2, parameters1);

                        lstwoForNotication.Add(WONo);

                        PMCheckList objPMCheckList = new PMCheckList();
                        int? tempSafetyNo = (from c in context.SearchAll<PMCheckList>(objPMCheckList) select c).Where(x => x.ChecklistID == PMChecklistID).FirstOrDefault().SafetyID;

                        if ((tempSafetyNo != null) & tempSafetyNo > 0)
                        {
                            AddSafetyItemsToWo(WONo, Convert.ToInt32(tempSafetyNo));
                        }

                        GenChecklistElementAndItems(WONo, PMChecklistID);
                        AddAssignToToWo(WONo, Convert.ToInt32(ObjPMMeterGen.MeterID));
                        AddWOStatusAudit(WONo, Userid);
                        ObjPMMeterGen.WorkorderNo = WONo;

                        string query3 = "update intermediate_pmmetersgenerate set WorkorderNo = '" + ObjPMMeterGen.WorkorderNo + "' where " + " PMMetersGenerateID = " + ObjPMMeterGen.PMMetersGenerateID;
                        context.ExecuteQuery(query3, parameters);

                        workordernumber = workordernumber + 1;
                        WONo = "PMM" + workordernumber.ToString().PadLeft(11, '0');

                        string query4 = "update ReadingSchedule set GenStatus = 'Done', GenDate = @CurrentDate where " + " ReadingScheduleID = " + ObjPMMeterGen.ReadingScheduleID;
                        context.ExecuteQuery(query4, parameters);


                        string query5 = string.Empty;

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            query5 = "MERGE INTO pmmeters pm USING " +
                                             " (SELECT pm.ROWID row_id, " +
                                             "  PMMeterMaster.LastReading + pm.IncFreq AS IncPMDue " +
                                             " FROM pmmeters pm " +
                                             " JOIN PMMeterMaster " +
                                             " ON PMMeterMaster.MeterMasterID = pm.MeterMasterID " +
                                             " WHERE pm.MeterID               = " + ObjPMMeterGen.MeterID +
                                             " ) src ON ( pm.ROWID            = src.row_id ) " +
                                             " WHEN MATCHED THEN " +
                                             "  UPDATE SET pm.IncPMDue = src.IncPMDue ";
                        }
                        else
                        {
                            query5 = "update pm set pm.IncPMDue = PMMeterMaster.LastReading + pm.IncFreq " +
                                  " from pmmeters pm inner join PMMeterMaster on PMMeterMaster.MeterMasterID=pm.MeterMasterID where pm.MeterID = " + ObjPMMeterGen.MeterID;

                        }

                        context.ExecuteQuery(query5, parameters);


                    }
                    //Region WOPM Meter End

                    //Region Update Release Lock Start

                    string query6 = "Delete from PMLockMeters where L2ID = " + L2ID + " and MaintDivisionID = " + MaintDivisionID + " and " + " maintDeptID = " + maintDeptID + " and MaintSubDeptID = " + MaintSubDeptID + " and UserId = " + Userid;
                    context.ExecuteQuery(query6, parameters);
                    //Region UpdatePmSchedule End


                    foreach (string strWo in lstwoForNotication)
                    {
                        EmailConfigurationService objService = new EmailConfigurationService();
                        objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderCreation, strWo, string.Empty, "1");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddSafetyItemsToWo(string WONo, int SafetyID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });

                using (ServiceContext context = new ServiceContext())
                {
                    string query = "INSERT INTO SITask (SeqId, Details, WoNo) " + " select seqID, Description,@WONo from tblSafetyInstructionItems " + " where SafetyID = " + SafetyID;
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GenChecklistElementAndItems(string WONo, int ChecklistID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });

                using (ServiceContext context = new ServiceContext())
                {
                    string query1 = "INSERT INTO workorderelements (WorkorderNo, SeqNo, TaskDesc, AltTaskDesc) " + " select " +
                                    "@WONo, SeqNo, TaskDesc, AltTaskDesc from checklistelements " + " where ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query1, parameters);

                    string query2 = "INSERT INTO WorkOrderItems (StockID, WorkOrderNo, SeqNo, Qty) " + " select checklistinv.PartID, " +
                                   "@WONo, checklistelements.SeqNo, checklistinv.Qty from checklistinv " +
                                    " inner join checklistelements on checklistInv.ChecklistelementId = checklistelements.ChecklistelementId " +
                                    " where checklistinv.ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query2, parameters);

                    string query3 = "INSERT INTO WorkOrderTools ( WorkOrderNo, SeqNo, ToolDescription, AltToolDescription, Quantity ) " +
                                    " SELECT @WONo, SeqNo, ToolDescription, AltToolDescription, Quantity FROM ChecklistTools " + " WHERE ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query3, parameters);

                    string query4 = "INSERT INTO WorkOrderPPE ( WorkOrderNo, SeqNo, PPEDescription, AltPPEDescription ) " +
                                    " SELECT @WONo, SeqNo, PPEDescription, AltPPEDescription FROM ChecklistPPE " + " WHERE ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query4, parameters);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddAssignToToWo(string WONo, int PMID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });

                using (ServiceContext context = new ServiceContext())
                {
                    string query = "INSERT INTO assigntoworkorder (EmployeeID, WorkorderNo) " + " select EmployeeID,@WONo from pmschedule_assignto " + " where PMID = " + PMID;
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddWOStatusAudit(string WONo, int Userid)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "Userid",
                    Value = Userid,
                    DBType = DbType.Int32
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                using (ServiceContext context = new ServiceContext())
                {
                    string query = string.Empty;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = "INSERT INTO WOStatusAudit (WONo, WOStatus, Date_, EmpID, source) " + " values (@WONo ,1, @CurrentDate,@Userid, 'PM Meter JO Generation')";
                    }
                    else
                    {
                        query = "INSERT INTO WOStatusAudit (WONo, WOStatus, Date, EmpID, source) " + " values (@WONo ,1, @CurrentDate,@Userid, 'PM Meter JO Generation')";
                    }
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
