﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PMCheckListService : DBExecute
    {
        public PMCheckListService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<PMCheckList> GetPMChecklistPage(string strWhere, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PMCheckList> list = new List<PMCheckList>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "EmpID",
                Value = empId,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "ChecklistID";
            }

            string strWhereClauseWithEmployee = string.Empty;

            if (IsCentral)
            {
                empId = 0;
            }

            if (empId > 0)
            {
                strWhereClauseWithEmployee = "  and ( pmchecklist.L2ID in (select L2Id from employees_L2 where empID =  @EmpID union Select L2Id from employees where employeeId =  @EmpID ) OR pmchecklist.L2ID IS NULL) ";
            }

            string query = @" Select  pmchecklist.ChecklistID, pmchecklist.SafetyID,pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, pmchecklist.EstimatedLaborHours, 
				              pmchecklist.FileLink, pmchecklist.L2Id, pmchecklist.MaintdeptId, pmchecklist.MainSubDeptId, 
                              ts.SafetyNo,ts.SafetyName,ts.AltSafetyName,
                              pmchecklist.MaintdivId, pmchecklist.CreatedBy, pmchecklist.CreatedDate, pmchecklist.ModifiedBy, 
                              pmchecklist.ModifiedDate,L2.L2Code,MainenanceDivision.MaintDivisionCode,    MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode  ,
							  eaf.FileLInk,Eaf.FileDescription,eaf.FileName,eaf.autoid 
                              FROM  pmchecklist left outer join L2 on pmchecklist.L2ID = L2.L2ID 
						      LEFT JOIN ExtAssetFiles eaf ON eaf.FkId = pmchecklist.ChecklistID AND ModuleType='P'
                              LEFT JOIN tblSafetyinstruction ts ON ts.SafetyID = pmchecklist.SafetyID
                              LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = pmchecklist.MaintdivId  
                              LEFT JOIN MaintenanceDepartment  ON MaintenanceDepartment.maintDeptID =pmchecklist.MaintdeptId  
                              LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = pmchecklist.MainSubDeptId  
                              WHERE pmchecklist.IsCleaningModule=0  " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMCheckList>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public virtual IList<TEntity> GetPMChecklistPageAdvancedSearch<TEntity>(TEntity entity, int PageID, int EmployeeID, int pageNo, string sortExpression, string sortDirection, bool IsSaved = true, string extraWhereClause = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            string whereClause = string.Empty;
            string query = string.Empty;
            string strWhereClause = string.Empty;
            IList<TEntity> list;
            try
            {
                Collection<DBParameters> outParameters = new Collection<DBParameters>();
                SearchFieldService obj = new SearchFieldService();
                whereClause = obj.GetWhereClause(PageID, EmployeeID, IsSaved, out outParameters);

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

                Page page = new Page();

                using (DapperContext objDapperContext = new DapperContext())
                {
                    page = objDapperContext.SelectObject<Page>(PageID);
                }
                if (page != null)
                {
                    if (String.IsNullOrEmpty(whereClause))
                    {
                        whereClause = "1 = 1";
                        ProjectSession.IsAdvanceSearch = false;
                    }
                    else
                    {
                        ProjectSession.IsAdvanceSearch = true;
                    }

                    whereClause += extraWhereClause;

                    if (String.IsNullOrEmpty(strWhereClause))
                    {
                        query = "Select " + page.DisplayColumns + " " + page.TableJoins + " AND " + whereClause;
                    }
                    else
                    {
                        query = "Select * from (Select " + page.DisplayColumns + " " + page.TableJoins + " AND " + whereClause + ")A Where " + strWhereClause;
                    }
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<TEntity>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;

            }

            finally
            {

            }
        }

        #region "CheckList Elements"
        /// <summary>
        /// Inserts the update check list elements.
        /// </summary>
        /// <param name="objCheckListElements">The object table check list elements.</param>
        /// <returns></returns>
        public bool InsertUpdateCheckListElement(Checklistelement objCheckListElements)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "ChecklistID",
                    Value = objCheckListElements.ChecklistID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqNo",
                    Value = objCheckListElements.SeqNo,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "TaskDesc",
                    Value = objCheckListElements.TaskDesc,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "AltTaskDesc",
                    Value = objCheckListElements.AltTaskDesc,
                    DBType = DbType.String
                });

                if (objCheckListElements.HdnSeqID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objCheckListElements.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objCheckListElements.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"UPDATE checklistelements SET TaskDesc = @TaskDesc, AltTaskDesc = @AltTaskDesc , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  where ChecklistID = @ChecklistID and SeqNo = " + objCheckListElements.HdnSeqID;
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = objCheckListElements.CreatedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = objCheckListElements.CreatedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"INSERT INTO checklistelements(ChecklistID,SeqNo,TaskDesc,AltTaskDesc,CreatedBy,CreatedDate) VALUES(@ChecklistID,@SeqNo,@TaskDesc,@AltTaskDesc,@CreatedBy,@CreatedDate)";
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        public bool SaveCheckListElement(List<Checklistelement> lstChecklistelement)
        {
            try
            {
                List<Checklistelement> lstInsert = lstChecklistelement.Where(x => x.ChecklistelementId == 0).ToList();
                List<Checklistelement> lstUpdate = lstChecklistelement.Where(x => x.ChecklistelementId > 0).ToList();
                string queryInsert = string.Empty;
                string queryUpdate = string.Empty;

                using (ServiceContext objContext = new ServiceContext())
                {
                    lstInsert.ForEach(p =>
                   {
                       Collection<DBParameters> parameters = new Collection<DBParameters>();

                       parameters.Add(new DBParameters()
                       {
                           Name = "ChecklistID",
                           Value = p.ChecklistID,
                           DBType = DbType.Int32
                       });
                       parameters.Add(new DBParameters()
                       {
                           Name = "SeqNo",
                           Value = p.SeqNo,
                           DBType = DbType.Decimal
                       });
                       parameters.Add(new DBParameters()
                       {
                           Name = "TaskDesc",
                           Value = p.TaskDesc,
                           DBType = DbType.String
                       });
                       parameters.Add(new DBParameters()
                       {
                           Name = "AltTaskDesc",
                           Value = p.AltTaskDesc,
                           DBType = DbType.String
                       });
                       parameters.Add(new DBParameters()
                       {
                           Name = "CreatedBy",
                           Value = ProjectSession.EmployeeID,
                           DBType = DbType.String
                       });

                       parameters.Add(new DBParameters()
                       {
                           Name = "CreatedDate",
                           Value = DateTime.Now,
                           DBType = DbType.DateTime
                       });

                       queryInsert = @"INSERT INTO checklistelements(ChecklistID,SeqNo,TaskDesc,AltTaskDesc,CreatedBy,CreatedDate) VALUES(@ChecklistID,@SeqNo,@TaskDesc,@AltTaskDesc,@CreatedBy,@CreatedDate)";
                       objContext.ExecuteQuery(queryInsert, parameters);
                   });

                    lstUpdate.ForEach(p =>
                    {
                        Collection<DBParameters> parameters = new Collection<DBParameters>();

                        parameters.Add(new DBParameters()
                        {
                            Name = "ChecklistelementId",
                            Value = p.ChecklistelementId,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "SeqNo",
                            Value = p.SeqNo,
                            DBType = DbType.Decimal
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "TaskDesc",
                            Value = p.TaskDesc,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "AltTaskDesc",
                            Value = p.AltTaskDesc,
                            DBType = DbType.String
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedBy",
                            Value = ProjectSession.EmployeeID,
                            DBType = DbType.String
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = DateTime.Now,
                            DBType = DbType.DateTime
                        });

                        queryUpdate = @"UPDATE checklistelements SET SeqNo = @SeqNo ,TaskDesc = @TaskDesc, AltTaskDesc = @AltTaskDesc , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  where ChecklistelementId = @ChecklistelementId";

                        objContext.ExecuteQuery(queryUpdate, parameters);
                    });
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }

        /// <summary>
        /// Deletes the check list elements.
        /// </summary>
        /// <param name="checklistID">The check list identifier.</param>
        /// <param name="seqNo">The seq identifier.</param>
        /// <returns></returns>
        public bool DeleteSafetyInstructionItem(int checklistID, int seqNo)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "ChecklistID",
                    Value = checklistID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqNo",
                    Value = seqNo,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From checklistelements where ChecklistID = @ChecklistID and SeqNo = @SeqNo";

                objDapperContext.ExecuteQuery(strQuery, parameters);
                UpdateSeqID(checklistID);
            }
            return true;
        }

        /// <summary>
        /// Updates the seq identifier.
        /// </summary>
        /// <param name="checklistID">The check list identifier.</param>
        /// <returns></returns>
        public bool UpdateSeqID(int checklistID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    strQuery = @";WITH cte AS(SELECT *, ROW_NUMBER() OVER(ORDER BY SeqNo) Rno FROM dbo.checklistelements WHERE ChecklistID = " + checklistID + ")" +
                                "UPDATE cte SET SeqNo = cte.Rno";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    strQuery = @"UPDATE checklistelements as c inner JOIN(SELECT *,  @rownum := @rownum + 1 AS rank FROM checklistelements,(SELECT @rownum := 0) r WHERE ChecklistID = " + checklistID + ") as cte " +
                               "on cte.ChecklistID = c.ChecklistID and cte.SeqNo = c.SeqNo SET c.SeqNo = cte.rank";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = @"UPDATE checklistelements c SET(c.SeqNo) = (SELECT ROW_NUMBER() OVER ( ORDER BY SeqNo) rno FROM   checklistelements cte WHERE  cte.ChecklistID = c.ChecklistID and cte.SeqNo = c.SeqNo) " +
                                "Where c.ChecklistID = " + checklistID;
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Get CheckList Items From CheckListID
        /// </summary>
        /// <param name="checklistID"></param>
        /// <param name="pageNo"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<ChecklistInv> GetCheckListInvByCheckListID(int checklistID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<ChecklistInv> list = new List<ChecklistInv>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "ChecklistID",
                Value = checklistID,
                DBType = DbType.Int32
            });

            string query = @"SELECT ChecklistInvId,StockNo AS PartsListNo,StockDescription AS PartsListDescription,StockID,StockID As PartID,
                             AltStockDescription AS PartsListAltDescription,Qty,cle.SeqNo FROM checklistInv CI
                             INNER JOIN stockcode SC ON SC.StockID=CI.PartID
                             LEFT JOIN checklistelements cle on cle.ChecklistelementId = ci.ChecklistelementId
                             WHERE CI.ChecklistID = @ChecklistID";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query = " Select * from (" + query + ") A Where " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ChecklistInv>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        /// <summary>
        /// Get CheckList Tools From CheckListID
        /// </summary>
        /// <param name="checklistID"></param>
        /// <param name="pageNo"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<ChecklistTool> GetCheckListToolsByCheckListID(int checklistID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<ChecklistTool> list = new List<ChecklistTool>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "ChecklistID",
                Value = checklistID,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "SeqNo";
            }

            string query = @"Select ChecklistToolID,ChecklistID,SeqNo,ToolDescription,AltToolDescription,Quantity FROM ChecklistTools 
                             WHERE ChecklistID = @ChecklistID";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ChecklistTool>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }
            return list;
        }

        /// <summary>
        /// Get CheckList PPE From CheckListID
        /// </summary>
        /// <param name="checklistID"></param>
        /// <param name="pageNo"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<ChecklistPPE> GetCheckListPPEByCheckListID(int checklistID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<ChecklistPPE> list = new List<ChecklistPPE>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "ChecklistID",
                Value = checklistID,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "SeqNo";
            }

            string query = @"Select ChecklistPPEID,ChecklistID,SeqNo,PPEDescription,AltPPEDescription FROM checklistppe 
                             WHERE ChecklistID = @ChecklistID";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ChecklistPPE>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }
            return list;
        }

        /// <summary>
        /// Get CheckList Elements
        /// </summary>
        /// <param name="checklistID">The Check List Identifier</param>
        /// <returns></returns>
        public IList<Checklistelement> GetCheckListElementsByID(int checklistID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Checklistelement> list = new List<Checklistelement>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "ChecklistID",
                Value = checklistID,
                DBType = DbType.Int32
            });

            string query = @" SELECT ChecklistelementId,ChecklistID,SeqNo,SeqNo As SeqNoText,TaskDesc,AltTaskDesc FROM checklistelements 
                              WHERE ChecklistID = @ChecklistID ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Checklistelement>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        /// <summary>
        /// Get CheckList ElementID By SeqNo
        /// </summary>
        /// <returns></returns>
        public IList<Checklistelement> GetCheckListElementIDBySeqNo(int CheckListId, int seqNo)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                IList<Checklistelement> list = new List<Checklistelement>();
                string query = "SELECT * FROM Checklistelements Where ChecklistID = @CheckListId AND SeqNo = @seqNo";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "ChecklistID",
                    Value = CheckListId,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "SeqNo",
                    Value = seqNo,
                    DBType = DbType.Int32
                });

                list = objDapperContext.AdvanceSearch<Checklistelement>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;

                return list;

            }
        }

        /// <summary>
        /// Checck if item already exists for CheckListInv or not.
        /// </summary>
        /// <returns></returns>
        public IList<ChecklistInv> checkDuplicatedforListInv(int CheckListId, int CheckListElementID, int PartID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                IList<ChecklistInv> list = new List<ChecklistInv>();
                string query = @"Select checklistInvId From checklistInv
                                Where PartId = @PartID AND
                                CheckListElementID = @CheckListElementID AND
                                CheckListID= @CheckListID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "PartId",
                    Value = PartID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CheckListElementID",
                    Value = CheckListElementID,
                    DBType = DbType.Int32
                });


                parameters.Add(new DBParameters()
                {
                    Name = "CheckListID",
                    Value = CheckListId,
                    DBType = DbType.Int32
                });

                list = objDapperContext.AdvanceSearch<ChecklistInv>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;

                return list;

            }
        }

        /// <summary>
        /// Checck if item already exists for PMCheckList With Same CheckListNo
        /// </summary>
        /// <returns></returns>
        public IList<PMCheckList> checkDuplicatedforCheckListNo(int CheckListId, string CheckListNo)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                IList<PMCheckList> list = new List<PMCheckList>();
                string query = @"Select * FROM PmCheckList
                                Where CheckListNo = @CheckListNo And ChecklistID <> @CheckListID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "CheckListNo",
                    Value = CheckListNo,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CheckListID",
                    Value = CheckListId,
                    DBType = DbType.String
                });

                list = objDapperContext.AdvanceSearch<PMCheckList>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;

                return list;
            }
        }

        #endregion

        public static void UpdateSafetyInstruction(int checklistID, int? safetyID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "checklistID",
                Value = checklistID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "safetyID",
                Value = safetyID,
                DBType = DbType.Int32
            });

            string query = "Update pmchecklist set safetyID = @safetyID where checklistID = @checklistID";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
            }
        }

        #region "PPE"
        public static int CheckPPEDuplicate(ChecklistPPE obj)
        {
            int count = 0;

            if (obj.ChecklistPPEID > 0)
            {
                string query = "Select count(1) from ChecklistPPE where checklistID = " + obj.ChecklistID + " and SeqNo = " + obj.SeqNo + " and PPEDescription = '" + Common.setQuote(obj.PPEDescription.Trim()) + "' and checklistPPEID <> " + obj.ChecklistPPEID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
            else
            {
                string query = "Select count(1) from ChecklistPPE where checklistID = " + obj.ChecklistID + " and SeqNo = " + obj.SeqNo + " and PPEDescription = '" + Common.setQuote(obj.PPEDescription.Trim()) + "'";

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
        }
        #endregion
    }
}
