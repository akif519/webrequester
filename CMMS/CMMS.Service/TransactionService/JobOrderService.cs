﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class JobOrderService : DBExecute
    {
        public JobOrderService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Job Order And Asset"
        string AssetListID = string.Empty;
        public string GetAssetChilds(int assetID)
        {
            AssetListID = Convert.ToString(assetID);
            RecursiveChildAssetForAsset(assetID);
            return AssetListID;
        }

        public void RecursiveChildAssetForAsset(int assetID)
        {
            using (ServiceContext context = new ServiceContext())
            {

                AssetRelationship obj = new AssetRelationship();
                List<AssetRelationship> lst = context.SearchAll<AssetRelationship>(obj).Where(x => x.AssetID == assetID).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        AssetListID = AssetListID + "," + lst[i].ChildId;
                        RecursiveChildAssetForAsset(lst[i].ChildId);
                    }
                }
            }
        }

        public IList<WorkOrder> GetJobOrderByAssetID(string assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<WorkOrder> list = new List<WorkOrder>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "select" +
                            " wo.WorkorderNo,wo.ProblemDescription,wo.ActionTaken,wo.DateReceived,wo.ActDateStart,wo.ActDateEnd, " +
                            " ws.WorkStatus, " +
                            " a.AssetNumber,a.AssetAltDescription,a.AssetDescription," +
                            " wt.WorkTypeDescription,wt.AltWorkTypeDescription,  " +
                            " wp.WorkPriority, " +
                            " wtr.WorkTrade,wtr.AltWorkTrade, " +
                            " MaintDivisionCode, " +
                            " MaintDeptCode, " +
                            " MaintSubDeptCode " +
                            "from workorders wo " +
                            "inner join workstatus ws on wo.WorkStatusID =ws.WorkStatusID " +
                            "inner join Assets a on a.AssetID = wo.AssetID " +
                            "inner join worktype wt on wt.WorkTypeID = wo.WorkTypeID " +
                            "inner join workpriority wp on wp.WorkPriorityID = wo.WorkPriorityID  " +
                            "inner join MainenanceDivision md on md.MaintDivisionID= wo.MaintDivisionID  " +
                            "inner join MaintenanceDepartment mdp on mdp.maintDeptID = wo.maintdeptID  " +
                            "inner join MaintSubDept msd on msd.MaintSubDeptID = wo.maintsubdeptID  " +
                            "left join worktrade wtr on wtr.WorkTradeID = wo.WOTradeID " +
                            "where wo.AssetID in (" + assetID + ")";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkOrder>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<WorkOrder> GetJobOrderByLocationID(int LocationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<WorkOrder> list = new List<WorkOrder>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "select" +
                            " wo.WorkorderNo,wo.ProblemDescription,wo.ActionTaken,wo.DateReceived,wo.ActDateStart,wo.ActDateEnd, " +
                            " ws.WorkStatus, " +
                            " a.AssetNumber,a.AssetAltDescription,a.AssetDescription," +
                            " wt.WorkTypeDescription,wt.AltWorkTypeDescription,  " +
                            " wp.WorkPriority, " +
                            " wtr.WorkTrade,wtr.AltWorkTrade, " +
                            " MaintDivisionCode, " +
                            " MaintDeptCode, " +
                            " MaintSubDeptCode " +
                            "from workorders wo " +
                            "inner join workstatus ws on wo.WorkStatusID =ws.WorkStatusID " +
                            "inner join Assets a on a.AssetID = wo.AssetID " +
                            "inner join worktype wt on wt.WorkTypeID = wo.WorkTypeID " +
                            "inner join workpriority wp on wp.WorkPriorityID = wo.WorkPriorityID  " +
                            "inner join MainenanceDivision md on md.MaintDivisionID= wo.MaintDivisionID  " +
                            "inner join MaintenanceDepartment mdp on mdp.maintDeptID = wo.maintdeptID  " +
                            "inner join MaintSubDept msd on msd.MaintSubDeptID = wo.maintsubdeptID  " +
                            "left join worktrade wtr on wtr.WorkTradeID = wo.WOTradeID " +
                            "where wo.LocationID = " + LocationID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkOrder>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<WorkOrder> GetJobOrderDetailByAssetID(string assetID)
        {
            IList<WorkOrder> list = new List<WorkOrder>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            //parameters.Add(new DBParameters()
            //{
            //    Name = "assetID",
            //    Value = assetID,
            //    DBType = DbType.String
            //});

            string query = "select workorders.WorkorderNo, isnull((select SUM(workorderlabor.TotHour) from workorderlabor where workorders.WorkorderNo = workorderlabor.WorkorderNo),0) as ManHours, " +
                                                                      "isnull((select SUM(workorderlabor.TotCost) from workorderlabor where workorders.WorkorderNo = workorderlabor.WorkorderNo),0) as LabourCost, " +
                                                                      "isnull((select SUM(direct.Qty * direct.Price) from direct where direct.WONo = workorders.WorkorderNo),0) As DICost,  " +
                                                                      "(isnull((select SUM(issue.QtyIssue * issue.Price3) from issue where issue.WONo = workorders.WorkorderNo),0) " +
                                                                       "- isnull((select SUM([return].Qty * [return].RetPrice) from [return] where [return].WONo = workorders.WorkorderNo ),0)) as ItemCost " +
                                                                        "from workorders  " +
                                                                      " where workorders.AssetID in (" + assetID + ")";

            using (DapperDBContext context = new DapperDBContext())
            {
                list = context.ExecuteQuery<WorkOrder>(query, parameters);
            }

            return list;
        }

        public int GetJOCountByAssetIDLocationID(int AssetID, int LocationID, string strWhereClauseWithEmployee)
        {
            try
            {
                string query = "Select count(*) from workorders where 1=1 " + strWhereClauseWithEmployee;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                int Cnt = 0;

                using (ServiceContext context = new ServiceContext())
                {
                    Cnt = Convert.ToInt32(context.ExecuteScalar(query));
                }

                return Cnt;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<WorkOrder> GetAllJOByAssetID(int AssetID, int empID, bool isCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<WorkOrder> list = new List<WorkOrder>();
                Collection<DBParameters> outParameters = new Collection<DBParameters>();

                string strWhereClauseWithEmployee = "";

                if (AssetID > 0)
                {
                    strWhereClauseWithEmployee = " and workorders.AssetID = " + AssetID;
                }

                if (!isCentral)
                {
                    strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " ) ";
                }
                if (empID > 0)
                {
                    strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empID + " union Select MaintSubDeptID from employees where employeeId = " + empID + " ) ";
                }

                string query = "Select WorkorderNo, workorders.WorkStatusID,w.WorkStatus,workorders.WorkTypeID,w2.WorkTypeDescription,w2.AltWorkTypeDescription,ProblemDescription,DateReceived " +
                    "from workorders INNER JOIN workstatus w ON workorders.WorkStatusID = w.WorkStatusID " +
                     " INNER JOIN worktype w2 ON workorders.WorkTypeID = w2.WorkTypeID  " + strWhereClauseWithEmployee;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrder>(query, outParameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<WorkOrder> GetJOByAssetIDLocationID(int AssetID, int LocationID, int empID, bool isCentral, int pageNo, string sortExpression, string sortDirection)
        {
            IList<WorkOrder> list = new List<WorkOrder>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            try
            {
                string strWhereClauseWithEmployee = "";
                if (AssetID > 0)
                {
                    strWhereClauseWithEmployee = " and workorders.AssetID = " + AssetID + " and WorkStatusID <> 2 and WorkStatusID <> 3 ";
                }
                else if (LocationID > 0)
                {
                    strWhereClauseWithEmployee = " and workorders.LocationID = " + LocationID + " and WorkStatusID <> 2 and WorkStatusID <> 3 ";
                }

                if (!isCentral)
                {
                    strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " ) ";
                }
                if (empID > 0)
                {
                    strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empID + " union Select MaintSubDeptID from employees where employeeId = " + empID + " ) ";
                }


                string query = "Select WorkorderNo, workorders.WorkStatusID,w.WorkStatus,workorders.WorkTypeID,w2.WorkTypeDescription,w2.AltWorkTypeDescription,ProblemDescription,DateReceived " +
                         "from workorders INNER JOIN workstatus w ON workorders.WorkStatusID = w.WorkStatusID " +
                          " INNER JOIN worktype w2 ON workorders.WorkTypeID = w2.WorkTypeID  " + strWhereClauseWithEmployee;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrder>(query, outParameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion

        #region "Job Order And Job Request"
        public string NewJobOrderByWorkRequest(Worequest objWR)
        {
            try
            {
                string WorkOrderNo = NewJobOrderByWorkRequestDAL(objWR);
                if (Convert.ToInt64(WorkOrderNo) > 0)
                {
                    //WorkTypeID = 2 means Preventive
                    if (objWR.WorkTypeID == 2)
                    {
                        NewJOStatusAudit(WorkOrderNo, Convert.ToInt32(objWR.RequestStatusID), Convert.ToInt32(objWR.CreatedBy), "PM Module");
                    }
                    else
                    {
                        NewJOStatusAudit(WorkOrderNo, Convert.ToInt32(objWR.RequestStatusID), Convert.ToInt32(objWR.CreatedBy), "Job Order Module");
                    }
                }
                return Convert.ToString(WorkOrderNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string NewJobOrderByWorkRequestDAL(Worequest newWR)
        {
            try
            {
                string strInsertColumn = "";
                string strInsertValues = "";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if ((newWR.L2ID != null))
                {
                    strInsertColumn = strInsertColumn + " L2Id, ";
                    strInsertValues = strInsertValues + "@L2ID, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = newWR.L2ID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.MaintdivId != null))
                {
                    strInsertColumn = strInsertColumn + " MaintDivisionID, ";
                    strInsertValues = strInsertValues + "@MaintdivId, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdivId",
                        Value = newWR.MaintdivId,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.MaintdeptId != null))
                {
                    strInsertColumn = strInsertColumn + " maintdeptID, ";
                    strInsertValues = strInsertValues + "@MaintdeptId, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdeptId",
                        Value = newWR.MaintdeptId,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.MainSubDeptId != null))
                {
                    strInsertColumn = strInsertColumn + " maintsubdeptID, ";
                    strInsertValues = strInsertValues + "@MainSubDeptId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MainSubDeptId",
                        Value = newWR.MainSubDeptId,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.Workpriorityid != null))
                {
                    strInsertColumn = strInsertColumn + " WorkPriorityID, ";
                    strInsertValues = strInsertValues + "@Workpriorityid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Workpriorityid",
                        Value = newWR.Workpriorityid,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.Worktradeid != null))
                {
                    strInsertColumn = strInsertColumn + " WOTradeID, ";
                    strInsertValues = strInsertValues + "@Worktradeid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Worktradeid",
                        Value = newWR.Worktradeid,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.WorkTypeID != null))
                {
                    strInsertColumn = strInsertColumn + " WorkTypeID, ";
                    strInsertValues = strInsertValues + "@WorkTypeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkTypeID",
                        Value = newWR.WorkTypeID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.L3ID != null))
                {
                    strInsertColumn = strInsertColumn + " L3ID, ";
                    strInsertValues = strInsertValues + "@L3ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L3ID",
                        Value = newWR.L3ID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.L4ID != null))
                {
                    strInsertColumn = strInsertColumn + " L4ID, ";
                    strInsertValues = strInsertValues + "@L4ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L4ID",
                        Value = newWR.L4ID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.L5ID != null))
                {
                    strInsertColumn = strInsertColumn + " L5ID, ";
                    strInsertValues = strInsertValues + "@L5ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L5ID",
                        Value = newWR.L5ID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.LocationID != null))
                {
                    strInsertColumn = strInsertColumn + " LocationID, ";
                    strInsertValues = strInsertValues + "@LocationID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LocationID",
                        Value = newWR.LocationID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.AssetID != null))
                {
                    strInsertColumn = strInsertColumn + " AssetID, ";
                    strInsertValues = strInsertValues + "@AssetID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AssetID",
                        Value = newWR.AssetID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.RequestStatusID != null))
                {
                    strInsertColumn = strInsertColumn + " WorkStatusID, ";
                    strInsertValues = strInsertValues + 1 + ", ";
                }
                if ((newWR.RequesterID != null))
                {
                    strInsertColumn = strInsertColumn + " RequestorID, ";
                    strInsertValues = strInsertValues + "@RequesterID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RequesterID",
                        Value = newWR.RequesterID,
                        DBType = DbType.Int32
                    });
                }
                if ((newWR.ProblemDesc != null))
                {
                    strInsertColumn = strInsertColumn + " ProblemDescription, ";
                    strInsertValues = strInsertValues + "@ProblemDesc, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProblemDesc",
                        Value = newWR.ProblemDesc,
                        DBType = DbType.String
                    });
                }
                if ((newWR.ReceivedDate != null))
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ReceivedDate",
                            Value = newWR.ReceivedDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ReceivedDate",
                            Value = Common.GetEnglishDate(newWR.ReceivedDate.Value),
                            DBType = DbType.DateTime
                        });
                    }
                    strInsertColumn = strInsertColumn + " DateReceived, ";
                    strInsertValues = strInsertValues + "@ReceivedDate, ";

                }
                if ((newWR.RequiredDate != null))
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "RequiredDate",
                            Value = newWR.RequiredDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "RequiredDate",
                            Value = Common.GetEnglishDate(newWR.RequiredDate.Value),
                            DBType = DbType.DateTime
                        });
                    }
                    strInsertColumn = strInsertColumn + " DateRequired, ";
                    strInsertValues = strInsertValues + "@RequiredDate, ";

                }
                if ((newWR.Remarks != null))
                {
                    strInsertColumn = strInsertColumn + " Notes, ";
                    strInsertValues = strInsertValues + "@Remarks, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Remarks",
                        Value = newWR.Remarks,
                        DBType = DbType.String
                    });
                }
                if ((newWR.ProjectNumber != null))
                {
                    strInsertColumn = strInsertColumn + " ProjectNumber, ";
                    strInsertValues = strInsertValues + "@ProjectNumber, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProjectNumber",
                        Value = newWR.ProjectNumber,
                        DBType = DbType.String
                    });
                }
                if ((newWR.AuthorisedEmployeeId != null))
                {
                    strInsertColumn = strInsertColumn + " AuthorisedEmployeeID, ";
                    strInsertValues = strInsertValues + "@AuthorisedEmployeeId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AuthorisedEmployeeId",
                        Value = newWR.AuthorisedEmployeeId,
                        DBType = DbType.Int32
                    });
                }

                // Created By
                if ((newWR.CreatedBy != null))
                {
                    strInsertColumn = strInsertColumn + " CreatedBy, ";
                    strInsertValues = strInsertValues + "@CreatedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = newWR.CreatedBy,
                        DBType = DbType.String
                    });
                }

                // Created Date
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                strInsertColumn = strInsertColumn + " CreatedDate, ";
                strInsertValues = strInsertValues + "@CreatedDate, ";

                Int64 CurrentMinNo = Convert.ToInt64(Convert.ToString(newWR.L2ID).PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));
                Int64 WorkOrderno = 0;

                IList<WorkOrder> TempListJO = new List<WorkOrder>();
                string query = string.Empty;
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(Convert.ToString(newWR.L2ID)))
                {
                    query = "SELECT Max(w.WorkorderNo) FROM workorders w WHERE lower(w.WorkorderNo) NOT LIKE lower('%PC%') AND lower(w.WorkorderNo) NOT LIKE lower('%PM%') AND lower(w.WorkorderNo) NOT LIKE lower('%PCM%') AND L2ID =" + newWR.L2ID;
                }
                else
                {
                    query = "SELECT Max(w.WorkorderNo) FROM workorders w WHERE w.WorkorderNo NOT LIKE '%PC%' AND w.WorkorderNo NOT LIKE '%PM%' AND w.WorkorderNo NOT LIKE '%PCM%' AND L2ID =" + newWR.L2ID;
                }
                using (ServiceContext objContext = new ServiceContext())
                {
                    string wo = objContext.ExecuteScalar(query);
                    if (!string.IsNullOrEmpty(wo))
                    { WorkOrderno = Convert.ToInt64(wo); }
                }

                if (WorkOrderno > CurrentMinNo)
                {
                    CurrentMinNo = WorkOrderno;
                }

                newWR.WorkorderNo = Convert.ToString((CurrentMinNo + 1)).PadLeft(11, '0');
                if ((newWR.WorkorderNo != null))
                {
                    strInsertColumn = strInsertColumn + " WorkorderNo ";
                    strInsertValues = strInsertValues + "@WorkorderNo";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkorderNo",
                        Value = newWR.WorkorderNo,
                        DBType = DbType.String
                    });
                }

                string strQuery = "INSERT INTO workorders(" + strInsertColumn + ")  " + " VALUES(" + strInsertValues + ")";

                using (ServiceContext context = new ServiceContext())
                {
                    context.ExecuteQuery<Worequest>(strQuery, parameters);
                }

                EmailConfigurationService objService = new EmailConfigurationService();
                objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderCreation, newWR.WorkorderNo, "", "1");

                //// Copy the attachments

                string currPhysPathJobOrder = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathJobOrder + newWR.WorkorderNo;
                string currPhysPathJobRequest = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathjobRequest + newWR.RequestNo;
                DirectoryInfo diJobOrder = new DirectoryInfo(currPhysPathJobOrder);
                DirectoryInfo diJobRequest = new DirectoryInfo(currPhysPathJobRequest);
                string updateAttachmentQuery = null;
                if (diJobRequest.Exists)
                {
                    // Copy the attachments in table
                    updateAttachmentQuery = "INSERT INTO extassetfiles(fkid, FileLink, FileDescription, [FileName], ModuleType, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate)  " + " SELECT '" + newWR.WorkorderNo + "', replace(replace(FileLink, 'JobRequest', 'JobOrder'), '" + newWR.RequestNo.ToString() + "' , '" + newWR.WorkorderNo.ToString() + "' ), FileDescription, [FileName], 'J', " + newWR.CreatedBy + ",GETDATE(), " + newWR.CreatedBy + ", GETDATE() FROM extassetfiles WHERE FkId = '" + newWR.RequestNo + "'";

                    parameters = new Collection<DBParameters>();
                    using (ServiceContext context = new ServiceContext())
                    {
                        context.ExecuteQuery(updateAttachmentQuery, parameters);
                    }

                    if (!diJobOrder.Exists)
                    {
                        // Create the Job Order folder
                        diJobOrder.Create();

                        // Copy the attachments
                        foreach (string fileAttach in Directory.GetFiles(currPhysPathJobRequest))
                        {
                            File.Copy(fileAttach, Path.Combine(currPhysPathJobOrder, Path.GetFileName(fileAttach)), true);
                        }
                    }
                }

                return newWR.WorkorderNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Job Order"
        public WorkOrder GetJobOrderByOrderNo(string workOrderNo)
        {
            try
            {
                WorkOrder objWO = new WorkOrder();
                IList<WorkOrder> lstWoRequest = new List<WorkOrder>();
                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkOrderNo",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                    //string query = "SELECT *,e.Name,w.WorkStatus as WorkStatusName,w.AltWorkStatus,w3.WorkStatus FROM worequest,employees e,workstatus w ,workorders w2,workstatus     w3" +
                    //             " where RequestNo=@RequestNo AND worequest.CreatedByID=e.EmployeeID  AND worequest.RequestStatusID = w.WorkStatusID " +
                    //             "  ";
                    string query = "Select  WorkOrders.*,w2.RequestNo,e.Name As CreatedByName,e2.Name,e2.AltName,e2.EmployeeNO,e2.Email,e2.WorkPhone,e2.Fax,e2.HandPhone,";
                    query += " e3.EmployeeNO As AcceptedByEmpNo,e3.Name As AcceptedByEmpName,e3.AltName As AcceptedByEmpAltName,";
                    query += " w.WorkStatus,w.AltWorkStatus, ";
                    query += "(SELECT NAME FROM employees e2 WHERE e2.EmployeeID = WorkOrders.AuthorisedEmployeeId) AS AuthEmployeeName, ";
                    query += "(SELECT NAME FROM employees e2 WHERE e2.EmployeeID = (SELECT Location.EmployeeID FROM Location  Where Location.LocationID = WorkOrders.LocationID)) AS LocationAuthEmployeeName, ";
                    query += "(SELECT NAME FROM employees  WHERE  employees.EmployeeID = WorkOrders.WOclosebyID) AS WOclosebyName, ";
                    query += "(SELECT l.NotetoTech FROM location l WHERE l.LocationID = WorkOrders.LocationID) AS NoteToTechLocation, ";
                    query += "(SELECT l.LocationNo FROM location l WHERE l.LocationID = WorkOrders.LocationID) AS LocationNo,";
                    query += "(SELECT l.LocationDescription FROM location l WHERE l.LocationID = WorkOrders.LocationID) AS LocationDescription,";
                    query += "(SELECT l.LocationAltDescription FROM location l WHERE l.LocationID = WorkOrders.LocationID) AS LocationAltDescription,";
                    query += "(SELECT l.L5No FROM L5 l WHERE l.L5ID = WorkOrders.L5ID) AS L5No,";
                    query += "(SELECT l.L5Description FROM L5 l WHERE l.L5ID = WorkOrders.L5ID) AS L5Description,";
                    query += "(SELECT l.L5AltDescription FROM L5 l WHERE l.L5ID = WorkOrders.L5ID) AS L5AltDescription, ";
                    query += "(SELECT s.SectorCode FROM sector s WHERE s.SectorID = (SELECT L2.SectorID FROM L2  Where L2.L2ID = WorkOrders.L2ID)) AS SectorCode,";
                    query += "(SELECT s.SectorName FROM sector s WHERE s.SectorID =( SELECT L2.SectorID FROM L2  Where L2.L2ID = WorkOrders.L2ID)) AS SectorName,";
                    query += "(SELECT s.SectorAltname FROM sector s WHERE s.SectorID =( SELECT L2.SectorID FROM L2  Where L2.L2ID = WorkOrders.L2ID)) AS SectorAltname,";
                    query += " pmcl.CheckListNo,pmcl.CheckListName,AltCheckListName ";
                    query += " FROM WorkOrders INNER JOIN ";
                    query += " employees e ON WorkOrders.CreatedBy = e.EmployeeID AND WorkOrders.WorkOrderno = @WorkOrderNo  INNER JOIN ";
                    query += " workstatus w ON  WorkOrders.WorkStatusID = w.WorkStatusID LEFT OUTER JOIN ";
                    query += " worequest w2 ON  workorders.WorkorderNo = w2.WorkorderNo ";
                    query += " LEFT OUTER JOIN pmchecklist pmcl On workorders.PmchecklistID = pmcl.CheckListID ";
                    query += " Left OUTER JOIN employees e2 ON workorders.RequestorId = e2.EmployeeID ";
                    query += " Left OUTER JOIN employees e3 ON workorders.AcceptedById = e3.EmployeeID ";

                    using (DapperDBContext context = new DapperDBContext())
                    {
                        lstWoRequest = context.ExecuteQuery<WorkOrder>(query, parameters);
                    }

                    if (lstWoRequest != null && lstWoRequest.Count > 0)
                    {
                        objWO = lstWoRequest.FirstOrDefault();
                    }

                    if (objWO == null || string.IsNullOrEmpty(objWO.WorkorderNo))
                    {
                        objWO = new WorkOrder();

                        //For New Request
                        employees objEmp = new employees();
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objEmp = objContext.SearchAllByID<employees>(objEmp, ProjectSession.EmployeeID).FirstOrDefault();
                        }

                        if (string.IsNullOrEmpty(objWO.SectorCode))
                        {
                            Sector objSector = new Sector();
                            L2 objL2 = new L2();
                            objL2.L2ID = Convert.ToInt32(objEmp.L2ID);
                            objWO.L2ID = objL2.L2ID;
                            objWO.MaintDivisionID = objEmp.MaintDivisionID;
                            objWO.MaintdeptID = objEmp.MaintDeptID;
                            objWO.MaintsubdeptID = objEmp.MaintSubDeptID;
                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objSector.SectorID = Convert.ToInt32(objContext.Search<L2>(objL2).FirstOrDefault().SectorID);
                                objSector = objContext.SearchAllByID<Sector>(objSector, objSector.SectorID).FirstOrDefault();
                            }
                            objWO.SectorCode = objSector.SectorCode;
                            objWO.SectorName = objSector.SectorName;
                            objWO.SectorAltName = objSector.SectorAltname;
                        }

                        Workpriority objWorkPriority = new Workpriority();
                        Worktype objWorktype = new Worktype();
                        Worktrade objWorktrade = new Worktrade();

                        objWorkPriority.IsDefault = true;
                        objWorktype.IsDefault = true;
                        objWorktrade.IsDefault = true;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objWorkPriority = objContext.Search<Workpriority>(objWorkPriority).FirstOrDefault();
                            objWorktype = objContext.Search<Worktype>(objWorktype).FirstOrDefault();
                            objWorktrade = objContext.Search<Worktrade>(objWorktrade).FirstOrDefault();
                        }

                        if (objWorkPriority != null)
                        {
                            objWO.WorkPriorityID = objWorkPriority.WorkPriorityID;
                        }
                        if (objWorktype != null)
                        {
                            objWO.WorkTypeID = objWorktype.WorkTypeID;
                        }
                        if (objWorktrade != null)
                        {
                            objWO.WOTradeID = objWorktrade.WorkTradeID;
                        }

                        Workstatu objWS = new Workstatu();
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objWS = objContext.SelectObject<CMMS.Model.Workstatu>(1);
                        }
                        objWO.WorkStatus = objWS.WorkStatus;
                        objWO.AltWorkStatus = objWS.AltWorkStatus;
                        objWO.WorkStatusID = objWS.WorkStatusID;


                        objWO.DateReceived = DateTime.Now;
                        objWO.DateRequired = DateTime.Now;
                        objWO.TimeReceived = objWO.DateReceived;
                        objWO.TimeRequired = objWO.DateRequired;
                        objWO.AstartTime = DateTime.Now;
                        objWO.AEndTime = DateTime.Now;
                        objWO.EstTimeStart = DateTime.Now;
                        objWO.EstTimeEnd = DateTime.Now;
                        objWO.ActTimeStart = DateTime.Now;
                        objWO.ActTimeEnd = DateTime.Now;
                    }
                    else
                    {
                        objWO.TimeReceived = objWO.DateReceived;
                        objWO.TimeRequired = objWO.DateRequired;
                        objWO.AstartTime = objWO.AstartDate;
                        objWO.AEndTime = objWO.AEndDate;
                        objWO.EstTimeStart = objWO.EstDateStart;
                        objWO.EstTimeEnd = objWO.EstDateEnd;
                        objWO.ActTimeStart = objWO.ActDateStart;
                        objWO.ActTimeEnd = objWO.ActDateEnd;
                        objWO.PMTarStartTime = objWO.PMTarStartDate;
                        objWO.PMTarCompTime = objWO.PMTarCompDate;
                        objWO.TimeHandover = objWO.DateHandover;
                    }


                    Asset objAsset = new Asset();

                    int id = objWO.AssetID.HasValue ? Convert.ToInt32(objWO.AssetID) : 0;
                    objAsset = AssetService.GetAssetDetailByAssetID(id);

                    objWO.Asset = objAsset;

                    if (objWO.CIGID > 0)
                    {
                        CleaningInspectionGroup objCIG = new CleaningInspectionGroup();
                        objCIG.CleaningInspectionGroupId = Convert.ToInt32(objWO.CIGID);
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objCIG = objContext.Search<CleaningInspectionGroup>(objCIG).FirstOrDefault();
                        }

                        objWO.cleaningInspectionGroup = objCIG;
                    }
                }
                return objWO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Workorderelement> GetJobOrderPMCheckListForDataSource(string employeeId, int NonCentral)
        {
            List<Workorderelement> objCheckList = new List<Workorderelement>();
            IList<Workorderelement> lstCheckList = new List<Workorderelement>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (NonCentral == 0)
            {
                if (!string.IsNullOrEmpty(employeeId))
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }
                query = "select workorderelements.SeqNo,workorderelements.TaskDesc,workorderelements.AltTaskDesc from workorderelements inner join workorders on workorderelements.WorkorderNo = workorders.WorkorderNo WHERE (workorders.WorkorderNo in (select intermediate_pmgenerate.WorkorderNo from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '')) Group By workorderelements.SeqNo,workorderelements.TaskDesc,workorderelements.AltTaskDesc order By workorderelements.SeqNo";
            }
            else
            {
                if (!string.IsNullOrEmpty(employeeId))
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }
                query = "select workorderelements.SeqNo,workorderelements.TaskDesc,workorderelements.AltTaskDesc from workorderelements inner join workorders on workorderelements.WorkorderNo = workorders.WorkorderNo WHERE (workorders.WorkorderNo in (select intermediate_pmgenerate.WorkorderNo from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '')) AND (workorders.L2ID in (select L2Id from employees_L2 where empID = @EmployeeID union Select L2Id from employees where employeeId = @EmployeeID)) Group By workorderelements.SeqNo,workorderelements.TaskDesc,workorderelements.AltTaskDesc order By workorderelements.SeqNo";
            }
            using (DapperDBContext context = new DapperDBContext())
            {
                lstCheckList = context.ExecuteQuery<Workorderelement>(query, parameters);
            }
            if (lstCheckList != null && lstCheckList.Count > 0)
            {
                objCheckList = lstCheckList.ToList();
            }
            return objCheckList;
        }

        public List<Jobtask> GetJobTaskForDataSource(string workOrderNo)
        {
            List<Jobtask> objJobTask = new List<Jobtask>();
            IList<Jobtask> listJobTask = new List<Jobtask>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhere = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                strWhere = " And WONO = @WONO";

                parameters.Add(new DBParameters()
                {
                    Name = "WONO",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            string queryJobTask = "Select * from jobtask where 1=1" + strWhere;
            using (DapperContext objDapperContext = new DapperContext())
            {
                listJobTask = objDapperContext.ExecuteQuery<Jobtask>(queryJobTask, parameters);
            }
            if (listJobTask != null && listJobTask.Count > 0)
            {
                objJobTask = listJobTask.ToList();
            }
            return objJobTask;
        }
        public List<SITask> PrintSafetyInsturctionDetails(string workOrderNo)
        {
            List<SITask> objSafetyInstruction = new List<SITask>();
            IList<SITask> lstSafetyInstruction = new List<SITask>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.Int32
                });
            }
            query = "select SeqId,Details,AltDetails from SITask where SITask.WoNo = @WorkorderNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstSafetyInstruction = context.ExecuteQuery<SITask>(query, parameters);
            }
            if (lstSafetyInstruction != null && lstSafetyInstruction.Count > 0)
            {
                objSafetyInstruction = lstSafetyInstruction.ToList();
            }
            return objSafetyInstruction;
        }
        public List<SITask> GetSafetyInsturctionDetailsForDataSource(string employeeId, int NonCentral)
        {
            List<SITask> objSafetyInstruction = new List<SITask>();
            IList<SITask> lstSafetyInstruction = new List<SITask>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (NonCentral == 0)
            {
                if (!string.IsNullOrEmpty(employeeId))
                {

                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }
                query = "select SeqId,Details,AltDetails from SITask where SITask.WoNo in (select intermediate_pmgenerate.WorkorderNo from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '') Group By SeqId,Details,AltDetails ";
            }
            else
            {
                if (!string.IsNullOrEmpty(employeeId))
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }
                query = "select SeqId,Details,AltDetails from SITask inner join workorders on SITask.WoNo = workorders.workorderNo where (SITask.WoNo in (select intermediate_pmgenerate.WorkorderNo from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '')) AND (workorders.L2ID in (select L2Id from employees_L2 where empID = @EmployeeID union Select L2Id from employees where employeeId = @EmployeeID)) Group By SeqId,Details,AltDetails";
            }
            using (DapperDBContext context = new DapperDBContext())
            {
                lstSafetyInstruction = context.ExecuteQuery<SITask>(query, parameters);
            }
            if (lstSafetyInstruction != null && lstSafetyInstruction.Count > 0)
            {
                objSafetyInstruction = lstSafetyInstruction.ToList();
            }
            return objSafetyInstruction;
        }

        public List<WorkOrder> PrintWorkOrderDetails(string workOrderNo)
        {
            List<WorkOrder> objWorkOrder = new List<WorkOrder>();
            IList<WorkOrder> lstWorkOrder = new List<WorkOrder>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                strWhereWorkOrderNo = " And wo.WorkorderNo = @WorkorderNo";

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "SELECT wt.WorkTrade,ws.WorkStatus,wp.WorkPriority,wtp.WorkTypeDescription FROM workorders wo INNER JOIN workstatus ws on ws.WorkStatusID = wo.WorkStatusID INNER JOIN workpriority wp on wp.WorkPriorityID = wo.WorkPriorityID INNER JOIN worktrade wt on wt.WorkTradeID = wo.WOTradeId INNER JOIN worktype wtp on wtp.WorkTypeId = wo.WorkTypeID where 1=1" + strWhereWorkOrderNo;
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWorkOrder = context.ExecuteQuery<WorkOrder>(query, parameters);
            }
            if (lstWorkOrder != null && lstWorkOrder.Count > 0)
            {
                objWorkOrder = lstWorkOrder.ToList();
            }
            return objWorkOrder;
        }

        public List<ItemAndLabourDetails> PrintItemAndLabourDetails(string workOrderNo)
        {
            List<ItemAndLabourDetails> objItemDetails = new List<ItemAndLabourDetails>();
            IList<ItemAndLabourDetails> lstItemDetails = new List<ItemAndLabourDetails>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                strWhereWorkOrderNo = " And wo.WorkorderNo = @WorkorderNo";

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "select  N'' as PartsNo,direct.StockDesc,direct.AltStockDesc,'D' As IssueType,'' As UOM,direct.Qty from direct where direct.WONo = @WorkorderNo UNION ALL select stockcode.StockNo As PartsNo,stockcode.StockDescription As StockDesc,stockcode.AltStockDescription As AltStockDesc,'I' As IssueType,issue.UoM,issue.QtyIssue As Qty  from issue inner join stockcode on issue.StockID = stockcode.StockID where issue.WONo = @WorkorderNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstItemDetails = context.ExecuteQuery<ItemAndLabourDetails>(query, parameters);
            }
            if (lstItemDetails != null && lstItemDetails.Count > 0)
            {
                objItemDetails = lstItemDetails.ToList();
            }
            return objItemDetails;
        }

        public List<Workorderlabor> PrintLabourDetails(string workOrderNo)
        {
            List<Workorderlabor> objLabourDetails = new List<Workorderlabor>();
            IList<Workorderlabor> lstLabourDetails = new List<Workorderlabor>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "select employees.Name,employees.EmployeeNO,workorderlabor.HourlySalary,workorderlabor.OverTime1, workorderlabor.OverTime2,workorderlabor.OverTime3 from workorderlabor inner join employees on workorderlabor.EmployeeID  = employees.EmployeeID where (workorderlabor.WorkorderNo = @WorkorderNo)";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstLabourDetails = context.ExecuteQuery<Workorderlabor>(query, parameters);
            }
            if (lstLabourDetails != null && lstLabourDetails.Count > 0)
            {
                objLabourDetails = lstLabourDetails.ToList();
            }
            return objLabourDetails;
        }

        public List<Assigntoworkorder> PrintAssignEmployeeDetails(string workOrderNo)
        {
            List<Assigntoworkorder> objAssignEmployee = new List<Assigntoworkorder>();
            IList<Assigntoworkorder> lstAssignEmployee = new List<Assigntoworkorder>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "select employees.Name,employees.EmployeeNO,assigntoworkorder.EstStartDate,assigntoworkorder.EstEndDate from assigntoworkorder inner join employees on assigntoworkorder.EmployeeID = employees.EmployeeID where (assigntoworkorder.WorkorderNo = @WorkorderNo)";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstAssignEmployee = context.ExecuteQuery<Assigntoworkorder>(query, parameters);
            }
            if (lstAssignEmployee != null && lstAssignEmployee.Count > 0)
            {
                objAssignEmployee = lstAssignEmployee.ToList();
            }
            return objAssignEmployee;
        }

        public List<Workorderelement> PrintWorkOrderElement(string workOrderNo)
        {
            List<Workorderelement> objWorkOrderElement = new List<Workorderelement>();
            IList<Workorderelement> lstWorkOrderElement = new List<Workorderelement>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "select workorderelements.SeqNo,workorderelements.TaskDesc,workorderelements.AltTaskDesc from workorderelements inner join workorders on workorderelements.WorkorderNo = workorders.WorkorderNo WHERE (workorders.WorkorderNo = @WorkorderNo OR @WorkorderNo IS NULL) order By workorderelements.SeqNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWorkOrderElement = context.ExecuteQuery<Workorderelement>(query, parameters);
            }
            if (lstWorkOrderElement != null && lstWorkOrderElement.Count > 0)
            {
                objWorkOrderElement = lstWorkOrderElement.ToList();
            }
            return objWorkOrderElement;
        }

        public List<ChecklistInv> PrintChecklistInvItems(string workOrderNo)
        {
            List<ChecklistInv> objWorkOrderElement = new List<ChecklistInv>();
            IList<ChecklistInv> lstWorkOrderElement = new List<ChecklistInv>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "SELECT ChecklistInvId,StockNo AS PartsListNo,StockDescription AS PartsListDescription,StockID As PartID,AltStockDescription AS PartsListAltDescription,Qty,cle.SeqNo FROM checklistInv CI INNER JOIN workorders on CI.ChecklistID = workorders.PMChecklistID INNER JOIN stockcode SC ON SC.StockID=CI.PartID LEFT JOIN checklistelements cle on cle.ChecklistelementId = ci.ChecklistelementId WHERE (workorders.WorkorderNo = @WorkorderNo OR @WorkorderNo IS NULL) order By cle.SeqNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWorkOrderElement = context.ExecuteQuery<ChecklistInv>(query, parameters);
            }
            if (lstWorkOrderElement != null && lstWorkOrderElement.Count > 0)
            {
                objWorkOrderElement = lstWorkOrderElement.ToList();
            }
            return objWorkOrderElement;
        }


        public List<ChecklistTool> PrintChecklistTools(string workOrderNo)
        {
            List<ChecklistTool> objWorkOrderElement = new List<ChecklistTool>();
            IList<ChecklistTool> lstWorkOrderElement = new List<ChecklistTool>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "Select ChecklistTools.ChecklistToolID,ChecklistTools.ChecklistID,ChecklistTools.SeqNo,ChecklistTools.ToolDescription,ChecklistTools.AltToolDescription,ChecklistTools.Quantity FROM ChecklistTools INNER JOIN workorders on ChecklistTools.ChecklistID = workorders.PMChecklistID WHERE (workorders.WorkorderNo = @WorkorderNo OR @WorkorderNo IS NULL) order By ChecklistTools.SeqNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWorkOrderElement = context.ExecuteQuery<ChecklistTool>(query, parameters);
            }
            if (lstWorkOrderElement != null && lstWorkOrderElement.Count > 0)
            {
                objWorkOrderElement = lstWorkOrderElement.ToList();
            }
            return objWorkOrderElement;
        }


        public List<ChecklistPPE> PrintChecklistPPE(string workOrderNo)
        {
            List<ChecklistPPE> objWorkOrderElement = new List<ChecklistPPE>();
            IList<ChecklistPPE> lstWorkOrderElement = new List<ChecklistPPE>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            query = "Select checklistppe.ChecklistPPEID,checklistppe.ChecklistID,checklistppe.SeqNo,checklistppe.PPEDescription,checklistppe.AltPPEDescription FROM checklistppe INNER JOIN workorders on checklistppe.ChecklistID = workorders.PMChecklistID  WHERE (workorders.WorkorderNo = @WorkorderNo OR @WorkorderNo IS NULL) order By checklistppe.SeqNo";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWorkOrderElement = context.ExecuteQuery<ChecklistPPE>(query, parameters);
            }
            if (lstWorkOrderElement != null && lstWorkOrderElement.Count > 0)
            {
                objWorkOrderElement = lstWorkOrderElement.ToList();
            }
            return objWorkOrderElement;
        }



        public List<WorkOrder> GetJobOrderDetailsForDataSource(string employeeId, int NonCentral)
        {
            List<WorkOrder> objWO = new List<WorkOrder>();
            IList<WorkOrder> lstWoRequest = new List<WorkOrder>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (NonCentral == 0)
            {
                if (!string.IsNullOrEmpty(employeeId))
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where Lower(workorders.WorkorderNo) in (select Lower(intermediate_pmgenerate.WorkorderNo) from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,' ') <> ' ') ";
                }
                else
                {
                    query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where Lower(workorders.WorkorderNo) in (select Lower(intermediate_pmgenerate.WorkorderNo) from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '') ";
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(employeeId))
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = employeeId,
                        DBType = DbType.Int32
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where Lower(workorders.WorkorderNo) in  (select Lower(intermediate_pmgenerate.WorkorderNo) from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,' ') <> ' ') AND (workorders.L2ID in (select L2Id from employees_L2 where empID = @EmployeeID union Select L2Id from employees where employeeId = @EmployeeID))";
                }
                else
                {
                    query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where Lower(workorders.WorkorderNo) in  (select Lower(intermediate_pmgenerate.WorkorderNo) from intermediate_pmgenerate where intermediate_pmgenerate.Userid = @EmployeeID And ISNULL(intermediate_pmgenerate.WorkorderNo,'') <> '') AND (workorders.L2ID in (select L2Id from employees_L2 where empID = @EmployeeID union Select L2Id from employees where employeeId = @EmployeeID))";
                }
            }
            using (DapperDBContext context = new DapperDBContext())
            {
                lstWoRequest = context.ExecuteQuery<WorkOrder>(query, parameters);
            }
            if (lstWoRequest != null && lstWoRequest.Count > 0)
            {
                objWO = lstWoRequest.ToList();
            }
            return objWO;
        }

        public List<WorkOrder> PrintJobOrderDetails(string workOrderNo)
        {
            List<WorkOrder> objPrintWO = new List<WorkOrder>();
            IList<WorkOrder> lstWoRequest = new List<WorkOrder>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (!string.IsNullOrEmpty(workOrderNo))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
            }
            //query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where workorders.WorkorderNo = @WorkorderNo ";
            query = "select workorders.WorkorderNo,worktrade.WorkTrade,worktrade.AltWorkTrade,worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workstatus.WorkStatus,workstatus.AltWorkStatus,workpriority.WorkPriority,workpriority.AltWorkPriority,ec.Name,workorders.DateReceived,workorders.DateRequired,suppliers.SupplierName,suppliers.AltSupplierName,er.Name,ec.WorkPhone,workorders.ProblemDescription,assets.NotesToTech,location.NotetoTech,failurecause.FailureCauseCode,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,pmschedule.PMNo,location.LocationNo,location.LocationDescription,location.LocationAltDescription,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription,pmchecklist.ChecklistNo,pmchecklist.CheckListName,pmchecklist.AltCheckListName,workorders.ImageRaw,workorders.ActionTaken,workorders.CauseDescription,workorders.PreventionTaken from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join workpriority on workorders.WorkPriorityID  = workpriority.WorkPriorityID inner join employees ec on workorders.CreatedBy = ec.EmployeeID left outer join suppliers on workorders.SupplierId = suppliers.SupplierID left outer join employees er on workorders.RequestorID = er.EmployeeID left outer join assets on workorders.AssetID = assets.AssetID left outer join location on workorders.LocationID = location.LocationID left outer join failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join L2 on workorders.L2ID = L2.L2ID left outer join L3 on workorders.L3ID = L3.L3ID left outer join l4 on workorders.L4ID = L4.L4ID left outer join L5 on workorders.L5ID = L5.L5ID inner join MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID left outer join pmschedule on workorders.PMID = pmschedule.PMID left outer join pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID where workorders.WorkorderNo = @WorkorderNo ";

            using (DapperDBContext context = new DapperDBContext())
            {
                lstWoRequest = context.ExecuteQuery<WorkOrder>(query, parameters);
            }
            if (lstWoRequest != null && lstWoRequest.Count > 0)
            {
                objPrintWO = lstWoRequest.ToList();
            }
            return objPrintWO;
        }

        public string NewJobOrder(WorkOrder newJO)
        {
            try
            {
                string strInsertColumn = "";
                string strInsertValues = "";

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                newJO.CreatedBy = ProjectSession.EmployeeID.ToString();

                if ((newJO.L2ID != null))
                {
                    strInsertColumn = strInsertColumn + " L2Id, ";
                    strInsertValues = strInsertValues + "@L2ID, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = newJO.L2ID,
                        DBType = DbType.Int32
                    });
                }
                if ((newJO.MaintDivisionID != null))
                {
                    strInsertColumn = strInsertColumn + " MaintDivisionID, ";
                    strInsertValues = strInsertValues + "@MaintDivisionID, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintDivisionID",
                        Value = newJO.MaintDivisionID,
                        DBType = DbType.Int32
                    });
                }
                if ((newJO.MaintdeptID != null))
                {
                    strInsertColumn = strInsertColumn + " maintdeptID, ";
                    strInsertValues = strInsertValues + "@MaintdeptID, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdeptID",
                        Value = newJO.MaintdeptID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.MaintsubdeptID != null))
                {
                    strInsertColumn = strInsertColumn + " maintsubdeptID, ";
                    strInsertValues = strInsertValues + "@MaintsubdeptID, ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintsubdeptID",
                        Value = newJO.MaintsubdeptID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.WorkPriorityID != null))
                {
                    strInsertColumn = strInsertColumn + " WorkPriorityID, ";
                    strInsertValues = strInsertValues + "@WorkPriorityID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkPriorityID",
                        Value = newJO.WorkPriorityID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.WOTradeID != null))
                {
                    strInsertColumn = strInsertColumn + " WOTradeID, ";
                    strInsertValues = strInsertValues + "@WOTradeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOTradeID",
                        Value = newJO.WOTradeID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.WorkTypeID != null))
                {
                    strInsertColumn = strInsertColumn + " WorkTypeID, ";
                    strInsertValues = strInsertValues + "@WorkTypeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkTypeID",
                        Value = newJO.WorkTypeID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.EmployeeID != null))
                {
                    strInsertColumn = strInsertColumn + " EmployeeID, ";
                    strInsertValues = strInsertValues + "@EmployeeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = newJO.EmployeeID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.L3ID != null))
                {
                    strInsertColumn = strInsertColumn + " L3ID, ";
                    strInsertValues = strInsertValues + "@L3ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L3ID",
                        Value = newJO.L3ID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.L4ID != null))
                {
                    strInsertColumn = strInsertColumn + " L4ID, ";
                    strInsertValues = strInsertValues + "@L4ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L4ID",
                        Value = newJO.L4ID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.L5ID != null))
                {
                    strInsertColumn = strInsertColumn + " L5ID, ";
                    strInsertValues = strInsertValues + "@L5ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L5ID",
                        Value = newJO.L5ID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.LocationID != null))
                {
                    strInsertColumn = strInsertColumn + " LocationID, ";
                    strInsertValues = strInsertValues + "@LocationID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LocationID",
                        Value = newJO.LocationID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.AssetID != null) && newJO.AssetID > 0)
                {
                    strInsertColumn = strInsertColumn + " AssetID, ";
                    strInsertValues = strInsertValues + "@AssetID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AssetID",
                        Value = newJO.AssetID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.CIGID != null))
                {
                    strInsertColumn = strInsertColumn + " CIGID, ";
                    strInsertValues = strInsertValues + newJO.CIGID + ", ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CIGID",
                        Value = newJO.CIGID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.IsCleaningModule != null))
                {
                    strInsertColumn = strInsertColumn + " IsCleaningModule, ";
                    strInsertValues = strInsertValues + "@IsCleaningModule,";
                    parameters.Add(new DBParameters()
                    {
                        Name = "IsCleaningModule",
                        Value = Convert.ToInt32(newJO.IsCleaningModule),
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.GroupID != null))
                {
                    strInsertColumn = strInsertColumn + " groupID, ";
                    strInsertValues = strInsertValues + "@GroupID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "GroupID",
                        Value = newJO.GroupID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.SupplierId != null))
                {
                    strInsertColumn = strInsertColumn + " SupplierId, ";
                    strInsertValues = strInsertValues + "@SupplierId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierId",
                        Value = newJO.SupplierId,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.WorkStatusID != null))
                {
                    strInsertColumn = strInsertColumn + " WorkStatusID, ";
                    strInsertValues = strInsertValues + "@WorkStatusID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkStatusID",
                        Value = newJO.WorkStatusID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.PMChecklistID != null))
                {
                    strInsertColumn = strInsertColumn + " PMChecklistID, ";
                    strInsertValues = strInsertValues + "@PMChecklistID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMChecklistID",
                        Value = newJO.PMChecklistID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.PMID != null))
                {
                    strInsertColumn = strInsertColumn + " PMID, ";
                    strInsertValues = strInsertValues + "@PMID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMID",
                        Value = newJO.PMID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.RequestorID != null) && newJO.RequestorID > 0)
                {
                    strInsertColumn = strInsertColumn + " RequestorID, ";
                    strInsertValues = strInsertValues + "@RequestorID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RequestorID",
                        Value = newJO.RequestorID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.FailureCauseID != null))
                {
                    strInsertColumn = strInsertColumn + " FailureCauseID, ";
                    strInsertValues = strInsertValues + "@FailureCauseID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "FailureCauseID",
                        Value = newJO.FailureCauseID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.MeterID != null))
                {
                    strInsertColumn = strInsertColumn + " MeterID, ";
                    strInsertValues = strInsertValues + "@MeterID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MeterID",
                        Value = newJO.MeterID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.AcceptedbyID != null))
                {
                    strInsertColumn = strInsertColumn + " AcceptedbyID, ";
                    strInsertValues = strInsertValues + "@AcceptedbyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AcceptedbyID",
                        Value = newJO.AcceptedbyID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.RatingsID != null))
                {
                    strInsertColumn = strInsertColumn + " RatingsID, ";
                    strInsertValues = strInsertValues + "@RatingsID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RatingsID",
                        Value = newJO.RatingsID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.WOclosebyID != null))
                {
                    strInsertColumn = strInsertColumn + " WOclosebyID, ";
                    strInsertValues = strInsertValues + "@WOclosebyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOclosebyID",
                        Value = newJO.WOclosebyID,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.CostCenterId != null))
                {
                    strInsertColumn = strInsertColumn + " CostCenterId, ";
                    strInsertValues = strInsertValues + "@CostCenterId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CostCenterId",
                        Value = newJO.CostCenterId,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.ProblemDescription != null))
                {
                    strInsertColumn = strInsertColumn + " ProblemDescription, ";
                    strInsertValues = strInsertValues + "@ProblemDescription, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProblemDescription",
                        Value = newJO.ProblemDescription,
                        DBType = DbType.String
                    });
                }

                if ((newJO.DateReceived != null))
                {
                    strInsertColumn = strInsertColumn + " DateReceived, ";
                    strInsertValues = strInsertValues + "@DateReceived, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateReceived",
                            Value = newJO.DateReceived,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateReceived",
                            Value = Common.GetEnglishDate(newJO.DateReceived),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.EstDateStart != null))
                {
                    strInsertColumn = strInsertColumn + " EstDateStart, ";
                    strInsertValues = strInsertValues + "@EstDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = newJO.EstDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = Common.GetEnglishDate(newJO.EstDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.EstDateEnd != null))
                {
                    strInsertColumn = strInsertColumn + " EstDateEnd, ";
                    strInsertValues = strInsertValues + "@EstDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = newJO.EstDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = Common.GetEnglishDate(newJO.EstDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.ActDateStart != null))
                {
                    strInsertColumn = strInsertColumn + " ActDateStart, ";
                    strInsertValues = strInsertValues + "@ActDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = newJO.ActDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = Common.GetEnglishDate(newJO.ActDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.ActDateEnd != null))
                {
                    strInsertColumn = strInsertColumn + " ActDateEnd, ";
                    strInsertValues = strInsertValues + "@ActDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = newJO.ActDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = Common.GetEnglishDate(newJO.ActDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.DateRequired != null))
                {
                    strInsertColumn = strInsertColumn + " DateRequired, ";
                    strInsertValues = strInsertValues + "@DateRequired, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateRequired",
                            Value = newJO.DateRequired.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateRequired",
                            Value = Common.GetEnglishDate(newJO.DateRequired.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.DateHandover != null))
                {
                    strInsertColumn = strInsertColumn + " DateHandover, ";
                    strInsertValues = strInsertValues + "@DateHandover, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = newJO.DateHandover.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = Common.GetEnglishDate(newJO.DateHandover.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.EstDuration != null))
                {
                    strInsertColumn = strInsertColumn + " EstDuration, ";
                    strInsertValues = strInsertValues + "@EstDuration, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "EstDuration",
                        Value = newJO.EstDuration,
                        DBType = DbType.Decimal
                    });
                }

                if ((newJO.ActionTaken != null))
                {
                    strInsertColumn = strInsertColumn + " ActionTaken, ";
                    strInsertValues = strInsertValues + "@ActionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ActionTaken",
                        Value = newJO.ActionTaken,
                        DBType = DbType.String
                    });
                }

                if ((newJO.CauseDescription != null))
                {
                    strInsertColumn = strInsertColumn + " CauseDescription, ";
                    strInsertValues = strInsertValues + "@CauseDescription, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CauseDescription",
                        Value = newJO.CauseDescription,
                        DBType = DbType.String
                    });
                }

                if ((newJO.PreventionTaken != null))
                {
                    strInsertColumn = strInsertColumn + " PreventionTaken, ";
                    strInsertValues = strInsertValues + "@PreventionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PreventionTaken",
                        Value = newJO.PreventionTaken,
                        DBType = DbType.String
                    });
                }

                if ((newJO.WOCost != null))
                {
                    strInsertColumn = strInsertColumn + " WOCost, ";
                    strInsertValues = strInsertValues + "@WOCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOCost",
                        Value = newJO.WOCost,
                        DBType = DbType.Decimal
                    });
                }

                if ((newJO.PMTarStartDate != null))
                {
                    strInsertColumn = strInsertColumn + " PMTarStartDate, ";
                    strInsertValues = strInsertValues + "@PMTarStartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = newJO.PMTarStartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = Common.GetEnglishDate(newJO.PMTarStartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.PMTarCompDate != null))
                {
                    strInsertColumn = strInsertColumn + " PMTarCompDate, ";
                    strInsertValues = strInsertValues + "@PMTarCompDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = newJO.PMTarCompDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = Common.GetEnglishDate(newJO.PMTarCompDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.AstartDate != null))
                {
                    strInsertColumn = strInsertColumn + " AstartDate, ";
                    strInsertValues = strInsertValues + "@AstartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = newJO.AstartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = Common.GetEnglishDate(newJO.AstartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.AEndDate != null))
                {
                    strInsertColumn = strInsertColumn + " AEndDate, ";
                    strInsertValues = strInsertValues + "@AEndDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = newJO.AEndDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = Common.GetEnglishDate(newJO.AEndDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.WOLaborCost != null))
                {
                    strInsertColumn = strInsertColumn + " WOLaborCost, ";
                    strInsertValues = strInsertValues + "@WOLaborCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOLaborCost",
                        Value = newJO.WOLaborCost,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.WODICost != null))
                {
                    strInsertColumn = strInsertColumn + " WODICost, ";
                    strInsertValues = strInsertValues + "@WODICost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WODICost",
                        Value = newJO.WODICost,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.WOPartCost != null))
                {
                    strInsertColumn = strInsertColumn + " WOPartCost, ";
                    strInsertValues = strInsertValues + "@WOPartCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOPartCost",
                        Value = newJO.WOPartCost,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.WOPMtype != null))
                {
                    strInsertColumn = strInsertColumn + " WOPMtype, ";
                    strInsertValues = strInsertValues + "@WOPMtype, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOPMtype",
                        Value = newJO.WOPMtype,
                        DBType = DbType.String
                    });
                }

                if ((newJO.PMJOGeneratedReading != null))
                {
                    strInsertColumn = strInsertColumn + " PMJOGeneratedReading, ";
                    strInsertValues = strInsertValues + "@PMJOGeneratedReading, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMJOGeneratedReading",
                        Value = newJO.PMJOGeneratedReading,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.PMMetReading != null))
                {
                    strInsertColumn = strInsertColumn + " PMMetReading, ";
                    strInsertValues = strInsertValues + "@PMMetReading, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMMetReading",
                        Value = newJO.PMMetReading,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.DownTime != null))
                {
                    strInsertColumn = strInsertColumn + " DownTime, ";
                    strInsertValues = strInsertValues + "@DownTime, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "DownTime",
                        Value = newJO.DownTime,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.LineDowntime != null))
                {
                    strInsertColumn = strInsertColumn + " LineDowntime, ";
                    strInsertValues = strInsertValues + "@LineDowntime, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LineDowntime",
                        Value = newJO.LineDowntime,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.MTask != null))
                {
                    strInsertColumn = strInsertColumn + " MTask, ";
                    strInsertValues = strInsertValues + "@MTask, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MTask",
                        Value = newJO.MTask,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.Notes != null))
                {
                    strInsertColumn = strInsertColumn + " Notes, ";
                    strInsertValues = strInsertValues + "@Notes, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Notes",
                        Value = newJO.Notes,
                        DBType = DbType.String
                    });
                }

                if ((newJO.MPMID != null))
                {
                    strInsertColumn = strInsertColumn + " MPMID, ";
                    strInsertValues = strInsertValues + "@MPMID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MPMID",
                        Value = newJO.MPMID,
                        DBType = DbType.Double
                    });
                }

                if ((newJO.Print_date != null))
                {
                    strInsertColumn = strInsertColumn + " print_date, ";
                    strInsertValues = strInsertValues + "@Print_date, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "Print_date",
                            Value = newJO.Print_date.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "Print_date",
                            Value = Common.GetEnglishDate(newJO.Print_date.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((newJO.TelNo != null))
                {
                    strInsertColumn = strInsertColumn + " TelNo, ";
                    strInsertValues = strInsertValues + "@TelNo, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "TelNo",
                        Value = newJO.TelNo,
                        DBType = DbType.String
                    });
                }

                if ((newJO.Accountcodeid != null))
                {
                    strInsertColumn = strInsertColumn + " accountcodeid, ";
                    strInsertValues = strInsertValues + "@Accountcodeid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Accountcodeid",
                        Value = newJO.Accountcodeid,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.Documentstatusid != null))
                {
                    strInsertColumn = strInsertColumn + " Documentstatusid, ";
                    strInsertValues = strInsertValues + "@Documentstatusid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Documentstatusid",
                        Value = newJO.Documentstatusid,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.AuthorisedEmployeeId != null))
                {
                    strInsertColumn = strInsertColumn + " AuthorisedEmployeeID, ";
                    strInsertValues = strInsertValues + "@AuthorisedEmployeeId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AuthorisedEmployeeId",
                        Value = newJO.AuthorisedEmployeeId,
                        DBType = DbType.Int32
                    });
                }

                if ((newJO.ProjectNumber != null))
                {
                    strInsertColumn = strInsertColumn + " ProjectNumber, ";
                    strInsertValues = strInsertValues + "@ProjectNumber, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProjectNumber",
                        Value = newJO.ProjectNumber,
                        DBType = DbType.String
                    });
                }

                if ((newJO.CreatedBy != null))
                {
                    strInsertColumn = strInsertColumn + " CreatedBy, ";
                    strInsertValues = strInsertValues + "@CreatedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = newJO.CreatedBy,
                        DBType = DbType.Int32
                    });
                }
                //if ((newJO.CreatedDate != null))
                //{
                strInsertColumn = strInsertColumn + " CreatedDate, ";
                strInsertValues = strInsertValues + "@CreatedDate, ";
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                //}

                newJO.WorkorderNo = GetNewJobOrderNo(Convert.ToInt32(newJO.L2ID));

                if ((newJO.WorkorderNo != null))
                {
                    strInsertColumn = strInsertColumn + " WorkorderNo ";
                    strInsertValues = strInsertValues + "@WorkorderNo ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkorderNo",
                        Value = newJO.WorkorderNo,
                        DBType = DbType.String
                    });
                }

                string strQuery = "INSERT into workorders(" + strInsertColumn + ")  " + " VALUES(" + strInsertValues + ")";


                using (ServiceContext context = new ServiceContext())
                {
                    context.ExecuteQuery<Worequest>(strQuery, parameters);
                }

                EmailConfigurationService objService = new EmailConfigurationService();
                objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderCreation, newJO.WorkorderNo, string.Empty, newJO.WorkStatusID.ToString());

                return newJO.WorkorderNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int SaveJobOrderByExtend(WorkOrder currentJO)
        {
            try
            {

                string strUpdateColumn = "";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if ((currentJO.RequestorID != null) && currentJO.RequestorID > 0)
                {
                    strUpdateColumn = strUpdateColumn + " RequestorID = @RequestorID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RequestorID",
                        Value = currentJO.RequestorID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " RequestorID = NULL , ";
                }

                if ((currentJO.EmployeeID != null))
                {
                    strUpdateColumn = strUpdateColumn + " EmployeeID = @EmployeeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "EmployeeID",
                        Value = currentJO.EmployeeID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.L2ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L2ID = @L2ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = currentJO.L2ID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.L3ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L3ID = @L3ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L3ID",
                        Value = currentJO.L3ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L3ID = NULL , ";
                }
                if ((currentJO.L4ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L4ID = @L4ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L4ID",
                        Value = currentJO.L4ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L4ID = NULL , ";
                }
                if ((currentJO.L5ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L5ID = @L5ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L5ID",
                        Value = currentJO.L5ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L5ID = NULL , ";
                }

                if ((currentJO.LocationID != null))
                {
                    strUpdateColumn = strUpdateColumn + " LocationID = @LocationID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LocationID",
                        Value = currentJO.LocationID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " LocationID = NULL , ";
                }

                if ((currentJO.AssetID != null) && currentJO.AssetID > 0)
                {
                    strUpdateColumn = strUpdateColumn + " AssetID = @AssetID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AssetID",
                        Value = currentJO.AssetID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " AssetID = NULL , ";
                }

                if ((currentJO.WorkStatusID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkStatusID = @WorkStatusID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkStatusID",
                        Value = currentJO.WorkStatusID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.WorkPriorityID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkPriorityID = @WorkPriorityID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkPriorityID",
                        Value = currentJO.WorkPriorityID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.FailureCauseID != null))
                {
                    strUpdateColumn = strUpdateColumn + " FailureCauseID = @FailureCauseID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "FailureCauseID",
                        Value = currentJO.FailureCauseID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.WorkTypeID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkTypeID = @WorkTypeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkTypeID",
                        Value = currentJO.WorkTypeID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.WOTradeID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOTradeID = @WOTradeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOTradeID",
                        Value = currentJO.WOTradeID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.MeterID != null))
                {
                    strUpdateColumn = strUpdateColumn + " MeterID = @MeterID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MeterID",
                        Value = currentJO.MeterID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " MeterID = NULL , ";
                }

                if ((currentJO.PMID != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMID = @PMID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMID",
                        Value = currentJO.PMID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " PMID = NULL , ";
                }

                if ((currentJO.MaintDivisionID != null))
                {
                    strUpdateColumn = strUpdateColumn + " MaintDivisionID = @MaintDivisionID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintDivisionID",
                        Value = currentJO.MaintDivisionID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.MaintdeptID != null))
                {
                    strUpdateColumn = strUpdateColumn + " maintdeptID = @MaintdeptID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdeptID",
                        Value = currentJO.MaintdeptID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.MaintsubdeptID != null))
                {
                    strUpdateColumn = strUpdateColumn + " maintsubdeptID = @MaintsubdeptID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintsubdeptID",
                        Value = currentJO.MaintsubdeptID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.GroupID != null))
                {
                    strUpdateColumn = strUpdateColumn + " groupID = @GroupID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "GroupID",
                        Value = currentJO.GroupID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.SupplierId != null))
                {
                    strUpdateColumn = strUpdateColumn + " SupplierId = @SupplierId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierId",
                        Value = currentJO.SupplierId,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.AcceptedbyID != null))
                {
                    strUpdateColumn = strUpdateColumn + " AcceptedbyID = @AcceptedbyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AcceptedbyID",
                        Value = currentJO.AcceptedbyID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.PMChecklistID != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMChecklistID = @PMChecklistID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMChecklistID",
                        Value = currentJO.PMChecklistID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.RatingsID != null))
                {
                    strUpdateColumn = strUpdateColumn + " RatingsID = @RatingsID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RatingsID",
                        Value = currentJO.RatingsID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.WOclosebyID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOclosebyID = @WOclosebyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOclosebyID",
                        Value = currentJO.WOclosebyID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.CostCenterId != null))
                {
                    strUpdateColumn = strUpdateColumn + " CostCenterId = @CostCenterId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CostCenterId",
                        Value = currentJO.CostCenterId,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.ProblemDescription != null))
                {
                    strUpdateColumn = strUpdateColumn + " ProblemDescription = @ProblemDescription, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProblemDescription",
                        Value = currentJO.ProblemDescription,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.DateReceived != null))
                {
                    strUpdateColumn = strUpdateColumn + " DateReceived = @DateReceived, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateReceived",
                            Value = currentJO.DateReceived.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateReceived",
                            Value = Common.GetEnglishDate(currentJO.DateReceived.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.EstDateStart != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDateStart = @EstDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = currentJO.EstDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = Common.GetEnglishDate(currentJO.EstDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.EstDateEnd != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDateEnd = @EstDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = currentJO.EstDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = Common.GetEnglishDate(currentJO.EstDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.ActDateStart != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActDateStart = @ActDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = currentJO.ActDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = Common.GetEnglishDate(currentJO.ActDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }
                if ((currentJO.ActDateEnd != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActDateEnd = @ActDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = currentJO.ActDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = Common.GetEnglishDate(currentJO.ActDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }
                if ((currentJO.DateRequired != null))
                {
                    strUpdateColumn = strUpdateColumn + " DateRequired = @DateRequired, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateRequired",
                            Value = currentJO.DateRequired.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateRequired",
                            Value = Common.GetEnglishDate(currentJO.DateRequired.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }
                if ((currentJO.DateHandover != null))
                {
                    strUpdateColumn = strUpdateColumn + " DateHandover = @DateHandover, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = currentJO.DateHandover.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = Common.GetEnglishDate(currentJO.DateHandover.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }
                if ((currentJO.EstDuration != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDuration = @EstDuration, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "EstDuration",
                        Value = currentJO.EstDuration,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.ActionTaken != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActionTaken = @ActionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ActionTaken",
                        Value = currentJO.ActionTaken,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.CauseDescription != null))
                {
                    strUpdateColumn = strUpdateColumn + " CauseDescription = @CauseDescription, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CauseDescription",
                        Value = currentJO.CauseDescription,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.PreventionTaken != null))
                {
                    strUpdateColumn = strUpdateColumn + " PreventionTaken = @PreventionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PreventionTaken",
                        Value = currentJO.PreventionTaken,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.WOCost != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOCost = @WOCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOCost",
                        Value = currentJO.WOCost,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.PMTarStartDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMTarStartDate = @PMTarStartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = currentJO.PMTarStartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = Common.GetEnglishDate(currentJO.PMTarStartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.PMTarCompDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMTarCompDate = @PMTarCompDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = currentJO.PMTarCompDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = Common.GetEnglishDate(currentJO.PMTarCompDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.AstartDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " AstartDate = @AstartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = currentJO.AstartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = Common.GetEnglishDate(currentJO.AstartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.AEndDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " AEndDate = @AEndDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = currentJO.AEndDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = Common.GetEnglishDate(currentJO.AEndDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.WOLaborCost != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOLaborCost = @WOLaborCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOLaborCost",
                        Value = currentJO.WOLaborCost,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.WODICost != null))
                {
                    strUpdateColumn = strUpdateColumn + " WODICost = @WODICost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WODICost",
                        Value = currentJO.WODICost,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.WOPartCost != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOPartCost = @WOPartCost, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOPartCost",
                        Value = currentJO.WOPartCost,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.WOPMtype != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOPMtype = @WOPMtype, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOPMtype",
                        Value = currentJO.WOPMtype,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.PMJOGeneratedReading != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMJOGeneratedReading = @PMJOGeneratedReading, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMJOGeneratedReading",
                        Value = currentJO.PMJOGeneratedReading,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.PMMetReading != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMMetReading = @PMMetReading, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PMMetReading",
                        Value = currentJO.PMMetReading,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.DownTime != null))
                {
                    strUpdateColumn = strUpdateColumn + " DownTime = @DownTime, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "DownTime",
                        Value = currentJO.DownTime,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.LineDowntime != null))
                {
                    strUpdateColumn = strUpdateColumn + " LineDowntime = @LineDowntime, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LineDowntime",
                        Value = currentJO.LineDowntime,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.MTask != null))
                {
                    strUpdateColumn = strUpdateColumn + " MTask = @MTask, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MTask",
                        Value = currentJO.MTask,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.Notes != null))
                {
                    strUpdateColumn = strUpdateColumn + " Notes = @Notes, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Notes",
                        Value = currentJO.Notes,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.MPMID != null))
                {
                    strUpdateColumn = strUpdateColumn + " MPMID = @MPMID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MPMID",
                        Value = currentJO.MPMID,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.Print_date != null))
                {
                    strUpdateColumn = strUpdateColumn + " print_date = @Print_date , ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "Print_date",
                            Value = currentJO.Print_date.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "Print_date",
                            Value = Common.GetEnglishDate(currentJO.Print_date.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.AssignTo != null))
                {
                    strUpdateColumn = strUpdateColumn + " AssignTo = @AssignTo, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AssignTo",
                        Value = currentJO.AssignTo,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.TelNo != null))
                {
                    strUpdateColumn = strUpdateColumn + " TelNo = @TelNo, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "TelNo",
                        Value = currentJO.TelNo,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.Accountcodeid != null))
                {
                    strUpdateColumn = strUpdateColumn + " accountcodeid = @Accountcodeid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Accountcodeid",
                        Value = currentJO.Accountcodeid,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.Documentstatusid != null))
                {
                    strUpdateColumn = strUpdateColumn + " Documentstatusid = @Documentstatusid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Documentstatusid",
                        Value = currentJO.Documentstatusid,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.AuthorisedEmployeeId != null))
                {
                    strUpdateColumn = strUpdateColumn + " AuthorisedEmployeeID = @AuthorisedEmployeeId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AuthorisedEmployeeId",
                        Value = currentJO.AuthorisedEmployeeId,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.ProjectNumber != null))
                {
                    strUpdateColumn = strUpdateColumn + " ProjectNumber = @ProjectNumber, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProjectNumber",
                        Value = currentJO.ProjectNumber,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.ModifiedBy != null))
                {
                    strUpdateColumn = strUpdateColumn + " ModifiedBy = @ModifiedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = currentJO.ModifiedBy,
                        DBType = DbType.String
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }

                strUpdateColumn = strUpdateColumn + " ModifiedDate = @ModifiedDate ";

                string strQuery = string.Empty;
                strQuery = "UPDATE workorders set " + strUpdateColumn + "  Where WorkorderNo = '" + currentJO.WorkorderNo + "'";
                int id = UpdateWorkOrder(strQuery, currentJO, parameters);

                if (id > 0)
                {
                    if ((currentJO.WorkTypeID == SystemEnum.WorkType.Preventive.GetHashCode() || currentJO.WorkTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode()) && currentJO.PMChecklistID > 0)
                    {
                        InsertWorkOrderItemsByPmCheckListID(currentJO.WorkorderNo, Convert.ToInt32(currentJO.PMChecklistID));
                        InsertWorkOrderToolsByPmCheckListID(currentJO.WorkorderNo, Convert.ToInt32(currentJO.PMChecklistID));
                        InsertWorkOrderPPEByPmCheckListID(currentJO.WorkorderNo, Convert.ToInt32(currentJO.PMChecklistID));
                        InsertWorkOrderElementsByPmCheckListID(currentJO.WorkorderNo, Convert.ToInt32(currentJO.PMChecklistID));
                    }
                }

                return id;
            }
            catch
            {
                return 0;
            }
        }

        public int SaveJobOrderFeedBackByExtend(WorkOrder currentJO)
        {
            try
            {
                string strUpdateColumn = "";
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if ((currentJO.RatingsID != null))
                {
                    strUpdateColumn = strUpdateColumn + " RatingsID = @RatingsID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RatingsID",
                        Value = currentJO.RatingsID,
                        DBType = DbType.Int32
                    });
                }
                if ((currentJO.ModifiedBy != null))
                {
                    strUpdateColumn = strUpdateColumn + " ModifiedBy = @ModifiedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = currentJO.ModifiedBy,
                        DBType = DbType.String
                    });
                } 
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }
                strUpdateColumn = strUpdateColumn + " ModifiedDate = @ModifiedDate";
                string strQuery = string.Empty;
                strQuery = "UPDATE workorders set " + strUpdateColumn + "  Where WorkorderNo = '" + currentJO.WorkorderNo + "'";

                return UpdateWorkOrder(strQuery, currentJO, parameters);
            }
            catch
            {
                return 0;
            }

        }
        public int SaveJobOrderClosingByExtend(WorkOrder currentJO)
        {
            try
            {
                string strUpdateColumn = "";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if ((currentJO.WorkStatusID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkStatusID = @WorkStatusID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkStatusID",
                        Value = currentJO.WorkStatusID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.AstartDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " AstartDate = @AstartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = currentJO.AstartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AstartDate",
                            Value = Common.GetEnglishDate(currentJO.AstartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.AEndDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " AEndDate = @AEndDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = currentJO.AEndDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "AEndDate",
                            Value = Common.GetEnglishDate(currentJO.AEndDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.EstDateStart != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDateStart = @EstDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = currentJO.EstDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateStart",
                            Value = Common.GetEnglishDate(currentJO.EstDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.EstDateEnd != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDateEnd = @EstDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = currentJO.EstDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EstDateEnd",
                            Value = Common.GetEnglishDate(currentJO.EstDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.ActDateStart != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActDateStart = @ActDateStart, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = currentJO.ActDateStart.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateStart",
                            Value = Common.GetEnglishDate(currentJO.ActDateStart.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.ActDateEnd != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActDateEnd = @ActDateEnd, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = currentJO.ActDateEnd.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ActDateEnd",
                            Value = Common.GetEnglishDate(currentJO.ActDateEnd.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.PMTarStartDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMTarStartDate = @PMTarStartDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = currentJO.PMTarStartDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarStartDate",
                            Value = Common.GetEnglishDate(currentJO.PMTarStartDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.PMTarCompDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " PMTarCompDate = @PMTarCompDate, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = currentJO.PMTarCompDate.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "PMTarCompDate",
                            Value = Common.GetEnglishDate(currentJO.PMTarCompDate.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.CauseDescription != null))
                {
                    strUpdateColumn = strUpdateColumn + " CauseDescription = @CauseDescription, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CauseDescription",
                        Value = currentJO.CauseDescription,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.ActionTaken != null))
                {
                    strUpdateColumn = strUpdateColumn + " ActionTaken = @ActionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ActionTaken",
                        Value = currentJO.ActionTaken,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.PreventionTaken != null))
                {
                    strUpdateColumn = strUpdateColumn + " PreventionTaken = @PreventionTaken, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "PreventionTaken",
                        Value = currentJO.PreventionTaken,
                        DBType = DbType.String
                    });
                }

                if ((currentJO.FailureCauseID != null))
                {
                    strUpdateColumn = strUpdateColumn + " FailureCauseID = @FailureCauseID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "FailureCauseID",
                        Value = currentJO.FailureCauseID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.RatingsID != null))
                {
                    strUpdateColumn = strUpdateColumn + " RatingsID = @RatingsID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RatingsID",
                        Value = currentJO.RatingsID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.CostCenterId != null))
                {
                    strUpdateColumn = strUpdateColumn + " CostCenterId = @CostCenterId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CostCenterId",
                        Value = currentJO.CostCenterId,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.Accountcodeid != null))
                {
                    strUpdateColumn = strUpdateColumn + " accountcodeid = @Accountcodeid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Accountcodeid",
                        Value = currentJO.Accountcodeid,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.AcceptedbyID != null))
                {
                    strUpdateColumn = strUpdateColumn + " AcceptedbyID = @AcceptedbyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AcceptedbyID",
                        Value = currentJO.AcceptedbyID,
                        DBType = DbType.Int32
                    });
                }

                if ((currentJO.WOclosebyID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WOclosebyID = @WOclosebyID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WOclosebyID",
                        Value = currentJO.WOclosebyID,
                        DBType = DbType.Int32
                    });

                }

                if ((currentJO.DateHandover != null))
                {
                    strUpdateColumn = strUpdateColumn + " DateHandover = @DateHandover, ";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = currentJO.DateHandover.Value,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "DateHandover",
                            Value = Common.GetEnglishDate(currentJO.DateHandover.Value),
                            DBType = DbType.DateTime
                        });
                    }

                }

                if ((currentJO.DownTime != null))
                {
                    strUpdateColumn = strUpdateColumn + " DownTime = @DownTime, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "DownTime",
                        Value = currentJO.DownTime,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.EstDuration != null))
                {
                    strUpdateColumn = strUpdateColumn + " EstDuration = @EstDuration, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "EstDuration",
                        Value = currentJO.EstDuration,
                        DBType = DbType.Double
                    });
                }

                if ((currentJO.ModifiedBy != null))
                {
                    strUpdateColumn = strUpdateColumn + " ModifiedBy = @ModifiedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = currentJO.ModifiedBy,
                        DBType = DbType.String
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }

                strUpdateColumn = strUpdateColumn + " ModifiedDate = @ModifiedDate";
                string strQuery = string.Empty;
                strQuery = "UPDATE workorders set " + strUpdateColumn + "  Where WorkorderNo = '" + currentJO.WorkorderNo + "'";

                return UpdateWorkOrder(strQuery, currentJO, parameters);
            }
            catch
            {
                return 0;
            }
        }

        private int UpdateWorkOrder(string strUpdateQuery, WorkOrder currentJO, Collection<DBParameters> parameters)
        {
            try
            {
                int result = -1;
                int OldWorkStatusID = -1;
                string strQuery = string.Empty;

                using (ServiceContext objContext = new ServiceContext())
                {
                    strQuery = "Select WorkStatusID from workorders  Where WorkorderNo = '" + currentJO.WorkorderNo + "'";
                    OldWorkStatusID = Convert.ToInt32(objContext.ExecuteScalar(strQuery));
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strUpdateQuery, parameters);
                }

                Worequest objWR = new Worequest();
                objWR.WorkorderNo = currentJO.WorkorderNo;
                EmailConfigurationService objService = new EmailConfigurationService();
                parameters = new Collection<DBParameters>();

                if (result == 0)
                {
                    if (currentJO.WorkStatusID == SystemEnum.WorkRequestStatus.Closed.GetHashCode() || currentJO.WorkStatusID == SystemEnum.WorkRequestStatus.Cancelled.GetHashCode())
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objWR = objContext.Search<Worequest>(objWR).FirstOrDefault();

                            if (objWR != null && !string.IsNullOrEmpty(objWR.RequestNo))
                            {
                                result = -1;
                                parameters.Add(new DBParameters()
                                {
                                    Name = "WorkStatusID",
                                    Value = currentJO.WorkStatusID,
                                    DBType = DbType.Int32
                                });
                                parameters.Add(new DBParameters()
                                {
                                    Name = "ModifiedBy",
                                    Value = currentJO.ModifiedBy,
                                    DBType = DbType.String
                                });
                                parameters.Add(new DBParameters()
                                {
                                    Name = "RequestNo",
                                    Value = objWR.RequestNo,
                                    DBType = DbType.String
                                });

                                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "ModifiedDate",
                                        Value = DateTime.Now,
                                        DBType = DbType.DateTime
                                    });
                                }
                                else
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "ModifiedDate",
                                        Value = Common.GetEnglishDate(DateTime.Now),
                                        DBType = DbType.DateTime
                                    });
                                }



                                strQuery = "Update worequest set RequestStatusID = @WorkStatusID , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate where RequestNo = @RequestNo";
                                result = objContext.ExecuteQuery(strQuery, parameters);

                                if (result == 0)
                                {
                                    objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkRequest, SystemEnum.ModuleEvent.JobRequestJobStatusChange, objWR.RequestNo);
                                }
                            }
                        }
                    }

                    if (currentJO.WorkStatusID != OldWorkStatusID && OldWorkStatusID != -1)
                    {
                        objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderJobStatusChange, currentJO.WorkorderNo, OldWorkStatusID.ToString(), currentJO.WorkStatusID.ToString());
                        return 2;
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateTargetStartDateForActualPM(int PMID, DateTime ActDateEnd)
        {
            try
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                int result = 0;
                string DateAddFunction = "DATEADD(DAY, (Frequency * FreqUnits) + PeriodDays, '" + Common.GetEnglishDate(ActDateEnd) + "') ";
                if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    DateAddFunction = "DATE_ADD('" + Common.GetEnglishDate(ActDateEnd) + "', INTERVAL (Frequency * FreqUnits) + PeriodDays DAY) ";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    DateAddFunction = Common.GetEnglishDate(ActDateEnd) + "(Frequency * FreqUnits) + PeriodDays";
                }

                parameters.Add(new DBParameters()
                {
                    Name = "DateAddFunction",
                    Value = DateAddFunction,
                    DBType = DbType.DateTime
                });

                parameters.Add(new DBParameters()
                {
                    Name = "PMID",
                    Value = PMID,
                    DBType = DbType.Int32
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ActDateEnd",
                        Value = ActDateEnd,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ActDateEnd",
                        Value = Common.GetEnglishDate(ActDateEnd),
                        DBType = DbType.DateTime
                    });
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    strQuery = "Update pmschedule set TargetStartDate = @DateAddFunction, ";
                    strQuery += " TargetCompDate = @DateAddFunction , ActualCompDate = @ActDateEnd ";
                    strQuery += " where PMID = @PMID and TypePMgenID = 2";

                    result = objContext.ExecuteQuery(strQuery, parameters);
                }
                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ChangeJobOrderStatusByWorkorderNo(string workorderNo, int workStatusID, int oldWorkStatusId, string modifiedBy, bool autoStatusAudit = false, string moduleName = "")
        {
            try
            {
                int result = -1;
                string workRequestNo = string.Empty;
                EmailConfigurationService objService = new EmailConfigurationService();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }
                string strSQl = "Update workorders set WorkStatusID = " + workStatusID + " , ModifiedBy = '" + Common.setQuote(modifiedBy) + "' , ModifiedDate = @ModifiedDate where WorkorderNo = '" + Common.setQuote(workorderNo) + "'";
                string strOldStatusSQL = "Select WorkStatusId from Workorders  where Workorderno ='" + Common.setQuote(workorderNo) + "'";
                using (ServiceContext objContext = new ServiceContext())
                {
                    if (oldWorkStatusId == -1)
                    {
                        oldWorkStatusId = Convert.ToInt32(objContext.ExecuteScalar(strOldStatusSQL));
                    }

                    if (oldWorkStatusId != workStatusID)
                    {
                        result = objContext.ExecuteQuery(strSQl, parameters);
                    }

                    if (result == 0)
                    {
                        objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderJobStatusChange, workorderNo, oldWorkStatusId.ToString(), workStatusID.ToString());
                        if (workStatusID == 2 || workStatusID == 3)
                        {
                            result = -1;
                            strSQl = "select worequest.RequestNo from worequest where WorkorderNo = '" + Common.setQuote(workorderNo) + "'";

                            workRequestNo = objContext.ExecuteScalar(strSQl);

                            if (!string.IsNullOrEmpty(workRequestNo))
                            {
                                strSQl = "Update worequest set RequestStatusID = " + workStatusID + " , ModifiedBy = '" + modifiedBy + "' , ModifiedDate = @ModifiedDate where RequestNo = '" + Common.setQuote(workRequestNo) + "'";
                                result = objContext.ExecuteQuery(strSQl, parameters);
                                if (result == 0)
                                {
                                    objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkRequest, SystemEnum.ModuleEvent.JobRequestJobStatusChange, workRequestNo);
                                }
                            }
                        }

                        //(Start)Added by Pratik T on 19-Jan-2017 to Enter Audit Data if Required
                        if (autoStatusAudit && !string.IsNullOrEmpty(moduleName) && workStatusID != oldWorkStatusId)
                        {
                            NewJOStatusAudit(workorderNo, workStatusID, ProjectSession.EmployeeID, moduleName);
                        }
                        //(End)Added by Pratik T on 19-Jan-2017 to Enter Audit Data if Required

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }


        public void ChangeJobOrderModificationDateTimeByWorkorderNo(string workorderNo, string modifiedBy)
        {
            try
            {
                
                string workRequestNo = string.Empty;
                
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }
                string strSQl = "Update workorders set ModifiedBy = '" + Common.setQuote(modifiedBy) + "' , ModifiedDate = @ModifiedDate where WorkorderNo = '" + Common.setQuote(workorderNo) + "'";
                using (ServiceContext objContext = new ServiceContext())
                {                    
                    objContext.ExecuteQuery(strSQl, parameters);                    
                }
            }
            catch (Exception ex)
            { throw ex; }
        }



        /// <summary>
        /// Gets the jo page with out load.
        /// </summary>
        /// <param name="strWhere">The string where.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<WorkOrder> GetJOPageWithOutLoad(string strWhere, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkOrder> list = new List<WorkOrder>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;

                string query = "Select workorders.workorderno,worequest.requestno,ProblemDescription,workstatus.WorkStatus, " +
                                " workstatus.AltWorkStatus, worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,workpriority.WorkPriority, " +
                                " workpriority.AltWorkPriority,workorders.PMTarStartDate, asset.AssetNumber, asset.AssetDescription,asset.AssetAltDescription, cleaningInspectionGroup.GroupNumber, " +
                                " cleaningInspectionGroup.GroupDescription, cleaningInspectionGroup.ArabicGroupDescription, " +
                                " workorders.LocationID,location.LocationNo,location.LocationDescription,location.LocationAltDescription,L2.L2Code, " +
                                " Sector.SectorCode,Sector.SectorName,Sector.SectorAltName, " +
                                " L2Classes.L2ClassCode,L2Classes.L2Class,L2Classes.AltL2Class, " +
                                " L3.L3No, L3.L3Desc,L3.L3AltDesc,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description, L5.L5AltDescription, " +
                                " MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, " +
                                " workorders.ActionTaken,workorders.DateReceived,workorders.DateRequired, workorders.ProjectNumber, " +
                                " workorders.ActDateEnd,workorders.EstDateStart,workorders.AssignTo,MaintGroup.GroupCode, " +
                                " failurecause.FailureCauseCode,(case isnull(workorders.meterid,0) when 0 then  pmschedule.PMNo else pmmeters.MeterNo end) as PMNo, " +
                                " pmchecklist.ChecklistNo,employees.Name " +
                                " from workorders inner join workstatus on workorders.WorkStatusID=WorkStatus.WorkStatusID  " +
                                " inner join worktype on worktype.WorkTypeID = workorders.WorkTypeID  " +
                                " inner join workpriority on workpriority.WorkPriorityID = workorders.WorkPriorityID   " +
                                " LEFT OUTER JOIN worequest ON worequest.WorkOrderNo = workorders.WorkOrderNo   " +
                                " LEFT OUTER JOIN assets asset ON asset.assetid = workorders.assetid   " +
                                " LEFT OUTER JOIN cleaningInspectionGroup  ON cleaningInspectionGroup.CleaningInspectionGroupId  = workorders.CIGID   " +
                                " LEFT OUTER JOIN location ON location.locationid = workorders.locationid   " +
                                " Inner JOIN L2 ON L2.L2ID = workorders.L2ID Inner JOIN Sector ON Sector.SectorID = L2.SectorID  " +
                                " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID  " +
                                " left outer join L3 on L3.L3ID = workorders.L3ID   " +
                                " left outer join L4 on L4.L4ID = workorders.L4ID   " +
                                " LEFT OUTER JOIN l5 ON l5.l5id = workorders.l5id  " +
                                " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID= workorders.MaintDivisionID  " +
                                " inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = workorders.maintdeptID   " +
                                " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = workorders.maintsubdeptID  " +
                                " left outer join MaintGroup on MaintGroup.GroupID = workorders.groupID  " +
                                " left outer join failurecause on failurecause.FailureCauseID = workorders.FailureCauseID   " +
                                " left outer join pmschedule on pmschedule.PMID = workorders.PMID   " +
                                " left outer join pmmeters on pmmeters.meterid = workorders.meterid   " +
                                " left outer join pmchecklist on pmchecklist.ChecklistID = workorders.PMChecklistID   " +
                                " left outer join employees on employees.EmployeeID = workorders.RequestorID  " +
                                " where 1=1 " + strWhere;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query = "Select * from (" + query + ") A Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrder>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool IsJOValidToClose(string workorderNo)
        {
            try
            {
                string UnApprovedCount = "0";
                string workRequestNo = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strSQl = "Select count(1) As UnApprovedPRCount from PurchaseRequestItems PRI inner join PurchaseRequest PR ON ";
                strSQl += "PRI.PurchaseRequestID = PR.id AND PRI.JoborderID = '" + Common.setQuote(workorderNo) + "' AND PR.auth_status != 3 AND PR.status_id != 2 And status_id != 3";

                using (ServiceContext objContext = new ServiceContext())
                {
                    UnApprovedCount = objContext.ExecuteScalar(strSQl);

                    if (!string.IsNullOrEmpty(UnApprovedCount))
                    {
                        if (Convert.ToInt32(UnApprovedCount) > 0)
                        {
                            return false;
                        }                        
                        else
                        {
                            UnApprovedCount = "0";
                            strSQl = "select count(1) as UnApprovedCount from Material_req_master MRM inner join material_req_details MRD on MRM.mr_no=MRD.mr_no and MRD.WorkorderNo='" + Common.setQuote(workorderNo) + "' and MRM.status_id !=2 and MRM.status_id != 3";
                            UnApprovedCount = objContext.ExecuteScalar(strSQl);
                            if (Convert.ToInt32(UnApprovedCount) > 0)
                            {
                                return false;
                            }
                            else
                            {
                                UnApprovedCount = "0";
                                strSQl = "Select count(1) As UnApprovedPOCount from PurchaseOrderItems POI inner join PurchaseOrder PO ON POI.PurchaseOrderID = PO.id AND POI.JobOrderID = '" + Common.setQuote(workorderNo) + "'  AND PO.OrderStatus != 'Authorised' AND PO.OrderStatus != 'Closed' AND PO.OrderStatus != 'Cancelled' ";
                                UnApprovedCount = objContext.ExecuteScalar(strSQl);
                                if (Convert.ToInt32(UnApprovedCount) > 0)
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion

        #region "JobOrderStatusAudit"

        public int NewJOStatusAudit(string WorkorderNo, int WorkStatusID, int CreatedBy, string ModuleName)
        {
            try
            {
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "WorkorderNo",
                    Value = WorkorderNo,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "WorkStatusID",
                    Value = WorkStatusID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "ModuleName",
                    Value = ModuleName,
                    DBType = DbType.Int32
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "INSERT INTO WOStatusAudit(WONo , WOStatus , DATE_ , EmpID , source) " + " VALUES(@WorkorderNo,@WorkStatusID , @CurrentDate , '" + CreatedBy + "' ,@ModuleName)";
                }
                else
                {
                    query = "INSERT INTO WOStatusAudit(WONo , WOStatus , Date, EmpID , source) " + " VALUES(@WorkorderNo,@WorkStatusID , @CurrentDate , '" + CreatedBy + "' ,@ModuleName)";
                }


                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }

                return 1;
                // Successfully
            }
            catch
            {
                return 0;
                // UnSuccessfully
            }
        }

        #endregion

        #region "Assign TO Tab"

        public IList<Assigntoworkorder> GetAssignToJOByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Assigntoworkorder> list = new List<Assigntoworkorder>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;
                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And assigntoworkorder.WorkOrderno = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "select assigntoworkorder.EmployeeID,employees.EmployeeNo,employees.Name,assigntoworkorder.EstStartDate,assigntoworkorder.EstEndDate,assigntoworkorder.WorkorderNo";
                query += " from assigntoworkorder Inner Join employees On assigntoworkorder.EmployeeID = employees.EmployeeID " + strWhere;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Assigntoworkorder>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool NewAssignToJO(Assigntoworkorder objAssignToWo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (objAssignToWo.EmployeeID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        if (DeleteAssignToJOByWorkOrderNo(objAssignToWo))
                        {
                            if (objAssignToWo.EstStartDate != null && objAssignToWo.EstEndDate != null)
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "EmployeeID",
                                    Value = objAssignToWo.EmployeeID,
                                    DBType = DbType.Int32
                                });
                                parameters.Add(new DBParameters()
                                {
                                    Name = "WorkorderNo",
                                    Value = objAssignToWo.WorkorderNo,
                                    DBType = DbType.String
                                });

                                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "EstStartDate",
                                        Value = objAssignToWo.EstStartDate.Value,
                                        DBType = DbType.DateTime
                                    });
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "EstEndDate",
                                        Value = objAssignToWo.EstEndDate.Value,
                                        DBType = DbType.DateTime
                                    });
                                }
                                else
                                {
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "EstStartDate",
                                        Value = Common.GetEnglishDate(objAssignToWo.EstStartDate.Value),
                                        DBType = DbType.DateTime
                                    });
                                    parameters.Add(new DBParameters()
                                    {
                                        Name = "EstEndDate",
                                        Value = Common.GetEnglishDate(objAssignToWo.EstEndDate.Value),
                                        DBType = DbType.DateTime
                                    });
                                }


                                query = "Insert Into assigntoworkorder (EmployeeID,WorkorderNo,EstStartDate,EstEndDate) Values (@EmployeeID,@WorkorderNo,@EstStartDate,@EstEndDate)";

                            }
                            else
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "EmployeeID",
                                    Value = objAssignToWo.EmployeeID,
                                    DBType = DbType.Int32
                                });
                                parameters.Add(new DBParameters()
                                {
                                    Name = "WorkorderNo",
                                    Value = objAssignToWo.WorkorderNo,
                                    DBType = DbType.String
                                });
                                query = "Insert Into assigntoworkorder (EmployeeID,WorkorderNo) Values (@EmployeeID,@WorkorderNo)";
                            }

                            objContext.ExecuteQuery(query, parameters);
                        }
                    }
                }
                else if (objAssignToWo.MaintGroupID > 0)
                {
                    if (objAssignToWo.EstStartDate != null && objAssignToWo.EstEndDate != null)
                    {
                        query = "Insert Into assigntoworkorder Select eg.EmployeeID,'" + Common.setQuote(objAssignToWo.WorkorderNo) + "',@WOEstStartDate,@WOEstEndDate from employeesmaintgroups eg inner join employees e on eg.EmployeeID = e.EmployeeID ";
                        query += " AND e.CategoryID = (Select  Case When ConfigValue is Not NULL THEN ConfigValue Else '' End As CategoryID from COnfigurations where ConfigKey = 'TechnicianCategory') AND MaintGroupId=" + objAssignToWo.MaintGroupID;
                        query += " And eg.EmployeeID Not In (Select Distinct EmployeeID from assigntoworkorder Where WorkorderNo= @WONO)";
                    }
                    else
                    {
                        query = "Insert Into assigntoworkorder Select eg.EmployeeID,'" + Common.setQuote(objAssignToWo.WorkorderNo) + "',NULL,NULL from employeesmaintgroups eg inner join employees e on eg.EmployeeID = e.EmployeeID ";
                        query += " AND e.CategoryID = (Select  Case When ConfigValue is Not NULL THEN ConfigValue Else '' End As CategoryID from COnfigurations where ConfigKey = 'TechnicianCategory') AND MaintGroupId=" + objAssignToWo.MaintGroupID;
                        query += " And eg.EmployeeID Not In (Select Distinct EmployeeID from assigntoworkorder Where WorkorderNo= @WONO)";
                    }


                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = objAssignToWo.WorkorderNo,
                        DBType = DbType.String
                    });
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "WOEstStartDate",
                            Value = objAssignToWo.EstStartDate,
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "WOEstEndDate",
                            Value = objAssignToWo.EstEndDate,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "WOEstStartDate",
                            Value = Common.GetEnglishDate(objAssignToWo.EstStartDate),
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "WOEstEndDate",
                            Value = Common.GetEnglishDate(objAssignToWo.EstEndDate),
                            DBType = DbType.DateTime
                        });
                    }


                    using (ServiceContext objContext = new ServiceContext())
                    {
                        if (objContext.ExecuteQuery(query, parameters) == 0)
                        {
                            query = "update workorders set GroupID = " + objAssignToWo.MaintGroupID.ToString() + " where WorkorderNo='" + Common.setQuote(objAssignToWo.WorkorderNo) + "'";
                            objContext.ExecuteQuery(query, parameters);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteAssignToJOByWorkOrderNo(Assigntoworkorder objAssignToWo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONO",
                    Value = objAssignToWo.WorkorderNo,
                    DBType = DbType.String
                });
                string query = string.Empty;
                if (objAssignToWo.EmployeeID > 0)
                {
                    query = "delete from assigntoworkorder where EmployeeID = " + objAssignToWo.EmployeeID + " And WorkOrderNo= @WONO";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateSupplierIDToWorkOrder(int supplierID, string workOrderNo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    if (supplierID > 0)
                    {
                        query = "update workorders set SupplierId = " + supplierID.ToString() + " where WorkorderNo=@WONO";
                    }
                    else
                    {
                        query = "update workorders set SupplierId = NULL where WorkorderNo=@WONO";
                    }
                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }

                    EmailConfigurationService objService = new EmailConfigurationService();
                    objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.Supplier, SystemEnum.ModuleEvent.Supplier, workOrderNo, string.Empty, string.Empty);

                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Assigntoworkorder GetSupplierInfo(string workOrderNo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                Assigntoworkorder objAssignTOWo = new Assigntoworkorder();
                string query = string.Empty;
                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    query = "Select SupplierID,SupplierNo,SupplierName,AltSupplierName from Suppliers where SupplierID = (select supplierid from workorders where WorkorderNo='" + Common.setQuote(workOrderNo) + "')";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objAssignTOWo = objContext.ExecuteQuery<Assigntoworkorder>(query, parameters).FirstOrDefault();
                    }
                }

                return objAssignTOWo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Asset GetAssetWarrentyInfo(string workOrderNo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                Asset objAsset = new Asset();
                string query = @"select WorkOrders.AssetID,Assets.AssetNumber, Assets.Warranty_ContractID,warrantycontract.Warranty_contract, Assets.Warranty_ContractExpiry, 
                                Assets.EmployeeID, e1.Name, l.EmployeeID As LocationAuthEmpID, e2.Name As LocationAuthEmployeeName  
                                from WorkOrders LEFT OUTER JOIN Assets On WorkOrders.AssetID = Assets.AssetID 
                                LEFT OUTER JOIN warrantycontract on Assets.Warranty_ContractID= warrantycontract.Warranty_ContractID
                                LEFt OUTER JOIN Location l on l.LocationID = WorkOrders.LocationID 
                                LEFT OUTER JOIN employees e1 on Assets.EmployeeID = e1.EmployeeID 
                                LEFT OUTER JOIN employees e2 on l.EmployeeID = e2.EmployeeID 
                                Where workOrders.WorkorderNo = '" + Common.setQuote(workOrderNo) + "'";
                using (ServiceContext objContext = new ServiceContext())
                {
                    objAsset = objContext.ExecuteQuery<Asset>(query, parameters).FirstOrDefault();
                }

                return objAsset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Labor Tab"

        public IList<Workorderlabor> GetLaborByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Workorderlabor> list = new List<Workorderlabor>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And workorderlabor.WorkOrderno = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "select workorderlabor.workOrderLaborID,workorderlabor.EmployeeID,employees.EmployeeNo,employees.Name,employees.AltName,";
                query += "workorderlabor.StartDate,workorderlabor.EndDate,workorderlabor.HourlySalary,workorderlabor.OverTime1,workorderlabor.OverTime2,workorderlabor.OverTime3,workorderlabor.TotHour,workorderlabor.TotCost";
                query += " from workorderlabor Inner Join employees On workorderlabor.EmployeeID = employees.EmployeeID " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Workorderlabor>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        //public bool NewLaborToJO(Workorderlabor objWOLabor)
        //{
        //    try
        //    {
        //        Collection<DBParameters> parameters = new Collection<DBParameters>();
        //        string query = string.Empty;
        //        if (objWOLabor.EmployeeID > 0)
        //        {
        //            using (ServiceContext objContext = new ServiceContext())
        //            {
        //                if (DeleteLaborToJOByWorkOrderNo(objWOLabor.WorkorderLaborID, objWOLabor.WorkorderNo))
        //                {
        //                    if (objWOLabor.StartDate != null && objWOLabor.EndDate != null)
        //                    {
        //                        query = "Insert Into workorderlabor (WorkorderNo,EmployeeID,HourlySalary,Comment,OverTime1,OverTime2,OverTime3,StartDate,EndDate,TotHour,TotCost,CreatedBy,CreatedDate) " +
        //                                "Values ('" + Common.setQuote(objWOLabor.WorkorderNo) + "',"+ objWOLabor.EmployeeID + "," + Common.GetEnglishDate(objWOLabor.EstStartDate.Value) + "','" + Common.GetEnglishDate(objWOLabor.EstEndDate.Value) + "')";

        //                    }
        //                    else
        //                    {
        //                        query = "Insert Into assigntoworkorder (EmployeeID,WorkorderNo) Values (" + objWOLabor.EmployeeID + ",'" + Common.setQuote(objWOLabor.WorkorderNo) + "')";
        //                    }

        //                    objContext.ExecuteQuery(query, parameters);
        //                }
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool DeleteLaborToJOByWorkOrderNo(int WorkOrderLaborID, string workOderNo)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                if (WorkOrderLaborID > 0)
                {
                    query = "delete from workorderlabor where WorkOrderLaborID = " + WorkOrderLaborID;
                }

                if (!string.IsNullOrEmpty(workOderNo))
                {
                    query += " And WorkOrderNo= @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOderNo,
                        DBType = DbType.String
                    });
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Items Tab"

        public IList<Issue> GetIssuesByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Issue> list = new List<Issue>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And issue.WONO = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "select IssueID,DateIssue,StockCode.StockNo,StockCode.StockDescription,AltStockDescription,SubStore.SubStoreCode,QtyIssue,Price3,";
                query += "(isnull(QtyIssue,0) * isnull(Price3,0)) As TotalPrice,";
                query += "StockCode.UOM ";
                query += " from issue Inner Join StockCode ";
                query += " on Issue.StockID = StockCode.StockID ";
                query += " inner join SubStore ";
                query += " on Issue.SubStoreID = SubStore.SubStoreId " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Issue>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<Return> GetReturnByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Return> list = new List<Return>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And R.WONO = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "select R.ReturnID,StockCode.StockNo,StockCode.StockDescription,AltStockDescription,SubStore.SubStoreCode,R.Qty,R.RetPrice,";
                query += "(isnull(R.Qty,0) * isnull(R.RetPrice,0)) As TotalPrice,";

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query += "R.Comment_,R.Date_";
                    query += " from return R Inner Join StockCode  ";
                }
                else
                {
                    query += "R.Comment,R.\"Date\" ";
                    query += " from \"return\" R Inner Join StockCode  ";
                }
                query += " on R.StockID = StockCode.StockID ";
                query += "inner join SubStore ";
                query += " on R.SubStoreID = SubStore.SubStoreId  " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Return>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<WorkOrderPPE> GetPPEByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkOrderPPE> list = new List<WorkOrderPPE>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WorkOrderPPE.WorkOrderNo = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "Select * from WorkOrderPPE where 1 = 1 " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrderPPE>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<WorkOrderTool> GetWOToolsByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkOrderTool> list = new List<WorkOrderTool>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WorkOrderTools.WorkOrderNo = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "Select * from WorkOrderTools where 1 = 1 " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrderTool>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<WorkOrderItem> GetWorkOrderItemsByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<WorkOrderItem> list = new List<WorkOrderItem>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WorkOrderItems.WorkOrderNo = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                }

                string query = "Select StockCode.StockNo,StockCode.StockDescription,AltStockDescription,";
                query += "WorkOrderItems.Qty,WorkOrderItems.SeqNo from WorkOrderItems  Inner JOIn StockCode On WorkOrderItems.StockID = StockCode.StockID " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }


                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrderItem>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool InsertWorkOrderItemsByPmCheckListID(string workOrderNo, int checkListID)
        {
            try
            {
                if (checkListID > 0)
                {
                    string queryDelete = "Delete from WorkOrderItems Where WorkOrderNo = @WONO";
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                    string query = "Insert Into WorkOrderItems Select PartID,'" + workOrderNo + "',(Select SeqNo from checklistelements where checklistelementid = checklistInv.checklistelementid),Qty From  checklistInv where CheckListID = " + checkListID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(queryDelete, parameters);
                        if (objContext.ExecuteQuery(query, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertWorkOrderToolsByPmCheckListID(string workOrderNo, int checkListID)
        {
            try
            {
                if (checkListID > 0)
                {
                    string queryDelete = "Delete from WorkOrderTools Where WorkOrderNo = @WONO";
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                    string query = "Insert Into WorkOrderTools Select '" + workOrderNo + "',SeqNo,ToolDescription,AltToolDescription,Quantity From  ChecklistTools where CheckListID = " + checkListID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(queryDelete, parameters);
                        if (objContext.ExecuteQuery(query, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertWorkOrderPPEByPmCheckListID(string workOrderNo, int checkListID)
        {
            try
            {
                if (checkListID > 0)
                {
                    string queryDelete = "Delete from WorkOrderPPE Where WorkOrderNo = @WONO";
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                    string query = "Insert Into WorkOrderPPE Select '" + workOrderNo + "',SeqNo,PPEDescription,AltPPEDescription From  ChecklistPPE where CheckListID = " + checkListID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(queryDelete, parameters);
                        if (objContext.ExecuteQuery(query, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<StockCode> GetStockCodePageWithBalance(int subStoreID, string strWhere, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<StockCode> list = new List<StockCode>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;
                string query = "select StockCode.StockID,sl.Balance As Balanace,StockCode.CriticalityID,Case When StockCode.CriticalityID = 0 Then 'No' When StockCode.CriticalityID = 1 Then 'Yes' Else '' End As Criticality,stockcode.AttrGroupID,stockcode.AttrSubID,stockcode.default_supplier_id,";
                query += " StockNo,StockDescription,  AltStockDescription,UOM,Status,Case When Status = 1 Then 'Active' When Status = 0 Then 'Not Active' When Status = 2 Then 'Disposed' Else ''  End As StatusText,Price2,Notes,LeadTime,AvgPrice,Specification,Manufacturer,ANV,";
                query += "StockCode.UOM,attrgroup.AttrName,attrgroup.AltAttrName, attrsubgroup.AttrSubName,attrsubgroup.AltAttrSubName ";
                query += " ,stockcode.CreatedBy,stockcode.CreatedDate,stockcode.ModifiedBy,stockcode.ModifiedDate FROM  stockcode ";
                //query += "left outer join (Select stock_id,sub_store_id,Balance from stockcode_levels where sub_store_id = " + subStoreID + ") sl on stockcode.StockID = sl.stock_id ";
                query += "left outer join (Select stock_id,sub_store_id,Balance from stockcode_levels) sl on stockcode.StockID = sl.stock_id ";
                query += "left outer join attrgroup  on stockcode.AttrGroupID = attrgroup.AttrID left outer Join attrsubgroup on stockcode.AttrSubID = attrsubgroup.AttrSubID  where 1=1 " + strWhere + " ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<StockCode>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public decimal GetStockBalance(int stockCodeID, int subStoreID)
        {
            string Query = "Select Sum(Balance) from stockcode_levels group by stock_id,sub_store_id Having stock_id  = " + stockCodeID + " And sub_store_id =" + subStoreID;
            string Balance = string.Empty;
            try
            {
                using (ServiceContext objContext = new ServiceContext())
                {
                    Balance = objContext.ExecuteScalar(Query);
                    return Convert.ToDecimal(Balance == string.Empty ? "0" : Balance);
                }
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region "Notes Tab"

        public bool UpdateNotesToWorkOrder(string workOrderNo, string notes)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    query = "update workorders set Notes = N'" + Common.setQuote(notes) + "' where WorkorderNo='" + Common.setQuote(workOrderNo) + "'";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "JOPlan Tab"

        public IList<Jobtask> GetJobTaskItemsByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Jobtask> list = new List<Jobtask>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;
                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WONO = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                }

                string query = "Select * from jobtask where 1=1" + strWhere;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }


                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Jobtask>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<TblJobPlan> GetJobPlansByEmployee(int employeeID, int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<TblJobPlan> list = new List<TblJobPlan>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;
                string strWhere = string.Empty;

                string query = "Select tblJobPlan.JobPlanID,tblJobPlan.JobPlanNo,tblJobPlan.JobPlanName,tblJobPlan.AltJobPlanName,tblJobPlan.L2Id from tblJobPlan inner join employees_L2 on tblJobPlan.L2Id = employees_L2.L2ID And employees_L2.empID = " + employeeID;
                query += " union Select tblJobPlan.JobPlanID,tblJobPlan.JobPlanNo,tblJobPlan.JobPlanName,tblJobPlan.AltJobPlanName,tblJobPlan.L2Id from tblJobPlan inner join employees on tblJobPlan.L2Id = employees.L2ID And employees.EmployeeID =" + employeeID;
                query += " Where 1 = 1 ";

                if (!string.IsNullOrEmpty(text))
                {
                    strWhere = " And (JobPlanNo like N'%" + text + "%' or JobPlanName like N'%" + text + "%' or AltJobPlanName like N'%" + text + "%')";
                    query += strWhere;
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<TblJobPlan>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool SaveWorkOrderJobPlanItem(string workOrderNo, List<Jobtask> lstJobPlanItems)
        {
            try
            {
                string queryDelete = "Delete from jobtask Where WoNo = @WONO";
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONO",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
                string queryInsert = string.Empty;

                Jobtask objJobTask = new Jobtask();
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(queryDelete, parameters);

                    lstJobPlanItems.ForEach(p =>
                            {
                                Collection<DBParameters> inParameters = new Collection<DBParameters>();
                                inParameters.Add(new DBParameters()
                                {
                                    Name = "CreatedDate",
                                    Value = p.CreatedDate,
                                    DBType = DbType.DateTime
                                });

                                inParameters.Add(new DBParameters()
                                {
                                    Name = "ModifiedDate",
                                    Value = p.ModifiedDate,
                                    DBType = DbType.DateTime
                                });

                                inParameters.Add(new DBParameters()
                                {
                                    Name = "AltDetails",
                                    Value = p.AltDetails,
                                    DBType = DbType.String
                                });

                                queryInsert = "Insert Into jobtask(WoNo,SeqId,Details,AltDetails,Remarks,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)"
                                        + " Values('" + Common.setQuote(workOrderNo) + "'," + p.SeqId + ",'" + Common.setQuote(p.Details) + "',@AltDetails,'" + Common.setQuote(p.Remarks) + "','" + Common.setQuote(p.CreatedBy) + "',@CreatedDate,'" + Common.setQuote(p.ModifiedBy) + "',@ModifiedDate)";
                                objContext.ExecuteQuery(queryInsert, inParameters);
                            }
                        );
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool CopyJobPlanItemsToWorkOrderNo(int jobPlanId, string workOrderNo)
        {
            try
            {
                string queryDelete = "Delete from jobtask Where WoNo = @WONO";
                string queryInsert = string.Empty;

                Jobtask objJobTask = new Jobtask();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONO",
                    Value = workOrderNo,
                    DBType = DbType.String
                });
                using (ServiceContext objContext = new ServiceContext())
                {
                    if (objContext.ExecuteQuery(queryDelete, parameters) == 0)
                    {
                        queryInsert = "Insert Into jobtask Select '" + Common.setQuote(workOrderNo) + "',SeqID,Description,AltDescription,NULL,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate from tblJobPlanItems";
                        queryInsert += " Where JobPlanID=" + jobPlanId;

                        if (objContext.ExecuteQuery(queryInsert, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        #region "Safety Instruction"

        public IList<SITask> GetSafetyInstructionByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<SITask> list = new List<SITask>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WONO = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                }

                string query = "Select * from SITask where 1=1" + strWhere;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<SITask>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<TblSafetyinstruction> GetSafetyInstrsByEmployee(int employeeID, int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<TblSafetyinstruction> list = new List<TblSafetyinstruction>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;
                string strWhereClause = string.Empty;
                string query = "Select tblSafetyinstruction.SafetyID,tblSafetyinstruction.SafetyNo,tblSafetyinstruction.SafetyName,tblSafetyinstruction.AltSafetyName,tblSafetyinstruction.L2Id  from tblSafetyinstruction inner join employees_L2 on tblSafetyinstruction.L2Id = employees_L2.L2ID And employees_L2.empID = " + employeeID;
                query += " union Select tblSafetyinstruction.SafetyID,tblSafetyinstruction.SafetyNo,tblSafetyinstruction.SafetyName,tblSafetyinstruction.AltSafetyName,tblSafetyinstruction.L2Id  from tblSafetyinstruction inner join employees on tblSafetyinstruction.L2Id = employees.L2ID And employees.EmployeeID =" + employeeID;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    strWhere = " where 1=1 and (S.SafetyNo like N'%" + text + "%' or S.SafetyName like N'%" + text + "%' or S.AltSafetyName like N'%" + text + "%')";

                    query = "Select * from (" + query + ") S " + strWhere + " ";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhere = " where 1=1 and (lower(S.SafetyNo) like N'%" + text + "%' or lower(S.SafetyName) like N'%" + text + "%' or lower(S.AltSafetyName) like N'%" + text + "%')";

                    query = "Select * from (" + query + ") S " + strWhere + " ";
                }
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<TblSafetyinstruction>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool SaveWorkOrderSafetyInstrItem(string workOrderNo, List<SITask> lstJobPlanItems)
        {
            try
            {
                string queryDelete = "Delete from SITask Where WoNo = @WONO";
                string queryInsert = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                Jobtask objJobTask = new Jobtask();
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(queryDelete, parameters);

                    lstJobPlanItems.ForEach(p =>
                    {
                        Collection<DBParameters> inParameters = new Collection<DBParameters>();
                        inParameters.Add(new DBParameters()
                        {
                            Name = "CreatedDate",
                            Value = p.CreatedDate,
                            DBType = DbType.DateTime
                        });

                        inParameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = p.ModifiedDate,
                            DBType = DbType.DateTime
                        });

                        inParameters.Add(new DBParameters()
                        {
                            Name = "AltDetails",
                            Value = p.AltDetails,
                            DBType = DbType.String
                        });

                        queryInsert = "Insert Into SITask(WoNo,SeqId,Details,AltDetails,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)"
                                + " Values('" + Common.setQuote(workOrderNo) + "'," + p.SeqId + ",'" + Common.setQuote(p.Details) + "',@AltDetails ,'" + Common.setQuote(p.CreatedBy) + "',@CreatedDate,'" + Common.setQuote(p.ModifiedBy) + "',@ModifiedDate )";
                        objContext.ExecuteQuery(queryInsert, inParameters);
                    }
                        );
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool CopySafetyInstrItemsToWorkOrder(int safetyId, string workOrderNo)
        {
            try
            {
                string queryDelete = "Delete from SITask Where WoNo = '" + Common.setQuote(workOrderNo) + "'";
                string queryInsert = string.Empty;

                Jobtask objJobTask = new Jobtask();
                using (ServiceContext objContext = new ServiceContext())
                {
                    if (objContext.ExecuteQuery(queryDelete, new Collection<DBParameters>()) == 0)
                    {
                        queryInsert = "Insert Into SITask Select '" + Common.setQuote(workOrderNo) + "',SeqID,Description,AltDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate from tblSafetyInstructionItems";
                        queryInsert += " Where SafetyID=" + safetyId;

                        if (objContext.ExecuteQuery(queryInsert, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion

        #region "Pm Check List Items Tab"
        public IList<Workorderelement> GetWorkOrderElementsByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<Workorderelement> list = new List<Workorderelement>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And WorkOrderNo = @WONO";

                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                }

                string query = "Select * from workorderelements where 1=1" + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Workorderelement>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool UpdateWorkOrderElementRemark(string workorderNo, int SeqNo, string Remarks)
        {
            string Query = string.Empty;
            try
            {
                Query = "Update workorderelements set Remarks='" + Common.setQuote(Remarks) + "',ModifiedBy= '" + ProjectSession.EmployeeID.ToString() + "',ModifiedDate = @ModifiedDate Where WorkorderNo = @WONO And SeqNo= " + SeqNo;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "WONO",
                    Value = workorderNo,
                    DBType = DbType.String
                });
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                using (ServiceContext objContext = new ServiceContext())
                {
                    if (objContext.ExecuteQuery(Query, parameters) == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertWorkOrderElementsByPmCheckListID(string workOrderNo, int checkListID)
        {
            try
            {
                if (checkListID > 0)
                {
                    string queryDelete = "Delete from workorderelements Where WorkOrderNo = @WONO";
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "WONO",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = DateTime.Now,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = Common.GetEnglishDate(DateTime.Now),
                            DBType = DbType.DateTime
                        });
                    }

                    string query = "Insert Into workorderelements Select '" + workOrderNo + "',SeqNo,TaskDesc,AltTaskDesc,NULL,'" + ProjectSession.EmployeeID.ToString() + "',@ModifiedDate ,NULL,NULL From  checklistelements where CheckListID = " + checkListID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(queryDelete, parameters);
                        if (objContext.ExecuteQuery(query, new Collection<DBParameters>()) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "JO Status Track Tab"

        public IList<JOStatusTrack> GetJOStatusTrackByWorkOrderNo(string workOrderNo, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<JOStatusTrack> list = new List<JOStatusTrack>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;


                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    strWhere = " And jst.WorkorderNo = @workOrderNo";

                    parameters.Add(new DBParameters()
                    {
                        Name = "workOrderNo",
                        Value = workOrderNo,
                        DBType = DbType.String
                    });
                }

                string query = "SELECT jst.JOStatusTrackID,jst.WorkorderNo,jst.Old_WorkStatusID,w.WorkStatus OldWorkStatus, w.AltWorkStatus OldAltWorkStatus,jst.New_WorkStatusID,w1.WorkStatus NewWorkStatus,w1.AltWorkStatus NewAltWorkStatus" +
                                " ,ISNULL(jst.Latitude,0) Latitude,ISNULL(jst.Longitude,0)  Longitude,jst.CreatedBy,jst.CreatedDate,e.Name,e.AltName  " +
                                " FROM JOStatusTrack jst " +
                                " LEFT JOIN workstatus w ON w.WorkStatusID = jst.Old_WorkStatusID " +
                                " LEFT JOIN workstatus w1 ON w1.WorkStatusID = jst.New_WorkStatusID " +
                                " LEFT JOIN employees e ON e.EmployeeID = jst.CreatedBy " +
                                " where 1=1 " + strWhere;

                string strWhereClause = string.Empty;
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<JOStatusTrack>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion

        public string GetNewJobOrderNo(int L2ID)
        {
            try
            {
                Int64 CurrentMinNo = Convert.ToInt64(Convert.ToString(L2ID).PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));
                Int64 WorkOrderno = 0;

                IList<WorkOrder> TempListJO = new List<WorkOrder>();
                string query = string.Empty;
                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    query = "SELECT Max(w.WorkorderNo) FROM workorders w WHERE w.WorkorderNo NOT LIKE '%PC%' AND w.WorkorderNo NOT LIKE '%PM%' AND w.WorkorderNo NOT LIKE '%PCM%' AND L2ID =" + L2ID;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "SELECT Max(w.WorkorderNo) FROM workorders w WHERE lower(w.WorkorderNo) NOT LIKE lower('%PC%') AND lower(w.WorkorderNo) NOT LIKE lower('%PM%') AND lower(w.WorkorderNo) NOT LIKE lower('%PCM%') AND L2ID =" + L2ID;
                }

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    string wo = objContext.ExecuteScalar(query);
                    if (!string.IsNullOrEmpty(wo))
                    { WorkOrderno = Convert.ToInt64(wo); }
                }

                if (WorkOrderno > CurrentMinNo)
                {
                    CurrentMinNo = WorkOrderno;
                }

                return Convert.ToString((CurrentMinNo + 1)).PadLeft(11, '0');
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool IsDateInWorkDaysAndTimeInWorkingHours(int cityID, DateTime dateSelected, DateTime timeSelected, out int reasonID)
        {
            string strSQL = string.Empty;
            bool result = true;

            try
            {
                List<WorkingDayHour> lstWorkingDayHours = new List<WorkingDayHour>();
                List<Holiday> lstHolidays = new List<Holiday>();
                WorkingDayHour objWorkingDayHour = new WorkingDayHour();
                Holiday objHoliday = new Holiday();
                int dayOfWeek = 0;
                reasonID = 0;

                using (ServiceContext objContext = new ServiceContext())
                {
                    lstHolidays = GetAllHolidaysByL2ID(cityID).ToList();

                    if (lstHolidays != null && lstHolidays.Count > 0 && lstHolidays.Any(o => o.HolidayDate.Date == dateSelected.Date))
                    {
                        result = false;
                        reasonID = 1;
                    }
                    else
                    {
                        lstWorkingDayHours = GetAllWorkingDayHoursByL2ID(cityID, true).ToList();

                        dayOfWeek = dateSelected.DayOfWeek.GetHashCode();
                        if (dayOfWeek == 0)
                        {
                            dayOfWeek = 7;
                        }

                        if (lstWorkingDayHours != null && lstWorkingDayHours.Count > 0 && lstWorkingDayHours.Any(o => o.DayID == dayOfWeek) == false)
                        {
                            result = false;
                            reasonID = 1;
                        }
                        else if (timeSelected != null && timeSelected != DateTime.MinValue)
                        {
                            objWorkingDayHour = lstWorkingDayHours.Where(o => o.StartTime != null && o.EndTime != null && o.StartTime != DateTime.MinValue && o.EndTime != DateTime.MinValue
                                                                            && o.DayID == dayOfWeek).FirstOrDefault();

                            if (objWorkingDayHour != null)
                            {
                                if (TimeSpan.Compare(timeSelected.TimeOfDay, Convert.ToDateTime(objWorkingDayHour.StartTime).TimeOfDay) == -1 ||
                                    TimeSpan.Compare(timeSelected.TimeOfDay, Convert.ToDateTime(objWorkingDayHour.EndTime).TimeOfDay) > 0)
                                {
                                    result = false;
                                    reasonID = 2;
                                }
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Holiday> GetAllHolidaysByL2ID(int cityID)
        {
            string strSQl = string.Empty;
            IList<Holiday> lstHolidays = new List<Holiday>();

            try
            {
                strSQl = "Select * from Holidays Where L2ID = " + cityID;

                using (ServiceContext objService = new ServiceContext())
                {
                    lstHolidays = objService.ExecuteQuery<Holiday>(strSQl, new Collection<DBParameters>());
                }

                return lstHolidays;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<WorkingDayHour> GetAllWorkingDayHoursByL2ID(int cityID, bool isWorkingDay)
        {
            string strSQl = string.Empty;
            IList<WorkingDayHour> lstHolidays = new List<WorkingDayHour>();

            try
            {
                strSQl = "Select * from WorkingDayHours Where WorkingDayID In (select WorkingDayID from WorkingDays where L2ID =" + cityID + ") AND IsWorkingDay = " + Convert.ToInt32(isWorkingDay);

                using (ServiceContext objService = new ServiceContext())
                {
                    lstHolidays = objService.ExecuteQuery<WorkingDayHour>(strSQl, new Collection<DBParameters>());
                }

                return lstHolidays;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "Reports"

        public IList<WorkOrder> GetWorkOrderListForReport(string workorders ="")
        {
            try
            {
                IList<WorkOrder> list = new List<WorkOrder>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                string sortExpression = "workorders.CreatedDate desc, workorders.workorderno";
                string sortDirection = "Descending";
                string strWhere = " and workorders.workstatusid <> 2 and workorders.workstatusid <> 3 ";

                if (!string.IsNullOrWhiteSpace(workorders))
                {
                    string swo = string.Empty;
                    foreach (string strwo in workorders.Split(','))
                    {
                        swo = swo + ",'" + strwo + "'";
                    }

                    strWhere += " and workorders.WorkorderNo in (" + swo.Trim(',') +" ) ";
                }


                string strAdvanceSearch = string.Empty;

                if (!ProjectSession.IsCentral)
                {
                    strWhere += " and workorders.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                    /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        strWhere += " and (workorders.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + " ) OR (workorders.L3ID IS NULL))";
                    }

                    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        strWhere += " and workorders.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                    }

                    /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                    strWhere += " and workorders.maintsubdeptID in (" + ProjectConfiguration.SUBDEPTIDLISTPERMISSIONWISE + ") ";
                }

                SearchFieldService objSearch = new SearchFieldService();
                WorkOrder objWorkOrder = new WorkOrder();

                //list = objSearch.AdvanceSearch(objWorkOrder, SystemEnum.Pages.JobOrder.GetHashCode(), ProjectSession.EmployeeID, 0, sortExpression, sortDirection, true, strWhere);

                if (ProjectSession.Culture == ProjectConfiguration.EnglishCultureCode)
                {
                    query = " Select workorders.WorkorderNo,worktrade.WorkTrade as WorkTrade,worktype.WorkTypeDescription,workstatus.WorkStatus,workpriority.WorkPriority,eCr.Name as CreatedBy,workorders.PMChecklistID, " +
                    " (Select Count(1) from WorkOrderElements Where WorkOrderNo = workorders.WorkorderNo ) As WorkOrderElementCount,"
                    + "sup.SupplierName,workorders.DateReceived,eReq.Name as RequestorName,workorders.DateRequired,eReq.WorkPhone,workorders.ProblemDescription,asset.NotesToTech As NoteToTechAsset,isnull(pmschedule.PMNo,'') as PMNo , failurecause.FailureCauseCode ,failurecause.FailureCauseDescription, " +
                    " Location.LocationNo, Location.LocationDescription, L2.L2Code, L3.L3No, asset.AssetNumber, asset.AssetDescription, L4.L4No, L5.L5No, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, " +
                    " MaintSubDept.MaintSubDeptCode, pmchecklist.ChecklistNo, pmchecklist.CheckListName,Location.NotetoTech As NoteToTechLocation  " +
                    " from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on worktype.WorkTypeID = workorders.WorkTypeID " +
                    " inner join workstatus on workstatus.WorkStatusID = workorders.WorkStatusID inner join workpriority on workpriority.WorkPriorityID = workorders.WorkPriorityID " +
                    " inner join Employees eCr on eCr.EmployeeId = workorders.createdBy left outer join suppliers sup on sup.SupplierId=workorders.SupplierId " +
                    " left outer join Employees eReq on eReq.EmployeeId=workorders.RequestorID left outer join assets asset on asset.AssetId=workorders.AssetId " +
                    " left outer join pmschedule on pmschedule.PMID = workorders.PMID left outer join failurecause on failurecause.FailureCauseID=workorders.FailureCauseID " +
                    " left outer join Location on Location.LocationId=workorders.LocationId inner join L2 on L2.L2Id=workorders.L2ID left outer join L3 on L3.L3Id=workorders.L3Id left outer join L4 on L4.L4Id=workorders.L4Id " +
                    " left outer join L5 on L5.L5Id=workorders.L5Id inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = workorders.MaintDivisionID inner join MaintenanceDepartment  on MaintenanceDepartment.maintDeptID = workorders.maintdeptID " +
                    " inner join MaintSubDept  on MaintSubDept.MaintSubDeptID=workorders.maintsubdeptID left outer join pmchecklist  on pmchecklist.ChecklistID=workorders.PMChecklistID " +
                    " where 1=1 ";
                }
                else
                {
                    query = "Select workorders.WorkorderNo,worktrade.AltWorkTrade as WorkTrade,worktype.AltWorkTypeDescription as WorkTypeDescription,workstatus.AltWorkStatus as WorkStatus,workpriority.AltWorkPriority as WorkPriority,eCr.AltName as CreatedBy,workorders.PMChecklistID," +
                        " (Select Count(1) from WorkOrderElements Where WorkOrderNo = workorders.WorkorderNo ) As WorkOrderElementCount," +
                        " sup.AltSupplierName as SupplierName,workorders.DateReceived,eReq.AltName as RequestorName,workorders.DateRequired,eReq.WorkPhone,workorders.ProblemDescription,asset.NotesToTech As NoteToTechAsset,isnull(pmschedule.PMNo,'') as PMNo,failurecause.FailureCauseCode,failurecause.FailureCauseAltDescription as FailureCauseDescription, " +
                        " Location.LocationNo,Location.LocationAltDescription as LocationDescription,L2.L2Code,L3.L3No,asset.AssetNumber,asset.AssetAltDescription as AssetDescription,L4.L4No,L5.L5No,MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode, " +
                        " MaintSubDept.MaintSubDeptCode,pmchecklist.ChecklistNo,pmchecklist.AltCheckListName as CheckListName,Location.NotetoTech As NoteToTechLocation  " +
                        " from workorders inner join worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join worktype on worktype.WorkTypeID = workorders.WorkTypeID " +
                        " inner join workstatus on workstatus.WorkStatusID = workorders.WorkStatusID inner join workpriority on workpriority.WorkPriorityID = workorders.WorkPriorityID " +
                        " inner join Employees eCr on eCr.EmployeeId = workorders.createdBy left outer join suppliers sup on sup.SupplierId=workorders.SupplierId " +
                        " left outer join Employees eReq on eReq.EmployeeId=workorders.RequestorID left outer join assets asset on asset.AssetId=workorders.AssetId " +
                        " left outer join pmschedule on pmschedule.PMID = workorders.PMID left outer join failurecause on failurecause.FailureCauseID=workorders.FailureCauseID " +
                        "left outer join Location on Location.LocationId=workorders.LocationId inner join L2 on L2.L2Id=workorders.L2ID left outer join L3 on L3.L3Id=workorders.L3Id left outer join L4 on L4.L4Id=workorders.L4Id " +
                        "left outer join L5 on L5.L5Id=workorders.L5Id inner join MainenanceDivision  on MainenanceDivision.MaintDivisionID = workorders.MaintDivisionID   inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = workorders.maintdeptID " +
                        "inner join MaintSubDept on MaintSubDept.MaintSubDeptID=workorders.maintsubdeptID left outer join pmchecklist on pmchecklist.ChecklistID=workorders.PMChecklistID " +
                        " where 1=1 ";
                }

                strAdvanceSearch = objSearch.GetWhereClause(SystemEnum.Pages.JobOrder.GetHashCode(), ProjectSession.EmployeeID, true, out parameters);

                if (!string.IsNullOrEmpty(strAdvanceSearch))
                {
                    strWhere += " AND " + strAdvanceSearch;
                }
                query += strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<WorkOrder>(query, parameters, 0);
                    }
                }

                if (list != null && list.Count > 0)
                {
                    foreach (WorkOrder objWO in list)
                    {
                        objWO.AssignTo = GetAssignedToEmployeeNamesByWONO(objWO.WorkorderNo);
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public string GetAssignedToEmployeeNamesByWONO(string woNo)
        {
            try
            {
                string namesToReturn = string.Empty;
                IList<string> listOfAssignedToNames = new List<string>();
                string query = "Select e.Name from assigntoworkorder aw inner join employees e on aw.EmployeeID = e.EmployeeID where aw.WorkorderNo = '" + Common.setQuote(woNo) + "'";
                using (DapperContext objDapperContext = new DapperContext())
                {
                    listOfAssignedToNames = objDapperContext.ExecuteQuery<string>(query, new Collection<DBParameters>());
                }

                if (listOfAssignedToNames != null && listOfAssignedToNames.Count > 0)
                {
                    foreach (string empName in listOfAssignedToNames)
                    {
                        namesToReturn += ", " + empName;
                    }
                }

                return namesToReturn.Trim(new Char[] { ',' });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
