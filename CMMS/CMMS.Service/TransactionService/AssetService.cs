﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class AssetService : DBExecute
    {
        public AssetService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }       

        public static void UpdateAssetDetail(int assetID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            string query = "Update PMSchedule set PMActive = 0  where AssetId = @AssetID";
            string query1 = "Update S set S.PMActive = 0 from PMMeters S inner join PMMeterMaster on PMMeterMaster.MeterMasterID = S.MeterMasterID Where PMMeterMaster.AssetID = @AssetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
                context.ExecuteQuery<Asset>(query1, parameters);
            }
        }

        public static void CheckAssetChildRefExist(int assetID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                AssetRelationship obj = new AssetRelationship();
                int childCount = context.SearchAll<AssetRelationship>(obj).Where(x => x.ChildId == assetID).ToList().Count();
                if (childCount > 0)
                {
                    string query = "Delete  from AssetRelationship Where AssetID = " + assetID;
                    context.ExecuteQuery<Asset>(query, parameters);
                }
            }
        }

        public static void DeleteAssets(int assetID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            string query = "Delete  from Assets Where AssetID = @AssetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
            }
        }

        #region "BOM List Tab"
        /// <summary>
        /// Updates the parts list identifier by asset identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="partsListID">The parts list identifier.</param>
        public static void UpdatePartsListIDByAssetID(int assetID, int? partsListID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "partsListID",
                Value = partsListID,
                DBType = DbType.Int32
            });

            string query = "Update Assets set PartsListID = @PartsListID where AssetID = @AssetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
            }
        }


        public static void ChangeAssetModificationDateTimeByAssetId(int assetID, string modifiedBy)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }
                string strSQl = "Update Assets set ModifiedBy = '" + Common.setQuote(modifiedBy) + "' , ModifiedDate = @ModifiedDate where AssetID = " + assetID ;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(strSQl, parameters);
                }
            }
            catch (Exception ex)
            { throw ex; }
        }


        /// <summary>
        /// Gets the asset detail by asset identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public static Asset GetAssetDetailByAssetID(int assetID)
        {
            Asset objAsset = new Asset();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });
            string query = "select ass.AssetID,  " +
                                    "ass.AssetNumber, " +
                                    "ass.AssetDescription, " +
                                    "ass.AssetAltDescription, " +
                                    "ass.ModelNumber, " +
                                    "ass.SerialNumber, " +
                                    "ass.Manufacturer,  " +
                                    "ass.Warranty_ContractExpiry, " +
                                    "ass.Warranty_ContractNotes, " +
                                    "ass.Comments, " +
                                    "ass.PurchasePrice, " +
                                    "ass.DateAcquired, " +
                                    "ass.DataDisposed, " +
                                    "ass.EstLife, " +
                                    "ass.NotesToTech, " +
                                    "ast.AssetStatusID, " +
                                    "ast.AssetStatusDesc, " +
                                    "l.LocationID, " +
                                    "l.LocationNo, " +
                                    "l.LocationDescription, " +
                                    "l.LocationAltDescription, " +
                                    "ac.AssetCatID, " +
                                    "ac.AssetCatCode, " +
                                    "assc.AssetSubCatID, " +
                                    "assc.AssetSubCatCode, " +
                                    "aco.AssetConditionID, " +
                                    "aco.AssetConditionCode, " +
                                    "acl.AssetClassCode, " +
                                    "acl.AssetClass, " +
                                    "acl.AltAssetClass, " +
                                    "l2.L1ID, " +
                                    "l2.L2ID, " +
                                    "l2.L2Code, " +
                                    "l2.L2Name, " +
                                    "l2.L2AltName, " +
                                    "L2Classes.L2ClassCode, " +
                                    "L3.L3No, " +
                                    "L3.L3Desc, " +
                                    "L3.L3AltDesc,	 " +
                                    "L4.L4No , " +
                                    "L4.L4Description, " +
                                    "L4.L4AltDescription, " +
                                    "L5.L5No, " +
                                    "L5.L5Description, " +
                                    "L5.L5AltDescription, " +
                                    "Sector.SectorCode, " +
                                    "Sector.SectorName, " +
                                    "Sector.SectorAltName, " +
                                    "s.SupplierID, " +
                                    "s.SupplierNo, " +
                                    "s.SupplierName, " +
                                    "s.AltSupplierName, " +
                                    "c.id , " +
                                    "wt.WorkTradeID, " +
                                    "e.EmployeeID, " +
                                    "e.EmployeeNO, " +
                                    "e.Name, " +
                                    "e.AltName, " +
                                    "wc.Warranty_contractID, " +
                                    "pl.PartsListID, " +
                                    "pl.PartsListNo, " +
                                    "pl.PartsListDescription, " +
                                    "pl.PartsListAltDescription " +
                                "from Assets ass " +
                                "INNER JOIN assetstatus ast on ast.AssetStatusID= ass.AssetStatusID  " +
                                "INNER JOIN location l ON l.locationid = ass.locationid " +
                                "LEFT JOIN assetcategories ac ON ac.AssetCatID =ass.AssetCatID   " +
                                "LEFT JOIN assetsubcategories assc ON assc.AssetSubCatID =ass.AssetSubCatID  " +
                                "LEFT JOIN assetconditions aco ON aco.AssetConditionID = ass.AssetConditionID   " +
                                "LEFT JOIN assetclasses acl ON acl.AssetClassID = ass.AssetClassID   " +
                                "Inner JOIN l2 ON l.l2id = l2.l2id  " +
                                "LEFT OUTER JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID  " +
                                "LEFT OUTER JOIN l3 ON l.l3id = l3.l3id   " +
                                "Inner JOIN Sector ON Sector.SectorID = L2.SectorID " +
                                "Inner JOIN  l4 ON l.l4id = l4.l4id " +
                                "LEFT  JOIN l5 ON l.l5id = l5.l5id  " +
                                "LEFT  JOIN suppliers s ON s.SupplierID = ass.SupplierID " +
                                "LEFT JOIN Criticality c on c.id = ass.CriticalityID " +
                                "LEFT JOIN worktrade wt on wt.WorkTradeID = ass.WorkTradeID " +
                                "LEFT JOIN employees e on e.EmployeeID = ass.EmployeeID " +
                                "LEFT JOIN WarrantyContract wc on wc.Warranty_contractID = ass.Warranty_contractID " +
                                "LEFT JOIN partslist pl on pl.PartsListID = ass.PartsListID " +
                                "Where ass.AssetID = @assetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                objAsset = context.ExecuteQuery<Asset>(query, parameters).FirstOrDefault();
            }

            if (objAsset != null)
            {
                if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                {
                    if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                    { objAsset.AssetWarrentyStatus = "ساري المفعول"; }
                    else
                    { objAsset.AssetWarrentyStatus = "Not Expired"; }
                }
                else
                {
                    if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                    { objAsset.AssetWarrentyStatus = "منتهية الصلاحية"; }
                    else
                    { objAsset.AssetWarrentyStatus = "Expired"; }
                }
            }

            return objAsset;
        }

        #endregion

        #region "RelationShip Tab"
        /// <summary>
        /// Gets the parent asset by asset identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public static CustomAssetRelationship GetParentAssetByAssetId(int assetID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });
            string query = "Select a.AssetID as ParentAssetID,a.AssetNumber as ParentAssetNumber,a.AssetAltDescription as ParentAssetAltDescription,a.AssetDescription as ParentAssetDescription,ast.AssetStatusDesc as ParentAssetStatusDesc,ast.AltAssetStatusDesc as ParentAltAssetStatusDesc " +
                            "from AssetRelationship ar " +
                            "inner join Assets a on ar.AssetID = a.AssetID " +
                            "left join AssetStatus ast on ast.AssetStatusID = a.AssetStatusID " +
                            "Where ChildId = @assetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<CustomAssetRelationship>(query, parameters).FirstOrDefault();
            }

        }

        /// <summary>
        /// Gets the asset page.
        /// </summary>
        /// <param name="empID">The emp identifier.</param>
        /// <param name="strWhere">The string where.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<Asset> GetAssetPage(int empID, string strWhere, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            string strWhereClauseWithEmployee = "";
            if (empID > 0)
            {
                strWhereClauseWithEmployee = " and l.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " )";
            }
            IList<Asset> list = new List<Asset>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "select  ass.AssetID, " +
                       "ass.AssetNumber, " +
                       "ass.AssetDescription, " +
                       "ass.AssetAltDescription,	 " +
                       "ass.ModelNumber, " +
                       "ass.SerialNumber, " +
                       "ass.Manufacturer, 	 " +
                       "ass.Warranty_ContractExpiry," +
                       "ast.AssetStatusDesc, " +
                       "ast.AltAssetStatusDesc," +
                       "ass.AssetStatusID, " +
                       "ass.NotesToTech, " +
                       "l.locationID, " +
                       "l.LocationNo, " +
                       "l.LocationDescription, " +
                       "l.LocationAltDescription, " +
                       "ac.AssetCatCode,	 " +
                       "l2.L2Code, " +
                       "l2.L2Name, " +
                       "l2.L2AltName, " +
                       "L4.L4No , " +
                       "L4.L4Description, " +
                       "L4.L4AltDescription, " +
                       "employees.Name, " +
                       "employees.AltName " +
           "FROM  assets ass " +
           "INNER JOIN assetstatus ast on ast.AssetStatusID= ass.AssetStatusID  " +
           "inner join location l on l.LocationID = ass.LocationID " +
           "left outer join L2 on l.L2ID = L2.L2ID         " +
           "left outer join L4 on l.L4ID = L4.L4ID	   " +
           "LEFT JOIN assetcategories ac ON ac.AssetCatID =ass.AssetCatID   " +
           "left outer join employees on ass.EmployeeID = employees.EmployeeID	   " +
           "where 1 = 1 " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Asset>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<Asset> GetAssetPageForPMAsset(int empID, string strWhere, int pageNo, string sortExpression, string sortDirection, string pmGroupNo, DateTime? startDate = null, DateTime? completedDate = null, DateTime? nextDate = null, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            string strWhereClauseWithEmployee = "";
            if (empID > 0)
            {
                strWhereClauseWithEmployee = " and l.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " )";
            }
            IList<Asset> list = new List<Asset>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "select  ass.AssetID, " +
                       "ass.AssetNumber, " +
                       "ass.AssetDescription, " +
                       "ass.AssetAltDescription,	 " +
                       "ass.ModelNumber, " +
                       "ass.SerialNumber, " +
                       "ass.Manufacturer, 	 " +
                       "ass.Warranty_ContractExpiry," +
                       "ast.AssetStatusDesc, " +
                       "ast.AltAssetStatusDesc," +
                       "ass.AssetStatusID, " +
                       "ass.NotesToTech, " +
                       "l.locationID, " +
                       "l.LocationNo, " +
                       "l.LocationDescription, " +
                       "l.LocationAltDescription, " +
                       "ac.AssetCatCode,	 " +
                       "l2.L2ID , " +
                       "l2.L2Code, " +
                       "l2.L2Name, " +
                       "l2.L2AltName, " +
                       "L4.L4No , " +
                       "L4.L4Description, " +
                       "L4.L4AltDescription, " +
                       "employees.Name, " +
                       "employees.AltName " +
                //"pmschedule.TargetStartDate,pmschedule.TargetCompDate, pmschedule.NextDate, " +
                //"CASE WHEN (pmschedule.AssetID is NULL) Then 0 ELSE 1  END AS IsInclude " +
           " FROM  assets ass " +
           "INNER JOIN assetstatus ast on ast.AssetStatusID= ass.AssetStatusID  " +
           "inner join location l on l.LocationID = ass.LocationID " +
           "left outer join L2 on l.L2ID = L2.L2ID         " +
           "left outer join L4 on l.L4ID = L4.L4ID	   " +
           "LEFT JOIN assetcategories ac ON ac.AssetCatID =ass.AssetCatID   " +
           "left outer join employees on ass.EmployeeID = employees.EmployeeID	   " +
                //" Left join pmschedule on pmschedule.AssetID = ass.AssetID and pmGroupNo = '" + pmGroupNo + "'" +
           " where 1 = 1 " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Asset>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            for (int i = 0; i < list.Count; i++)
            {
                list[i].TargetStartDate = startDate;
                list[i].TargetCompDate = completedDate;
                list[i].NextDate = nextDate;
            }

            return list;
        }

        public IList<Asset> GetAssetPageForPMAssetForEdit(int empID, string strWhere, int pageNo, string sortExpression, string sortDirection, string pmGroupNo, DateTime? startDate = null, DateTime? completedDate = null, DateTime? nextDate = null, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            string strWhereClauseWithEmployee = "";
            if (empID > 0)
            {
                strWhereClauseWithEmployee = " and l.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " )";
            }
            IList<Asset> list = new List<Asset>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "select  ass.AssetID, " +
                       "ass.AssetNumber, " +
                       "ass.AssetDescription, " +
                       "ass.AssetAltDescription,	 " +
                       "ass.ModelNumber, " +
                       "ass.SerialNumber, " +
                       "ass.Manufacturer, 	 " +
                       "ass.Warranty_ContractExpiry," +
                       "ast.AssetStatusDesc, " +
                       "ast.AltAssetStatusDesc," +
                       "ass.AssetStatusID, " +
                       "ass.NotesToTech, " +
                       "l.locationID, " +
                       "l.LocationNo, " +
                       "l.LocationDescription, " +
                       "l.LocationAltDescription, " +
                       "ac.AssetCatCode,	 " +
                       "l2.L2ID , " +
                       "l2.L2Code, " +
                       "l2.L2Name, " +
                       "l2.L2AltName, " +
                       "L4.L4No , " +
                       "L4.L4Description, " +
                       "L4.L4AltDescription, " +
                       "employees.Name, " +
                       "employees.AltName, " +
                "pmschedule.TargetStartDate,pmschedule.TargetCompDate, pmschedule.NextDate " +
                //"CASE WHEN (pmschedule.AssetID is NULL) Then 0 ELSE 1  END AS IsInclude " +
           " FROM  assets ass " +
           "INNER JOIN assetstatus ast on ast.AssetStatusID= ass.AssetStatusID  " +
           "inner join location l on l.LocationID = ass.LocationID " +
           "left outer join L2 on l.L2ID = L2.L2ID         " +
           "left outer join L4 on l.L4ID = L4.L4ID	   " +
           "LEFT JOIN assetcategories ac ON ac.AssetCatID =ass.AssetCatID   " +
           "left outer join employees on ass.EmployeeID = employees.EmployeeID	   " +
                " Left join pmschedule on pmschedule.AssetID = ass.AssetID and pmGroupNo = '" + pmGroupNo + "'" +
           " where 1 = 1 " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Asset>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            PmSchedule obj = new PmSchedule();
            obj.PMGroupNo = pmGroupNo;
            obj.IsCleaningModule = false;
            using (DapperContext context = new DapperContext())
            {
                obj = context.Search<PmSchedule>(obj).FirstOrDefault();
            }

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].TargetStartDate == null || list[i].TargetStartDate == DateTime.MinValue)
                {
                    list[i].TargetStartDate = obj.TargetStartDate;
                    list[i].TargetCompDate = obj.TargetCompDate;
                    list[i].NextDate = obj.NextDate;
                }
            }

            return list;
        }

        /// <summary>
        /// Gets the asset page.
        /// </summary>
        /// <param name="empID">The emp identifier.</param>
        /// <param name="strWhere">The string where.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<Asset> GetAssetPageWithAllRelatedData(int empID, string strWhere, int pageNo, string sortExpression, string sortDirection, int assetID = 0, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            string strWhereClauseWithEmployee = "";
            if (empID > 0)
            {
                strWhereClauseWithEmployee = " and l.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " )";
            }
            IList<Asset> list = new List<Asset>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string strAreaJoins = " left outer join L3 on l.L3ID = L3.L3ID ";
            string strAreaColumns = " L3.L3ID,";

            string query = "select  ass.AssetID, " +
                       "ass.AssetNumber, " +
                       "ass.AssetDescription, " +
                       "ass.AssetAltDescription,	 " +
                       "ass.ModelNumber, " +
                       "ass.SerialNumber, " +
                       "ass.Manufacturer, 	 " +
                       "ass.Warranty_ContractExpiry," +
                       "ass.Warranty_ContractID," +
                       "wct.Warranty_contract," +
                       "wct.AltWarranty_contract," +
                       "ast.AssetStatusDesc, " +
                       "ast.AltAssetStatusDesc," +
                       "ass.AssetStatusID, " +
                       "ass.NotesToTech, " +
                       "ass.LocationID," +
                       "l.LocationNo, " +
                       "l.NoteToTech," +
                       "l.LocationDescription, " +
                       "l.LocationAltDescription, " +
                       "ac.AssetCatCode,	 " +
                       "l2.l1ID, " +
                       "l2.L2ID, " +
                       "l2.L2Code, " +
                       "l2.L2Name, " +
                       (ProjectSession.IsAreaFieldsVisible ? strAreaColumns : string.Empty) +
                       "l.L4ID," +
                       "l.L5ID," +
                       "L5.L5No," +
                       "L5.L5Description," +
                       "L5.L5AltDescription," +
                       "l2.L2AltName, " +
                       "L4.L4No , " +
                       "L4.L4Description, " +
                       "L4.L4AltDescription, " +
                       "employees.Name, " +
                       "employees.AltName, " +
                       "e2.Name As LocationAuthEmployeeName," +
                       "ass.EmployeeID " +
           "FROM  assets ass " +
           "INNER JOIN assetstatus ast on ast.AssetStatusID= ass.AssetStatusID  " +
           "inner join location l on l.LocationID = ass.LocationID " +
           "left outer join L2 on l.L2ID = L2.L2ID " +
           "left outer join L4 on l.L4ID = L4.L4ID " +
           (ProjectSession.IsAreaFieldsVisible ? strAreaJoins : string.Empty) +
           "LEFT JOIN assetcategories ac ON ac.AssetCatID =ass.AssetCatID   " +
           "left outer join employees on ass.EmployeeID = employees.EmployeeID	   " +
           "left outer join employees e2 on l.EmployeeID = e2.EmployeeID	   " +
           "left outer join L5 on l.L5ID = L5.L5ID	   " +
           "LEFT OUTER JOIN warrantycontract wct on ass.Warranty_ContractID= wct.Warranty_ContractID  " +
           "where 1 = 1 " + strWhere + strWhereClauseWithEmployee;

            if (assetID > 0)
            {
                parameters.Add(new DBParameters()
                {
                    Name = "assetID",
                    Value = assetID,
                    DBType = DbType.Int32
                });

                query += " AND ass.AssetID=@assetID";
            }

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Asset>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }
            return list;
        }

        /// <summary>
        /// Gets the asset childs.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<CustomAssetRelationship> GetAssetChilds(int assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<CustomAssetRelationship> list = new List<CustomAssetRelationship>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select a.AssetID,a.AssetNumber,a.AssetAltDescription,a.AssetDescription,ast.AssetStatusDesc,ast.AltAssetStatusDesc,a.AssetStatusID " +
                            "from AssetRelationship ar " +
                            "inner join Assets a on ar.ChildId = a.AssetID " +
                            "left join AssetStatus ast on ast.AssetStatusID = a.AssetStatusID " +
                             "Where ar.AssetID = " + assetID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<CustomAssetRelationship>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Recursives the parent child exists.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="childID">The child identifier.</param>
        /// <returns></returns>
        public static bool RecursiveParentChildExists(int assetID, int childID)
        {
            using (ServiceContext context = new ServiceContext())
            {
                AssetRelationship obj = new AssetRelationship();
                obj = context.SearchAll<AssetRelationship>(obj).Where(x => x.ChildId == assetID).FirstOrDefault();
                if (obj != null)
                {
                    int ObjecAssetRelactionAssetId = obj.AssetID;
                    if (ObjecAssetRelactionAssetId > 0)
                    {
                        if (ObjecAssetRelactionAssetId == childID)
                        {
                            return false;
                        }
                        else
                        {
                            return RecursiveParentChildExists(ObjecAssetRelactionAssetId, childID);
                        }
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Checks the child already exists.
        /// </summary>
        /// <param name="childID">The child identifier.</param>
        /// <returns></returns>
        public static bool CheckChildAlreadyExists(int childID)
        {
            using (ServiceContext context = new ServiceContext())
            {
                AssetRelationship obj = new AssetRelationship();
                obj = context.SearchAll<AssetRelationship>(obj).Where(x => x.ChildId == childID).FirstOrDefault();
                if (obj != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Deletes the child asset.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="childID">The child identifier.</param>
        public static void DeleteChildAsset(int assetID, int? childID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "childID",
                Value = childID,
                DBType = DbType.Int32
            });

            string query = "Delete  from AssetRelationship Where AssetID = @AssetID and ChildId = @ChildId";
            //string query = "Update Assets set PartsListID = @PartsListID where AssetID = @AssetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
            }
        }

        /// <summary>
        /// Adds the asset child.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="childID">The child identifier.</param>
        public static void AddAssetChild(int assetID, int? childID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "childID",
                Value = childID,
                DBType = DbType.Int32
            });

            string query = " insert into AssetRelationship (AssetID,ChildId) values(@AssetID,@ChildId)";

            using (DapperDBContext context = new DapperDBContext())
            {
                context.ExecuteQuery<Asset>(query, parameters);
            }
        }

        #endregion

        #region "Asset Tranfer Tab"
        /// <summary>
        /// Gets the asset detail.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public static Asset GetAssetDetail(int assetID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });
            string query = "select a.AssetID, a.AssetNumber,a.AssetDescription,a.AssetAltDescription, " +
                            "a.LocationID,l.LocationDescription,l.LocationAltDescription,l.L2ID, L2.L2name,L2.L2Altname  " +
                            "from Assets a " +
                            "INNER JOIN Location l on l.LocationID = a.LocationID " +
                            "INNER JOIN L2 on L2.L2ID = l.L2ID " +
                            "Where AssetID = @assetID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<Asset>(query, parameters).FirstOrDefault();
            }
        }

        /// <summary>
        /// Gets the asset transfer list.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<AssetsTransfer> GetAssetTransferList(int assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<AssetsTransfer> list = new List<AssetsTransfer>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "Select at.*,L2.L2Name,L2.L2AltName,l.LocationDescription,l.LocationAltDescription,a.AssetAltDescription,a.AssetDescription " +
                            "from assetstransfer at " +
                            "LEFT JOIN L2 on L2.L2ID = at.L2ID  " +
                            "LEFT JOIN location l on l.LocationID = at.LocationID " +
                            "LEFT JOIN Assets a on a.AssetID = at.AssetID " +
                             " where at.AssetID = " + assetID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<AssetsTransfer>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Checks for outstanding w os.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public string checkForOutstandingWOs(int assetID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strAssetIds = string.Empty;
                string workorderNos = string.Empty;
                GetAllChildAssetIds(assetID, ref strAssetIds);
                string strAsset;
                strAsset = (assetID + "," + strAssetIds);

                //string[] strAsset;
                //strAsset = (assetID + "," + strAssetIds).Split(',');

                //  List<string> strTest = strAsset.ToList();

                string query = "select wo.* from WorkOrders wo " +
                               "where (WorkStatusID != 2 and WorkStatusID != 3) and AssetID in ( " + strAsset.TrimEnd(',') + " )";

                WorkOrder obj = new WorkOrder();
                using (ServiceContext context = new ServiceContext())
                {
                    //List<WorkOrder> lstWorkOrder = context.SearchAll(obj).ToList();
                    List<WorkOrder> lst = context.ExecuteQuery<WorkOrder>(query, parameters).ToList();
                    //dynamic Query = (from c in lstWorkOrder where strAsset.Contains(c.AssetID.Value == null ? "" : c.AssetID.Value.ToString()) & (c.WorkStatusID != 2 & c.WorkStatusID != 3) select c.WorkorderNo).ToList();
                    //IList<WorkOrder> lst = (from c in context.SearchAll(obj) where strAsset.Contains(c.AssetID.ToString()) && (c.WorkStatusID != 2 && c.WorkStatusID != 3) select c).ToList();
                    //List<WorkOrder> lst = context.SearchAll<WorkOrder>(obj).Where(x => x.WorkStatusID != 2 && x.WorkStatusID != 3 && strAsset.Contains(x.AssetID.Value.ToString())).ToList();
                    //List<WorkOrder> lst = context.SearchAll<WorkOrder>(obj).Where(x => x.WorkStatusID != 2 && x.WorkStatusID != 3 && strTest.Any(x.AssetID.ToString().Contains)).ToList();
                    if (lst.Count > 0)
                    {
                        for (int i = 0; i < lst.Count; i++)
                        {
                            workorderNos = workorderNos + lst[i].WorkorderNo + "<br/>";
                        }
                    }
                    return workorderNos;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Gets all child asset ids.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="strAssetIds">The string asset ids.</param>
        /// <returns></returns>
        public string GetAllChildAssetIds(int assetID, ref string strAssetIds)
        {
            using (ServiceContext context = new ServiceContext())
            {
                AssetRelationship obj = new AssetRelationship();
                List<AssetRelationship> lst = context.SearchAll<AssetRelationship>(obj).Where(x => x.AssetID == assetID).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        strAssetIds = strAssetIds + lst[i].ChildId + ",";
                        GetAllChildAssetIds(lst[i].ChildId, ref strAssetIds);
                    }
                }
                return strAssetIds;
            }
        }

        /// <summary>
        /// Checks for pm schedule exists.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public string CheckForPMScheduleExists(int assetID)
        {
            string strAssetIds = string.Empty;
            string PMNos = string.Empty;
            GetAllChildAssetIds(assetID, ref strAssetIds);
            string strAsset;
            strAsset = (assetID + "," + strAssetIds);

            //string[] strAsset;
            //strAsset = (assetID + "," + strAssetIds).Split(',');
            PmSchedule objPmSchedule = new PmSchedule();
            PmMeter objPmMeter = new PmMeter();
            PMMeterMaster objPMMaster = new PMMeterMaster();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                string query = "select * from PmSchedule " +
                                 "where PMActive = 1 and AssetID in ( " + strAsset.TrimEnd(',') + " )";

                List<PmSchedule> lst = context.ExecuteQuery<PmSchedule>(query, parameters).ToList();

                //List<PmSchedule> lst = context.SearchAll(objPmSchedule).Where(x => x.PMActive == 1 && strAsset.Contains(assetID.ToString())).ToList();

                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        PMNos = PMNos + lst[i].PMNo + Environment.NewLine;
                    }
                }

                string query1 = "select pm.* from pmmeters pm inner join PMMeterMaster pmm on pm.MeterMasterID = pmm.MeterMasterID " +
                                 "where PMActive = 1 and AssetID in ( " + strAsset.TrimEnd(',') + " )";

                List<PmMeter> lstPMMeter = context.ExecuteQuery<PmMeter>(query, parameters).ToList();

                //List<PmMeter> lstPMMeter = (from c in context.SearchAll(objPmMeter)
                //                            join d in context.SearchAll(objPMMaster)
                //                            on c.MeterMasterID equals d.MeterMasterID
                //                            where c.PMActive == 1 && strAsset.Contains(assetID.ToString())
                //                            select c).ToList();

                if (lstPMMeter.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        PMNos = PMNos + lst[i].PMNo + Environment.NewLine;
                    }
                }
                return PMNos;
            }
        }

        /// <summary>
        /// Saves the assets by location identifier.
        /// </summary>
        /// <param name="currentAsset">The current asset.</param>
        public void SaveAssetsByLocationId(Asset currentAsset)
        {
            try
            {
                string query = string.Empty;
                string query1 = string.Empty;
                string query2 = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                     query = "Update Assets set LocationId = " + currentAsset.LocationID + " where AssetId = " + currentAsset.AssetID;
                }
                else 
                {
                    query = "Update Assets set LocationId = " + currentAsset.LocationID + " where AssetId = " + currentAsset.AssetID;
                }
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query1 = "MERGE INTO PMSchedule S USING (SELECT S.ROWID row_id," + currentAsset.LocationID + ",Location.L2Id FROM PMSchedule S JOIN Location ON Location.LocationID = " + currentAsset.LocationID + " WHERE AssetId = " + currentAsset.AssetID + ") src ON ( S.ROWID     = src.row_id )WHEN MATCHED THEN UPDATE SET S.PhyLocationID = " + currentAsset.LocationID + ", S.L2Id = src.L2Id";
                }
                else
                {
                    query1 = "Update S set  S.PhyLocationID = " + currentAsset.LocationID + " , S.L2Id=Location.L2Id From PMSchedule S inner join Location on Location.LocationID = " + currentAsset.LocationID + "  Where AssetId=  " + currentAsset.AssetID;
                }
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                     query2 = "MERGE INTO PMMeterMaster S USING (SELECT S.ROWID row_id,Location.L2Id FROM PMMeterMaster S JOIN Assets ON S.AssetId = Assets.AssetId JOIN Location ON Location.LocationId = Assets.LocationID WHERE S.AssetId = " + currentAsset.AssetID + ") src ON (S.ROWID = src.row_id) WHEN MATCHED THEN UPDATE SET S.L2Id = src.L2Id";
                }
                else 
                {
                    query2 = "Update S Set S.L2Id = Location.L2Id From PMMeterMaster S inner join Assets on S.AssetId = Assets.AssetId inner join Location on Location.LocationId = Assets.LocationID Where S.AssetId= " + currentAsset.AssetID;
                }
                

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                    context.ExecuteQuery<Asset>(query1, parameters);
                    context.ExecuteQuery<Asset>(query2, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// News the assets transfer for n level.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void NewAssetsTransferForNLevel(AssetsTransfer obj)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "L2ID",
                    Value = obj.L2ID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "LocationID",
                    Value = obj.LocationID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "AssetID",
                    Value = obj.AssetID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "ParentAssetID",
                    Value = obj.ParentAssetID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "Remarks",
                    Value = obj.Remarks,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                 {
                     Name = "CreatedBy",
                     Value = obj.CreatedBy,
                     DBType = DbType.String
                 });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "TransferDate",
                        Value = obj.TransferDate,
                        DBType = DbType.DateTime
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = obj.CreatedDate,
                        DBType = DbType.DateTime

                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "TransferDate",
                        Value = Common.GetEnglishDate(obj.TransferDate),
                        DBType = DbType.DateTime
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = Common.GetEnglishDate(obj.CreatedDate),
                        DBType = DbType.DateTime

                    });
                }


                if (Convert.ToInt32(obj.ParentAssetID) != 0)
                {
                    string query = "INSERT INTO assetstransfer(L2ID,LocationID,AssetID,ParentAssetID,Remarks,TransferDate,CreatedBy,CreatedDate) VALUES(" +
                                    "@L2ID ,@LocationID ,@AssetID ,@ParentAssetID,@Remarks ,@TransferDate ,@CreatedBy ,@CreatedDate )";
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        context.ExecuteQuery<Asset>(query, parameters);
                    }
                }
                else
                {
                    string query = "INSERT INTO assetstransfer(L2ID,LocationID,AssetID,Remarks,TransferDate,CreatedBy,CreatedDate) VALUES(" +
                                    "@L2ID ,@LocationID ,@AssetID ,@Remarks ,@TransferDate,@CreatedBy ,@CreatedDate )";
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        context.ExecuteQuery<Asset>(query, parameters);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Recursives the location change for child asset.
        /// </summary>
        /// <param name="newAssetstrans">The new assetstrans.</param>
        /// <param name="currentAssetLocation">The current asset location.</param>
        /// <param name="isActual">if set to <c>true</c> [is actual].</param>
        public void RecursiveLocationChangeForChildAsset(AssetsTransfer newAssetstrans, Asset currentAssetLocation, bool isActual)
        {
            AssetService objService = new AssetService();
            List<AssetRelationship> lst = GetAssetChildByAssetId(currentAssetLocation.AssetID);
            if (lst.Count > 0)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    if (!isActual)
                    {
                        objService.SaveAssetsByLocationId(currentAssetLocation);
                        objService.NewAssetsTransferForNLevel(newAssetstrans);
                    }

                    Asset objAsset = new Asset();
                    objAsset.LocationID = currentAssetLocation.LocationID;
                    objAsset.AssetID = lst[i].ChildId;


                    AssetsTransfer objNew = new AssetsTransfer();
                    objNew.Remarks = newAssetstrans.Remarks;
                    objNew.LocationID = newAssetstrans.LocationID;
                    objNew.L2ID = newAssetstrans.L2ID;
                    objNew.AssetID = lst[i].ChildId;
                    objNew.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objNew.CreatedDate = DateTime.Now;
                    objNew.TransferDate = newAssetstrans.TransferDate;

                    RecursiveLocationChangeForChildAsset(objNew, objAsset, false);
                }
            }
            else
            {
                if (!isActual)
                {
                    objService.SaveAssetsByLocationId(currentAssetLocation);
                    objService.NewAssetsTransferForNLevel(newAssetstrans);
                }
            }

        }

        /// <summary>
        /// Gets the asset child by asset identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public static List<AssetRelationship> GetAssetChildByAssetId(int assetID)
        {
            using (DapperDBContext context = new DapperDBContext())
            {
                AssetRelationship obj = new AssetRelationship();
                return context.SearchAll(obj).Where(x => x.AssetID == assetID).ToList();
            }
        }

        /// <summary>
        /// Deletes the child asset by child identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public int DeleteChildAssetByChildID(int assetID)
        {
            try
            {
                AssetRelationship obj = new AssetRelationship();
                using (DapperDBContext context = new DapperDBContext())
                {
                    obj = context.SearchAll(obj).Where(x => x.ChildId == assetID).FirstOrDefault();
                    if (obj != null)
                    {
                        DeleteChildAsset(obj.AssetID, obj.ChildId);
                    }
                    return 0;
                }
            }
            catch
            {
                return -1;
            }

        }
        #endregion

        #region "Asset Tree"

        public static IList<L1> GetL1s(int l2Id, int empId)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            L1 objL1 = new L1();
            using (ServiceContext context = new ServiceContext())
            {
                if (l2Id == 0 && empId == 0)
                {
                    //return context.SearchAll(objL1).OrderBy(x => x.L1No).ToList();
                    var query = "Select L1.*, (Select count(1) from L2 where L2.L1ID = L1.L1ID) HasChild " +
                               "from L1 " +
                               "order by L1.L1no ";
                    IList<L1> lst = context.ExecuteQuery<L1>(query, parameters);
                    return lst;
                }
                else if (l2Id == 0 && empId != 0)
                {
                    var query = "Select distinct L1.*, 1 HasChild " +
                                "from L2 " +
                                "inner join employees_L2 el2 on L2.L2ID = el2.L2ID " +
                                "join L1 on L2.L1ID = L1.L1ID  " +
                                "Where el2.empID = " + empId +
                                "order by L1.L1no ";

                    IList<L1> lst = context.ExecuteQuery<L1>(query, parameters);
                    return lst;
                }
                else if (l2Id != 0 && empId != 0)
                {
                    var query = "Select distinct L1.* , 1 HasChild " +
                              "from L2 " +
                              "inner join employees_L2 el2 on L2.L2ID = el2.L2ID " +
                              "join L1 on L2.L1ID = L1.L1ID  " +
                              "Where el2.empID = " + empId + " and L2.L2ID = " + l2Id +
                              " order by L1.L1no ";

                    IList<L1> lst = context.ExecuteQuery<L1>(query, parameters);
                    return lst;
                }
                else
                {
                    var query = "Select distinct L1.* , 1 HasChild " +
                             "from L1 " +
                             "join L2 on L2.L1ID = L1.L1ID  " +
                             "Where L2.L2ID = " + l2Id +
                             " order by L1.L1no ";

                    IList<L1> lst = context.ExecuteQuery<L1>(query, parameters);
                    return lst;
                }
            }
        }

        public static IList<Sector> GetSector(int l1Id, int empId, int l2Id)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            Sector objSector = new Sector();
            using (ServiceContext context = new ServiceContext())
            {
                if (l2Id == 0 && empId == 0)
                {
                    //return context.SearchAll(objL1).OrderBy(x => x.L1No).ToList();
                    var query = "Select distinct sector.* , 1 as HasChild  " +
                                "from L2  " +
                                "inner join sector on sector.SectorID = L2.SectorID " +
                                "where L2.L1ID =  " + l1Id +
                                "order by sector.SectorCode";
                    IList<Sector> lst = context.ExecuteQuery<Sector>(query, parameters);
                    return lst;
                }
                else if (l2Id == 0 && empId != 0)
                {
                    var query = "Select distinct sector.*, 1 as HasChild  " +
                                " from L2  " +
                                " inner join sector on sector.SectorID = L2.SectorID " +
                                " inner join employees_L2 el2 on L2.L2ID = el2.L2ID  " +
                                " where L2.L1ID = " + l1Id + " and el2.empID = " + empId +
                                " order by sector.SectorCode";

                    IList<Sector> lst = context.ExecuteQuery<Sector>(query, parameters);
                    return lst;
                }
                else if (l2Id != 0 && empId != 0)
                {
                    var query = "Select distinct sector.*, 1 as HasChild  " +
                                " from L2  " +
                                " inner join sector on sector.SectorID = L2.SectorID " +
                                " inner join employees_L2 el2 on L2.L2ID = el2.L2ID  " +
                                " where L2.L1ID = " + l1Id + " and el2.empID = " + empId + " and l2.l2Id = " + l2Id +
                                " order by sector.SectorCode";

                    IList<Sector> lst = context.ExecuteQuery<Sector>(query, parameters);
                    return lst;
                }
                else
                {
                    var query = "Select distinct sector.*, 1 as HasChild  " +
                                " from L2  " +
                                " inner join sector on sector.SectorID = L2.SectorID " +
                                " where L2.L1ID =  " + l1Id + "  and l2.l2Id = " + l2Id +
                                " order by sector.SectorCode";

                    IList<Sector> lst = context.ExecuteQuery<Sector>(query, parameters);
                    return lst;
                }
            }
        }

        //public static IList<L2> GetL2s(int l1Id, int empId, int l2Id)
        //{
        //    L2 objL2 = new L2();
        //    Collection<DBParameters> parameters = new Collection<DBParameters>();
        //    using (ServiceContext context = new ServiceContext())
        //    {
        //        if (empId == 0)
        //        {
        //            if (l2Id == 0)
        //            {
        //                //return context.SearchAll(objL2).Where(x => x.L1ID == l1Id).OrderBy(x => x.L2Code).ToList();

        //                var query = " Select L2.* , (SELECT count(1) FROM L4 WHERE L4.L2ID = L2.L2ID) HasChild " +
        //                             "from L2 where L2.L1ID = " + l1Id + " order by L2.L2Code ";

        //                IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
        //                return lst;
        //            }
        //            else
        //            {
        //                // return context.SearchAll(objL2).Where(x => x.L1ID == l1Id && x.L2ID == l2Id).OrderBy(x => x.L2Code).ToList();
        //                var query = " Select L2.* , (SELECT count(1) FROM L4 WHERE L4.L2ID = L2.L2ID) HasChild " +
        //                          "from L2 where L2.L1ID = " + l1Id + " and L2.L2ID = " + l2Id + " order by L2.L2Code ";

        //                IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
        //                return lst;
        //            }
        //        }
        //        else
        //        {
        //            if (l2Id == 0)
        //            {
        //                var query = "Select distinct * " +
        //                    ", (SELECT count(1) FROM L4 WHERE L4.L2ID = c.L2ID) HasChild" + //New added
        //                   " from L2 c " +
        //                   "inner join employees_L2 d on c.L2ID = d.L2ID  " +
        //                   " Where d.empID = " + empId + " And c.L1ID =" + l1Id +
        //                   " order by c.L2Code ";

        //                IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
        //                return lst;
        //            }
        //            else
        //            {
        //                var query = "Select distinct * " +
        //                    ", (SELECT count(1) FROM L4 WHERE L4.L2ID = c.L2ID) HasChild" + //New added
        //                  " from L2 c " +
        //                  "inner join employees_L2 d on c.L2ID = d.L2ID  " +
        //                  " Where d.empID = " + empId + " and c.L1ID =" + l1Id + " and c.L2ID =" + l2Id +
        //                  " order by c.L2Code ";

        //                IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
        //                return lst;
        //            }
        //        }
        //    }
        //}

        public static IList<L2> GetL2s(int sectorID, int empId, int l2Id, int l1Id)
        {
            L2 objL2 = new L2();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                if (empId == 0)
                {
                    if (l2Id == 0)
                    {
                        //return context.SearchAll(objL2).Where(x => x.L1ID == l1Id).OrderBy(x => x.L2Code).ToList();

                        var query = " Select L2.* , (SELECT count(1) FROM L4 WHERE L4.L2ID = L2.L2ID) HasChild " +
                                     "from L2 where L2.SectorID = " + sectorID + " and L2.Status = 1 and L2.L1Id = " + l1Id + " order by L2.L2Code ";

                        IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
                        return lst;
                    }
                    else
                    {
                        // return context.SearchAll(objL2).Where(x => x.L1ID == l1Id && x.L2ID == l2Id).OrderBy(x => x.L2Code).ToList();
                        var query = " Select L2.* , (SELECT count(1) FROM L4 WHERE L4.L2ID = L2.L2ID) HasChild " +
                                  "from L2 where L2.SectorID = " + sectorID + " and L2.Status = 1 and L2.L1Id = " + l1Id + " and L2.L2ID = " + l2Id + " order by L2.L2Code ";

                        IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
                        return lst;
                    }
                }
                else
                {
                    if (l2Id == 0)
                    {
                        var query = "Select distinct c.*, d.* " +
                            ", (SELECT count(1) FROM L4 WHERE L4.L2ID = c.L2ID) HasChild" + //New added
                           " from L2 c " +
                           "inner join employees_L2 d on c.L2ID = d.L2ID  " +
                           " Where d.empID = " + empId + " And c.SectorID =" + sectorID + " and c.Status = 1 and c.L1Id = " + l1Id +
                           " order by c.L2Code ";

                        IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
                        return lst;
                    }
                    else
                    {
                        var query = "Select distinct c.*, d.* " +
                            ", (SELECT count(1) FROM L4 WHERE L4.L2ID = c.L2ID) HasChild" + //New added
                          " from L2 c " +
                          "inner join employees_L2 d on c.L2ID = d.L2ID  " +
                          " Where d.empID = " + empId + " and c.SectorID =" + sectorID + " and c.Status = 1 and c.L1Id = " + l1Id + " and c.L2ID =" + l2Id +
                          " order by c.L2Code ";

                        IList<L2> lst = context.ExecuteQuery<L2>(query, parameters);
                        return lst;
                    }
                }
            }
        }

        public static IList<L3> GetL3s(int l2Id)
        {
            L3 objL3 = new L3();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            using (ServiceContext context = new ServiceContext())
            {
                // return context.SearchAll(objL3).Where(x => x.L2ID == l2Id).OrderBy(x => x.L3No).ToList();

                var query = " Select L3.* , (SELECT count(1) FROM L4 WHERE L4.L3ID = L3.L3ID ) HasChild  " +
                            " , (SELECT count(1) FROM L5 WHERE L5.L3ID = L3.L3ID ) HasChild1 " + //Juhi : 24_2_2017 : 
                            " from L3 where L3.L2ID = " + l2Id + " order by L3.L3No ";

                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                if (!ProjectSession.IsCentral)
                {
                    query = " Select L3.* , (SELECT count(1) FROM L4 WHERE L4.L3ID = L3.L3ID ) HasChild " +
                            " , (SELECT count(1) FROM L5 WHERE L5.L3ID = L3.L3ID ) HasChild1 " + //Juhi : 24_2_2017 : 
                                    " from L3 Inner Join Employees_L3 On L3.L3ID = Employees_L3.L3ID where Employees_L3.EmpID = " + ProjectSession.EmployeeID + " AND L3.L2ID = " + l2Id + " order by L3.L3No ";
                }
                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                IList<L3> lst = context.ExecuteQuery<L3>(query, parameters);
                return lst;
            }
        }

        public static IList<L3> GetL3sAndL4sByL2Id(int l2Id)
        {
            L3 objL3 = new L3();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            using (ServiceContext context = new ServiceContext())
            {
                // return context.SearchAll(objL3).Where(x => x.L2ID == l2Id).OrderBy(x => x.L3No).ToList();

                var query = " Select L3.L3ID,L3.L3No,L3.L3Desc , L3.L3AltDesc, 1 as IsL3Level,(SELECT count(1) FROM L4 WHERE L4.L3ID = L3.L3ID ) HasChild  " +
                            " , (SELECT count(1) FROM L5 WHERE L5.L3ID = L3.L3ID ) HasChild1 " + //Juhi : 24_2_2017 : 
                            " from L3 where L3.L2ID = " + l2Id +
                            " union " +
                            " select L4.L4ID As L3ID,L4.L4No As L3No,L4.L4Description As L3Desc , L4.L4AltDescription As L3AltDesc, 0 as IsL3Level " +
                            " , (Select count(1) from L5 where L5.L4ID = L4.L4ID) HasChild " +
                            " , (Select count(1) from location where location.L4ID = L4.L4ID) HasChild1 " +
                            " from L4  where L4.L2ID = " + l2Id + " and (L4.L3ID is null or L4.L3ID  = 0 )";

                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                if (!ProjectSession.IsCentral)
                {
                    query = " Select L3.L3ID,L3.L3No,L3.L3Desc , L3.L3AltDesc, 1 as IsL3Level, (SELECT count(1) FROM L4 WHERE L4.L3ID = L3.L3ID ) HasChild " +
                            " , (SELECT count(1) FROM L5 WHERE L5.L3ID = L3.L3ID ) HasChild1 " + //Juhi : 24_2_2017 : 
                            " from L3 Inner Join Employees_L3 On L3.L3ID = Employees_L3.L3ID where Employees_L3.EmpID = " + ProjectSession.EmployeeID + " AND L3.L2ID = " + l2Id +
                            " union " +
                            " select L4.L4ID As L3ID,L4.L4No As L3No,L4.L4Description As L3Desc , L4.L4AltDescription As L3AltDesc, 0 as IsL3Level " +
                            " , (Select count(1) from L5 where L5.L4ID = L4.L4ID) HasChild " +
                            " , (Select count(1) from location where location.L4ID = L4.L4ID) HasChild1 " +
                            " from L4  where L4.L2ID = " + l2Id + " and (L4.L3ID is null or L4.L3ID  = 0 )";
                }
                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                IList<L3> lst = context.ExecuteQuery<L3>(query, parameters);
                return lst;
            }
        }

        public static IList<L4> GetL4sByL2Id(int l2Id)
        {
            L4 objL4 = new L4();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            using (ServiceContext context = new ServiceContext())
            {
                if (ProjectSession.IsAreaFieldsVisible)
                {
                    //return context.SearchAll(objL4).Where(x => x.L2ID == l2Id && (x.L3ID == null || x.L3ID == 0)).OrderBy(x => x.L4No).ToList();

                    var query = "  select L4.* " +
                                   " , (Select count(1) from L5 where L5.L4ID = L4.L4ID) HasChild " +
                                   ", (Select count(1) from location where location.L4ID = L4.L4ID) HasChild1 " +
                                   " from L4  where L4.L2ID = " + l2Id + " and (L4.L3ID is null or L4.L3ID  = 0 ) order by L4.L4No";

                    IList<L4> lst = context.ExecuteQuery<L4>(query, parameters);
                    lst = lst.Where(x => x.HasChild > 0 || x.HasChild1 > 0).ToList();
                    return lst;
                }
                else
                {
                    //return context.SearchAll(objL4).Where(x => x.L2ID == l2Id).OrderBy(x => x.L4No).ToList();

                    var query = "  select L4.* " +
                                  " , (Select count(1) from L5 where L5.L4ID = L4.L4ID) HasChild " +
                                  ", (Select count(1) from location where location.L4ID = L4.L4ID) HasChild1 " +
                                  "from L4  where L4.L2ID = " + l2Id + " order by L4.L4No";

                    IList<L4> lst = context.ExecuteQuery<L4>(query, parameters);
                    lst = lst.Where(x => x.HasChild > 0 || x.HasChild1 > 0).ToList();
                    return lst;
                }
            }
        }

        public static IList<L4> GetL4s(int l3Id)
        {
            L4 objL4 = new L4();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            using (ServiceContext context = new ServiceContext())
            {
                //return context.SearchAll(objL4).Where(x => x.L3ID == l3Id).OrderBy(x => x.L4No).ToList();
                var query = "  select L4.* " +
                                 " , (Select count(1) from L5 where L5.L4ID = L4.L4ID) HasChild " +
                                 ", (Select count(1) from location where location.L4ID = L4.L4ID) HasChild1 " +
                                 "from L4  where L4.L3ID = " + l3Id + " order by L4.L4No";

                IList<L4> lst = context.ExecuteQuery<L4>(query, parameters);
                return lst;
            }
        }

        public static IList<L5> GetL5s(int l4Id)
        {
            L5 objL5 = new L5();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                //return context.SearchAll(objL5).Where(x => x.L4ID == l4Id).OrderBy(x => x.L5No).ToList();
                var query = "  select L5.* " +
                                " , (select count(1) from location where location.L5ID = L5.L5ID) HasChild " +
                                "from L5 where L5.L4ID = " + l4Id + " order by L5.L5NO";

                IList<L5> lst = context.ExecuteQuery<L5>(query, parameters);
                return lst;
            }
        }

        public static IList<Location> GetLocationsByL4Id(int l4Id)
        {
            Location objLocation = new Location();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                //return context.SearchAll(objLocation).Where(x => x.L4ID == l4Id).OrderBy(x => x.LocationNo).ToList();
                /*(Start)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                var query = "  select Location.* " +
                               " ,	(select count(1)  from assets where assets.LocationID = Location.LocationID ) HasChild " +
                               " from Location Inner Join Employees_Location On Location.LocationID = Employees_Location.LocationID  AND Employees_Location.EmpID = " + ProjectSession.EmployeeID + " AND Location.L4ID =  " + l4Id + " and ( Location.L5ID is null or Location.L5ID = 0)  order by Location.LocationNo";
                /*(End)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                IList<Location> lst = context.ExecuteQuery<Location>(query, parameters);
                return lst;
            }
        }

        public static IList<L5> GetL5sAndLocationByL4Id(int l4Id)
        {
            L5 objL5 = new L5();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                if (ProjectSession.IsCentral || !ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    var query = "  select L5.L5ID ,L5.L5no,L5.L5Description,L5.L5AltDescription, 1 as IsL5Level  " +
                                    " , (select count(1) from location where location.L5ID = L5.L5ID) HasChild " +
                                    " from L5 where L5.L4ID = " + l4Id +
                                    " union " +
                                    " select Location.LocationID As L5ID, location.LocationNo As L5no,location.LocationDescription As L5Description,location.LocationAltDescription As L5AltDescription, 0 as  IsL5Level " +
                                    " ,(select count(1)  from assets where assets.LocationID = Location.LocationID ) HasChild " +
                                    " from Location " +
                                    " where Location.L4ID = " + l4Id + " and ( Location.L5ID is null or Location.L5ID = 0) ";

                    IList<L5> lst = context.ExecuteQuery<L5>(query, parameters);
                    return lst;
                }
                else
                {
                    var query = "  select L5.L5ID ,L5.L5no,L5.L5Description,L5.L5AltDescription, 1 as IsL5Level  " +
                                   " , (select count(1) from location where location.L5ID = L5.L5ID) HasChild " +
                                   " from L5 where L5.L4ID = " + l4Id +
                                   " union " +
                                   " select Location.LocationID As L5ID, location.LocationNo As L5no,location.LocationDescription As L5Description,location.LocationAltDescription As L5AltDescription, 0 as  IsL5Level " +
                                   " ,(select count(1)  from assets where assets.LocationID = Location.LocationID ) HasChild " +
                                   " from Location " +
                                   " Inner Join Employees_Location On Location.LocationID = Employees_Location.LocationID  AND Employees_Location.EmpID = " + ProjectSession.EmployeeID + " AND " +
                                   " Location.L4ID = " + l4Id + " and ( Location.L5ID is null or Location.L5ID = 0) ";

                    IList<L5> lst = context.ExecuteQuery<L5>(query, parameters);
                    return lst;
                }
            }
        }

        public static IList<Location> GetLocationsByL5Id(int l5Id)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            using (ServiceContext context = new ServiceContext())
            {
                /*(Start)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                if (ProjectSession.IsCentral)
                {
                    query = "SELECT l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, " +
                       "l.LocationDescription, l.NotetoTech, l.LocationAltDescription,  " +
                       "( CASE WHEN COUNT(a.AssetID) > 0 THEN 1 ELSE 0 END) AS AssetsCount  " +
                       "FROM location l LEFT JOIN assets a ON a.LocationID = l.LocationID " +
                       "  WHERE l.L5ID =  " + l5Id +
                       " GROUP BY l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, l.LocationDescription, l.NotetoTech, l.LocationAltDescription  " +
                       " ORDER BY l.LocationNo";

                }
                else
                {
                    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        query = "SELECT l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, " +
                          "l.LocationDescription, l.NotetoTech, l.LocationAltDescription,  " +
                          "( CASE WHEN COUNT(a.AssetID) > 0 THEN 1 ELSE 0 END) AS AssetsCount  " +
                          "FROM location l LEFT JOIN assets a ON a.LocationID = l.LocationID " +
                          " Inner Join Employees_Location On l.LocationID = Employees_Location.LocationID  AND Employees_Location.EmpID = " + ProjectSession.EmployeeID +
                          "  AND l.L5ID =  " + l5Id +
                          " GROUP BY l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, l.LocationDescription, l.NotetoTech, l.LocationAltDescription  " +
                          " ORDER BY l.LocationNo";
                    }
                    else
                    {
                        query = "SELECT l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, " +
                       "l.LocationDescription, l.NotetoTech, l.LocationAltDescription,  " +
                       "( CASE WHEN COUNT(a.AssetID) > 0 THEN 1 ELSE 0 END) AS AssetsCount  " +
                       "FROM location l LEFT JOIN assets a ON a.LocationID = l.LocationID " +
                       "  WHERE l.L5ID =  " + l5Id +
                       " GROUP BY l.LocationID, l.L2ID, l.L3ID, l.L4ID, l.L5ID, l.CriticalityID, l.LocationNo, l.LocationDescription, l.NotetoTech, l.LocationAltDescription  " +
                       " ORDER BY l.LocationNo";
                    }

                }

                /*(End)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                IList<Location> lst = context.ExecuteQuery<Location>(query, parameters);
                return lst;
            }
        }

        public static IList<Asset> GetAssetsByLocation(int locationId)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            Asset objAsset = new Asset();
            IList<Asset> lstResult = new List<Asset>();
            using (ServiceContext context = new ServiceContext())
            {
                List<Asset> listAsset = context.SearchAll(objAsset).Where(x => x.LocationID == locationId).ToList();

                //Testing
                var query = "select ar.AssetId as RelationAssetID, ar.childID as RelationChildID, Assets.* " +
                    //" ,1 as HasChild " +//New added
                             "from Assets " +
                            "inner join AssetRelationShip ar on ar.ChildId = Assets.AssetID " +
                            "where LocationID = " + locationId;
                IList<Asset> lst = context.ExecuteQuery<Asset>(query, parameters);

                for (int i = 0; i < lst.Count; i++)
                {
                    listAsset.RemoveAll(x => x.AssetID == lst[i].AssetID);
                    //var result = listAsset.Where(x => x.AssetID == lst[i].RelationAssetID);
                }

                var result = lst.Select(x => x.RelationAssetID).Distinct().ToList();
                listAsset.Where(x => result.Contains(x.AssetID)).ToList().ForEach(item =>
                {
                    Asset objAsset1 = new Asset();
                    objAsset1 = item;
                    objAsset1.HasChild = 1;
                });
                return listAsset.OrderBy(x => x.AssetNumber).ToList();
            }
        }

        public static IList<Asset> GetAssetsByAsset(int assetId)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            using (ServiceContext context = new ServiceContext())
            {
                var query = "Select ass.* " +
                            " ,(select count(1) from assetrelationship  where assetrelationship.AssetID= ar.childID ) haschild " +
                            " from assets ass " +
                            "inner join assetrelationship ar on ar.ChildId = ass.AssetID  " +
                            "where ar.AssetID = " + assetId;

                IList<Asset> lst = context.ExecuteQuery<Asset>(query, parameters);
                return lst;
            }
        }

        #endregion

        #region "Assets By LocationID"
        /// <summary>
        /// Get Assets By LocationID
        /// </summary>
        /// <param name="LocationID"></param>
        /// <param name="pageNo"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<Asset> GetAssetsByLocationID(int LocationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Asset> list = new List<Asset>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = @"Select AssetID,AssetNumber,AssetDescription,AssetAltDescription,ModelNumber,SerialNumber,Manufacturer,ac.AssetCategory,AltAssetCategory FROM assets a
                             LEFT JOIN assetcategories ac on ac.AssetCatID = a.AssetCatID
                             Where LocationID = " + LocationID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Asset>(query, outParameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
        #endregion

        #region "PM Schedule By Asset"
        public IList<Asset> GetAssetsInfo(string AssetIDs)
        {
            List<Asset> lst = new List<Asset>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = "Select Assets.*,L2.L2ID,L2.L2Code from Assets " +
                            " inner join location on  location.LocationID = Assets.LocationID " +
                            " left join L2 on L2.L2ID = location.L2ID " +
                            " where AssetID in ( " + AssetIDs + ")";

            using (DapperDBContext context = new DapperDBContext())
            {
                lst = context.ExecuteQuery<Asset>(query, parameters).ToList();
            }

            return lst;
        }
        #endregion

        #region "Asset Specification"

        public IList<AssetSpecification> GetAssetSpecificationByAssetID(int assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            outParameters.Add(new DBParameters()
            {
                Name = "assetID",
                Value = assetID,
                DBType = DbType.Int32
            });

            IList<AssetSpecification> list = new List<AssetSpecification>();
            string query = @"select  AssetSpecification.*,Specifications.SpecDesc,Specifications.SpecAltDesc,Specifications.UOM 
                            from AssetSpecification 
                            inner join Specifications on Specifications.SpecID = AssetSpecification.SpecID
                            where assetID = @assetID";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetSpecification>(query, outParameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public static int GeAssetSpecificationCount(int assetID, int specId, int assetSpecId)
        {
            int count = 0;

            if (assetSpecId > 0)
            {
                string query = " Select count(1) from AssetSpecification where AssetID = " + assetID + " and SpecId = " + specId + " and assetSpecId != " + assetSpecId;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
            else
            {
                string query = "Select count(1) from AssetSpecification where AssetID = " + assetID + " and SpecId = " + specId;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
        }

        #endregion


      
    }
}
