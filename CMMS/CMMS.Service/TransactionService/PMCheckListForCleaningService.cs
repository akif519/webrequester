﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PMCheckListForCleaningService : DBExecute
    {
        public PMCheckListForCleaningService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public virtual IList<TEntity> GetPMChecklistPageAdvancedSearch<TEntity>(TEntity entity, int PageID, int EmployeeID, int pageNo, string sortExpression, string sortDirection, bool IsSaved = true, string extraWhereClause = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            string whereClause = string.Empty;
            string query = string.Empty;
            string strWhereClause = string.Empty;
            IList<TEntity> list;
            try
            {
                Collection<DBParameters> outParameters = new Collection<DBParameters>();
                SearchFieldService obj = new SearchFieldService();
                whereClause = obj.GetWhereClause(PageID, EmployeeID, IsSaved, out outParameters);

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

                Page page = new Page();

                using (DapperContext objDapperContext = new DapperContext())
                {
                    page = objDapperContext.SelectObject<Page>(PageID);
                }
                if (page != null)
                {
                    if (String.IsNullOrEmpty(whereClause))
                    {
                        whereClause = "1 = 1";
                        ProjectSession.IsAdvanceSearch = false;
                    }
                    else
                    {
                        ProjectSession.IsAdvanceSearch = true;
                    }

                    whereClause += extraWhereClause;

                    if (String.IsNullOrEmpty(strWhereClause))
                    {
                        query = "Select " + page.DisplayColumns + " " + page.TableJoins + " AND " + whereClause;
                    }
                    else
                    {
                        query = "Select * from (Select " + page.DisplayColumns + " " + page.TableJoins + " AND " + whereClause + ")A Where " + strWhereClause;
                    }
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<TEntity>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;

            }

            finally
            {

            }
        }

        public static PMCheckList GetPMChecklistForCleaningByID(int checkListID)
        {
            IList<PMCheckList> list = new List<PMCheckList>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string query = " Select pmchecklist.* , ExtAssetFiles.FileLInk , ExtAssetFiles.FileDescription,ExtAssetFiles.FileName, ExtAssetFiles.AutoId, " +
                            " tblSafetyinstruction.SafetyNo,tblSafetyinstruction.SafetyName,tblSafetyinstruction.AltSafetyName " +
                            "  FROM  pmchecklist  " +
                            " LEFT JOIN tblSafetyinstruction on   tblSafetyinstruction.SafetyID = pmchecklist.SafetyID " +
                             " LEFT JOIN ExtAssetFiles  ON ExtAssetFiles.FkId = pmchecklist.ChecklistID AND ModuleType='P'  " +
                             " WHERE pmchecklist.IsCleaningModule= 1   and pmchecklist.checkListID = " + checkListID;

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PMCheckList>(query, parameters).FirstOrDefault();
            }
        }

        public IList<PMCheckList> GetCleaningChecklistPage(string strWhere, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PMCheckList> list = new List<PMCheckList>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "EmpID",
                Value = empId,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "ChecklistID";
            }

            string strWhereClauseWithEmployee = string.Empty;

            if (IsCentral)
            {
                empId = 0;
            }

            if (empId > 0)
            {
                strWhereClauseWithEmployee = "  and ( pmchecklist.L2ID in (select L2Id from employees_L2 where empID =  @EmpID union Select L2Id from employees where employeeId =  @EmpID ) OR pmchecklist.L2ID IS NULL) ";
            }

            string query = @" Select  pmchecklist.ChecklistID, pmchecklist.SafetyID,pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, pmchecklist.EstimatedLaborHours, 
				              pmchecklist.FileLink, pmchecklist.L2Id, pmchecklist.MaintdeptId, pmchecklist.MainSubDeptId, 
                              ts.SafetyNo,ts.SafetyName,ts.AltSafetyName,
                              pmchecklist.MaintdivId, pmchecklist.CreatedBy, pmchecklist.CreatedDate, pmchecklist.ModifiedBy, 
                              pmchecklist.ModifiedDate,L2.L2Code,MainenanceDivision.MaintDivisionCode,    MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode  ,
							  eaf.FileLInk,Eaf.FileDescription,eaf.FileName,eaf.autoid 
                              FROM  pmchecklist left outer join L2 on pmchecklist.L2ID = L2.L2ID 
						      LEFT JOIN ExtAssetFiles eaf ON eaf.FkId = pmchecklist.ChecklistID AND ModuleType='P'
                              LEFT JOIN tblSafetyinstruction ts ON ts.SafetyID = pmchecklist.SafetyID
                              LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = pmchecklist.MaintdivId  
                              LEFT JOIN MaintenanceDepartment  ON MaintenanceDepartment.maintDeptID =pmchecklist.MaintdeptId  
                              LEFT JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = pmchecklist.MainSubDeptId  
                              WHERE pmchecklist.IsCleaningModule=1  " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMCheckList>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }
}
