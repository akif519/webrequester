﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PurchaseService : DBExecute
    {
        public PurchaseService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Purchase Request"

        public static PurchaseRequest GetPurchaseRequestDetail(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " Select purchaserequest.*,  " +
                         " purchase_req_status.status_desc,purchase_req_status.altstatus_desc,  " +
                         " ReqByemployees.EmployeeNO As RequestedByEmployeeNO,ReqByemployees.Name As RequestedByEmployeeName ,ReqByemployees.AltName as RequestedByAltEmployeeName,  " +
                         " CreateByemployees.EmployeeNO As CreatedByEmployeeNO, CreateByemployees.Name As CreatedByEmployeeName ,CreateByemployees.AltName as CreatedByAltEmployeeName,  " +
                         " CancelByemployees.Name As Cancel_byName, " +
                         " pr_authorisation_status.auth_status_desc,pr_authorisation_status.Altauth_status_desc  " +
                         " FROM PurchaseRequest    " +
                         " left outer join employees ReqByemployees on ReqByemployees.EmployeeID = purchaserequest.RequestedBy   " +
                         " left outer join employees CreateByemployees on CreateByemployees.EmployeeID = purchaserequest.CreatedBy   " +
                         " left outer join employees CancelByemployees on CancelByemployees.EmployeeID = purchaserequest.cancel_by " +
                         " LEFT JOIN purchase_req_status ON purchase_req_status.pr_status_id = purchaserequest.status_id  " +
                         " LEFT JOIN pr_authorisation_status ON pr_authorisation_status.auth_status_id = purchaserequest.auth_status  " +
                            " where PurchaseRequest.Id = @ID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PurchaseRequest>(query, parameters).FirstOrDefault();
            }
        }

        public IList<PurchaseRequestItem> GetPurchaseRequestItemsByPRID(string strWhere, int pageNo, string sortExpression, string sortDirection)
        {
            IList<PurchaseRequestItem> list = new List<PurchaseRequestItem>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = " Select PurchaseRequestItems.*,SC.StockNo,SC.AltStockDescription" +
                            " from PurchaseRequestItems   " +
                            " Left Outer Join stockcode SC on SC.StockID = PurchaseRequestItems.PartNumber " +
                             " where 1=1 " + strWhere;

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PurchaseRequestItem>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            IList<PurchaseRequestItem> listFinal = new List<PurchaseRequestItem>();
            if (list.Count > 0)
            {
                foreach (PurchaseRequestItem obj in list)
                {
                    obj.Guid_ItemID = System.Guid.NewGuid();
                    listFinal.Add(obj);
                }
            }

            return listFinal;
        }

        public IList<Purchase_req_auth> GetApprovePRBypr_ID(int pageNo, string sortExpression, string sortDirection, int id, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Purchase_req_auth> list = new List<Purchase_req_auth>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "  Select purchase_req_auth.*, " +
                           " employees.EmployeeNO,employees.Name, " +
                           " PRApprovalLevels.LevelNo,PRApprovalLevels.LevelName " +
                           "  from purchase_req_auth " +
                           " inner join employees on employees.EmployeeID = purchase_req_auth.EmployeeID " +
                           " inner join PRApprovalLevelMappings on PRApprovalLevelMappings.AutoId = purchase_req_auth.PRApprovalLevelMappingId " +
                           " inner join PRApprovalLevels on PRApprovalLevels.PRApprovalLevelId = PRApprovalLevelMappings.PRApprovalLevelId " +
                           " where purchase_req_auth.PurchaseRequestID = " + id;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Purchase_req_auth>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static IList<PRApprovalLevel> GetPRApprovalLevelsByL2ID_pr_ID(int cityID, int purchaseReqID)
        {
            PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            int LastLevel = 0;
            string query = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                query = "Select PRApprovalLevels.* from purchase_req_auth " +
                                                " inner join PRApprovalLevelMappings on PRApprovalLevelMappings.AutoId =  purchase_req_auth.PRApprovalLevelMappingId " +
                                                " inner join PRApprovalLevels on PRApprovalLevels.PRApprovalLevelId = PRApprovalLevelMappings.PRApprovalLevelId " +
                                                " where PurchaseRequestID = " + purchaseReqID + " and ROWNUM = 1 order by auth_date desc";
            }
            else if(ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
            {
                query = "Select PRApprovalLevels.* from purchase_req_auth " +
                                            " inner join PRApprovalLevelMappings on PRApprovalLevelMappings.AutoId =  purchase_req_auth.PRApprovalLevelMappingId " +
                                            " inner join PRApprovalLevels on PRApprovalLevels.PRApprovalLevelId = PRApprovalLevelMappings.PRApprovalLevelId " +
                                            " where PurchaseRequestID = " + purchaseReqID + " order by auth_date desc LIMIT 1";
            }
            else
            {
                query = "Select top 1 PRApprovalLevels.* from purchase_req_auth " +
                                                " inner join PRApprovalLevelMappings on PRApprovalLevelMappings.AutoId =  purchase_req_auth.PRApprovalLevelMappingId " +
                                                " inner join PRApprovalLevels on PRApprovalLevels.PRApprovalLevelId = PRApprovalLevelMappings.PRApprovalLevelId " +
                                                " where PurchaseRequestID = " + purchaseReqID + " order by auth_date desc";
            }


            using (ServiceContext context = new ServiceContext())
            {
                objPRApprovalLevel = context.ExecuteQuery<PRApprovalLevel>(query, parameters).FirstOrDefault();
            }

            if (objPRApprovalLevel != null)
            {
                if (objPRApprovalLevel.IsFinalLevel)
                {
                    LastLevel = 0;
                }
                else
                {
                    LastLevel = objPRApprovalLevel.LevelNo;
                }
            }

            string query1 = " Select  PRApprovalLevelMappings.AutoId ,PRApprovalLevelMappings.Mandatory , PRApprovalLevels.* from  PRApprovalLevels " +
                            " inner join PRApprovalLevelMappings on PRApprovalLevels.PRApprovalLevelId = PRApprovalLevelMappings.PRApprovalLevelId " +
                            " where PRApprovalLevelMappings.L2ID = " + cityID + " and PRApprovalLevels.LevelNo > " + LastLevel + " order by PRApprovalLevels.LevelNo ";

            List<PRApprovalLevel> lstPRApprovalLevel = new List<PRApprovalLevel>();
            List<PRApprovalLevel> finallstPRApprovalLevel = new List<PRApprovalLevel>();
            using (ServiceContext context = new ServiceContext())
            {
                lstPRApprovalLevel = context.ExecuteQuery<PRApprovalLevel>(query1, parameters).ToList();

                foreach (PRApprovalLevel item in lstPRApprovalLevel)
                {
                    if (item.Mandatory == false)
                    {
                        finallstPRApprovalLevel.Add(item);
                    }
                    else
                    {
                        finallstPRApprovalLevel.Add(item);
                        break;
                    }
                }

                return finallstPRApprovalLevel;
            }
        }

        public static IList<employees> GetEmployeesByPRApprovalLevelId_L2ID(int cityID, int PRApprovalLevelId)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string query1 = " select employees.EmployeeID,employees.EmployeeNO,employees.Name,employees.AltName " +
                            " from employees where EmployeeID " +
                            " in (select EmployeeId from PRApprovalLevelMappings_employees " +
                            " where AutoId in (select AutoId from PRApprovalLevelMappings " +
                            " where PRApprovalLevelId = " + PRApprovalLevelId +
                            " and L2ID =  " + cityID +
                            " )) ";

            using (ServiceContext context = new ServiceContext())
            {
                List<employees> lstemployees = context.ExecuteQuery<employees>(query1, parameters).ToList();
                return lstemployees;
            }
        }

        public static bool IsAnyPRItemRaisedToPO(int ID)
        {
            string query = " select COUNT(1) As NoOfOrder from PurchaseRequestItems " +
                        " INNER JOIN PurchaseOrderItems on PurchaseOrderItems.PurchaseRequestItemID  = PurchaseRequestItems.ID " +
                        " where PurchaseRequestItems.PurchaseRequestID = " + ID;

            using (DapperDBContext context = new DapperDBContext())
            {
                int RecordCount = Convert.ToInt32(context.ExecuteScalar(query));
                if (RecordCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void NewPurchaseRequestItemDetails(PurchaseRequest model)
        {
            try
            {
                JobOrderService objService = new JobOrderService();
                if (model.lstPurchaseRequestItem != null && model.lstPurchaseRequestItem.Count() > 0)
                {
                    List<PurchaseRequestItem> lstToAdd = new List<PurchaseRequestItem>();

                    IList<PurchaseRequestItem> lstExistingItem = new List<PurchaseRequestItem>();
                    PurchaseRequestItem obj = new PurchaseRequestItem();
                    using (DapperContext objContext = new DapperContext())
                    {
                        obj.PurchaseRequestID = model.Id;
                        lstExistingItem = objContext.Search<PurchaseRequestItem>(obj);
                    }

                    List<PurchaseRequestItem> lstForUpdate = lstExistingItem.Where(p => p.PurchaseRequestID == model.Id && model.lstPurchaseRequestItem.Select(ep => ep.ID).Any(ep => ep == p.ID)).ToList();

                    if (lstForUpdate.Count() > 0)
                    {
                        lstForUpdate.ForEach(p =>
                        {
                            PurchaseRequestItem editObject = model.lstPurchaseRequestItem.FirstOrDefault(ep => ep.ID == p.ID);
                            Collection<DBParameters> parameters = new Collection<DBParameters>();

                            parameters.Add(new DBParameters()
                            {
                                Name = "PurchaseRequestID",
                                Value = editObject.PurchaseRequestID,
                                DBType = DbType.Int32
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PartNumber",
                                Value = editObject.PartNumber,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PartDescription",
                                Value = editObject.PartDescription,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "UoM",
                                Value = editObject.UoM,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Quantity",
                                Value = editObject.Quantity,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "JoborderID",
                                Value = editObject.JoborderID,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Source",
                                Value = editObject.Source,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "RefPrice",
                                Value = editObject.RefPrice,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "ID",
                                Value = editObject.ID,
                                DBType = DbType.Int32
                            });

                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "ModifiedDate",
                                    Value = DateTime.Now,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "ModifiedDate",
                                    Value = Common.GetEnglishDate(DateTime.Now),
                                    DBType = DbType.DateTime
                                });
                            }

                            string query = " UPDATE PurchaseRequestItems   SET " +
                                          "  PurchaseRequestID = @PurchaseRequestID" +
                                          " ,PartNumber = @PartNumber" +
                                          " ,PartDescription = @PartDescription" +
                                          " ,UoM = @UoM" +
                                          " ,Quantity =  @Quantity" +
                                          " ,JoborderID = @JoborderID" +
                                          " ,Source = @Source" +
                                          " ,RefPrice = @RefPrice" +
                                          " ,ModifiedBy = " + ProjectSession.EmployeeID +
                                          " ,ModifiedDate = @ModifiedDate" +
                                           " WHERE ID  = @ID";
                            using (DapperContext context = new DapperContext())
                            {
                                context.ExecuteQuery<PurchaseRequestItem>(query, parameters);
                            }

                            //Pratik changes
                            if (editObject.JoborderID != null && objService.ChangeJobOrderStatusByWorkorderNo(editObject.JoborderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                            {
                                objService.NewJOStatusAudit(editObject.JoborderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                            }
                            //End Pratik changes
                        });

                        lstToAdd = model.lstPurchaseRequestItem.AsEnumerable().Where(p => !lstForUpdate.Any(ep => ep.ID == p.ID)).ToList();
                    }
                    else
                    {
                        lstToAdd = model.lstPurchaseRequestItem.ToList();
                    }

                    foreach (var item in lstToAdd)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.PurchaseRequestID = model.Id;
                        using (DapperContext objDapper = new DapperContext())
                        {
                            int result = objDapper.Save<PurchaseRequestItem>(item);
                        }

                        //Pratik changes
                        if (item.JoborderID != null && objService.ChangeJobOrderStatusByWorkorderNo(item.JoborderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                        {
                            objService.NewJOStatusAudit(item.JoborderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                        }
                        //End Pratik changes
                    }

                    List<PurchaseRequestItem> lstToDelete = lstExistingItem.AsEnumerable().Where(p => !lstForUpdate.Any(ep => ep.ID == p.ID) && !lstToAdd.Any(ep => ep.ID == p.ID)).ToList();
                    foreach (var item in lstToDelete)
                    {
                        using (DapperContext objDapper = new DapperContext())
                        {
                            int result = objDapper.Delete<PurchaseRequestItem>(item.ID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<PrintPurchaseRequisitions> GetPurchaseRequestItemsByPRIDForReport(int prid)
        {
            IList<PrintPurchaseRequisitions> lstDetails = new List<PrintPurchaseRequisitions>();
            List<PrintPurchaseRequisitions> lstPRItem = new List<PrintPurchaseRequisitions>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (prid != null)
            {
                parameters.Add(new DBParameters()
                {
                    Name = "PurchaseRequestID",
                    Value = prid,
                    DBType = DbType.Int32
                });
            }

            query = "SELECT Row_number() OVER (ORDER BY prItems.ID) AS Rowno, stockcode.StockNo, prItems.PartDescription, prItems.Altpart_desc, prItems.uom, prItems.Quantity," +
                " ISNULL(prItems.JoborderID, '') AS WorkorderNo, pr.ID,pr.L2ID,pr.PurchaseRequestID, " +
                " pr.MaintdivId,MainenanceDivision.MaintDivisionName,MainenanceDivision.MaintDivisionAltName,pr.MaintdeptId, " +
                " MaintenanceDepartment.MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc,pr.CreatedDate " +
                " FROM PurchaseRequestItems  prItems INNER JOIN " +
                " PurchaseRequest pr ON prItems.PurchaseRequestID = pr.ID LEFT OUTER JOIN " +
                " stockcode ON stockcode.StockID = prItems.PartNumber " +
                " INNER JOIN MainenanceDivision on MainenanceDivision.MaintDivisionID = pr.MaintdivId " +
                " INNER JOIN MaintenanceDepartment on MaintenanceDepartment.maintDeptID = pr.MaintdeptId " +
                "WHERE prItems.PurchaseRequestID = @PurchaseRequestID";

            using (DapperDBContext context = new DapperDBContext())
            {
                lstDetails = context.ExecuteQuery<PrintPurchaseRequisitions>(query, parameters);
            }
            if (lstDetails != null && lstDetails.Count > 0)
            {
                lstPRItem = lstDetails.ToList();
            }
            return lstPRItem;
        }

        #endregion

        #region "Purchase Order"
        public IList<PurchaseOrderItem> GetPurchaseOrderItemsByPRIDWithOutLoad(string strWhere, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<PurchaseOrderItem> list = new List<PurchaseOrderItem>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = " Select POI.ID,POI.PurchaseOrderID,POI.PartNumber,POI.UoM,POI.Quantity,POI.JobOrderID,POI.UnitPrice,POI.Discount,POI.Tax,POI.LineCost,POI.BaseCost,POI.[Source], " +
                            " POI.PurchaseRequestItemID,POI.CreatedDate,POI.PurchaseRequestNo,SC.StockNo, " +
                            " POI.PartDescription, SC.AltStockDescription " +
                            " from PurchaseOrderItems POI   " +
                            " Left Outer Join stockcode SC on SC.StockID = POI.PartNumber " +
                             " where 1=1 " + strWhere;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PurchaseOrderItem>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            IList<PurchaseOrderItem> listFinal = new List<PurchaseOrderItem>();
            if (list.Count > 0)
            {
                foreach (PurchaseOrderItem obj in list)
                {
                    obj.Guid_ItemID = System.Guid.NewGuid();
                    listFinal.Add(obj);
                }
            }

            return listFinal;
        }

        public static PurchaseOrder GetPurchaseOrderDetail(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " Select PurchaseOrder.*, " +
                            " OrderByEmp.EmployeeNO As OrderByEmployeeNO,OrderByEmp.Name As OrderByEmployeeName,OrderByEmp.AltName As OrderByAltEmployeeName, " +
                            " InvoiceToEmp.EmployeeNO As InvoiceToEmployeeNO,InvoiceToEmp.Name As InvoiceToEmployeeName,InvoiceToEmp.AltName As InvoiceToAltEmployeeName, " +
                            " Accountcode.AccountCode,Accountcode.AccountCodeDesc,Accountcode.AltAccountCodeDesc,  " +
                            " CostCenter.CostCenterNo,CostCenter.CostCenterName,CostCenter.AltCostCenterName, " +
                            " suppliers.SupplierNo,suppliers.SupplierName,suppliers.AltSupplierName, " +
                            " Currency.CurrencyCode,Currency.CurrencyDescription,Currency.AltCurrencyDescription,Currency.Rate as CurrencyRate " +
                            " FROM PurchaseOrder  " +
                            " INNER JOIN employees OrderByEmp on OrderByEmp.EmployeeID = PurchaseOrder.OrderBy " +
                            " INNER JOIN employees InvoiceToEmp on InvoiceToEmp.EmployeeID = PurchaseOrder.InvoiceTo " +
                            " INNER Join suppliers on suppliers.SupplierID = PurchaseOrder.SupplierID " +
                            " LEFT JOIN Accountcode on Accountcode.AccCodeid = PurchaseOrder.AccountCodeID " +
                            " LEFT JOIN CostCenter on CostCenter.CostCenterId = accountcode.CostCenterId " +
                            " LEFT JOIN Currency on Currency.CurrencyID = PurchaseOrder.CurrencyID " +
                            "Where PurchaseOrder.ID = @ID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PurchaseOrder>(query, parameters).FirstOrDefault();
            }
        }

        public static void UpdatePurchaseOrder(int ID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "ID",
                    Value = ID,
                    DBType = DbType.Int32
                });

                string query = "Update PurchaseOrder set orderstatus = 'Closed' where Id= @ID";
                string query1 = "Update PurchaseRequest set status_id = 2 where Id in (Select PurchaseRequestItemID from PurchaseOrderItems Where PurchaseOrderID = @ID )";

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery<Asset>(query, parameters);
                    context.ExecuteQuery<Asset>(query1, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<PurchaseRequest> GetPRForPO(int pageNo, string sortExpression, string sortDirection, int cityID, [DataSourceRequest]DataSourceRequest request = null)
        {
            //vwPRForPOs
            string strWhereClause = string.Empty;
            IList<PurchaseRequest> list = new List<PurchaseRequest>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = " Select t.* from (SELECT pr.Id AS PRID, pr.PurchaseRequestID AS PRNO, pr.RequestedBy,pr.L2ID,pr.auth_status, " +
                            "  PRI.ID AS PRIID, PRI.PartNumber, PRI.PartDescription, PRI.UoM, PRI.Quantity, PRI.JoborderID, PRI.Source,PRI.RefPrice,   " +
                            " ISNULL(SUM(poi.Quantity), 0.0000) AS POIQuantity,  " +
                            " CASE WHEN ISNULL(SUM(poi.Quantity), 0) = 0 THEN 'NotOrdered'  " +
                            "      WHEN ISNULL(SUM(poi.Quantity), 0) < pri.Quantity THEN 'Partially Ordered'  " +
                            " 	 WHEN ISNULL(SUM(poi.Quantity), 0) >= pri.Quantity THEN 'Fully Ordered' END AS Status,  " +
                            " 	 PAS.auth_status_desc, SC.StockNo, SC.StockID, " +
                            " 	  Emp.Name AS RequestedByEmployeeName " +
                            " FROM PurchaseRequest pr " +
                            "  LEFT OUTER JOIN PurchaseRequestItems PRI ON pr.Id = PRI.PurchaseRequestID  " +
                            " LEFT OUTER JOIN PurchaseOrderItems poi ON PRI.ID = poi.PurchaseRequestItemID  " +
                            " LEFT OUTER JOIN PurchaseOrder PO ON poi.PurchaseOrderID = PO.ID  " +
                            " LEFT OUTER JOIN pr_authorisation_status PAS ON PAS.auth_status_id = pr.auth_status  " +
                            " LEFT OUTER JOIN stockcode SC ON PRI.PartNumber = SC.StockID  " +
                            " INNER JOIN employees Emp ON pr.RequestedBy = Emp.EmployeeID " +
                            " GROUP BY pr.Id, pr.PurchaseRequestID, pr.RequestedBy, PRI.ID, PRI.PartNumber, PRI.PartDescription, PRI.UoM, PRI.Quantity, PRI.JoborderID, PRI.Source,PRI.RefPrice, pr.L2ID,  " +
                            " pr.auth_status, PAS.auth_status_desc, SC.StockNo,SC.StockID, Emp.Name " +
                            " ) t where POIQuantity < Quantity and auth_status = 3 and L2ID = " + cityID;


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PurchaseRequest>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<Material_req_detail> GetIRForPO(int pageNo, string sortExpression, string sortDirection, int cityID, [DataSourceRequest]DataSourceRequest request = null)
        {
            //vwPRForPOs
            string strWhereClause = string.Empty;
            IList<Material_req_detail> list = new List<Material_req_detail>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = " select * from material_req_details inner join Material_req_master on Material_req_master.mr_no = material_req_details.mr_no left join stockcode on stockcode.StockID = material_req_details.partid where auth_status = 3 and L2ID = " + cityID;


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Material_req_detail>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static bool IsAnyPOItemReceivedOrCancelled(int ID)
        {
            string query = "  Select count(1) from trans " +
                           " inner join PurchaseOrderItems on PurchaseOrderItems.ID = trans.PurchaseOrderItemsId " +
                           " where PurchaseOrderItems.PurchaseOrderID =" + ID;

            using (DapperDBContext context = new DapperDBContext())
            {
                int RecordCount = Convert.ToInt32(context.ExecuteScalar(query));
                if (RecordCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static void UpdateDeliverynOrderStatusPO(int id, string DeliveryStatus, string OrderStatus, int EmployeeID)
        {
            try
            {
                PurchaseOrder objPurchaseOrder = new PurchaseOrder();

                using (ServiceContext context = new ServiceContext())
                {
                    objPurchaseOrder = context.SelectObject<PurchaseOrder>(id);
                }

                if (!string.IsNullOrEmpty(DeliveryStatus))
                {
                    objPurchaseOrder.DeliveryStatus = DeliveryStatus;
                }

                if (objPurchaseOrder.OrderStatus != "Authorised" && OrderStatus == "Authorised")
                {
                    Purchase_order_auth obj = new Purchase_order_auth();
                    obj.ApprovalDate = DateTime.Now;
                    obj.AuthorisedBy = EmployeeID;
                    obj.PurchaseOrderID = objPurchaseOrder.ID;
                    using (ServiceContext context = new ServiceContext())
                    {
                        int result = context.Save<Purchase_order_auth>(obj);
                    }
                }

                if (!string.IsNullOrEmpty(OrderStatus))
                {
                    objPurchaseOrder.OrderStatus = OrderStatus;
                }

                //Generate GRNNo - Goods Receipt Number when order is Authorised
                if (objPurchaseOrder.OrderStatus == "Authorised" && string.IsNullOrEmpty(objPurchaseOrder.GRNNo))
                {
                    objPurchaseOrder.GRNNo = objPurchaseOrder.PurchaseOrderNo.Replace("PO", "GR");
                    objPurchaseOrder.GRNCreatedDate = DateTime.Now;
                }

                using (ServiceContext context = new ServiceContext())
                {
                    int result = context.Save<PurchaseOrder>(objPurchaseOrder);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Purchase_order_auth> GetPOAuthHistory(int pageNo, string sortExpression, string sortDirection, int id, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Purchase_order_auth> list = new List<Purchase_order_auth>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "  Select purchase_order_auth.*,employees.EmployeeNO,employees.Name from purchase_order_auth " +
                            " inner join employees on employees.EmployeeID = purchase_order_auth.AuthorisedBy " +
                             "where PurchaseOrderID = " + id;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Purchase_order_auth>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static void NewPurchaseOrderItemDetails(PurchaseOrder model)
        {
            try
            {
                JobOrderService objService = new JobOrderService();
                if (model.lstPurchaseOrderItem != null && model.lstPurchaseOrderItem.Count() > 0)
                {
                    List<PurchaseOrderItem> lstToAdd = new List<PurchaseOrderItem>();

                    IList<PurchaseOrderItem> lstExistingItem = new List<PurchaseOrderItem>();
                    PurchaseOrderItem obj = new PurchaseOrderItem();
                    using (DapperContext objContext = new DapperContext())
                    {
                        obj.PurchaseOrderID = model.ID;
                        lstExistingItem = objContext.Search<PurchaseOrderItem>(obj);
                    }

                    List<PurchaseOrderItem> lstForUpdate = lstExistingItem.Where(p => p.PurchaseOrderID == model.ID && model.lstPurchaseOrderItem.Select(ep => ep.ID).Any(ep => ep == p.ID)).ToList();

                    if (lstForUpdate.Count() > 0)
                    {
                        lstForUpdate.ForEach(p =>
                        {
                            PurchaseOrderItem editObject = model.lstPurchaseOrderItem.FirstOrDefault(ep => ep.ID == p.ID);
                            Collection<DBParameters> parameters = new Collection<DBParameters>();

                            parameters.Add(new DBParameters()
                            {
                                Name = "PurchaseOrderID",
                                Value = editObject.PurchaseOrderID,
                                DBType = DbType.Int32
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PartNumber",
                                Value = editObject.PartNumber,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PartDescription",
                                Value = editObject.PartDescription,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "UoM",
                                Value = editObject.UoM,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Quantity",
                                Value = editObject.Quantity,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "JobOrderID",
                                Value = editObject.JobOrderID,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "UnitPrice",
                                Value = editObject.UnitPrice,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Discount",
                                Value = editObject.Discount,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Tax",
                                Value = editObject.Tax,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "LineCost",
                                Value = editObject.LineCost,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "BaseCost",
                                Value = editObject.BaseCost,
                                DBType = DbType.Decimal
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "Source",
                                Value = editObject.Source,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PurchaseRequestItemID",
                                Value = editObject.PurchaseRequestItemID,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "PurchaseRequestNo",
                                Value = editObject.PurchaseRequestNo,
                                DBType = DbType.String
                            });

                            parameters.Add(new DBParameters()
                            {
                                Name = "ID",
                                Value = editObject.ID,
                                DBType = DbType.Int32
                            });

                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "CreatedDate",
                                    Value = DateTime.Now,
                                    DBType = DbType.DateTime
                                });

                            }
                            else
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "CreatedDate",
                                    Value = Common.GetEnglishDate(DateTime.Now),
                                    DBType = DbType.DateTime
                                });

                            }
  
                            string query = " UPDATE PurchaseOrderItems   SET " +
                                          "  PurchaseOrderID = @PurchaseOrderID" +
                                          " ,PartNumber = @PartNumber" +
                                          " ,PartDescription = @PartDescription" +
                                          " ,UoM = @UoM" +
                                          " ,Quantity = @Quantity" +
                                          " ,JobOrderID = @JobOrderID" +
                                          " ,UnitPrice = @UnitPrice" +
                                          " ,Discount = @Discount" +
                                          " ,Tax = @Tax" +
                                          " ,LineCost = @LineCost" +
                                          " ,BaseCost = @BaseCost" +
                                          " ,Source = @Source" +
                                          " ,PurchaseRequestItemID = @PurchaseRequestItemID" +
                                          " ,CreatedDate = @CreatedDate" +
                                           " ,PurchaseRequestNo = @PurchaseRequestNo " +
                                           " WHERE ID  = @ID";
                            using (DapperContext context = new DapperContext())
                            {

                                context.ExecuteQuery<PurchaseOrderItem>(query, parameters);
                                //context.Save<PurchaseOrderItem>(editObject);
                            }

                            //Pratik changes
                            if (editObject.JobOrderID != null && objService.ChangeJobOrderStatusByWorkorderNo(editObject.JobOrderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                            {
                                objService.NewJOStatusAudit(editObject.JobOrderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                            }
                            //End Pratik changes
                        });

                        lstToAdd = model.lstPurchaseOrderItem.AsEnumerable().Where(p => !lstForUpdate.Any(ep => ep.ID == p.ID)).ToList();
                    }
                    else
                    {
                        lstToAdd = model.lstPurchaseOrderItem.ToList();
                    }

                    foreach (var item in lstToAdd)
                    {
                        item.CreatedDate = DateTime.Now;
                        item.PurchaseOrderID = model.ID;
                        using (DapperContext objDapper = new DapperContext())
                        {
                            int result = objDapper.Save<PurchaseOrderItem>(item);
                        }

                        //Pratik changes
                        if (item.JobOrderID != null && objService.ChangeJobOrderStatusByWorkorderNo(item.JobOrderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                        {
                            objService.NewJOStatusAudit(item.JobOrderID, SystemEnum.WorkRequestStatus.Approval.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                        }
                        //End Pratik changes

                    }

                    List<PurchaseOrderItem> lstToDelete = lstExistingItem.AsEnumerable().Where(p => !lstForUpdate.Any(ep => ep.ID == p.ID) && !lstToAdd.Any(ep => ep.ID == p.ID)).ToList();
                    foreach (var item in lstToDelete)
                    {
                        using (DapperContext objDapper = new DapperContext())
                        {
                            int result = objDapper.Delete<PurchaseOrderItem>(item.ID);
                        }
                    }

                    //PurchaseRequest table changes
                    List<PurchaseOrderItem> lst = model.lstPurchaseOrderItem.Where(x => x.PurchaseRequestItemID != null && x.PurchaseRequestItemID > 0).ToList();
                    foreach (var poItem in lst)
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            PurchaseRequestItem prItem = context.SelectObject<PurchaseRequestItem>(Convert.ToInt32(poItem.PurchaseRequestItemID));
                            string query = "Select SUM(Quantity) As Result from PurchaseOrderItems where PurchaseRequestItemID =" + prItem.ID;
                            prItem.ReceivedQty = Convert.ToDecimal(context.ExecuteScalar(query));

                            int result = context.Save<PurchaseRequestItem>(prItem);
                        }
                    }

                    foreach (var poItem in lst)
                    {
                        int counter1 = 0;
                        int counter2 = 0;
                        using (DapperContext context = new DapperContext())
                        {
                            PurchaseRequestItem prItem = context.SelectObject<PurchaseRequestItem>(Convert.ToInt32(poItem.PurchaseRequestItemID));
                            PurchaseRequest pr = context.SelectObject<PurchaseRequest>(Convert.ToInt32(prItem.PurchaseRequestID));
                            PurchaseRequestItem obj1 = new PurchaseRequestItem();
                            obj1.PurchaseRequestID = pr.Id;
                            List<PurchaseRequestItem> lstItems = context.Search<PurchaseRequestItem>(obj1).ToList();

                            foreach (var item in lstItems)
                            {
                                if (item.ReceivedQty >= item.Quantity)
                                    counter1 = counter1 + 1;
                                else if (item.ReceivedQty > 0 && item.ReceivedQty < item.Quantity)
                                    counter2 = counter2 + 1;
                            }

                            if (counter1 == lstItems.Count)
                                pr.Status_id = 5; //Fully Ordered
                            else if (counter2 > 0 || counter1 > 0)
                                pr.Status_id = 4; //Partially Ordered
                            else if (counter1 == 0 && counter2 == 0)
                                pr.Status_id = 1; //Open

                            int result = context.Save<PurchaseRequest>(pr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Good Receipt"

        public IList<Tran> GetPOItemTransactions(int pageNo, string sortExpression, string sortDirection, int id)
        {
            IList<Tran> list = new List<Tran>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "SELECT t.TransactionID,s.StockNo,s.StockDescription,s.AltStockDescription,poi.PartDescription,t.TransactionType,t.Quantity,poi.UnitPrice,t.TransDate,t.DeliveryOrderNo,t.InvoiceNo,t.InvoiceDate " +
                            "FROM trans t " +
                            "LEFT JOIN PurchaseOrderItems poi ON poi.ID = t.PurchaseOrderItemsId " +
                            "LEFT JOIN stockcode s ON s.StockID = poi.PartNumber " +
                            "LEFT JOIN PurchaseOrder po ON po.ID = poi.PurchaseOrderID " +
                            "WHERE po.ID = " + id;

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Tran>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public PoItemDetailModal GetTranDetailByPOItemID(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            IList<Tran> list = new List<Tran>();

            PoItemDetailModal objPoItemDetailModal = new PoItemDetailModal();

            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " SELECT poi.Quantity AS TotalQuantity,t.* FROM trans t " +
                            " LEFT JOIN PurchaseOrderItems poi ON poi.ID = t.PurchaseOrderItemsId " +
                            "Where poi.ID = @ID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Tran>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            if (list.Count > 0)
            {
                objPoItemDetailModal.LastReceivedDate = list.Max(o => o.TransDate);
                objPoItemDetailModal.TotalOrdered = ConvertTo.Decimal(list.Select(o => o.TotalQuantity).FirstOrDefault());
                objPoItemDetailModal.TotalReceived = ConvertTo.Decimal(list.Where(o => o.TransactionType == 1).Sum(o => o.Quantity));
                objPoItemDetailModal.TotalCancelled = ConvertTo.Decimal(list.Where(o => o.TransactionType == 2).Sum(o => o.Quantity));
                objPoItemDetailModal.Balance = ConvertTo.Decimal((objPoItemDetailModal.TotalOrdered - objPoItemDetailModal.TotalReceived - objPoItemDetailModal.TotalCancelled));
            }
            else
            {
                objPoItemDetailModal = null;
            }
            return objPoItemDetailModal;
        }

        #endregion

    }
}
