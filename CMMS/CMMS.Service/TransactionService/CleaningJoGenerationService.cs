﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.IO;
using Kendo.Mvc.UI;

namespace CMMS.Service.TransactionService
{
    public class CleaningJoGenerationService : DBExecute
    {
        public CleaningJoGenerationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public static List<PMGenDate> GetListPMGenDates(string cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = " SELECT [pmgendates].* FROM [pmgendates]   " +
                             " INNER JOIN ( SELECT [L2ID], [MaintdivId], [MaintdeptId], [MainSubDeptId], MAX([ToDate]) AS ToDate FROM [pmgendates] GROUP BY [L2ID], [MaintdivId], [MaintdeptId], [MainSubDeptId] )  PMGenDatesGroup   " +
                             " ON PMGenDatesGroup.[L2ID] = [pmgendates].[L2ID]    " +
                             " AND PMGenDatesGroup.[MaintdivId] = [pmgendates].[MaintdivId]   " +
                             " AND PMGenDatesGroup.[MaintdeptId] = [pmgendates].[MaintdeptId]   " +
                             " AND PMGenDatesGroup.[MainSubDeptId] = [pmgendates].[MainSubDeptId]   " +
                             " AND PMGenDatesGroup.[ToDate] = [pmgendates].[ToDate]   " +
                             " WHERE [pmgendates].[IsCleaningModule] = 1  " +
                             " AND [pmgendates].[L2ID] IN ( " + cityID + ") " +
                             " AND ([pmgendates].[MaintdivId] = " + maintenanceDiv + " OR [pmgendates].[MaintdivId] = 0)   " +
                             " AND ([pmgendates].[MaintdeptId] = " + maintenanceDept + " OR [pmgendates].[MaintdeptId] = 0) " +
                             " AND ([pmgendates].[MainSubDeptId] = " + maintenanceSubDept + " OR [pmgendates].[MainSubDeptId] = 0)  ";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PMGenDate>(query, parameters).ToList();
            }
        }

        public void clearCleaningLockIfStuck(int userID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "userID",
                    Value = userID,
                    DBType = DbType.Int32
                });

                string query = "Delete from PMLock where IsCleaningModule = 1 and  UserId = @userID";

                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveIntermediateDataByListL2ID(DateTime GenerateUntil, DateTime date1, int Userid, string L2IDs, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "GenerateUntil",
                        Value = GenerateUntil,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "GenerateUntil",
                        Value = Common.GetEnglishDate(GenerateUntil),
                        DBType = DbType.DateTime
                    });

                }

                //string query1 = "Delete from intermediate_multiplepm where Userid = " + Userid;
                string query2 = "Delete from intermediate_pmgenerate where IsCleaningModule = 1 and  Userid = " + Userid;
                string query3 = "Delete from intermediate_pmschedule where IsCleaningModule = 1 and  Userid = " + Userid;
                using (DapperDBContext context = new DapperDBContext())
                {
                    //context.ExecuteQuery(query1, parameters);
                    context.ExecuteQuery(query2, parameters);
                    context.ExecuteQuery(query3, parameters);
                }

                string strWhere = "";
                if (MaintDivisionID > 0)
                {
                    strWhere = strWhere + " and pmschedule.MaintDivisionID = " + MaintDivisionID;
                }
                if (maintDeptID > 0)
                {
                    strWhere = strWhere + " and pmschedule.maintDeptID = " + maintDeptID;
                }
                if (MaintSubDeptID > 0)
                {
                    strWhere = strWhere + " and pmschedule.MaintSubDeptID = " + MaintSubDeptID;
                }
                else if (Userid > 0)
                {
                    if (!ProjectSession.IsCentral)
                    {
                        strWhere = strWhere + " and pmschedule.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + Userid + " union Select MaintSubDeptID from employees where employeeId = " + Userid + " ) ";
                    }
                }

                string query4 = " INSERT INTO intermediate_pmschedule " +
                                " (PMID, L2ID, AssetID, PhyLocationID, ChecklistID, WorkTypeID, worktradeid, " +
                                " TypePMgenID, workpriorityid, PMNo, PMName, AltPMName, PeriodDays, NextSchComp, FreqUnits, Frequency, " +
                                " TargetStartDate, TargetCompDate, ActualCompDate, NextDate, TypePM, PMCounter, PMMultiple, PMActive, " +
                                " WeekofMonth, DayOfWeek, maintDeptID, MaintSubDeptID, MaintDivisionID, GroupId, Userid,IsCleaningModule) SELECT PMID, L2ID, " +
                                " AssetID, PhyLocationID, ChecklistID, WorkTypeID, worktradeid, TypePMgenID, workpriorityid, PMNo, PMName, " +
                                " AltPMName, PeriodDays, NextSchComp, FreqUnits, Frequency, TargetStartDate, TargetCompDate, ActualCompDate, " +
                                " NextDate, TypePM, PMCounter, PMMultiple, PMActive, WeekofMonth, DayofWeek, maintDeptID, MaintSubDeptID, " +
                                " MaintDivisionID, GroupId, "
                                 + Userid + " as Userid ,1 FROM pmschedule " +
                                 " WHERE IsCleaningModule = 1 and   PMActive = 1 and L2ID IN (" + L2IDs + ") and " +
                                 " ((TargetStartDate <= @GenerateUntil ) or (nextdate <= @GenerateUntil )) "
                                 + " " + strWhere;

                //string query5 = "INSERT INTO intermediate_multiplepm (PMID, MTaskID, MSeq, MDescription, MStartDate, MStartDateCount, MTaskSeq, " +
                //                " MScount, MPMCounter, MCount, Userid) SELECT multiplepm.PMID, MTaskID, MSeq, MDescription, MStartDate, " +
                //                " MStartDateCount, MTaskSeq, MScount, MPMCounter, 1 as MCount, " + Userid + " as Userid FROM multiplepm " +
                //                " inner join pmschedule on multiplepm.PMID = pmschedule.PMID " +
                //                " where pmschedule.L2ID IN (" + L2IDs + ") " +
                //                strWhere;
                using (DapperDBContext context = new DapperDBContext())
                {
                    context.ExecuteQuery(query4, parameters);
                    //context.ExecuteQuery(query5, parameters);
                }

                ProcessSavedIntermediateData(GenerateUntil, date1, Userid);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int zMonth, zYear, Dayno;
        public DateTime PMDate, tempDay, firstDay, lastDay;
        public void ProcessSavedIntermediateData(DateTime GenerateUntill, DateTime date1, int Userid)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            List<Intermediate_pmschedule> lstTempPMSchedule = new List<Intermediate_pmschedule>();
            List<Intermediate_multiplepm> lstTempMultiplePM = new List<Intermediate_multiplepm>();
            Intermediate_pmgenerate lstTempPMGen = new Intermediate_pmgenerate();

            string query1 = " Select * from intermediate_pmschedule where IsCleaningModule = 1 and  Userid = " + Userid;
            //string query2 = " Select * from intermediate_multiplepm where Userid = " + Userid;

            using (DapperDBContext context = new DapperDBContext())
            {
                lstTempPMSchedule = context.ExecuteQuery<Intermediate_pmschedule>(query1, parameters).ToList();
                //lstTempMultiplePM = context.ExecuteQuery<Intermediate_multiplepm>(query2, parameters).ToList();
            }

            try
            {
                int Task = 0;
                foreach (Intermediate_pmschedule tempPMSchedule in lstTempPMSchedule)
                {
                    //type of PM (schedule) if#1
                    if (tempPMSchedule.TypePMgenID == 1)
                    {
                        while (tempPMSchedule.TargetStartDate >= date1 & tempPMSchedule.TargetStartDate <= GenerateUntill)
                        {
                            Task = Convert.ToInt32(tempPMSchedule.ChecklistID);

                            //if (tempPMSchedule.PMMultiple == 1)
                            //{
                            //    List<Intermediate_multiplepm> lst = lstTempMultiplePM.Where(x => x.PMID == tempPMSchedule.PMID).ToList();

                            //    //loop to select which multiplepm to take
                            //    foreach (Intermediate_multiplepm item in lst)
                            //    {

                            //        if (tempPMSchedule.PMCounter == 1 | item.MPMCounter == 1)
                            //        {
                            //            item.MScount = item.MStartDateCount;
                            //            string query3 = "update intermediate_multiplepm set MScount = " + item.MScount + " where " + " userid = " + Userid + " and PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;

                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query3, parameters);
                            //            }
                            //        }
                            //        if (item.MScount == tempPMSchedule.PMCounter)
                            //        {
                            //            Task = Convert.ToInt32(item.MTaskID);
                            //            item.MScount = item.MScount + item.MSeq;
                            //            item.MPMCounter = item.MPMCounter + 1;
                            //            string query4 = "update intermediate_multiplepm set MScount = " + item.MScount + ", " + " MPMCounter = " + item.MPMCounter + " where " + " userid = " + Userid + " and  PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query4, parameters);
                            //            }
                            //        }
                            //    }
                            //}

                            //Add PM to generation temp
                            //Possibly take out the pm generation table by adding data straight away to the list
                            string query5 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                            " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                            " TargetStartDate, TargetCompDate, NextDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " +
                                            " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                            " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                            " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                            " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid, IsCleaningModule) " +
                                            " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                            " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " +
                                            Task + ", " + " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " +
                                            " ip.TargetStartDate, ip.TargetCompDate, ip.NextDate, " +
                                            " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                            " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                            " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                            " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                            " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " +
                                            " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," + 1 + ", " +
                                            " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " +
                                            " left join location on location.LocationID = ip.PhyLocationID " +
                                            " inner join pmchecklist on pmchecklist.ChecklistID =" + Task + "" +
                                            " left outer join assets on assets.AssetID = ip.AssetID " +
                                            " inner join L2 on L2.L2ID = ip.L2ID " +
                                            " left outer join L3 on L3.L3ID = location.L3ID " +
                                            " left outer join L4 on L4.L4ID = location.L4ID " +
                                            " left outer join L5 on L5.L5ID = location.L5ID " +
                                            " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                            " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                            " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " +
                                            " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" +
                                            " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" + " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" +
                                            " where ip.IsCleaningModule = 1 and   Userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;

                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query5, parameters);
                            }

                            //change the temp pm schedule to new pm schedule
                            tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                            tempPMSchedule.TargetStartDate = Convert.ToDateTime(tempPMSchedule.NextDate);
                            tempPMSchedule.TargetCompDate = tempPMSchedule.TargetStartDate.AddDays(Convert.ToInt32(tempPMSchedule.PeriodDays) - 1);
                            tempPMSchedule.NextDate = tempPMSchedule.TargetStartDate.AddDays(Convert.ToInt32(tempPMSchedule.Frequency) * Convert.ToInt32(tempPMSchedule.FreqUnits));

                            Collection<DBParameters> parameters6 = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = tempPMSchedule.TargetStartDate,
                                    DBType = DbType.DateTime
                                });
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = tempPMSchedule.TargetCompDate.Value,
                                    DBType = DbType.DateTime
                                });
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = tempPMSchedule.NextDate.Value,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetStartDate),
                                    DBType = DbType.DateTime
                                });
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetCompDate.Value),
                                    DBType = DbType.DateTime
                                });
                                parameters6.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.NextDate.Value),
                                    DBType = DbType.DateTime
                                });
                            }

                            string query6 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + ", " +
                                            " TargetStartDate = @TargetStartDate , " + " TargetCompDate = @TargetCompDate" +
                                            " , " + " NextDate = @NextDate where " + " userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query6, parameters6);
                            }
                        }
                    }
                    else if (tempPMSchedule.TypePMgenID == 2)
                    {
                        bool ActualCond = false;

                        if (tempPMSchedule.PMCounter >= 1)
                        {
                            ActualCond = (tempPMSchedule.TargetStartDate <= GenerateUntill);
                        }
                        else
                        {
                            //If Not IsNothing(tempPMSchedule.NextDate) Then
                            //    ActualCond = (tempPMSchedule.NextDate <= GenerateUntill)
                            //Else
                            ActualCond = false;
                            //End If
                        }

                        Nullable<int> lstJOStatus;

                        //string query7 = "Select WorkStatusID from WorkOrders where PMID = tempPMSchedule.PMID and WorkStatusID != 2 order by CreatedDate descending";
                        //using (DapperDBContext context = new DapperDBContext())
                        //{
                        //    lstJOStatus = context.ExecuteQuery<WorkOrder>(query7, parameters).FirstOrDefault().WorkStatusID;
                        //}

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            WorkOrder objWorkOrder = new WorkOrder();
                            List<WorkOrder> templst = context.SearchAll<WorkOrder>(objWorkOrder).ToList();
                            lstJOStatus = (from c in templst
                                           where c.PMID == tempPMSchedule.PMID && c.WorkStatusID != 2
                                           orderby c.CreatedDate descending
                                           select c.WorkStatusID).FirstOrDefault();
                        }

                        if ((lstJOStatus != null))
                        {
                            ActualCond = false;
                        }

                        if (ActualCond)
                        {
                            Task = Convert.ToInt32(tempPMSchedule.ChecklistID);
                            //Copy from PM Schedule to PMSchTemp
                            //if (tempPMSchedule.PMMultiple == 1)
                            //{
                            //    List<Intermediate_multiplepm> lst = lstTempMultiplePM.Where(x => x.PMID == tempPMSchedule.PMID).ToList();
                            //    //loop to select which multiplepm to take
                            //    foreach (Intermediate_multiplepm item in lst)
                            //    {
                            //        if (tempPMSchedule.PMCounter == 1 | item.MPMCounter == 1)
                            //        {
                            //            item.MScount = item.MStartDateCount;
                            //            string query8 = "update intermediate_multiplepm set MScount = " + item.MScount + " where " + " userid = " + Userid + " and  PMID = " +
                            //                            item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query8, parameters);
                            //            }
                            //        }
                            //        if (item.MScount == tempPMSchedule.PMCounter)
                            //        {
                            //            Task = Convert.ToInt32(item.MTaskID);
                            //            item.MScount = item.MScount + item.MSeq;
                            //            item.MPMCounter = item.MPMCounter + 1;
                            //            string query9 = "update intermediate_multiplepm set MScount = " + item.MScount + ", " + " MPMCounter = " + item.MPMCounter + " where " + " userid = " + Userid + " and  PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query9, parameters);
                            //            }
                            //        }
                            //    }
                            //}

                            if ((tempPMSchedule.TargetStartDate != null))
                            {
                                // PM WO will raise for first time generate PM
                                if (tempPMSchedule.PMCounter >= 1)
                                {
                                    string query10 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                        " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                        " TargetStartDate, TargetCompDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " + " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                        " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                        " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                        " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid,IsCleaningModule) " + " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                        " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " + Task + ", " +
                                        " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " + " ip.TargetStartDate, ip.TargetCompDate, " +
                                        " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                        " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                        " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                        " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                        " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " + " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," + 1 +
                                        ", " + " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " + " left join location on location.LocationID = ip.PhyLocationID " +
                                        " inner join pmchecklist on pmchecklist.ChecklistID =" + Task + " " + " left outer join assets on assets.AssetID = ip.AssetID " +
                                        " inner join L2 on L2.L2ID = ip.L2ID " + " left outer join L3 on L3.L3ID = location.L3ID " + " left outer join L4 on L4.L4ID = location.L4ID " +
                                        " left outer join L5 on L5.L5ID = location.L5ID " + " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                        " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                        " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " + " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" +
                                        " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" + " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" +
                                        " where ip.IsCleaningModule = 1 and Userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;

                                    using (DapperDBContext context = new DapperDBContext())
                                    {
                                        context.ExecuteQuery(query10, parameters);
                                    }

                                    tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                                    string query11 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + " where " + " userid = " + Userid +
                                                     " and PMID = " + tempPMSchedule.PMID;
                                    using (DapperDBContext context = new DapperDBContext())
                                    {
                                        context.ExecuteQuery(query11, parameters);
                                    }
                                }
                                //Exit Do
                            }
                            else
                            {
                                //PM generation will perform when Actual Comp Date provided
                                tempPMSchedule.TargetStartDate = Convert.ToDateTime(tempPMSchedule.NextDate);
                                tempPMSchedule.TargetCompDate = tempPMSchedule.TargetStartDate.AddDays(Convert.ToInt32(tempPMSchedule.PeriodDays) - 1);

                                Collection<DBParameters> parameters12 = new Collection<DBParameters>();
                                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                {
                                    parameters12.Add(new DBParameters()
                                    {
                                        Name = "TargetStartDate",
                                        Value = tempPMSchedule.TargetStartDate,
                                        DBType = DbType.DateTime
                                    });
                                    parameters12.Add(new DBParameters()
                                    {
                                        Name = "TargetCompDate",
                                        Value = tempPMSchedule.TargetCompDate.Value,
                                        DBType = DbType.DateTime
                                    });
                                }
                                else
                                {
                                    parameters12.Add(new DBParameters()
                                    {
                                        Name = "TargetStartDate",
                                        Value = Common.GetEnglishDate(tempPMSchedule.TargetStartDate),
                                        DBType = DbType.DateTime
                                    });
                                    parameters12.Add(new DBParameters()
                                    {
                                        Name = "TargetCompDate",
                                        Value = Common.GetEnglishDate(tempPMSchedule.TargetCompDate.Value),
                                        DBType = DbType.DateTime
                                    });

                                }


                                string query12 = "update intermediate_pmschedule set TargetStartDate = @TargetStartDate , " +
                                                " TargetCompDate = @TargetCompDate where " + " userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                                using (DapperDBContext context = new DapperDBContext())
                                {
                                    context.ExecuteQuery(query12, parameters12);
                                }

                                string query13 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                                 " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                                 " TargetStartDate, TargetCompDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " +
                                                 " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                                 " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                                 " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                                 " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid,IsCleaningModule) " + " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                                 " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " + Task + ", " +
                                                 " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " +
                                                 " ip.TargetStartDate, ip.TargetCompDate, " + " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                                 " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                                 " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                                 " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                                 " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " + " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," + 1 +
                                                 ", " + " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " + " left join location on location.LocationID = ip.PhyLocationID " +
                                                 " inner join pmchecklist on pmchecklist.ChecklistID = " + Task + " " + " left outer join assets on assets.AssetID = ip.AssetID " +
                                                 " inner join L2 on L2.L2ID = ip.L2ID " + " left outer join L3 on L3.L3ID = location.L3ID " + " left outer join L4 on L4.L4ID = location.L4ID " +
                                                 " left outer join L5 on L5.L5ID = location.L5ID " + " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                                 " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                                 " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " +
                                                 " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" + " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" +
                                                 " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" + " where ip.IsCleaningModule = 1 and Userid = " + Userid +
                                                 " and PMID = " + tempPMSchedule.PMID;
                                using (DapperDBContext context = new DapperDBContext())
                                {
                                    context.ExecuteQuery(query13, parameters);
                                }

                                tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                                string query14 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + " where " + " userid = " + Userid +
                                                 " and PMID = " + tempPMSchedule.PMID;
                                using (DapperDBContext context = new DapperDBContext())
                                {
                                    context.ExecuteQuery(query14, parameters);
                                }
                                //Exit Do
                            }
                        }
                    }
                    else if (tempPMSchedule.TypePMgenID == 3)
                    {
                        while (tempPMSchedule.TargetStartDate >= date1 & tempPMSchedule.TargetStartDate <= GenerateUntill)
                        {
                            Task = Convert.ToInt32(tempPMSchedule.ChecklistID);

                            //if (tempPMSchedule.PMMultiple == 1)
                            //{
                            //    List<Intermediate_multiplepm> lst = lstTempMultiplePM.Where(x => x.PMID == tempPMSchedule.PMID).ToList();
                            //    //loop to select which multiplepm to take
                            //    foreach (Intermediate_multiplepm item in lst)
                            //    {

                            //        if (tempPMSchedule.PMCounter == 1 | item.MPMCounter == 1)
                            //        {
                            //            item.MScount = item.MStartDateCount;
                            //            string query15 = "update intermediate_multiplepm set MScount = " + item.MScount + " where " + " userid = " + Userid + " and  PMID = " + item.PMID +
                            //                             " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query15, parameters);
                            //            }
                            //        }
                            //        if (item.MScount == tempPMSchedule.PMCounter)
                            //        {
                            //            Task = Convert.ToInt32(item.MTaskID);
                            //            item.MScount = item.MScount + item.MSeq;
                            //            item.MPMCounter = item.MPMCounter + 1;
                            //            string query16 = "update intermediate_multiplepm set MScount = " + item.MScount + ", " + " MPMCounter = " + item.MPMCounter + " where " + " userid = " +
                            //                             Userid + " and  PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query16, parameters);
                            //            }
                            //        }
                            //    }
                            //}

                            string query17 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                " TargetStartDate, TargetCompDate, NextDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " +
                                " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid,IsCleaningModule) " +
                                " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " + Task + ", " +
                                " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " +
                                " ip.TargetStartDate, ip.TargetCompDate, ip.NextDate, " + " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " + " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," + 1 + ", " +
                                " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " + " left join location on location.LocationID = ip.PhyLocationID " +
                                " inner join pmchecklist on pmchecklist.ChecklistID = " + Task + " " + " left outer join assets on assets.AssetID = ip.AssetID " +
                                " inner join L2 on L2.L2ID = ip.L2ID " + " left outer join L3 on L3.L3ID = location.L3ID " + " left outer join L4 on L4.L4ID = location.L4ID " +
                                " left outer join L5 on L5.L5ID = location.L5ID " + " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " + " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" +
                                " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" + " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" +
                                " where ip.IsCleaningModule = 1 and Userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query17, parameters);
                            }

                            //change here'
                            tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                            tempPMSchedule.TargetStartDate = Convert.ToDateTime(tempPMSchedule.NextDate);
                            tempPMSchedule.TargetCompDate = tempPMSchedule.TargetStartDate.AddDays(Convert.ToInt32(tempPMSchedule.PeriodDays) - 1);
                            zMonth = Convert.ToInt32(Microsoft.VisualBasic.DateAndTime.Month(Convert.ToDateTime(tempPMSchedule.NextDate)) + tempPMSchedule.Frequency);
                            zYear = Microsoft.VisualBasic.DateAndTime.Year(Convert.ToDateTime(tempPMSchedule.NextDate));
                            Dayno = Convert.ToInt32(tempPMSchedule.DayofWeek);
                            getNextDate(Convert.ToInt32(tempPMSchedule.WeekofMonth));
                            tempPMSchedule.NextDate = PMDate;

                            Collection<DBParameters> parameters18 = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = tempPMSchedule.TargetStartDate,
                                    DBType = DbType.DateTime
                                });
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = tempPMSchedule.TargetCompDate.Value,
                                    DBType = DbType.DateTime
                                });
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = tempPMSchedule.NextDate.Value,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetStartDate),
                                    DBType = DbType.DateTime
                                });
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetCompDate.Value),
                                    DBType = DbType.DateTime
                                });
                                parameters18.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.NextDate.Value),
                                    DBType = DbType.DateTime
                                });

                            }

                            string query18 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + ", " + " TargetStartDate = @TargetStartDate , " +
                                               " TargetCompDate = @TargetStartDate , " +
                                            " NextDate = @NextDate where " + " userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;

                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query18, parameters18);
                            }
                            //rsPMschTemp.MoveNext
                        }
                    }
                    else if (tempPMSchedule.TypePMgenID == 4)
                    {
                        while (tempPMSchedule.TargetStartDate >= date1 & tempPMSchedule.TargetStartDate <= GenerateUntill)
                        {
                            Task = Convert.ToInt32(tempPMSchedule.ChecklistID);

                            //if (tempPMSchedule.PMMultiple == 1)
                            //{
                            //    List<Intermediate_multiplepm> lst = lstTempMultiplePM.Where(x => x.PMID == tempPMSchedule.PMID).ToList();

                            //    //loop to select which multiplepm to take
                            //    foreach (Intermediate_multiplepm item in lst)
                            //    {
                            //        if (tempPMSchedule.PMCounter == 1 | item.MPMCounter == 1)
                            //        {
                            //            item.MScount = item.MStartDateCount;
                            //            string query19 = "update intermediate_multiplepm set MScount = " + item.MScount + " where " + " userid = " + Userid +
                            //                             " and PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query19, parameters);
                            //            }
                            //        }
                            //        if (item.MScount == tempPMSchedule.PMCounter)
                            //        {
                            //            Task = Convert.ToInt32(item.MTaskID);
                            //            item.MScount = item.MScount + item.MSeq;
                            //            item.MPMCounter = item.MPMCounter + 1;
                            //            string query20 = "update intermediate_multiplepm set MScount = " + item.MScount + ", " + " MPMCounter = " + item.MPMCounter + " where "
                            //                            + " userid = " + Userid + " and PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query20, parameters);
                            //            }
                            //        }
                            //    }
                            //}

                            string query21 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                            " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                            " TargetStartDate, TargetCompDate, NextDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " +
                                            " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                            " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                            " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                            " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid,IsCleaningModule) " +
                                            " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                            " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " + Task + ", " +
                                            " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " +
                                            " ip.TargetStartDate, ip.TargetCompDate, ip.NextDate, " + " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                            " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                            " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                            " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                            " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " + " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," + 1 +
                                            ", " + " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " +
                                            " left join location on location.LocationID = ip.PhyLocationID " +
                                            " inner join pmchecklist on pmchecklist.ChecklistID = " + Task + " " +
                                            " left outer join assets on assets.AssetID = ip.AssetID " + " inner join L2 on L2.L2ID = ip.L2ID " + " left outer join L3 on L3.L3ID = location.L3ID " +
                                            " left outer join L4 on L4.L4ID = location.L4ID " + " left outer join L5 on L5.L5ID = location.L5ID " +
                                            " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                            " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                            " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " +
                                            " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" +
                                            " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" + " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" +
                                            " where ip.IsCleaningModule = 1 and Userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;

                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query21, parameters);
                            }
                            //change here'
                            tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                            tempPMSchedule.TargetStartDate = Convert.ToDateTime(tempPMSchedule.NextDate);
                            tempPMSchedule.TargetCompDate = tempPMSchedule.TargetStartDate.AddDays(Convert.ToInt32(tempPMSchedule.PeriodDays) - 1);
                            PMDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", Convert.ToDouble(tempPMSchedule.Frequency), tempPMSchedule.NextDate);
                            tempPMSchedule.NextDate = PMDate;

                            Collection<DBParameters> parameters22 = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = tempPMSchedule.TargetStartDate,
                                    DBType = DbType.DateTime
                                });
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = tempPMSchedule.TargetCompDate.Value,
                                    DBType = DbType.DateTime
                                });
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = tempPMSchedule.NextDate.Value,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetStartDate),
                                    DBType = DbType.DateTime
                                });
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetCompDate.Value),
                                    DBType = DbType.DateTime
                                });
                                parameters22.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.NextDate.Value),
                                    DBType = DbType.DateTime
                                });

                            }

                            string query22 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + ", " + " TargetStartDate = @TargetStartDate , " +
                                             " TargetCompDate = @TargetStartDate, " +
                                              " NextDate = @NextDate where " + " userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query22, parameters22);
                            }
                        }
                    }
                    else if (tempPMSchedule.TypePMgenID == 5)
                    {
                        while (tempPMSchedule.TargetStartDate >= date1 & tempPMSchedule.TargetStartDate <= GenerateUntill)
                        {
                            Task = Convert.ToInt32(tempPMSchedule.ChecklistID);

                            //if (tempPMSchedule.PMMultiple == 1)
                            //{
                            //    List<Intermediate_multiplepm> lst = lstTempMultiplePM.Where(x => x.PMID == tempPMSchedule.PMID).ToList();
                            //    //loop to select which multiplepm to take
                            //    foreach (Intermediate_multiplepm item in lst)
                            //    {
                            //        if (tempPMSchedule.PMCounter == 1 | item.MPMCounter == 1)
                            //        {
                            //            item.MScount = item.MStartDateCount;
                            //            string query23 = "update intermediate_multiplepm set MScount = " + item.MScount + " where " + " userid = " + Userid +
                            //                             " and  PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query23, parameters);
                            //            }
                            //        }
                            //        if (item.MScount == tempPMSchedule.PMCounter)
                            //        {
                            //            Task = Convert.ToInt32(item.MTaskID);
                            //            item.MScount = item.MScount + item.MSeq;
                            //            item.MPMCounter = item.MPMCounter + 1;
                            //            string query24 = "update intermediate_multiplepm set MScount = " + item.MScount + ", " + " MPMCounter = " + item.MPMCounter +
                            //                             " where " + " userid = " + Userid + " and PMID = " + item.PMID + " and MTaskSeq=" + item.MTaskSeq;
                            //            using (DapperDBContext context = new DapperDBContext())
                            //            {
                            //                context.ExecuteQuery(query24, parameters);
                            //            }
                            //        }
                            //    }
                            //}

                            string query25 = "INSERT INTO intermediate_pmgenerate (ActualCompDate, PMID, PMNo, PMName, AltPMName, AssetID, AssetNumber, " +
                                             " AssetDescription, AssetAltDescription, ChecklistID, ChecklistNo, CheckListName, AltCheckListName, " +
                                             " TargetStartDate, TargetCompDate, NextDate, L2ID, L2Code, L3ID, L3No, L4ID, L4No, " +
                                             " L5ID, L5No, LocationID, LocationNo, LocationDescription, LocationAltDescription, " +
                                             " MaintDivisionID, MaintDivisionCode, maintDeptID, MaintDeptCode, MaintSubDeptID, MaintSubDeptCode, " +
                                             " WorkPriorityID, WorkPriority, AltWorkPriority, WorkTypeID, WorkTypeDescription, AltWorkTypeDescription, " +
                                             " WOTradeID, WorkTrade, AltWorkTrade, WOPMtype, TypePMgenID, Userid,IsCleaningModule) " + " SELECT ip.ActualCompDate, ip.PMID, ip.PMNo, ip.PMName, ip.AltPMName, " +
                                             " ip.AssetID, assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, " + Task + ", " +
                                             " pmchecklist.ChecklistNo, pmchecklist.CheckListName, pmchecklist.AltCheckListName, " + " ip.TargetStartDate, ip.TargetCompDate, ip.NextDate, " +
                                             " ip.L2ID, L2.L2Code, location.L3ID, L3.L3No, location.L4ID, L4.L4No, location.L5ID, L5.L5No, " +
                                             " ip.PhyLocationID, location.LocationNo, location.LocationDescription, location.LocationAltDescription, " +
                                             " ip.MaintDivisionID, MainenanceDivision.MaintDivisionCode, ip.maintDeptID, MaintenanceDepartment.MaintDeptCode, " +
                                             " ip.MaintSubDeptID, MaintSubDept.MaintSubDeptCode, " + " ip.workpriorityid, workpriority.WorkPriority, workpriority.AltWorkPriority, " +
                                             " ip.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, " + " ip.worktradeid, worktrade.WorkTrade, worktrade.AltWorkTrade," +
                                             1 + ", " + " ip.TypePMgenID, ip.Userid,1 FROM " + " intermediate_pmschedule ip " +
                                             " left join location on location.LocationID = ip.PhyLocationID " + " inner join pmchecklist on pmchecklist.ChecklistID = " + Task +
                                             " " + " left outer join assets on assets.AssetID = ip.AssetID " + " inner join L2 on L2.L2ID = ip.L2ID " +
                                             " left outer join L3 on L3.L3ID = location.L3ID " + " left outer join L4 on L4.L4ID = location.L4ID " +
                                             " left outer join L5 on L5.L5ID = location.L5ID " + " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = ip.MaintDivisionID " +
                                             " inner join MaintenanceDepartment on MaintenanceDepartment.MaintdeptId = ip.maintDeptID " +
                                             " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = ip.MaintSubDeptID " +
                                             " inner join worktrade on worktrade.WorkTradeID = ip.worktradeid" +
                                             " inner join worktype on worktype.WorkTypeID = ip.WorkTypeID" +
                                             " inner join workpriority on workpriority.WorkPriorityID = ip.workpriorityid" +
                                             " where ip.IsCleaningModule = 1 and Userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query25, parameters);
                            }

                            //change here'
                            tempPMSchedule.PMCounter = tempPMSchedule.PMCounter + 1;
                            tempPMSchedule.TargetStartDate = Convert.ToDateTime(tempPMSchedule.NextDate);
                            tempPMSchedule.TargetCompDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", 1, tempPMSchedule.TargetStartDate).AddDays(-1);
                            PMDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", Convert.ToDouble(tempPMSchedule.Frequency), tempPMSchedule.NextDate);
                            tempPMSchedule.NextDate = PMDate;

                            Collection<DBParameters> parameters26 = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = tempPMSchedule.TargetStartDate,
                                    DBType = DbType.DateTime
                                });
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = tempPMSchedule.TargetCompDate.Value,
                                    DBType = DbType.DateTime
                                });
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = tempPMSchedule.NextDate.Value,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "TargetStartDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetStartDate),
                                    DBType = DbType.DateTime
                                });
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "TargetCompDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.TargetCompDate.Value),
                                    DBType = DbType.DateTime
                                });
                                parameters26.Add(new DBParameters()
                                {
                                    Name = "NextDate",
                                    Value = Common.GetEnglishDate(tempPMSchedule.NextDate.Value),
                                    DBType = DbType.DateTime
                                });

                            }

                            string query26 = "update intermediate_pmschedule set PMCounter = " + tempPMSchedule.PMCounter + ", " + " TargetStartDate = @TargetStartDate , " +
                                " TargetCompDate = @TargetStartDate , " + " NextDate = @NextDate where " + " userid = " + Userid + " and PMID = " + tempPMSchedule.PMID;
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery(query26, parameters26);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetPMDayStart(int weeksDay)
        {
            if (weeksDay != Dayno)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(Dayno + (weeksDay < Dayno ? 0 : 7));
            }
            else
            {
                PMDate = tempDay;
            }
        }

        public void GetDayofPM(int weeksDay)
        {
            if (weeksDay > Dayno)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(Dayno - (weeksDay > Dayno ? 0 : 7));
            }
            else
            {
                if (weeksDay < Dayno)
                {
                    PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(Dayno - (weeksDay < Dayno ? 0 : 7));
                }
                else
                {
                    PMDate = tempDay;
                }
            }
        }

        public void GetPMDayLast(int weeksDay)
        {
            if (weeksDay != Dayno)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(Dayno - (weeksDay > Dayno ? 0 : 7));
            }
            else
            {
                PMDate = tempDay;
            }
        }

        public void getNextDate(int cboWeeks)
        {
            try
            {
                if (zMonth > 12)
                {
                    zYear = zYear + 1;
                    zMonth = zMonth - 12;
                }

                switch (cboWeeks)
                {
                    case 0:
                        firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                        tempDay = firstDay;
                        GetPMDayStart(Microsoft.VisualBasic.DateAndTime.Weekday(firstDay));
                        break;
                    case 1:
                    case 2:
                    case 3:
                        firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                        tempDay = Microsoft.VisualBasic.DateAndTime.DateAdd("ww", cboWeeks, firstDay);
                        GetDayofPM(Microsoft.VisualBasic.DateAndTime.Weekday(tempDay));
                        break;
                    case 4:
                        lastDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth + 1, 0);
                        tempDay = lastDay;
                        GetPMDayLast(Microsoft.VisualBasic.DateAndTime.Weekday(lastDay));
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Intermediate_pmgenerate> GetIntermediateDataByUserId_ScheduleDate(int userID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {

            IList<Intermediate_pmgenerate> list = new List<Intermediate_pmgenerate>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = " Select * from intermediate_pmgenerate " +
                            " where  IsCleaningModule = 1 and Userid = " + userID + " and TypePMgenID In (1,3,4,5) ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Intermediate_pmgenerate>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            if (list != null && list.Count > 0)
            {
                int reasonID = 0;
                JobOrderService objService = new JobOrderService();
                foreach (Intermediate_pmgenerate objIP in list)
                {
                    objIP.IsTargetStartDateHoliday = !objService.IsDateInWorkDaysAndTimeInWorkingHours(objIP.L2ID, Convert.ToDateTime(objIP.TargetStartDate), DateTime.MinValue, out reasonID);
                }
            }

            return list;
        }

        public IList<Intermediate_pmgenerate> GetIntermediateDataByUserId_CompletedDate(int userID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {

            IList<Intermediate_pmgenerate> list = new List<Intermediate_pmgenerate>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = " Select * from intermediate_pmgenerate " +
                            " where  IsCleaningModule = 1 and Userid = " + userID + " and TypePMgenID = 2 ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Intermediate_pmgenerate>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public bool SetCleaningLock(int userID, string L2IDs, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            int RecordCount = 0;
            if (MaintDivisionID <= 0)
            {
                string query1 = "Select count(1) As noOfData from PMLock  where IsCleaningModule = 1 and  L2ID IN (" + L2IDs + ")";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query1));
                }
            }
            else if (maintDeptID <= 0)
            {
                string query2 = "Select count(1) As noOfData from PMLock " + " where IsCleaningModule = 1 and  L2ID IN (" +
                                L2IDs + ") and (MaintDivisionID = 0 or MaintDivisionID = " + MaintDivisionID + ")";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query2));
                }
            }
            else if (MaintSubDeptID <= 0)
            {
                string query3 = "Select count(1) As noOfData from PMLock " + " where IsCleaningModule = 1 and  L2ID IN (" + L2IDs + ") and ((MaintDivisionID = " + MaintDivisionID +
                                " and maintDeptID = " + maintDeptID + ") or" + " (MaintDivisionID = 0) or (MaintDivisionID = " + MaintDivisionID + " and maintDeptID = 0))";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query3));
                }
            }
            else
            {
                string query4 = "Select count(1) As noOfData from PMLock " + " where IsCleaningModule = 1 and  L2ID IN (" + L2IDs + ") and ((MaintDivisionID = " + MaintDivisionID + " and " + " maintDeptID = " +
                                maintDeptID + " and MaintSubDeptID = " + MaintSubDeptID + ") " + " or (MaintDivisionID = 0) " + " or (MaintDivisionID = " + MaintDivisionID +
                                " and maintDeptID = 0 and MaintSubDeptID = 0) " + " or (MaintDivisionID = " + MaintDivisionID + " and maintDeptID = " + maintDeptID + " and MaintSubDeptID = 0)) ";
                using (DapperDBContext context = new DapperDBContext())
                {
                    RecordCount = Convert.ToInt32(context.ExecuteScalar(query4));
                }
            }

            if (RecordCount > 0)
            {
                return false;
            }
            else
            {
                string[] strL2IDs = L2IDs.Split(',');
                for (int i = 0; i < strL2IDs.Count(); i++)
                {
                    string query5 = "INSERT INTO PMLock (L2ID, MaintDivisionID, maintDeptID, MaintSubDeptID, Userid,IsCleaningModule) values " + " (" + strL2IDs[i] + ", " + MaintDivisionID + ", " + maintDeptID + ", " + MaintSubDeptID + ", " + userID + " , 1)";
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        context.ExecuteQuery(query5, parameters);
                    }
                }
                return true;
            }
        }

        public void GenerateCleaningJO(DateTime GenerateUntil, DateTime date1, int Userid, string L2IDs, int MaintDivisionID, int maintDeptID, int MaintSubDeptID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "date1",
                            Value = date1,
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "GenerateUntil",
                            Value = GenerateUntil,
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = DateTime.Now,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "date1",
                            Value = Common.GetEnglishDate(date1),
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "GenerateUntil",
                            Value = Common.GetEnglishDate(GenerateUntil),
                            DBType = DbType.DateTime
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = Common.GetEnglishDate(DateTime.Now),
                            DBType = DbType.DateTime
                        });
                    }
                    Int64 workordernumber;
                    string WONo = null;
                    List<string> lstwoForNotication = new List<string>();

                    List<Intermediate_pmgenerate> ObjectListPMGen = new List<Intermediate_pmgenerate>();
                    string query1 = "Select * from intermediate_pmgenerate " + " where IsCleaningModule = 1 and Userid = " + Userid;

                    ObjectListPMGen = context.ExecuteQuery<Intermediate_pmgenerate>(query1, parameters).ToList();


                    dynamic ListPMID = ObjectListPMGen.Select(Item => Item.PMID).Distinct().ToList();

                    PmSchedule objPmSchedule = new PmSchedule();
                    List<PmSchedule> ListObjectPMSchedule = context.SearchAll<PmSchedule>(objPmSchedule).Where(Item => ListPMID.Contains(Item.PMID)).ToList();
                    string query2 = string.Empty;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query2 = "SELECT WorkorderNo, L2ID FROM [workorders] WHERE lower([WorkorderNo]) LIKE lower('%PCM%') AND [L2ID] IN (" + L2IDs + ")";
                    }
                    else
                    {
                        query2 = "SELECT WorkorderNo, L2ID FROM [workorders] WHERE [WorkorderNo] LIKE '%PCM%' AND [L2ID] IN (" + L2IDs + ")";
                    }
                    List<WorkOrder> TempListJONo = context.ExecuteQuery<WorkOrder>(query2, parameters).ToList();

                    //string[] L2ID = L2IDs.Split(',');
                    foreach (string strL2ID in L2IDs.Split(','))
                    {
                        int L2ID = Convert.ToInt32(strL2ID);
                        List<Intermediate_pmgenerate> ObjectListPMGenByL2 = ObjectListPMGen.Where(Item => Item.L2ID == L2ID).ToList();


                        if ((ObjectListPMGenByL2 != null))
                        {
                            dynamic TempListJONoByL2 = TempListJONo.Where(Item => Item.L2ID == L2ID).Select(Item => Item.WorkorderNo).ToList();
                            Int64 CurrentMinNo = Convert.ToInt64(L2ID.ToString().PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));

                            foreach (string item in TempListJONoByL2)
                            {

                                if (Convert.ToInt64(item.Replace("PCM", "")) > CurrentMinNo)
                                {
                                    CurrentMinNo = Convert.ToInt64(item.Replace("PCM", ""));
                                }

                            }

                            workordernumber = CurrentMinNo + 1;
                            WONo = "PCM" + workordernumber.ToString().PadLeft(11, '0');

                            Collection<DBParameters> parameters1 = new Collection<DBParameters>();
                            foreach (Intermediate_pmgenerate ObjPMGen in ObjectListPMGenByL2)
                            {
                                parameters1 = new Collection<DBParameters>();
                                //unable to find supplierid in pmchecklist table for CPMMS
                                //![EmployeeID2] = GetSuppFromCheckList(rsPMGen![ChecklistNo]) - in CworksSQL

                                string strInsertColumn = "";
                                string strInsertValues = "";

                                if ((ObjPMGen.TargetStartDate != null))
                                {
                                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                    {
                                        parameters1.Add(new DBParameters()
                                        {
                                            Name = "TargetStartDate",
                                            Value = ObjPMGen.TargetStartDate,
                                            DBType = DbType.DateTime
                                        });

                                    }
                                    else
                                    {
                                        parameters1.Add(new DBParameters()
                                        {
                                            Name = "TargetStartDate",
                                            Value = Common.GetEnglishDate(ObjPMGen.TargetStartDate),
                                            DBType = DbType.DateTime
                                        });

                                    }

                                    strInsertColumn = strInsertColumn + " PMTarStartDate, ";
                                    strInsertValues = strInsertValues + "@TargetStartDate, ";
                                }
                                if ((ObjPMGen.TargetCompDate != null))
                                {

                                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                    {
                                        parameters1.Add(new DBParameters()
                                        {
                                            Name = "PMTarCompDate",
                                            Value = ObjPMGen.TargetStartDate,
                                            DBType = DbType.DateTime
                                        });
                                    }
                                    else
                                    {
                                        parameters1.Add(new DBParameters()
                                        {
                                            Name = "PMTarCompDate",
                                            Value = Common.GetEnglishDate(ObjPMGen.TargetStartDate),
                                            DBType = DbType.DateTime
                                        });
                                    }
                                    strInsertColumn = strInsertColumn + " PMTarCompDate, ";
                                    strInsertValues = strInsertValues + "@PMTarCompDate, ";
                                }
                                if ((ObjPMGen.WOTradeID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "WOTradeID",
                                        Value = ObjPMGen.WOTradeID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " WOTradeID, ";
                                    strInsertValues = strInsertValues + "@WOTradeID, ";
                                }
                                if ((ObjPMGen.ChecklistID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "ChecklistID",
                                        Value = ObjPMGen.ChecklistID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " PMChecklistID, ";
                                    strInsertValues = strInsertValues + "@ChecklistID , ";
                                }
                                if ((ObjPMGen.L3ID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "L3ID",
                                        Value = ObjPMGen.L3ID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " L3ID, ";
                                    strInsertValues = strInsertValues + "@L3ID , ";
                                }
                                if ((ObjPMGen.L4ID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "L4ID",
                                        Value = ObjPMGen.L4ID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " L4ID, ";
                                    strInsertValues = strInsertValues + " @L4ID , ";
                                }
                                if ((ObjPMGen.L5ID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "L5ID",
                                        Value = ObjPMGen.L5ID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " L5ID, ";
                                    strInsertValues = strInsertValues + "@L5ID, ";
                                }
                                if ((ObjPMGen.LocationID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "LocationID",
                                        Value = ObjPMGen.LocationID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " LocationID, ";
                                    strInsertValues = strInsertValues + "@LocationID , ";
                                }
                                if ((ObjPMGen.AssetID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "AssetID",
                                        Value = ObjPMGen.AssetID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " AssetID, ";
                                    strInsertValues = strInsertValues + "@AssetID, ";
                                }
                                if ((ObjPMGen.MaintDivisionID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "MaintDivisionID",
                                        Value = ObjPMGen.MaintDivisionID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " MaintDivisionID, ";
                                    strInsertValues = strInsertValues + "@MaintDivisionID, ";
                                }
                                if ((ObjPMGen.MaintDeptID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "MaintDeptID",
                                        Value = ObjPMGen.MaintDeptID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " maintdeptID, ";
                                    strInsertValues = strInsertValues + "@MaintDeptID, ";
                                }
                                if ((ObjPMGen.MaintSubDeptID != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "MaintSubDeptID",
                                        Value = ObjPMGen.MaintSubDeptID,
                                        DBType = DbType.Int32
                                    });

                                    strInsertColumn = strInsertColumn + " maintsubdeptID, ";
                                    strInsertValues = strInsertValues + "@MaintSubDeptID, ";
                                }

                                if ((ObjPMGen.AssetID != null))
                                {

                                    Asset objAsset = new Asset();
                                    objAsset.AssetID = Convert.ToInt32(ObjPMGen.AssetID);
                                    Nullable<int> AuthEmpId = context.Search<Asset>(objAsset).FirstOrDefault().EmployeeID;

                                    if ((AuthEmpId != null))
                                    {
                                        parameters1.Add(new DBParameters()
                                        {
                                            Name = "AuthEmpId",
                                            Value = AuthEmpId,
                                            DBType = DbType.Int32
                                        });

                                        strInsertColumn = strInsertColumn + " AuthorisedEmployeeID, ";
                                        strInsertValues = strInsertValues + "@AuthEmpId, ";

                                    }
                                }

                                PmSchedule ObjectPMSchedule = ListObjectPMSchedule.FirstOrDefault(Item => Item.PMID == ObjPMGen.PMID && (!string.IsNullOrEmpty(Item.ProjectNumber)));
                                if ((ObjectPMSchedule != null))
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "ProjectNumber",
                                        Value = ObjectPMSchedule.ProjectNumber,
                                        DBType = DbType.String
                                    });

                                    strInsertColumn = strInsertColumn + " ProjectNumber, ";
                                    strInsertValues = strInsertValues + "@ProjectNumber, ";
                                }

                                parameters1.Add(new DBParameters()
                                {
                                    Name = "WONo",
                                    Value = WONo,
                                    DBType = DbType.String
                                });
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "ProblemDescription",
                                    Value = ObjPMGen.PMNo + " - " + ObjPMGen.PMName + Environment.NewLine + ObjPMGen.AltPMName,
                                    DBType = DbType.String
                                });

                                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "CurrentDate",
                                        Value = DateTime.Now,
                                        DBType = DbType.DateTime
                                    });
                                }
                                else
                                {
                                    parameters1.Add(new DBParameters()
                                    {
                                        Name = "CurrentDate",
                                        Value = Common.GetEnglishDate(DateTime.Now),
                                        DBType = DbType.DateTime
                                    });
                                }

                                string query3 = "INSERT INTO workorders ( " + strInsertColumn + " WorkorderNo, DateReceived, ProblemDescription, " +
                                                " WOPMtype, WorkTypeID, WorkStatusID, WorkPriorityID, PMID, EmployeeID, " + " L2ID, CreatedBy, CreatedDate) values " + " ( " + strInsertValues +
                                                "@WONo , @CurrentDate , " + " @ProblemDescription, " + " '1', 2, 1, 1, " + ObjPMGen.PMID + ", " + ObjPMGen.Userid + ", " + ObjPMGen.L2ID + ", " + " " + ObjPMGen.Userid + ", " +
                                                "@CurrentDate)";

                                context.ExecuteQuery(query3, parameters1);

                                lstwoForNotication.Add(WONo);
                                ExtAssetFile objExtAssetFile = new ExtAssetFile();
                                ExtAssetFile ObjectExtassetFiles = (from c in context.SearchAll<ExtAssetFile>(objExtAssetFile) where (c.FkId == ObjPMGen.ChecklistID.ToString() & c.ModuleType == "P") select c).FirstOrDefault();

                                if ((ObjectExtassetFiles != null))
                                {
                                    string strSourceFile = AppDomain.CurrentDomain.BaseDirectory + "Attachments/PMChecklist/" + ObjectExtassetFiles.FkId + "/" + ObjectExtassetFiles.FileName;

                                    if (File.Exists(strSourceFile))
                                    {
                                        string strdestFile = AppDomain.CurrentDomain.BaseDirectory + "Attachments/JobOrder/" + WONo + "/" + ObjectExtassetFiles.FileName;
                                        if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "Attachments/JobOrder/" + WONo))
                                        {
                                            Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "Attachments/JobOrder/" + WONo);
                                        }
                                        File.Copy(strSourceFile, strdestFile);
                                    }

                                    ExtAssetFile ObjectnewExtassetFiles = new ExtAssetFile();
                                    ObjectnewExtassetFiles.FkId = WONo;
                                    ObjectnewExtassetFiles.FileName = ObjectExtassetFiles.FileName;
                                    ObjectnewExtassetFiles.FileDescription = ObjectExtassetFiles.FileDescription;
                                    ObjectnewExtassetFiles.ModuleType = "J";
                                    ObjectnewExtassetFiles.CreatedBy = ObjPMGen.Userid;
                                    ObjectnewExtassetFiles.CreatedDate = System.DateTime.Now;
                                    ObjectnewExtassetFiles.ModifiedBy = ObjPMGen.Userid;
                                    ObjectnewExtassetFiles.ModifiedDate = System.DateTime.Now;
                                    ObjectnewExtassetFiles.FileLink = "Attachments/JobOrder/" + WONo + "/" + ObjectExtassetFiles.FileName;

                                    context.Save<ExtAssetFile>(ObjectnewExtassetFiles);

                                }

                                PMCheckList objPMCheckList = new PMCheckList();
                                int? tempSafetyNo = (from c in context.SearchAll<PMCheckList>(objPMCheckList) select c).Where(x => x.ChecklistID == ObjPMGen.ChecklistID).FirstOrDefault().SafetyID;

                                if ((tempSafetyNo != null) & tempSafetyNo > 0)
                                {
                                    AddSafetyItemsToWo(WONo, Convert.ToInt32(tempSafetyNo));
                                }
                                GenChecklistElementAndItems(WONo, Convert.ToInt32(ObjPMGen.ChecklistID));
                                AddAssignToToWo(WONo, ObjPMGen.PMID);
                                AddWOStatusAudit(WONo, Userid);
                                ObjPMGen.WorkorderNo = WONo;

                                string query4 = "update intermediate_pmgenerate set WorkorderNo = '" + ObjPMGen.WorkorderNo + "' where " + " PMGenerateID = " + ObjPMGen.PMGenerateID;
                                context.ExecuteQuery(query4, parameters);

                                workordernumber = workordernumber + 1;
                                WONo = "PCM" + workordernumber.ToString().PadLeft(11, '0');
                            }
                        }
                    }
                    //Region WOPM End

                    //Region UpdatePmSchedule Start
                    string query5 = "Select * from intermediate_pmschedule  where IsCleaningModule = 1 and Userid = " + Userid;
                    List<Intermediate_pmschedule> ObjectListPMSch = context.ExecuteQuery<Intermediate_pmschedule>(query5, parameters).ToList();

                    foreach (Intermediate_pmschedule ObjPMSch in ObjectListPMSch)
                    {
                        Collection<DBParameters> parameters6 = new Collection<DBParameters>();

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            parameters6.Add(new DBParameters()
                            {
                                Name = "TargetStartDate",
                                Value = ObjPMSch.TargetStartDate,
                                DBType = DbType.DateTime
                            });
                            parameters6.Add(new DBParameters()
                            {
                                Name = "TargetCompDate",
                                Value = ObjPMSch.TargetCompDate.Value,
                                DBType = DbType.DateTime
                            });
                            parameters6.Add(new DBParameters()
                            {
                                Name = "NextDate",
                                Value = ObjPMSch.NextDate.Value,
                                DBType = DbType.DateTime
                            });
                        }
                        else
                        {
                            parameters6.Add(new DBParameters()
                            {
                                Name = "TargetStartDate",
                                Value = Common.GetEnglishDate(ObjPMSch.TargetStartDate),
                                DBType = DbType.DateTime
                            });
                            parameters6.Add(new DBParameters()
                            {
                                Name = "TargetCompDate",
                                Value = Common.GetEnglishDate(ObjPMSch.TargetCompDate.Value),
                                DBType = DbType.DateTime
                            });
                            parameters6.Add(new DBParameters()
                            {
                                Name = "NextDate",
                                Value = Common.GetEnglishDate(ObjPMSch.NextDate.Value),
                                DBType = DbType.DateTime
                            });
                        }

                        string strInsertColumn = "";

                        if ((ObjPMSch.TargetStartDate != null))
                        {
                            strInsertColumn = strInsertColumn + " TargetStartDate = @TargetStartDate , ";
                        }
                        if ((ObjPMSch.TargetCompDate != null))
                        {
                            strInsertColumn = strInsertColumn + " TargetCompDate = @TargetCompDate, ";
                        }
                        if ((ObjPMSch.NextDate != null))
                        {
                            strInsertColumn = strInsertColumn + " NextDate = @NextDate , ";
                        }
                        if ((ObjPMSch.PMCounter != null))
                        {
                            strInsertColumn = strInsertColumn + " PMCounter = " + ObjPMSch.PMCounter;
                        }

                        string query6 = "UPDATE pmschedule SET " + strInsertColumn + " where " + " PMID = " + ObjPMSch.PMID;
                        context.ExecuteQuery(query6, parameters6);

                        if (ObjPMSch.TypePMgenID == 2)
                        {
                            string query7 = "UPDATE pmschedule SET " + " NextDate = NULL, ActualCompDate = NULL " + " where PMID = " + ObjPMSch.PMID;
                            context.ExecuteQuery(query7, parameters);
                        }
                    }

                    //string query8 = "Select * from intermediate_multiplepm " + " where Userid = " + Userid;
                    //List<Intermediate_multiplepm> ObjectListMultiplePM = context.ExecuteQuery<Intermediate_multiplepm>(query8, parameters).ToList();

                    //foreach (Intermediate_multiplepm ObjMultiplePM in ObjectListMultiplePM)
                    //{

                    //    string query9 = "UPDATE multiplepm SET " + " MSCount= " + ObjMultiplePM.MScount + ", " + " MPMCounter = " + ObjMultiplePM.MPMCounter + " where " + " PMID = " + ObjMultiplePM.PMID +
                    //                    " and MTaskSeq = " + ObjMultiplePM.MTaskSeq;
                    //    context.ExecuteQuery(query9, parameters);
                    //}


                    foreach (string strL2ID in L2IDs.Split(','))
                    {
                        int L2ID = Convert.ToInt32(strL2ID);
                        string query10 = "INSERT INTO pmgendates (L2ID, FromDate, ToDate, DateGenerate, MaintdeptId, MainSubDeptId, MaintdivId, " + " CreatedBy, CreatedDate,IsCleaningModule) " +
                                         " values (" + L2ID + ", @date1, @GenerateUntil, @CurrentDate , " + " " + maintDeptID +
                                         ", " + MaintSubDeptID + ", " + MaintDivisionID + ", " + " " + Userid + ", @CurrentDate,1)";
                        context.ExecuteQuery(query10, parameters);

                        string query11 = "Delete from PMLock where L2ID = " + L2ID + " and MaintDivisionID = " + MaintDivisionID + " and " + " maintDeptID = " + maintDeptID +
                                          " and MaintSubDeptID = " + MaintSubDeptID + " and IsCleaningModule = 1 and UserId = " + Userid;
                        context.ExecuteQuery(query11, parameters);
                    }
                    //Region UpdatePmSchedule End

                    foreach (string strWo in lstwoForNotication)
                    {
                        EmailConfigurationService objService = new EmailConfigurationService();
                        objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkOrder, SystemEnum.ModuleEvent.JobOrderCreation, strWo, string.Empty, "1");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddSafetyItemsToWo(string WONo, int SafetyID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });
                using (ServiceContext context = new ServiceContext())
                {
                    string query = "INSERT INTO SITask (SeqId, Details, WoNo) " + " select seqID, Description,@WONo from tblSafetyInstructionItems " + " where SafetyID = " + SafetyID;
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GenChecklistElementAndItems(string WONo, int ChecklistID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });
                using (ServiceContext context = new ServiceContext())
                {
                    string query1 = "INSERT INTO workorderelements (WorkorderNo, SeqNo, TaskDesc, AltTaskDesc) " + " select " +
                                    "@WONo, SeqNo, TaskDesc, AltTaskDesc from checklistelements " + " where ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query1, parameters);

                    string query2 = "INSERT INTO WorkOrderItems (StockID, WorkOrderNo, SeqNo, Qty) " + " select checklistinv.PartID, " +
                                     "@WONo, checklistelements.SeqNo, checklistinv.Qty from checklistinv " +
                                    " inner join checklistelements on checklistInv.ChecklistelementId = checklistelements.ChecklistelementId " +
                                    " where checklistinv.ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query2, parameters);

                    string query3 = "INSERT INTO WorkOrderTools ( WorkOrderNo, SeqNo, ToolDescription, AltToolDescription, Quantity ) " +
                                    " SELECT @WONo, SeqNo, ToolDescription, AltToolDescription, Quantity FROM ChecklistTools " + " WHERE ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query3, parameters);

                    string query4 = "INSERT INTO WorkOrderPPE ( WorkOrderNo, SeqNo, PPEDescription, AltPPEDescription ) " +
                                    " SELECT @WONo, SeqNo, PPEDescription, AltPPEDescription FROM ChecklistPPE " + " WHERE ChecklistID = " + ChecklistID;
                    context.ExecuteQuery(query4, parameters);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddAssignToToWo(string WONo, int PMID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });
                using (ServiceContext context = new ServiceContext())
                {
                    string query = "INSERT INTO assigntoworkorder (EmployeeID, WorkorderNo) " + " select EmployeeID,@WONo from pmschedule_assignto " + " where PMID = " + PMID;
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddWOStatusAudit(string WONo, int Userid)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "WONo",
                    Value = WONo,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "Userid",
                    Value = Userid,
                    DBType = DbType.Int32
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                string query = string.Empty;

                using (ServiceContext context = new ServiceContext())
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = "INSERT INTO WOStatusAudit (WONo, WOStatus, Date_, EmpID, source) " + " values (@WONo ,1, @CurrentDate,@Userid,'PM JO Generation')";
                    }
                    else
                    {
                        query = "INSERT INTO WOStatusAudit (WONo, WOStatus, Date, EmpID, source) " + " values (@WONo ,1, @CurrentDate,@Userid,'PM JO Generation')";
                    }
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
