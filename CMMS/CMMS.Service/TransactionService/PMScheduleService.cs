﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PMScheduleService : DBExecute
    {
        public PMScheduleService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "PM Schedule"

        public static PmSchedule GetPmScheduleById(int timeID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "timeID",
                Value = timeID,
                DBType = DbType.Int32
            });
            string query = " Select  pmschedule.*, " +
                            " Assets.AssetID,Assets.AssetNumber,Assets.AssetDescription,Assets.AssetAltDescription, " +
                            " LocationAsset.LocationID LocationIDAsset,LocationAsset.LocationNo LocationNoAsset,LocationAsset.LocationDescription LocationDescriptionAsset,LocationAsset.LocationAltDescription LocationAltDescriptionAsset, " +
                            " Location.LocationID,Location.LocationNo,Location.LocationDescription,Location.LocationAltDescription, " +
                            " pmchecklist.ChecklistNo , pmchecklist.CheckListName , pmchecklist.AltCheckListName " +
                            " from pmschedule  " +
                            " INNER JOIN pmchecklist on pmchecklist.ChecklistID = pmschedule.ChecklistID " +
                            " LEFT JOIN Assets on Assets.AssetID = pmschedule.AssetID " +
                            " LEFT JOIN Location LocationAsset on LocationAsset.LocationID = Assets.LocationID " +
                            " LEFT JOIN Location on Location.LocationID = pmschedule.PhyLocationID " +
                            "Where PMID = @timeID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PmSchedule>(query, parameters).FirstOrDefault();
            }

        }

        #endregion

        #region "Mutliple PM checkList"

        public IList<MultiplePM> GetMultiPMList(int pmID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<MultiplePM> list = new List<MultiplePM>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "SELECT PC.ChecklistNo, " +
                       "MP.* " +
           "FROM  [multiplepm] MP " +
           "INNER JOIN  [pmschedule] PS ON MP.PMID = PS.PMID " +
           "INNER JOIN [PMCheckList] PC ON MP.MTaskID = PC.CheckListId " +
           "where MP.PMID =" + pmID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<MultiplePM>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<PmForecastTemp> ApplyPagging(IList<PmForecastTemp> lst, int pageNo)
        {
            try
            {
                using (DapperContext context = new DapperContext())
                {
                    return context.ApplyPagging<PmForecastTemp>(lst, pageNo);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ManageMultiplePM(MultiplePM obj)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "PMID",
                        Value = obj.PMID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MTaskID",
                        Value = obj.MTaskID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MSeq",
                        Value = obj.MSeq,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MDescription",
                        Value = obj.MDescription,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MStartDate",
                        Value = obj.MStartDate,
                        DBType = DbType.DateTime
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MStartDateCount",
                        Value = obj.MStartDateCount,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MTaskSeq",
                        Value = obj.MTaskSeq,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "MScount",
                        Value = obj.MScount,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "MPMCounter",
                        Value = obj.MPMCounter,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = obj.CreatedBy,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = obj.CreatedDate,
                        DBType = DbType.DateTime
                    });

                    string query = "INSERT INTO multiplepm (PMID,MTaskID,MSeq,MDescription,MStartDate,MStartDateCount,MTaskSeq,MScount,MPMCounter,CreatedBy,CreatedDate)";
                    query += " VALUES(@PMID ,@MTaskID ,@MSeq ,@MDescription ,@MStartDate ,@MStartDateCount ,@MTaskSeq ,@MScount ,@MPMCounter ,@CreatedBy,@CreatedDate)";
                    context.ExecuteQuery(query, parameters);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteMultiplePM(int timeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                query = "delete from multiplepm where pmID = " + timeID;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ManageMultiplePMForAsset(int oldPMID, int newPMID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                    string query = "insert into MultiplePM select " + newPMID + ",MTaskID,MSeq,MDescription,MStartDate,MStartDateCount,MTaskSeq,MScount,MPMCounter,'" + ProjectSession.EmployeeID + "',@CurrentDate,NULL,NULL from MultiplePM where PMID = " + oldPMID;

                    context.ExecuteQuery(query, parameters);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountOfMultiplePMByPMId(int timeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    string query = "select count(1) from multiplepm where PMID = " + timeID;

                    string count = context.ExecuteScalar(query);
                    return Convert.ToInt32(count);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "PM Assign To"
        public IList<PmSchedule_AssignTo> GetPMAssignList(int pmID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {

            IList<PmSchedule_AssignTo> list = new List<PmSchedule_AssignTo>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = "Select pmAssignTo.*,employees.EmployeeNO,Name   " +
                           "from pmschedule_assignto pmAssignTo " +
                           "inner join employees on employees.EmployeeID = pmAssignTo.EmployeeID  " +
                           "where pmAssignTo.PMID =" + pmID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PmSchedule_AssignTo>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static void ManagePMAssignToEmployeeGroup(int timeID, int groupID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    string query = "insert into pmschedule_assignto Select " + timeID + " ,EmployeeId from EmployeesMaintGroups where MaintGroupId = " + groupID;
                    query += " And EmployeeID Not In (Select Distinct EmployeeID from pmschedule_assignto where PMID = " + timeID + " )";
                    context.ExecuteQuery(query, parameters);

                    string query1 = "update pmschedule set GroupID = " + groupID + " where pmID = " + timeID;
                    context.ExecuteQuery(query1, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ManagePMAssignToEmployee(int timeID, int employeeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    string query = "insert into pmschedule_assignto values( " + timeID + " , " + employeeID + ")";
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeletePmScheduleAssignto(int timeID, int employeeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                query = "delete from pmschedule_assignto where pmID = " + timeID + " And employeeID= " + employeeID;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool ManagePmScheduleAssigntoForAsset(int oldPMID, int newPMID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    string query = "insert into pmschedule_assignto select " + newPMID + ",EmployeeID from pmschedule_assignto where PMID = " + oldPMID;

                    context.ExecuteQuery(query, parameters);
                    return true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int GetCountOfPmScheduleAssigntoByPMId(int timeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    string query = "select count(1) from pmschedule_assignto where PMID = " + timeID;

                    string count = context.ExecuteScalar(query);
                    return Convert.ToInt32(count);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "PM Group"
        public static List<PmSchedule> GetPmAssetScheduleByPMGroupNO(string groupNO)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "groupNO",
                Value = groupNO,
                DBType = DbType.String
            });
            string query = " Select  pmschedule.*, " +
                            " Assets.AssetID,Assets.AssetNumber,Assets.AssetDescription,Assets.AssetAltDescription, " +
                            " LocationAsset.LocationID LocationIDAsset,LocationAsset.LocationNo LocationNoAsset,LocationAsset.LocationDescription LocationDescriptionAsset,LocationAsset.LocationAltDescription LocationAltDescriptionAsset, " +
                            " Location.LocationID,Location.LocationNo,Location.LocationDescription,Location.LocationAltDescription, " +
                            " pmchecklist.ChecklistNo , pmchecklist.CheckListName , pmchecklist.AltCheckListName " +
                            " from pmschedule  " +
                            " INNER JOIN pmchecklist on pmchecklist.ChecklistID = pmschedule.ChecklistID " +
                            " LEFT JOIN Assets on Assets.AssetID = pmschedule.AssetID " +
                            " LEFT JOIN Location LocationAsset on LocationAsset.LocationID = Assets.LocationID " +
                            " LEFT JOIN Location on Location.LocationID = pmschedule.PhyLocationID " +
                            "Where PMGroupNo = @groupNO";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PmSchedule>(query, parameters).ToList();
            }

        }

        public static bool DeleteAllPmScheduleAssignto(int timeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                query = "delete from pmschedule_assignto where pmID = " + timeID; ;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Cleaning Schedule"

        public static PmSchedule GetPmScheduleByCleaningId(int timeID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "timeID",
                Value = timeID,
                DBType = DbType.Int32
            });
            string query = "  Select " +
                         " pmschedule.*, " +
                         " pmchecklist.ChecklistNo,  pmchecklist.CheckListName,pmchecklist.AltCheckListName, " +
                         " location.LocationNo,location.LocationDescription,location.LocationAltDescription, " +
                         " CIGID , cleaningInspectionGroup.GroupNumber As CIGGroupNumber, " +
                         " cleaningInspectionGroup.GroupDescription As CIGGroupDescription , cleaningInspectionGroup.ArabicGroupDescription As CIGArabicGroupDescription " +
                         " FROM pmschedule  " +
                         " INNER JOIN pmchecklist ON pmchecklist.ChecklistID= pmschedule.ChecklistID    " +
                         " left join location ON location.LocationID= pmschedule.PhyLocationID  " +
                         " LEFT OUTER join cleaningInspectionGroup  ON cleaningInspectionGroup.CleaningInspectionGroupId= pmschedule.CIGID " +
                         " where pmschedule.IsCleaningModule = 1  and PMID = @timeID ";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<PmSchedule>(query, parameters).FirstOrDefault();
            }
        }

        public static int GetCleaningScheduleByScheduleslistNo(int timeID, string timeNo)
        {
            int count = 0;

            if (timeID > 0)
            {
                string query = " Select count(1) from pmschedule where IsCleaningModule = 1 and PMNo = '" + Common.setQuote(timeNo) + "' and PMID !=  " + timeID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
            else
            {
                string query = " Select count(1) from pmschedule where IsCleaningModule = 1 and PMNo = '" + Common.setQuote(timeNo) + "'";

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
        }

        #endregion
    }
}
