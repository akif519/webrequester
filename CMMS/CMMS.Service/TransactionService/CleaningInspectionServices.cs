﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CMMS.Service
{
    public class CleaningInspectionServices : DBExecute
    {
        public CleaningInspectionServices()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<Location> GetCleaningLocationList()
        {
            try
            {
                IList<Location> list = new List<Location>();
                string query = string.Empty;


                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "select * from location inner join L2 " +
                          "on location.L2ID = L2.L2ID ";
                /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                if (!ProjectSession.IsCentral)
                {
                    query += " inner join employees_L2 el2 on el2.L2ID = L2.L2ID AND el2.EmpID = " + ProjectSession.EmployeeID;

                    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        query += " inner join employees_Location el on el.locationID = location.locationID AND el.EmpID = " + ProjectSession.EmployeeID;
                    }
                }
                /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                query += " left join criticality " +
                "on location.criticalityid = criticality.id " +
                "left outer join L3 on " +
                "location.L3ID = L3.L3ID " +
                "left outer join L4 on " +
                "location.L4ID = L4.L4ID " +
                "left outer join L5 on  " +
                "location.L5ID = L5.L5ID " +
                "inner join cleaningclassification " +
                "on location.CleaningClassificationId = cleaningclassification.CleaningClassificationID ";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Location>(query, parameters, 1);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveWorkOrderSafetyInstrItem(int cleaningInspectionGroupId, List<CleaningInspectionGroupSequenceNumber> lstSequenceNumbers)
        {
            try
            {
                string queryDelete = "Delete from cleaningInspectionGroupSequenceNumber Where CleaningInspectionGroupId = " + cleaningInspectionGroupId;
                string queryInsert = string.Empty;

                CleaningInspectionGroupSequenceNumber objJobTask = new CleaningInspectionGroupSequenceNumber();
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(queryDelete, new Collection<DBParameters>());

                    lstSequenceNumbers.ForEach(p =>
                    {
                        Collection<DBParameters> parameters = new Collection<DBParameters>();
                        parameters.Add(new DBParameters()
                        {
                            Name = "CreatedDate",
                            Value = p.CreatedDate,
                            DBType = DbType.DateTime
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = p.ModifiedDate,
                            DBType = DbType.DateTime
                        });

                        queryInsert = "Insert Into cleaningInspectionGroupSequenceNumber(CleaningInspectionGroupId,SequenceNumber,LocationId,TaskDesc,AltTaskDesc,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)"
                                + " Values(" + p.CleaningInspectionGroupId + "," + p.SequenceNumber + ",'" + p.LocationId + "','" + Common.setQuote(p.TaskDesc) + "','" + Common.setQuote(p.AltTaskDesc) + "','" + p.CreatedBy + "',@CreatedDate,'" + p.ModifiedBy + "',@ModifiedDate )";
                        objContext.ExecuteQuery(queryInsert, parameters);
                    }
                        );
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public static List<employees> GetEmployeeAuto(string text)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                string strWhere = string.Empty;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhere = " and (lower(EmployeeNO) like N'%" + text + "%' or lower(Name) like N'%" + text + "%' or lower(Altname) like N'%" + text + "%')";
                }
                else 
                {
                    strWhere = " and (EmployeeNO like N'%" + text + "%' or Name like N'%" + text + "%' or Altname like N'%" + text + "%')";
                }

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                query = "select EmployeeID,EmployeeNO,Name,Altname from employees where 1=1 " + strWhere + " ";

                return context.ExecuteQuery<employees>(query, parameters).ToList();
            }
        }

    }
}
