﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class JobRequestService : DBExecute
    {
        public JobRequestService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<Worequest> GetJobRequests(int empId, bool IsCentral, int pageNo, string strWhere, string sortExpression, string sortDirection)
        {
            IList<Worequest> list = new List<Worequest>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "worequest.CreatedDate desc, worequest.workorderno";
            }

            string strWhereClauseWithEmployee = string.Empty;

            if (strWhere == string.Empty)
            {
                strWhere = "and worequest.requeststatusid IN (1, 8) ";
            }


            if (!IsCentral)
            {
                strWhereClauseWithEmployee = " and worequest.L2ID in (select L2Id from employees_L2 where empID = " + empId + " union Select L2Id from employees where employeeId = " + empId + " ) ";
                strWhereClauseWithEmployee += " and worequest.MainSubDeptId in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empId + " union Select MaintSubDeptID from employees where employeeId = " + empId + " ) ";
            }

            string query = "SELECT  worequest.RequestNo,worequest.WorkOrderNo,worequest.ProblemDesc,workstatus.WorkStatus as workstatusName,workstatus.AltWorkStatus," +
                           " worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,ExternalRequesterName,workpriority.WorkPriority as workpriorityName," +
                           " workpriority.AltWorkPriority,asset.AssetNumber, asset.AssetDescription,asset.AssetAltDescription," +
                           " location.LocationNo,location.LocationDescription,location.LocationAltDescription,L2.L2Code,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName,L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class,L3.L3No," +
                           " L3.L3Desc,L3.L3AltDesc,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description,L5.L5AltDescription," +
                           " MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode," +
                           " MaintSubDept.MaintSubDeptCode,worequest.ReceivedDate,worequest.RequiredDate," +
                           " employees.Name, workorders.ProjectNumber" +
                           " from worequest " +
                           " inner join workstatus on worequest.RequestStatusID=WorkStatus.WorkStatusID" +
                           " inner join worktype on worktype.WorkTypeID = worequest.WorkTypeID " +
                           " inner join workpriority on workpriority.WorkPriorityID = worequest.WorkPriorityID " +
                           " LEFT OUTER JOIN assets asset ON asset.assetid = worequest.assetid " +
                           " LEFT OUTER JOIN location ON location.locationid = worequest.locationid " +
                           " Inner JOIN L2 ON L2.L2ID = worequest.L2ID" +
                           " Inner JOIN Sector ON Sector.SectorID = L2.SectorID" +
                           " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID " +
                           " left outer join L3 on L3.L3ID = worequest.L3ID " +
                           " left outer join L4 on L4.L4ID = worequest.L4ID " +
                           " left outer join L5 on L5.L5ID = worequest.L5ID " +
                           " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID= worequest.MaintdivId " +
                           " inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = worequest.MaintdeptId " +
                           " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = worequest.MainSubDeptId " +
                           " left outer join employees on employees.EmployeeID = worequest.RequesterID " +
                           " left outer join workorders on workorders.WorkorderNo = worequest.WorkorderNo " +
                           " where 1=1 " + strWhere + strWhereClauseWithEmployee + " ";

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Worequest>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public Worequest GetJobRequestByRequestNo(string requestNo)
        {
            try
            {
                Worequest objWORequest = new Worequest();
                IList<Worequest> lstWoRequest = new List<Worequest>();
                if (!string.IsNullOrEmpty(requestNo))
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "RequestNo",
                        Value = requestNo,
                        DBType = DbType.String
                    });

                    //string query = "SELECT *,e.Name,w.WorkStatus as WorkStatusName,w.AltWorkStatus,w3.WorkStatus FROM worequest,employees e,workstatus w ,workorders w2,workstatus     w3" +
                    //             " where RequestNo=@RequestNo AND worequest.CreatedByID=e.EmployeeID  AND worequest.RequestStatusID = w.WorkStatusID " +
                    //             "  ";
                    string query = "Select  worequest.*,e.Name As CreatedByName, e2.Name,e2.AltName,e2.EmployeeNO,e2.Email,e2.WorkPhone,e2.Fax,e2.HandPhone,w3.WorkStatus,w.WorkStatus as JobStatus,w.AltWorkStatus, ";
                    query += "(SELECT NAME FROM employees e2 WHERE e2.EmployeeID = worequest.AuthorisedEmployeeId) AS AuthEmployeeName, ";
                    query += "(SELECT l.NotetoTech FROM location l WHERE l.LocationID = worequest.LocationID) AS NoteToTechLocation, ";
                    query += "(SELECT l.LocationNo FROM location l WHERE l.LocationID = worequest.LocationID) AS LocationNo,";
                    query += "(SELECT l.LocationDescription FROM location l WHERE l.LocationID = worequest.LocationID) AS LocationDescription,";
                    query += "(SELECT l.LocationAltDescription FROM location l WHERE l.LocationID = worequest.LocationID) AS LocationAltDescription,";
                    query += "(SELECT l.L5No FROM L5 l WHERE l.L5ID = worequest.L5ID) AS L5No,";
                    query += "(SELECT l.L5Description FROM L5 l WHERE l.L5ID = worequest.L5ID) AS L5Description,";
                    query += "(SELECT l.L5AltDescription FROM L5 l WHERE l.L5ID = worequest.L5ID) AS L5AltDescription, ";
                    query += "(SELECT s.SectorCode FROM sector s WHERE s.SectorID = (SELECT L2.SectorID FROM L2  Where L2.L2ID = worequest.L2ID)) AS SectorCode,";
                    query += "(SELECT s.SectorName FROM sector s WHERE s.SectorID =( SELECT L2.SectorID FROM L2  Where L2.L2ID = worequest.L2ID)) AS SectorName,";
                    query += "(SELECT s.SectorAltname FROM sector s WHERE s.SectorID =( SELECT L2.SectorID FROM L2  Where L2.L2ID = worequest.L2ID)) AS SectorAltname";
                    query += " FROM worequest INNER JOIN ";
                    query += " employees e ON worequest.CreatedByID = e.EmployeeID AND worequest.RequestNo = @RequestNo  INNER JOIN ";
                    query += " workstatus w ON  worequest.RequestStatusID = w.WorkStatusID LEFT OUTER JOIN ";
                    query += " workorders w2 ON  worequest.WorkorderNo = w2.WorkorderNo LEFT OUTER JOIN ";
                    query += " workstatus w3 ON w2.WorkStatusID = w3.WorkStatusID";
                    query += " Left OUTER JOIN employees e2 ON worequest.RequesterId = e2.EmployeeID ";

                    using (DapperDBContext context = new DapperDBContext())
                    {
                        lstWoRequest = context.ExecuteQuery<Worequest>(query, parameters);
                    }

                    if (lstWoRequest != null && lstWoRequest.Count > 0)
                    {
                        objWORequest = lstWoRequest.FirstOrDefault();
                    }

                    if (objWORequest != null)
                    {
                        Asset objAsset = new Asset();

                        int id = objWORequest.AssetID.HasValue ? Convert.ToInt32(objWORequest.AssetID) : 0;
                        objAsset = AssetService.GetAssetDetailByAssetID(id);

                        objWORequest.Asset = objAsset;

                        if (string.IsNullOrEmpty(objWORequest.Name))
                        {
                            //For New Request
                            employees objEmp = new employees();
                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objEmp = objContext.SearchAllByID<employees>(objEmp, ProjectSession.EmployeeID).FirstOrDefault();
                            }

                            if (objEmp != null)
                            {
                                objWORequest.Name = objEmp.Name;
                                objWORequest.AltName = objEmp.AltName;
                                objWORequest.Email = objEmp.Email;
                                objWORequest.WorkPhone = objEmp.WorkPhone;
                                objWORequest.Fax = objEmp.Fax;
                                objWORequest.HandPhone = objEmp.HandPhone;
                                objWORequest.L2ID = objEmp.L2ID;
                                objWORequest.RequesterID = objEmp.EmployeeID;
                                objWORequest.EmployeeNO = objEmp.EmployeeNO;
                                objWORequest.ReceivedDate = DateTime.Now;
                                objWORequest.RequiredDate = DateTime.Now;
                            }
                            if (string.IsNullOrEmpty(objWORequest.SectorCode))
                            {
                                Sector objSector = new Sector();
                                L2 objL2 = new L2();
                                objL2.L2ID = Convert.ToInt32(objEmp.L2ID);
                                objWORequest.MaintdivId = objEmp.MaintDivisionID;
                                objWORequest.MaintdeptId = objEmp.MaintDeptID;
                                objWORequest.MainSubDeptId = objEmp.MaintSubDeptID;
                                using (ServiceContext objContext = new ServiceContext())
                                {
                                    objSector.SectorID = Convert.ToInt32(objContext.Search<L2>(objL2).FirstOrDefault().SectorID);
                                    objSector = objContext.SearchAllByID<Sector>(objSector, objSector.SectorID).FirstOrDefault();
                                }
                                objWORequest.SectorCode = objSector.SectorCode;
                                objWORequest.SectorName = objSector.SectorName;
                                objWORequest.SectorAltName = objSector.SectorAltname;
                            }

                            Workpriority objWorkPriority = new Workpriority();
                            Worktype objWorktype = new Worktype();
                            Worktrade objWorktrade = new Worktrade();

                            objWorkPriority.IsDefault = true;
                            objWorktype.IsDefault = true;
                            objWorktrade.IsDefault = true;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objWorkPriority = objContext.Search<Workpriority>(objWorkPriority).FirstOrDefault();
                                objWorktype = objContext.Search<Worktype>(objWorktype).FirstOrDefault();
                                objWorktrade = objContext.Search<Worktrade>(objWorktrade).FirstOrDefault();
                            }

                            if (objWorkPriority != null)
                            {
                                objWORequest.Workpriorityid = objWorkPriority.WorkPriorityID;
                            }
                            if (objWorktype != null)
                            {
                                objWORequest.WorkTypeID = objWorktype.WorkTypeID;
                            }
                            if (objWorktrade != null)
                            {
                                objWORequest.Worktradeid = objWorktrade.WorkTradeID;
                            }

                            Workstatu objWS = new Workstatu();
                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objWS = objContext.SelectObject<CMMS.Model.Workstatu>(1);
                            }
                            objWORequest.JobStatus = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? objWS.AltWorkStatus : objWS.WorkStatus;
                            objWORequest.RequestStatusID = objWS.WorkStatusID;
                        }

                        objWORequest.ReceivedTime = objWORequest.ReceivedDate;
                        objWORequest.RequiredTime = objWORequest.RequiredDate;
                    }
                }
                return objWORequest;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<MainenanceDivision> GetMaintDivisionByEmployeeIDOrProjectNumber(int empId, string projectNumber)
        {
            try
            {
                if (ProjectSession.IsCentral)
                {
                    empId = 0;
                }

                Collection<DBParameters> parameters = new Collection<DBParameters>();



                string query = string.Empty;

                query = "SELECT * FROM   MainenanceDivision mdv ";
                query += " WHERE  mdv.MaintDivisionID IN (SELECT DISTINCT md.MaintDivisionID";
                query += " FROM   MaintenanceDepartment md";
                query += " WHERE  md.maintDeptID IN (SELECT DISTINCT ";
                query += " MaintenanceDepartment.maintDeptID";
                query += " FROM (";

                if (string.IsNullOrEmpty(projectNumber))
                {
                    if (empId > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "EmpID",
                            Value = empId,
                            DBType = DbType.Int32
                        });

                        query += "   ( SELECT  MaintSubDept.* FROM MaintSubDept INNER JOIN Employees_MaintSubDept ON Employees_MaintSubDept.MaintSubDeptID = MaintSubDept.MaintSubDeptID";
                        query += " WHERE  Employees_MaintSubDept.EmpId = @EmpID )";
                        query += " UNION ALL ";
                        query += " (";
                        query += "  SELECT MaintSubDept.* From MaintSubDept";
                        query += " INNER JOIN employees ON ";
                        query += " employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID";
                        query += " Where employees.EmployeeID = @EmpID )";
                    }
                    else
                    {
                        query += "SELECT MaintSubDept.maintDeptID FROM MaintSubDept  " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDept.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID ";
                    }
                }
                else
                {
                    query += GetMaintSubDeptQueryByProjectNumber(projectNumber, empId);
                }

                query += " ) MaintSubDepartment INNER JOIN MaintenanceDepartment ";
                query += " ON MaintenanceDepartment.maintDeptID = MaintSubDepartment.MaintDeptID ";
                query += " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID )) ";

                using (DapperDBContext context = new DapperDBContext())
                {
                    return context.ExecuteQuery<MainenanceDivision>(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string GetMaintSubDeptQueryByProjectNumber(string projectNumber, int employeeID)
        {

            try
            {
                string ProjectTypeQuery = "SELECT cmprojectrequest.ProjectTypeID FROM cmprojectrequest  " +
                                        "LEFT JOIN cmApprovedProjectRequest ON cmApprovedProjectRequest.ProjectReqNo = cmprojectrequest.ProjectReqNo  " +
                                        "WHERE cmApprovedProjectRequest.ProjectNumber = '" + Common.setQuote(projectNumber) + "'";

                int intProjectType = 0;

                using (ServiceContext objContext = new ServiceContext())
                {
                    intProjectType = Convert.ToInt32(objContext.ExecuteScalar(ProjectTypeQuery));
                }

                string Query = string.Empty;

                if (employeeID > 0)
                {
                    if (intProjectType == 1)
                    {
                        Query = " SELECT MaintSubDepartment.* FROM " +
                            " ((SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN Employees_MaintSubDept ON Employees_MaintSubDept.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE Employees_MaintSubDept.EmpId = '" + employeeID + "') UNION ALL " +
                            " (SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN employees ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE employees.EmployeeID = '" + employeeID + "')) AS MaintSubDepartment " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDepartment.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 1 ";
                    }
                    else if (intProjectType == 2)
                    {
                        Query = " SELECT MaintSubDepartment.* FROM " +
                            " ((SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN Employees_MaintSubDept ON Employees_MaintSubDept.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE Employees_MaintSubDept.EmpId = '" + employeeID + "') UNION ALL " +
                            " (SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN employees ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE employees.EmployeeID = '" + employeeID + "')) AS MaintSubDepartment " +
                            " INNER JOIN cmProjectPerformanceEvaluation ON cmProjectPerformanceEvaluation.ID = MaintSubDepartment.cmActivityID " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDepartment.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 2 AND cmProjectPerformanceEvaluation.Projectnumber = '" + projectNumber + "' ";

                    }
                    else if (intProjectType == 4)
                    {
                        Query = " SELECT MaintSubDepartment.* FROM " +
                            " ((SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN Employees_MaintSubDept ON Employees_MaintSubDept.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE Employees_MaintSubDept.EmpId = '" + employeeID + "') UNION ALL " +
                            " (SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN employees ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE employees.EmployeeID = '" + employeeID + "')) AS MaintSubDepartment " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDepartment.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 1 " + " UNION ALL " + " SELECT MaintSubDepartment.* FROM " +
                            " ((SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN Employees_MaintSubDept ON Employees_MaintSubDept.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE Employees_MaintSubDept.EmpId = '" + employeeID + "') UNION ALL " +
                            " (SELECT MaintSubDept.* FROM MaintSubDept INNER JOIN employees ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID " +
                            " WHERE employees.EmployeeID = '" + employeeID + "')) AS MaintSubDepartment " +
                            " INNER JOIN cmProjectPerformanceEvaluation ON cmProjectPerformanceEvaluation.ID = MaintSubDepartment.cmActivityID " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDepartment.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 2 AND cmProjectPerformanceEvaluation.Projectnumber = '" + projectNumber + "' ";
                    }

                }
                else
                {
                    if (intProjectType == 1)
                    {
                        Query = "SELECT [MaintSubDept].* FROM [MaintSubDept] " +
                            " INNER JOIN [MaintenanceDepartment] ON [MaintenanceDepartment].[maintDeptID] = [MaintSubDept].[MaintDeptID] " +
                            " INNER JOIN [MainenanceDivision] ON [dbo].[MainenanceDivision].[MaintDivisionID] = [MaintenanceDepartment].[MaintDivisionID] " +
                            " WHERE [MainenanceDivision].[ProjectType] = 1 ";
                    }
                    else if (intProjectType == 2)
                    {
                        Query = "SELECT MaintSubDept.* FROM MaintSubDept " +
                            " INNER JOIN cmProjectPerformanceEvaluation ON cmProjectPerformanceEvaluation.ID = MaintSubDept.cmActivityID " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDept.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON dbo.MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 2 AND cmProjectPerformanceEvaluation.Projectnumber = '" + projectNumber + "' ";

                    }
                    else if (intProjectType == 4)
                    {
                        Query = "SELECT MaintSubDept.* FROM MaintSubDept " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDept.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON dbo.MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 1 " + " UNION ALL " + " SELECT MaintSubDept.* FROM MaintSubDept " +
                            " INNER JOIN cmProjectPerformanceEvaluation ON cmProjectPerformanceEvaluation.ID = MaintSubDept.cmActivityID " +
                            " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = MaintSubDept.MaintDeptID " +
                            " INNER JOIN MainenanceDivision ON dbo.MainenanceDivision.MaintDivisionID = MaintenanceDepartment.MaintDivisionID " +
                            " WHERE MainenanceDivision.ProjectType = 2 AND cmProjectPerformanceEvaluation.Projectnumber = '" + projectNumber + "' ";
                    }
                }

                return Query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Worequest> GetAllWRByRequesterID(int requesterID, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Worequest> list = new List<Worequest>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "worequest.CreatedDate desc, worequest.workorderno";
            }

            string strWhereClauseWithEmployee = string.Empty;

            if (requesterID > 0)
            {
                strWhereClauseWithEmployee = " and worequest.RequesterID = " + requesterID;
            }

            if (!IsCentral)
            {
                strWhereClauseWithEmployee += " and worequest.L2ID in (select L2Id from employees_L2 where empID = " + empId + " union Select L2Id from employees where employeeId = " + empId + " ) ";
            }

            if (empId > 0)
            {
                strWhereClauseWithEmployee += " and worequest.mainsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empId + " union Select MaintSubDeptID from employees where employeeId = " + empId + " ) ";
            }

            string query = "Select RequestNo,RequestStatusID,worequest.WorkTypeID,wt.WorkTypeDescription,wt.AltWorkTypeDescription,ProblemDesc,ReceivedDate,workstatus.WorkStatus,workstatus.AltWorkStatus from worequest inner join worktype wt on worequest.WorkTypeID=wt.WorkTypeID " +
                           " inner join workstatus on workstatus.WorkStatusID = worequest.RequestStatusID " +
                           " where 1=1 " + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Worequest>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Gets the work request list by asset id
        /// </summary>
        /// <param name="AssetID">Asset ID</param>
        /// <param name="empID">Employee ID</param>
        /// <param name="isCentral">Is central site?</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public IList<Worequest> GetAllWRByAssetID(int AssetID, int empID, bool isCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Worequest> list = new List<Worequest>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "worequest.CreatedDate desc, worequest.workorderno";
            }

            string strWhereClauseWithEmployee = "";
            if (AssetID > 0)
            {
                strWhereClauseWithEmployee = " and worequest.AssetID = " + AssetID;
            }

            if (!isCentral)
            {
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and worequest.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " ) ";
            }
            if (empID > 0)
            {
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and worequest.mainsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empID + " union Select MaintSubDeptID from employees where employeeId = " + empID + " ) ";
            }

            string query = "Select RequestNo,RequestStatusID, worequest.WorkTypeID,w.WorkStatus,w2.WorkTypeDescription,w2.AltWorkTypeDescription,ProblemDesc,ReceivedDate " +
                            "from worequest INNER JOIN workstatus w ON  worequest.RequestStatusID = w.WorkStatusID  " +
                            "INNER JOIN worktype w2  ON worequest.WorkTypeID = w2.WorkTypeID " + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Worequest>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// To Add new Work Request
        /// </summary>
        /// <param name="objWR">Instance Of Work Request</param>
        /// <returns>True/False</returns>
        public bool SaveWorkRequestByExtend(Worequest objWR)
        {
            try
            {
                bool successStatus = false;

                string strUpdateColumn = "";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                // L2ID
                if ((objWR.L2ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L2ID = @L2ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = objWR.L2ID,
                        DBType = DbType.Int32
                    });
                }

                // L3ID
                if ((objWR.L3ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L3ID = @L3ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L3ID",
                        Value = objWR.L3ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L3ID = NULL , ";
                }

                // L4ID
                if ((objWR.L4ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L4ID = @L4ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L4ID",
                        Value = objWR.L4ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L4ID = NULL , ";
                }

                // L5ID
                if ((objWR.L5ID != null))
                {
                    strUpdateColumn = strUpdateColumn + " L5ID = @L5ID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L5ID",
                        Value = objWR.L5ID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " L5ID = NULL , ";
                }

                // LocationID
                if ((objWR.LocationID != null))
                {
                    strUpdateColumn = strUpdateColumn + " LocationID = @LocationID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "LocationID",
                        Value = objWR.LocationID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " LocationID = NULL , ";
                }

                // AssetID
                if ((objWR.AssetID != null))
                {
                    strUpdateColumn = strUpdateColumn + " AssetID = @AssetID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "AssetID",
                        Value = objWR.AssetID,
                        DBType = DbType.Int32
                    });
                }
                else
                {
                    strUpdateColumn = strUpdateColumn + " AssetID = NULL , ";
                }

                // Work Trade ID
                if ((objWR.Worktradeid != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkTradeID = @Worktradeid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Worktradeid",
                        Value = objWR.Worktradeid,
                        DBType = DbType.Int32
                    });
                }

                // Work Type ID
                if ((objWR.WorkTypeID != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkTypeID = @WorkTypeID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkTypeID",
                        Value = objWR.WorkTypeID,
                        DBType = DbType.Int32
                    });
                }

                // Work Priority ID
                if ((objWR.Workpriorityid != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkPriorityID = @Workpriorityid, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Workpriorityid",
                        Value = objWR.Workpriorityid,
                        DBType = DbType.Int32
                    });
                }

                // Requester ID
                if ((objWR.RequesterID != null))
                {
                    strUpdateColumn = strUpdateColumn + " RequesterID = @RequesterID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RequesterID",
                        Value = objWR.RequesterID,
                        DBType = DbType.Int32
                    });
                }

                // ExternalRequesterName
                if ((objWR.ExternalRequesterName != null))
                {
                    strUpdateColumn = strUpdateColumn + " ExternalRequesterName = @ExternalRequesterName, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ExternalRequesterName",
                        Value = objWR.ExternalRequesterName,
                        DBType = DbType.String
                    });
                }

                // Request Status ID
                if ((objWR.RequestStatusID != null))
                {
                    strUpdateColumn = strUpdateColumn + " RequestStatusID = @RequestStatusID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "RequestStatusID",
                        Value = objWR.RequestStatusID,
                        DBType = DbType.Int32
                    });
                }

                // Maintenance Department ID
                if ((objWR.MaintdeptId != null))
                {
                    strUpdateColumn = strUpdateColumn + " maintdeptID = @MaintdeptId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdeptId",
                        Value = objWR.MaintdeptId,
                        DBType = DbType.Int32
                    });
                }

                // Maintenance Sub Dept ID
                if ((objWR.MainSubDeptId != null))
                {
                    strUpdateColumn = strUpdateColumn + " MainSubDeptId = @MainSubDeptId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MainSubDeptId",
                        Value = objWR.MainSubDeptId,
                        DBType = DbType.Int32
                    });
                }

                // Maintenance Division ID
                if ((objWR.MaintdivId != null))
                {
                    strUpdateColumn = strUpdateColumn + " MaintdivId = @MaintdivId, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "MaintdivId",
                        Value = objWR.MaintdivId,
                        DBType = DbType.Int32
                    });
                }

                // Problem Desc
                if ((objWR.ProblemDesc != null))
                {
                    strUpdateColumn = strUpdateColumn + " ProblemDesc = @ProblemDesc, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ProblemDesc",
                        Value = objWR.ProblemDesc,
                        DBType = DbType.String
                    });
                }

                // Received Date
                if ((objWR.ReceivedDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " ReceivedDate = @ReceivedDate, ";

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ReceivedDate",
                            Value = objWR.ReceivedDate,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "ReceivedDate",
                            Value = Common.GetEnglishDate(objWR.ReceivedDate),
                            DBType = DbType.DateTime
                        });
                    }
                }

                // Required Date
                if ((objWR.RequiredDate != null))
                {
                    strUpdateColumn = strUpdateColumn + " RequiredDate = @RequiredDate, ";

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "RequiredDate",
                            Value = objWR.RequiredDate,
                            DBType = DbType.DateTime
                        });
                    }else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "RequiredDate",
                            Value = Common.GetEnglishDate(objWR.RequiredDate),
                            DBType = DbType.DateTime
                        });
                    }
                }

                // CancelledByID
                if ((objWR.CancelledByID != null))
                {
                    strUpdateColumn = strUpdateColumn + " CancelledByID = @CancelledByID, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "CancelledByID",
                        Value = objWR.CancelledByID,
                        DBType = DbType.Int32
                    });
                }

                // Remarks
                if ((objWR.Remarks != null))
                {
                    strUpdateColumn = strUpdateColumn + " Remarks = @Remarks, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "Remarks",
                        Value = objWR.Remarks,
                        DBType = DbType.String
                    });
                }

                // Work Order
                if ((objWR.WorkorderNo != null))
                {
                    strUpdateColumn = strUpdateColumn + " WorkorderNo = @WorkorderNo, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "WorkorderNo",
                        Value = objWR.WorkorderNo,
                        DBType = DbType.String
                    });
                }

                // Modified By
                if ((objWR.ModifiedBy != null))
                {
                    strUpdateColumn = strUpdateColumn + " ModifiedBy = @ModifiedBy, ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objWR.ModifiedBy,
                        DBType = DbType.String
                    });
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                // Modified Date
                strUpdateColumn = strUpdateColumn + " ModifiedDate = @ModifiedDate ";

                string strQuery = "UPDATE worequest set " + strUpdateColumn + "  Where RequestNo = @RequestNo";
                parameters.Add(new DBParameters()
                {
                    Name = "RequestNo",
                    Value = objWR.RequestNo,
                    DBType = DbType.String
                });
                try
                {
                    string query = "SELECT RequestStatusID FROM worequest w WHERE w.RequestNo='" + objWR.RequestNo + "'";
                    int OlRequestStatusID = 0;

                    using (ServiceContext context = new ServiceContext())
                    {
                        string RequestStatusID = context.ExecuteScalar(query);
                        if (!string.IsNullOrEmpty(RequestStatusID))
                        {
                            OlRequestStatusID = Convert.ToInt32(RequestStatusID);
                        }

                        context.ExecuteQuery<Worequest>(strQuery, parameters);
                    }

                    if (objWR.RequestStatusID != OlRequestStatusID)
                    {
                        EmailConfigurationService objService = new EmailConfigurationService();
                        objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkRequest, SystemEnum.ModuleEvent.JobRequestJobStatusChange, objWR.RequestNo);
                    }
                    successStatus = true;

                }
                catch
                {
                    successStatus = false;
                }

                return successStatus;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<CmApprovedProjectRequest> GetApprovedProjectListPage(string listSiteID, int pageNo, string strWhere, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<CmApprovedProjectRequest> list = new List<CmApprovedProjectRequest>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "ProjectNumber";
            }

            string StringWhereClause = string.Empty;
            IList<int> listSectorID = null;

            if (!string.IsNullOrEmpty(listSiteID))
            {
                using (ServiceContext obj = new ServiceContext())
                {
                    listSectorID = obj.ExecuteQuery<int>("SELECT DISTINCT SectorID FROM L2 WHERE L2ID IN (" + listSiteID + ") ", new Collection<DBParameters>());
                }

                if ((listSectorID != null) && listSectorID.Count > 0)
                {
                    StringWhereClause = " AND ( cmprojectsite.L2Id IN (" + listSiteID + ") OR cmprojectsite.SectorID IN (" + string.Join(",", listSectorID) + ") )  ";
                }
                else
                {
                    StringWhereClause = " AND cmprojectsite.L2Id IN (" + listSiteID + ") ";
                }
            }

            string query = " SELECT cmApprovedProjectRequest.ProjectNumber, cmprojectrequest.ProjectName, cmprojectrequest.AltProjectName, " +
                " cmApprovedProjectRequest.Classification, cmApprovedProjectRequest.StartDate, cmprojectrequest.ReqDate, cmprojectrequest.RequestedAmount, cmApprovedProjectRequest.ProjectReqNo, " +
                " cmApprovedProjectRequest.ApprovalDate, cmApprovedProjectRequest.ApprovedAmount, cmApprovedProjectRequest.ExpiryDate FROM cmApprovedProjectRequest " +
                " LEFT JOIN cmprojectrequest ON cmprojectrequest.ProjectReqNo = cmApprovedProjectRequest.ProjectReqNo "
                + " INNER JOIN cmprojectsite ON cmprojectsite.ProjectReqNo = cmprojectrequest.ProjectReqNo "
                + " WHERE cmprojectrequest.ProjectStaus = 6 " + StringWhereClause + strWhere + " ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<CmApprovedProjectRequest>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }


        public void ChangeJobRequestModificationDateTimeByWorkRequestNo(string workrequestNo, string modifiedBy)
        {
            try
            {
                
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }
                string strSQl = "Update worequest set ModifiedBy = '" + Common.setQuote(modifiedBy) + "' , ModifiedDate = @ModifiedDate where RequestNo = '" + Common.setQuote(workrequestNo) + "'";
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(strSQl, parameters);
                }
            }
            catch (Exception ex)
            { throw ex; }
        }




    }
}
