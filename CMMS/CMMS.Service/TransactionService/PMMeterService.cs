﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PMMeterService : DBExecute
    {
        private string[] colours = { "#578ebe", "#ce3434", "#7bd2f6", "#ffb800", "#ff8517", "#e34a00", "#d49fdd", "#c17969", "#7fc896", "#d9d86f", "#d59a8d", "#70b7c7", "#c788d2", "#e18345", "#71b586", "#0069a5", "#0098ee", "#4e9fb1", "#af6dbb", "#d06c2a" };

        public PMMeterService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Gets the pm meter history by asset identifier.
        /// 
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<Reading> GetPMMeterHistoryByAssetId(int assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Reading> list = new List<Reading>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "select  r.ReadingDate,r.ReadingID,r.CurrentReading,pmmm.MeterNo  " +
                            "from Reading r " +
                            "inner join PMMeterMaster pmmm on pmmm.MeterMasterID = r.MeterMasterID  " +
                            "where pmmm.AssetID = " + assetID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Reading>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<Reading> GetPMMeterHistoryByLocationId(int locationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Reading> list = new List<Reading>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "select  r.ReadingDate,r.ReadingID,r.CurrentReading,pmmm.MeterNo  " +
                            "from Reading r " +
                            "inner join PMMeterMaster pmmm on pmmm.MeterMasterID = r.MeterMasterID  " +
                            "where pmmm.locationID = " + locationID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Reading>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Gets the pm meter schedule history by asset identifier
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<PmMeter> GetPMMeterScheduleHistoryByAssetId(int assetID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PmMeter> list = new List<PmMeter>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select pm.MeterID, pm.MeterNo, pm.MeterDescription, pm.IncFreq, pm.IncPMDue, pm.PMActive,  " +
                            "wt.WorkTrade ,wt.AltWorkTrade,  " +
                            "md.MaintDivisionCode,mdp.MaintDeptCode,msd.MaintSubDeptCode " +
                            "from PmMeters pm " +
                            "inner join MainenanceDivision md on md.MaintDivisionID= pm.MaintDivisionID  " +
                            "inner join MaintenanceDepartment mdp on mdp.maintDeptID = pm.maintdeptID  " +
                            "inner join MaintSubDept msd on msd.MaintSubDeptID = pm.maintsubdeptID " +
                            "inner join worktrade wt on wt.WorkTradeID = pm.worktradeid " +
                            "where  pm.MeterMasterId IN (SELECT MeterMasterID FROM PMMeterMaster WHERE AssetID  =  " + assetID +
                            " and pm.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID +
                            " union " +
                            " Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ))";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PmMeter>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<PmMeter> GetPMMeterScheduleHistoryByLocationId(int locationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PmMeter> list = new List<PmMeter>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select pm.MeterID, pm.MeterNo, pm.MeterDescription, pm.IncFreq, pm.IncPMDue, pm.PMActive,  " +
                            "wt.WorkTrade ,wt.AltWorkTrade,  " +
                            "md.MaintDivisionCode,mdp.MaintDeptCode,msd.MaintSubDeptCode " +
                            "from PmMeters pm " +
                            "inner join MainenanceDivision md on md.MaintDivisionID= pm.MaintDivisionID  " +
                            "inner join MaintenanceDepartment mdp on mdp.maintDeptID = pm.maintdeptID  " +
                            "inner join MaintSubDept msd on msd.MaintSubDeptID = pm.maintsubdeptID " +
                            "inner join worktrade wt on wt.WorkTradeID = pm.worktradeid " +
                            "where  pm.MeterMasterId IN (SELECT MeterMasterID FROM PMMeterMaster WHERE locationID  =  " + locationID +
                            " and pm.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID +
                            " union " +
                            " Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ))";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PmMeter>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// get Asset Details by meterMasterID
        /// </summary>
        /// <param name="meterMasterID">The asset details identifier </param>
        /// <returns></returns>
        public static PMMeterMaster GetAssetDetailByMeterMasterID(int meterMasterID)
        {
            PMMeterMaster objPMMeterMaster = new PMMeterMaster();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "meterMasterID",
                Value = meterMasterID,
                DBType = DbType.Int32
            });
            string query = "select PMMeterMaster.MeterMasterID,  " +
                            "PMMeterMaster.MeterNo, " +
                            "PMMeterMaster.MeterDescription, " +
                            "PMMeterMaster.AltMeterDescription, " +
                            "PMMeterMaster.MeterType," +
                            "PMMeterMaster.MeterUnit, " +
                            "PMMeterMaster.LastReading, " +
                            "assets.AssetID, " +
                            "assets.AssetNumber, " +
                            "assets.AssetDescription," +
                            "assets.AssetAltDescription, " +
                            "location.LocationID , " +
                            "location.LocationNo," +
                            "location.LocationDescription," +
                            "location.LocationAltDescription," +
                            " LocationAsset.LocationID LocationIDAsset,LocationAsset.LocationNo LocationNoAsset,LocationAsset.LocationDescription LocationDescriptionAsset,LocationAsset.LocationAltDescription LocationAltDescriptionAsset, " +
                            "L5.L5No," +
                            "L5.L5Description," +
                            "L5.L5AltDescription," +
                            "L4.L4No," + "L4.L4Description," + "L4.L4AltDescription," +
                            "L3.L3No," + "L3.L3Desc," + "L3.L3AltDesc," +
                            "L2.L2ID," + "L2.L2Code," + "L2.L2name," + "L2.L2Altname, " +
                            "PMMeterMaster.Active," + "PMMeterMaster.MaxDailyIncrement," + "PMMeterMaster.StartMeterReading ,PMMeterMaster.TypeMeter " +
                            "from PMMeterMaster " +
                            "INNER JOIN L2 ON L2.L2ID=PMMeterMaster.L2ID " +
                            " LEFT JOIN assets ON assets.AssetID=PMMeterMaster.AssetID " +
                            " LEFT JOIN Location LocationAsset on LocationAsset.LocationID = assets.LocationID " +
                            " LEFT OUTER JOIN L3 ON L3.L3ID=LocationAsset.L3ID " +
                            " LEFT OUTER JOIN L5 ON L5.L5ID=LocationAsset.L5ID " +
                            " LEFT JOIN L4 ON L4.L4ID=LocationAsset.L4ID " +
                            " LEFT JOIN location on location.LocationID = PMMeterMaster.LocationID " +
                            " Where PMMeterMaster.MeterMasterID = @meterMasterID";

            using (DapperDBContext context = new DapperDBContext())
            {
                objPMMeterMaster = context.ExecuteQuery<PMMeterMaster>(query, parameters).FirstOrDefault();
            }
            return objPMMeterMaster;
        }

        /// <summary>
        /// get Asset Details by meterMasterID
        /// </summary>
        /// <param name="meterMasterID">The asset details identifier </param>
        /// <returns></returns>
        public static PmMeter GetPmchecklistDetailByPMMeterID(int meterID)
        {
            PmMeter objPmMeter = new PmMeter();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "meterID",
                Value = meterID,
                DBType = DbType.Int32
            });
            string query = "select PmMeters.* " +
                            "from PmMeters " +
                            "Where PmMeters.MeterID = @meterID";

            using (DapperDBContext context = new DapperDBContext())
            {
                objPmMeter = context.ExecuteQuery<PmMeter>(query, parameters).FirstOrDefault();
            }
            return objPmMeter;
        }

        /// <summary>
        /// Update PMMeter Reading
        /// </summary>
        /// <param name="objReading">the Reading</param>
        public static double UpdatePMMeterReading(Reading objReading)
        {
            PMMeterMaster objPMMeterMaster = new PMMeterMaster();
            //float lastReading;
            //string meterType = string.Empty;

            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "readingID",
                    Value = objReading.ReadingID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "currentReading",
                    Value = objReading.CurrentReading,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "modifiedBy",
                    Value = objReading.ModifiedBy,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "modifiedDate",
                    Value = objReading.ModifiedDate,
                    DBType = DbType.DateTime
                });
                parameters.Add(new DBParameters()
                {
                    Name = "meterMasterID",
                    Value = objReading.MeterMasterID,
                    DBType = DbType.Int32
                });

                using (DapperContext context = new DapperContext())
                {
                    objPMMeterMaster = context.SearchAll(objPMMeterMaster).Where(m => m.MeterMasterID == objReading.MeterMasterID).FirstOrDefault();
                    //meterType = result.MeterType.ToString().ToLower();
                    //lastReading = result.LastReading;
                }

                if (objPMMeterMaster.MeterType.ToLower() == "incremental")
                {
                    Reading objReadingtemp = new Reading();
                    int ReadingCount = 0;
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        ReadingCount = (from c in context.SearchAll<Reading>(objReadingtemp)
                                        where c.MeterMasterID == objReading.MeterMasterID && c.ReadingID != objReading.ReadingID
                                            && ((c.ReadingDate >= objReading.ReadingDate && c.CurrentReading <= objReading.CurrentReading)
                                            || (c.ReadingDate <= objReading.ReadingDate && c.CurrentReading >= objReading.CurrentReading)
                                            )
                                        select c).Count();
                    }

                    if (ReadingCount == 0)
                    {
                        if (objPMMeterMaster.MaxDailyIncrement > 0)
                        {
                            Reading objTempReading = new Reading();
                            using (DapperContext context = new DapperContext())
                            {
                                objTempReading = (from c in context.SearchAll<Reading>(objTempReading) where c.MeterMasterID == objReading.MeterMasterID && c.ReadingID != objReading.ReadingID select c).OrderByDescending(c => c.CurrentReading).FirstOrDefault();
                            }

                            if (objTempReading != null)
                            {
                                var timeDiff = (objTempReading.ReadingDate - objReading.ReadingDate).Value.TotalDays;
                                var readingDiff = objTempReading.CurrentReading - objReading.CurrentReading;
                                var avgDailyInc = readingDiff / timeDiff;
                                if (Convert.ToDouble(avgDailyInc) > Convert.ToDouble(objPMMeterMaster.MaxDailyIncrement))
                                {
                                    return Convert.ToDouble(avgDailyInc);
                                }
                            }
                        }

                        string query = @"UPDATE reading SET CurrentReading = @currentReading, ModifiedBy = @modifiedBy, ModifiedDate = @modifiedDate Where ReadingID = @readingID";
                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<Reading>(query, parameters);
                        }

                        string updateReadingSchedule = @"update ReadingSchedule set Reading = @currentReading, " +
                                                                  " GenStatus = 'No', LastPMDue = pmmeters.IncPMDue " +
                                                                  " from ReadingSchedule inner join pmmeters on ReadingSchedule.MeterID = pmmeters.MeterID " +
                                                                  " where ReadingID = @readingID";
                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                        }

                        Reading objTempReading1 = new Reading();
                        objTempReading1.MeterMasterID = objReading.MeterMasterID;
                        using (DapperContext context = new DapperContext())
                        {
                            objTempReading1 = context.Search<Reading>(objTempReading1).OrderByDescending(x => x.CurrentReading).FirstOrDefault();
                        }

                        if (objTempReading1 != null)
                        {
                            if (objTempReading1.ReadingID == objReading.ReadingID)
                            {
                                string updateReadingSchedule1 = @"update ReadingSchedule set " +
                                                                  " GenStatus = CASE WHEN LastPMDue <=  @currentReading THEN 'Yes' else 'No' End," +
                                                                  " ModifiedBy = @modifiedBy, ModifiedDate = @modifiedDate " +
                                                                  " where ReadingID = @readingID";
                                using (DapperDBContext context = new DapperDBContext())
                                {
                                    context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule1, parameters);
                                }

                                string updatePMMeterMaster = @"update PMMeterMaster set LastReading = @currentReading, ModifiedBy = @modifiedBy ," +
                                                             " ModifiedDate =@modifiedDate where MeterMasterID = @meterMasterID";
                                using (DapperDBContext context = new DapperDBContext())
                                {
                                    context.ExecuteQuery<PMMeterMaster>(updatePMMeterMaster, parameters);
                                }
                            }
                        }
                        return -2;
                    }
                }

                if (objPMMeterMaster.MeterType.ToLower() == "threshold")
                {
                    string query = @"UPDATE reading SET CurrentReading = @currentReading, ModifiedBy = @modifiedBy, ModifiedDate = @modifiedDate Where ReadingID = @readingID";
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        context.ExecuteQuery<Reading>(query, parameters);
                    }

                    string updateReadingSchedule = string.Empty;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        updateReadingSchedule = " MERGE INTO ReadingSchedule USING " +
                                                " (SELECT ReadingSchedule.ROWID row_id, " +
                                                           " @currentReading, " +
                                                "   CASE " +
                                                "     WHEN @currentReading BETWEEN pmmeters.LowerLimit AND pmmeters.UpperLimit " +
                                                "     THEN 'No' " +
                                                "     ELSE 'Yes' " +
                                                "   END AS pos_3, " +
                                                "   pmmeters.LowerLimit, " +
                                                "   pmmeters.UpperLimit, " +
                                                "   @modifiedBy, " +
                                                "   @modifiedDate " +
                                                " FROM ReadingSchedule " +
                                                " JOIN pmmeters " +
                                                " ON ReadingSchedule.MeterID       = pmmeters.MeterID " +
                                                " WHERE ReadingID                  = @readingID " +
                                                " ) src ON ( ReadingSchedule.ROWID = src.row_id ) " +
                                                " WHEN MATCHED THEN " +
                                                "   UPDATE " +
                                                "   SET Reading    = @currentReading, " +
                                                "     GenStatus    = pos_3, " +
                                                "     LowerLimit   = src.LowerLimit, " +
                                                "     UpperLimit   = src.UpperLimit, " +
                                                "     ModifiedBy   = @modifiedBy, " +
                                                "     ModifiedDate = @modifiedDate ";
                    }
                    else
                    {
                        updateReadingSchedule = @"update ReadingSchedule set Reading = @currentReading, GenStatus = CASE WHEN @currentReading BETWEEN " +
                                                             " pmmeters.LowerLimit AND pmmeters.UpperLimit THEN 'No' else 'Yes' End, LowerLimit = pmmeters.LowerLimit, " +
                                                             " UpperLimit = pmmeters.UpperLimit, ModifiedBy = @modifiedBy , ModifiedDate = @modifiedDate" +
                                                             " from ReadingSchedule inner join pmmeters on ReadingSchedule.MeterID = pmmeters.MeterID " +
                                                             " where ReadingID = @readingID";
                    }


                    using (DapperDBContext context = new DapperDBContext())
                    {
                        context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                    }

                    Reading objTempReading1 = new Reading();
                    objTempReading1.MeterMasterID = objReading.MeterMasterID;
                    using (DapperContext context = new DapperContext())
                    {
                        objTempReading1 = context.Search<Reading>(objTempReading1).OrderByDescending(x => x.CurrentReading).FirstOrDefault();
                    }

                    if (objTempReading1 != null)
                    {
                        string updatePMMeterMaster = @"update PMMeterMaster set LastReading = @currentReading, ModifiedBy = @modifiedBy ," +
                                                                        " ModifiedDate =@modifiedDate where MeterMasterID = @meterMasterID";
                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<PMMeterMaster>(updatePMMeterMaster, parameters);
                        }
                    }
                    return -2;
                }
                return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add PMMeter Reading
        /// </summary>
        /// <param name="ObjReadingAddEdit">the Reading</param>
        public static double AddPMMeterReading(Reading ObjReadingAddEdit)
        {
            int ReadingID = 0;
            string meterType = string.Empty;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "meterMasterID",
                    Value = ObjReadingAddEdit.MeterMasterID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "currentReading",
                    Value = ObjReadingAddEdit.CurrentReading,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "readingDate",
                    Value = ObjReadingAddEdit.ReadingDate,
                    DBType = DbType.DateTime
                });
                parameters.Add(new DBParameters()
                {
                    Name = "createdBy",
                    Value = ObjReadingAddEdit.CreatedBy,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "createdDate",
                    Value = ObjReadingAddEdit.CreatedDate,
                    DBType = DbType.DateTime
                });

                Reading objTempReading = new Reading();

                using (DapperContext context = new DapperContext())
                {
                    objTempReading = context.SearchAll(objTempReading).Where(m => m.MeterMasterID == ObjReadingAddEdit.MeterMasterID).OrderByDescending(x => x.ReadingDate).FirstOrDefault();
                }

                if (objTempReading != null && (objTempReading.ReadingDate >= ObjReadingAddEdit.ReadingDate))
                {
                    return -3;
                }

                PMMeterMaster ObjPMMeterMaster = new PMMeterMaster();
                using (DapperContext context = new DapperContext())
                {
                    ObjPMMeterMaster = context.SearchAll(ObjPMMeterMaster).Where(m => m.MeterMasterID == ObjReadingAddEdit.MeterMasterID).FirstOrDefault();
                    ////meterType = result.MeterType.ToString().ToLower();
                    ////lastReading = result.LastReading;
                }

                if (ObjPMMeterMaster.MeterType.ToLower() == "incremental")
                {
                    if (ObjPMMeterMaster.MaxDailyIncrement > 0)
                    {
                        if (!(objTempReading == null))
                        {
                            var timeDiff = (objTempReading.ReadingDate - ObjReadingAddEdit.ReadingDate).Value.TotalDays;
                            var readingDiff = objTempReading.CurrentReading - ObjReadingAddEdit.CurrentReading;
                            var avgDailyInc = readingDiff / timeDiff;
                            if (Convert.ToDouble(avgDailyInc) > Convert.ToDouble(ObjPMMeterMaster.MaxDailyIncrement))
                            {
                                return Convert.ToDouble(avgDailyInc);
                            }
                        }
                    }

                    using (DapperDBContext context = new DapperDBContext())
                    {
                        ReadingID = context.Save(ObjReadingAddEdit);
                    }
                    if (ReadingID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "readingID",
                            Value = ReadingID,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "lastReading",
                            Value = ObjPMMeterMaster.LastReading,
                            DBType = DbType.Int32
                        });

                        string updateReadingSchedule = "update ReadingSchedule set GenStatus = 'No'," +
                                                             " ModifiedBy = @createdBy, ModifiedDate = @createdDate" +
                                                             " where ReadingSchedule.MeterID in (select MeterID from pmmeters " +
                                                             " where MeterMasterID = @meterMasterID) and GenStatus = 'yes'";

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                        }
                        string insertReadingSchedule = string.Empty;

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            insertReadingSchedule = @"insert into ReadingSchedule (MeterID,ReadingID,DATE_, MeterDescription, Reading, GenStatus,
                                                            LastReading, LastPMDue, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN IncPMDue <= @currentReading THEN 'Yes' else 'No' end, "
                                                            + "@lastReading , IncPMDue, @createdDate, PMActive,@createdBy, @createdDate from pmmeters where MeterMasterID = @meterMasterID";

                        }
                        else
                        {
                            insertReadingSchedule = @"insert into ReadingSchedule (MeterID,ReadingID,Date, MeterDescription, Reading, GenStatus,
                                                            LastReading, LastPMDue, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN IncPMDue <= @currentReading THEN 'Yes' else 'No' end, "
                                                            + "@lastReading , IncPMDue, @createdDate, PMActive,@createdBy, @createdDate from pmmeters where MeterMasterID = @meterMasterID";

                        }

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(insertReadingSchedule, parameters);
                        }

                        if (ObjReadingAddEdit.CurrentReading > ObjPMMeterMaster.LastReading)
                        {
                            string updateQuery = @"UPDATE PMMeterMaster SET LastReading = @currentReading, ModifiedBy = @createdBy, ModifiedDate = @createdDate Where MeterMasterID = @meterMasterID";
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery<PMMeterMaster>(updateQuery, parameters);
                            }
                        }
                        return -2;
                    }
                }
                else if (ObjPMMeterMaster.MeterType.ToLower() == "threshold")
                {
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        ReadingID = context.Save(ObjReadingAddEdit);
                    }

                    if (ReadingID > 0)
                    {
                        parameters.Add(new DBParameters()
                                               {
                                                   Name = "readingID",
                                                   Value = ReadingID,
                                                   DBType = DbType.Int32
                                               });
                        parameters.Add(new DBParameters()
                        {
                            Name = "lastReading",
                            Value = ObjPMMeterMaster.LastReading,
                            DBType = DbType.Int32
                        });

                        string updateReadingSchedule = string.Empty;

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            updateReadingSchedule = @"insert into ReadingSchedule (MeterID, ReadingID, DATE_, MeterDescription, Reading, GenStatus, LastReading, "
                                                             + " LowerLimit, UpperLimit, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN @currentReading BETWEEN Lowerlimit AND UpperLimit THEN 'No' else 'Yes' End, " +
                                                             "@lastReading ,Lowerlimit, UpperLimit,@createdDate,PMActive,@createdBy,@createdDate from " +
                                                             "pmmeters where MeterMasterID = @meterMasterID";
                        }
                        else
                        {
                            updateReadingSchedule = @"insert into ReadingSchedule (MeterID, ReadingID, Date, MeterDescription, Reading, GenStatus, LastReading, "
                                                             + " LowerLimit, UpperLimit, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN @currentReading BETWEEN Lowerlimit AND UpperLimit THEN 'No' else 'Yes' End, " +
                                                             "@lastReading ,Lowerlimit, UpperLimit,@createdDate,PMActive,@createdBy,@createdDate from " +
                                                             "pmmeters where MeterMasterID = @meterMasterID";
                        }

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                        }

                        if (ObjReadingAddEdit.CurrentReading > ObjPMMeterMaster.LastReading)
                        {
                            string updateQuery = @"UPDATE PMMeterMaster SET LastReading = @currentReading, ModifiedBy = @createdBy, ModifiedDate = @createdDate Where MeterMasterID = @meterMasterID";
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery<PMMeterMaster>(updateQuery, parameters);
                            }
                        }
                        return -2;
                    }
                }
                return -1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "PM Meter - Assign To"

        public IList<PmMeters_AssignTo> GetPMMeterAssignList(int meterID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PmMeters_AssignTo> list = new List<PmMeters_AssignTo>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "Select pmAssignTo.*,employees.EmployeeNO,Name   " +
                           "from PmMeters_AssignTo pmAssignTo " +
                           "inner join employees on employees.EmployeeID = pmAssignTo.EmployeeID  " +
                           "where pmAssignTo.meterID =" + meterID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PmMeters_AssignTo>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static void ManagePMMeterAssignToEmployeeGroup(int meterID, int groupID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                    string query = "insert into pmmeters_assignto Select " + meterID + " ,EmployeeId " + ",'" + ProjectSession.EmployeeID.ToString() + "',@CurrentDate,NULL,NULL from EmployeesMaintGroups where MaintGroupId = " + groupID;
                    query += " And EmployeeID Not In (Select Distinct EmployeeID from pmmeters_assignto where meterID = " + meterID + " )";
                    context.ExecuteQuery(query, parameters);

                    //string query1 = "update pmschedule set GroupID = " + groupID + " where meterID = " + meterID;
                    //context.ExecuteQuery(query1, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void ManagePMMeterAssignToEmployee(int meterID, int employeeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                    string query = "insert into pmmeters_assignto values( " + meterID + " , " + employeeID + ",'" + ProjectSession.EmployeeID.ToString() + "',@CurrentDate,NULL,NULL)";
                    context.ExecuteQuery(query, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeletePmMeterScheduleAssignto(int meterID, int employeeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                query = "delete from pmmeters_assignto where meterID = " + meterID + " And employeeID= " + employeeID;
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(query, parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Reading"
        public static IList<WorkOrder> GetJobOrderDetailsByMeterMasterID(int MeterMasterID)
        {
            try
            {
                IList<WorkOrder> list = new List<WorkOrder>();
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "MeterMasterID",
                    Value = MeterMasterID,
                    DBType = DbType.Int32
                });


                query += "select top 1 workorders.WorkorderNo, workstatus.WorkStatus,workstatus.AltWorkStatus from workorders " +
                            "inner join workstatus on workorders.WorkStatusID = workstatus.WorkStatusID " +
                            "where workorders.MeterID in(select MeterID from  pmmeters where MeterMasterID = @MeterMasterID)" +
                            "order by workorders.CreatedDate desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.ExecuteQuery<WorkOrder>(query, parameters);
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int DeleteReading(int ReadingID, int MeterMasterID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });

                }

                int TempCount;
                string query = "Select count(1) as noOfRows from ReadingSchedule where ReadingID = " + ReadingID + " and GenStatus = 'done'";
                using (ServiceContext context = new ServiceContext())
                {
                    TempCount = Convert.ToInt32(context.ExecuteScalar(query));
                }

                if (TempCount > 0)
                {
                    return -1;
                }

                Reading ObjReading = new Reading();
                ObjReading.MeterMasterID = MeterMasterID;
                using (ServiceContext context = new ServiceContext())
                {
                    ObjReading = context.Search<Reading>(ObjReading).OrderByDescending(x => x.CurrentReading).FirstOrDefault();
                }

                string query1 = "delete from ReadingSchedule where ReadingID = " + ReadingID;
                using (ServiceContext context = new ServiceContext())
                {
                    context.ExecuteQuery(query1, parameters);
                }

                string query2 = "delete from Reading where ReadingID = " + ReadingID;
                using (ServiceContext context = new ServiceContext())
                {
                    context.ExecuteQuery(query2, parameters);
                }

                if (ObjReading != null)
                {
                    if (ObjReading.ReadingID == ReadingID)
                    {
                        Reading ObjReading1 = new Reading();
                        ObjReading1.MeterMasterID = MeterMasterID;
                        using (ServiceContext context = new ServiceContext())
                        {
                            ObjReading1 = context.Search<Reading>(ObjReading1).OrderByDescending(x => x.CurrentReading).FirstOrDefault();
                        }


                        if ((ObjReading1 != null))
                        {
                            string query3 = "update ReadingSchedule set " + " GenStatus = CASE WHEN LastPMDue <= Reading THEN 'Yes' else 'No' End, " + " ModifiedBy = " + ProjectSession.EmployeeID + ", ModifiedDate = @CurrentDate " + " where ReadingID = " + ObjReading1.ReadingID + " and LastPMDue is not NULL";
                            using (ServiceContext context = new ServiceContext())
                            {
                                context.ExecuteQuery(query3, parameters);
                            }

                            string query4 = "update PMMeterMaster set LastReading = " + ObjReading1.CurrentReading + ", ModifiedBy = " + ProjectSession.EmployeeID + ", " + " ModifiedDate = @CurrentDate where MeterMasterID = " + MeterMasterID;
                            using (ServiceContext context = new ServiceContext())
                            {
                                context.ExecuteQuery(query4, parameters);
                            }
                        }
                        else
                        {
                            string query5 = "update PMMeterMaster set LastReading = StartMeterReading, ModifiedBy = " + ProjectSession.EmployeeID + ", " + " ModifiedDate = @CurrentDate where MeterMasterID = " + MeterMasterID;
                            using (ServiceContext context = new ServiceContext())
                            {
                                context.ExecuteQuery(query5, parameters);
                            }
                        }
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<ReadingGraph> ReadingGraph(int meterMasterID, string meterType, DateTime startDate, DateTime endDate)
        {
            int idx = 0;
            var viewModel = new List<CMMS.Model.ReadingGraph>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            IList<ReadingGraph> list = new List<ReadingGraph>();
            parameters.Add(new DBParameters()
            {
                Name = "MeterMasterID",
                Value = meterMasterID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "MeterType",
                Value = meterType,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "StartDate",
                Value = startDate,
                DBType = DbType.DateTime
            });
            parameters.Add(new DBParameters()
            {
                Name = "EndDate",
                Value = endDate,
                DBType = DbType.DateTime
            });

            string query = string.Empty;

            if (meterType.ToLower() == "incremental")
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    query = "select max(r.CurrentReading) - MIN(r.CurrentReading) as Reading, " +
                                                " Convert(nvarchar(3), DATENAME(MONTH, r.ReadingDate)) + ' - ' + Convert(nvarchar(4),YEAR(r.ReadingDate)) as MonthYear" +
                                                " from Reading as r where MeterMasterID = @MeterMasterID and " +
                                                " r.ReadingDate between @StartDate and @EndDate " +
                                                " group by DATENAME(MONTH, r.ReadingDate), YEAR(r.ReadingDate), MONTH(r.ReadingDate) " +
                                                " order by YEAR(r.ReadingDate),MONTH(r.ReadingDate)";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    query = @"select max(r.CurrentReading) - MIN(r.CurrentReading) as Reading, 
                                CONCAT(DATE_FORMAT(r.ReadingDate,'%b'), ' - ', DATE_FORMAT(r.ReadingDate,'%Y'))  as MonthYear 
                                from Reading  r
                                where MeterMasterID = @MeterMasterID and  r.ReadingDate between @StartDate and @EndDate
                                group by DATE_FORMAT(r.ReadingDate,'%b'), DATE_FORMAT(r.ReadingDate,'%Y'), DATE_FORMAT(r.ReadingDate,'%m')  order by  DATE_FORMAT(r.ReadingDate,'%Y'),DATE_FORMAT(r.ReadingDate,'%m') ";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = "select max(r.CurrentReading) - MIN(r.CurrentReading) as Reading, " +
                                                " to_char(r.ReadingDate, 'Mon') || ' - ' || to_char(r.ReadingDate,'yyyy') as MonthYear " +
                                                " from Reading r where MeterMasterID = @MeterMasterID and " +
                                                " r.ReadingDate between @StartDate and @EndDate " +
                                                " group by to_char(r.ReadingDate, 'Month'), to_char(r.ReadingDate,'yyyy'), to_char(r.ReadingDate,'mm'),r.ReadingDate " +
                                                " order by to_char(r.ReadingDate,'yyyy'),to_char(r.ReadingDate,'mm')";
                }
            }
            else if (meterType.ToLower() == "threshold")
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    query = "select AVG(rs.Reading) as Reading, AVG(rs.UpperLimit) as AvgUpperLimit, " +
                                                " AVG(rs.LowerLimit) as AvgLowerLimit, Convert(nvarchar(3), " +
                                                " DATENAME(MONTH, rs.Date)) + ' - ' + Convert(nvarchar(4),YEAR(rs.Date)) as MonthYear " +
                                                " from ReadingSchedule as rs " +
                                                " where MeterID in (select MeterID from pmmeters " +
                                                " where metermasterid = @MeterMasterID ) and  " +
                                                " rs.Date between @StartDate and @EndDate " +
                                                " group by DATENAME(MONTH, rs.Date), YEAR(rs.Date), MONTH(rs.Date) " +
                                                " order by YEAR(rs.Date),MONTH(rs.Date)";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    query = @"select AVG(rs.Reading) as Reading, AVG(rs.UpperLimit) as AvgUpperLimit,
                              AVG(rs.LowerLimit) as AvgLowerLimit, CONCAT(DATE_FORMAT(rs.Date,'%b'), ' - ', DATE_FORMAT(rs.Date,'%Y'))  as MonthYear 
                              from ReadingSchedule as rs 
                              where MeterID in (select MeterID from pmmeters 
                              where metermasterid = @MeterMasterID ) and  
                              rs.Date between @StartDate and @EndDate 
                              group by DATE_FORMAT(rs.Date,'%b'), DATE_FORMAT(rs.Date,'%Y'),DATE_FORMAT(rs.Date,'%m') 
                              order by DATE_FORMAT(rs.Date,'%Y'),DATE_FORMAT(rs.Date,'%m') ";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    query = @"select AVG(rs.Reading) as Reading, AVG(rs.UpperLimit) as AvgUpperLimit, " +
                                                " AVG(rs.LowerLimit) as AvgLowerLimit, " +
                                                " to_char(rs.Date_, 'Month') || ' - ' || to_char(rs.Date_,'yyyy') as MonthYear " +
                                                " from ReadingSchedule rs " +
                                                " where MeterID in (select MeterID from pmmeters " +
                                                " where metermasterid = @MeterMasterID ) and  " +
                                                " rs.Date_ between @StartDate and @EndDate " +
                                                " group by to_char(rs.Date_, 'Month'), to_char(rs.Date_,'yyyy'), to_char(rs.Date_,'mm') " +
                                                " order by to_char(rs.Date_,'yyyy'),to_char(rs.Date_,'mm')";
                }
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ReadingGraph>(query, parameters, 0);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            foreach (var x in list)
            {
                viewModel.Add(new ReadingGraph { Reading = x.Reading, AvgLowerLimit = x.AvgLowerLimit, AvgUpperLimit = x.AvgUpperLimit, color = colours[idx], MonthYear = x.MonthYear });
                idx++;
                idx = (idx % colours.Length);
            }

            return list;
        }

        public static int ExportReadingCount(int cityID)
        {
            int count = 0;
            string query = " select count(1) " +
                            "FROM PMMeterMaster " +
                            "INNER JOIN L2 ON L2.L2ID = PMMeterMaster.L2ID " +
                            "where PMMeterMaster.L2ID = " + cityID + " and PMMeterMaster.Active = 1 ";

            using (DapperDBContext context = new DapperDBContext())
            {
                count = Convert.ToInt32(context.ExecuteScalar(query));
                return count;
            }
        }
        public static List<PMMeterMaster> ExportReading(int cityID)
        {
            List<PMMeterMaster> lst = new List<PMMeterMaster>();

            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "cityID",
                Value = cityID,
                DBType = DbType.Int32
            });

            //for oracle database this query will execute
            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                query = "select temp.* from (select " +
                           " ROW_NUMBER() Over(PARTITION BY PMMeterMaster.MeterNo ORDER BY Reading.ReadingDate DESC) rnum, " +
                           " PMMeterMaster.MeterNo, " +
                           " L2.L2Code, " +
                           " PMMeterMaster.MeterMasterID, " +
                           " PMMeterMaster.MeterDescription, " +
                           " PMMeterMaster.AltMeterDescription, " +
                           " PMMeterMaster.MeterType, " +
                           " PMMeterMaster.MeterUnit, " +
                           " PMMeterMaster.LastReading, " +
                           " Reading.ReadingDate As LastReadingDate " +
                           "  FROM PMMeterMaster " +
                           " INNER JOIN L2 ON L2.L2ID=PMMeterMaster.L2ID " +
                           " LEft join Reading on Reading.MeterMasterID = PMMeterMaster.MeterMasterID " +
                           " where PMMeterMaster.L2ID = @cityID and PMMeterMaster.Active = 1 " +
                           " ) temp  " +
                           " where temp.rnum = 1 ";
            }
            //for MYSQL database this query will execute
            else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
            {
                query = " select temp.* from (select " +
                           " @rownum := @rownum + 1 as rnum, " +
                           " PMMeterMaster.MeterNo, " +
                           " L2.L2Code, " +
                           " PMMeterMaster.MeterMasterID, " +
                           " PMMeterMaster.MeterDescription, " +
                           " PMMeterMaster.AltMeterDescription, " +
                           " PMMeterMaster.MeterType, " +
                           " PMMeterMaster.MeterUnit, " +
                           " PMMeterMaster.LastReading, " +
                           " Reading.ReadingDate As LastReadingDate " +
                           "  FROM PMMeterMaster " +
                           " INNER JOIN L2 ON L2.L2ID=PMMeterMaster.L2ID " +
                           " LEft join Reading on Reading.MeterMasterID = PMMeterMaster.MeterMasterID ," +
                           "(SELECT @rownum := 0) r " +
                           " where PMMeterMaster.L2ID = @cityID and PMMeterMaster.Active = 1 " +
                           "ORDER BY Reading.ReadingDate DESC " +
                           " ) temp  " +
                           " where temp.rnum = 1 ";
            }
            //for SQL database this query will execute
            else
            {
                query = ";With CTE As (select " +
                           " ROW_NUMBER() Over(PARTITION BY PMMeterMaster.MeterNo ORDER BY Reading.ReadingDate DESC) as rownum, " +
                           " PMMeterMaster.MeterNo, " +
                           " L2.L2Code, " +
                           " PMMeterMaster.MeterMasterID, " +
                           " PMMeterMaster.MeterDescription, " +
                           " PMMeterMaster.AltMeterDescription, " +
                           " PMMeterMaster.MeterType, " +
                           " PMMeterMaster.MeterUnit, " +
                           " PMMeterMaster.LastReading, " +
                           " Reading.ReadingDate As LastReadingDate " +
                           "  FROM PMMeterMaster " +
                           " INNER JOIN L2 ON L2.L2ID=PMMeterMaster.L2ID " +
                           " LEft join Reading on Reading.MeterMasterID = PMMeterMaster.MeterMasterID " +
                           " where PMMeterMaster.L2ID = @cityID and PMMeterMaster.Active = 1 " +
                           " )  " +
                           " Select * from CTE  where rownum = 1 ";
            }


            using (DapperDBContext context = new DapperDBContext())
            {
                lst = context.ExecuteQuery<PMMeterMaster>(query, parameters).ToList();
            }
            return lst;
        }

        public static List<PMMeterMaster> GetValidPMMeter(string meters)
        {
            List<PMMeterMaster> lst = new List<PMMeterMaster>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "meters",
                Value = meters,
                DBType = DbType.String
            });
            string query = "Select * from PmMeterMaster where meterNo in ('" + meters + "')";

            using (DapperDBContext context = new DapperDBContext())
            {
                lst = context.ExecuteQuery<PMMeterMaster>(query, parameters).ToList();
            }
            return lst;
        }

        public static List<L2> GetValidCity(string city)
        {
            List<L2> lst = new List<L2>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string query = "Select * from L2 where L2Code in ('" + city + "')";

            using (DapperDBContext context = new DapperDBContext())
            {
                lst = context.ExecuteQuery<L2>(query, parameters).ToList();
            }
            return lst;
        }

        public static int AddPMMeterReadingExcel(Reading ObjReadingAddEdit, string MeterType, float LastReading)
        {
            try
            {
                int ReadingID = 0;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "meterMasterID",
                    Value = ObjReadingAddEdit.MeterMasterID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "currentReading",
                    Value = ObjReadingAddEdit.CurrentReading,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "readingDate",
                    Value = ObjReadingAddEdit.ReadingDate,
                    DBType = DbType.DateTime
                });
                parameters.Add(new DBParameters()
                {
                    Name = "createdBy",
                    Value = ObjReadingAddEdit.CreatedBy,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "createdDate",
                    Value = ObjReadingAddEdit.CreatedDate,
                    DBType = DbType.DateTime
                });

                if (MeterType.ToLower() == "incremental")
                {
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        ReadingID = context.Save(ObjReadingAddEdit);
                    }
                    if (ReadingID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "readingID",
                            Value = ReadingID,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "lastReading",
                            Value = LastReading,
                            DBType = DbType.Int32
                        });

                        string updateReadingSchedule = "update ReadingSchedule set GenStatus = 'No'," +
                                                             " ModifiedBy = @createdBy, ModifiedDate = @createdDate" +
                                                             " where ReadingSchedule.MeterID in (select MeterID from pmmeters " +
                                                             " where MeterMasterID = @meterMasterID) and GenStatus = 'yes'";

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                        }

                        string insertReadingSchedule = string.Empty;

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            insertReadingSchedule = @"insert into ReadingSchedule (MeterID,ReadingID,DATE_, MeterDescription, Reading, GenStatus,
                                                            LastReading, LastPMDue, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN IncPMDue <= @currentReading THEN 'Yes' else 'No' end, "
                                                            + "@lastReading , IncPMDue, @createdDate, PMActive,@createdBy, @createdDate from pmmeters where MeterMasterID = @meterMasterID";
                        }
                        else
                        {
                            insertReadingSchedule = @"insert into ReadingSchedule (MeterID,ReadingID,Date, MeterDescription, Reading, GenStatus,
                                                            LastReading, LastPMDue, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                          "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN IncPMDue <= @currentReading THEN 'Yes' else 'No' end, "
                                                          + "@lastReading , IncPMDue, @createdDate, PMActive,@createdBy, @createdDate from pmmeters where MeterMasterID = @meterMasterID";
                        }

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(insertReadingSchedule, parameters);
                        }

                        if (ObjReadingAddEdit.CurrentReading > LastReading)
                        {
                            string updateQuery = @"UPDATE PMMeterMaster SET LastReading = @currentReading, ModifiedBy = @createdBy, ModifiedDate = @createdDate Where MeterMasterID = @meterMasterID";
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery<PMMeterMaster>(updateQuery, parameters);
                            }
                        }
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else if (MeterType.ToLower() == "threshold")
                {
                    using (DapperDBContext context = new DapperDBContext())
                    {
                        ReadingID = context.Save(ObjReadingAddEdit);
                    }

                    if (ReadingID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "readingID",
                            Value = ReadingID,
                            DBType = DbType.Int32
                        });
                        parameters.Add(new DBParameters()
                        {
                            Name = "lastReading",
                            Value = LastReading,
                            DBType = DbType.Int32
                        });

                        string updateReadingSchedule = @"insert into ReadingSchedule (MeterID, ReadingID, Date, MeterDescription, Reading, GenStatus, LastReading, "
                                                             + " LowerLimit, UpperLimit, Enterdate, PMActive, CreatedBy, CreatedDate)" +
                                                            "select MeterID, @readingID , @readingDate, MeterDescription,@currentReading ,CASE WHEN @currentReading BETWEEN Lowerlimit AND UpperLimit THEN 'No' else 'Yes' End, " +
                                                             "@lastReading ,Lowerlimit, UpperLimit,@createdDate,PMActive,@createdBy,@createdDate from " +
                                                             "pmmeters where MeterMasterID = @meterMasterID";

                        using (DapperDBContext context = new DapperDBContext())
                        {
                            context.ExecuteQuery<ReadingSchedule>(updateReadingSchedule, parameters);
                        }

                        if (ObjReadingAddEdit.CurrentReading > LastReading)
                        {
                            string updateQuery = @"UPDATE PMMeterMaster SET LastReading = @currentReading, ModifiedBy = @createdBy, ModifiedDate = @createdDate Where MeterMasterID = @meterMasterID";
                            using (DapperDBContext context = new DapperDBContext())
                            {
                                context.ExecuteQuery<PMMeterMaster>(updateQuery, parameters);
                            }
                        }

                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
