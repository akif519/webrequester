﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CMMS.Service
{
    public class StockCodeService : DBExecute
    {
        public StockCodeService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<StockCode> GetPurchaseProposalHighpriority(int pageNo, string sortExpression, string sortDirection, int storeID, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<StockCode> list = new List<StockCode>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "Select sc.StockID,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.Manufacturer, sc.UOM," +
                            " ss.SubStoreID,ss.SubStoreDesc,ss.AltSubStoreDesc, " +
                            " sl.max_level,sl.re_order_level,sl.min_level,sl.reorder_qty,sl.Balance,ss.L2Id " +
                            " from  stockcode  sc  " +
                            " left join stockcode_levels sl on sc.StockID = sl.stock_id " +
                            " left join substore ss on sl.sub_store_id = ss.SubStoreID " +
                            " Where Balance < min_level and SubStoreID = " + storeID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<StockCode>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<StockCode> GetPurchaseProposalNormalpriority(int pageNo, string sortExpression, string sortDirection, int storeID, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<StockCode> list = new List<StockCode>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = "Select sc.StockID,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.Manufacturer,sc.UOM, " +
                            " ss.SubStoreID,ss.SubStoreDesc,ss.AltSubStoreDesc, " +
                            " sl.max_level,sl.re_order_level,sl.min_level,sl.reorder_qty,sl.Balance,ss.L2Id " +
                            " from  stockcode sc  " +
                            " left join stockcode_levels sl on sc.StockID = sl.stock_id " +
                            " left join substore ss on sl.sub_store_id = ss.SubStoreID " +
                            " Where Balance < re_order_level and Balance >= min_level and SubStoreID = " + storeID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<StockCode>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }


        #region "Consumables"

        public SubStoreCardForm GetConsumablesDetailByCardID(int ID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "ID",
                Value = ID,
                DBType = DbType.Int32
            });

            string query = " Select s.CardId,s.EmployeeId,s.CardNo,s.FileNo,s.PrivateNo,s.SNo,s.Attached,s.DateValue,s.SubStoreIdRef,s.Status,d.CardIssueId,e.MaintSubDeptID, " +
                            " d.StockId,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.UOM,d.QtyIssue,d.Remarks,e.Name,e.EmployeeNO,e.Positions,sc.Price2 " +
                            " FROM SubStoreCardForm s  " +
                            " LEFT OUTER JOIN SubStoreCardFormDetail d on s.CardId= d.CardIRef " +
                            " LEFT OUTER JOIN stockcode sc on sc.stockId = d.stockId " +
                            " INNER JOIN employees e on s.EmployeeId = e.EmployeeID" +
                            " Where s.CardId = @ID";

            using (DapperDBContext context = new DapperDBContext())
            {
                return context.ExecuteQuery<SubStoreCardForm>(query, parameters).FirstOrDefault();
            }
        }

        public IList<SubStoreCardFormDetail> GetSubStoreCardFormDetailByCardID(string strWhere, int pageNo, string sortExpression, string sortDirection, int returnStore, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<SubStoreCardFormDetail> list = new List<SubStoreCardFormDetail>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = " Select s.CardId,d.CardIssueId,d.StockId,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.UOM,d.QtyIssue,d.Remarks,s.SubStoreIdRef,s.DateValue,sc.Price2 " +
                            " from SubStoreCardForm s " +
                            " INNER join SubStoreCardFormDetail d on s.CardId= d.CardIRef " +
                            " left outer join stockcode sc on sc.stockId = d.stockId  " +
                            " where 1=1 " + strWhere;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                pageNo = returnStore == 1 ? 0 : pageNo;

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<SubStoreCardFormDetail>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }
            return list;
        }

        public IList<employees> GetEmployeesBySubDeptID(int subDeptID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            IList<employees> list = new List<employees>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "EmployeeID";
            }

            string query = "Select employees.* , employeecategories.CategoryName,employeecategories.AltCategoryName ,MaintenanceDepartment.MaintDeptCode," +
                            "MaintSubDept.MaintSubDeptCode,L2.L2Code,MaintSubDept.MaintSubDeptCode,MainenanceDivision.MaintDivisionCode,employeeStatuses.EmployeeStatus,employeeStatuses.EmployeeAltStatus " +
                           "from employees " +
                           " Left Outer join L2 on employees.L2ID = L2.L2ID " +
                           " Left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID =  employees.maintDeptID  " +
                           " left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = employees.MaintSubDeptID  " +
                           " left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = employees.MaintDivisionID  " +
                           " left outer join  employeeStatuses on employeeStatuses.EmployeeStatusId = employees.EmployeeStatusId  " +
                           " left outer join  employeecategories on employeecategories.CategoryID = employees.CategoryID" +
                           " Where employees.MaintSubDeptID = " + subDeptID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }


            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhereAuto = " AND (lower(Name) like N'%" + text + "%' or lower(Positions) like N'%" + text + "%')";
                query += strWhereAuto;
            }
            else
            {
                strWhereAuto = " AND (Name like N'%" + text + "%' or Positions like N'%" + text + "%')";
                query += strWhereAuto;
            }
            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<employees>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public bool LockIssue(int cardId, int statusId, int l2Id)
        {
            try
            {
                using (DapperContext objContext = new DapperContext())
                {
                    string query = "UPDATE SubStoreCardForm set status= " + statusId + " Where CardId = " + cardId;
                    objContext.ExecuteQuery(query, null);

                    string insertQuery = string.Empty;
                    string UpdateQuery = string.Empty;
                    string Issuestring = string.Empty;
                    List<SubStoreCardFormDetail> lstSubStoreCardFormDetail = GetSubStoreCardFormDetailByCardID("and s.CardId = " + cardId, 0, "CardIssueId", "Descending", 1).ToList();
                    if (statusId == 4)
                    {


                        foreach (var lst in lstSubStoreCardFormDetail)
                        {

                            Collection<DBParameters> parameters = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "CreatedDate",
                                    Value = DateTime.Now,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters.Add(new DBParameters()
                                {
                                    Name = "CreatedDate",
                                    Value = Common.GetEnglishDate(DateTime.Now),
                                    DBType = DbType.DateTime
                                });
                            }

                            Collection<DBParameters> parameters1 = new Collection<DBParameters>();
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "DateIssue",
                                    Value = lst.DateValue,
                                    DBType = DbType.DateTime
                                });
                            }
                            else
                            {
                                parameters1.Add(new DBParameters()
                                {
                                    Name = "DateIssue",
                                    Value = Common.GetEnglishDate(lst.DateValue),
                                    DBType = DbType.DateTime
                                });
                            }

                            insertQuery = string.Empty;
                            UpdateQuery = string.Empty;
                            Issuestring = string.Empty;

                            Stockcode_level objStockcode_level = new Stockcode_level();
                            var ListEmpName = objContext.SearchAll(objStockcode_level).Where(o => o.Stock_id == lst.StockId && o.Sub_store_id == lst.SubStoreIdRef).FirstOrDefault();
                            if (ListEmpName == null || ListEmpName.Stock_id == 0)
                            {
                                insertQuery += " Insert into stockcode_levels ( Stock_Id,Sub_Store_Id,max_level,re_order_level,min_level,reorder_qty,Balance,CreatedBy,CreatedDate) " +
                                            " select d.StockId,s.SubStoreIdRef,0,0,0,0,sum(d.QtyIssue),max(d.CreatedBy),@CreatedDate   from SubStoreCardForm s inner join SubStoreCardFormDetail d on s.CardId= d.CardIRef " +
                                            "  where s.CardId =  " + cardId.ToString() + " and d.StockId = " + lst.StockId + " and s.SubStoreIdRef =" + lst.SubStoreIdRef + "  GROUP BY d.StockId,s.SubStoreIdRef ";

                            }

                            Issuestring += " Insert into Issue (StockId, L2Id, SubStoreId,DateIssue,QtyIssue,Price3) " +
                                            " values (" + lst.StockId + "," + l2Id + "," + lst.SubStoreIdRef + ",@DateIssue," + lst.QtyIssue + "," + lst.Price2 + ")";

                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                UpdateQuery += " MERGE INTO stockcode_levels SS USING " +
                                                " (SELECT distinct SS.ROWID row_id, " +
                                                "   SS.Balance - D.QtyIssue AS Balance " +
                                                " FROM SubStoreCardForm S " +
                                                " JOIN SubStoreCardFormDetail D " +
                                                " ON S.CardId = D.CardIRef " +
                                                " JOIN stockcode_levels SS " +
                                                " ON SS.stock_id      = D.StockId " +
                                                " AND SS.sub_store_id = S.SubStoreIdRef " +
                                                " WHERE S.CardId      = " + cardId.ToString() +
                                                " ) src ON ( SS.ROWID = src.row_id ) " +
                                                " WHEN MATCHED THEN " +
                                                "   UPDATE SET SS.Balance = src.Balance ";
                            }
                            else
                            {
                                UpdateQuery += " Update ss set ss.Balance = ss.Balance - d.QtyIssue from SubStoreCardForm s inner join SubStoreCardFormDetail d on s.CardId= d.CardIRef  " +
                                                " inner join stockcode_levels ss on ss.stock_id = d.StockId and ss.sub_store_id = s.SubStoreIdRef where s.CardId =   " + cardId.ToString();
                            }

                            if (insertQuery != string.Empty)
                            {
                                objContext.ExecuteQuery(insertQuery, parameters);
                            }

                            if (Issuestring != string.Empty)
                            {
                                objContext.ExecuteQuery(Issuestring, parameters1);
                            }

                            if (UpdateQuery != string.Empty)
                            {
                                objContext.ExecuteQuery(UpdateQuery, null);
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<PrintConsumableTable> PrintConsumableTableDetails(int cardId)
        {
            List<PrintConsumableTable> objStockCode = new List<PrintConsumableTable>();
            IList<PrintConsumableTable> lstStockCode = new List<PrintConsumableTable>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;
            string strWhereWorkOrderNo = string.Empty;
            if (cardId != 0)
            {
                parameters.Add(new DBParameters()
                {
                    Name = "cardId",
                    Value = cardId,
                    DBType = DbType.String
                });
            }
            query = "Select ROW_NUMBER() OVER (ORDER BY stockcode.StockDescription) AS RowNo, stockcode.AttrGroupID,stockcode.AttrSubID,stockcode.Status,stockcode.StockNo, stockcode.StockDescription,stockcode.AltStockDescription, attrgroup.AttrName, attrgroup.AltAttrName, attrsubgroup.AttrSubName, attrsubgroup.AltAttrSubName,stockcode.Specification, stockcode.Manufacturer, stockcode.Price2, stockcode.AvgPrice, stockcode.Status,stockcode.uom,s.Remarks ,s.QtyIssue from SubStoreCardFormDetail s Left Join stockcode stockcode On stockcode.StockID = s.StockId LEFT OUTER JOIN attrgroup ON stockcode.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN attrsubgroup ON stockcode.AttrSubID = attrsubgroup.AttrSubID where  CardIref = @cardId";
            using (DapperDBContext context = new DapperDBContext())
            {
                lstStockCode = context.ExecuteQuery<PrintConsumableTable>(query, parameters);
            }
            if (lstStockCode != null && lstStockCode.Count > 0)
            {
                objStockCode = lstStockCode.ToList();
            }
            return objStockCode;
        }
        public bool NewSubStoreCardReturOrDispose(List<SubStoreCardFormDetail> lstSubStoreCardFormDetail, int isType)
        {
            try
            {
                string strInsertColumn = string.Empty;
                string strInsertValues = string.Empty;
                string strInsertColumn1 = string.Empty;
                string strInsertValues1 = string.Empty;
                string strUpdateCardFormDetail = string.Empty;
                string strInsertCardFormDetail = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "CurrentDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });

                string L2Id = string.Empty;
                L2Id = Common.GetConfigKeyValue("CityStore");

                int cardId = ConvertTo.Integer(lstSubStoreCardFormDetail[0].CardIRef);
                List<SubStoreCardFormDetail> lstSubStoreCardFormDetailOld = GetSubStoreCardFormDetailByCardID("and s.CardId = " + cardId, 0, "CardIssueId", "Descending", 1).ToList();
                foreach (var lst in lstSubStoreCardFormDetail)
                {
                    SubStoreCardFormDetail objSubStoreCardFormDetail = new SubStoreCardFormDetail();
                    int cardIssueId = lst.CardIssueId;
                    objSubStoreCardFormDetail = lstSubStoreCardFormDetailOld.Where(o => o.CardIssueId == cardIssueId).FirstOrDefault();
                    if (objSubStoreCardFormDetail != null)
                    {
                        if (objSubStoreCardFormDetail.QtyIssue == lst.QtyNeeded)
                        {
                            strUpdateCardFormDetail = " Delete SubStoreCardFormDetail where CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;
                        }
                        if (objSubStoreCardFormDetail.QtyIssue > lst.QtyNeeded)
                        {
                            strUpdateCardFormDetail = " Update SubStoreCardFormDetail set QtyIssue = QtyIssue - " + lst.QtyNeeded + " where CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;
                        }
                    }
                    strInsertColumn1 = string.Empty;
                    strInsertValues1 = string.Empty;
                    strInsertColumn = string.Empty;
                    strInsertValues = string.Empty;

                    strInsertColumn1 += " L2Id, ";
                    strInsertValues1 += ConvertTo.Integer(L2Id) + ", ";

                    if (lst.StockId != 0)
                    {
                        strInsertColumn += " StockIdRef, ";
                        strInsertValues += lst.StockId + ", ";
                        strInsertColumn1 += " StockID, ";
                        strInsertValues1 += lst.StockId + ", ";
                    }
                    if (objSubStoreCardFormDetail.SubStoreIdRef != 0)
                    {
                        strInsertColumn1 += " SubStoreID, ";
                        strInsertValues1 += objSubStoreCardFormDetail.SubStoreIdRef + ", ";
                    }
                    if (objSubStoreCardFormDetail.CardIssueId != 0)
                    {
                        strInsertColumn += " CardIdRef, ";
                        strInsertValues += lst.CardIssueId + ", ";
                    }
                    if (isType != 0)
                    {
                        strInsertColumn += " IsType, ";
                        strInsertValues += isType + ", ";
                    }
                    if (lst.QtyIssue != 0)
                    {
                        strInsertColumn += " QtyIssued, ";
                        strInsertValues += lst.QtyIssue + ", ";
                        if (isType == 1)
                        {
                            strInsertColumn1 += " Qty, ";
                            strInsertValues1 += lst.QtyIssue + ", ";
                        }
                        else
                        {
                            strInsertColumn1 += " QtyAdj, ";
                            strInsertValues1 += lst.QtyIssue + ", ";
                        }
                    }
                    if (lst.Remarks != string.Empty)
                    {
                        strInsertColumn += " Remarks, ";
                        strInsertValues += "'" + lst.Remarks + "', ";

                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            strInsertColumn1 += " Comment_, ";
                            strInsertValues1 += "'" + lst.Remarks + "', ";
                        }
                        else
                        {
                            strInsertColumn1 += " Comment, ";
                            strInsertValues1 += "'" + lst.Remarks + "', ";
                        }
                    }
                    strInsertColumn += " CreatedBy, ";
                    strInsertValues += ProjectSession.EmployeeID.ToString() + ", ";
                    strInsertColumn += " CreatedDate ";
                    strInsertValues += "@CurrentDate ";

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        strInsertColumn1 += " Date_ ";
                        strInsertValues1 += "@CurrentDate ";
                    }
                    else
                    {
                        strInsertColumn1 += " Date ";
                        strInsertValues1 += "@CurrentDate ";
                    }
                    string UpdateQuery1 = string.Empty;
                    string UpdateQuery2 = string.Empty;

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        UpdateQuery1 = " MERGE INTO stockcode_levels SS USING " +
                                    " (SELECT distinct SS.ROWID row_id, " +
                                    "   SS.Balance + " + lst.QtyNeeded + " AS Balance " +
                                    " FROM SubStoreCardFormDetail D " +
                                    " JOIN SubStoreCardForm S " +
                                    " ON D.CardIRef = S.CardId " +
                                    " JOIN stockcode_levels SS " +
                                    " ON SS.stock_id      = D.StockId " +
                                    " AND SS.sub_store_id = S.SubStoreIdRef " +
                                    " WHERE D.CardIssueId =  " + objSubStoreCardFormDetail.CardIssueId +
                                    " ) src ON ( SS.ROWID = src.row_id ) " +
                                    " WHEN MATCHED THEN " +
                                    "   UPDATE SET SS.Balance = src.Balance";

                        //UpdateQuery = " update   stockcode_levels s " +
                        // " SET s.Balance = s.Balance + " + lst.QtyNeeded +
                        // " Where (s.Stock_ID ) in ( " +
                        // "       SELECT SubStoreCardFormDetail.StockId             " +
                        // "        FROM SubStoreCardFormDetail " +
                        // "        INNER JOIN SubStoreCardForm  ON SubStoreCardFormDetail.CardIRef = SubStoreCardForm.CardId " +
                        // "        WHERE SubStoreCardFormDetail.CardIssueId = " + objSubStoreCardFormDetail.CardIssueId + " )  " +
                        // "  and s.Sub_Store_id in  " +
                        // "  (SELECT SubStoreCardForm.SubStoreIdRef " +
                        // "        FROM SubStoreCardFormDetail " +
                        // "        INNER JOIN SubStoreCardForm ON SubStoreCardFormDetail.CardIRef = SubStoreCardForm.CardId " +
                        // "        WHERE SubStoreCardFormDetail.CardIssueId = " + objSubStoreCardFormDetail.CardIssueId + " ) ";

                        if (isType == 2)
                        {

                            UpdateQuery2 += "MERGE INTO stockCode SS USING " +
                                            " (SELECT distinct SS.ROWID row_id, " +
                                            "   2 " +
                                            " FROM SubStoreCardFormDetail D " +
                                            " JOIN stockCode SS " +
                                            " ON SS.stockid       = D.StockId " +
                                            " WHERE D.CardIssueId = " + objSubStoreCardFormDetail.CardIssueId +
                                            " ) src ON ( SS.ROWID = src.row_id ) " +
                                            " WHEN MATCHED THEN " +
                                            "   UPDATE SET SS.STATUS = 2";

                            //UpdateQuery = "; update stockCode set stockCode.status = 2 " +
                            //              " where stockid in (Select StockId from SubStoreCardFormDetail where  CardIssueId =  " + objSubStoreCardFormDetail.CardIssueId + ") ";
                        }
                    }
                    else
                    {
                        UpdateQuery1 = " Update ss set ss.Balance = ss.Balance + " + lst.QtyNeeded + " from   SubStoreCardFormDetail d inner join SubStoreCardForm s on d.CardIRef=s.CardId   " +
                                            " inner join stockcode_levels ss on ss.stock_id = d.StockId and ss.sub_store_id = s.SubStoreIdRef where  d.CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;

                        if (isType == 2)
                        {
                            UpdateQuery2 += " Update ss set ss.status = 2 from   SubStoreCardFormDetail d    " +
                                           " inner join stockCode ss on ss.stockid = d.StockId  where  d.CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;
                        }
                    }

                    using (DapperContext objContext = new DapperContext())
                    {
                        if (UpdateQuery1 != string.Empty)
                        {
                            objContext.ExecuteQuery(UpdateQuery1, new Collection<DBParameters>());
                        }
                        if (UpdateQuery2 != string.Empty)
                        {
                            objContext.ExecuteQuery(UpdateQuery2, parameters);
                        }

                        string strUpdateCardFormDetail1 = " INSERT INTO SubStoreFormRetunDetail(" + strInsertColumn + ")" +
                                                   " VALUES(" + strInsertValues + ")";

                        if (strUpdateCardFormDetail != string.Empty)
                        {
                            objContext.ExecuteQuery(strUpdateCardFormDetail, parameters);
                            objContext.ExecuteQuery(strUpdateCardFormDetail1, parameters);
                        }

                        if (isType == 1)
                        {
                            string Query1 = string.Empty;
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                            {
                                Query1 = "INSERT INTO `return`(" + strInsertColumn1 + ") " +
                                           " VALUES(" + strInsertValues1 + ")";

                            }
                            else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                Query1 = "INSERT INTO \"RETURN\"(" + strInsertColumn1 + ") VALUES(" + strInsertValues1 + ")";

                            }
                            else
                            {
                                Query1 = "INSERT INTO [return](" + strInsertColumn1 + ") " +
                                           " VALUES(" + strInsertValues1 + ")";
                            }

                            objContext.ExecuteQuery(Query1, parameters);
                        }
                        else
                        {
                            string Query2 = "INSERT INTO [adjustment](" + strInsertColumn1 + ")  VALUES(" + strInsertValues1 + ")";

                            objContext.ExecuteQuery(Query2, parameters);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Personal"

        public IList<employees> GetEmployeesByL2ID(int l2Id, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<employees> list = new List<employees>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "EmployeeId";
            }

            //string query = "Select EmployeeId, EmployeeNo, Name, AltName,Positions,maintDeptID,MaintSubDeptID,MaintDivisionID from Employees where EmployeeStatusId = 1 and Central =1 or EmployeeId " +
            //                "in (select empID from employees_L2 where L2Id = " + l2Id + " union Select EmployeeId from employees where L2Id = " + l2Id + ") ";

            string query = "Select employees.EmployeeID,employees.L2ID,L2.L2Code,employees.CategoryID,employeecategories.CategoryName,employeecategories.AltCategoryName,employees.WorkTradeID,employees.UserGroupId,employees.EmployeeNO,employees.Name,employees.AltName,employees.Positions,employees.Extension, "
                            + " employees.WorkPhone,employees.HandPhone,employees.Email,employees.Fax,employees.Housephone,employees.Address,employees.OfficeLocation, " +
                            " employees.HourlySalary,employees.OverTime1,employees.OverTime2,employees.OverTime3,employees.Accessibility,  " +
                            " employees.UserID,employees.Password,employees.Central,employees.Auth,employees.ExecLevel,employees.WebAccess,  " +
                            " employees.OpenWorkorders,employees.ReopenWorkorders,employees.Connected,employees.WRConnected,employees.AllowAdjustment, "
                            + " employees.pr_auto_amount,employees.EmpDeptCode,employees.AllowCancelPO,employees.ConnectedMW,employees.WOReturn," +
                            " employees.WOIssue,employees.hide_cost,employees.LanguageCode,employees.AllowInterSiteTransfer,employees.postal_address," +
                            " employees.maintDeptID,MaintenanceDepartment.MaintDeptCode,employees.MaintSubDeptID,MaintSubDept.MaintSubDeptCode,employees.MaintDivisionID,MainenanceDivision.MaintDivisionCode,employees.EmployeeStatusId,employeeStatuses.EmployeeStatus,employeeStatuses.EmployeeAltStatus,employees.CreatedBy, " +
                            " employees.CreatedDate,employees.ModifiedBy,employees.ModifiedDate " +
                            " ,employees.TBtransaction,employees.TBSetup,employees.TBReport,employees.TBDashboard " +
                            " FROM  employees Left Outer join L2 on employees.L2ID = L2.L2ID  Left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID =  employees.maintDeptID " +
                            " left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = employees.MaintSubDeptID " +
                            " left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = employees.MaintDivisionID " +
                            " left outer join  employeeStatuses on employeeStatuses.EmployeeStatusId = employees.EmployeeStatusId " +
                            " left outer join  employeecategories on employeecategories.CategoryID = employees.CategoryID " +
                            " where (employeeStatuses.EmployeeStatusId = 1 and Central = 1 or EmployeeId " +
                            " in (select empID from employees_L2 where L2Id = " + l2Id + " union Select EmployeeId from employees where L2Id = " + l2Id + ")) ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<employees>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static List<SubStoreCardForm> GetCardNobyEmployeeId(int employeeid, int isToCard)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "Select CardId,CardNo from SubStoreCardForm where EmployeeId = " + employeeid;
                if (isToCard == 1)
                {
                    query += " and Status = 2 ";
                }

                return context.ExecuteQuery<SubStoreCardForm>(query, parameters).ToList();
            }
        }

        public bool NewSubStoreCardTransfer(List<SubStoreCardFormDetail> lstSubStoreCardFormDetail, int ToCardId, int FromCardId, int ToEmployeeId, int FromEmployeeId)
        {
            try
            {
                string strUpdateCardFormDetail = string.Empty;
                string strInsertCardFormDetail = string.Empty;
                string strInsertCardTransfer = string.Empty;
                int cardId = ConvertTo.Integer(lstSubStoreCardFormDetail[0].CardIRef);
                List<SubStoreCardFormDetail> lstSubStoreCardFormDetailOld = GetSubStoreCardFormDetailByCardID("and s.CardId = " + cardId, 0, "CardIssueId", "Descending", 1).ToList();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "CurrentDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });
                foreach (var lst in lstSubStoreCardFormDetail)
                {
                    strUpdateCardFormDetail = string.Empty;
                    strInsertCardFormDetail = string.Empty;
                    strInsertCardTransfer = string.Empty;
                    SubStoreCardFormDetail objSubStoreCardFormDetail = new SubStoreCardFormDetail();
                    int cardIssueId = lst.CardIssueId;
                    objSubStoreCardFormDetail = lstSubStoreCardFormDetailOld.Where(o => o.CardIssueId == cardIssueId).FirstOrDefault();
                    if (objSubStoreCardFormDetail != null)
                    {
                        if (objSubStoreCardFormDetail.QtyIssue == lst.QtyNeeded)
                        {
                            strUpdateCardFormDetail = " Update SubStoreCardFormDetail set CardIRef = " + ToCardId + " where CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;
                        }
                        if (objSubStoreCardFormDetail.QtyIssue > lst.QtyNeeded)
                        {
                            strUpdateCardFormDetail = " Update SubStoreCardFormDetail set QtyIssue = QtyIssue - " + lst.QtyNeeded + " where CardIssueId = " + objSubStoreCardFormDetail.CardIssueId;
                            strInsertCardFormDetail = "INSERT INTO SubStoreCardFormDetail(StockId,CardIRef,QtyIssue,Remarks,CreatedBy,CreatedDate) VALUES(" + lst.StockId + "," + ToCardId + "," + lst.QtyNeeded + ",'" + lst.Remarks + "'," + ProjectSession.EmployeeID.ToString() + ",@CurrentDate)";
                        }
                    }
                    strInsertCardTransfer = "INSERT INTO SubStoreCardTransferDetail(StockIdRef,ToEmployeeId,FromEmployeeId,ToCardId,FromCardId,CurrentyQty,QtyNeeded,Remarks,CreatedBy,CreatedDate) " +
                                                  "VALUES(" + lst.StockId + "," + ToEmployeeId + "," + FromEmployeeId + "," + ToCardId + "," + FromCardId + "," + lst.QtyIssue + "," + lst.QtyNeeded + ",'" + lst.Remarks + "'," + ProjectSession.EmployeeID.ToString() + ",@CurrentDate)";

                    using (DapperContext objContext = new DapperContext())
                    {
                        if (strUpdateCardFormDetail != string.Empty)
                        {
                            objContext.ExecuteQuery(strUpdateCardFormDetail, parameters);
                        }

                        if (strInsertCardTransfer != string.Empty)
                        {
                            objContext.ExecuteQuery(strInsertCardTransfer, parameters);
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Items list"

        public bool UpdateStockCodeCDCTableOnDeleteItem(int id)
        {
            try
            {
                if (ProjectSession.ModulesAccess.AuditTrail.HasValue ? !Convert.ToBoolean(ProjectSession.ModulesAccess.AuditTrail) : false)
                {
                    return true;
                }

                int result = -1;
                string strSQl = string.Empty;
                strSQl = "Update cdc.dbo_StockCode_CT set ModifiedBy='" + ProjectSession.EmployeeID.ToString() + "', ModifiedDate = '" + Common.GetEnglishDate(DateTime.Now) + "'  where  StockID = " + id + " and __$operation = 1 ";
                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, new Collection<DBParameters>());
                }

                if (result == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Basic Information"

        public List<String> GetLastStocCodeStoreNo()
        {
            try
            {
                string strQuery = string.Empty;
                string strQuery2 = string.Empty;
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = "Select StockNo from StockCode where lower(StockNo) like lower('%CON-%') order by StockNo desc";
                }
                else
                {
                    strQuery = "Select StockNo from StockCode where StockNo like '%CON-%' order by StockNo desc";
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery2 = "Select StockNo from StockCode where lower(StockNo) like lower('%PER-%') order by StockNo desc";
                }
                else
                {
                    strQuery2 = "Select StockNo from StockCode where StockNo like '%PER-%' order by StockNo desc";
                }

                string strCON = string.Empty;
                string strPER = string.Empty;

                using (ServiceContext objContext = new ServiceContext())
                {
                    strCON = objContext.ExecuteQuery<string>(strQuery, new Collection<DBParameters>()).FirstOrDefault();
                    strPER = objContext.ExecuteQuery<string>(strQuery2, new Collection<DBParameters>()).FirstOrDefault();
                }

                return new List<string>() { strCON, strPER };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool isStockNoDuplicate(string stockNo, int StockID)
        {
            try
            {
                string strSQL;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (StockID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = StockID,
                        DBType = DbType.String
                    });

                    strSQL = "Select * from StockCode Where StockNo = @StockNo and StockID != @StockID";
                }
                else
                {
                    strSQL = "Select * from StockCode Where StockNo = @StockNo";
                }

                string strCount = string.Empty;
                StockCode obj = new StockCode();

                parameters.Add(new DBParameters()
                {
                    Name = "StockNo",
                    Value = stockNo,
                    DBType = DbType.String
                });

                using (ServiceContext objContext = new ServiceContext())
                {
                    obj = objContext.ExecuteQuery<StockCode>(strSQL, parameters).FirstOrDefault();
                }

                if (obj != null && obj.StockID > 0 && !string.IsNullOrEmpty(obj.StockNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Stock Alternatives"

        public bool DeleteAlternativeItembyStockId(int stockId, int partId)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (stockId > 0 && partId > 0)
                {
                    query = "delete from stock_alternative where StockID = " + stockId.ToString() + " And AlternativePart= '" + partId.ToString() + "'";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Stock_alternative> GetAlternativeItembyStockID(int stockId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Stock_alternative> list = new List<Stock_alternative>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockId > 0)
                {
                    strWhere = " And sa.StockID = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockId,
                        DBType = DbType.Int32
                    });

                }

                string query = "Select s1.StockNo,s1.StockDescription,s1.AltStockDescription,sa.AlternativePart from stock_alternative sa inner join StockCode s on ";
                query += "sa.StockID = s.StockID inner join StockCode s1 on sa.AlternativePart = s1.StockID " + strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stock_alternative>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool SaveAlternativeItem(int stockCodeId, int altPartId)
        {
            try
            {
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                if (stockCodeId > 0 && altPartId > 0)
                {
                    query = "Select count(1) from stock_alternative where StockID=" + stockCodeId.ToString() + " and AlternativePart=" + altPartId.ToString() + "";

                    string cntDuplicate = "0";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        cntDuplicate = objContext.ExecuteScalar(query);
                        cntDuplicate = string.IsNullOrEmpty(cntDuplicate) ? "0" : cntDuplicate;
                    }

                    if (Convert.ToInt32(cntDuplicate) == 0)
                    {
                        query = "Insert Into stock_alternative (StockID,AlternativePart,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) ";
                        query += " Values(" + stockCodeId.ToString() + "," + altPartId.ToString() + "," + ProjectSession.EmployeeID.ToString() + ",@CurrentDate,NULL,NULL)";

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objContext.ExecuteQuery(query, parameters);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Suppliers Alternatives"

        public bool DeleteAlternativeSupplierbyStockId(int stockId, int supplierId)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (stockId > 0 && supplierId > 0)
                {
                    query = "delete from Stock_code_supplier where stock_id = " + stockId.ToString() + " And supplier_id= '" + supplierId.ToString() + "'";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Stock_code_supplier> GetAlternativeSuppliersByStockID(int stockId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Stock_code_supplier> list = new List<Stock_code_supplier>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockId > 0)
                {
                    strWhere = " And ss.stock_id = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockId,
                        DBType = DbType.Int32
                    });

                }

                string query = "Select ss.supplier_id ,s.SupplierNo,s.SupplierName,s.AltSupplierName from Stock_code_supplier ss inner join Suppliers s on ";
                query += "ss.supplier_id = s.SupplierID " + strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stock_code_supplier>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool SaveAlternativeSupplier(int stockCodeId, int supplierId)
        {
            try
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }

                if (stockCodeId > 0 && supplierId > 0)
                {
                    query = "Select count(1) from stock_code_supplier where stock_id=" + stockCodeId.ToString() + " and supplier_id=" + supplierId.ToString() + "";

                    string cntDuplicate = "0";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        cntDuplicate = objContext.ExecuteScalar(query);
                        cntDuplicate = string.IsNullOrEmpty(cntDuplicate) ? "0" : cntDuplicate;
                    }

                    if (Convert.ToInt32(cntDuplicate) == 0)
                    {
                        query = "Insert Into stock_code_supplier (stock_id,supplier_id,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) ";
                        query += " Values(" + stockCodeId.ToString() + "," + supplierId.ToString() + "," + ProjectSession.EmployeeID.ToString() + ",@CurrentDate,NULL,NULL)";

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objContext.ExecuteQuery(query, parameters);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Stock_Code_Levels"

        public IList<Stockcode_level> GetStockCodeLevelByStockId(int stockId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Stockcode_level> list = new List<Stockcode_level>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockId > 0)
                {
                    strWhere = " Where sl.stock_id = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockId,
                        DBType = DbType.Int32
                    });

                }

                string query = "Select sl.sub_store_id,s.SubStoreCode,sl.max_level,sl.re_order_level,sl.min_level,sl.reorder_qty,pl.PartLocation ";
                query += " from Stockcode_levels sl inner join substore s on sl.sub_store_id = s.SubStoreID left outer join PartLocation pl on sl.PartLocationID = pl.ID " + strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stockcode_level>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool DeleteStockCodeLevel(int stockId, int substoreid)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;
                if (stockId > 0 && substoreid > 0)
                {
                    query = "delete from Stockcode_levels where stock_id = " + stockId.ToString() + " And sub_store_id= '" + substoreid.ToString() + "'";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveStockCodeLevel(Stockcode_level obj)
        {
            try
            {
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CurrentDate",
                        Value = Common.GetEnglishDate(DateTime.Now),
                        DBType = DbType.DateTime
                    });
                }
                if (obj.Stock_id > 0 && obj.Sub_store_id > 0)
                {
                    query = "Insert Into Stockcode_levels (stock_id,sub_store_id,PartLocationID,max_level,re_order_level,min_level,reorder_qty,Balance,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate) ";
                    query += " Values(" + obj.Stock_id.ToString() + "," + obj.Sub_store_id.ToString() + "," + (obj.PartLocationID == null ? "NULL" : obj.PartLocationID.ToString()) + "," + obj.Max_level.ToString() + "," + obj.Re_order_level.ToString() + "," + obj.Min_level.ToString() + "," + obj.Reorder_qty.ToString() + "," + (obj.Balance == null ? "NULL" : obj.Balance.ToString()) + ",'" + ProjectSession.EmployeeID.ToString() + "',@CurrentDate,NULL,NULL)";

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(query, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Stockcode_level> GetStockCodeLevelCityByStockId(int stockId, int pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                IList<Stockcode_level> list = new List<Stockcode_level>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strWhere = string.Empty;

                if (stockId > 0)
                {
                    strWhere = " And sl.stock_id = @StockID";

                    parameters.Add(new DBParameters()
                    {
                        Name = "StockID",
                        Value = stockId,
                        DBType = DbType.Int32
                    });

                }

                string query = "Select sl.stock_id,sl.sub_store_id,L2.L2Code,s.SubStoreCode,sl.balance from Stockcode_levels sl inner join substore s on";
                query += " sl.sub_store_id = s.SubStoreID inner join L2 On s.L2ID = L2.L2ID " + strWhere;

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Stockcode_level>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion
    }
}
