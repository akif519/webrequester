﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Services;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.ConfigMaster
{
    public class AuthenticationData : DBExecute
    {
        public AuthenticationData()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }
        public static Tbl_AdminUser GetAccount(string username, string password, int clientId)
        {
            try
            {
                CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
                using (ServiceContext context = new ServiceContext())
                {
                    Tbl_AdminUser objAdminUser = new Tbl_AdminUser();
                    objAdminUser.UserName = username;
                    objAdminUser.Password = password;
                    objAdminUser.UserID = clientId;
                    return context.Search(objAdminUser, null, null, null).FirstOrDefault();
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Account> GetAccountList(string strWhere, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strWhereClause = string.Empty;
            IList<Account> list = new List<Account>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string query = string.Empty;

            //query += @"USE master;";
            query += @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;";
            query += @"SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts]";
            //query += @"Select ClientID,UserName,Password,ConCurrentUsers,MobAppLicCon,MobAppLicNamed,SubCycle,(SELECT CONVERT(datetime, DateCreated))as DateCreated,(SELECT CONVERT(datetime, ExpDate))as ExpDate,AccStatus,dbName,dbConString from Accounts";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);
            if (!string.IsNullOrEmpty(strWhereClause))
            {
                if (!string.IsNullOrEmpty(text))
                {
                    query += " AND " + strWhereClause;
                }
                else
                {
                    query += " Where " + strWhereClause;
                }
            }
            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Account>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            foreach (Account ac in list)
            {
                if (!System.IO.File.Exists(ac.SharedFolderPath + "\\" + ac.ClientCode + "\\HeaderImages\\" + ac.HdrLogo))
                {
                    ac.HdrLogo = "";
                }

                if (!System.IO.File.Exists(ac.SharedFolderPath + "\\" + ac.ClientCode + "\\HeaderImages\\" + ac.RprtLogo))
                {
                    ac.RprtLogo = "";
                }
            }

            return list;
        }
    }

}
