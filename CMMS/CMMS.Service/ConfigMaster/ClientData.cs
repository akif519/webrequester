﻿using CMMS.DAL;
using CMMS.Model;
using CMMS.Service.UserAuthentication;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.ConfigMaster
{
  public class ClientData : DBExecute
    {
      public static Account GetClientDetailByID(int ClientID)
      {
          //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
          List<Account> lstobjAcc = new List<Account>();
          Account objClientDetail = new Account();
          List<TabAccessModel> lstobjTabAccess = new List<TabAccessModel>();
          TabAccessModel objTabAccess = new TabAccessModel();
          List<ReportAccessModel> lstobjReportAccess = new List<ReportAccessModel>();
          ReportAccessModel objReportAcces = new ReportAccessModel();

          List<FeatureAccessModel> lstobjFeatureAccess = new List<FeatureAccessModel>();
          FeatureAccessModel objFeatureAcces = new FeatureAccessModel();

          List<ModulesAccessModel> lstobjModuleAccess = new List<ModulesAccessModel>();
          ModulesAccessModel objModuleAcces = new ModulesAccessModel();

          List<LevelNamesModel> lstobjLevelNames = new List<LevelNamesModel>();
          LevelNamesModel objModuleLevelNames = new LevelNamesModel();

          List<CEUML_configModel> lstCEUML_config = new List<CEUML_configModel>();
          CEUML_configModel objCEUML_configModel = new CEUML_configModel();

          List<FirstLevelTblModel> lstFirstLevelTblModel = new List<FirstLevelTblModel>();
          FirstLevelTblModel objFirstLevelTblModel = new FirstLevelTblModel();

          Collection<DBParameters> parameters = new Collection<DBParameters>();
          parameters.Add(new DBParameters()
          {
              Name = "clientID",
              Value = ClientID,
              DBType = DbType.Int32
          });
          string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";
          string query1 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(int, DecryptByKey(Transactions)) AS 'Transactions',CONVERT(int, DecryptByKey(Setup)) AS 'Setup',CONVERT(int, DecryptByKey(Reports)) AS 'Reports',CONVERT(int, DecryptByKey(Dashboard)) AS 'Dashboard' FROM [dbo].[TabAccess] WHERE ClientID = @ClientID";
          string query2 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(int, DecryptByKey(Assets)) AS 'Assets',CONVERT(int, DecryptByKey(Preventive)) AS 'Preventive',CONVERT(int, DecryptByKey(Store)) AS 'Store',CONVERT(int, DecryptByKey(Employee)) AS 'Employee',CONVERT(int, DecryptByKey(Setups)) AS 'Setups',CONVERT(int, DecryptByKey(Purchasing)) AS 'Purchasing',CONVERT(int, DecryptByKey(Cleaning)) AS 'Cleaning',CONVERT(int, DecryptByKey([JobOrder])) AS 'JobOrder'FROM [dbo].[ReportAccess]  WHERE ClientID = @ClientID";
          string query3 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebRequesterLicence)) AS 'WebRequesterLicence'FROM [dbo].[Tbl_Feature] WHERE ClientID = @ClientID";          
          string query4 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,CONVERT(bit, DecryptByKey(AuditTrail)) AS 'AuditTrail',CONVERT(bit, DecryptByKey(JobRequest)) AS 'JobRequest',CONVERT(bit, DecryptByKey(JobOrder)) AS 'JobOrder',CONVERT(bit, DecryptByKey(Assets)) AS 'Assets',CONVERT(bit, DecryptByKey(Locations)) AS 'Locations',CONVERT(bit, DecryptByKey(PreventiveMaintenance)) AS 'PreventiveMaintenance', CONVERT(bit, DecryptByKey(Store)) AS 'Store', CONVERT(bit, DecryptByKey(Purchasing)) AS 'Purchasing', CONVERT(bit, DecryptByKey(Employee)) AS 'Employee', CONVERT(bit, DecryptByKey(Cleaning)) AS 'Cleaning', CONVERT(bit, DecryptByKey(CEUML)) AS 'CEUML', CONVERT(bit, DecryptByKey(Sector)) AS 'Sector', CONVERT(bit, DecryptByKey(Zone)) AS 'Zone', CONVERT(bit, DecryptByKey(Building)) AS 'Building', CONVERT(bit, DecryptByKey(Division)) AS 'Division', CONVERT(bit, DecryptByKey(Department)) AS 'Department', CONVERT(bit, DecryptByKey(SubDept)) AS 'SubDept', CONVERT(bit, DecryptByKey(Groups)) AS 'Groups', CONVERT(bit, DecryptByKey(FailureCodes)) AS 'FailureCodes', CONVERT(bit, DecryptByKey(Suppliers_Contractors)) AS 'Suppliers_Contractors', CONVERT(bit, DecryptByKey(BoMList)) AS 'BoMList', CONVERT(bit, DecryptByKey(CostCenter)) AS 'CostCenter', CONVERT(bit, DecryptByKey(AccountCode)) AS 'AccountCode', CONVERT(bit, DecryptByKey(JobPlans)) AS 'JobPlans', CONVERT(bit, DecryptByKey(SafetyInstructions)) AS 'SafetyInstructions', CONVERT(bit, DecryptByKey(SiteSetups)) AS 'SiteSetups', CONVERT(bit, DecryptByKey(StoreSetups)) AS 'StoreSetups', CONVERT(bit, DecryptByKey(PurchaseRequisition)) AS 'PurchaseRequisition', CONVERT(bit, DecryptByKey(JobManagement)) AS 'JobManagement', CONVERT(bit, DecryptByKey(AssetSetup)) AS 'AssetSetup', CONVERT(bit, DecryptByKey(Employee_User)) AS 'Employee_User', CONVERT(bit, DecryptByKey(CleaningSetup)) AS 'CleaningSetup', CONVERT(bit, DecryptByKey(PurchaseSetup)) AS 'PurchaseSetup', CONVERT(bit, DecryptByKey(NotificationSetup)) AS 'NotificationSetup', CONVERT(bit, DecryptByKey(ItemRequisition)) AS 'ItemRequisition', CONVERT(bit, DecryptByKey(RequestCard)) AS 'RequestCard',CONVERT(bit, DecryptByKey(Help)) AS 'Help',CONVERT(bit, DecryptByKey(SLA)) AS 'SLA'  FROM dbo.ModulesAccess WHERE ClientID = @ClientID";
          string query5 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(nvarchar(max), DecryptByKey(L2_en)) AS 'L2_en',CONVERT(nvarchar(max), DecryptByKey(L2_ar)) AS 'L2_ar',CONVERT(nvarchar(max), DecryptByKey(L3_en)) AS 'L3_en',CONVERT(nvarchar(max), DecryptByKey(L3_ar)) AS 'L3_ar',CONVERT(nvarchar(max), DecryptByKey(L4_en)) AS 'L4_en',CONVERT(nvarchar(max), DecryptByKey(L4_ar)) AS 'L4_ar',CONVERT(nvarchar(max), DecryptByKey(L5_en)) AS 'L5_en',CONVERT(nvarchar(max), DecryptByKey(L5_ar)) AS 'L5_ar'FROM [dbo].[LevelNamesTbl] WHERE L1_ID = @ClientID";
          string query6 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(int, DecryptByKey(CEUML_configId)) AS 'CEUML_configId',CONVERT(nvarchar(max), DecryptByKey(ceUser)) AS 'ceUser',CONVERT(nvarchar(max), DecryptByKey(Token)) AS 'Token'FROM [dbo].[CEUML_config] WHERE ClientID = @ClientID";
          string query7 = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(nvarchar(max), DecryptByKey(L1_en)) AS 'L1_en',CONVERT(nvarchar(max), DecryptByKey(L1_ar)) AS 'L1_ar'FROM [dbo].[FirstLevelTbl] Where ClientID = @ClientID";
          using (DapperDBContext context = new DapperDBContext())
          {
              objClientDetail = context.ExecuteQuery<Account>(query, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objTabAccess = context.ExecuteQuery<TabAccessModel>(query1, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objReportAcces = context.ExecuteQuery<ReportAccessModel>(query2, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objFeatureAcces = context.ExecuteQuery<FeatureAccessModel>(query3, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objModuleAcces = context.ExecuteQuery<ModulesAccessModel>(query4, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objModuleLevelNames = context.ExecuteQuery<LevelNamesModel>(query5, parameters).FirstOrDefault();
              //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
              objCEUML_configModel = context.ExecuteQuery<CEUML_configModel>(query6, parameters).FirstOrDefault();

              objFirstLevelTblModel = context.ExecuteQuery<FirstLevelTblModel>(query7, parameters).FirstOrDefault();
          }
          lstobjTabAccess.Add(objTabAccess);
          lstobjReportAccess.Add(objReportAcces);
          lstobjFeatureAccess.Add(objFeatureAcces);
          lstobjModuleAccess.Add(objModuleAcces);
          lstobjLevelNames.Add(objModuleLevelNames);
          lstCEUML_config.Add(objCEUML_configModel);
          lstFirstLevelTblModel.Add(objFirstLevelTblModel);
          if (objTabAccess != null)
          {
              objClientDetail.TabAccess = new TabAccessModel();
              objClientDetail.TabAccess.ClientID = lstobjTabAccess.FirstOrDefault().ClientID;
              objClientDetail.TabAccess.Transactions = lstobjTabAccess.FirstOrDefault().Transactions;
              objClientDetail.TabAccess.Reports = lstobjTabAccess.FirstOrDefault().Reports;
              objClientDetail.TabAccess.Dashboard = lstobjTabAccess.FirstOrDefault().Dashboard;
              objClientDetail.TabAccess.Setup = lstobjTabAccess.FirstOrDefault().Setup;   
          }
          if (objReportAcces != null)
          {
              objClientDetail.ReportsAccess = new ReportAccessModel();
              objClientDetail.ReportsAccess.ClientID = lstobjReportAccess.FirstOrDefault().ClientID;
              objClientDetail.ReportsAccess.Assets = lstobjReportAccess.FirstOrDefault().Assets;
              objClientDetail.ReportsAccess.JobOrder = lstobjReportAccess.FirstOrDefault().JobOrder;
              objClientDetail.ReportsAccess.Preventive = lstobjReportAccess.FirstOrDefault().Preventive;
              objClientDetail.ReportsAccess.Store = lstobjReportAccess.FirstOrDefault().Store;
              objClientDetail.ReportsAccess.Employee = lstobjReportAccess.FirstOrDefault().Employee;
              objClientDetail.ReportsAccess.Purchasing = lstobjReportAccess.FirstOrDefault().Purchasing;
              objClientDetail.ReportsAccess.Cleaning = lstobjReportAccess.FirstOrDefault().Cleaning;
              objClientDetail.ReportsAccess.Setups = lstobjReportAccess.FirstOrDefault().Setups;    
          }
          if (objFeatureAcces != null)
          {
              objClientDetail.FeatureAccess = new FeatureAccessModel();
              objClientDetail.FeatureAccess.ClientID = lstobjFeatureAccess.FirstOrDefault().ClientID;
              objClientDetail.FeatureAccess.Area = lstobjFeatureAccess.FirstOrDefault().Area;
              objClientDetail.FeatureAccess.CMProject = lstobjFeatureAccess.FirstOrDefault().CMProject;
              objClientDetail.FeatureAccess.CMProjectNoMandatory = lstobjFeatureAccess.FirstOrDefault().CMProjectNoMandatory;
              objClientDetail.FeatureAccess.WebRequesterLicence = lstobjFeatureAccess.FirstOrDefault().WebRequesterLicence; 
          }
          if (objModuleAcces != null)
	        {
                objClientDetail.ModulesAccess = new ModulesAccessModel();
             objClientDetail.ModulesAccess.ClientID = lstobjModuleAccess.FirstOrDefault().ClientID;
             objClientDetail.ModulesAccess.Assets = lstobjModuleAccess.FirstOrDefault().Assets;
             objClientDetail.ModulesAccess.JobOrder = lstobjModuleAccess.FirstOrDefault().JobOrder;
             objClientDetail.ModulesAccess.PreventiveMaintenance = lstobjModuleAccess.FirstOrDefault().PreventiveMaintenance;
             objClientDetail.ModulesAccess.Employee = lstobjModuleAccess.FirstOrDefault().Employee;
             objClientDetail.ModulesAccess.AssetSetup = lstobjModuleAccess.FirstOrDefault().AssetSetup;
             objClientDetail.ModulesAccess.Purchasing = lstobjModuleAccess.FirstOrDefault().Purchasing;
             objClientDetail.ModulesAccess.Cleaning = lstobjModuleAccess.FirstOrDefault().Cleaning;
                             
             objClientDetail.ModulesAccess.AccountCode = lstobjModuleAccess.FirstOrDefault().AccountCode;
             objClientDetail.ModulesAccess.AuditTrail = lstobjModuleAccess.FirstOrDefault().AuditTrail;
             objClientDetail.ModulesAccess.BoMList = lstobjModuleAccess.FirstOrDefault().BoMList;
             objClientDetail.ModulesAccess.Building = lstobjModuleAccess.FirstOrDefault().Building;
             objClientDetail.ModulesAccess.CEUML = lstobjModuleAccess.FirstOrDefault().CEUML;
             objClientDetail.ModulesAccess.CleaningSetup = lstobjModuleAccess.FirstOrDefault().CleaningSetup;
             objClientDetail.ModulesAccess.CostCenter = lstobjModuleAccess.FirstOrDefault().CostCenter;
                             
             objClientDetail.ModulesAccess.Department = lstobjModuleAccess.FirstOrDefault().Department;
             objClientDetail.ModulesAccess.Division = lstobjModuleAccess.FirstOrDefault().Division;
             objClientDetail.ModulesAccess.Employee_User = lstobjModuleAccess.FirstOrDefault().Employee_User;
             objClientDetail.ModulesAccess.FailureCodes = lstobjModuleAccess.FirstOrDefault().FailureCodes;
             objClientDetail.ModulesAccess.Groups = lstobjModuleAccess.FirstOrDefault().Groups;
             objClientDetail.ModulesAccess.ItemRequisition = lstobjModuleAccess.FirstOrDefault().ItemRequisition;
             objClientDetail.ModulesAccess.JobManagement = lstobjModuleAccess.FirstOrDefault().JobManagement;
                             
             objClientDetail.ModulesAccess.JobPlans = lstobjModuleAccess.FirstOrDefault().JobPlans;
             objClientDetail.ModulesAccess.JobRequest = lstobjModuleAccess.FirstOrDefault().JobRequest;
             objClientDetail.ModulesAccess.Locations = lstobjModuleAccess.FirstOrDefault().Locations;
             objClientDetail.ModulesAccess.NotificationSetup = lstobjModuleAccess.FirstOrDefault().NotificationSetup;
             objClientDetail.ModulesAccess.PurchaseRequisition = lstobjModuleAccess.FirstOrDefault().PurchaseRequisition;
             objClientDetail.ModulesAccess.PurchaseSetup = lstobjModuleAccess.FirstOrDefault().PurchaseSetup;
             objClientDetail.ModulesAccess.SafetyInstructions = lstobjModuleAccess.FirstOrDefault().SafetyInstructions;
             objClientDetail.ModulesAccess.Sector = lstobjModuleAccess.FirstOrDefault().Sector;
                             
             objClientDetail.ModulesAccess.SiteSetups = lstobjModuleAccess.FirstOrDefault().SiteSetups;
             objClientDetail.ModulesAccess.Store = lstobjModuleAccess.FirstOrDefault().Store;
             objClientDetail.ModulesAccess.StoreSetups = lstobjModuleAccess.FirstOrDefault().StoreSetups;
             objClientDetail.ModulesAccess.SubDept = lstobjModuleAccess.FirstOrDefault().SubDept;
             objClientDetail.ModulesAccess.Suppliers_Contractors = lstobjModuleAccess.FirstOrDefault().Suppliers_Contractors;
             objClientDetail.ModulesAccess.SLA = lstobjModuleAccess.FirstOrDefault().SLA;
             objClientDetail.ModulesAccess.Zone = lstobjModuleAccess.FirstOrDefault().Zone;
             objClientDetail.ModulesAccess.RequestCard = lstobjModuleAccess.FirstOrDefault().RequestCard;
             objClientDetail.ModulesAccess.Help = lstobjModuleAccess.FirstOrDefault().Help;
	        }
          if (objModuleLevelNames != null)
          {
            objClientDetail.LevelNames = new LevelNamesModel();
            objClientDetail.LevelNames.L1_ID = lstobjLevelNames.FirstOrDefault().L1_ID;
            objClientDetail.LevelNames.L2_ar = lstobjLevelNames.FirstOrDefault().L2_ar;
            objClientDetail.LevelNames.L2_en = lstobjLevelNames.FirstOrDefault().L2_en;
            objClientDetail.LevelNames.L3_ar = lstobjLevelNames.FirstOrDefault().L3_ar;
            objClientDetail.LevelNames.L3_en = lstobjLevelNames.FirstOrDefault().L3_en;
            objClientDetail.LevelNames.L4_ar = lstobjLevelNames.FirstOrDefault().L4_ar;
            objClientDetail.LevelNames.L4_en = lstobjLevelNames.FirstOrDefault().L4_en;
            objClientDetail.LevelNames.L5_ar = lstobjLevelNames.FirstOrDefault().L5_ar;
            objClientDetail.LevelNames.L5_en = lstobjLevelNames.FirstOrDefault().L5_en;
          }

          if (objFirstLevelTblModel != null)
          {
              objClientDetail.FirstLevelTbl = new FirstLevelTblModel();
              objClientDetail.FirstLevelTbl.L1_ID = lstFirstLevelTblModel.FirstOrDefault().L1_ID;
              objClientDetail.FirstLevelTbl.L1_ar = lstFirstLevelTblModel.FirstOrDefault().L1_ar;
              objClientDetail.FirstLevelTbl.L1_en = lstFirstLevelTblModel.FirstOrDefault().L1_en;
          }

          if (objCEUML_configModel != null)
          {
             objClientDetail.CEUML_config = new CEUML_configModel();
            objClientDetail.CEUML_config.ClientID = lstCEUML_config.FirstOrDefault().ClientID;
            objClientDetail.CEUML_config.CEUML_configId = lstCEUML_config.FirstOrDefault().CEUML_configId;
            objClientDetail.CEUML_config.CeUser = lstCEUML_config.FirstOrDefault().CeUser;
            objClientDetail.CEUML_config.Token = lstCEUML_config.FirstOrDefault().Token;
          }
         
          return objClientDetail;
      }
      public static Account CreateNewDBByClientID(int ClientID)
      {
          //CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
          Account objClientDetail = new Account();      
          Collection<DBParameters> parameters = new Collection<DBParameters>();
          parameters.Add(new DBParameters()
          {
              Name = "ClientID",
              Value = ClientID,
              DBType = DbType.Int32
          });
          string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] WHERE ClientID = @ClientID";

          using (DapperDBContext context = new DapperDBContext())
          {
              objClientDetail = context.ExecuteQuery<Account>(query, parameters).FirstOrDefault();
          }
          return objClientDetail;
      }
    }
}
