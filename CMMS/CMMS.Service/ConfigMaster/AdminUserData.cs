﻿using CMMS.DAL;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.ConfigMaster
{
    public class AdminUserData : DBExecute
    {
        public AdminUserData()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }

        public IList<Tbl_AdminUser> GetAdminUserList(string strWhere, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            string strWhereClause = string.Empty;
            IList<Tbl_AdminUser> list = new List<Tbl_AdminUser>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();



            string query = @"OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(nvarchar(50), DecryptByKey(FirsName)) AS 'FirstName',CONVERT(nvarchar(50), DecryptByKey(LastName)) AS 'LastName',CONVERT(nvarchar(50), DecryptByKey(UserName)) AS 'UserName',CONVERT(nvarchar(50), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser";
            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);
            if (!string.IsNullOrEmpty(strWhereClause))
            {
                if (!string.IsNullOrEmpty(text))
                {
                    query += " AND " + strWhereClause;
                }
                else
                {
                    query += " Where " + strWhereClause;
                }
            }
            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Tbl_AdminUser>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public static Tbl_AdminUser GetAdminUserEditID(int userID)
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            Tbl_AdminUser objAdminUser = new Tbl_AdminUser();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "userID",
                Value = userID,
                DBType = DbType.Int32
            });
            string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(nvarchar(50), DecryptByKey(FirsName)) AS 'FirstName',CONVERT(nvarchar(50), DecryptByKey(LastName)) AS 'LastName',CONVERT(nvarchar(50), DecryptByKey(UserName)) AS 'UserName',CONVERT(nvarchar(50), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where UserID = @UserID";
            //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where UserID = @UserID
            using (DapperDBContext context = new DapperDBContext())
            {
                objAdminUser = context.ExecuteQuery<Tbl_AdminUser>(query, parameters).FirstOrDefault();
            }
            return objAdminUser;
        }

        public static IList<Tbl_AdminUser> GetLoginAccount(string username, string password)
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            IList<Tbl_AdminUser> objAdminUser = new List<Tbl_AdminUser>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "username",
                Value = username,
                DBType = DbType.String
            });
            parameters.Add(new DBParameters()
            {
                Name = "password",
                Value = password,
                DBType = DbType.String
            });
            string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(nvarchar(50), DecryptByKey(UserName)) AS 'UserName',CONVERT(nvarchar(50), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName  and CONVERT(varchar(max), DecryptByKey([Password])) = @Password";
            //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName  and CONVERT(varchar(max), DecryptByKey([Password])) = @Password
            using (DapperDBContext context = new DapperDBContext())
            {
                objAdminUser = context.ExecuteQuery<Tbl_AdminUser>(query, parameters);
            }
            return objAdminUser;
        }


        public static IList<Tbl_AdminUser> CheckAccountExistOrNot(string username)
        {
            CMMS.Infrastructure.ProjectSession.UseMasterDB = true;
            IList<Tbl_AdminUser> objAdminUser = new List<Tbl_AdminUser>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            parameters.Add(new DBParameters()
            {
                Name = "username",
                Value = username,
                DBType = DbType.String
            });

            string query = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(nvarchar(50), DecryptByKey(UserName)) AS 'UserName' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName";
            //OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT UserID,CONVERT(varchar(max), DecryptByKey(FirstName)) AS 'FirstName',CONVERT(varchar(max), DecryptByKey(LastName)) AS 'LastName',CONVERT(varchar(max), DecryptByKey(UserName)) AS 'UserName',CONVERT(varchar(max), DecryptByKey([Password])) AS 'Password' FROM Tbl_AdminUser where CONVERT(varchar(max), DecryptByKey(UserName)) = @UserName 
            using (DapperDBContext context = new DapperDBContext())
            {
                objAdminUser = context.ExecuteQuery<Tbl_AdminUser>(query, parameters);
            }
            return objAdminUser;
        }
    }
}
