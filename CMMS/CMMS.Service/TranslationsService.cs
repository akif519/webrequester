﻿using CMMS.Model;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Collections.ObjectModel;
using System.Data;
using System.Reflection;
using System.Web.Mvc;

namespace CMMS.Service
{
    public class TranslationsService : DBExecute
    {
        public TranslationsService()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }

        public Resources GetResourceValues(string LanguageCode, string DefaultLanguage)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "LanguageCode",
                Value = LanguageCode,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "DefaultLanguage",
                Value = DefaultLanguage,
                DBType = DbType.String
            });

            Resources objResource = new Resources();
            string strQuery;
            using (DapperContext objDapperContext = new DapperContext())
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = "SELECT DISTINCT T.Type ,";
                    strQuery += " (REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') || '_' || T.labelname ) AS ResourceKey ,";
                    strQuery += " NVL(T1.ToWord, T.FromWord) AS ResourceValue ";
                    strQuery += " FROM  translations T ";
                    strQuery += " LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName ";
                    strQuery += " AND T1.ToLanguageCode = @LanguageCode ";
                    strQuery += " WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'L' ";
                }
                else
                {
                    strQuery = @"SELECT DISTINCT T.Type ,
                                CONCAT(REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') , '_' , T.labelname ) AS ResourceKey ,
                                ISNULL(T1.ToWord, T.FromWord) AS ResourceValue
                                FROM  translations T
                                LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName 
                                          AND T1.ToLanguageCode = @LanguageCode
                                WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'L'";
                }
                List<Resources> lst = objDapperContext.ExecuteQuery<Resources>(strQuery, parameters).ToList();


                foreach (Resources lstResources in lst)
                {
                    string strMessage = string.Empty;
                    PropertyInfo info = objResource.label.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstResources.ResourceKey.ToString().ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        strMessage = lstResources.ResourceValue;
                        if (ProjectSession.FirstLevelTbl != null)
                        {
                            strMessage = strMessage.Replace("[L1_en]", ProjectSession.FirstLevelTbl.L1_en);
                            strMessage = strMessage.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        }

                        if (ProjectSession.LevelNamesTbl != null)
                        {
                            strMessage = strMessage.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en).Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en).Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en).Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
                            strMessage = strMessage.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar).Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar).Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar).Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        }

                        //strMessage = strMessage.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        info.SetValue(objResource.label, strMessage);

                        //strMessage = lstResources.ResourceValue.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en);
                        //strMessage = strMessage.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar);
                        //strMessage = strMessage.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar);
                        //info.SetValue(objResource.label, strMessage);

                        //strMessage = lstResources.ResourceValue.Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en);
                        //strMessage = strMessage.Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar);
                        //strMessage = strMessage.Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar);
                        //info.SetValue(objResource.label, strMessage);

                        //strMessage = lstResources.ResourceValue.Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en);
                        //strMessage = strMessage.Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar);
                        //strMessage = strMessage.Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar);
                        //info.SetValue(objResource.label, strMessage);

                        //strMessage = lstResources.ResourceValue.Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
                        //strMessage = strMessage.Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        //strMessage = strMessage.Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        //info.SetValue(objResource.label, strMessage);
                    }
                }
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = "SELECT DISTINCT T.Type ,";
                    strQuery += " (REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') || '_' || T.labelname ) AS ResourceKey ,";
                    strQuery += " NVL(T1.ToWord, T.FromWord) AS ResourceValue ";
                    strQuery += " FROM  translations T ";
                    strQuery += " LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName ";
                    strQuery += " AND T1.ToLanguageCode = @LanguageCode ";
                    strQuery += " WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'M' ";
                }
                else
                {
                    strQuery = @"SELECT DISTINCT T.Type ,
                                CONCAT(REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') , '_' , T.labelname) AS ResourceKey ,
                                ISNULL(T1.ToWord, T.FromWord) AS ResourceValue
                                FROM  translations T
                                LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName 
                                          AND T1.ToLanguageCode = @LanguageCode
                                WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'M'";
                }
                List<Resources> lst = objDapperContext.ExecuteQuery<Resources>(strQuery, parameters).ToList();
                foreach (Resources lstResources in lst)
                {
                    string strMessageM = string.Empty;
                    PropertyInfo info = objResource.message.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstResources.ResourceKey.ToString().ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        //info.SetValue(objResource.message, lstResources.ResourceValue);

                        strMessageM = lstResources.ResourceValue;
                        
                        //strMessage = strMessage.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);

                        if (ProjectSession.FirstLevelTbl != null)
                        {
                            strMessageM = strMessageM.Replace("[L1_en]", ProjectSession.FirstLevelTbl.L1_en);
                            strMessageM = strMessageM.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        }

                        if (ProjectSession.LevelNamesTbl != null)
                        {
                            strMessageM = strMessageM.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en).Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en).Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en).Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
                            strMessageM = strMessageM.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar).Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar).Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar).Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        }

                        if (!string.IsNullOrWhiteSpace(ProjectSession.AllowedFileTypes))
                        {
                            strMessageM = strMessageM.Replace("[AllowedFileTypes]", ProjectSession.AllowedFileTypes);
                        }

                        info.SetValue(objResource.message, strMessageM);

                    }
                }
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = "SELECT DISTINCT T.Type ,";
                    strQuery += " (REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') || '_' || T.labelname ) AS ResourceKey ,";
                    strQuery += " NVL(T1.ToWord, T.FromWord) AS ResourceValue ";
                    strQuery += " FROM  translations T ";
                    strQuery += " LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName ";
                    strQuery += " AND T1.ToLanguageCode = @LanguageCode ";
                    strQuery += " WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'ME' ";
                }
                else
                {
                    strQuery = @"SELECT DISTINCT T.Type ,
                                CONCAT(REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') , '_' , T.labelname) AS ResourceKey ,
                                ISNULL(T1.ToWord, T.FromWord) AS ResourceValue
                                FROM  translations T
                                LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName 
                                          AND T1.ToLanguageCode = @LanguageCode
                                WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'ME'";
                }
                List<Resources> lst = objDapperContext.ExecuteQuery<Resources>(strQuery, parameters).ToList();
                foreach (Resources lstResources in lst)
                {
                    string strMessageMenu = string.Empty;
                    PropertyInfo info = objResource.menu.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstResources.ResourceKey.ToString().ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        //info.SetValue(objResource.menu, lstResources.ResourceValue);
                        strMessageMenu = lstResources.ResourceValue;

                        if (ProjectSession.FirstLevelTbl != null)
                        {
                            strMessageMenu = strMessageMenu.Replace("[L1_en]", ProjectSession.FirstLevelTbl.L1_en);
                            strMessageMenu = strMessageMenu.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        }

                        if (ProjectSession.FirstLevelTbl != null)
                        {
                            strMessageMenu = strMessageMenu.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en).Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en).Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en).Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
                            strMessageMenu = strMessageMenu.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar).Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar).Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar).Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        }

                        //strMessage = strMessage.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        info.SetValue(objResource.menu, strMessageMenu);
                    }
                }
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = "SELECT DISTINCT T.Type ,";
                    strQuery += " (REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') || '_' || T.labelname ) AS ResourceKey ,";
                    strQuery += " NVL(T1.ToWord, T.FromWord) AS ResourceValue ";
                    strQuery += " FROM  translations T ";
                    strQuery += " LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName ";
                    strQuery += " AND T1.ToLanguageCode = @LanguageCode ";
                    strQuery += " WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'DA' ";
                }
                else
                {
                    strQuery = @"SELECT DISTINCT T.Type ,
                                CONCAT(REPLACE(REPLACE(RTRIM(LTRIM(T.ModuleName)), ' ', ''), '/', '_') , '_' , T.labelname) AS ResourceKey ,
                                ISNULL(T1.ToWord, T.FromWord) AS ResourceValue
                                FROM  translations T
                                LEFT JOIN translations T1 ON T1.labelname = T.labelname AND T.Type = T1.Type AND T.ModuleName = T1.ModuleName 
                                          AND T1.ToLanguageCode = @LanguageCode
                                WHERE T.FromLanguageCode = @DefaultLanguage AND T.Type = 'DA'";
                }
                List<Resources> lst = objDapperContext.ExecuteQuery<Resources>(strQuery, parameters).ToList();
                foreach (Resources lstResources in lst)
                {
                    string strMessageAudit = string.Empty;
                    PropertyInfo info = objResource.audit.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstResources.ResourceKey.ToString().ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        //info.SetValue(objResource.audit, lstResources.ResourceValue);
                        strMessageAudit = lstResources.ResourceValue;
                        
                        //strMessage = strMessage.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);

                        if (ProjectSession.FirstLevelTbl != null)
                        {
                            strMessageAudit = strMessageAudit.Replace("[L1_en]", ProjectSession.FirstLevelTbl.L1_en);
                            strMessageAudit = strMessageAudit.Replace("[L1_ar]", ProjectSession.FirstLevelTbl.L1_ar);
                        }

                        if (ProjectSession.LevelNamesTbl != null)
                        {
                            strMessageAudit = strMessageAudit.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en).Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en).Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en).Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
                            strMessageAudit = strMessageAudit.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar).Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar).Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar).Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
                        }

                        info.SetValue(objResource.audit, strMessageAudit);
                    }
                }
            }

            //DataSet ds = (DataSet)ExecuteProcedure("UspTranslation_ResourceValue", ExecuteType.ExecuteDataSet, parameters);

            //foreach (DataRow dr in ds.Tables[0].Rows)
            //{
            //    PropertyInfo info = objResource.label.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr["ResourceKey"].ToString().ToLower());
            //    if (info != null)
            //    {
            //        /*Set the Value to Model*/
            //        info.SetValue(objResource.label, dr["ResourceValue"]);
            //    }
            //}

            //foreach (DataRow dr in ds.Tables[1].Rows)
            //{
            //    PropertyInfo info = objResource.message.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr["ResourceKey"].ToString().ToLower());
            //    if (info != null)
            //    {
            //        /*Set the Value to Model*/
            //        info.SetValue(objResource.message, dr["ResourceValue"]);
            //    }
            //}

            //foreach (DataRow dr in ds.Tables[2].Rows)
            //{
            //    PropertyInfo info = objResource.menu.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr["ResourceKey"].ToString().ToLower());
            //    if (info != null)
            //    {
            //        /*Set the Value to Model*/
            //        info.SetValue(objResource.menu, dr["ResourceValue"]);
            //    }
            //}

            return objResource;
        }

        #region "tbl_LanguageSelection"
        public static List<SelectListItem> LanguageForDropDown()
        {
            using (ServiceContext context = new ServiceContext())
            {
                Tbl_LanguageSelection objlanguage = new Tbl_LanguageSelection();

                List<SelectListItem> obj = TranslationsService.GetLanguages().Select(x =>
                    new SelectListItem()
                    {
                        Text = x.Language,
                        Value = x.LanguageCode,
                        //Selected = true
                    }
                    ).OrderBy(x => x.Text).ToList();
                return obj;
            }
        }

        public static List<Tbl_LanguageSelection> GetLanguages()
        {
            using (DapperContext context = new DapperContext())
            {
                Tbl_LanguageSelection objlanguage = new Tbl_LanguageSelection();
                List<Tbl_LanguageSelection> lstLanguage = new List<Tbl_LanguageSelection>();
                if (HttpRuntime.Cache["Language" + ProjectSession.AccountID] == null)
                {
                    lstLanguage = context.Search(objlanguage, null, null, null).ToList();
                    HttpRuntime.Cache["Language" + ProjectSession.AccountID] = lstLanguage;
                }
                else
                {
                    lstLanguage = (List<Tbl_LanguageSelection>)HttpRuntime.Cache["Language" + ProjectSession.AccountID];
                }
                return lstLanguage;
            }
        }
        #endregion
    }
}
