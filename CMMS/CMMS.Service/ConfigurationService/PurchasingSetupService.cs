﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;

namespace CMMS.Service.ConfigurationService
{
    public class PurchasingSetupService : DBExecute
    {
        public PurchasingSetupService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Updates the default setup.
        /// </summary>
        /// <param name="lstDefaultSetup">The LST default setup.</param>
        /// <returns></returns>
        public bool UpdateDefaultSetup(List<DefaultSetup> lstDefaultSetup)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                foreach (var lst in lstDefaultSetup)
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    string strQuery = @"UPDATE DefaultSetups SET VALUE = @Value Where SettingName = @SettingName ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "Value",
                        Value = lst.Value,
                        DBType = DbType.Decimal
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "SettingName",
                        Value = lst.SettingName,
                        DBType = DbType.String
                    });

                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }
            }
            return true;
        }
    }
}
