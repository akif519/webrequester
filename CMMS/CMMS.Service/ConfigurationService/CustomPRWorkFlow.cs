﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;

namespace CMMS.Service.ConfigurationService
{
    public class CustomPRWorkFlow : DBExecute
    {
        public CustomPRWorkFlow()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public bool InsertPRApprovalLevelMappingsEmployeeBulk(int autoId, string assignedEmployee)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = autoId,
                    DBType = DbType.Int32
                });

                // DELETE ALL EXISTING Employees Mapping
                string strQuery = @"
                DELETE FROM PRApprovalLevelMappings_employees WHERE AutoId = @AutoId";
                objDapperContext.ExecuteQuery(strQuery, parameters);


                string[] values = assignedEmployee.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    strQuery = @"INSERT INTO PRApprovalLevelMappings_employees
                    (AutoId ,[EmployeeID]) VALUES(@AutoId,@AssignedEmployee)";

                    parameters.Add(new DBParameters()
                    {
                        Name = "AssignedEmployee",
                        Value = values[i].Trim(),
                        DBType = DbType.String
                    });

                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }

                //objDapperContext.ex
            }
            return true;
        }

        public virtual List<employees> GetPRApprovalLevelMappingsEmployeesInfoSearch(int? autoId, int pageNo, string sortExpression, string sortDirection)
        {
            string strEmployeeIDs = string.Empty;
            string query = string.Empty;
            List<employees> employeeInfo;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = autoId,
                    DBType = DbType.Int32
                });

                query = @"Select m.EmployeeID,m.EmployeeNo,m.Name,m.Positions from Employees m 
                            INNER JOIN PRApprovalLevelMappings_employees IR ON IR.EmployeeID = m.EmployeeID  WHERE AutoID = @AutoId";

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    /// This Name will Work For this case too. This is no Advance Search but it's normal search
                    employeeInfo = new List<employees>(objDapperContext.AdvanceSearch<employees>(query, parameters, pageNo));
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
                return employeeInfo;
            }

            finally
            {

            }
        }

        public virtual int DeletePRWorkFlowMapping(int workFlowId, int l2ID, int prApprovalLevelId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = l2ID,
                        DBType = DbType.Int32
                    });

                    var objPRApprovalLevels = objDapperContext.SelectObject<PRApprovalLevel>(prApprovalLevelId);

                    if (objPRApprovalLevels.IsFinalLevel)
                    {
                        return -3;
                    }

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM PRApprovalLevelMappings plm
                            INNER JOIN PRApprovalLevels pl on plm.PRApprovalLevelID = pl.PRApprovalLevelID
                            where L2ID = @l2ID and pl.IsFinalLevel != 1";
                    IList<PRApprovalLevelMapping> lstPRApprovalLevelMapping = new List<PRApprovalLevelMapping>();
                    lstPRApprovalLevelMapping = objDapperContext.ExecuteQuery<PRApprovalLevelMapping>(strQuery, parameters).ToList();

                    if (lstPRApprovalLevelMapping.Count > 0)
                    {
                        int maxcount = lstPRApprovalLevelMapping.Max(c => c.LevelNo);

                        PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
                        objPRApprovalLevel = objDapperContext.SelectObject<PRApprovalLevel>(prApprovalLevelId);
                        if (objPRApprovalLevel.LevelNo != maxcount)
                        {
                            return -3;
                        }
                    }

                    //First Need TO Delete from Employee Table and Then From Mapping Table
                    string strQuery1 = @"DELETE FROM PRApprovalLevelMappings_employees WHERE AutoId = " + workFlowId;
                    objDapperContext.ExecuteQuery(strQuery1, parameters);

                    int returnValue = objDapperContext.Delete<PRApprovalLevelMapping>(workFlowId, true);
                    return returnValue;
                }
            }
            finally
            {

            }
        }

        public virtual int SavePRWorkFlowMapping(int l2ID, int prApprovalLevelId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = l2ID,
                        DBType = DbType.Int32
                    });

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM PRApprovalLevelMappings plm
                            INNER JOIN PRApprovalLevels pl on plm.PRApprovalLevelID = pl.PRApprovalLevelID
                            where L2ID = @l2ID and pl.IsFinalLevel != 1";
                    IList<PRApprovalLevelMapping> lstPRApprovalLevelMapping = new List<PRApprovalLevelMapping>();
                    lstPRApprovalLevelMapping = objDapperContext.ExecuteQuery<PRApprovalLevelMapping>(strQuery, parameters).ToList();

                    PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
                    objPRApprovalLevel = objDapperContext.SelectObject<PRApprovalLevel>(prApprovalLevelId);

                    if (lstPRApprovalLevelMapping.Count > 0)
                    {
                        int maxcount = lstPRApprovalLevelMapping.Max(c => c.LevelNo);
                        if (objPRApprovalLevel.LevelNo == maxcount + 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return -3;
                        }
                    }
                    else if (objPRApprovalLevel.LevelNo == 1)
                    {
                        return 1;
                    }
                    return -3;
                }
            }
            finally
            {

            }
        }

        public virtual int NewPRApprovalLevelMappingInsertFinalLevel(int l2ID)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = l2ID,
                        DBType = DbType.Int32
                    });

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM PRApprovalLevelMappings plm
                            INNER JOIN PRApprovalLevels pl on plm.PRApprovalLevelID = pl.PRApprovalLevelID
                            where L2ID = @l2ID and pl.IsFinalLevel = 1";
                    IList<PRApprovalLevelMapping> lstPRApprovalLevelMapping = new List<PRApprovalLevelMapping>();
                    lstPRApprovalLevelMapping = objDapperContext.ExecuteQuery<PRApprovalLevelMapping>(strQuery, parameters).ToList();

                    if (lstPRApprovalLevelMapping.Count == 0)
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            strQuery = @"INSERT INTO PRApprovalLevelMappings(L2ID,PRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + l2ID + " ,(SELECT TOP 1 PRApprovalLevelId FROM PRApprovalLevels WHERE IsFinalLevel =1), 1,(SELECT TOP 1 pas.auth_status_id FROM pr_authorisation_status pas WHERE pas.auth_status_desc='Approved'),1) ";

                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            strQuery = @"INSERT INTO PRApprovalLevelMappings(L2ID,PRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + l2ID + " ,(SELECT PRApprovalLevelId FROM PRApprovalLevels WHERE IsFinalLevel =1 LIMIT 1), 1,(SELECT pas.auth_status_id FROM pr_authorisation_status pas WHERE pas.auth_status_desc='Approved' LIMIT 1),1) ";

                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            strQuery = @"INSERT INTO PRApprovalLevelMappings(L2ID,PRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + l2ID + " ,(SELECT PRApprovalLevelId FROM PRApprovalLevels WHERE IsFinalLevel =1 AND ROWNUM = 1), 1,(SELECT pas.auth_status_id FROM pr_authorisation_status pas WHERE pas.auth_status_desc='Approved'  AND ROWNUM = 1),1) ";
                        }
                        objDapperContext.ExecuteQuery(strQuery, parameters);
                    }
                    return -1;
                }
            }
            finally
            {

            }
        }

        public virtual int CheckForDuplicatesPRReq(int cityID, int pRApprovalLevelId, int autoId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "cityID",
                        Value = cityID,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "PRApprovalLevelId",
                        Value = pRApprovalLevelId,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "AutoId",
                        Value = autoId,
                        DBType = DbType.Int32
                    });

                    strQuery = @"SELECT * FROM PRApprovalLevelMappings
                                Where L2ID = @cityID And pRApprovalLevelId = @pRApprovalLevelId And AutoId <> @AutoId";

                    IList<PRApprovalLevelMapping> lstPRApprovalLevelMapping = new List<PRApprovalLevelMapping>();
                    lstPRApprovalLevelMapping = objDapperContext.ExecuteQuery<PRApprovalLevelMapping>(strQuery, parameters).ToList();

                    if (lstPRApprovalLevelMapping.Count > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            finally
            {

            }
        }
    }
}
