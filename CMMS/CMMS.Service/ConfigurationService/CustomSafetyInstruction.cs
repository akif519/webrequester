﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Service.ConfigurationService
{
    public class CustomSafetyInstruction : DBExecute
    {
        public CustomSafetyInstruction()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }
        
        /// <summary>
        /// Inserts the update safety instruction item.
        /// </summary>
        /// <param name="objTblSafetyInstructionItem">The object table safety instruction item.</param>
        /// <returns></returns>
        public bool InsertUpdateSafetyInstructionItem(TblSafetyInstructionItem objTblSafetyInstructionItem)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "SafetyID",
                    Value = objTblSafetyInstructionItem.SafetyID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqID",
                    Value = objTblSafetyInstructionItem.SeqID,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "Description",
                    Value = objTblSafetyInstructionItem.Description,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "AltDescription",
                    Value = objTblSafetyInstructionItem.AltDescription,
                    DBType = DbType.String
                });

                if (objTblSafetyInstructionItem.HdnSeqID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objTblSafetyInstructionItem.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objTblSafetyInstructionItem.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"UPDATE tblSafetyInstructionItems SET Description = @Description, AltDescription = @AltDescription , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  where SafetyID = @SafetyID and SeqID = " + objTblSafetyInstructionItem.HdnSeqID;
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = objTblSafetyInstructionItem.CreatedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = objTblSafetyInstructionItem.CreatedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"INSERT INTO tblSafetyInstructionItems(SafetyID,SeqID,Description,AltDescription,CreatedBy,CreatedDate) VALUES(@SafetyID,@SeqID,@Description,@AltDescription,@CreatedBy,@CreatedDate)";
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Deletes the safety instruction item.
        /// </summary>
        /// <param name="safetyId">The safety identifier.</param>
        /// <param name="seqId">The seq identifier.</param>
        /// <returns></returns>
        public bool DeleteSafetyInstructionItem(int safetyId, int seqId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "SafetyID",
                    Value = safetyId,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqID",
                    Value = seqId,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From tblSafetyInstructionItems where SafetyID = @SafetyID and SeqID = @SeqID";

                objDapperContext.ExecuteQuery(strQuery, parameters);
                UpdateSeqID(safetyId);
            }
            return true;
        }

        /// <summary>
        /// Updates the seq identifier.
        /// </summary>
        /// <param name="safetyId">The safety identifier.</param>
        /// <returns></returns>
        public bool UpdateSeqID(int safetyId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    strQuery = @";WITH cte AS(SELECT *, ROW_NUMBER() OVER(ORDER BY SeqID) Rno FROM dbo.tblSafetyInstructionItems WHERE SafetyID = " + safetyId + ")" +
                                "UPDATE cte SET SeqID = cte.Rno";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    strQuery = @"UPDATE tblSafetyInstructionItems as c inner JOIN(SELECT *,  @rownum := @rownum + 1 AS rank FROM tblSafetyInstructionItems,(SELECT @rownum := 0) r WHERE SafetyID = " + safetyId + ") as cte " +
                               "on cte.SafetyID = c.SafetyID and cte.SeqID = c.SeqID SET c.SeqID = cte.rank";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = @"UPDATE tblSafetyInstructionItems c SET(c.SEQID) = (SELECT ROW_NUMBER() OVER ( ORDER BY SeqID) rno FROM   tblSafetyInstructionItems cte WHERE  cte.SafetyID = c.SafetyID and cte.SeqID = c.SeqID) " +
                                "Where c.SafetyID = " + safetyId;
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Gets the safety instruction item by safety identifier.
        /// </summary>
        /// <param name="safetyId">The safety identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<TblSafetyInstructionItem> GetSafetyInstructionItemBySafetyID(int safetyId, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<TblSafetyInstructionItem> list = new List<TblSafetyInstructionItem>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "SELECT SafetyID, SeqID, Description, AltDescription " +
                           "FROM tblSafetyInstructionItems " +
                           "WHERE SafetyID = " + safetyId;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<TblSafetyInstructionItem>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public bool SaveSafetyInstrItem(int SafetyID, List<TblSafetyInstructionItem> lstJobPlanItems)
        {
            try
            {
                string queryDelete = "Delete from tblSafetyInstructionItems Where SafetyID = '" + SafetyID + "'";
                string queryInsert = string.Empty;

                Jobtask objJobTask = new Jobtask();

               

                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(queryDelete, new Collection<DBParameters>());

                    lstJobPlanItems.ForEach(p =>
                    {
                        Collection<DBParameters> parameters = new Collection<DBParameters>();

                        parameters.Add(new DBParameters()
                        {
                            Name = "CreatedDate",
                            Value = p.CreatedDate,
                            DBType = DbType.DateTime
                        });

                        parameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = p.ModifiedDate,
                            DBType = DbType.DateTime
                        });

                        queryInsert = "Insert Into tblSafetyInstructionItems(SafetyID,SeqID,Description,AltDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)"
                                + " Values('" + SafetyID + "'," + p.SeqID + ",'" + p.Description + "',N'" + p.AltDescription + "','" + p.CreatedBy + "',@CreatedDate,'" + p.ModifiedBy + "',@ModifiedDate )";
                        objContext.ExecuteQuery(queryInsert, parameters);
                    });
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
