﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;


namespace CMMS.Service.ConfigurationService
{
    public class CustomIRWorkFlow : DBExecute
    {
        public CustomIRWorkFlow()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        //public List<employees> GetIRApprovalLevelMappingsEmployeesInfoSearch(int? autoId, int pageNo, string sortExpression, string sortDirection)
        //{
        //    Collection<DBParameters> parameters = new Collection<DBParameters>();

        //    parameters.Add(new DBParameters()
        //    {
        //        Name = "AutoId",
        //        Value = autoId,
        //        DBType = DbType.Int32
        //    });

        //    if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
        //    {
        //        parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
        //        parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
        //    }

        //    parameters.Add(new DBParameters()
        //    {
        //        Name = "SortExpression",
        //        DBType = DbType.String,
        //        Value = sortExpression

        //    });
        //    parameters.Add(new DBParameters()
        //    {
        //        Name = "SortDirection",
        //        DBType = DbType.String,
        //        Value = sortDirection

        //    });
        //    List<employees> employeeInfo = this.ExecuteProcedure<employees>("UspIRApprovalLevelMappingsemployeesInfoSearch", parameters).ToList();
        //    return employeeInfo;
        //}

        public virtual List<employees> GetIRApprovalLevelMappingsEmployeesInfoSearch(int? autoId, int pageNo, string sortExpression, string sortDirection)
        {
            string strEmployeeIDs = string.Empty;
            string query = string.Empty;
            List<employees> employeeInfo;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                
                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = autoId,
                    DBType = DbType.Int32
                });

                query = @"Select m.EmployeeID,m.EmployeeNo,m.Name,m.Positions from Employees m 
                            INNER JOIN IRApprovalLevelMappings_employees IR ON IR.EmployeeID = m.EmployeeID  WHERE AutoID = @AutoId";

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    /// This Name will Work For this case too. This is no Advance Search but it's normal search
                    employeeInfo =  new List<employees>(objDapperContext.AdvanceSearch<employees>(query, parameters, pageNo));
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
                return employeeInfo;
            }

            finally
            {

            }
        }

        /// <summary>
        /// Checks the name of the level.
        /// </summary>
        /// <param name="objIRApprovalLevel">The object ir approval level.</param>
        /// <returns></returns>
        public bool CheckLevelName(IRApprovalLevel objIRApprovalLevel)
        {
            IRApprovalLevel obj = new IRApprovalLevel();

            if (objIRApprovalLevel.IRApprovalLevelId > 0)
            {
                using (DapperContext objContext = new DapperContext())
                {
                    obj = objContext.SearchAll(obj).Where(o => o.LevelName == objIRApprovalLevel.LevelName && o.IRApprovalLevelId != objIRApprovalLevel.IRApprovalLevelId).FirstOrDefault();
                }
            }
            else
            {
                using (DapperContext objContext = new DapperContext())
                {
                    obj = objContext.SearchAll(obj).Where(o => o.LevelName == objIRApprovalLevel.LevelName).FirstOrDefault();
                }
            }

            if (obj != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual int DeleteIRWorkFlowMapping(int workFlowId, int subStoreId, int prApprovalLevelId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreId",
                        Value = subStoreId,
                        DBType = DbType.Int32
                    });

                    var objIRApprovalLevels = objDapperContext.SelectObject<IRApprovalLevel>(prApprovalLevelId);

                    if (objIRApprovalLevels.IsFinalLevel)
                    {
                        return -3;
                    }

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM IRApprovalLevelMappings plm
                            INNER JOIN IRApprovalLevels pl on plm.IRApprovalLevelID = pl.IRApprovalLevelID
                            where SubStoreId = @SubStoreId and pl.IsFinalLevel != 1";
                    IList<IRApprovalLevelMapping> lstIRApprovalLevelMapping = new List<IRApprovalLevelMapping>();
                    lstIRApprovalLevelMapping = objDapperContext.ExecuteQuery<IRApprovalLevelMapping>(strQuery, parameters).ToList();
                    if (lstIRApprovalLevelMapping.Count > 0)
                    {
                        int maxcount = lstIRApprovalLevelMapping.Max(c => c.LevelNo);

                        IRApprovalLevel objPRApprovalLevel = new IRApprovalLevel();
                        objPRApprovalLevel = objDapperContext.SelectObject<IRApprovalLevel>(prApprovalLevelId);
                        if (objPRApprovalLevel.LevelNo != maxcount)
                        {
                            return -4;
                        }
                    }

                    //First Need TO Delete from Employee Table and Then From Mapping Table
                    string strQuery1 = @"DELETE FROM IRApprovalLevelMappings_employees WHERE AutoId = " + workFlowId ;
                    objDapperContext.ExecuteQuery(strQuery1, parameters);

                    int returnValue = objDapperContext.Delete<IRApprovalLevelMapping>(workFlowId, true);
                    return returnValue;
                }
            }
            finally
            {

            }
        }

        public virtual int SaveIRWorkFlowMapping(int subStoreId, int irApprovalLevelId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreId",
                        Value = subStoreId,
                        DBType = DbType.Int32
                    });

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM IRApprovalLevelMappings plm
                            INNER JOIN IRApprovalLevels pl on plm.IRApprovalLevelID = pl.IRApprovalLevelID
                            where SubStoreId = @SubStoreId and pl.IsFinalLevel != 1";
                    IList<IRApprovalLevelMapping> lstIRApprovalLevelMapping = new List<IRApprovalLevelMapping>();
                    lstIRApprovalLevelMapping = objDapperContext.ExecuteQuery<IRApprovalLevelMapping>(strQuery, parameters).ToList();

                    IRApprovalLevel objIRApprovalLevel = new IRApprovalLevel();
                    objIRApprovalLevel = objDapperContext.SelectObject<IRApprovalLevel>(irApprovalLevelId);

                    if (lstIRApprovalLevelMapping.Count > 0)
                    {
                        int maxcount = lstIRApprovalLevelMapping.Max(c => c.LevelNo);
                        if (objIRApprovalLevel.LevelNo == maxcount + 1)
                        {
                            return 1;
                        }
                        else
                        {
                            return -3;
                        }
                    }
                    else if (objIRApprovalLevel.LevelNo == 1)
                    {
                        return 1;
                    }
                    return -3;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Inserts the update employee ir approval level.
        /// </summary>
        /// <param name="objEmployeesIRApprovalLevel">The object employees ir approval level.</param>
        /// <returns></returns>
        public bool InsertUpdateEmployeeIRApprovalLevel(IRApprovalLevelMappings_employee objEmployeesIRApprovalLevel)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = objEmployeesIRApprovalLevel.AutoId,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = objEmployeesIRApprovalLevel.EmployeeId,
                    DBType = DbType.Int32
                });

                strQuery = @"INSERT INTO IRApprovalLevelMappings_employees(AutoId,EmployeeID) VALUES(@AutoId,@EmployeeId)";

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        public bool InsertIRApprovalLevelMappingsEmployeeBulk(int autoId, string assignedEmployee)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = autoId,
                    DBType = DbType.Int32
                });

                // DELETE ALL EXISTING Employees Mapping
                string strQuery = @"
                DELETE FROM IRApprovalLevelMappings_employees WHERE AutoId = @AutoId";
                objDapperContext.ExecuteQuery(strQuery, parameters);


                string[] values = assignedEmployee.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    strQuery = @"INSERT INTO IRApprovalLevelMappings_employees
                    (AutoId ,[EmployeeID]) VALUES(@AutoId,@AssignedEmployee)";

                    parameters.Add(new DBParameters()
                    {
                        Name = "AssignedEmployee",
                        Value = values[i].Trim(),
                        DBType = DbType.String
                    });

                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }

                //objDapperContext.ex
            }
            return true;
        }

        public virtual int NewIRApprovalLevelMappingInsertFinalLevel(int substoreid)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "substoreid",
                        Value = substoreid,
                        DBType = DbType.Int32
                    });

                    strQuery = @"Select AutoId,LevelNo,LevelName FROM IRApprovalLevelMappings plm
                            INNER JOIN IRApprovalLevels pl on plm.IRApprovalLevelID = pl.IRApprovalLevelID
                            where substoreid = @substoreid and pl.IsFinalLevel = 1";
                    IList<IRApprovalLevelMapping> lstPRApprovalLevelMapping = new List<IRApprovalLevelMapping>();
                    lstPRApprovalLevelMapping = objDapperContext.ExecuteQuery<IRApprovalLevelMapping>(strQuery, parameters).ToList();

                    if (lstPRApprovalLevelMapping.Count == 0)
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            strQuery = @"INSERT INTO IRApprovalLevelMappings(SubStoreId,IRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + substoreid + " ,(SELECT TOP 1 IRApprovalLevelId FROM IRApprovalLevels WHERE IsFinalLevel =1), 1,(SELECT TOP 1 pas.auth_status_id FROM mr_authorisation_status pas WHERE pas.auth_status_desc='Approved'),1) ";

                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            strQuery = @"INSERT INTO IRApprovalLevelMappings(SubStoreId,IRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + substoreid + " ,(SELECT IRApprovalLevelId FROM IRApprovalLevels WHERE IsFinalLevel =1 LIMIT 1), 1,(SELECT pas.auth_status_id FROM mr_authorisation_status pas WHERE pas.auth_status_desc='Approved' LIMIT 1),1) ";

                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            strQuery = @"INSERT INTO IRApprovalLevelMappings(SubStoreId,IRApprovalLevelId,Mandatory,auth_status_id,IsPrintingEnable) 
                                 Values ( " + substoreid + " ,(SELECT IRApprovalLevelId FROM IRApprovalLevels WHERE IsFinalLevel =1 AND ROWNUM = 1), 1,(SELECT pas.auth_status_id FROM mr_authorisation_status pas WHERE pas.auth_status_desc='Approved'  AND ROWNUM = 1),1) ";
                        }
                        objDapperContext.ExecuteQuery(strQuery, parameters);
                    }
                    return -1;
                }
            }
            finally
            {

            }
        }

        /// <summary>
        /// Deletes the employee ir approval level.
        /// </summary>
        /// <param name="autoId">The automatic identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public bool DeleteEmployeeIRApprovalLevel(int autoId, int employeeId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "AutoId",
                    Value = autoId,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = employeeId,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From IRApprovalLevelMappings_employees where AutoId = @AutoId and EmployeeId = @EmployeeId";

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        public virtual int CheckForDuplicatesIRReq(int subStoreId, int iRApprovalLevelId, int autoId)
        {
            string strQuery = string.Empty;
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "SubStoreId",
                        Value = subStoreId,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "IRApprovalLevelId",
                        Value = iRApprovalLevelId,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "AutoId",
                        Value = autoId,
                        DBType = DbType.Int32
                    });

                    strQuery = @"SELECT * FROM IRApprovalLevelMappings
                                Where SubStoreId = @SubStoreId And IRApprovalLevelId = @IRApprovalLevelId And AutoId <> @AutoId";

                    IList<IRApprovalLevelMapping> lstIRApprovalLevelMapping = new List<IRApprovalLevelMapping>();
                    lstIRApprovalLevelMapping = objDapperContext.ExecuteQuery<IRApprovalLevelMapping>(strQuery, parameters).ToList();

                    if (lstIRApprovalLevelMapping.Count > 0)
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            finally
            {

            }
        }
    }
}
