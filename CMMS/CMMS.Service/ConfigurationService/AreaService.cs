﻿using CMMS.DAL;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Infrastructure;

namespace CMMS.Service
{
    public class AreaService : DBExecute
    {
        public AreaService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<L3> GetAreaDetailsByL2ID(int L2ID, int pageNo, int orgID = 0)
        {
            try
            {
                IList<L3> list = new List<L3>();
                string query = string.Empty;
                string L2Filter = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (L2ID > 0)
                {
                    L2Filter = " AND L3.L2ID =@L2ID ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = L2ID,
                        DBType = DbType.Int32
                    });
                }

                if (orgID > 0)
                {
                    L2Filter = L2Filter + " and L2.L1ID = @L1ID ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L1ID",
                        Value = orgID,
                        DBType = DbType.Int32
                    });
                }

                if (ProjectSession.IsCentral)
                {
                    query = "SELECT L3.*,L2.L2Code,L2.L2ID From L3 INNER JOIN L2  ON  L3.L2ID = L2.L2ID " + L2Filter;
                }
                else
                {
                    query = "SELECT L3.*,L2.L2Code,L2.L2ID From L3 INNER JOIN L2  ON  L3.L2ID = L2.L2ID " + L2Filter + " Inner join Employees_L3";
                    query += " on L3.L3ID  =  Employees_L3.L3ID And Employees_L3.EmpID = " + ProjectSession.EmployeeID;
                }


                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<L3>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Employees_L3> GetAreaByPermissionAndL2ID(int logedInEmpID, int empID, int cityID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<Employees_L3> list = new List<Employees_L3>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });


                parameters.Add(new DBParameters()
                {
                    Name = "L2ID",
                    Value = cityID,
                    DBType = DbType.Int32
                });

                if (logedInEmpID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LOGEDINEMPID",
                        Value = logedInEmpID,
                        DBType = DbType.Int32
                    });

                    query = "select (case isnull((Select employees_L3.L3Id from employees_L3 where employees_L3.L3ID=L3.L3ID " +
                                    "and employees_L3.empID = @EMPID),0)  when 0  then 0 else 1 end) as ChkL3, L3.* from L3 " +
                                     "inner join  Employees_L3 on Employees_L3.L3ID = L3.L3ID " +
                                      "where Employees_L3.empID = @LOGEDINEMPID and L3.L2ID = @L2ID";
                }
                else
                {
                    query = "Select (case isnull((Select employees_L3.L3Id from employees_L3 where employees_L3.L3ID=L3.L3ID " +
                                    "and employees_L3.empID = @EMPID),0)  when 0  then 0 else 1 end) as ChkL3,L3.* from L3 where L3.L2ID = @L2ID";
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Employees_L3>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
