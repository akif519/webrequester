﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;


namespace CMMS.Service
{
    public class EmailConfigurationService : DBExecute
    {
        public EmailConfigurationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Update Email Configurations
        /// </summary>
        /// <param name="lstConfiguration">The list of Configurations item.</param>
        /// <returns></returns>
        public bool UpdateEmailConfigurations(List<Configuration> lstConfiguration)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                foreach (var lst in lstConfiguration)
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    string strQuery = @"UPDATE configuration SET VALUE = @Value Where [Key] = @Key ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "Value",
                        Value = lst.Value,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Key",
                        Value = lst.Key,
                        DBType = DbType.String
                    });

                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }
            }
            return true;
        }

        /// <summary>
        /// Update sms Configurations
        /// </summary>
        /// <param name="lstConfiguration">The list of Configurations item.</param>
        /// <returns></returns>
        public bool UpdateSMSConfigurations(List<Configuration> lstConfiguration)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                foreach (var lst in lstConfiguration)
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    string strQuery = @"UPDATE configuration SET VALUE = @Value Where [Key] = @Key ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "Value",
                        Value = lst.Value,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Key",
                        Value = lst.Key,
                        DBType = DbType.String
                    });

                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }
            }
            return true;
        }

        /// <summary>
        /// Get Data Related to L2 For Notification Rule
        /// </summary>
        /// <returns></returns>
        public virtual IList<TEntity> GetL2DataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT L1.L1ID,L2.L2ID,L2Classes.L2ClassID,sector.SectorID, 
                                L2.L2Code,L2.L2name,L2.L2Altname,L1.L1No,sector.SectorCode,L2Classes.L2ClassCode,NotificationRuleCityID 
                                FROM L2 
                                LEFT JOIN emailnotificationrules_L2 el2 ON L2.L2ID  = el2.L2ID  AND NotificationRuleID = @NotificationRuleID
                                LEFT JOIN L1 ON L1.L1ID  = L2.L1ID 
                                LEFT JOIN L2Classes ON L2Classes.L2ClassID  = L2.L2ClassID 
                                LEFT JOIN sector ON sector.SectorID = L2.SectorID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;

            }
        }

        /// <summary>
        /// Get Data Related to WorkPriority For Notification Rule
        /// </summary>
        /// <returns></returns>
        public virtual IList<TEntity> GetWorkPriorityDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT WP.WorkPriorityID,WP.WorkPriority,WP.AltWorkPriority,EWP.NotificationRuleWorkPriorityID
                                 FROM WorkPriority  WP
                                 LEFT JOIN emailnotificationrules_workpriority EWP ON WP.WorkPriorityID = EWp.WorkPriorityID
                                 AND NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Work Trade For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Work Trade Data</typeparam>
        /// <param name="entity">The Work Trade Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetWorkTradeDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT WT.WorkTradeID,WT.WorkTrade,WT.AltWorkTrade,EWT.NotificationRuleWorkTradeID 
                                 FROM WorkTrade  WT 
                                 LEFT JOIN emailnotificationrules_worktrade EWT ON WT.WorkTradeID = EWT.WorkTradeID 
                                 AND NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Work Trade For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Work Trade Data</typeparam>
        /// <param name="entity">The Work Trade Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetWorkStatusDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT WS.WorkStatusID,WS.WorkStatus,WS.AltWorkStatus,NotificationRuleWorkStatusID,NotificationRuleID
                               FROM WORKStatus  WS 
                               LEFT JOIN emailnotificationrules_workstatus EWS ON EWS.WorkStatusID = WS.WorkStatusID 
                               AND NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Work Type For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Work Type Data</typeparam>
        /// <param name="entity">The Work Type Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetWorkTypeDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT WT.WorkTypeID,WT.WorkTypeDescription,WT.AltWorkTypeDescription,EWT.NotificationRuleWorkTypeID 
                                 FROM WorkType  WT 
                                 LEFT JOIN emailnotificationrules_worktype EWT ON WT.WorkTypeID = EWT.WorkTypeID 
                                 AND NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to L5 For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The L5 Data</typeparam>
        /// <param name="entity">The L5 Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetL5DataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select EL5.NotificationRuleID,EL5.NotificationRuleBuildingID,L5.L5ID,L5.L5No,L5.L5Description,L5.L5AltDescription,L4.L4ID,L4.L4No 
                                 FROM emailnotificationrules_L5 EL5 
                                 INNER JOIN L5 ON L5.L5ID = EL5.L5ID 
                                 INNER JOIN L4 ON L5.L4ID = L4.L4ID 
                                 WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        public virtual IList<TEntity> GetLocationTypeDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select EmailNotification_LocationType.NotificationLocationTypeID,EmailNotification_LocationType.NotificationRuleID,
                                LocationType.LocationTypeId,LocationType.LocationTypeCode,LocationType.LocationTypeDesc,LocationType.LocationTypeAltDesc
                                from EmailNotification_LocationType
                                inner join LocationType on LocationType.LocationTypeID = EmailNotification_LocationType.LocationTypeID
                                WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        public virtual IList<TEntity> GetSubCategoryDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select EmailNotification_SubCategory.NotificationSubCategoryID,EmailNotification_SubCategory.NotificationRuleID,
                                assetsubcategories.AssetSubCatID,assetsubcategories.AssetSubCatCode,assetsubcategories.AssetSubCategory,assetsubcategories.AltAssetSubCategory,
                                assetcategories.AssetCatID,assetcategories.AssetCatCode,assetcategories.AssetCategory,assetcategories.AltAssetCategory
                                from EmailNotification_SubCategory
                                inner join assetsubcategories on assetsubcategories.AssetSubCatID = EmailNotification_SubCategory.AssetSubCatID
                                inner join assetcategories on assetcategories.AssetCatID = assetsubcategories.AssetCatID
                                WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        public virtual IList<TEntity> GetCriticalityDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select EmailNotification_Criticality.NotificationCriticalityID,EmailNotification_Criticality.NotificationRuleID,
                                Criticality.id As CriticalityId, Criticality,AltCriticality
                                from EmailNotification_Criticality
                                inner join Criticality on Criticality.id = EmailNotification_Criticality.CriticalityId 
                                WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Location For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Location Data</typeparam>
        /// <param name="entity">The Location Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetLocationDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select ELocation.NotificationRuleID,ELocation.NotificationRuleLocationID,   
                                 L.LocationID,L.LocationNo,L.LocationDescription,L.LocationAltDescription,L2.L2Code,S.SectorID,S.SectorCode,C.Criticality, 
                                 S.SectorName,L4.L4ID,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5ID,L5.L5Description,L5.L5No ,L5.L5AltDescription
                                 FROM emailnotificationrules_location ELocation 
                                 INNER JOIN Location L ON L.LocationID = ELocation.LocationID 
                                 INNER JOIN L5 ON L5.L5ID = L.L5ID 
                                 INNER JOIN L4 ON L4.L4ID = L.L4ID 
                                 INNER JOIN L2 ON L2.L2ID = L.L2ID 
                                 INNER JOIN L1 ON L1.L1ID = L.L2ID 
                                 INNER JOIN sector S ON S.SectorID = L2.SectorID 
                                 LEFT JOIN criticality C ON C.id = L.CriticalityID 
                                 WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Assets For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Assets Data</typeparam>
        /// <param name="entity">The Assets Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetAssetsDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"SELECT assets.AssetID,assets.AssetStatusID,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription, 
                                 assetstatus.AssetStatusDesc,AltAssetStatusDesc,L.LocationNo,L.LocationDescription,L.LocationAltDescription,ac.AssetCategory,AssetSubCategory,AssetClass,AssetCondition, 
                                 L2.L2ClassID,L2ClassCode,L2.L2ID,L2.L2Code,L2.L2name,sector.SectorID,sector.SectorCode,SectorName,L4.L4ID,L4.L4No,L4.L4Description,L5.L5ID,L5.L5No,L5.L5Description, 
                                 assets.ModelNumber,assets.SerialNumber,NotificationRuleAssetID,NotificationRuleID 
                                 FROM emailnotificationrules_assets ea 
                                 INNER JOIN assets ON ea.AssetID = assets.AssetID 
                                 LEFT JOIN assetstatus ON assets.AssetStatusID = assetstatus.AssetStatusID 
                                 LEFT JOIN Location L ON L.LocationID = assets.LocationID 
                                 LEFT JOIN L2 ON L2.L2ID = L.L2ID 
                                 LEFT JOIN L4 ON L4.L4ID = L.L4ID 
                                 LEFT JOIN L5 ON L5.L5ID = L.L5ID 
                                 LEFT JOIN sector ON L2.SectorID = sector.SectorID 
                                 LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID 
                                 LEFT JOIN assetcategories ac on ac.AssetCatID = assets.AssetCatID 
                                 LEFT JOIN assetsubcategories asubc on asubc.AssetSubCatID = assets.AssetSubCatID 
                                 LEFT JOIN assetclasses aclass on aclass.AssetClassID = assets.AssetClassID 
                                 LEFT JOIN assetconditions on assetconditions.AssetConditionID = assets.AssetConditionID 
                                 WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Employee For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Employee Data</typeparam>
        /// <param name="entity">The Employee Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetEmployeeDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select E.EmployeeID,E.EmployeeNO,E.Name,L2.L2Code,Mdi.MaintDivisionCode,Mdep.MaintDeptCode,MSub.MaintSubDeptCode,
                                E.Positions,Ec.CategoryName,Es.EmployeeStatus,Ene.NotificationRuleID,Ene.NotificationRuleEmployeeID  , 
                                Ene.IsSelectedCriticalProposal,Ene.IsSelectedNormalProposal 
                                FROM employees E 
                                LEFT JOIN emailnotificationrules_employees Ene ON Ene.EmployeeID = E.EmployeeID  AND NotificationRuleID = @NotificationRuleID 
                                INNER JOIN L2 ON E.L2ID = L2.L2ID 
                                LEFT JOIN MainenanceDivision MDi ON MDi.MaintDivisionID = E.MaintDivisionID 
                                LEFT JOIN MaintenanceDepartment Mdep ON Mdep.maintDeptID = E.maintDeptID 
                                LEFT JOIN MaintSubDept MSub ON MSub.MaintSubDeptID = E.MaintSubDeptID 
                                LEFT JOIN employeecategories Ec ON Ec.CategoryID = E.CategoryID 
                                LEFT JOIN employeeStatuses Es ON Es.EmployeeStatusId = E.EmployeeStatusId 
                                 ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        public virtual IList<TEntity> GetSupplierDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select emailnotification_supplier.NotificationRuleSupplierID,emailnotification_supplier.NotificationRuleID,suppliers.SupplierID,
                                suppliers.SupplierNo,suppliers.SupplierName,suppliers.AltSupplierName
                                from suppliers
                                Left join emailnotification_supplier on suppliers.SupplierID = emailnotification_supplier.SupplierID and emailnotification_supplier.NotificationRuleID = @NotificationRuleID";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Get Data Related to Employee Escalation Data For Notification Rule
        /// </summary>
        /// <typeparam name="TEntity">The Employee Data</typeparam>
        /// <param name="entity">The Employee Data Entity</param>
        /// <param name="notificationRuleID">The Notification Rule ID</param>
        /// <returns></returns>
        public virtual IList<TEntity> GetEmployeeEscalationDataForEmailNotification<TEntity>(TEntity entity, int notificationRuleID, [DataSourceRequest]DataSourceRequest request = null)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationRuleID,
                    DBType = DbType.Int32
                });

                string query = @"Select NotificationRuleEscalationID,E.EmployeeID,E.EmployeeNO,E.Name,EscalationTimeInMin 
                                    FROM emailnotificationrules_escalation Ere 
                                    INNER JOIN employees E ON Ere.EmployeeID = E.EmployeeID  
                                    WHERE NotificationRuleID = @NotificationRuleID ";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                var list = objDapperContext.ExecuteQuery<TEntity>(query, parameters);
                this.PagingInformation.TotalPages = list.Count();
                return list;
            }
        }

        /// <summary>
        /// Delete Old Email Notification RUles
        /// </summary>
        public virtual int DeleteNotificationRules(int notificationruleID)
        {
            /*Execute Stored Procedure*/
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = "NotificationRuleID",
                        Value = notificationruleID,
                        DBType = DbType.Int32
                    });

                    if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        string query = @"DELETE FROM emailnotificationrules_L2 Where NotificationRuleID = @NotificationRuleID 
                                ;DELETE FROM emailnotificationrules_L5 Where NotificationRuleID = @NotificationRuleID 
                                ;DELETE FROM emailnotificationrules_events  Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_escalation Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_employees Where NotificationRuleID = @NotificationRuleID 
                                ;DELETE FROM emailnotification_supplier Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_assets Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_location Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM EmailNotification_LocationType Where NotificationRuleID = @NotificationRuleID
                                ;DELETE FROM EmailNotification_SubCategory Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM EmailNotification_Criticality Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_maintsubdept Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_modules Where NotificationRuleID = @NotificationRuleID 
                                ;DELETE FROM emailnotificationrules_processedrule Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_workpriority Where NotificationRuleID= @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_worktype Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_workstatus Where NotificationRuleID = @NotificationRuleID  
                                ;DELETE FROM emailnotificationrules_worktrade Where NotificationRuleID = @NotificationRuleID  ";
                        objDapperContext.ExecuteQuery(query, parameters);
                    }
                    else
                    {
                        string query = "";
                        query = @"DELETE FROM emailnotificationrules_L2 Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_L5 Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_events Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_escalation Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_employees Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotification_supplier Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_assets Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_location Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM EmailNotification_LocationType Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM EmailNotification_SubCategory Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM EmailNotification_Criticality Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_maintsubdept Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_processedrule Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_workpriority Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_worktype Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_workstatus Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                        query = @"DELETE FROM emailnotificationrules_worktrade Where NotificationRuleID = @NotificationRuleID";
                        objDapperContext.ExecuteQuery(query, parameters);
                    }
                    /*define Stored Procedure Name*/

                }
                return 0;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Insert/Update Email Notification RUles
        /// </summary>
        public int SaveNotificationRule(Emailnotificationrule objEmailNotificationRule)
        {
            /*Execute Stored Procedure*/
            try
            {
                int notificationruleID = objEmailNotificationRule.NotificationRuleID;
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters()
                {
                    Name = "NotificationRuleID",
                    Value = notificationruleID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedBy",
                    Value = objEmailNotificationRule.CreatedBy,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedDate",
                    Value = objEmailNotificationRule.CreatedDate,
                    DBType = DbType.Int32
                });

                if (notificationruleID > 0)
                {
                    DeleteNotificationRules(notificationruleID);
                }
                using (DapperContext objDapperContext = new DapperContext())
                {
                    string query = "";
                    if (objEmailNotificationRule.EmailRule_L2 != null)
                    {
                        string L2 = objEmailNotificationRule.EmailRule_L2;
                        string[] values = L2.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_L2(NotificationRuleID,L2ID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_L5 != null)
                    {
                        string L5 = objEmailNotificationRule.EmailRule_L5;
                        string[] values = L5.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_L5(NotificationRuleID,L5ID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_Location != null)
                    {
                        string Location = objEmailNotificationRule.EmailRule_Location;
                        string[] values = Location.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_Location(NotificationRuleID,LocationID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_Assets != null)
                    {
                        string Assets = objEmailNotificationRule.EmailRule_Assets;
                        string[] values = Assets.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_Assets(NotificationRuleID,AssetID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_LocationType != null)
                    {
                        string LocationType = objEmailNotificationRule.EmailRule_LocationType;
                        string[] values = LocationType.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO EmailNotification_LocationType(NotificationRuleID,LocationTypeID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_SubCategory != null)
                    {
                        string SubCategory = objEmailNotificationRule.EmailRule_SubCategory;
                        string[] values = SubCategory.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO EmailNotification_SubCategory(NotificationRuleID,AssetSubCatID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_Criticality != null)
                    {
                        string Criticality = objEmailNotificationRule.EmailRule_Criticality;
                        string[] values = Criticality.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO EmailNotification_Criticality(NotificationRuleID,CriticalityID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_WorkPriority != null)
                    {
                        string Priority = objEmailNotificationRule.EmailRule_WorkPriority;
                        string[] values = Priority.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_workpriority(NotificationRuleID,WorkPriorityID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }
                    if (objEmailNotificationRule.EmailRule_WorkTrade != null)
                    {
                        string Trade = objEmailNotificationRule.EmailRule_WorkTrade;
                        string[] values = Trade.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_worktrade(NotificationRuleID,WorkTradeID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }
                    if (objEmailNotificationRule.EmailRule_WorkType != null)
                    {
                        string Type = objEmailNotificationRule.EmailRule_WorkType;
                        string[] values = Type.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_worktype(NotificationRuleID,WorkTypeID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_WorkStatus != null)
                    {
                        string Status = objEmailNotificationRule.EmailRule_WorkStatus;
                        string[] values = Status.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_workstatus(NotificationRuleID,WorkStatusID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }
                    if (objEmailNotificationRule.EmailRule_EmployeeESC != null)
                    {
                        string EmployeeESC = objEmailNotificationRule.EmailRule_EmployeeESC;
                        string[] values = EmployeeESC.Split(',');
                        string EmployeeESCTime = objEmailNotificationRule.EmailRule_EscTime;
                        string[] values1 = EmployeeESCTime.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotificationrules_escalation(NotificationRuleID,EmployeeID,EscalationTimeInMin,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + "," + values1[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_Employee != null || objEmailNotificationRule.EmailRule_EmployeeCritical != null || objEmailNotificationRule.EmailRule_EmployeeNormal != null)
                    {
                        List<string> lstEmployee = new List<string>();
                        List<string> lstEmployeeNormal = new List<string>();
                        List<string> lstEmployeeCritical = new List<string>();

                        string Employee = objEmailNotificationRule.EmailRule_Employee;
                        string EmployeeNormal = objEmailNotificationRule.EmailRule_EmployeeNormal;
                        string EmployeeCritical = objEmailNotificationRule.EmailRule_EmployeeCritical;

                        if (Employee != null && Employee != "")
                        {
                            string[] values = Employee.Split(',');
                            for (int i = 0; i < values.Length; i++)
                            {
                                lstEmployee.Add(values[i]);
                            }
                        }

                        if (EmployeeNormal != null && EmployeeNormal != "")
                        {
                            string[] values1 = EmployeeNormal.Split(',');
                            for (int i = 0; i < values1.Length; i++)
                            {
                                lstEmployee.Add(values1[i]);
                                lstEmployeeNormal.Add(values1[i]);
                            }
                        }

                        if (EmployeeCritical != null && EmployeeCritical != "")
                        {
                            string[] values2 = EmployeeCritical.Split(',');
                            for (int i = 0; i < values2.Length; i++)
                            {
                                lstEmployee.Add(values2[i]);
                                lstEmployeeCritical.Add(values2[i]);
                            }
                        }

                        var lstDistinctEmployees = lstEmployee.Distinct();

                        foreach (var lst in lstDistinctEmployees)
                        {
                            int intNormal = lstEmployeeNormal.Where(x => x.Contains(lst)).Count();
                            int intCritical = lstEmployeeCritical.Where(x => x.Contains(lst)).Count();


                            string strquery = @" INSERT INTO emailnotificationrules_employees(NotificationRuleID,EmployeeID,CreatedBy,
                                                 CreatedDate,IsSelectedCriticalProposal,IsSelectedNormalProposal) 
                                                 VALUES (@NotificationRuleID," + lst + ",@CreatedBy,@CreatedDate," + intCritical + ", " + intNormal + ")";


                            objDapperContext.ExecuteQuery(strquery, parameters);
                        }
                    }

                    if (objEmailNotificationRule.EmailRule_Supplier != null)
                    {
                        string Supplier = objEmailNotificationRule.EmailRule_Supplier;
                        string[] values = Supplier.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            query = @" INSERT INTO emailnotification_supplier(NotificationRuleID,SupplierID,CreatedBy,CreatedDate)
                                          VALUES (@NotificationRuleID," + values[i].Trim() + ",@CreatedBy,@CreatedDate)";

                            objDapperContext.ExecuteQuery(query, parameters);
                        }
                    }

                }
                return 0;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        #region "Rule Apply Methods"
        public void CheckEmailNotificationRule(SystemEnum.ModuleCode ModuleCode, SystemEnum.ModuleEvent EventCode, string JobID, string oldParamValue = "", string newParamValue = "")
        {
            if (ModuleCode == SystemEnum.ModuleCode.WorkRequest)
            {
                ProcessWorkRequest(JobID, ModuleCode, EventCode);
            }
            else if (ModuleCode == SystemEnum.ModuleCode.WorkOrder)
            {
                ProcessWorkOrder(JobID, ModuleCode, EventCode);

                if ((EventCode == SystemEnum.ModuleEvent.JobOrderCreation || EventCode == SystemEnum.ModuleEvent.JobOrderJobStatusChange) && !string.IsNullOrEmpty(newParamValue))
                {
                    UpdateJobOrderStatusTrack(JobID, oldParamValue, newParamValue);
                }
            }
            else if(ModuleCode == SystemEnum.ModuleCode.Supplier)
            {
                ProcessWorkOrder(JobID, ModuleCode, EventCode);
            }
        }

        private bool UpdateJobOrderStatusTrack(string workOrderNo, string oldStatus, string newStatus)
        {
            try
            {
                JOStatusTrack objJOStatusTrack = new JOStatusTrack();
                objJOStatusTrack.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJOStatusTrack.CreatedDate = DateTime.Now;
                objJOStatusTrack.WorkorderNo = workOrderNo;
                objJOStatusTrack.Old_WorkStatusID = ConvertTo.Integer(oldStatus == string.Empty ? "1" : oldStatus);
                objJOStatusTrack.New_WorkStatusID = ConvertTo.Integer(newStatus == string.Empty ? "1" : newStatus);
                objJOStatusTrack.Latitude = ProjectSession.Latitude;
                objJOStatusTrack.Longitude = ProjectSession.Longitude;
                using (DapperContext context = new DapperContext())
                {
                    int JobStatusID = context.Save(objJOStatusTrack);
                    if (JobStatusID > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void ProcessWorkRequest(string WorkRequestID, SystemEnum.ModuleCode ModuleCode, SystemEnum.ModuleEvent EventCode)
        {
            Worequest ObjectWorkRequest = new Worequest();
            ObjectWorkRequest.RequestNo = WorkRequestID;

            Emailnotificationrule objEmailNotificationRule = new Emailnotificationrule();
            IList<Emailnotificationrule> ListEmailNotificationRule = null;
            objEmailNotificationRule.NotificationRuleModuleID = ModuleCode.GetHashCode();
            objEmailNotificationRule.NotificationRuleEventID = EventCode.GetHashCode();
            objEmailNotificationRule.IsActive = true;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRule = objContext.Search<Emailnotificationrule>(objEmailNotificationRule);
            }

            using (ServiceContext objContext = new ServiceContext())
            {
                ObjectWorkRequest = objContext.Search<Worequest>(ObjectWorkRequest).FirstOrDefault();
            }

            if (ListEmailNotificationRule.Count > 0 && (ObjectWorkRequest != null))
            {
                List<Emailnotificationrules_processedrule> ListEmailNotificationRule_ProcessedRule = new List<Emailnotificationrules_processedrule>();

                IList<Emailnotificationrules_L2> lstEmailNotificationRules_L2 = null;
                using (ServiceContext objContext = new ServiceContext())
                {
                    lstEmailNotificationRules_L2 = objContext.SearchAll<Emailnotificationrules_L2>(new Emailnotificationrules_L2());
                }

                foreach (Emailnotificationrule ObjectEmailNotificationRule in ListEmailNotificationRule)
                {
                    List<int?> ListCityID = lstEmailNotificationRules_L2.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Item.L2ID).ToList();
                    bool IsRuleApplicable = false;

                    if ((ListCityID != null) && ListCityID.Count > 0)
                    {
                        if (ListCityID.Contains(ObjectWorkRequest.L2ID.Value))
                        {
                            IsRuleApplicable = true;
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkPriority(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.Workpriorityid));
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkType(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.WorkTypeID));
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkTrade(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.Worktradeid));
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintDivisionID != null) && ObjectWorkRequest.MaintdivId != ObjectEmailNotificationRule.MaintDivisionID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintDeptID != null) && ObjectWorkRequest.MaintdeptId != ObjectEmailNotificationRule.MaintDeptID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintSubDeptID != null) && ObjectWorkRequest.MainSubDeptId != ObjectEmailNotificationRule.MaintSubDeptID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable && EventCode == SystemEnum.ModuleEvent.JobRequestJobStatusChange)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkStatus(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.RequestStatusID));
                        }

                        if (IsRuleApplicable && (ObjectWorkRequest.L5ID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Building(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.L5ID));
                        }

                        if (IsRuleApplicable && (ObjectWorkRequest.LocationID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Location(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.LocationID));
                        }

                        if (IsRuleApplicable && (ObjectWorkRequest.AssetID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Asset(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkRequest.AssetID));
                        }

                        if (IsRuleApplicable)
                        {
                            Emailnotificationrules_processedrule ObjectEmailNotificationRule_ProcessedRule = new Emailnotificationrules_processedrule();
                            ObjectEmailNotificationRule_ProcessedRule.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;
                            ObjectEmailNotificationRule_ProcessedRule.ReferenceID = ObjectWorkRequest.RequestNo;
                            ObjectEmailNotificationRule_ProcessedRule.IsMailSent = false;
                            ObjectEmailNotificationRule_ProcessedRule.CreatedDate = DateTime.Now;
                            ObjectEmailNotificationRule_ProcessedRule.emailnotificationrule = ObjectEmailNotificationRule;
                            ListEmailNotificationRule_ProcessedRule.Add(ObjectEmailNotificationRule_ProcessedRule);
                        }
                    }
                }

                SaveNewEmailNotificationRule_ProcessedRule(ListEmailNotificationRule_ProcessedRule);
            }
        }

        private void ProcessWorkOrder(string WorkOrderID, SystemEnum.ModuleCode ModuleCode, SystemEnum.ModuleEvent EventCode)
        {
            WorkOrder ObjectWorkOrder = new WorkOrder();
            ObjectWorkOrder.WorkorderNo = WorkOrderID;

            Emailnotificationrule objEmailNotificationRule = new Emailnotificationrule();
            IList<Emailnotificationrule> ListEmailNotificationRule = null;
            objEmailNotificationRule.NotificationRuleModuleID = ModuleCode.GetHashCode();
            objEmailNotificationRule.NotificationRuleEventID = EventCode.GetHashCode();
            objEmailNotificationRule.IsActive = true;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRule = objContext.Search<Emailnotificationrule>(objEmailNotificationRule);
            }

            using (ServiceContext objContext = new ServiceContext())
            {
                ObjectWorkOrder = objContext.Search<WorkOrder>(ObjectWorkOrder).FirstOrDefault();
            }

            if (ListEmailNotificationRule.Count > 0 && (ObjectWorkOrder != null))
            {
                List<Emailnotificationrules_processedrule> ListEmailNotificationRule_ProcessedRule = new List<Emailnotificationrules_processedrule>();
                IList<Emailnotificationrules_L2> lstEmailNotificationRules_L2 = null;

                using (ServiceContext objContext = new ServiceContext())
                {
                    lstEmailNotificationRules_L2 = objContext.SearchAll<Emailnotificationrules_L2>(new Emailnotificationrules_L2());
                }

                foreach (Emailnotificationrule ObjectEmailNotificationRule in ListEmailNotificationRule)
                {
                    List<int> ListCityID = lstEmailNotificationRules_L2.Where(Item => Item.NotificationRuleID == ObjectEmailNotificationRule.NotificationRuleID).Select(Item => Convert.ToInt32(Item.L2ID)).ToList();
                    if ((ListCityID != null) && ListCityID.Count > 0)
                    {
                        bool IsRuleApplicable = false;

                        if (ListCityID.Contains(ObjectWorkOrder.L2ID.Value))
                        {
                            IsRuleApplicable = true;
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkPriority(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.WorkPriorityID));
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkType(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.WorkTypeID));
                        }

                        if (IsRuleApplicable)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkTrade(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.WOTradeID));
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintDivisionID != null) && ObjectWorkOrder.MaintDivisionID != ObjectEmailNotificationRule.MaintDivisionID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintDeptID != null) && ObjectWorkOrder.MaintdeptID != ObjectEmailNotificationRule.MaintDeptID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable)
                        {
                            if ((ObjectEmailNotificationRule.MaintSubDeptID != null) && ObjectWorkOrder.MaintsubdeptID != ObjectEmailNotificationRule.MaintSubDeptID)
                            {
                                IsRuleApplicable = false;
                            }
                        }

                        if (IsRuleApplicable && EventCode == SystemEnum.ModuleEvent.JobOrderJobStatusChange)
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_WorkStatus(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.WorkStatusID));
                        }

                        if (IsRuleApplicable && (ObjectWorkOrder.L5ID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Building(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.L5ID));
                        }

                        if (IsRuleApplicable && (ObjectWorkOrder.LocationID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Location(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.LocationID));
                        }

                        if (IsRuleApplicable && (ObjectWorkOrder.AssetID != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Asset(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.AssetID));
                        }

                        if (IsRuleApplicable && (ObjectWorkOrder.SupplierId != null))
                        {
                            IsRuleApplicable = CheckIfRuleApplicable_Supplier(ObjectEmailNotificationRule.NotificationRuleID, Convert.ToInt32(ObjectWorkOrder.SupplierId));
                        }

                        if (IsRuleApplicable)
                        {
                            Emailnotificationrules_processedrule ObjectEmailNotificationRule_ProcessedRule = new Emailnotificationrules_processedrule();
                            ObjectEmailNotificationRule_ProcessedRule.NotificationRuleID = ObjectEmailNotificationRule.NotificationRuleID;
                            ObjectEmailNotificationRule_ProcessedRule.ReferenceID = ObjectWorkOrder.WorkorderNo;
                            ObjectEmailNotificationRule_ProcessedRule.IsMailSent = false;
                            ObjectEmailNotificationRule_ProcessedRule.CreatedDate = DateTime.Now;
                            ObjectEmailNotificationRule_ProcessedRule.emailnotificationrule = ObjectEmailNotificationRule;
                            ListEmailNotificationRule_ProcessedRule.Add(ObjectEmailNotificationRule_ProcessedRule);

                        }
                    }
                }

                SaveNewEmailNotificationRule_ProcessedRule(ListEmailNotificationRule_ProcessedRule);
            }
        }

        private bool CheckIfRuleApplicable_WorkPriority(int EmailNotificationRuleID, int WorkPriorityID)
        {
            IList<Emailnotificationrules_workpriority> ListEmailNotificationRules_WorkPriority = null;
            Emailnotificationrules_workpriority obj = new Emailnotificationrules_workpriority();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_WorkPriority = objContext.Search<Emailnotificationrules_workpriority>(obj);
            }

            if (ListEmailNotificationRules_WorkPriority.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_WorkPriority.Count > 0 && ListEmailNotificationRules_WorkPriority.Any(Item => Item.WorkPriorityID == WorkPriorityID))
            {
                return true;
            }

            return false;
        }

        private bool CheckIfRuleApplicable_WorkType(int EmailNotificationRuleID, int WorkTypeID)
        {
            IList<Emailnotificationrules_worktype> ListEmailNotificationRules_WorkType = null;
            Emailnotificationrules_worktype obj = new Emailnotificationrules_worktype();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_WorkType = objContext.Search<Emailnotificationrules_worktype>(obj);
            }

            if (ListEmailNotificationRules_WorkType.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_WorkType.Count > 0 && ListEmailNotificationRules_WorkType.Any(Item => Item.WorkTypeID == WorkTypeID))
            {
                return true;
            }

            return false;

        }

        private bool CheckIfRuleApplicable_WorkTrade(int EmailNotificationRuleID, int WorkTradeID)
        {
            IList<Emailnotificationrules_worktrade> ListEmailNotificationRules_WorkTrade = null;
            Emailnotificationrules_worktrade obj = new Emailnotificationrules_worktrade();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_WorkTrade = objContext.Search<Emailnotificationrules_worktrade>(obj);
            }

            if (ListEmailNotificationRules_WorkTrade.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_WorkTrade.Count > 0 && ListEmailNotificationRules_WorkTrade.Any(Item => Item.WorkTradeID == WorkTradeID))
            {
                return true;
            }

            return false;
        }

        private bool CheckIfRuleApplicable_WorkStatus(int EmailNotificationRuleID, int WorkStatusID)
        {
            IList<Emailnotificationrules_workstatu> ListEmailNotificationRules_WorkStatus = null;
            Emailnotificationrules_workstatu obj = new Emailnotificationrules_workstatu();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_WorkStatus = objContext.Search<Emailnotificationrules_workstatu>(obj);
            }

            if (ListEmailNotificationRules_WorkStatus.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_WorkStatus.Count > 0 && ListEmailNotificationRules_WorkStatus.Any(Item => Item.WorkStatusID == WorkStatusID))
            {
                return true;
            }

            return false;
        }

        private bool CheckIfRuleApplicable_Supplier(int EmailNotificationRuleID, int SupplierID)
        {
            IList<EmailNotification_Supplier> ListEmailNotification_Supplier = null;
            EmailNotification_Supplier obj = new EmailNotification_Supplier();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotification_Supplier = objContext.Search<EmailNotification_Supplier>(obj);
            }

            if (ListEmailNotification_Supplier.Count == 0)
            {
                return true;
            }

            if (ListEmailNotification_Supplier.Count > 0 && ListEmailNotification_Supplier.Any(Item => Item.SupplierID == SupplierID))
            {
                return true;
            }

            return false;
        }

        private bool CheckIfRuleApplicable_Building(int EmailNotificationRuleID, int BuildingID)
        {
            IList<Emailnotificationrules_L5> ListEmailNotificationRules_Building = null;
            Emailnotificationrules_L5 obj = new Emailnotificationrules_L5();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_Building = objContext.Search<Emailnotificationrules_L5>(obj);
            }

            if (ListEmailNotificationRules_Building.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_Building.Count > 0 && ListEmailNotificationRules_Building.Any(Item => Item.L5ID == BuildingID))
            {
                return true;
            }

            return false;

        }

        private bool CheckIfRuleApplicable_Location(int EmailNotificationRuleID, int LocationID)
        {
            IList<Emailnotificationrules_location> ListEmailNotificationRules_Location = null;
            Emailnotificationrules_location obj = new Emailnotificationrules_location();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_Location = objContext.Search<Emailnotificationrules_location>(obj);
            }

            if (ListEmailNotificationRules_Location.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_Location.Count > 0 && ListEmailNotificationRules_Location.Any(Item => Item.LocationID == LocationID))
            {
                return true;
            }

            return false;

        }

        private bool CheckIfRuleApplicable_Asset(int EmailNotificationRuleID, int AssetID)
        {
            IList<Emailnotificationrules_asset> ListEmailNotificationRules_Asset = null;
            Emailnotificationrules_asset obj = new Emailnotificationrules_asset();
            obj.NotificationRuleID = EmailNotificationRuleID;

            using (ServiceContext objContext = new ServiceContext())
            {
                ListEmailNotificationRules_Asset = objContext.Search<Emailnotificationrules_asset>(obj);
            }

            if (ListEmailNotificationRules_Asset.Count == 0)
            {
                return true;
            }

            if (ListEmailNotificationRules_Asset.Count > 0 && ListEmailNotificationRules_Asset.Any(Item => Item.AssetID == AssetID))
            {
                return true;
            }

            return false;

        }

        public void SaveNewEmailNotificationRule_ProcessedRule(List<Emailnotificationrules_processedrule> ListEmailNotificationRule_ProcessedRule)
        {
            if (ListEmailNotificationRule_ProcessedRule.Count > 0)
            {
                List<Emailnotificationrules_processedrule> ListNewEmailNotificationRule_ProcessedRule = new List<Emailnotificationrules_processedrule>();
                IList<Emailnotificationrules_processedrule> emailnotificationrules_processedrules = new List<Emailnotificationrules_processedrule>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    emailnotificationrules_processedrules = objContext.SearchAll<Emailnotificationrules_processedrule>(new Emailnotificationrules_processedrule());
                }

                foreach (Emailnotificationrules_processedrule ObjectEmailNotificationRule_ProcessedRule in ListEmailNotificationRule_ProcessedRule)
                {
                    if (!emailnotificationrules_processedrules.Any(Item => Item.NotificationRuleID == ObjectEmailNotificationRule_ProcessedRule.NotificationRuleID & Item.ReferenceID == ObjectEmailNotificationRule_ProcessedRule.ReferenceID))
                    {
                        ListNewEmailNotificationRule_ProcessedRule.Add(ObjectEmailNotificationRule_ProcessedRule);
                    }
                }

                foreach (Emailnotificationrules_processedrule ObjectEmailNotificationRule_ProcessedRule in ListNewEmailNotificationRule_ProcessedRule)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.Save<Emailnotificationrules_processedrule>(ObjectEmailNotificationRule_ProcessedRule);
                    }
                }
            }
        }
        #endregion


        #region "General Configuration"

        /// <summary>
        /// Updates the general configurations.
        /// </summary>
        /// <param name="lstConfigurations">The LST configurations.</param>
        /// <returns></returns>
        public bool UpdateGeneralConfigurations(List<Configurations> lstConfigurations)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                foreach (var lst in lstConfigurations)
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    string strQuery = @"UPDATE Configurations SET ConfigValue = @ConfigValue , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  Where [ConfigKey] = @ConfigKey ";

                    parameters.Add(new DBParameters()
                    {
                        Name = "ConfigValue",
                        Value = lst.ConfigValue,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ConfigKey",
                        Value = lst.ConfigKey,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = ProjectSession.EmployeeID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });


                    objDapperContext.ExecuteQuery(strQuery, parameters);
                }
            }
            return true;
        }

        #endregion
    }
}
