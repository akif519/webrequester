﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Service
{
    public class BuildingService : DBExecute
    {
        public BuildingService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<L5> GetBuildingDetails(int L2ID, int L3ID, int L4ID, int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<L5> list = new List<L5>();
                string query = string.Empty;
                string L2Filter = string.Empty;
                string L4Filter = string.Empty;
                string L3Filter = string.Empty;
                string strWhereClause = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();


                if (L4ID > 0)
                {
                    L4Filter = " AND L5.L4ID =@L4ID ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L4ID",
                        Value = L4ID,
                        DBType = DbType.Int32
                    });
                }
                if (L3ID > 0)
                {
                    L3Filter = " AND L5.L3ID =@L3ID ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L3ID",
                        Value = L3ID,
                        DBType = DbType.Int32
                    });
                }
                if (L2ID > 0)
                {
                    L2Filter = " AND L5.L2ID =@L2ID ";
                    parameters.Add(new DBParameters()
                    {
                        Name = "L2ID",
                        Value = L2ID,
                        DBType = DbType.Int32
                    });
                }

                query += "SELECT L5.*,L2.L2Code,L4.L4No,L3No From L5 INNER JOIN L2  ON  L5.L2ID = L2.L2ID " + L2Filter + " INNER JOIN L4 ON  L5.L4ID = L4.L4ID " + L4Filter + " LEFT JOIN L3  ON  L5.L3ID = L3.L3ID " + L3Filter;

                 if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    query += " where 1=1 and (lower(L5.L5No) like N'%" + text + "%' or lower(L5.L5Description) like N'%" + text + "%' or lower(L5.L5AltDescription) like N'%" + text + "%')";
                }
                 else
                {
                    query += " where 1=1 and (L5.L5No like N'%" + text + "%' or L5.L5Description like N'%" + text + "%' or L5.L5AltDescription like N'%" + text + "%')";
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    if (!string.IsNullOrEmpty(text))
                    {
                        query += " AND " + strWhereClause;
                    }
                    else
                    {
                        query += " Where " + strWhereClause;
                    }
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<L5>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Building (l5) Data For Email Rules , Can be Used to Bind All Data Grid For Building
        /// </summary>
        /// <param name="pageNo"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<L5> GetBuildingDetailsAll(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<L5> list = new List<L5>();
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "Select L5.L5ID,L5.L4ID,L5.L5No,L5Description,L5AltDescription,L4No FROM L5 INNER JOIN L4 ON L4.L4ID = L5.L4ID";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<L5>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }


                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<LocationType> GetLocationTypeAll(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<LocationType> list = new List<LocationType>();
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "Select * from LocationType";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<LocationType>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }


                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<Criticalities> GetCriticalityAll(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<Criticalities> list = new List<Criticalities>();
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += "Select * from Criticality";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Criticalities>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }


                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<AssetSubCategorie> GetSubCategoryAll(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<AssetSubCategorie> list = new List<AssetSubCategorie>();
                string query = string.Empty;

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                query += @"Select 
                        assetsubcategories.AssetSubCatID,assetsubcategories.AssetSubCatCode,assetsubcategories.AssetSubCategory,assetsubcategories.AltAssetSubCategory,
                        assetcategories.AssetCatID,assetcategories.AssetCatCode,assetcategories.AssetCategory,assetcategories.AltAssetCategory
                        from assetsubcategories 
                        inner join assetcategories on assetcategories.AssetCatID = assetsubcategories.AssetCatID";

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " Where " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<AssetSubCategorie>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }


                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
