﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Service.ConfigurationService
{
    public class CustomHelpService : DBExecute
    {
        public CustomHelpService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<HelpModule_Detail> GetHelpModuleList(string langCode)
        {
            ProjectSession.UseHelpDB = true;

            IList<HelpModule_Detail> list = new List<HelpModule_Detail>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters() { Name = "LangCode", Value = langCode, DBType = DbType.String });

            string query = "SELECT hmd.HelpModuleID,hmd.HelpTitle,hmd.HelpModuelDetailID FROM HelpModule_Detail hmd " +
                            "INNER JOIN HelpModule hm ON hm.HelpModuleID = hmd.HelpModuleID " +
                            "INNER JOIN [Language] l ON l.LanguageID = hmd.LanguageID " +
                            "Where l.LanguageCode = @LangCode And hm.ParentID = 0 ";
            //" hm.HasHelpDescription = @HasHelpDescription ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.ExecuteQuery<HelpModule_Detail>(query, outParameters);
            }
            return list;
        }

        /// <summary>
        /// Gets the help module grid.
        /// </summary>
        /// <param name="helpModuleID">The help module identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<HelpModule> GetHelpModuleGrid(int helpModuleID, string pageName, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            ProjectSession.UseHelpDB = true;
            string strWhereClause = string.Empty;
            IList<HelpModule> list = new List<HelpModule>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters() { Name = "LangCode", Value = ProjectSession.Culture, DBType = DbType.String });

            outParameters.Add(new DBParameters() { Name = "helpModuleID", Value = helpModuleID, DBType = DbType.Int32 });

            outParameters.Add(new DBParameters() { Name = "PageName", Value = pageName, DBType = DbType.String });
            string query = string.Empty;

            query += "SELECT isnull(hmd2.HelpTitle,hm.ModuleName) AS ModuleName,isnull(hmd.HelpTitle,hm.ModuleName) AS PageName,hm.HasHelpDescription,hmd.HelpModuelDetailID,hm.HelpModuleID,l.LanguageID ";
            query += "FROM  HelpModule hm ";
            query += "INNER JOIN [Language] l ON l.LanguageCode = @LangCode ";
            query += "Left JOIN HelpModule_Detail hmd ON hm.HelpModuleID = hmd.HelpModuleID AND  l.LanguageID = hmd.LanguageID ";
            query += "LEFT JOIN HelpModule hmp ON hmp.HelpModuleId = hm.ParentID  ";
            query += "LEFT JOIN HelpModule_Detail hmd2 ON hmd2.HelpModuleID = hmp.HelpModuleID AND hmd2.LanguageID = l.LanguageID WHERE  1=1 ";

            if (helpModuleID > 0)
            {
                query += "And (hm.HelpModuleID = @helpModuleID OR hm.ParentID = @helpModuleID) ";
            }
            query += "And ISNULL(hmd.HelpTitle,hm.ModuleName) LIKE ('%'+ISNULL(@PageName,'')+'%')";


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query = "Select * from (" + query + ")A Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<HelpModule>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<HelpChapter> GetChapterList(int helpModuleDetailID, int pageNo, string sortExpression, string sortDirection)
        {
            ProjectSession.UseHelpDB = true;

            IList<HelpChapter> list = new List<HelpChapter>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters() { Name = "helpModuleDetailID", Value = helpModuleDetailID, DBType = DbType.Int32 });
            string query = string.Empty;

            query += "SELECT * FROM HelpChapter hc ";
            if (helpModuleDetailID > 0)
            {
                query += "WHERE hc.HelpModuleDetailID = @helpModuleDetailID ";
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<HelpChapter>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public bool InsertHelpDetailPage(HelpModule_Detail objHelpModuleDetail)
        {
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    Language objLanguage = new Language();
                    ProjectSession.UseHelpDB = true;
                    objLanguage = objDapperContext.SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();
                    objHelpModuleDetail.LanguageID = objLanguage.LanguageID;
                    if (objHelpModuleDetail.HelpModuelDetailID == 0)
                    {
                        objHelpModuleDetail.CreatedBy = ProjectSession.EmployeeID;
                        objHelpModuleDetail.CreatedDate = DateTime.Now;
                        ProjectSession.UseHelpDB = true;
                        objDapperContext.Save(objHelpModuleDetail);
                    }
                    else
                    {
                        HelpModule_Detail objNew = new HelpModule_Detail();
                        ProjectSession.UseHelpDB = true;
                        objNew = objDapperContext.SelectObject<HelpModule_Detail>(objHelpModuleDetail.HelpModuelDetailID);
                        if (objNew != null)
                        {
                            if (objHelpModuleDetail.LanguageID == objHelpModuleDetail.LanguageID)
                            {
                                objHelpModuleDetail.ModifiedBy = ProjectSession.EmployeeID;
                                objHelpModuleDetail.ModifiedDate = DateTime.Now;
                                ProjectSession.UseHelpDB = true;
                                objDapperContext.Save(objHelpModuleDetail);
                            }
                            else
                            {
                                objHelpModuleDetail.HelpModuelDetailID = 0;
                                objHelpModuleDetail.CreatedBy = ProjectSession.EmployeeID;
                                objHelpModuleDetail.CreatedDate = DateTime.Now;
                                ProjectSession.UseHelpDB = true;
                                objDapperContext.Save(objHelpModuleDetail);
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<HelpRelatedTopic> GetHelpRelatedTopics(int helpModuleDetailID, int pageNo, string sortExpression, string sortDirection)
        {
            ProjectSession.UseHelpDB = true;

            IList<HelpRelatedTopic> list = new List<HelpRelatedTopic>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters() { Name = "helpModuleDetailID", Value = helpModuleDetailID, DBType = DbType.Int32 });
            string query = string.Empty;

            query += "SELECT hrt.RelatedTopicID, hrt.ParentHelpModuleDetailID,hmd.HelpTitle AS ParentHelpTitle,hrt.ChildHelpModuleDetailID,hmd1.HelpTitle AS ChildHelpTitle ";
            query += "FROM HelpRelatedTopics hrt ";
            query += "INNER JOIN HelpModule_Detail hmd ON hmd.HelpModuelDetailID = hrt.ParentHelpModuleDetailID ";
            query += "INNER JOIN HelpModule_Detail hmd1 ON hmd1.HelpModuelDetailID = hrt.ChildHelpModuleDetailID ";
            if (helpModuleDetailID > 0)
            {
                query += "WHERE hrt.ParentHelpModuleDetailID = @helpModuleDetailID ";
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<HelpRelatedTopic>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<HelpModule_Detail> GetHelpRelatedTopicsList(string langCode, int hasHelpDescription, int helpDetailId, int? childHelpModuleDetailID)
        {
            ProjectSession.UseHelpDB = true;

            IList<HelpModule_Detail> list = new List<HelpModule_Detail>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            outParameters.Add(new DBParameters() { Name = "LangCode", Value = langCode, DBType = DbType.String });

            outParameters.Add(new DBParameters() { Name = "HasHelpDescription", Value = hasHelpDescription, DBType = DbType.Int32 });

            outParameters.Add(new DBParameters() { Name = "HelpDetailID", Value = helpDetailId, DBType = DbType.Int32 });
            string query = string.Empty;

            if (childHelpModuleDetailID > 0)
            {

                query = "SELECT hmd.HelpModuleID,hmd.HelpTitle,hmd.HelpModuelDetailID FROM HelpModule_Detail hmd " +
                                "INNER JOIN HelpModule hm ON hm.HelpModuleID = hmd.HelpModuleID " +
                                "INNER JOIN [Language] l ON l.LanguageID = hmd.LanguageID " +
                                "LEFT JOIN HelpRelatedTopics hrt ON hrt.ChildHelpModuleDetailID = hmd.HelpModuelDetailID AND hrt.ParentHelpModuleDetailID = @HelpDetailID " +
                                "Where l.LanguageCode = @LangCode And hm.HasHelpDescription = @HasHelpDescription AND hmd.HelpModuelDetailID NOT IN (@HelpDetailID) AND hrt.RelatedTopicID IS NULL " +
                                "union " +
                                "select HelpModule_Detail.HelpModuleID,HelpModule_Detail.HelpTitle,HelpModule_Detail.HelpModuelDetailID from HelpModule_Detail WHERE HelpModule_Detail.HelpModuelDetailID = " + childHelpModuleDetailID;

            }
            else
            {
                query = "SELECT hmd.HelpModuleID,hmd.HelpTitle,hmd.HelpModuelDetailID FROM HelpModule_Detail hmd " +
                                               "INNER JOIN HelpModule hm ON hm.HelpModuleID = hmd.HelpModuleID " +
                                               "INNER JOIN [Language] l ON l.LanguageID = hmd.LanguageID " +
                                               "LEFT JOIN HelpRelatedTopics hrt ON hrt.ChildHelpModuleDetailID = hmd.HelpModuelDetailID AND hrt.ParentHelpModuleDetailID = @HelpDetailID " +
                                               "Where l.LanguageCode = @LangCode And hm.HasHelpDescription = @HasHelpDescription AND hmd.HelpModuelDetailID NOT IN (@HelpDetailID) AND hrt.RelatedTopicID IS NULL";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.ExecuteQuery<HelpModule_Detail>(query, outParameters);
            }
            return list;
        }

        public int DeleteHelpModuleById(int helpModuleID)
        {
            int returnValue = 0;
            try
            {
                HelpModule_Detail objHelpModuleDetail = new HelpModule_Detail();
                Language objLanguage = new Language();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (DapperContext objContext = new DapperContext())
                {
                    ProjectSession.UseHelpDB = true;
                    objLanguage = objContext.SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();
                    ProjectSession.UseHelpDB = true;
                    objHelpModuleDetail = objContext.SearchAll(objHelpModuleDetail).Where(o => o.HelpModuleID == helpModuleID && o.LanguageID == objLanguage.LanguageID).FirstOrDefault();
                    if (objHelpModuleDetail != null && objHelpModuleDetail.HelpModuelDetailID > 0)
                    {
                        string QueryHelpChapter = "Delete From HelpChapter Where HelpModuleDetailID = " + objHelpModuleDetail.HelpModuelDetailID;
                        ProjectSession.UseHelpDB = true;
                        returnValue = objContext.ExecuteQuery(QueryHelpChapter, parameters);

                        string QueryHelpRelatedTopics = "Delete From HelpRelatedTopics Where ParentHelpModuleDetailID = " + objHelpModuleDetail.HelpModuelDetailID;
                        ProjectSession.UseHelpDB = true;
                        returnValue = objContext.ExecuteQuery(QueryHelpRelatedTopics, parameters);

                        string QueryHelpModuleDetail = "Delete From HelpModule_Detail Where HelpModuelDetailID = " + objHelpModuleDetail.HelpModuelDetailID;
                        ProjectSession.UseHelpDB = true;
                        returnValue = objContext.ExecuteQuery(QueryHelpModuleDetail, parameters);
                    }

                    ProjectSession.UseHelpDB = true;
                    returnValue = objContext.Delete<HelpModule>(helpModuleID, false);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
