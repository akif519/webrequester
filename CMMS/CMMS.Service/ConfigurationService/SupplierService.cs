﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CMMS.Service
{
    public class SupplierService : DBExecute
    {
        public SupplierService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<Supplier> GetAllSuppliers(string strWhere, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<Supplier> list = new List<Supplier>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = "Select " +
                                " SupplierID,SupplierNo,SupplierName,AltSupplierName,suppliers.CategoryID,SupplierCategoryName " +
                                " FROM suppliers LEFT OUTER JOIN SupplierCategory ON SupplierCategory.CategoryID = suppliers.CategoryID " +
                                " where 1=1 " + strWhere;

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";

                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        list = objDapperContext.AdvanceSearch<Supplier>(query, parameters, pageNo);
                        this.PagingInformation = objDapperContext.PagingInformation;
                    }
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public IList<CmSupplierContractorDocument> GetSupplierDocument(int supplierID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<CmSupplierContractorDocument> list = new List<CmSupplierContractorDocument>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "DocID";
            }

            string query = "Select * from cmSupplierContractorDocument where SupplierContractorID = " + supplierID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<CmSupplierContractorDocument>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Checks the document duplicate.
        /// </summary>
        /// <param name="docID">The document identifier.</param>
        /// <param name="docName">Name of the document.</param>
        /// <returns></returns>
        public static int checkDocumentDuplicate(int docID, string docName, int supplierID)
        {
            int count = 0;

            if (docID > 0)
            {
                string query = " Select count(1) from cmSupplierContractorDocument where docName = '" + Common.setQuote(docName) + "' and SupplierContractorID = " + supplierID + " and docID !=  " + docID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
            else
            {
                string query = " Select count(1) from cmSupplierContractorDocument where  docName = '" + Common.setQuote(docName) + "'and SupplierContractorID = " + supplierID;

                using (DapperDBContext context = new DapperDBContext())
                {
                    count = Convert.ToInt32(context.ExecuteScalar(query));
                    return count;
                }
            }
        }

        public static bool UpdateSupplierContact(Supplier objSupplier)
        {
            try
            {
                using (DapperContext context = new DapperContext())
                {
                    string strQuery = string.Empty;
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierID",
                        Value = objSupplier.SupplierID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactName",
                        Value = objSupplier.ContactName,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactName2",
                        Value = objSupplier.ContactName2,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactName3",
                        Value = objSupplier.ContactName3,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactTitle",
                        Value = objSupplier.ContactTitle,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactTitle2",
                        Value = objSupplier.ContactTitle2,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ContactTitle3",
                        Value = objSupplier.ContactTitle3,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "Email1",
                        Value = objSupplier.Email1,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Email2",
                        Value = objSupplier.Email2,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Email3",
                        Value = objSupplier.Email3,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "Phone1",
                        Value = objSupplier.Phone1,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Phone2",
                        Value = objSupplier.Phone2,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Phone3",
                        Value = objSupplier.Phone3,
                        DBType = DbType.String
                    });


                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objSupplier.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objSupplier.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"UPDATE Suppliers SET ContactName = @ContactName , ContactName2 = @ContactName2 , ContactName3 = @ContactName3,
                                                  ContactTitle = @ContactTitle , ContactTitle2 = @ContactTitle2 , ContactTitle3 = @ContactTitle3,
                                                  Email1 = @Email1 , Email2 = @Email2 , Email3 = @Email3,
                                                  Phone1 = @Phone1 , Phone2 = @Phone2 , Phone3 = @Phone3,
                                                  ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate 
                            where SupplierID = @SupplierID ";


                    context.ExecuteQuery(strQuery, parameters);
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool UpdateSupplier(Supplier objSupplier)
        {
            try
            {
                using (DapperContext context = new DapperContext())
                {
                    string strQuery = string.Empty;
                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierID",
                        Value = objSupplier.SupplierID,
                        DBType = DbType.Int32
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierNo",
                        Value = objSupplier.SupplierNo,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "SupplierName",
                        Value = objSupplier.SupplierName,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "AltSupplierName",
                        Value = objSupplier.AltSupplierName,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CategoryID",
                        Value = objSupplier.CategoryID,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Address",
                        Value = objSupplier.Address,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Services",
                        Value = objSupplier.Services,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "StateOrProvince",
                        Value = objSupplier.StateOrProvince,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "PostalCode",
                        Value = objSupplier.PostalCode,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "City",
                        Value = objSupplier.City,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "Country",
                        Value = objSupplier.Country,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "PhoneNumber",
                        Value = objSupplier.PhoneNumber,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "FaxNumber",
                        Value = objSupplier.FaxNumber,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "Email",
                        Value = objSupplier.Email,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "ClassificationLevel",
                        Value = objSupplier.ClassificationLevel,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "CR_No",
                        Value = objSupplier.CR_No,
                        DBType = DbType.String
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "CR_ExpiryDate",
                        Value = objSupplier.CR_ExpiryDate,
                        DBType = DbType.DateTime
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "CostCenterId",
                        Value = objSupplier.CostCenterId,
                        DBType = DbType.Int32
                    });
                    parameters.Add(new DBParameters()
                    {
                        Name = "AccCodeid",
                        Value = objSupplier.AccCodeid,
                        DBType = DbType.Int32
                    });


                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objSupplier.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objSupplier.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    //Update
                    strQuery = @"UPDATE Suppliers SET SupplierNo = @SupplierNo , SupplierName = @SupplierName , AltSupplierName = @AltSupplierName,
                                                  CategoryID = @CategoryID , Address = @Address , Services = @Services,
                                                  StateOrProvince = @StateOrProvince , PostalCode = @PostalCode , City = @City,
                                                  Country = @Country , PhoneNumber = @PhoneNumber , FaxNumber = @FaxNumber,
                                                  Email = @Email , ClassificationLevel = @ClassificationLevel , CR_No = @CR_No,
                                                  CR_ExpiryDate = @CR_ExpiryDate , CostCenterId = @CostCenterId , AccCodeid = @AccCodeid,
                                                  ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate 
                            where SupplierID = @SupplierID ";

                    context.ExecuteQuery(strQuery, parameters);

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckDupliacteSupplier(int supplierid, string supplierNo)
        {
            try
            {
                string query = "Select count(1) from Suppliers where supplierid != " + supplierid + " and supplierNo = '" + Common.setQuote(supplierNo) + "'";

                using (DapperDBContext context = new DapperDBContext())
                {
                    int count = Convert.ToInt32(context.ExecuteScalar(query));
                    if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
