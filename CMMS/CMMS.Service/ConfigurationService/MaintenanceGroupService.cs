﻿using CMMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Model;
using System.Collections.ObjectModel;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Service
{
    public class MaintenanceGroupService : DBExecute
    {
        public MaintenanceGroupService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<MaintGroup> GetMaintGroupsHavingTechnicians(int pageNo, string sortExpression, string sortDirection, string text = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<MaintGroup> list = new List<MaintGroup>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select distinct mg.GroupID,mg.GroupCode,mg.GroupDesc,mg.GroupAltDesc,mg.L2ID,L2.L2Code" +
                            " from " +
                            " MaintGroup mg inner join employeesmaintgroups emg " +
                            " on mg.GroupID = emg.MaintGroupId Inner JOIN employees e " +
                            " on emg.EmployeeId = e.EmployeeID AND " +
                            " e.CategoryID = (Select  Case When ConfigValue is Not NULL THEN ConfigValue Else '' End As CategoryID from COnfigurations where ConfigKey = 'TechnicianCategory') " +
                            " LEFT OUTER JOIN L2 ON  L2.L2ID = mg.L2ID where 1=1 ";

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                string strWhere = " and (lower(mg.GroupCode) like N'%" + text + "%' or lower(mg.GroupDesc) like N'%" + text + "%' or lower(mg.GroupAltDesc) like N'%" + text + "%')";

                query += strWhere;
            }
            else
            {
                string strWhere = " and (mg.GroupCode like N'%" + text + "%' or mg.GroupDesc like N'%" + text + "%' or mg.GroupAltDesc like N'%" + text + "%')";

                query += strWhere;
            }


            SearchFieldService objSearchField = new SearchFieldService();
            string strWhereClause = string.Empty;
            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<MaintGroup>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }
    }
}
