﻿using CMMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Model;
using CMMS.Infrastructure;
using System.Collections.ObjectModel;
using Kendo.Mvc.UI;
using System.Data;

namespace CMMS.Service
{
    public class DivisionService : DBExecute
    {
        public DivisionService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<MainenanceDivision> GetMaintDivisionPermissionVise(int logedInEmpID, int empID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<MainenanceDivision> list = new List<MainenanceDivision>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });

                if (logedInEmpID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LOGEDINEMPID",
                        Value = logedInEmpID,
                        DBType = DbType.Int32
                    });

                    query = @"Select temp.*, Case When  temp.totalCnt = temp.permissionCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (
                                Select main.*,(Select count(1) totalCnt from MaintSubDept where maintDeptID IN
                                (Select maintDeptID From MaintenanceDepartment where MaintDivisionID = main.MaintDivisionID) 
                                 AND maintDeptID IN  
                                (Select  maintDeptID From MaintSubDept where MaintSubDeptID IN (select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                AND  MaintSubDeptID in (  select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID))) As totalCnt, 
                                (Select count(1) permissionCnt from MaintSubDept where maintDeptID IN(
                                Select maintDeptID From MaintenanceDepartment where maintDeptID IN (
                                Select  maintDeptID From MaintSubDept where MaintSubDeptID IN (
                                select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                ) AND MaintenanceDepartment.MaintDivisionID = main.MaintDivisionID) AND  MaintSubDeptID in (  select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                AND  MaintSubDeptID in (  select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)) As permissionCnt 
                                 from MainenanceDivision main 
                                 where MaintDivisionID IN (
                                    Select MaintDivisionID from MaintenanceDepartment where maintDeptID in 
                                    (Select MaintDeptID From MaintSubDept where MaintSubDeptID IN (
                                    select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID))) 
                                 ) temp";
                }
                else
                {
                    query = @"Select temp.*, Case When temp.permissionCnt = temp.totalCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (
                                Select main.*,(Select count(1) totalCnt from MaintSubDept where maintDeptID IN
                                (Select maintDeptID From MaintenanceDepartment where MaintDivisionID = main.MaintDivisionID)) As totalCnt, 
                                (Select count(1) permissionCnt from MaintSubDept where maintDeptID IN(
                                Select maintDeptID From MaintenanceDepartment where maintDeptID IN (
                                Select  maintDeptID From MaintSubDept where MaintSubDeptID IN (
                                select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                ) AND MaintenanceDepartment.MaintDivisionID = main.MaintDivisionID) AND  MaintSubDeptID in (  select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID) ) As permissionCnt 
                                 from MainenanceDivision main 
                                 ) temp";
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }
                else
                {
                    query += " Order by MaintDivisionID Asc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<MainenanceDivision>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<MaintenanceDepartment> GetMaintDeptPermissionVise(int logedInEmpID, int empID, int divisionID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<MaintenanceDepartment> list = new List<MaintenanceDepartment>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    if (logedInEmpID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "LOGEDINEMPID",
                            Value = logedInEmpID,
                            DBType = DbType.Int32
                        });

                        query = @"Select temp.*,Case When temp.permissionCnt = temp.totalCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (
                                Select main.*,
                                (Select count(1) totalCnt
                                    from MaintSubDept where maintDeptID IN
                                    (Select maintDeptID From MaintenanceDepartment where maintDeptID = main.maintDeptID) 
                                    AND MaintSubDept.MaintSubDeptID IN (select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID 
                                     union
                                    Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                 )As totalCnt,
                                (Select count(1) permissionCnt From MaintSubDept where MaintSubDept.MaintSubDeptID IN 
                                (select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                 AND MaintSubDept.maintDeptID = main.maintDeptID) As permissionCnt 
                                 from MaintenanceDepartment main 
                                Where main.maintDeptID IN
                                (
                                Select  maintDeptID From MaintSubDept where MaintSubDeptID IN (
                                select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                ) AND main.MaintDivisionID = " + divisionID + " ) temp ";
                    }
                    else
                    {
                        query = @"Select temp.*,Case When temp.permissionCnt = temp.totalCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (Select main.*,
                                    (Select count(1) totalCnt from 
                                          MaintSubDept 
                                            where MaintSubDept.MaintSubDeptID IN 
                                              ( 
                                              Select MaintSubDept.MaintSubDeptID from MaintenanceDepartment where  MaintenanceDepartment.maintDeptID = main.maintDeptID           
                                                UNION 
                                                Select MaintSubDeptID from employees where employeeId = 61) AND MaintSubDept.maintDeptID = main.maintDeptID)                                             
                                           As totalCnt,
                                (Select count(1) permissionCnt From MaintSubDept where MaintSubDept.MaintSubDeptID IN 
                                (select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                 AND MaintSubDept.maintDeptID = main.maintDeptID) As permissionCnt 
                                 from MaintenanceDepartment main Where main.MaintDivisionID =" + divisionID + " ) temp ";
                    }
                }
                else
                {
                    if (logedInEmpID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "LOGEDINEMPID",
                            Value = logedInEmpID,
                            DBType = DbType.Int32
                        });

                        query = @"Select temp.*,Case When temp.permissionCnt = temp.totalCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (
                                Select main.*,
                                (Select count(1) totalCnt from MaintSubDept where maintDeptID IN
                                (Select maintDeptID From MaintenanceDepartment where maintDeptID = main.maintDeptID) 
                                 AND MaintSubDept.MaintSubDeptID IN (select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                 )As totalCnt,
                                (Select count(1) permissionCnt From MaintSubDept where MaintSubDept.MaintSubDeptID IN 
                                (select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                 AND MaintSubDept.maintDeptID = main.maintDeptID) As permissionCnt 
                                 from MaintenanceDepartment main 
                                Where main.maintDeptID IN
                                (
                                Select  maintDeptID From MaintSubDept where MaintSubDeptID IN (
                                select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)
                                ) AND main.MaintDivisionID = " + divisionID + " ) temp ";
                    }
                    else
                    {
                        query = @"Select temp.*,Case When temp.permissionCnt = temp.totalCnt AND temp.totalCnt > 0 Then 1 Else 0 End As IsChecked from (Select main.*,
                                (Select count(1) totalCnt from 
                                        ( 
                                        Select MaintSubDeptID from  MaintSubDept where maintDeptID IN
                                            (Select maintDeptID From MaintenanceDepartment where maintDeptID = main.maintDeptID)
                                    UNION 
                                        Select MaintSubDeptID from  MaintSubDept where maintDeptID IN
                                            (Select MaintSubDeptID from employees where employeeId = @EMPID)
                                            AND maintDeptID = main.maintDeptID 
                                        ) a                                       
                                 )As totalCnt,
                                (Select count(1) permissionCnt From MaintSubDept where MaintSubDept.MaintSubDeptID IN 
                                (select MaintSubDeptID from Employees_MaintSubDept where empID = @EMPID union Select MaintSubDeptID from employees where employeeId = @EMPID)
                                 AND MaintSubDept.maintDeptID = main.maintDeptID) As permissionCnt 
                                 from MaintenanceDepartment main Where main.MaintDivisionID =" + divisionID + " ) temp ";
                    }
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }
                else
                {
                    query += " Order by temp.maintDeptID Asc";
                }
                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<MaintenanceDepartment>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IList<MaintSubDept> GetMaintSubDivisionPermissionVise(int DepartmentID)
        {
            try
            {
                IList<MaintSubDept> list = new List<MaintSubDept>();
                Collection<DBParameters> outParameters = new Collection<DBParameters>();

                outParameters.Add(new DBParameters()
                {
                    Name = "EmployeeID",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });

                outParameters.Add(new DBParameters()
                {
                    Name = "DepartmentID",
                    Value = DepartmentID,
                    DBType = DbType.Int32
                });

                string query = string.Empty;

                if (ProjectSession.IsCentral)
                {
                    query = "select * from MaintSubDept where MaintDeptID = @DepartmentID  Order by MaintSubDeptDesc ASC";
                }
                else
                {
                    query = @" Select * from ( Select ms.* From MaintSubDept ms inner join Employees_MaintSubDept em
                             on ms.MaintSubDeptID = em.MaintSubDeptID AND em.EMpID = @EmployeeID AND ms.MaintDeptID = @DepartmentID
                             union 
                             Select * from MaintSubDept Where MaintSubDeptID = 
                             (Select MaintSubDeptID from employees where employeeId = @EmployeeID) AND MaintDeptID = @DepartmentID)  temp Order by temp.MaintSubDeptDesc ASC";
                }
                using (DapperDBContext context = new DapperDBContext())
                {
                    list = context.ExecuteQuery<MaintSubDept>(query, outParameters).ToList();
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
