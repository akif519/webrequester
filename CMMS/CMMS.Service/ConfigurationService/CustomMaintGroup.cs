﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Service.ConfigurationService
{
    public class CustomMaintGroup : DBExecute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomMaintGroup"/> class.
        /// </summary>
        public CustomMaintGroup()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Gets the maint group employee information search.
        /// </summary>
        /// <param name="maintGroupId">The maint group identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public virtual List<employees> GetMaintGroupEmployeeInfoSearch(int? maintGroupId, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            string strEmployeeIDs = string.Empty;
            string query = string.Empty;
            string strWhereAuto = string.Empty;
            List<employees> employeeInfo;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "MaintGroupId",
                    Value = maintGroupId,
                    DBType = DbType.Int32
                });

                query = @"Select m.EmployeeID,m.EmployeeNo,m.Name,m.Positions from Employees m 
                            INNER JOIN employeesmaintgroups em ON em.EmployeeID = m.EmployeeID  WHERE MaintGroupId = @MaintGroupId";

                SearchFieldService objSearchField = new SearchFieldService();
                string strWhereClause = string.Empty;
                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhereAuto = " AND (lower(EmployeeNO) like N'%" + text + "%' or lower(Name) like N'%" + text + "%')";
                    query += strWhereAuto;
                }
                else 
                {
                    strWhereAuto = " AND (EmployeeNO like N'%" + text + "%' or Name like N'%" + text + "%')";
                    query += strWhereAuto;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    /// This Name will Work For this case too. This is no Advance Search but it's normal search
                    employeeInfo = new List<employees>(objDapperContext.AdvanceSearch<employees>(query, parameters, pageNo));
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
                return employeeInfo;
            }

            finally
            {

            }
        }

        /// <summary>
        /// Inserts the update employee maint group.
        /// </summary>
        /// <param name="objEmployeesmaintgroup">The object employeesmaintgroup.</param>
        /// <returns></returns>
        public bool InsertUpdateEmployeeMaintGroup(Employeesmaintgroup objEmployeesmaintgroup)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "MaintGroupId",
                    Value = objEmployeesmaintgroup.MaintGroupId,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = objEmployeesmaintgroup.EmployeeId,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedBy",
                    Value = objEmployeesmaintgroup.CreatedBy,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedDate",
                    Value = objEmployeesmaintgroup.CreatedDate,
                    DBType = DbType.DateTime
                });

                strQuery = @"INSERT INTO EmployeesMaintGroups(MaintGroupId,EmployeeID,CreatedBy,CreatedDate) VALUES(@MaintGroupId,@EmployeeId,@CreatedBy,@CreatedDate)";

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Deletes the employee maint group.
        /// </summary>
        /// <param name="maintGroupID">The maint group identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public bool DeleteEmployeeMaintGroup(int maintGroupID, int employeeId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "MaintGroupId",
                    Value = maintGroupID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = employeeId,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From EmployeesMaintGroups where MaintGroupId = @MaintGroupId and EmployeeId = @EmployeeId";

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

    }
}
