﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class L2Service : DBExecute
    {
        public static List<L2> GetCitiesByEmployee(int empID, int? cityID, int? orgID)
        {
            using (DapperDBContext context = new ServiceContext())
            {
                int? status = 0;
                string query = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (cityID > 0)
                {
                    L2 objL2Check = new L2();
                    status = context.SelectObject<L2>(Convert.ToInt32(cityID)).Status;
                    if (status == 1)
                    {
                        if (orgID > 0)
                        {
                            query = "select L2.* from L2 " +
                                   " inner join  employees on employees.L2ID = L2.L2ID " +
                                    " where employees.EmployeeID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID +
                                    " union " +
                                    " select L2.* from L2 " +
                                    " inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                    " where Employees_L2.empID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID;
                        }
                        else
                        {
                            query = "select L2.* from L2 " +
                                     " inner join  employees on employees.L2ID = L2.L2ID " +
                                      " where employees.EmployeeID = " + empID + " and L2.status = 1 " +
                                      " union " +
                                      " select L2.* from L2 " +
                                      " inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                      " where Employees_L2.empID = " + empID + " and L2.status = 1";
                        }

                    }
                    else
                    {
                        if (orgID > 0)
                        {
                            query = "select L2.* from L2 " +
                                    "inner join  employees on employees.L2ID = L2.L2ID " +
                                     "where employees.EmployeeID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID +
                                     "union " +
                                     "select L2.* from L2 " +
                                     "inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                     "where Employees_L2.empID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID +
                                     "union " +
                                     "select L2.* from L2 WHERE L2.L2ID = " + cityID + " and L2.L1ID = " + orgID;
                        }
                        else
                        {
                            query = "select L2.* from L2 " +
                                    "inner join  employees on employees.L2ID = L2.L2ID " +
                                     "where employees.EmployeeID = " + empID + " and L2.status = 1 " +
                                     "union " +
                                     "select L2.* from L2 " +
                                     "inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                     "where Employees_L2.empID = " + empID + " and L2.status = 1 " +
                                     "union " +
                                     "select L2.* from L2 WHERE L2.L2ID = " + cityID;
                        }

                    }
                }
                else
                {
                    if (orgID > 0)
                    {
                        query = "select L2.* from L2 " +
                                   "inner join  employees on employees.L2ID = L2.L2ID " +
                                    "where employees.EmployeeID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID +
                                    "union " +
                                    "select L2.* from L2 " +
                                    "inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                    "where Employees_L2.empID = " + empID + " and L2.status = 1 and L2.L1ID = " + orgID;
                    }
                    else
                    {
                        query = "select L2.* from L2 " +
                                   "inner join  employees on employees.L2ID = L2.L2ID " +
                                    "where employees.EmployeeID = " + empID + " and L2.status = 1 " +
                                    "union " +
                                    "select L2.* from L2 " +
                                    "inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                    "where Employees_L2.empID = " + empID + " and L2.status = 1 ";
                    }

                }
                return context.ExecuteQuery<L2>(query, parameters).ToList();
            }
        }

        public IList<Employees_L2> GetCityByPermissionAndL1ID(int logedInEmpID, int empID, int orgID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<Employees_L2> list = new List<Employees_L2>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "L1ID",
                    Value = orgID,
                    DBType = DbType.Int32
                });

                if (logedInEmpID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LOGEDINEMPID",
                        Value = logedInEmpID,
                        DBType = DbType.Int32
                    });

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query += "Select * from ( ";
                    }

                    query += "select (case isnull((Select employees_L2.L2Id from employees_L2 where employees_L2.L2ID=L2.L2ID " +
                                    "and employees_L2.empID = @EMPID),0)  when 0  then 0 else 1 end) as ChkL2, L2.* from L2 " +
                                     "inner join  employees on employees.L2ID = L2.L2ID " +
                                      "where employees.EmployeeID = @LOGEDINEMPID and L2.status = 1  And L1ID = @L1ID " +
                                      "union " +
                                      "select (case isnull((Select employees_L2.L2Id from employees_L2 where employees_L2.L2ID=L2.L2ID " +
                                       "and employees_L2.empID = @EMPID),0)  when 0  then 0 else 1 end) as ChkL2,L2.* from L2 " +
                                      "inner join  Employees_L2 on Employees_L2.L2ID = L2.L2ID  " +
                                      "where Employees_L2.empID = @LOGEDINEMPID and L2.status = 1  And L1ID = @L1ID ";

                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query += ")";
                    }
                }
                else
                {
                    query = "Select (case isnull((Select employees_L2.L2Id from employees_L2 where employees_L2.L2ID=L2.L2ID " +
                                    "and employees_L2.empID = @EMPID),0)  when 0  then 0 else 1 end) as ChkL2,L2.* from L2 where L2.status = 1 AND L2.L1ID = @L1ID";
                }

                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Employees_L2>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }


                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }

        //public static List<L2> GetCitiesByEmployee(int empID, int cityID)
        //{
        //    using (DapperContext context = new DapperContext())
        //    {
        //        L2 objL2 = new L2();
        //        employees objemployees = new employees();
        //        Employees_L2 objEmployees_L2 = new Employees_L2();

        //        //Method 1
        //        List<L2> obj = context.SearchAll(objL2).Join(context.SearchAll(objemployees), c => c.L2ID, p => p.L2ID, (c, p) => new L2() { L2ID = c.L2ID, L2name = c.L2name, L2Code = c.L2Code, L2Altname = c.L2Altname, EmployeeID = c.EmployeeID }).Where(x => x.EmployeeID == empID)
        //              .Union(context.SearchAll(objL2).Join(context.SearchAll(objEmployees_L2), c => c.L2ID, p => p.L2ID, (c, p) => new L2() { L2ID = c.L2ID, L2name = c.L2name, L2Code = c.L2Code, L2Altname = c.L2Altname, EmployeeID = p.EmpID }).Where(x => x.EmployeeID == empID)).ToList();

        //        return obj;

        //        //Method 2
        //        //List<L2> obj2 = (from c in context.SearchAll(objL2)
        //        //                 join o in context.SearchAll(objEmployees_L2)
        //        //                 on c.L2ID equals o.L2ID
        //        //                 where o.EmpID == empID
        //        //                 select c).Union(from c in context.SearchAll(objL2)
        //        //                                 join o in context.SearchAll(objemployees)
        //        //                                 on c.L2ID equals o.L2ID
        //        //                                 where o.EmployeeID == empID
        //        //                                 select c).ToList();

        //        //return obj2;

        //        //Method 3
        //        //var Test = (from c in context.SearchAll(objL2)
        //        //            join o in context.SearchAll(objEmployees_L2)
        //        //            on c.L2ID equals o.L2ID
        //        //            where o.EmpID == empID
        //        //            select c).Union(from c in context.SearchAll(objL2)
        //        //                            join o in context.SearchAll(objemployees)
        //        //                            on c.L2ID equals o.L2ID
        //        //                            where o.EmployeeID == empID
        //        //                            select c);

        //        //List<L2> obj3 = new List<L2>();
        //        //obj3.AddRange(Test.ToList());
        //        //return obj3;
        //    }


        //}
    }
}
