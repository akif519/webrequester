﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Service.ConfigurationService
{
    public class CurrencyService : DBExecute
    {
        public CurrencyService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<Currency> GetCurrencys(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            IList<Currency> list = new List<Currency>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select * from Currency ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhereAuto = " Where (lower(CurrencyCode) like N'%" + text + "%' or lower(CurrencyDescription) like N'%" + text + "%' or lower(AltCurrencyDescription) like N'%" + text + "%')";
                query += strWhereAuto;
            }
            else 
            {
                strWhereAuto = " Where (CurrencyCode like N'%" + text + "%' or CurrencyDescription like N'%" + text + "%' or AltCurrencyDescription like N'%" + text + "%')";
                query += strWhereAuto;
            }
            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Currency>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }
    }
}
