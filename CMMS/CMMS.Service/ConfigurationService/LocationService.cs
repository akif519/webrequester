﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service.ConfigurationService
{
    public class LocationService : DBExecute
    {
        public LocationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public List<Location> GetLocationBySite(int? siteID, int pageNo, string sortExpression, string sortDirection)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "siteID",
                Value = siteID,
                DBType = DbType.Int32
            });

            parameters.Add(new DBParameters()
            {
                Name = "EmployeeID",
                Value = ProjectSession.EmployeeID,
                DBType = DbType.Int32
            });

            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            parameters.Add(new DBParameters()
            {
                Name = "SortExpression",
                DBType = DbType.String,
                Value = sortExpression

            });
            parameters.Add(new DBParameters()
            {
                Name = "SortDirection",
                DBType = DbType.String,
                Value = sortDirection

            });
            List<Location> employeeInfo = this.ExecuteProcedure<Location>("UspGetLocation", parameters).ToList();
            return employeeInfo;
        }

        public IList<Location> GetLocationPage(string strWhere, int empId, bool IsCentral, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Location> list = new List<Location>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "EmpID",
                Value = empId,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "LocationID";
            }

            string strWhereClauseWithEmployee = string.Empty;

            if (IsCentral)
            {
                empId = 0;
            }

            if (empId > 0)
            {
                strWhereClauseWithEmployee = " and location.L2ID in (select L2Id from employees_L2 where empID = @EmpID union Select L2Id from employees where employeeId = @EmpID ) ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    strWhereClauseWithEmployee += " And (location.L3ID IN (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + " ) OR (location.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    strWhereClauseWithEmployee += " and location.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            string query = "Select location.LocationID ,location.L2ID,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName,L1.IsAreaEnable As IsAreaEnableOrgWise,L2.L1ID,L2.L2Code,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description,L5.L5AltDescription,c.Criticality,L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class,location.L3ID,location.L4ID,location.L5ID,location.CriticalityID,location.LocationNo,L3.L3No,L3.L3Desc,L3.L3AltDesc, "
                            + " location.LocationDescription,location.NotetoTech,location.LocationAltDescription,location.CreatedBy,location.Longitude,location.Latitude, "
                            + " location.CreatedDate,location.ModifiedBy,location.ModifiedDate,location.CleaningClassificationId,location.LocationTypeID,LocationType.LocationTypeCode,location.EmployeeID,e2.Name,e2.Name As LocationAuthEmployeeName, e2.AltName,e2.EmployeeNO  FROM  location "
                            + " left outer join L2 on location.L2ID = L2.L2ID  "
                            + " left outer join L1 on L2.L1ID = L1.L1ID  "
                            + " Inner JOIN Sector ON Sector.SectorID = L2.SectorID "
                            + " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID "
                            + " LEFT OUTER JOIN LocationType ON LocationType.LocationTypeID = location.LocationTypeID "
                            + " left outer join L3 on L3.L3ID = location.L3ID "
                            + " Left OUTER JOIN employees e2 ON location.EmployeeID = e2.EmployeeID "
                            + " left outer join L4 on L4.L4ID = location.L4ID left outer join L5 on L5.L5ID = location.L5ID Left join Criticality c on c.id = location.CriticalityID where 1=1 " + strWhere + strWhereClauseWithEmployee;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Location>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }


        //public IList<Location> GetAllLocationDropDown(int empId, bool IsCentral)
        //{
        //    IList<Location> list = new List<Location>();
        //    Collection<DBParameters> parameters = new Collection<DBParameters>();

        //    parameters.Add(new DBParameters()
        //    {
        //        Name = "EmpID",
        //        Value = empId,
        //        DBType = DbType.Int32
        //    });

        //    if (IsCentral)
        //    {
        //        empId = 0;
        //    }

        //    string query = "SELECT l.LocationID,l.LocationNo,l.LocationDescription,l.LocationAltDescription FROM location l " +
        //                    "JOIN employees_L2 el ON el.L2ID = l.L2ID "+
        //                    "WHERE el.empID = @empId " +
        //                    "UNION " +
        //                    "SELECT l.LocationID,l.LocationNo,l.LocationDescription,l.LocationAltDescription FROM location l " +
        //                    "JOIN employees c ON c.L2ID  = l.L2ID " +
        //                    "WHERE c.EmployeeID = @empId ";

        //    using (DapperContext objDapperContext = new DapperContext())
        //    {
        //        list = objDapperContext.ExecuteQuery<Location>(query, parameters);
        //        this.PagingInformation = objDapperContext.PagingInformation;
        //    }

        //    return list;
        //}

        #region "Linked Locations"

        public IList<Location> GetUnLinkedLocationPage(int cityID, int parentLocationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Location> list = new List<Location>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string query = string.Empty;

            parameters.Add(new DBParameters()
            {
                Name = "cityID",
                Value = cityID,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "LocationID";
            }

            if (ProjectSession.IsCentral)
            {
                query = "Select location.LocationID ,location.L2ID,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName,L2.L2Code,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description,L5.L5AltDescription,c.Criticality,L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class,location.L3ID,location.L4ID,location.L5ID,location.CriticalityID,location.LocationNo,L3.L3No,L3.L3Desc,L3.L3AltDesc, "
                           + " location.LocationDescription,location.NotetoTech,location.LocationAltDescription,location.CreatedBy,location.Longitude,location.Latitude, "
                           + " location.CreatedDate,location.ModifiedBy,location.ModifiedDate,location.CleaningClassificationId,location.LocationTypeID,LocationType.LocationTypeCode,location.EmployeeID,e2.Name,e2.AltName,e2.EmployeeNO  from Location"
                         + " Inner Join L2"
                        + " On Location.L2ID = L2.L2ID "
                        + " AND Location.L2ID = @cityID"
                        + " AND Location.LocationID Not IN ( "
                        + " Select LinkedLocationID from LinkedLocation Union Select " + parentLocationID + " from Location Where LocationID = " + parentLocationID + " )"
                        + " Inner JOIN Sector ON Sector.SectorID = L2.SectorID "
                          + " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID "
                          + " LEFT OUTER JOIN LocationType ON LocationType.LocationTypeID = location.LocationTypeID "
                          + " left outer join L3 on L3.L3ID = location.L3ID "
                          + " Left OUTER JOIN employees e2 ON location.EmployeeID = e2.EmployeeID "
                          + " left outer join L4 on L4.L4ID = location.L4ID left outer join L5 on L5.L5ID = location.L5ID Left join Criticality c on c.id = location.CriticalityID where 1=1 ";
            }
            else
            {
                string LocationFilter = string.Empty;
                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    LocationFilter = @" Inner Join Employees_Location 
                                                on Location.LocationID = Employees_Location.LocationID
                                                AND Employees_Location.EmpID = " + ProjectSession.EmployeeID;
                }

                query = "Select location.LocationID ,location.L2ID,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName,L2.L2Code,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description,L5.L5AltDescription,c.Criticality,L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class,location.L3ID,location.L4ID,location.L5ID,location.CriticalityID,location.LocationNo,L3.L3No,L3.L3Desc,L3.L3AltDesc, "
                           + " location.LocationDescription,location.NotetoTech,location.LocationAltDescription,location.CreatedBy,location.Longitude,location.Latitude, "
                           + " location.CreatedDate,location.ModifiedBy,location.ModifiedDate,location.CleaningClassificationId,location.LocationTypeID,LocationType.LocationTypeCode,location.EmployeeID,e2.Name,e2.AltName,e2.EmployeeNO  FROM  location "
                           + @"Inner Join L2 
                                On Location.L2ID = L2.L2ID 
                                AND Location.L2ID = @cityID
                                Inner Join Employees_L2
                                On Location.L2ID = employees_L2.L2ID
                                AND employees_L2.empID = " + ProjectSession.EmployeeID
                           + LocationFilter
                           + " AND Location.LocationID Not IN ( Select LinkedLocationID from LinkedLocation Union Select " + parentLocationID + " from Location Where LocationID = " + parentLocationID + ")"
                           + " Inner JOIN Sector ON Sector.SectorID = L2.SectorID "
                           + " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID "
                           + " LEFT OUTER JOIN LocationType ON LocationType.LocationTypeID = location.LocationTypeID "
                           + " left outer join L3 on L3.L3ID = location.L3ID "
                           + " Left OUTER JOIN employees e2 ON location.EmployeeID = e2.EmployeeID "
                           + " left outer join L4 on L4.L4ID = location.L4ID left outer join L5 on L5.L5ID = location.L5ID Left join Criticality c on c.id = location.CriticalityID where 1=1 ";
            }

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Location>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<Location> GetLinkedLocations(int locationID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Location> list = new List<Location>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "LocationID",
                Value = locationID,
                DBType = DbType.Int32
            });

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "LocationID";
            }

            string query = string.Empty;

            query = @"Select Location.* from LinkedLocation
                        Inner Join Location on Location.LocationID = LinkedLocation.LinkedLocationID
                        AND LinkedLocation.LocationID = @LocationID";


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Location>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public static int GetLinkedLocationsCount(int locationID)
        {
            int count = 0;


            string query = "Select COUNT(*) As LinkedLocationCount from LinkedLocation " +
                   " Inner Join Location on Location.LocationID = LinkedLocation.LinkedLocationID " +
                   " AND LinkedLocation.LocationID =" + locationID;

            using (DapperDBContext context = new DapperDBContext())
            {
                count = Convert.ToInt32(context.ExecuteScalar(query));
                return count;
            }

        }

        public bool DeleteLinkedLocationByList(List<int> lstLocations, int parentLocationID)
        {
            string strSQL = " Delete from LinkedLocation Where LocationID = " + parentLocationID;
            StringBuilder strDeleteList = new StringBuilder();
            int result = -1;

            try
            {
                foreach (int locationID in lstLocations)
                {
                    if (strDeleteList.Length == 0)
                    {
                        strDeleteList.Append(locationID);
                    }
                    else
                    {
                        strDeleteList.Append("," + locationID);
                    }
                }

                strSQL += " And  LinkedLocationID in (" + strDeleteList.ToString() + ")";

                if (strDeleteList.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertLinkedLocationByList(List<int> lstLocation, int parentLocationID)
        {
            string strSQL = " insert into LinkedLocation (LocationID,LinkedLocationID,CreatedBy,CreatedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int empID = 0;
            int result = -1;

            string oraFrom = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";
            }

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });

            try
            {
                foreach (int locationID in lstLocation)
                {
                    if (strInsertValues.Length == 0)
                    {

                        strInsertValues.Append(" Select " + parentLocationID + "," + locationID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                    else
                    {
                        strInsertValues.Append("  Union All Select " + parentLocationID + "," + locationID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                }

                if (parentLocationID > 0)
                {
                    DeleteLinkedLocationByList(lstLocation, parentLocationID);
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteLinkedLocationByparentLocationID(int parentLocationID, int locationID)
        {
            string strSQL = " Delete from LinkedLocation Where LocationID = " + parentLocationID;
            int result = -1;

            try
            {
                if (locationID > 0)
                {
                    strSQL += " And LinkedLocationID in (" + locationID + ")";
                }

                if (parentLocationID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
