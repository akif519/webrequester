﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;

namespace CMMS.Service
{
    public class AccountCodeService : DBExecute
    {
        public AccountCodeService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<Accountcode> GetAccountCodes(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            IList<Accountcode> list = new List<Accountcode>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select AccountCode.*,CostCenter.CostCenterNo,CostCenter.CostCenterName,CostCenter.AltCostCenterName " +
                            " from AccountCode inner join CostCenter on CostCenter.CostCenterId = AccountCode.CostCenterId ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhereAuto = " Where (lower(AccountCode) like N'%" + text + "%' or lower(AccountCodeDesc) like N'%" + text + "%' or lower(AltAccountCodeDesc) like N'%" + text + "%')";
                query += strWhereAuto;
            }
            else 
            {
                strWhereAuto = " Where (AccountCode like N'%" + text + "%' or AccountCodeDesc like N'%" + text + "%' or AltAccountCodeDesc like N'%" + text + "%')";
                query += strWhereAuto;
            }
            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Accountcode>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }
    }
}
