﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Service.ConfigurationService
{
    public class CustomJobPlan : DBExecute
    {
        public CustomJobPlan()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Inserts the update job plan item.
        /// </summary>
        /// <param name="objTblJobPlanItem">The object table job plan item.</param>
        /// <returns></returns>
        public bool InsertUpdateJobPlanItem(TblJobPlanItem objTblJobPlanItem)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "JobPlanID",
                    Value = objTblJobPlanItem.JobPlanID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqID",
                    Value = objTblJobPlanItem.SeqID,
                    DBType = DbType.Decimal
                });
                parameters.Add(new DBParameters()
                {
                    Name = "Description",
                    Value = objTblJobPlanItem.Description,
                    DBType = DbType.String
                });
                parameters.Add(new DBParameters()
                {
                    Name = "AltDescription",
                    Value = objTblJobPlanItem.AltDescription,
                    DBType = DbType.String
                });

                if (objTblJobPlanItem.HdnSeqID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objTblJobPlanItem.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objTblJobPlanItem.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"UPDATE tblJobPlanItems SET Description = @Description, AltDescription = @AltDescription , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  where JobPlanID = @JobPlanID and SeqID = " + objTblJobPlanItem.HdnSeqID;
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = objTblJobPlanItem.CreatedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = objTblJobPlanItem.CreatedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"INSERT INTO tblJobPlanItems(JobPlanID,SeqID,Description,AltDescription,CreatedBy,CreatedDate) VALUES(@JobPlanID,@SeqID,@Description,@AltDescription,@CreatedBy,@CreatedDate)";
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Gets the job plan item by plan identifier.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<TblJobPlanItem> GetJobPlanItemByPlanID(int jobPlanId, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<TblJobPlanItem> list = new List<TblJobPlanItem>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "SELECT JobPlanID, SeqID, Description, AltDescription " +
                           "FROM tblJobPlanItems " +
                           "WHERE JobPlanID = " + jobPlanId;

            SearchFieldService objSearchField = new SearchFieldService();
            string strWhereClause = string.Empty;
            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<TblJobPlanItem>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Deletes the job plan item.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <param name="seqId">The seq identifier.</param>
        /// <returns></returns>
        public bool DeleteJobPlanItem(int jobPlanId, int seqId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "JobPlanID",
                    Value = jobPlanId,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "SeqID",
                    Value = seqId,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From tblJobPlanItems where JobPlanID = @JobPlanID and SeqID = @SeqID";

                objDapperContext.ExecuteQuery(strQuery, parameters);
                UpdateSeqID(jobPlanId);
            }
            return true;
        }

        /// <summary>
        /// Updates the seq identifier.
        /// </summary>
        /// <param name="jobplanId">The jobplan identifier.</param>
        /// <returns></returns>
        public bool UpdateSeqID(int jobplanId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    strQuery = @";WITH cte AS(SELECT *, ROW_NUMBER() OVER(ORDER BY SeqID) Rno FROM dbo.tblJobPlanItems WHERE JobPlanID = " + jobplanId + ")" +
                                "UPDATE cte SET SeqID = cte.Rno";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    strQuery = @"UPDATE tblJobPlanItems as c inner JOIN(SELECT *,  @rownum := @rownum + 1 AS rank FROM tblJobPlanItems,(SELECT @rownum := 0) r WHERE JobPlanID = " + jobplanId + ") as cte " +
                               "on cte.JobPlanID = c.JobPlanID and cte.SeqID = c.SeqID SET c.SeqID = cte.rank";
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    strQuery = @"UPDATE tblJobPlanItems c SET(c.SEQID) = (SELECT ROW_NUMBER() OVER ( ORDER BY SeqID) rno FROM   tblJobPlanItems cte WHERE  cte.JobPlanID = c.JobPlanID and cte.SeqID = c.SeqID) " +
                                "Where c.JobPlanID = " + jobplanId;
                }
                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        public bool SaveJobPlanItemInsert(int JobPlanID, List<TblJobPlanItem> lstJobPlanItems)
        {
            try
            {
                string queryDelete = "Delete from tblJobPlanItems Where JobPlanID = '" + JobPlanID + "'";
                string queryInsert = string.Empty;

                Jobtask objJobTask = new Jobtask();
                using (ServiceContext objContext = new ServiceContext())
                {
                    objContext.ExecuteQuery(queryDelete, new Collection<DBParameters>());

                    lstJobPlanItems.ForEach(p =>
                    {
                        Collection<DBParameters> inParameters = new Collection<DBParameters>();
                        inParameters.Add(new DBParameters()
                        {
                            Name = "CreatedDate",
                            Value = p.CreatedDate,
                            DBType = DbType.DateTime
                        });

                        inParameters.Add(new DBParameters()
                        {
                            Name = "ModifiedDate",
                            Value = p.ModifiedDate,
                            DBType = DbType.DateTime
                        });

                        queryInsert = "Insert Into tblJobPlanItems(JobPlanID,SeqID,Description,AltDescription,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)"
                                + " Values('" + JobPlanID + "'," + p.SeqID + ",'" + p.Description + "',N'" + p.AltDescription + "','" + p.CreatedBy + "',@CreatedDate,'" + p.ModifiedBy + "',@ModifiedDate )";
                        objContext.ExecuteQuery(queryInsert, inParameters);
                    }
                        );
                }

                return true;
            }
            catch (Exception ex)
            { throw ex; }
        }


    }
}
