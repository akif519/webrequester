﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using CMMS.Model;
using System.Collections.ObjectModel;
using System.Data;
using Kendo.Mvc.UI;
using Kendo.Mvc;
using CMMS.Infrastructure;

namespace CMMS.Service
{
    public class BoMService : DBExecute
    {
        public BoMService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<PartsListDetail> GetBoMDetailsByBoMId(int partID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<PartsListDetail> list = new List<PartsListDetail>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "SELECT pld.StockID, pld.PartsListID,sc.StockNo,sc.StockDescription,sc.AltStockDescription,pld.PartsQty " +
                           "FROM partslistdetails pld " +
                           "INNER JOIN stockcode sc ON pld.StockID = sc.StockID " +
                           "WHERE  pld.PartsListID = " + partID;

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<PartsListDetail>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<StockCode> GetAllItemDetail(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            IList<StockCode> list = new List<StockCode>();
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "SELECT s.StockID,s.CriticalityID,c.Criticality,s.AttrGroupID,a.AttrName,s.AttrSubID,a2.AttrSubName,s.StockNo,s.StockDescription,s.AltStockDescription,s.Price2,s.Manufacturer,s.Status,s.Specification " +
                           "FROM stockcode s " +
                           "LEFT OUTER JOIN criticality c ON c.id = s.CriticalityID " +
                           "LEFT OUTER JOIN attrgroup a ON a.AttrID = s.AttrGroupID " +
                           "LEFT OUTER JOIN attrsubgroup a2 ON a2.AttrSubID = s.AttrSubID ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhereAuto = " Where (lower(StockNo) like N'%" + text + "%' or lower(StockDescription) like N'%" + text + "%' or  lower(AltStockDescription) like N'%" + text + "%')";
                query += strWhereAuto;
            }
            else
            {
                strWhereAuto = " Where (StockNo like N'%" + text + "%' or StockDescription like N'%" + text + "%' or  AltStockDescription like N'%" + text + "%')";
                query += strWhereAuto;
            }
            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<StockCode>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        /// <summary>
        /// Inserts the update part list item.
        /// </summary>
        /// <param name="objPartListDetail">The object part list detail.</param>
        /// <returns></returns>
        public bool InsertUpdatePartListItem(PartsListDetail objPartListDetail)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "PartsListID",
                    Value = objPartListDetail.PartsListID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "StockID",
                    Value = objPartListDetail.StockID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "PartsQty",
                    Value = objPartListDetail.PartsQty,
                    DBType = DbType.Decimal
                });

                if (objPartListDetail.hdnPartsListDetailID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedBy",
                        Value = objPartListDetail.ModifiedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "ModifiedDate",
                        Value = objPartListDetail.ModifiedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"UPDATE partslistdetails SET PartsQty = @PartsQty , ModifiedBy = @ModifiedBy , ModifiedDate = @ModifiedDate  where PartsListID = @PartsListID and StockID = @StockID";
                }
                else
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedBy",
                        Value = objPartListDetail.CreatedBy,
                        DBType = DbType.String
                    });

                    parameters.Add(new DBParameters()
                    {
                        Name = "CreatedDate",
                        Value = objPartListDetail.CreatedDate,
                        DBType = DbType.DateTime
                    });

                    strQuery = @"INSERT INTO partslistdetails(PartsListID,StockID,PartsQty,CreatedBy,CreatedDate) VALUES(@PartsListID,@StockID,@PartsQty,@CreatedBy,@CreatedDate)";
                }

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }

        /// <summary>
        /// Deletes the part list item.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="partsListID">The parts list identifier.</param>
        /// <returns></returns>
        public bool DeletePartListItem(int stockID, int partsListID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "PartsListID",
                    Value = partsListID,
                    DBType = DbType.Int32
                });
                parameters.Add(new DBParameters()
                {
                    Name = "StockID",
                    Value = stockID,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From partslistdetails where PartsListID = @PartsListID and StockID = @StockID";

                objDapperContext.ExecuteQuery(strQuery, parameters);
            }
            return true;
        }
    }
}
