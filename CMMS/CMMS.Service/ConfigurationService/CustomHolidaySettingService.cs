﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Service.ConfigurationService
{
    public class CustomHolidaySettingService : DBExecute
    {
        public CustomHolidaySettingService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<WorkingDay> GetWorkingDay(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<WorkingDay> list = new List<WorkingDay>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = string.Empty;

            query += "SELECT wd.*,l.L2Code,l.L2name,l.L2Altname,l1.L1No,l1.L1Name,l1.L1AltName FROM WorkingDays wd ";
            query += "INNER JOIN L2 l ON l.L2ID = wd.L2ID  ";
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                query += " AND l.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") " ;
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            query += "LEFT OUTER JOIN L1 l1 ON l1.L1ID = wd.L1ID ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkingDay>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<Holiday> GetHoliday(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Holiday> list = new List<Holiday>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = string.Empty;

            query += "SELECT h.*,l.L2Code,l.L2name,l.L2Altname,l1.L1No,l1.L1Name,l1.L1AltName FROM Holidays h ";
            query += "INNER JOIN L2 l ON l.L2ID = h.L2ID  ";
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                query += " AND l.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            query += "LEFT OUTER JOIN L1 l1 ON l1.L1ID = h.L1ID ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Holiday>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<WorkingHour> GetWorkingHour(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<WorkingHour> list = new List<WorkingHour>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            string query = string.Empty;

            query += "SELECT w.*,l.L2Code,l.L2name,l.L2Altname,l1.L1No,l1.L1Name,l1.L1AltName FROM WorkingHours w ";
            query += "INNER JOIN L2 l ON l.L2ID = w.L2ID  ";
            query += "LEFT OUTER JOIN L1 l1 ON l1.L1ID = w.L1ID ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<WorkingHour>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public WorkingDayHour GetWorkingDayHoursByID(int workingdayId)
        {
            try
            {
                WorkingDay objWorkingDay = new WorkingDay();
                List<WorkingDayHour> lstDay = new List<WorkingDayHour>();
                WorkingDayHour objWorkingDayHour = new WorkingDayHour();
                if (workingdayId > 0)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        lstDay = context.SearchAll(new WorkingDayHour()).Where(o => o.WorkingDayID == workingdayId).ToList();
                        foreach (var lst in lstDay)
                        {
                            if (lst.DayID == 1)
                            {
                                objWorkingDayHour.StartTime1 = lst.StartTime;
                                objWorkingDayHour.EndTime1 = lst.EndTime;
                                objWorkingDayHour.Monday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 2)
                            {
                                objWorkingDayHour.StartTime2 = lst.StartTime;
                                objWorkingDayHour.EndTime2 = lst.EndTime;
                                objWorkingDayHour.Tuesday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 3)
                            {
                                objWorkingDayHour.StartTime3 = lst.StartTime;
                                objWorkingDayHour.EndTime3 = lst.EndTime;
                                objWorkingDayHour.Wednesday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 4)
                            {
                                objWorkingDayHour.StartTime4 = lst.StartTime;
                                objWorkingDayHour.EndTime4 = lst.EndTime;
                                objWorkingDayHour.Thursday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 5)
                            {
                                objWorkingDayHour.StartTime5 = lst.StartTime;
                                objWorkingDayHour.EndTime5 = lst.EndTime;
                                objWorkingDayHour.Friday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 6)
                            {
                                objWorkingDayHour.StartTime6 = lst.StartTime;
                                objWorkingDayHour.EndTime6 = lst.EndTime;
                                objWorkingDayHour.Saturday = lst.IsWorkingDay;
                            }
                            else if (lst.DayID == 7)
                            {
                                objWorkingDayHour.StartTime7 = lst.StartTime;
                                objWorkingDayHour.EndTime7 = lst.EndTime;
                                objWorkingDayHour.Sunday = lst.IsWorkingDay;
                            }

                        }
                    }
                }
                return objWorkingDayHour;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveWorkingDayHour(WorkingDay objWorkingDay)
        {
            try
            {
                using (DapperContext objContext = new DapperContext())
                {
                    int WorkingdayID = objWorkingDay.WorkingDayID;
                    if (WorkingdayID > 0)
                    {
                        var result = objContext.SearchAll(new WorkingDayHour()).Where(o => o.WorkingDayID == WorkingdayID).ToList();
                        for (int i = 1; i <= 7; i++)
                        {
                            WorkingDayHour objWorkingDayHour = new WorkingDayHour();
                            objWorkingDayHour.WorkingDayID = WorkingdayID;
                            WorkingDayHour objCurrentWorkingDayHour = new WorkingDayHour();
                            if (i == 1)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Monday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime1;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime1;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Monday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 2)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Tuesday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime2;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime2;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Tuesday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 3)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Wednesday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime3;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime3;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Wednesday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 4)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Thursday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime4;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime4;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Thursday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 5)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Friday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime5;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime5;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Friday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 6)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Saturday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime6;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime6;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Saturday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }
                            else if (i == 7)
                            {
                                objWorkingDayHour.DayID = @SystemEnum.WeekDays.Sunday.GetHashCode();
                                objWorkingDayHour.StartTime = objWorkingDay.objWorkingDayHour.StartTime7;
                                objWorkingDayHour.EndTime = objWorkingDay.objWorkingDayHour.EndTime7;
                                objWorkingDayHour.IsWorkingDay = objWorkingDay.objWorkingDayHour.Sunday;
                                objCurrentWorkingDayHour = result.Where(o => o.DayID == i).FirstOrDefault();
                            }

                            if (objCurrentWorkingDayHour == null)
                            {
                                objWorkingDayHour.CreatedBy = ConvertTo.String(ProjectSession.EmployeeID);
                                objWorkingDayHour.CreatedDate = DateTime.Now;
                                if (!objWorkingDayHour.IsWorkingDay)
                                {
                                    objWorkingDayHour.StartTime = null;
                                    objWorkingDayHour.EndTime = null;
                                }

                                objContext.Save(objWorkingDayHour);
                            }
                            else
                            {
                                objWorkingDayHour.ModifiedBy = ConvertTo.String(ProjectSession.EmployeeID);
                                objWorkingDayHour.ModifiedDate = DateTime.Now;
                                objWorkingDayHour.WorkingDayHourID = objCurrentWorkingDayHour.WorkingDayHourID;

                                if (!objWorkingDayHour.IsWorkingDay)
                                {
                                    objWorkingDayHour.StartTime = null;
                                    objWorkingDayHour.EndTime = null;
                                }

                                objContext.Save(objWorkingDayHour);
                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveHoliday(Holiday objHoliday)
        {
            List<Holiday> lstHoliday = new List<Holiday>();
            Holiday objOldHoliday = new Holiday();
            try
            {
                bool flag = false;
                if (objHoliday.HolidayID > 0)
                {
                    objHoliday.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objHoliday.ModifiedDate = DateTime.Now;
                    using (ServiceContext objService = new ServiceContext())
                    {
                        lstHoliday = objService.SearchAll(objOldHoliday).Where(o => o.L1ID == objHoliday.L1ID && o.L2ID == objHoliday.L2ID && (o.HolidayDesc == objHoliday.HolidayDesc) && (o.HolidayDate.ToShortDateString() == objHoliday.HolidayDate.ToShortDateString()) && o.HolidayID != objHoliday.HolidayID).ToList();
                        if (lstHoliday.Count > 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            if (objService.Save(objHoliday) > 0)
                            {
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                            }
                        }
                    }
                }
                else
                {
                    objHoliday.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objHoliday.CreatedDate = DateTime.Now;
                    string[] L2IDs = objHoliday.L2IDs.Split(',').Select(sValue => sValue.Trim()).ToArray();
                    foreach (var id in L2IDs)
                    {
                        objHoliday.L2ID = ConvertTo.Integer(id);

                        using (ServiceContext objService = new ServiceContext())
                        {
                            lstHoliday = objService.SearchAll(objOldHoliday).Where(o => o.L1ID == objHoliday.L1ID && o.L2ID == objHoliday.L2ID && (o.HolidayDesc == objHoliday.HolidayDesc) && (o.HolidayDate.ToShortDateString() == objHoliday.HolidayDate.ToShortDateString())).ToList();
                            if (lstHoliday.Count > 0)
                            {
                                flag = false;
                            }
                            else
                            {

                                if (objService.Save(objHoliday) > 0)
                                {
                                    flag = true;
                                }
                                else
                                {
                                    if (L2IDs.Count() > 1)
                                    {
                                        flag = true;
                                    }
                                    else
                                    {
                                        flag = false;
                                    }
                                }
                            }
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveWorkingDay(WorkingDay objWorkingDay)
        {
            List<WorkingDay> lstWorkingDay = new List<WorkingDay>();
            WorkingDay objOldWorkingDay = new WorkingDay();
            try
            {
                bool flag = false;
                if (objWorkingDay.WorkingDayID > 0)
                {
                    objWorkingDay.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objWorkingDay.ModifiedDate = DateTime.Now;
                    using (ServiceContext objService = new ServiceContext())
                    {
                        lstWorkingDay = objService.SearchAll(objOldWorkingDay).Where(o => o.L1ID == objWorkingDay.L1ID && o.L2ID == objWorkingDay.L2ID && o.WorkingDayID != objWorkingDay.WorkingDayID).ToList();
                        if (lstWorkingDay.Count > 0)
                        {
                            flag = false;
                        }
                        else
                        {
                            int workingDayId = objService.Save(objWorkingDay);
                            if (workingDayId > 0)
                            {
                                objWorkingDay.WorkingDayID = workingDayId;
                                SaveWorkingDayHour(objWorkingDay);
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                            }
                        }
                    }
                }
                else
                {
                    objWorkingDay.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objWorkingDay.CreatedDate = DateTime.Now;
                    string[] L2IDs = objWorkingDay.L2IDs.Split(',').Select(sValue => sValue.Trim()).ToArray();
                    foreach (var id in L2IDs)
                    {
                        objWorkingDay.L2ID = ConvertTo.Integer(id);

                        using (ServiceContext objService = new ServiceContext())
                        {
                            lstWorkingDay = objService.SearchAll(objOldWorkingDay).Where(o => o.L1ID == objWorkingDay.L1ID && o.L2ID == objWorkingDay.L2ID).ToList();
                            if (lstWorkingDay.Count > 0)
                            {
                                flag = false;
                            }
                            else
                            {
                                int workingDayId = 0;
                                workingDayId = objService.Save(objWorkingDay);
                                if (workingDayId > 0)
                                {
                                    objWorkingDay.WorkingDayID = workingDayId;
                                    SaveWorkingDayHour(objWorkingDay);
                                    flag = true;
                                }
                                else
                                {
                                    if (L2IDs.Count() > 1)
                                    {
                                        flag = true;
                                    }
                                    else
                                    {
                                        flag = false;
                                    }
                                }
                            }
                        }
                    }

                }
                return flag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeleteWorkingDayByID(int workingDayID)
        {
            int returnValue = 0;
            WorkingDay objWorkingDay = new WorkingDay();
            using (DapperContext objDapperContext = new DapperContext())
            {
                string strQuery = string.Empty;
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "WorkingDayID",
                    Value = workingDayID,
                    DBType = DbType.Int32
                });

                strQuery = @"Delete From WorkingDayHours where WorkingDayID = @WorkingDayID";
                objDapperContext.ExecuteQuery(strQuery, parameters);

                returnValue = objDapperContext.Delete<WorkingDay>(workingDayID, true);

            }
            return returnValue;
        }
    }
}
