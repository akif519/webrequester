﻿using CMMS.DAL;
using CMMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class SearchFilterService : DBExecute
    {
        public SearchFilterService()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }

        public virtual IList<TEntity> GetAdvanceSearchFiltersByPage<TEntity>(TEntity entity, int PageID, int EmployeeID)
        {
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    string sqlquery = @"Select 
                            SF.SearchFieldID,S.SearchFilterID,SF.PageID,S.EmployeeID,SF.DBColumnType,S.EmployeeID,S.OperatorID,S.Value1,S.Value2,S.Condition
                            FROM SearchFields SF
                            INNER JOIN SearchFilters S ON S.SearchFieldID = SF.SearchFieldID
                            WHERE SF.PageID = @PageID AND S.EmployeeID = @EmployeeID ANd S.IsSaved = 1";

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "PageID", Value = PageID, DBType = DbType.Int32 });
                    parameters.Add(new DBParameters() { Name = "EmployeeID", Value = EmployeeID, DBType = DbType.Int32 });

                    /*Convert Dataset to Model List object*/
                    return objDapperContext.ExecuteQuery<TEntity>(sqlquery, parameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Records Before Search Again
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="PageID">PageID</param>
        /// <param name="EmployeeID">EmployeeID</param>
        /// <returns></returns>
        public virtual int DeletePreviousFilter<TEntity>(int PageID, int EmployeeID, bool IsSaved = true)
        {
            try
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    string sqlquery = "";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlquery = @"DELETE FROM ( SELECT SFilters.* 
                                    FROM SearchFields SF
                                    INNER JOIN SearchFilters SFilters ON SFilters.SearchFieldID = SF.SearchFieldID
                                    Where SFilters.EmployeeID = @EmployeeID AND SF.PageID = @PageID  AND ISNULL(IsSaved,0) = @IsSaved)";

                    }
                    else
                    {
                        sqlquery = @"DELETE SFilters
                                    FROM SearchFields SF
                                    INNER JOIN SearchFilters SFilters ON SFilters.SearchFieldID = SF.SearchFieldID
                                    Where SFilters.EmployeeID = @EmployeeID AND SF.PageID = @PageID  AND ISNULL(IsSaved,0) = @IsSaved";
                    }

                    Collection<DBParameters> parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters() { Name = "PageID", Value = PageID, DBType = DbType.Int32 });
                    parameters.Add(new DBParameters() { Name = "EmployeeID", Value = EmployeeID, DBType = DbType.Int32 });
                    parameters.Add(new DBParameters() { Name = "IsSaved", Value = IsSaved, DBType = DbType.Boolean });
                    objDapperContext.ExecuteQuery(sqlquery, parameters);
                }
                return 0;
            }
            catch
            {
                return -1;
            }
        }
    }
}
