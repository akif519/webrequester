﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Services;
using CMMS.Service;
using CMMS.Infrastructure;
using System.Reflection;

namespace CMMS.Services
{
    public class PermissionService : DBExecute
    {
        public PermissionService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Get Permission list by EmployeeId/ Or Employee GroupId
        /// </summary>
        /// <returns></returns>
        public IList<Permission> GetEmployeepermissionListByEmployee(employees emp)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                IList<Permission> lstPermissions = null;

                parameters.Add(new DBParameters()
                {
                    Name = "EmpId",
                    Value = emp.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "GroupID",
                    Value = emp.UserGroupId,
                    DBType = DbType.Int32
                });

                string query = string.Empty;

                //query = "SELECT *,";
                //query += " ISNULL((Select AllowAccess FROM permissionsmappings p WHERE p.PermissionId=(SELECT PermissionId FROM PERMISSIONS WHERE PERMISSIONS.ModuleName=Temp.ModuleName AND PERMISSIONS.SubModuleName='' ) AND p.EmployeeId= @EmpId),0) AS AllowModuleByEmp,";
                //query += " ISNULL((Select AllowAccess FROM permissionsmappings p WHERE p.PermissionId=(SELECT PermissionId FROM PERMISSIONS WHERE PERMISSIONS.ModuleName=Temp.ModuleName AND PERMISSIONS.SubModuleName='' ) AND p.UserGroupID=1 ),0) AS AllowModuleByGroup";
                //query += " FROM   (";
                query += " SELECT p.PermissionId,p.ModuleName,p.SubModuleName,p.PermissionDesc,p.CreatedBy,p.ModifiedBy,p.ModifiedDate,ISNULL(p2.AllowAccess, 0)  AS AllowAccessByUserGroup,ISNULL(p3.AllowAccess, 0)  AS AllowAccessByEmployee ";
                query += "FROM permissions p";
                query += " LEFT JOIN permissionsmappings p2";
                query += " ON  p2.PermissionId = p.PermissionId";
                query += " AND p2.UserGroupId = @GroupID";
                query += " LEFT JOIN permissionsmappings p3";
                query += " ON  p3.PermissionId = p.PermissionId";
                query += " AND p3.EmployeeId = @EmpId";
                //query += " ) Temp";
                query += " ORDER BY PermissionId";
                //query += " WHERE Temp.SubModuleName<>''";
                //query += " ORDER BY Temp.PermissionId";

                using (DapperDBContext context = new DapperDBContext())
                {
                    lstPermissions = context.ExecuteQuery<Permission>(query, parameters);
                }
                return lstPermissions;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// To Update Pemission By User Group ID
        /// </summary>
        /// <param name="lstEmployeePermissionModel">List Of Permission </param>
        /// <param name="userGroupId">GroupID</param>
        public void UpdatePermissionByUserGroup(List<Permission> lstEmployeePermissionModel, int userGroupId)
        {

            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            try
            {

                if (lstEmployeePermissionModel.Count() > 0)
                {
                    List<Permission> listofAddnewPermissionRecords = new List<Permission>();
                    IList<Permissionsmapping> listOfPermissionMappings = new List<Permissionsmapping>();

                    Permissionsmapping objPermissionMapping = new Permissionsmapping();
                    objPermissionMapping.UserGroupId = userGroupId;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        listOfPermissionMappings = objContext.Search<Permissionsmapping>(objPermissionMapping);
                    }
                    //List<Permissionsmapping> listofNeedToUpdatePermissionRecords = listOfPermissionMappings.Where(p => p.UserGroupId == userGroupId && lstEmployeePermissionModel.Select(ep => ep.PermissionId).Any(ep => ep == p.PermissionId)).ToList();
                    List<Permissionsmapping> listofNeedToUpdatePermissionRecords = listOfPermissionMappings.Where(p => p.AllowAccess != (lstEmployeePermissionModel.Where(ep => ep.PermissionId == p.PermissionId).Select(x => x.AllowAccessByUserGroup).FirstOrDefault())).ToList();

                    if (listofNeedToUpdatePermissionRecords.Count() > 0)
                    {

                        listofNeedToUpdatePermissionRecords.ForEach(p =>
                        {
                            objPermissionMapping = p;
                            objPermissionMapping.AllowAccess = lstEmployeePermissionModel.FirstOrDefault(ep => ep.PermissionId == p.PermissionId).AllowAccessByUserGroup;
                            objPermissionMapping.ModifiedBy = Convert.ToString(ProjectSession.EmployeeID);
                            objPermissionMapping.ModifiedDate = DateTime.Now;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objContext.Save<Permissionsmapping>(objPermissionMapping);
                            }
                        });
                        listofAddnewPermissionRecords = lstEmployeePermissionModel
                            .Where(p => p.AllowAccessByUserGroup && !listOfPermissionMappings.Any(ep => ep.PermissionId == p.PermissionId)).ToList();
                    }
                    else
                    {
                        //listofAddnewPermissionRecords = lstEmployeePermissionModel.Where(p => p.AllowAccessByUserGroup).ToList();
                        listofAddnewPermissionRecords = lstEmployeePermissionModel
                           .Where(p => p.AllowAccessByUserGroup && !listOfPermissionMappings.Any(ep => ep.PermissionId == p.PermissionId)).ToList();
                    }

                    foreach (Permission objMapping in listofAddnewPermissionRecords)
                    {
                        objPermissionMapping = new Permissionsmapping();
                        objPermissionMapping.CreatedDate = DateTime.Now;
                        objPermissionMapping.CreatedBy = Convert.ToString(ProjectSession.EmployeeID);
                        objPermissionMapping.UserGroupId = userGroupId;
                        objPermissionMapping.AllowAccess = objMapping.AllowAccessByUserGroup;
                        objPermissionMapping.PermissionId = objMapping.PermissionId;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objContext.Save<Permissionsmapping>(objPermissionMapping);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            //}
        }

        /// <summary>
        /// To Update Pemission By User Group ID
        /// </summary>
        /// <param name="lstEmployeePermissionModel">List Of Permission </param>
        /// <param name="employeeId">employee id</param>
        public void UpdatePermissionByEmployee(List<Permission> lstEmployeePermissionModel, int employeeId)
        {

            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            try
            {

                if (lstEmployeePermissionModel.Count() > 0)
                {
                    List<Permission> listofAddnewPermissionRecords = new List<Permission>();
                    IList<Permissionsmapping> listOfPermissionMappings = new List<Permissionsmapping>();

                    Permissionsmapping objPermissionMapping = new Permissionsmapping();
                    objPermissionMapping.EmployeeId = employeeId;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        listOfPermissionMappings = objContext.Search<Permissionsmapping>(objPermissionMapping);
                    }
                    List<Permissionsmapping> listofNeedToUpdatePermissionRecords = listOfPermissionMappings.Where(p => p.AllowAccess != (lstEmployeePermissionModel.Where(ep => ep.PermissionId == p.PermissionId && p.PermissionId > 0).Select(x => x.AllowAccessByEmployee).FirstOrDefault())).ToList();

                    if (listofNeedToUpdatePermissionRecords.Count() > 0)
                    {

                        listofNeedToUpdatePermissionRecords.ForEach(p =>
                        {
                            if (p.PermissionId != null && p.PermissionId > 0)
                            {
                                objPermissionMapping = p;
                                objPermissionMapping.AllowAccess = lstEmployeePermissionModel.FirstOrDefault(ep => ep.PermissionId == p.PermissionId).AllowAccessByEmployee;
                                objPermissionMapping.ModifiedBy = Convert.ToString(ProjectSession.EmployeeID);
                                objPermissionMapping.ModifiedDate = DateTime.Now;

                                using (ServiceContext objContext = new ServiceContext())
                                {
                                    objContext.Save<Permissionsmapping>(objPermissionMapping);
                                }
                            }
                        });
                        listofAddnewPermissionRecords = lstEmployeePermissionModel.AsEnumerable()
                                                             .Where(p => p.AllowAccessByEmployee && !listOfPermissionMappings.Any(ep => ep.PermissionId == p.PermissionId)).ToList();

                    }
                    else
                    {
                        // listofAddnewPermissionRecords = lstEmployeePermissionModel.Where(p => p.AllowAccessByEmployee).ToList();
                        listofAddnewPermissionRecords = lstEmployeePermissionModel.AsEnumerable()
                                                             .Where(p => p.AllowAccessByEmployee && !listOfPermissionMappings.Any(ep => ep.PermissionId == p.PermissionId)).ToList();
                    }

                    foreach (Permission objMapping in listofAddnewPermissionRecords)
                    {
                        objPermissionMapping = new Permissionsmapping();
                        objPermissionMapping.CreatedDate = DateTime.Now;
                        objPermissionMapping.CreatedBy = Convert.ToString(ProjectSession.EmployeeID);
                        objPermissionMapping.EmployeeId = employeeId;
                        objPermissionMapping.AllowAccess = objMapping.AllowAccessByUserGroup;
                        objPermissionMapping.PermissionId = objMapping.PermissionId;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objContext.Save<Permissionsmapping>(objPermissionMapping);
                        }
                    }

                    employees objemployees = new employees();
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        objemployees.EmployeeID = employeeId;
                        objemployees = objDapperContext.Search<employees>(objemployees).FirstOrDefault();
                        objemployees.LastConfigChangeDateTime = DateTime.Now;
                        objDapperContext.Save<employees>(objemployees);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //}
        }

        public static PermissionAccess SetEmployeePermission(employees emp)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();


                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = emp.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "UserGroupId",
                    Value = emp.UserGroupId,
                    DBType = DbType.Int32
                });

                string query = string.Empty;
                PermissionAccess objPermission = new PermissionAccess();

                using (DapperContext objDapperContext = new DapperContext())
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = @"SELECT  
 (REPLACE(REPLACE(ModuleName,' ',''),'/','') ||  '_' ||  REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') ||  '_' ||  REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(AllowAccess) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";
                    }
                    else
                    {
                        query = @"SELECT  
 CONCAT(REPLACE(REPLACE(ModuleName,' ',''),'/','') , '_' , REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') , '_' , REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(CAST(AllowAccess AS DECIMAL)) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";

                    }

                    List<Permission> lst = objDapperContext.ExecuteQuery<Permission>(query, parameters).ToList();

                    foreach (Permission lstPermission in lst)
                    {
                        PropertyInfo info = objPermission.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstPermission.PermissionName.ToString().ToLower());
                        if (info != null)
                        {
                            /*Set the Value to Model*/
                            info.SetValue(objPermission, lstPermission.AllowAccess);
                        }
                    }
                }
                return objPermission;

            }
            catch
            {
                throw;
            }
        }

    }
}
