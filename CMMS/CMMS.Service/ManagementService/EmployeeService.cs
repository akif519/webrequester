﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Model;
using CMMS.Services;
using CMMS.Service;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Service
{
    public class EmployeeService : DBExecute
    {
        public EmployeeService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #region "Employee"

        public IList<employees> GetEmployeeAssignPage(int empID, int L2ID, int pageNo, string strWhere, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<employees> list = new List<employees>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "EmployeeID";
            }
            string strWhereClause = string.Empty;
            string strWhereClauseWithEmployee = string.Empty;
            strWhereClauseWithEmployee = " and employees.EmployeeStatusId in (select EmployeeStatusId from employeeStatuses where DisplayInModule = 1) " +
              " and (Central = 1 or employees.employeeId in (select empId from employees_L2 where employees_L2.L2Id = " + L2ID + " )" +
             "  or employees.L2ID = " + L2ID + ") ";

            string query = "Select employees.EmployeeID,employees.L2ID,L2.L2Code,employees.CategoryID,employeecategories.CategoryName,employeecategories.AltCategoryName,employees.WorkTradeID,employees.UserGroupId,employees.EmployeeNO,employees.Name,employees.AltName,employees.Positions,employees.Extension, "
                            + " employees.WorkPhone,employees.HandPhone,employees.Email,employees.Fax,employees.Housephone,employees.Address,employees.OfficeLocation, " +
                            " employees.HourlySalary,employees.OverTime1,employees.OverTime2,employees.OverTime3,employees.Accessibility,  " +
                            " employees.UserID,employees.Password,employees.Central,employees.Auth,employees.ExecLevel,employees.WebAccess,  " +
                            " employees.OpenWorkorders,employees.ReopenWorkorders,employees.Connected,employees.WRConnected,employees.AllowAdjustment, "
                            + " employees.pr_auto_amount,employees.EmpDeptCode,employees.AllowCancelPO,employees.ConnectedMW,employees.WOReturn," +
                            " employees.WOIssue,employees.hide_cost,employees.LanguageCode,employees.AllowInterSiteTransfer,employees.postal_address," +
                            " employees.maintDeptID,MaintenanceDepartment.MaintDeptCode,employees.MaintSubDeptID,MaintSubDept.MaintSubDeptCode,employees.MaintDivisionID,MainenanceDivision.MaintDivisionCode,employees.EmployeeStatusId,employeeStatuses.EmployeeStatus,employeeStatuses.EmployeeAltStatus,employees.CreatedBy, " +
                            " employees.CreatedDate,employees.ModifiedBy,employees.ModifiedDate " +
                            " ,employees.TBtransaction,employees.TBSetup,employees.TBReport,employees.TBDashboard " +
                            " FROM  employees Left Outer join L2 on employees.L2ID = L2.L2ID  Left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID =  employees.maintDeptID " +
                            " left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = employees.MaintSubDeptID " +
                            " left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = employees.MaintDivisionID " +
                            " left outer join  employeeStatuses on employeeStatuses.EmployeeStatusId = employees.EmployeeStatusId " +
                            " left outer join  employeecategories on employeecategories.CategoryID = employees.CategoryID "
                            + " where 1=1 " + strWhere + strWhereClauseWithEmployee + " ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<employees>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }
        /// <summary>
        /// Get Employee List for Active directory search
        /// </summary>
        /// <param name="ModelList, CurUser"></param>
        /// <param name="pageNo"></param>
        /// <param name="strWhere"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public void SaveWebreqEmployee(employees objEmp)
        {
            int result = 0;
            //string directoryName = ProjectConfiguration.UploadPath + "UserImages";
            string fileName = string.Empty;
            string oldFileName = string.Empty;
            string validations = string.Empty;
            bool createRecords = false;
            bool editRecords = false;

            try
            {
                if (objEmp.isfrmTransaction == 0)
                {
                    createRecords = ProjectSession.PermissionAccess.Management_User_Createnewrecords;
                    editRecords = ProjectSession.PermissionAccess.Management_User_Editrecords;
                }
                else
                {
                    createRecords = ProjectSession.PermissionAccess.Employee_Employee_Createnewrecords;
                    editRecords = ProjectSession.PermissionAccess.Employee_Employee_Editrecords;
                }

                employees objemployee = new employees();

                IList<employees> lstEmployees = new List<employees>();
                using (DapperContext objDapper = new DapperContext())
                {
                    lstEmployees = objDapper.SearchAll<employees>(objemployee);
                }

                if (objEmp.EmployeeID > 0)
                {
                    //objEmp.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    //objEmp.ModifiedByName = ProjectSession.EmployeeName;
                    objEmp.ModifiedDate = DateTime.Now;
                }
                else
                {
                    //objEmp.CreatedBy = ProjectSession.EmployeeID.ToString();
                    //objEmp.CreatedByName = ProjectSession.EmployeeName;
                    objEmp.CreatedDate = DateTime.Now;
                }
                using (DapperContext objContext = new DapperContext())
                {

                    result = objContext.Save<employees>(objEmp);

                    if ((objEmp.EmployeeID == 0) && (result > 0) && (Convert.ToInt32(objEmp.UserGroupId) > 0))
                    {
                        EmployeeService objServiceemp = new EmployeeService();
                        objServiceemp.CopyUserGroupPermissionsToEmployee(result, (Convert.ToInt32(objEmp.UserGroupId)));
                    }

                }
            }


            catch
            {
                throw;
                //if (Convert.ToString(TempData["Message"]) == string.Empty)
                //{
                //    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                //}
                //TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                //return RedirectToAction(Actions.GetEmployeeByID, Pages.Controllers.Management, new { id = objEmp.EmployeeID, frm = objEmp.isfrmTransaction });
            }
        }
        
        /// <summary>
        /// Get Employee List As per the conditions
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="pageNo"></param>
        /// <param name="strWhere"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<employees> GetEmployeePage(int empID, int pageNo, string strWhere, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<employees> list = new List<employees>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "EmployeeID";
            }

            string strWhereClauseWithEmployee = string.Empty;
            if (empID > 0)
            {
                strWhereClauseWithEmployee = " and employees.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " ) ";
            }

            string query = @"Select employees.EmployeeID,employees.L2ID,employees.CategoryID,employees.WorkTradeID,employees.UserGroupId,employees.EmployeeNO,employees.Name,employees.AltName,employees.Positions,employees.Extension,
                             employees.WorkPhone,employees.HandPhone,employees.Email,employees.Fax,employees.Housephone,employees.Address,employees.OfficeLocation, 
                             employees.HourlySalary,employees.OverTime1,employees.OverTime2,employees.OverTime3,employees.Accessibility,  
                             employees.UserID,employees.Password,employees.Central,employees.Auth,employees.ExecLevel,employees.WebAccess,  
                             employees.OpenWorkorders,employees.ReopenWorkorders,employees.Connected,employees.WRConnected,employees.AllowAdjustment,  
                             employees.pr_auto_amount,employees.EmpDeptCode,employees.AllowCancelPO,employees.ConnectedMW,employees.WOReturn,
                             employees.WOIssue,employees.hide_cost,employees.LanguageCode,employees.AllowInterSiteTransfer,employees.postal_address,
                             employees.maintDeptID,employees.MaintSubDeptID,employees.MaintDivisionID,employees.EmployeeStatusId,employees.CreatedBy, 
                             employees.CreatedDate,employees.ModifiedBy,employees.ModifiedDate 
                             ,employees.maintDeptID,MaintenanceDepartment.MaintDeptCode,employees.MaintSubDeptID,MaintSubDept.MaintSubDeptCode,employees.MaintDivisionID,
                             MainenanceDivision.MaintDivisionCode,employees.EmployeeStatusId,employeeStatuses.EmployeeStatus,employeeStatuses.EmployeeAltStatus,employees.CreatedBy
                             ,employeecategories.CategoryName,employeecategories.AltCategoryName,L2.L2Code
                             ,IsStoredSupervisor,POAuthorisationLimit
                             FROM  employees 
                             Left Outer join L2 on employees.L2ID = L2.L2ID
                             Left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID =  employees.maintDeptID 
                             left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = employees.MaintSubDeptID 
                             left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = employees.MaintDivisionID 
                             left outer join  employeeStatuses on employeeStatuses.EmployeeStatusId = employees.EmployeeStatusId 
                             left outer join  employeecategories on employeecategories.CategoryID = employees.CategoryID 
                             left outer join  EmployeeProfession on EmployeeProfession.EmployeeProfID = employees.EmployeeProfessionID 
                             where 1=1 " + strWhere + strWhereClauseWithEmployee + " ";


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<employees>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<employees> GetEmployees(int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null, string text = "")
        {
            IList<employees> list = new List<employees>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "EmployeeID";
            }

            string query = "Select employees.* , employeecategories.CategoryName,employeecategories.AltCategoryName ,MaintenanceDepartment.MaintDeptCode," +
                            "MaintSubDept.MaintSubDeptCode,L2.L2Code,MaintSubDept.MaintSubDeptCode,MainenanceDivision.MaintDivisionCode,employeeStatuses.EmployeeStatus,employeeStatuses.EmployeeAltStatus " +
                           "from employees " +
                           " Left Outer join L2 on employees.L2ID = L2.L2ID " +
                           " Left outer join MaintenanceDepartment on MaintenanceDepartment.maintDeptID =  employees.maintDeptID  " +
                           " left outer join MaintSubDept on MaintSubDept.MaintSubDeptID = employees.MaintSubDeptID  " +
                           " left outer join MainenanceDivision on MainenanceDivision.MaintDivisionID = employees.MaintDivisionID  " +
                           " left outer join  employeeStatuses on employeeStatuses.EmployeeStatusId = employees.EmployeeStatusId  " +
                           " left outer join  employeecategories on employeecategories.CategoryID = employees.CategoryID  Where 1 = 1 ";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhereAuto = " AND (lower(EmployeeNO) like N'%" + text + "%' or lower(Name) like N'%" + text + "%' or lower(AltName) like N'%" + text + "%')";
                query += strWhereAuto;
            }
            else 
            {
                strWhereAuto = " AND (EmployeeNO like N'%" + text + "%' or Name like N'%" + text + "%' or AltName like N'%" + text + "%')";
                query += strWhereAuto;
            }
            

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<employees>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static employees ValidateEmployee(string username, string password, string IPAddress)
        {
            employees objemployees = new employees();
            objemployees.UserID = username;
            objemployees.Password = password;
            using (ServiceContext context = new ServiceContext())
            {
                objemployees = context.Search<employees>(objemployees).FirstOrDefault();
            }
            ///remaining code
            return objemployees;
        }

        public bool CopyUserGroupPermissionsToEmployee(int employeeID, int userGroupID)
        {
            try
            {
                string strSQL = string.Empty;
                int result = -1;

                strSQL = "Delete from permissionsmappings where EmployeeId = " + employeeID + " AND UserGroupID is null";

                using (ServiceContext objContext = new ServiceContext(true))
                {
                    try
                    {
                        objContext.BeginTransaction();
                        result = objContext.ExecuteQuery(strSQL, objContext.Connection, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            strSQL = "Insert Into permissionsmappings select " + employeeID + ",NULL,PermissionID,AllowAccess,1,GETDATE(),NULL,NULL from permissionsmappings where usergroupid = " + userGroupID + " AND EmployeeID is NUll";
                            result = objContext.ExecuteQuery(strSQL, objContext.Connection, new Collection<DBParameters>());
                        }

                        if (result == 0)
                        {
                            objContext.CommitTransaction();
                            return true;
                        }
                        else
                        {
                            objContext.RollBackTransaction();
                            return false;
                        }
                    }
                    catch (Exception ex1)
                    {
                        objContext.RollBackTransaction();
                        throw ex1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TabAccess GetTabRightsOfEmployee(int employeeID)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = employeeID,
                    DBType = DbType.Int32
                });

                string strSQl = "Select TBTransaction As Transactions,TBSetup As Setup,TBReport As Reports,TBDashBoard As Dashboard from Employees Where EmployeeID = @EMPID";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQuery<TabAccess>(strSQl, parameters).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IList<employees> GetAllEmployees()
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string strSQl = "Select * from employees " +
                                "inner join employeeStatuses on employeeStatuses.employeeStatusid = employees.employeeStatusid";

                using (ServiceContext objContext = new ServiceContext())
                {
                    return objContext.ExecuteQuery<employees>(strSQl, parameters).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Employee Documents"

        public List<EmployeeDocuments> GetDocumentsByEmployee(Cmemployeedocument objEmpDocs, int? pageNo, string sortExpression, string sortDirection)
        {
            IList<Cmemployeedocument> lstDocuments = null;
            List<EmployeeDocuments> lstEmpDocuments = new List<EmployeeDocuments>();
            EmployeeDocuments objEmpDocuments = null;
            using (ServiceContext userService = new ServiceContext())
            {
                lstDocuments = userService.Search<Cmemployeedocument>(objEmpDocs, pageNo, sortExpression, sortDirection);
            }

            if (lstDocuments != null && lstDocuments.Count > 0)
            {
                CmAttachment objAttachment = new CmAttachment();
                foreach (Cmemployeedocument objDoc in lstDocuments)
                {
                    objEmpDocuments = new EmployeeDocuments();
                    objEmpDocuments.employeedocs = objDoc;
                    objEmpDocuments.Guid_DocID = System.Guid.NewGuid();
                    objAttachment.FkId = Convert.ToString(objDoc.DocID);
                    objAttachment.ModuleType = CMMS.Infrastructure.SystemEnum.ModuleType.Employee.ToString();
                    using (ServiceContext userService = new ServiceContext())
                    {
                        IList<CmAttachment> lstAttachments = userService.Search<CmAttachment>(objAttachment, pageNo, sortExpression, sortDirection);

                        objEmpDocuments.lstAttachment = lstAttachments.Select(attachment => new clsextassetfiles
                        {
                            guidId = Guid.NewGuid(),
                            AutoId = attachment.AutoId,
                            FkId = attachment.FkId,
                            FileLink = attachment.FileLink,
                            FileDescription = attachment.FileDescription,
                            FileName = attachment.FileName,
                            ModuleType = attachment.ModuleType

                        }).ToList();
                    }

                    lstEmpDocuments.Add(objEmpDocuments);
                }
            }
            return lstEmpDocuments;
        }

        public int UpdateEmployeeDocuments(employees model)
        {
            try
            {
                if (model.lstEmployeeDocumentModel != null && model.lstEmployeeDocumentModel.Count() > 0)
                {
                    List<EmployeeDocuments> listofAddnewEmployeeDocumentRecords = new List<EmployeeDocuments>();

                    IList<Cmemployeedocument> lstEmpDocs = new List<Cmemployeedocument>();
                    Cmemployeedocument objEmpDoc = new Cmemployeedocument();
                    using (DapperContext objContext = new DapperContext())
                    {
                        objEmpDoc.EmployeeID = model.EmployeeID;
                        lstEmpDocs = objContext.SearchAll<Cmemployeedocument>(objEmpDoc);
                    }

                    List<Cmemployeedocument> listofNeedToUpdateRecords = lstEmpDocs.Where(p => p.EmployeeID == model.EmployeeID && model.lstEmployeeDocumentModel.Select(ep => ep.employeedocs.DocID).Any(ep => ep == p.DocID)).ToList();

                    if (listofNeedToUpdateRecords.Count() > 0)
                    {

                        listofNeedToUpdateRecords.ForEach(p =>
                        {
                            EmployeeDocuments editObject = model.lstEmployeeDocumentModel.FirstOrDefault(ep => ep.employeedocs.DocID == p.DocID);

                            objEmpDoc = new Cmemployeedocument
                            {
                                DocID = editObject.employeedocs.DocID,
                                DocName = editObject.employeedocs.DocName,
                                DocArabicName = editObject.employeedocs.DocArabicName,
                                Doc_ExpiryDate = editObject.employeedocs.Doc_ExpiryDate,
                                Issuer = editObject.employeedocs.Issuer,
                                Remarks = editObject.employeedocs.Remarks,
                                ModifiedDate = model.ModifiedDate,
                                ModifiedBy = Convert.ToInt32(model.ModifiedBy)
                            };
                            using (DapperContext context = new DapperContext())
                            {
                                context.Save<Cmemployeedocument>(objEmpDoc);
                            }
                        });

                        model.lstEmployeeDocumentModel.Where(d => d.employeedocs.DocID > 0).ToList().ForEach(e =>
                        {
                            SaveEmpoyeeDocumentAttachment(e);
                        });

                        listofAddnewEmployeeDocumentRecords = model.lstEmployeeDocumentModel.AsEnumerable().Where(p => !listofNeedToUpdateRecords.Any(ep => ep.DocID == p.employeedocs.DocID)).ToList();
                    }
                    else
                    {
                        listofAddnewEmployeeDocumentRecords = model.lstEmployeeDocumentModel.ToList();
                    }

                    foreach (var item in listofAddnewEmployeeDocumentRecords)
                    {
                        Cmemployeedocument newdoc = new Cmemployeedocument()
                        {
                            DocName = item.employeedocs.DocName,
                            DocArabicName = item.employeedocs.DocArabicName,
                            Doc_ExpiryDate = item.employeedocs.Doc_ExpiryDate,
                            Issuer = item.employeedocs.Issuer,
                            Remarks = item.employeedocs.Remarks,
                            CreatedBy = (model.ModifiedBy != string.Empty && model.ModifiedBy != "0") ? Convert.ToInt32(model.ModifiedBy) : Convert.ToInt32(model.CreatedBy),
                            CreatedDate = model.ModifiedDate.HasValue ? model.ModifiedDate : model.CreatedDate,
                            EmployeeID = model.EmployeeID
                        };

                        using (DapperContext objDapper = new DapperContext())
                        {
                            item.employeedocs = newdoc;
                            item.employeedocs.DocID = objDapper.Save<Cmemployeedocument>(newdoc);
                        }

                        SaveEmpoyeeDocumentAttachment(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return 1;
        }

        /// <summary>
        /// For Add attachment for Document
        /// </summary>
        /// <param name="model"></param>
        public void SaveEmpoyeeDocumentAttachment(EmployeeDocuments model)
        {

            if (model.lstAttachment != null)
            {
                CmAttachment objNewAttachment = new CmAttachment();
                foreach (CmAttachment attachment in model.lstAttachment)
                {
                    if (attachment.AutoId == 0)
                    {
                        objNewAttachment.FkId = model.employeedocs.DocID.ToString();
                        objNewAttachment.FileLink = attachment.FileLink;
                        objNewAttachment.FileDescription = attachment.FileDescription;
                        objNewAttachment.FileName = attachment.FileName;
                        objNewAttachment.ModuleType = SystemEnum.ModuleType.Employee.ToString();
                        objNewAttachment.CreatedBy = (model.employeedocs.ModifiedBy > 0) ? model.employeedocs.ModifiedBy : model.employeedocs.CreatedBy;
                        objNewAttachment.CreatedDate = model.employeedocs.ModifiedDate.HasValue ? model.employeedocs.ModifiedDate : model.employeedocs.CreatedDate;

                        using (DapperContext objContext = new DapperContext())
                        {
                            objContext.Save<CmAttachment>(objNewAttachment);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delete Employee Document by ID
        /// </summary>
        /// <param name="AutoId"></param>
        public int DeleteEmployeeDocumentByDocId(int DocId)
        {
            try
            {
                Cmemployeedocument objEmpDoc = new Cmemployeedocument();
                objEmpDoc.DocID = DocId;
                int result = 0;
                using (DapperContext context = new DapperContext())
                {
                    result = context.Delete<Cmemployeedocument>(DocId);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Employee Login Tab"

        public bool UpdateEmployeeLogin(int employeeID, string userID, string passWord, int transactionTab, int setupTab, int reportTab, int dashboardTab, decimal? poAuthLimit, int isCentral, int isSuperWiser)
        {
            string strSQl = string.Empty;
            int result = -1;
            try
            {
                strSQl = "Update Employees Set UserID = '" + Common.setQuote(userID) + "', Password = '" + Common.setQuote(passWord) + "', TBtransaction = " + transactionTab + ", TBSetup = " + setupTab + ", TBReport = " + reportTab + ", TBDashboard = " + dashboardTab;
                strSQl += ",POAuthorisationLimit=" + (poAuthLimit == null ? "NULL" : poAuthLimit.ToString()) + ",Central=" + isCentral + ",IsStoredSupervisor = " + isSuperWiser + " Where EmployeeID =" + employeeID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, new Collection<DBParameters>());

                    if (result == 0)
                    {
                        employees objemployees = new employees();
                        using (DapperContext objDapperContext = new DapperContext())
                        {
                            objemployees.EmployeeID = employeeID;
                            objemployees = objDapperContext.Search<employees>(objemployees).FirstOrDefault();
                            objemployees.LastConfigChangeDateTime = DateTime.Now;
                            objDapperContext.Save<employees>(objemployees);
                        }                        
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }     


        #endregion

        #region "Employee City Permissions"

        public IList<Employees_L2> GetCitiesWithCheckBox(int empID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Employees_L2> list = new List<Employees_L2>();
            Collection<DBParameters> Parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "L2ID";
            }

            string query = "Select l2.L2ID,l2.L1ID,l2.L2Code,l2.L2name,l2.L2Altname,l2.DefaultCalendar,l2.Timezone," +
                            "L2.Status,L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class," +
                           "Status,l2.CreatedBy,l2.CreatedDate,l2.ModifiedBy,l2.ModifiedDate," +
                           " (case isnull((Select employees_L2.L2Id from employees_L2 where employees_L2.L2ID=L2.L2ID   " +
                           " and employees_L2.empID = " + empID + "),0)  when 0  then 0 else 1 end) as ChkL2 from l2 LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID ";

            SearchFieldService objSearchField = new SearchFieldService();

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Employees_L2>(query, Parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public bool DeleteEmployeeL2(int employeeID, int cityID)
        {
            string strSQl = string.Empty;
            int result = -1;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EmpID",
                    Value = employeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "L2ID",
                    Value = cityID,
                    DBType = DbType.Int32
                });

                strSQl = "Delete From employees_L2  Where  empID = @EmpID AND L2ID = @L2ID";

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, parameters);

                    if (result == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool InsertEmployeeL2(Employees_L2 objEmpL2)
        {
            string strSQl = string.Empty;
            int result = -1;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EmpID",
                    Value = objEmpL2.EmpID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "L2ID",
                    Value = objEmpL2.L2ID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedBy",
                    Value = objEmpL2.CreatedBy,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedDate",
                    Value = objEmpL2.CreatedDate,
                    DBType = DbType.DateTime

                });

                strSQl = "Insert Into employees_L2(L2ID,empID,CreatedBy,CreatedDate) Values(@L2ID,@EmpID,@CreatedBy,@CreatedDate)";

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, parameters);

                    if (result == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool DeleteEmployee_L2ByList(List<Employees_L2> lstEmpL2, int employeeID)
        {
            string strSQL = " Delete from Employees_L2 Where EmpID = " + employeeID;
            StringBuilder strDeleteList = new StringBuilder();
            int result = -1;

            try
            {
                foreach (Employees_L2 item in lstEmpL2)
                {
                    if (strDeleteList.Length == 0)
                    {
                        strDeleteList.Append(item.L2ID);
                    }
                    else
                    {
                        strDeleteList.Append("," + item.L2ID);
                    }
                }

                strSQL += " And  L2ID in (" + strDeleteList.ToString() + ")";

                if (strDeleteList.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteEmployee_L3ByList(List<Employees_L3> lstEmpL3, int employeeID)
        {
            string strSQL = " Delete from Employees_L3 Where EmpID = " + employeeID;
            StringBuilder strDeleteList = new StringBuilder();
            int result = -1;

            try
            {
                foreach (Employees_L3 item in lstEmpL3)
                {
                    if (strDeleteList.Length == 0)
                    {
                        strDeleteList.Append(item.L3ID);
                    }
                    else
                    {
                        strDeleteList.Append("," + item.L3ID);
                    }
                }

                strSQL += " And  L3ID in (" + strDeleteList.ToString() + ")";

                if (strDeleteList.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertEmployee_L2ByList(List<Employees_L2> lstEmpL2)
        {
            string strSQL = " insert into Employees_L2 (L2ID,empID,CreatedBy,CreatedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int empID = 0;
            int result = -1;

            string oraFrom = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";
            }

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });


            try
            {

                foreach (Employees_L2 item in lstEmpL2)
                {
                    if (strInsertValues.Length == 0)
                    {
                        empID = item.EmpID;
                        strInsertValues.Append(" Select " + item.L2ID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                    else
                    {
                        strInsertValues.Append("  Union All Select " + item.L2ID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                }

                if (empID > 0)
                {
                    DeleteEmployee_L2ByList(lstEmpL2, empID);
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertEmployee_L3ByList(List<Employees_L3> lstEmpL3, List<Employees_L2> lstEmpL2)
        {
            string strSQL = " insert into Employees_L3 (L3ID,empID,CreatedBy,CreatedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            StringBuilder strL3IDList = new StringBuilder();
            StringBuilder strL2IDList = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int empID = 0;
            int result = -1;
            string oraFrom = string.Empty;

            Collection<DBParameters> oraparameters = new Collection<DBParameters>();

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";


                oraparameters.Add(new DBParameters()
                {
                    Name = "EngDate",
                    Value = DateTime.Now,
                    DBType = DbType.DateTime
                });
            }
            else
            {
                oraparameters.Add(new DBParameters()
                {
                    Name = "EngDate",
                    Value = Common.GetEnglishDate(DateTime.Now),
                    DBType = DbType.DateTime
                });
            }

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });

            try
            {

                foreach (Employees_L3 item in lstEmpL3)
                {
                    if (strInsertValues.Length == 0)
                    {
                        empID = item.EmpID;
                        strL3IDList.Append(item.L3ID);
                        strInsertValues.Append(" Select " + item.L3ID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                    else
                    {
                        strL3IDList.Append("," + item.L3ID);
                        strInsertValues.Append("  Union All Select " + item.L3ID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                }

                if (empID > 0)
                {
                    DeleteEmployee_L3ByList(lstEmpL3, empID);
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            if (strL3IDList.Length > 0)
                            {
                                strSQL = " insert into Employees_L2 (L2ID,empID,CreatedBy,CreatedDate) Select Distinct L2ID," + empID + "," + employeeID + ",@CurrentDate from L3 ";
                                strSQL += " Where L3ID in (" + strL3IDList.ToString() + ") And L2ID Not IN ( Select L2ID from Employees_L2 Where EmpID=" + empID + ")";

                                result = objContext.ExecuteQuery(strSQL, parameters);
                            }

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                foreach (Employees_L2 item in lstEmpL2)
                {
                    if (strL2IDList.Length == 0)
                    {
                        empID = item.EmpID;
                        strL2IDList.Append(item.L2ID);
                    }
                    else
                    {
                        strL2IDList.Append("," + item.L2ID);
                    }
                }

                if (strL2IDList.Length > 0)
                {
                    strSQL = " insert into Employees_L3 (L3ID,empID,CreatedBy,CreatedDate) Select L3ID," + empID + "," + employeeID + ",@EngDate from L3 Where L2ID IN(" + strL2IDList.ToString() + ") AND L3ID Not IN (Select L3ID from Employees_L3 Where EmpID= " + empID + ")";
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, oraparameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertEmployee_L2L3ByL1(List<Int32> lstL1s, int empiD)
        {
            int createdBy = ProjectSession.EmployeeID;

            string strL2SQL = string.Empty;
            string strL3SQl = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            int loggedinEmpID = ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID;
            try
            {
                foreach (int orgID in lstL1s)
                {
                    parameters = new Collection<DBParameters>();

                    parameters.Add(new DBParameters()
                   {
                       Name = "EmpID",
                       Value = empiD,
                       DBType = DbType.Int32
                   });

                    parameters.Add(new DBParameters()
                    {
                        Name = "L1ID",
                        Value = orgID,
                        DBType = DbType.Int32
                    });

                    if (loggedinEmpID > 0)
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "LoggedInEmpID",
                            Value = loggedinEmpID,
                            DBType = DbType.Int32
                        });
                    }
                    if(ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = System.DateTime.Now,
                            DBType = DbType.DateTime
                        });
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = "CurrentDate",
                            Value = Common.GetEnglishDate(System.DateTime.Now),
                            DBType = DbType.DateTime
                        });
                    }

                    strL2SQL = "  insert into Employees_L2 (L2ID,empID,CreatedBy,CreatedDate) ";
                    strL2SQL += " Select L2ID, " + empiD + ", " + createdBy + ",@CurrentDate from L2 ";
                    strL2SQL += " Where L2.L1ID = @L1ID AND L2.Status = 1 AND L2.L2ID NOT IN (Select L2ID From Employees_L2 Where EMPID = @EmpID)";

                    strL3SQl = "  insert into Employees_L3 (L3ID,empID,CreatedBy,CreatedDate) ";
                    strL3SQl += " Select L3ID, " + empiD + ", " + createdBy + ",@CurrentDate from L3 ";
                    strL3SQl += " Where L3.L2ID IN (Select L2ID From L2 Where L2.L1ID = @L1ID AND L2.Status = 1) AND L3.L3ID NOT IN (Select L3ID From Employees_L3 Where EMPID = @EmpID)";

                    if (loggedinEmpID > 0)
                    {
                        strL2SQL += " AND L2.L2ID IN (Select L2ID From Employees_L2 Where EMPID = @LoggedInEmpID)";
                        strL3SQl += " AND L3.L3ID IN (Select L3ID From Employees_L3 Where EMPID = @LoggedInEmpID)";
                    }

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objContext.ExecuteQuery(strL2SQL, parameters);
                        objContext.ExecuteQuery(strL3SQl, parameters);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Employee SubDept Permission"

        public bool DeleteEmployee_MaintSubDept(int employeeID, int subDeptID)
        {
            string strSQl = string.Empty;
            int result = -1;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EmpID",
                    Value = employeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "MaintSubDeptID",
                    Value = subDeptID,
                    DBType = DbType.Int32
                });

                strSQl = "Delete From Employees_MaintSubDept Where  EmpId = @EmpID AND MaintSubDeptID = @MaintSubDeptID";

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, parameters);

                    if (result == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool InsertEmployee_MaintSubDept(Employees_L2 objEmpL2)
        {
            string strSQl = string.Empty;
            int result = -1;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EmpID",
                    Value = objEmpL2.EmpID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "L2ID",
                    Value = objEmpL2.L2ID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedBy",
                    Value = objEmpL2.CreatedBy,
                    DBType = DbType.String
                });

                parameters.Add(new DBParameters()
                {
                    Name = "CreatedDate",
                    Value = objEmpL2.CreatedDate,
                    DBType = DbType.DateTime

                });

                strSQl = "Insert Into employees_L2(L2ID,empID,CreatedBy,CreatedDate) Values(@L2ID,@EmpID,@CreatedBy,@CreatedDate)";

                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.ExecuteQuery(strSQl, parameters);

                    if (result == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        public bool InsertEmployee_MaintSubDeptByList(List<Employees_MaintSubDept> lstEmpSubDept)
        {
            string strSQL = " insert into Employees_MaintSubDept ( MaintSubDeptID,EmpId,CreatedBy,CreatedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int empID = 0;
            int result = -1;

            string oraFrom = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";
            }

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });

            try
            {

                foreach (Employees_MaintSubDept item in lstEmpSubDept)
                {
                    if (strInsertValues.Length == 0)
                    {
                        empID = item.EmpId;
                        strInsertValues.Append(" Select " + item.MaintSubDeptID + "," + item.EmpId + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                    else
                    {
                        strInsertValues.Append("  Union All Select " + item.MaintSubDeptID + "," + item.EmpId + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                }

                if (empID > 0)
                {
                    DeleteEmployee_MaintSubDeptByList(lstEmpSubDept, empID);
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteEmployee_MaintSubDeptByList(List<Employees_MaintSubDept> lstEmpSubDept, int employeeID)
        {
            string strSQL = " Delete from Employees_MaintSubDept Where EmpID = " + employeeID;
            StringBuilder strDeleteList = new StringBuilder();
            int result = -1;

            try
            {
                foreach (Employees_MaintSubDept item in lstEmpSubDept)
                {
                    if (strDeleteList.Length == 0)
                    {
                        strDeleteList.Append(item.MaintSubDeptID);
                    }
                    else
                    {
                        strDeleteList.Append("," + item.MaintSubDeptID);
                    }
                }

                strSQL += " And  maintSubDeptID in (" + strDeleteList.ToString() + ")";

                if (strDeleteList.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertEmployee_MaintSubDeptByMaintDivAndDept(List<Int32> lstMaintDepts, List<Int32> lstMaintDivs, int empiD, int loggedInEmployeeID)
        {
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });

            int createdBy = ProjectSession.EmployeeID;

            string strBaseSQL = " insert into Employees_MaintSubDept ( MaintSubDeptID,EmpId,CreatedBy,CreatedDate) ";
            strBaseSQL += " Select MaintSubDeptID, " + empiD + ", " + createdBy + ",@CurrentDate from MaintSubDept";

            string strSQl = string.Empty;
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int result = 0;

            try
            {
                foreach (int deptId in lstMaintDepts)
                {
                    if (strInsertValues.Length == 0)
                    {
                        strInsertValues.Append(deptId);
                    }
                    else
                    {
                        strInsertValues.Append("," + deptId);
                    }
                }


                if (strInsertValues.Length > 0)
                {
                    strSQl += " Where MaintDeptID In(" + strInsertValues.ToString() + ") AND MaintSubDeptID Not In (Select MaintSubDeptID From Employees_MaintSubDept Where EmpID = " + empiD + ")";

                    if (loggedInEmployeeID > 0)
                    {
                        strSQl += " AND MaintSubDeptID IN ( Select MaintSubDeptID From MaintSubDept Where MaintSubDeptID IN ( Select MaintSubDeptID from Employees_MaintSubDept Where EmpID =" + loggedInEmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + loggedInEmployeeID + ") AND MaintDeptID In(" + strInsertValues.ToString() + ") ) ";
                    }
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strBaseSQL + strSQl, parameters);
                    }
                }

                if (result == 0)
                {
                    strInsertValues.Length = 0;

                    foreach (int divID in lstMaintDivs)
                    {
                        if (strInsertValues.Length == 0)
                        {
                            strInsertValues.Append(divID);
                        }
                        else
                        {
                            strInsertValues.Append("," + divID);
                        }
                    }

                    if (strInsertValues.Length > 0)
                    {
                        string strMaintDivQuery = "Select maintDeptID from MaintenanceDepartment Where MaintDivisionID IN (" + strInsertValues.ToString() + ")";
                        strSQl = " Where MaintDeptID In(" + strMaintDivQuery + ") AND MaintSubDeptID Not In (Select MaintSubDeptID From Employees_MaintSubDept Where EmpID = " + empiD + ")";
                        if (loggedInEmployeeID > 0)
                        {
                            strSQl += " AND MaintSubDeptID IN ( Select MaintSubDeptID From MaintSubDept Where MaintSubDeptID IN ( Select MaintSubDeptID from Employees_MaintSubDept Where EmpID =" + loggedInEmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + loggedInEmployeeID + ") AND MaintDeptID In(" + strMaintDivQuery + "))";
                        }
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.ExecuteQuery(strBaseSQL + strSQl, parameters);
                        }

                        if (result != 0)
                        {
                            return false;
                        }
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IList<MaintSubDept> GetSubDeptsWithCheckBox(int logedInEmpID, int empID, int maintDeptID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                IList<MaintSubDept> list = new List<MaintSubDept>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                string strWhereClause = string.Empty;
                string strWhereAuto = string.Empty;
                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "MAINTDEPTID",
                    Value = maintDeptID,
                    DBType = DbType.Int32
                });

                if (logedInEmpID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LOGEDINEMPID",
                        Value = logedInEmpID,
                        DBType = DbType.Int32
                    });

                    query = @"Select MaintSubDept.MaintSubDeptID,MaintSubDept.MaintDeptID,MaintSubDept.MaintSubDeptCode,
                             MaintSubDept.MaintSubDeptDesc,MaintSubDept.MaintSubDeptAltDesc,MaintSubDept.CreatedBy,
                            MaintSubDept.CreatedDate,MaintSubDept.ModifiedBy,MaintSubDept.ModifiedDate,  
                            Case ISNULL( (Select MaintSubDeptID from Employees_MaintSubDept Where EmpID = @EMPID  AND MaintSubDeptID = MaintSubDept.MaintSubDeptID)  ,0)  when 0  then 
                                ( Case ISNULL( (Select employees.MaintSubDeptID from employees Inner Join MaintSubDept on employees.MaintSubDeptID =
							         MaintSubDept.MaintSubDeptID  AND  employees.EmployeeID =  @EMPID AND MaintSubDept.MaintDeptID = @MAINTDEPTID )  ,0)  when 0  then 0 Else 1 end )
                            else 1 end as ChkMaintSubDept 
                             from MaintSubDept  Where MaintSubDept.maintDeptID = @MAINTDEPTID 
                             AND MaintSubDept.MaintSubDeptID IN (select MaintSubDeptID from Employees_MaintSubDept where empID = @LOGEDINEMPID union Select MaintSubDeptID from employees where employeeId = @LOGEDINEMPID)";
                }
                else
                {

                    query = @"Select  t.*, 
                            Case ISNULL( (Select MaintSubDeptID from Employees_MaintSubDept Where EmpID = @EMPID AND MaintSubDeptID = t.MaintSubDeptID )  ,0)  when 0  then 
                                    (Case ISNULL( (Select employees.MaintSubDeptID from employees Inner Join MaintSubDept on employees.MaintSubDeptID =
							         MaintSubDept.MaintSubDeptID  where  employees.EmployeeID =  @EMPID AND MaintSubDept.MaintDeptID = t.maintDeptID )  ,0)  when 0  then 0 Else 1 end)
                                else 1 end as ChkMaintSubDept 
				            from (	Select *	from MaintSubDept union 
						            Select MaintSubDept.* from MaintSubDept Where MaintSubDeptID = (Select MaintSubDeptID from employees where employeeid = @EMPID) 
						            ) t
                                     Where t.maintDeptID = @MAINTDEPTID ";

                }

                SearchFieldService objSearchField = new SearchFieldService();

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }
                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<MaintSubDept>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Employee Location Permission"

        public IList<Employees_Location> GetLocationsCheckBox(int empID, int cityID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<Employees_Location> list = new List<Employees_Location>();
            string strWhereClause = string.Empty;
            string strWhereAuto = string.Empty;
            string paramName = string.Empty;
            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "LocationId";
            }

            string query = string.Empty;

            //string query = "Select (case isnull((Select employees_Location.LocationId from employees_Location where employees_Location.LocationId=Location.LocationID " +
            //                "and employees_Location.empID = " + empID + " ),0)  when 0  then 0 else 1 end) as ChkLocation,Location.* from Location Where 1=1 ";

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "EMPID",
                Value = empID,
                DBType = DbType.Int32
            });

            if (ProjectSession.IsCentral)
            {
                query = @"Select 
                        (case isnull((Select employees_Location.LocationId from employees_Location where employees_Location.LocationId=Location.LocationID and employees_Location.empID = @EMPID ),0) 
                         when 0  then 0 else 1 end) as ChkLocation,
                         Location.LocationID, Location.LocationNo,Location.LocationDescription, Location.LocationAltDescription,
                         L2.L2ID,L2.L2Code, L2.L2name, L2.L2AltName,L1.L1ID, L1.L1No, L1.L1Name, L1.L1AltName, L5.L5ID, L5.L5No,L5.L5Description,L5.L5AltDescription
                         from Location
	                     Inner JOIN
                         (Select L2ID from  Employees_L2 el2 Where el2.EmpID = @EMPID Union Select L2ID from employees Where EmployeeID = @EMPID) l2Rights
                         On Location.L2ID = l2Rights.L2ID
                         INNER JOIN L2 On l2Rights.L2ID = L2.L2ID 
                         AND L2.Status = 1 
                         Inner JOIN L1
                         On L1.L1ID = L2.L1ID
                         LEFT JOIN L5
                         On Location.L5ID = L5.L5ID";
            }
            else
            {
                parameters.Add(new DBParameters()
                {
                    Name = "LOGGEDINEMPID",
                    Value = ProjectSession.EmployeeID,
                    DBType = DbType.Int32
                });


                query = @"Select 
                        (case isnull((Select employees_Location.LocationId from employees_Location where employees_Location.LocationId=Location.LocationID and employees_Location.empID = @EMPID ),0) 
                         when 0  then 0 else 1 end) as ChkLocation,
                         Location.LocationID, Location.LocationNo,Location.LocationDescription, Location.LocationAltDescription,
                         L2.L2ID,L2.L2Code, L2.L2name, L2.L2AltName,L1.L1ID, L1.L1No, L1.L1Name, L1.L1AltName, L5.L5ID, L5.L5No,L5.L5Description,L5.L5AltDescription
                         from Location
	                        Inner JOIN
                         (Select L2ID from  Employees_L2 el2 Where el2.EmpID = @EMPID Union Select L2ID from employees Where EmployeeID = @EMPID) l2Rights
                         On Location.L2ID = l2Rights.L2ID
                         INNER JOIN L2 On l2Rights.L2ID = L2.L2ID 
                         AND L2.Status = 1 
                         Inner JOIN
                         (Select LocationID from  Employees_Location el1 Where el1.EmpID = @LOGGEDINEMPID) l2LogedInUserRights
                         On Location.LocationID = l2LogedInUserRights.LocationID
                         Inner JOIN L1
                         On L1.L1ID = L2.L1ID
                         LEFT JOIN L5
                         On Location.L5ID = L5.L5ID";
            }

            SearchFieldService objSearchField = new SearchFieldService();
            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query = "Select * from (" + query + ") A Where " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<Employees_Location>(query, parameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public bool DeleteEmployee_LocationByList(List<Employees_Location> lstEmpLocation, int employeeID)
        {
            string strSQL = " Delete from Employees_Location Where EmpID = " + employeeID;
            StringBuilder strDeleteList = new StringBuilder();
            int result = -1;

            try
            {
                foreach (Employees_Location item in lstEmpLocation)
                {
                    if (strDeleteList.Length == 0)
                    {
                        strDeleteList.Append(item.LocationID);
                    }
                    else
                    {
                        strDeleteList.Append("," + item.LocationID);
                    }
                }

                strSQL += " And  LocationID in (" + strDeleteList.ToString() + ")";

                if (strDeleteList.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, new Collection<DBParameters>());

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertEmployee_LocationByList(List<Employees_Location> lstEmpLocation)
        {
            string strSQL = " insert into Employees_Location (LocationID,empID,CreatedBy,CreatedDate) ";
            StringBuilder strInsertValues = new StringBuilder();
            int employeeID = ProjectSession.EmployeeID;
            int empID = 0;
            int result = -1;
            string oraFrom = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                oraFrom = " FROM DUAL ";
            }

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            parameters.Add(new DBParameters()
            {
                Name = "CurrentDate",
                Value = DateTime.Now,
                DBType = DbType.DateTime
            });

            try
            {

                foreach (Employees_Location item in lstEmpLocation)
                {
                    if (strInsertValues.Length == 0)
                    {
                        empID = item.EmpID;
                        strInsertValues.Append(" Select " + item.LocationID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                    else
                    {
                        strInsertValues.Append("  Union All Select " + item.LocationID + "," + item.EmpID + "," + employeeID + ",@CurrentDate " + oraFrom);
                    }
                }

                if (empID > 0)
                {
                    DeleteEmployee_LocationByList(lstEmpLocation, empID);
                }

                strSQL += strInsertValues.ToString();

                if (strInsertValues.Length > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        result = objContext.ExecuteQuery(strSQL, parameters);

                        if (result == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
