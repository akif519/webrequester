﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.Model;
using CMMS.DAL;
using System.Collections.ObjectModel;
using CMMS.Service;
using CMMS.Infrastructure;
using System.Data;

namespace CMMS.Services
{
    public class DatabaseAuditTrailServices : DBExecute
    {
        /// <summary>
        /// Get Employee List As per the conditions
        /// </summary>
        /// <param name="empID"></param>
        /// <param name="pageNo"></param>
        /// <param name="strWhere"></param>
        /// <param name="sortExpression"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public IList<TEntity> GetDataBaseAuditTrial<TEntity>(int pageNo, string strWhere, string sortExpression, string sortDirection, string strTableName)
        {

            Collection<DBParameters> outParameters = new Collection<DBParameters>();

            if (sortExpression == null || sortExpression == string.Empty)
            {
                sortExpression = "ModifiedBy";
            }

            string query = GetDatabaseAuditTrialQuery(strWhere, strTableName);

            if (strTableName == "mr_authorisation_status" || strTableName == "irapprovallevelmappings" || strTableName == "irapprovallevelmappings_employees" || strTableName == "assigntoworkorder")
            {

            }
            else if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                var list = objDapperContext.AdvanceSearch<TEntity>(query, outParameters, pageNo);
                this.PagingInformation = objDapperContext.PagingInformation;
                return list;
            }
        }

        public bool DeleteAuditTrialAllRecords(string strTableName)
        {
            try
            {
                if ((!string.IsNullOrEmpty(strTableName)) && (strTableName.ToLower().Contains("cdc.dbo_")))
                {
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        string strQuery = string.Empty;
                        Collection<DBParameters> parameters = new Collection<DBParameters>();

                        strQuery = @"Delete From " + strTableName;

                        objDapperContext.ExecuteQuery(strQuery, parameters);

                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Get Database Audit Trial Query based on Entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        private string GetDatabaseAuditTrialQuery(string strWhere, string strTableName)
        {

            string query = string.Empty;

            switch (strTableName.ToLower())
            {
                case "workorders":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                               " C.Workorderno, C.ProblemDescription,ws.WorkStatus,wt.WorkTypeDescription,wp.WorkPriority,wtr.WorkTrade,C.PMTarStartDate, C.PMTarCompDate,C.AstartDate,C.AEndDate," +
                                               "a.AssetNumber,l.LocationNo,L2.L2Code,L3.L3No,L4.L4No,L5.L5No,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,C.ActionTaken, C.PreventionTaken," +
                                                  "C.DateReceived,C.DateRequired,C.ActDateStart,C.ActDateEnd,C.EstDateStart,C.EstDateEnd,C.DateHandover,C.WOCost,C.EstDuration,wclose.Name as WOClosedBy,mg.GroupCode," +
                                                  "s.SupplierNo,accpt.Name as AcceptedName,fc.FailureCauseCode,cc.CostCenterNo,pc.CheckListNo,rating.RateNo,emp.Name as AuthEmp,req.Name Requester," +
                                               "C.WOLaborCost,C.WODICost,C.WOPartCost,C.WOPMtype,C.PMJOGeneratedReading,C.PMMetReading,C.DownTime,C.LineDowntime,C.MTask,C.Notes,C.print_date,C.AssignTo, " +
                                               "C.TelNo,ac.AccountCode,ds.Statusname," +
                                               " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                               " from CDC.dbo_workorders_CT C Left Outer Join employees req on req.EmployeeId=C.RequestorID Left Outer Join employees emp on emp.EmployeeId=C.EmployeeId inner join L2 On L2.L2Id=C.L2Id " +
                                               " left outer join L3 on L3.L3Id=C.L3Id left outer join L4 On L4.L4ID=C.L4Id left outer join L5 on L5.L5ID=C.L5ID left outer join Location l on l.LocationId=C.LocationId left outer join Assets a on a.AssetId= C.AssetId " +
                                               " inner join Workstatus ws on ws.WorkStatusId=C.workStatusId inner join workpriority wp on wp.WorkPriorityID = C.WorkPriorityID inner join WorkType wt on wt.WorkTypeId = C.WorkTypeId " +
                                               " inner join worktrade wtr on wtr.WorkTradeID=C.WOTradeID inner join MainenanceDivision mdiv on mdiv.MaintDivisionID=C.MaintDivisionID inner join MaintenanceDepartment mdept on mdept.maintDeptID=C.maintDeptID " +
                                               " inner join MaintSubDept msdept on msdept.MaintSubDeptID = C.MaintSubDeptID left outer join MaintGroup mg on mg.GroupId=C.GroupId left outer join Suppliers s on s.SupplierId=C.SupplierId " +
                                               " left outer join FailureCause fc on fc.FailureCauseID=C.FailureCauseID left outer join employees accpt on accpt.EmployeeId=C.AcceptedbyID left outer join pmchecklist pc on pc.CheckListId=C.PMChecklistID  " +
                                               " left outer join rating on rating.RateID=C.RatingsID left outer join employees wclose on wclose.EmployeeId=C.WOclosebyID left outer join costcenter cc on cc.CostCenterId=C.CostCenterId left outer join AccountCode ac on ac.AccCodeid = C.AccountCodeId " +
                                               " left outer join DocumentStatus ds on ds.Documentstatusid=C.Documentstatusid left outer join Employees authemp on authemp.EmployeeId=C.AuthorisedEmployeeID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                               " where 1=1 " + strWhere + " ";
                    break;

                case "assigntoworkorder":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " e.Name as EmployeeName,C.WorkorderNo,C.EstStartDate,C.EstEndDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_assigntoworkorder_CT C inner join employees e on e.EmployeeId = C.EmployeeID " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "workorderlabor":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WorkOrderNo,en.Name as EmployeeName,C.HourlySalary,C.Comment,C.OverTime1,C.OverTime2,C.OverTime3,C.StartDate,C.EndDate,C.TotHour,C.TotCost,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_workorderlabor_CT C inner join employees en on en.EmployeeId = C.EmployeeID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "direct":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WONo,s.SupplierNo,C.StockDesc,C.AltStockDesc,C.Qty,C.Price,C.invoiceNumber,C.Date,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_direct_CT C left outer join Suppliers s on s.supplierId=C.Supplierid left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "issue":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WONo,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.AvgPrice,L2.L2Code,ss.SubStoreCode,C.DateIssue,C.QtyIssue,C.Price3,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_issue_CT C inner join StockCode sc on sc.StockID=C.StockID inner join L2 on L2.L2Id=C.L2Id inner join SubStore ss on ss.SubStoreID = C.SubStoreID left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "return":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                        " C.WONo,sc.StockNo,sc.StockDescription,sc.AltStockDescription,sc.AvgPrice,L2.L2Code,ss.SubStoreCode,C.Date,C.Qty,C.Comment,C.RetPrice,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_return_CT C inner join StockCode sc on sc.StockID=C.StockID inner join L2 on L2.L2Id=C.L2Id inner join SubStore ss on ss.SubStoreID = C.SubStoreID left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "workorderelements":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                   " C.WorkOrderno,C.SeqNo,C.TaskDesc,C.AltTaskDesc,C.Remarks,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                   " from CDC.dbo_workorderelements_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)" +
                                                   "where 1=1 " + strWhere + " ";
                    break;

                case "workorderitems":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WorkorderNo,s.StockNo,s.StockDescription,s.AltStockDescription,C.SeqNo,C.Qty  from CDC.dbo_WorkOrderItems_CT C inner join StockCode s on C.StockId=s.StockId  where 1=1 " + strWhere + " ";
                    break;

                case "jobtask":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WoNo,C.SeqId,C.Details,C.AltDetails,C.Remarks from CDC.dbo_jobtask_CT C  where 1=1 " + strWhere + " ";
                    break;

                case "sitask":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.WoNo,C.SeqId,C.Details,C.AltDetails from CDC.dbo_SITask_CT C  where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_j":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " S.WorkorderNo as No,C.FileName,C.FileDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                             " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  inner join  workorders as S on WorkorderNo=FkId and ModuleType='J' " +
                                             " where 1=1 " + strWhere + " ";
                    break;

                case "assets":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.assetnumber,C.assetdescription,C.assetaltdescription,asts.assetstatusdesc,l.locationno,cat.assetcatcode,l2.l2code,l3.l3no,l4.l4no,l5.l5no,C.modelnumber,C.serialnumber,cr.Criticality, " +
                                                    " wt.WorkTrade,auEmp.Name as AuthEmployee,wc.Warranty_contract,S.SupplierNo,pl.PartsListNo, " +
                                                    " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " FROM cdc.dbo_assets_CT C INNER JOIN assetstatus asts ON asts.assetstatusid = C.assetstatusid INNER JOIN location l ON l.locationid = C.locationid LEFT OUTER JOIN assetcategories cat ON cat.assetcatid = C.assetcatid  " +
                                                    " INNER JOIN l2 ON l.l2id = l2.l2id LEFT OUTER JOIN l3 ON l.l3id = l3.l3id INNER JOIN l4 ON l.l4id = l4.l4id LEFT OUTER JOIN l5 ON l.l5id = l5.l5id left outer join criticality cr on cr.id=C.CriticalityID  " +
                                                    " left outer Join worktrade wt on wt.WorkTradeID=C.WorkTradeID left outer join employees auEmp on auEmp.EmployeeId=C.EmployeeId left outer join warrantycontract wc on wc.Warranty_contractID = C.Warranty_ContractID  " +
                                                    " left outer join Suppliers S on S.SupplierID=C.SupplierID left outer join partslist pl on pl.PartsListID=C.PartsListID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_a":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " S.AssetNumber as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                             " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  assets as S on S.AssetID=FkId and ModuleType='A' " +
                                             " where 1=1 " + strWhere + " ";
                    break;

                case "l4":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    "C.L4No,C.L4Description,C.L4AltDescription,l.L2Code,L3.L3No,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_L4_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join L2 as l on l.L2Id=C.L2Id " +
                                                    "  left outer join L3 on C.L3Id=L3.L3Id where 1=1 " + strWhere + " ";
                    break;

                case "l5":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " C.L5No,C.L5Description,C.L5AltDescription,l.L2Code,L3.L3No,L4.L4No,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_L5_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join L2 as l on l.L2Id=C.L2Id " +
                                                    "  left outer join L3 on C.L3Id=L3.L3Id inner join L4 on L4.L4Id= C.L4Id where 1=1 " + strWhere + " ";
                    break;

                case "location":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " C.LocationNo,C.LocationDescription,C.LocationAltDescription, " +
                                                    " l.L2Code,L3.L3No,L4.L4No,L5.L5No,Criticality.Criticality, " +
                                                    "C.NotetoTech,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    "from cdc.dbo_Location_CT as C left outer join L5 on C.L5Id=L5.L5Id inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join L2 as l on l.L2Id=C.L2Id " +
                                                    "  left outer join L3 on C.L3Id=L3.L3Id inner join L4 on L4.L4Id= C.L4Id left outer join Criticality on Criticality.id=C.CriticalityID where 1=1 " + strWhere + " ";
                    break;

                case "pmschedule":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    "C.PMNo,C.PMName,C.AltPMName, " +
                                                      " L2.L2Code,a.AssetNumber,l.LocationNo,pc.ChecklistNo,wt.WorkTypeDescription,wtr.WorkTrade,C.PeriodDays,C.FreqUnits,C.Frequency,C.TargetStartDate,C.TargetCompDate," +
                                                    " C.ActualCompDate,C.NextDate,C.TypePM,C.PMMultiple,C.PMActive,C.weekofMonth,C.DayOfWeek,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy" +
                                                    " from CDC.dbo_pmschedule_CT C inner join L2 on L2.L2Id=C.L2Id left outer join Assets a on a.AssetID=C.AssetId inner join Location l on l.LocationId=C.PhyLocationID " +
                                                    " inner join pmchecklist pc on pc.ChecklistID=C.ChecklistID  inner join worktype wt on wt.WorkTypeID=C.WorkTypeID inner join WorkTrade wtr on wtr.WorktradeId=C.WorktradeId " +
                                                    " inner join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintDivisionID inner join MaintenanceDepartment mdept on mdept.maintDeptID =C.maintDeptID " +
                                                    " inner join MaintSubDept msdept on msdept.MaintSubDeptID=C.MaintSubDeptID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 and C.IsCleaningModule = 0 and C.PMGroupNo is NULL " + strWhere + " ";
                    break;

                case "multiplepm":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " pms.PMNo,pc.ChecklistNo,C.MSeq,C.MDescription,C.MStartDate,C.MStartDateCount,C.MTaskSeq,C.MPMCounter,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_multiplepm_CT C Inner join pmschedule pms on pms.PMID=C.PMID inner join pmchecklist pc on pc.ChecklistID=C.MTaskID  inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1  and pms.IsCleaningModule = 0 and pms.PMGroupNo is NULL " + strWhere + " ";
                    break;

                case "pmmetermaster":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " C.MeterNo,C.MeterDescription,C.AltMeterDescription,L2.L2Code,a.AssetNumber,C.MeterUnit,C.MeterType,C.LastReading,C.MaxDailyIncrement,C.StartMeterReading,C.Active,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_PMMeterMaster_CT C inner join  L2 on L2.L2Id=C.L2Id inner Join Assets a on a.AssetId=C.AssetId inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "pmmeters":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " PMM.MeterNo as MasterMeterNo,C.MeterNo,C.MeterDescription,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,Low.ChecklistNo as LowChecklistNo, " +
                                                    " High.ChecklistNo as HighChecklistNo,Inc.ChecklistNo as IncChecklistNo,wt.WorkTrade,C.LowerLimit,C.UpperLimit,C.LastPMreading,C.StartMeterReading,C.IncFreq,C.IncPMDue, " +
                                                    " (Case C.PMType when 1 then 'Actual' else 'Schedule' end ) as PMTypeName,C.PMActive,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_pmmeters_CT C inner join PMMeterMaster PMM on PMM.MeterMasterId=C.MeterMasterId inner join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintDivisionID  " +
                                                    " inner join MaintenanceDepartment mdept on mdept.maintDeptID =C.maintDeptID inner join MaintSubDept msdept on msdept.MaintSubDeptID=C.MaintSubDeptID Left outer join PMChecklist Low on Low.CheckListId=C.PMLowTaskId " +
                                                    " Left outer join PMChecklist High on High.CheckListId=C.PMUppTaskId Left outer join PMChecklist Inc on Inc.CheckListId=C.PMIncTaskId Inner join worktrade wt on wt.WorkTradeId=C.WorktradeId inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "pmmeters_assignto":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " PM.MeterNo,e.Name,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                     " from CDC.dbo_pmmeters_assignto_CT C inner join pmmeters PM on PM.MeterId=C.MeterId inner join employees e on e.EmployeeID= C.EmployeeID  where 1=1 " + strWhere + " ";
                    break;

                case "pmchecklist":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " si.SafetyNo,C.CheckListNo,C.CheckListName,C.AltCheckListName,C.EstimatedLaborHours,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy  " +
                                                    " from CDC.dbo_pmchecklist_CT C left outer join tblSafetyinstruction si on si.SafetyId = C.SafetyID left outer join L2 on L2.L2Id=C.L2Id inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Left outer join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintdivId  Left outer join MaintenanceDepartment mdept on mdept.maintDeptID =C.MaintdeptId Left outer join MaintSubDept msdept on msdept.MaintSubDeptID=C.MainSubDeptId " +
                                                    " where 1=1 and C.IsCleaningModule = 0 " + strWhere + " ";
                    break;

                case "checklistelements":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " PC.CheckListNo,C.SeqNo,C.TaskDesc,C.AltTaskDesc,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate , e.Name as ModifiedBy " +
                                                    " from CDC.dbo_checklistelements_CT C inner join PMchecklist PC on PC.CheckListId=C.CheckListId" +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 and PC.IsCleaningModule = 0 " + strWhere + " ";
                    break;

                case "checklistinv":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " PC.CheckListNo,chke.SeqNo,s.StockNo,s.StockDescription,s.AltStockDescription,C.Qty,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate , e.Name as ModifiedBy " +
                                                    " from CDC.dbo_checklistInv_CT C inner join PMchecklist PC on PC.CheckListId=C.CheckListId inner join StockCode s on s.StockId=C.PartId inner join checklistelements chke on chke.CheckListelementId=C.ChecklistelementId" +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 and PC.IsCleaningModule = 0 " + strWhere + " ";

                    break;

                case "extassetfiles_p":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " S.ChecklistNo as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                             " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  Pmchecklist as S on S.ChecklistID=FkId and ModuleType='P' " +
                                             " where 1=1 and S.IsCleaningModule = 0 " + strWhere + " ";
                    break;

                case "substore":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " C.SubStoreCode,C.SubStoreDesc,C.AltSubStoreDesc,l.L2Code,C.isDefault,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_substore_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join L2 as l on l.L2Id=C.L2Id " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "attrgroup":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " C.AttrName,C.AltAttrName,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_attrgroup_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "attrsubgroup":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " C.AttrSubName,C.AltAttrSubName,attrgroup.AttrName,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    "  from cdc.dbo_attrsubgroup_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    "  inner join attrgroup on attrgroup.AttrID=C.AttrID where 1=1 " + strWhere + " ";
                    break;

                case "partlocation":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " l.L2Code,C.PartLocation,substore.SubStoreCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_PartLocation_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                                                    "  Inner join substore on substore.SubStoreID = C.SubStoreID Inner join L2 as l on l.L2Id=substore.L2Id   where 1=1 " + strWhere + " ";
                    break;

                case "stockcode":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " C.StockNo,C.StockDescription,C.AltStockDescription,C.Status,C.Specification,C.Manufacturer,C.CriticalityID,AG.AttrName,ASG.AttrSubName,C.UOM, " +
                                                    " C.Price2,C.Notes,C.LeadTime,C.AvgPrice,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_StockCode_CT as C  inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) Left outer join attrGroup as AG on  AG.AttrID = C.AttrGroupID " +
                                                    " left outer join attrsubgroup as ASG on ASG.AttrSubID=C.AttrSubID left outer join Suppliers as S on S.SupplierID=C.default_supplier_id where 1=1 " + strWhere + " ";
                    break;

                case "stock_alternative":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " s.StockNo,al.StockNo as AlternativeStockCodeNo,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                     " from CDC.dbo_Stock_alternative_CT as C inner join StockCode as s on s.StockId=C.stockId  inner join StockCode as al on al.stockId=C.AlternativePart" +
                                                     " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 " + strWhere + " ";
                    break;

                case "stock_code_supplier":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " s.StockNo,Sp.SupplierNo,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                     " from cdc.dbo_stock_code_supplier_CT C inner join StockCode s on s.StockID=C.stock_id inner join suppliers Sp on Sp.SupplierID = C.supplier_id " +
                                                     " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 " + strWhere + " ";
                    break;

                case "stockcode_levels":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                     " st.SubStoreCode,s.StockNo,pl.PartLocation,C.max_level,C.re_order_level,C.min_level,C.reorder_qty,C.Balance, " +
                                                     " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                     " from CDC.dbo_stockcode_levels_ct C inner join StockCode s on s.StockId=C.Stock_id inner join substore st on st.SubStoreID = C.sub_store_id " +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) left outer join PartLocation pl on pl.ID=C.PartLocationID where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_s":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " S.StockNo as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  StockCode as S on StockID=FkId and ModuleType='S'  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "material_req_master":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " C.mr_no,C.mr_date,C.order_date,mrs.Status_Desc,mas.auth_status_desc,req.Name as RequestBy,s.SupplierNo,C.requiredby_date,C.mr_notes,C.cancel_date,cnl.Name as CancelBy,C.cancel_notes, " +
                                                 " L2.L2Code,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,cc.CostCenterNo,ac.AccountCode,ss.Name as SectionSupName,stSup.Name as StoreSupName,SubStore.SubStoreCode,C.PrintDate, " +
                                                "C.ModifiedDate,e.Name as ModifiedBy " +
                                                   " FROM CDC.dbo_material_req_master_CT as C inner join material_req_status mrs ON C.status_id = mrs.mr_status_id LEFT OUTER JOIN mr_authorisation_status mas ON mas.auth_status_id = C.auth_status " +
                                                    " left outer join employees req on req.EmployeeId = C.req_by left outer join suppliers s on s.SupplierID=C.supplier_Id left outer join employees cnl on cnl.EmployeeId = C.cancel_by " +
                                                    " inner join L2 on L2.L2Id = C.L2Id left outer join MainenanceDivision mdiv on mdiv.MaintDivisionID=C.MaintdivId left outer join MaintenanceDepartment mdept on mdept.maintDeptID= C.MaintdeptId " +
                                                    " left outer join MaintSubDept msdept on msdept.MaintSubDeptID=C.MainSubDeptId left outer join CostCenter cc on cc.CostCenterId = C.costCenterID " +
                                                    " left outer join AccountCode ac on ac.AccCodeid= C.accountcodeid left outer join Employees ss on ss.EmployeeId=C.SectionSupId " +
                                                    " left outer join Employees stSup on stSup.EmployeeId=C.StoreSupId and stSup.IsStoredSupervisor = 1 inner join SubStore on SubStore.SubStoreID = C.SubStoreId " +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "material_req_details":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " C.partid,s.StockNo,C.Part_desc,C.Altpart_desc,C.workorderno,l.LocationNo,a.AssetNumber, " +
                                                 " C.UOM,C.Qty, C.ReceivedQty,C.CanceledQty, " +
                                                    "whs.Name as SubwhseSup,whi.Name as SubWhseIssueTech, " +
                                                    " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_material_req_details_CT C Left outer join stockcode s on s.StockID=C.partid left outer join Assets a on a.AssetID=C.AssetId " +
                                                    "inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " left outer join location l on l.locationid= C.locationid left outer join employees whs on whs.EmployeeId = C.SubWHSupId left outer join employees whi on whi.EmployeeId = C.SubWHIssueTechId " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "employees":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.EmployeeNo,C.Name,L2.L2Code,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,C.Positions, " +
                                                 "cat.CategoryName,es.EmployeeStatus,lng.Language,C.WorkPhone,C.HandPhone,C.HousePhone,C.Email,C.Fax,C.Address,C.OfficeLocation,C.HourlySalary, " +
                                                 " C.OverTime1,C.OverTime2,C.OverTime3,C.IsStoredSupervisor,C.UserId,C.Central, " +
                                                    " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_Employees_CT C inner join L2 on L2.L2Id= C.L2Id inner join employeecategories cat on cat.CategoryId=C.CategoryId left outer join worktrade wt on wt.WorktradeId =C.worktradeid " +
                                                    " left outer join UserGroups ug on ug.UserGroupId = C.UserGroupId inner join MainenanceDivision mdiv on mdiv.MaintDivisionID=C.MaintDivisionID inner join MaintenanceDepartment mdept on mdept.maintDeptID=C.maintDeptID " +
                                                    " inner join MaintSubDept msdept on msdept.MaintSubDeptID=C.MaintSubDeptID inner join employeeStatuses  es on es.EmployeeStatusId=C.EmployeeStatusId " +
                                                    " inner join tbl_LanguageSelection lng on lng.LanguageCode=C.LanguageCode inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "l2":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.L2Code,C.L2name,C.L2Altname,C.DefaultCalendar,C.Timezone,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_L2_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "l3":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.L3No,C.L3Desc,C.L3AltDesc,l.L2Code,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_L3_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join L2 as l on l.L2Id=C.L2Id " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "mainenancedivision":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.MaintDivisionCode,C.MaintDivisionName,C.MaintDivisionAltName,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_MainenanceDivision_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "maintenancedepartment":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.MaintDeptCode,C.MaintDeptdesc,C.MaintDeptAltdesc,M.MaintDivisionCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_MaintenanceDepartment_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " inner join MainenanceDivision as M on M.MaintDivisionID=C.MaintDivisionID where 1=1 " + strWhere + " ";
                    break;

                case "maintsubdept":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                   " C.MaintSubDeptCode,C.MaintSubDeptDesc,C.MaintSubDeptAltDesc,M1.MaintDivisionCode,M.MaintDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_MaintSubDept_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " inner join MaintenanceDepartment as M on M.maintDeptID=C.MaintDeptID inner join MainenanceDivision as M1 on M1.MaintDivisionID=M.MaintDivisionID " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "maintgroup":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.GroupCode,C.GroupDesc,C.GroupAltDesc,L2.L2Code,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_MaintGroup_ct as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join L2 on L2.L2Id=C.L2Id where 1=1 " + strWhere + " ";
                    break;

                case "failurecause":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.FailureCauseCode,C.FailureCauseDescription,C.FailureCauseAltDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_failurecause_ct as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "suppliers":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.SupplierNo,C.SupplierName,C.AltSupplierName," +
                                                   " C.ContactName,C.ContactTitle,C.Address,C.City,C.PostalCode,C.StateOrProvince,C.Country,C.PhoneNumber,C.FaxNumber,C.Services, " +
                                                 " C.Phone1,C.ContactName2,C.ContactTitle2,C.Phone2,C.ContactName3,C.ContactTitle3,C.Phone3, " +
                                                   "isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                  " from CDC.dbo_Suppliers_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "partslist":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.PartsListNo,C.PartsListDescription,C.PartsListAltDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " FROM CDC.dbo_partslist_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "partslistdetails":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " partslist.PartsListNo,stockcode.StockNo,C.PartsQty,C.comments,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_partslistdetails_CT as C Left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join partslist on partslist.PartsListID=C.PartsListID inner join stockcode on stockcode.StockID = C.StockID  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "costcenter":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " C.CostCenterNo,C.CostCenterName,C.AltCostCenterName,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_costCenter_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "accountcode":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " C.AccountCode,C.AccountCodeDesc,C.AltAccountCodeDesc,CostCenter.CostCenterNo, C.DateStart, C.DateEnd, C.Status, " +
                                                  " isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_accountcode_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join CostCenter on CostCenter.CostCenterId=C.CostCenterId  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "tbljobplan":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                               " C.JobPlanNo,C.JobPlanName,C.AltJobPlanName,L2.L2Code,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_tblJobPlan_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join L2 on L2.L2Id=C.L2Id  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "tbljobplanitems":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                "  tblJobPlan.JobPlanNo,C.SeqID,C.Description,C.AltDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_tblJobPlanItems_CT as C Left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join tblJobPlan on tblJobPlan.JobPlanID=C.JobPlanID  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "tblsafetyinstruction":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " C.SafetyNo,C.SafetyName,C.AltSafetyName,L2.L2Code,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " FROM CDC.dbo_tblSafetyinstruction_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) Left outer join L2 on L2.L2Id=C.L2Id " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "tblsafetyinstructionitems":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " tblSafetyinstruction.SafetyNo,C.seqID,C.Description,C.AltDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_tblSafetyinstructionItems_CT as C Left outer join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Inner join tblSafetyinstruction on tblSafetyinstruction.SafetyID=C.SafetyID   " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "mr_authorisation_status":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                "C.auth_status_desc,C.Altauth_status_desc " +
                                                " from CDC.dbo_mr_authorisation_status_CT as C  where 1=1 " + strWhere + " ";
                    break;

                case "irapprovallevels":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                " C.LevelName,C.AltLevelName,C.LevelNo,C.IsFinalLevel,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_IRApprovalLevels_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "irapprovallevelmappings":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " l.L2Code,substore.SubStoreCode,IAL.LevelName,IAL.LevelNo,mas.auth_status_desc, " +
                                                 " C.Mandatory,C.IsPrintingEnable  " +
                                                    " from CDC.dbo_IRApprovalLevelMappings_CT as C inner join mr_authorisation_status as mas on mas.auth_status_id=C.auth_status_id  " +
                                                    " inner join IRApprovalLevels as IAL  on IAL.IRApprovalLevelId =C.IRApprovalLevelId Inner join substore on substore.SubStoreID =C.SubStoreId  " +
                                                    "  inner join L2 as l on l.L2Id= substore.L2Id  where 1=1 " + strWhere + " ";
                    break;

                case "irapprovallevelmappings_employees":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " e.Name,l.L2Code,substore.SubStoreCode,IAL.LevelName,IAL.LevelNo,mas.auth_status_desc " +
                                                    " from CDC.dbo_IRApprovalLevelMappings_employees_CT as C inner join IRApprovalLevelMappings as M on C.AutoId=M.AutoId Inner join Employees as e on e.EmployeeId = C.EmployeeId " +
                                                    " inner join mr_authorisation_status as mas on mas.auth_status_id=M.auth_status_id inner join IRApprovalLevels as IAL  on IAL.IRApprovalLevelId =M.IRApprovalLevelId   " +
                                                    "  Inner join substore on substore.SubStoreID =M.SubStoreId  inner join L2 as l on l.L2Id= substore.L2Id  where 1=1 " + strWhere + " ";

                    break;

                case "workpriority":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.WorkPriority,C.AltWorkPriority,C.IsDefault,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_workpriority_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "workstatus":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.WorkStatus,C.AltWorkStatus,C.IsDefault,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_workstatus_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "worktype":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.WorkTypeDescription,C.AltWorkTypeDescription,C.IsDefault,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_worktype_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "worktrade":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.WorkTrade,C.AltWorkTrade,C.IsDefault,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_worktrade_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "assetcategories":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.AssetCatCode,C.AssetCategory,C.AltAssetCategory,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_assetcategories_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "assetstatus":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.AssetStatusDesc,C.AltAssetStatusDesc,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_Assetstatus_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "criticality":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                 " C.Criticality,C.AltCriticality,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_criticality_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "warrantycontract":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.Warranty_contract,C.AltWarranty_contract,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_warrantycontract_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "employeecategories":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.CategoryName,C.AltCategoryName,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_employeecategories_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "employeestatuses":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.EmployeeStatus,C.EmployeeAltStatus,C.DisplayInModule,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                    " from cdc.dbo_employeeStatuses_CT as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "pmchecklist_clean":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " si.SafetyNo,C.CheckListNo,C.CheckListName,C.AltCheckListName,C.EstimatedLaborHours,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy  " +
                                                    " from CDC.dbo_pmchecklist_CT C left outer join tblSafetyinstruction si on si.SafetyId = C.SafetyID left outer join L2 on L2.L2Id=C.L2Id inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " Left outer join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintdivId  Left outer join MaintenanceDepartment mdept on mdept.maintDeptID =C.MaintdeptId Left outer join MaintSubDept msdept on msdept.MaintSubDeptID=C.MainSubDeptId " +
                                                    " where 1=1 and C.IsCleaningModule = 1 " + strWhere + " ";
                    break;

                case "checklistelements_clean":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " PC.CheckListNo,C.SeqNo,C.TaskDesc,C.AltTaskDesc,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate , e.Name as ModifiedBy " +
                                                    " from CDC.dbo_checklistelements_CT C inner join PMchecklist PC on PC.CheckListId=C.CheckListId" +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 and PC.IsCleaningModule = 1 " + strWhere + " ";
                    break;

                case "checklistinv_clean":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " PC.CheckListNo,chke.SeqNo,s.StockNo,s.StockDescription,s.AltStockDescription,C.Qty,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate , e.Name as ModifiedBy " +
                                                    " from CDC.dbo_checklistInv_CT C inner join PMchecklist PC on PC.CheckListId=C.CheckListId inner join StockCode s on s.StockId=C.PartId inner join checklistelements chke on chke.CheckListelementId=C.ChecklistelementId" +
                                                    " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) where 1=1 and PC.IsCleaningModule = 1 " + strWhere + " ";

                    break;

                case "extassetfiles_clean":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                      " S.ChecklistNo as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                             " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  Pmchecklist as S on S.ChecklistID=FkId and ModuleType='P' " +
                                             " where 1=1 and S.IsCleaningModule = 1 " + strWhere + " ";
                    break;

                case "pmschedule_clean":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    "C.PMNo,C.PMName,C.AltPMName, " +
                                                      " L2.L2Code,a.AssetNumber,l.LocationNo,pc.ChecklistNo,wt.WorkTypeDescription,wtr.WorkTrade,C.PeriodDays,C.FreqUnits,C.Frequency,C.TargetStartDate,C.TargetCompDate," +
                                                    " C.ActualCompDate,C.NextDate,C.TypePM,C.PMMultiple,C.PMActive,C.weekofMonth,C.DayOfWeek,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy" +
                                                    " from CDC.dbo_pmschedule_CT C inner join L2 on L2.L2Id=C.L2Id left outer join Assets a on a.AssetID=C.AssetId inner join Location l on l.LocationId=C.PhyLocationID " +
                                                    " inner join pmchecklist pc on pc.ChecklistID=C.ChecklistID  inner join worktype wt on wt.WorkTypeID=C.WorkTypeID inner join WorkTrade wtr on wtr.WorktradeId=C.WorktradeId " +
                                                    " inner join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintDivisionID inner join MaintenanceDepartment mdept on mdept.maintDeptID =C.maintDeptID " +
                                                    " inner join MaintSubDept msdept on msdept.MaintSubDeptID=C.MaintSubDeptID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1  and C.IsCleaningModule = 1 and C.PMGroupNo is NULL " + strWhere + " ";
                    break;

                case "pmschedule_asset":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    "C.PMNo,C.PMName,C.AltPMName, " +
                                                      " L2.L2Code,a.AssetNumber,l.LocationNo,pc.ChecklistNo,wt.WorkTypeDescription,wtr.WorkTrade,C.PeriodDays,C.FreqUnits,C.Frequency,C.TargetStartDate,C.TargetCompDate," +
                                                    " C.ActualCompDate,C.NextDate,C.TypePM,C.PMMultiple,C.PMActive,C.weekofMonth,C.DayOfWeek,mdiv.MaintDivisionCode,mdept.MaintDeptCode,msdept.MaintSubDeptCode,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy" +
                                                    " from CDC.dbo_pmschedule_CT C inner join L2 on L2.L2Id=C.L2Id left outer join Assets a on a.AssetID=C.AssetId inner join Location l on l.LocationId=C.PhyLocationID " +
                                                    " inner join pmchecklist pc on pc.ChecklistID=C.ChecklistID  inner join worktype wt on wt.WorkTypeID=C.WorkTypeID inner join WorkTrade wtr on wtr.WorktradeId=C.WorktradeId " +
                                                    " inner join MainenanceDivision mdiv on mdiv.MaintDivisionID = C.MaintDivisionID inner join MaintenanceDepartment mdept on mdept.maintDeptID =C.maintDeptID " +
                                                    " inner join MaintSubDept msdept on msdept.MaintSubDeptID=C.MaintSubDeptID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1 and C.PMGroupNo is not NULL " + strWhere + " ";
                    break;

                case "multiplepm_asset":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " pms.PMNo,pc.ChecklistNo,C.MSeq,C.MDescription,C.MStartDate,C.MStartDateCount,C.MTaskSeq,C.MPMCounter,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                                                    " from CDC.dbo_multiplepm_CT C Inner join pmschedule pms on pms.PMID=C.PMID inner join pmchecklist pc on pc.ChecklistID=C.MTaskID  inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                                                    " where 1=1  and pms.PMGroupNo is not NULL " + strWhere + " ";
                    break;

                case "cleaninginspectiongroup":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.GroupNumber,C.GroupDescription,C.ArabicGroupDescription,C.TotalArea,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy  " +
                                                    " from CDC.dbo_cleaningInspectionGroup_ct as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "cleaninginspectiongroupsequencenumber":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                  " C.SequenceNumber,l.LocationNo,l.LocationDescription, l.LocationAltDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy  " +
                                                    " from  CDC.dbo_cleaningInspectionGroupSequenceNumber_ct C inner join location l on l.LocationID = C.LocationID inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                                                    " where 1=1 " + strWhere + " ";
                    break;

                case "cleaninginspectionmaster":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                           "C.ReferenceNo,C.DateInspection,C.Comments ,C.ArabicComments,e.Name As InspectionByName, isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                           "from CDC.dbo_cleaninginspectionmaster_ct as C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                           "inner join employees as emp on emp.EmployeeID = C.InspectionBy " +
                           " where 1=1 " + strWhere + " ";
                    break;

                case "cleaninginspectiondetails":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                          " C.InspectionDate,C.Floor,C.Wall,C.Window,C.Ceiling,C.Furniture,C.WasteBag,C.Consumables,C.Remarks,C.ArabicRemarks,C.TotalPercentage,C.SatisfactionResult,l.LocationDescription,cg.GroupDesc,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                          " from  CDC.dbo_cleaninginspectiondetails_ct C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  " +
                          " inner join cleaninginspectionmaster cm on C.CleaningInspectionId = cm.CleaningInspectionId " +
                          " inner join Location l on l.LocationID = C.LocationID " +
                          " inner join cleaninggroup cg on cg.CleaningGroupId = C.CleaningGroupId " +
                          " where 1=1 " + strWhere + " ";
                    break;

                case "purchaserequest":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                            " C.PurchaseRequestID,L2.L2Code,L2.L2name,L2.L2Altname,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName, " +
                            " employees.Name,C.CreatedDate,C.OrderStatus,C.ApprovalStatus,MainenanceDivision.MaintDivisionCode, " +
                            " MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,purchase_req_status.status_desc,purchase_req_status.altstatus_desc, " +
                            " pr_authorisation_status.auth_status_desc,pr_authorisation_status.Altauth_status_desc,C.ProjectNumber,C.CreatedDate as ModifiedDate ,e.Name as ModifiedBy " +
                            " FROM [cdc].[dbo_PurchaseRequest_CT] C " +
                            " Inner JOIN L2 ON L2.L2ID = C.L2ID  " +
                            " Inner JOIN Sector ON Sector.SectorID = L2.SectorID  " +
                            " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID= C.MaintdivId " +
                            " inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = C.MaintdeptId  " +
                            " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = C.MainSubDeptId " +
                            " left outer join employees on employees.EmployeeID = C.RequestedBy  " +
                            " LEFT JOIN purchase_req_status ON purchase_req_status.pr_status_id = C.status_id  " +
                            " LEFT JOIN pr_authorisation_status ON pr_authorisation_status.auth_status_id = C.auth_status " +
                            " inner join employees as e on e.EmployeeID = C.CreatedBy " +
                            " where 1=1 " + strWhere + " ";
                    break;

                case "purchaserequestitems":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                            " C.PartDescription,C.UoM,C.Quantity,C.Source,C.JoborderID,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,C.RefPrice, " +
                            " SC.StockNo,SC.AltStockDescription, PurchaseRequest.PurchaseRequestID,e.Name as ModifiedBy " +
                            " from CDC.dbo_PurchaseRequestItems_CT  C    " +
                            " Left Outer Join stockcode SC on SC.StockID = C.PartNumber  " +
                            " inner join PurchaseRequest on PurchaseRequest.ID = C.PurchaseRequestID " +
                            " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                            " where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_pr":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                               " PR.PurchaseRequestID as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                                   " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  PurchaseRequest as PR on PR.Id =FkId and ModuleType='PR'  " +
                                                   " where 1=1 " + strWhere + " ";
                    break;

                case "purchaseorder":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                            " C.ID,C.PurchaseOrderNo,C.OrderTotal,C.OrderedDate,C.OrderStatus,C.ExpDeliveryDate,C. " +
                            " DeliveryStatus,C.ProjectNumber,  L2.L2Code,L2.L2name,L2.L2Altname,Sector.SectorCode,Sector.SectorName, Sector.SectorAltname,  MainenanceDivision.MaintDivisionCode, " +
                            " MaintenanceDepartment.MaintDeptCode,MaintSubDept.MaintSubDeptCode,s.SupplierName,s.AltSupplierName,e.Name as ModifiedBy    " +
                            " FROM CDC.dbo_PurchaseOrder_CT  C  " +
                            " Inner JOIN L2 ON L2.L2ID = C.L2ID    " +
                            " Inner JOIN Sector ON Sector.SectorID = L2.SectorID  " +
                            " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID= C.MaintDivisionID  " +
                            " inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = C.maintDeptID   " +
                            " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = C.maintsubdeptID   " +
                            " LEFT JOIN suppliers s ON s.SupplierID = C.SupplierID " +
                            " left join employees as e on e.EmployeeID = C.OrderBy " +
                            " where 1=1 " + strWhere + " ";
                    break;

                case "purchaseorderitems":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                        " C.PartNumber,C.UoM,C.Quantity,C.JobOrderID,C.UnitPrice,C.Discount,C.Tax,C.LineCost,C.BaseCost,C.[Source], " +
                        " C.PurchaseRequestItemID,C.CreatedDate,C.PurchaseRequestNo,SC.StockNo, PurchaseOrder.PurchaseOrderNo, " +
                        " C.PartDescription, SC.AltStockDescription ,e.Name as ModifiedBy " +
                        " from CDC.dbo_PurchaseOrderItems_CT C    " +
                        " Left Outer Join stockcode SC on SC.StockID = C.PartNumber  " +
                        " inner join PurchaseOrder on PurchaseOrder.ID = C.PurchaseOrderID " +
                        " inner join employees as e on e.EmployeeID = PurchaseOrder.OrderBy " +
                        " where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_po":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                            " PO.PurchaseOrderNo as No,C.FkId,C.FileLink,C.FileDescription,C.FileName,C.ModuleType,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                            " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) inner join  PurchaseOrder as PO on PO.Id =FkId and ModuleType='PO'  " +
                            " where 1=1 " + strWhere + " ";
                    break;

                case "worequest":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                          " C.RequestNo,C.WorkOrderNo,C.ProblemDesc,workstatus.WorkStatus as JobStatus,workstatus.AltWorkStatus, " +
                            " worktype.WorkTypeDescription,worktype.AltWorkTypeDescription,ExternalRequesterName,workpriority.WorkPriority as workpriorityName, " +
                            " workpriority.AltWorkPriority,asset.AssetNumber, asset.AssetDescription,asset.AssetAltDescription, " +
                            " location.LocationNo,location.LocationDescription,location.LocationAltDescription,L2.L2Code,Sector.SectorCode,Sector.SectorName,Sector.SectorAltName, " +
                            " L2Classes.L2ClassCode AS L2ClassCode,L2Classes.L2Class AS L2Class,L2Classes.AltL2Class AS AltL2Class,L3.L3No, " +
                            " L3.L3Desc,L3.L3AltDesc,L4.L4No,L4.L4Description,L4.L4AltDescription,L5.L5No,L5.L5Description,L5.L5AltDescription, " +
                            " MainenanceDivision.MaintDivisionCode,MaintenanceDepartment.MaintDeptCode,  " +
                            " MaintSubDept.MaintSubDeptCode,C.ReceivedDate,C.RequiredDate, " +
                            " employees.Name, workorders.ProjectNumber ,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate ,e.Name as ModifiedBy " +
                            "  FROM  " +
                            " [cdc].[dbo_WoRequest_CT] C " +
                            " inner join workstatus on C.RequestStatusID=WorkStatus.WorkStatusID  " +
                            " inner join worktype on worktype.WorkTypeID = C.WorkTypeID   " +
                            " inner join workpriority on workpriority.WorkPriorityID = C.WorkPriorityID   " +
                            " LEFT OUTER JOIN assets AS asset ON asset.assetid = C.assetid  " +
                            " LEFT OUTER JOIN location ON location.locationid = C.locationid  " +
                            " Inner JOIN L2 ON L2.L2ID = C.L2ID  " +
                            " Inner JOIN Sector ON Sector.SectorID = L2.SectorID  " +
                            " LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID  " +
                            " left outer join L3 on L3.L3ID = C.L3ID  " +
                            " left outer join L4 on L4.L4ID = C.L4ID  " +
                            " left outer join L5 on L5.L5ID = C.L5ID  " +
                            " inner join MainenanceDivision on MainenanceDivision.MaintDivisionID= C.MaintdivId  " +
                            " inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = C.MaintdeptId " +
                            " inner join MaintSubDept on MaintSubDept.MaintSubDeptID = C.MainSubDeptId  " +
                            " left outer join employees on employees.EmployeeID = C.RequesterID  " +
                            " left outer join workorders on workorders.WorkorderNo = C.WorkorderNo  " +
                            " inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy) " +
                          " where 1=1 " + strWhere + " ";
                    break;

                case "extassetfiles_r":
                    query = @"Select C.__$operation as Operation,C.__$update_mask as update_mask, " +
                                                    " R.RequestNo as No,C.FileName,C.FileDescription,isnull(C.ModifiedDate,C.CreatedDate) as ModifiedDate,e.Name as ModifiedBy " +
                                             " from CDC.dbo_extassetfiles_CT C inner join employees as e on e.EmployeeID = isnull(C.ModifiedBy,C.CreatedBy)  inner join  worequest as R on RequestNo=FkId and ModuleType='R' " +
                                             " where 1=1 " + strWhere + " ";
                    break;

            }

            return query;
        }
    }
}
