﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMMS.DAL;
using CMMS.Model;
using System.Collections.ObjectModel;
using System.Data;
using Kendo.Mvc.UI;

namespace CMMS.Service.ManagementService
{
    public class LanguageTranslationService : DBExecute
    {
        public LanguageTranslationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Gets the un translated word by code.
        /// </summary>
        /// <param name="ToLanguageCode">To language code.</param>
        /// <param name="pageNo">The page no.</param>
        /// <param name="sortExpression">The sort expression.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <returns></returns>
        public IList<Untranslatedword> GetUnTranslatedWordByCode(string ToLanguageCode, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            string strWhereClause = string.Empty;
            IList<Untranslatedword> list = new List<Untranslatedword>();
            Collection<DBParameters> outParameters = new Collection<DBParameters>();
            string query = "Select * from untranslatedwords c " +
                           "where c.labelname not in (select translations.labelname  from translations where ToLanguageCode = '" + ToLanguageCode + "' )";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, outParameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<Untranslatedword>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public static List<Translation> GetTranslationForReport()
        {
            string strWhereClause = string.Empty;
            List<Translation> list = new List<Translation>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string query = "Select * from translations";

            using (DapperDBContext context = new ServiceContext())
            {
                list = context.ExecuteQuery<Translation>(query, parameters).ToList();
            }
            return list;
        }
    }
}
