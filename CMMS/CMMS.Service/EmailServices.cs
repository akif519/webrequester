﻿using CMMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MobilySendSMS;
using System.Net.Http;

namespace CMMS.Service
{
    public class EmailServices
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Email" /> class.
        /// </summary>
        public EmailServices()
        {
        }

        /// <summary>
        /// Email type
        /// </summary>
        public enum EmailType
        {
            /// <summary>
            /// Default Type with Master Template
            /// </summary>
            Default,

            /// <summary>
            /// Mail without Master Template
            /// </summary>
            NoMaster
        }

        #endregion

        /// <summary>
        /// Sending An Email with master mail template
        /// </summary>
        /// <param name="mailFrom">Mail From</param>
        /// <param name="mailTo">Mail To</param>
        /// <param name="mailCC">Mail CC</param>
        /// <param name="mailBCC">Mail BCC</param>
        /// <param name="subject">Subject of mail</param>
        /// <param name="body">Body of mail</param>
        /// <param name="attachment">Attachment for the mail</param>
        /// <param name="emailType">Email Type</param>
        /// <returns>return send status</returns>
        public static bool Send(string mailTo, string mailCC, string mailBCC, string subject, string body, byte[] attachmentFile = null, string attachmentName = null, System.Net.Mail.SmtpClient objectSMTPClient = null)
        {
            string mailFrom;

            mailFrom = ProjectConfiguration.FromEmailAddress;

            if (ValidateEmail(mailFrom, mailTo) && (string.IsNullOrEmpty(mailCC) || IsEmail(mailCC)) && (string.IsNullOrEmpty(mailBCC) || IsEmail(mailBCC)))
            {

                System.Net.Mail.MailMessage mailMesg = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient objSMTP = new System.Net.Mail.SmtpClient();

                if (ProjectConfiguration.TestMode)
                {
                    mailFrom = ProjectConfiguration.TestEmailAddress;
                    mailTo = ProjectConfiguration.TestEmailAddress;
                    mailCC = string.Empty;
                    mailBCC = string.Empty;
                }

                mailMesg.From = new System.Net.Mail.MailAddress(mailFrom);
                mailMesg.To.Add(mailTo);

                if (!string.IsNullOrEmpty(mailCC))
                {
                    string[] mailCCArray = mailCC.Split(';');
                    foreach (string email in mailCCArray)
                    {
                        mailMesg.CC.Add(email);
                    }
                }

                if (!string.IsNullOrEmpty(mailBCC))
                {
                    mailBCC = mailBCC.Replace(";", ",");
                    mailMesg.Bcc.Add(mailBCC);
                }


                //SetSMTPDetail(ref objSMTP);
                if (attachmentFile != null)
                {
                    mailMesg.Attachments.Add(new Attachment(new MemoryStream(attachmentFile), attachmentName));
                }

                //if (!string.IsNullOrEmpty(attachment))
                //{
                //    string[] attachmentArray = attachment.Split(';');
                //    foreach (string attachFile in attachmentArray)
                //    {
                //        try
                //        {
                //            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(attachFile);
                //            mailMesg.Attachments.Add(attach);

                //        }
                //        catch
                //        {
                //        }
                //    }
                //}

                mailMesg.Subject = subject;

                //mailMesg.AlternateViews.Add(GetMasterBody(body, subject));
                mailMesg.Body = body;
                mailMesg.IsBodyHtml = true;

                try
                {
                    if (objectSMTPClient != null)
                    {
                        objectSMTPClient.Send(mailMesg);
                    }
                    else
                    {
                        objSMTP.Send(mailMesg);
                    }
                    return true;
                }
                catch
                {
                    //mailMesg.Dispose();
                    //mailMesg = null;
                    return false;
                }
                finally
                {
                    mailMesg.Dispose();
                }
            }

            return false;
        }

        /// <summary>
        /// Read the Template from Format and return
        /// </summary>
        /// <param name="emailTemplate">Email Template</param>
        /// <returns>Return body Of Email Template</returns>
        public static string GetEmailTemplate(string emailTemplate)
        {
            string bodyTemplate = string.Empty;
            string filePath = emailTemplate.ToString() + ".html";
            //string filePath = ProjectConfiguration.EmailTemplatePath + emailTemplate.ToString() + ".html";
            if (File.Exists(filePath))
            {
                try
                {
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        bodyTemplate = reader.ReadToEnd();
                    }
                }
                catch
                {
                    throw;
                }
            }

            return bodyTemplate;
        }

        /// <summary>
        /// Get Master Body HTML
        /// </summary>
        /// <param name="body">Body Text</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="emailType">Email Type</param>
        /// <returns>Alternate View</returns>
        private static System.Net.Mail.AlternateView GetMasterBody(string body, string subject, EmailType emailType)
        {
            if (emailType == EmailType.Default)
            {
                string masterEmailTemplate = EmailServices.GetEmailTemplate("daa");
                body = masterEmailTemplate.Replace("[@MainContent]", body);

                body = body.Replace("[@Subject]", subject);

                string logo = ProjectConfiguration.ApplicationRootPath + @"\images\logo.png";

                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, null, "text/html");
                System.Net.Mail.LinkedResource logoResource = new System.Net.Mail.LinkedResource(logo, "image/gif");
                logoResource.ContentId = "LogoImage";
                logoResource.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;
                htmlView.LinkedResources.Add(logoResource);
                return htmlView;
            }
            else
            {
                return System.Net.Mail.AlternateView.CreateAlternateViewFromString(body, null, "text/html");
            }
        }

        /// <summary>
        /// Method is used to Validate Email
        /// </summary>
        /// <param name="fromEmail">From email List</param>
        /// <param name="toEmail">To Email list</param>
        /// <returns>Returns validation result</returns>
        private static bool ValidateEmail(string fromEmail, string toEmail)
        {
            bool isValid = true;
            if (!IsEmail(fromEmail))
            {
                isValid = false;
            }

            if (!string.IsNullOrEmpty(toEmail))
            {
                toEmail = toEmail.Replace(" ", string.Empty);
                string[] emailList = null;
                try
                {
                    emailList = toEmail.Split(',');
                }
                catch
                {
                    isValid = false;
                }

                if (emailList != null && emailList.Count() > 0)
                {
                    foreach (string email in emailList)
                    {
                        if (!IsEmail(email))
                        {
                            isValid = false;
                        }
                    }
                }
                else
                {
                    isValid = false;
                }
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }

        /// <summary>
        /// Check email string is Email or not
        /// </summary>
        /// <param name="email">Email to verify</param>
        /// <returns>return email validation result</returns>
        private static bool IsEmail(string email)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string SendSMSFromDll(string username, string password, string message, string toMobileno, string sender)
        {
            try
            {

                SendSMS snd = new SendSMS();
                var result = snd.SendSMS(ref username, ref password, ref message, ref toMobileno, ref sender);

                String strFailedReason = string.Empty;
                switch (result)
                {
                    case "1":
                        //SMS Have Been Send
                        strFailedReason = string.Empty;
                        break;
                    case "2":
                        //balabce is zero (SMS Not Send)
                        strFailedReason = "Balabce is zero (SMS Could Not Be Sent).";
                        break;
                    case "3":
                        //balance not enough (SMS Not Send)
                        strFailedReason = "Insufficient Balance (SMS Could Not Be Sent).";
                        break;
                    case "4":
                        //error in user name (SMS Not Send)
                        strFailedReason = "User name or number is not available (SMS Could Not Be Sent).";
                        break;
                    case "5":
                        //error in password (SMS Not Send)
                        strFailedReason = "Password is incorrect (SMS Could Not Be Sent).";
                        break;
                    case "6":
                        //there is a problem in sending, try again later  (SMS Not Send)
                        strFailedReason = "There is a problem in sending, try again later (SMS Could Not Be Sent).";
                        break;
                    case "10":
                        strFailedReason = "Number of numbers and number of messages not equal (SMS Could Not Be Sent).";
                        break;
                    case "13":
                        strFailedReason = "Unaccepted Username (SMS Could Not Be Sent).";
                        break;
                    case "-1":
                        strFailedReason = "Cannot connect to the server (SMS Could Not Be Sent).";
                        break;
                    case "-2":
                        strFailedReason = "Cannot connect to the server (SMS Could Not Be Sent).";
                        break;
                }

                if (strFailedReason != string.Empty)
                {
                    return strFailedReason;
                }
                else if ( result == string.Empty)
                {
                    return result;
                }
                else
                {
                    return ProjectSession.Resources.message.Common_MsgSMSNotSentSuccessfully;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string SendSMSFromAPI(string username, string password, string message, string toMobileno, string apiid)
        {
            try
            {
                string URL = "http://api.clickatell.com/http/";
                string urlParameters = "sendmsg?user=" + username + "&password=" + password + "&api_id=" + apiid + "&to=" + toMobileno + "&text=" + message;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                if (response.IsSuccessStatusCode)
                {
                    return string.Empty;
                }
                else
                {
                    return ProjectSession.Resources.message.Common_MsgSMSNotSentSuccessfully;
                };

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
