﻿using CMMS.DAL;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class DocumentService : DBExecute
    {
        public DocumentService()
        {
            this.PagingInformation = new Pagination() { PageSize = DefaultPagerSize, PagerSize = DefaultPagerSize };
        }

        public IList<ExtAssetFile> GetDocumentList(string fkID, string moduleType, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<ExtAssetFile> list = new List<ExtAssetFile>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "FkID",
                Value = fkID,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "moduleType",
                Value = moduleType,
                DBType = DbType.String
            });


            string query = "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name as ModifiedByName " +
                //"ass.AssetID,ass.AssetNumber,ass.AssetDescription,ass.AssetAltDescription " +
                           "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                //"inner join assets ass on ass.AssetID = att.FkId " +
                           " where  att.FkId = @FkID and  att.ModuleType = @moduleType";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<ExtAssetFile>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<ExtAssetFile> GetPODocumentList(string fkID, string moduleType, string lstPRID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<ExtAssetFile> list = new List<ExtAssetFile>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "FkID",
                Value = fkID,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "moduleType",
                Value = moduleType,
                DBType = DbType.String
            });

            parameters.Add(new DBParameters()
            {
                Name = "lstPRID",
                Value = lstPRID,
                DBType = DbType.String
            });

            string query = "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name as ModifiedByName " +
                           " ,PurchaseOrder.PurchaseOrderNo As AssetNumber " +
                           "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                           " LEFT JOIN PurchaseOrder on PurchaseOrder.Id = att.FkId " +
                           " where  att.FkId = @FkID and  att.ModuleType = @moduleType ";

            if (lstPRID != string.Empty)
            {
                string query1 = " union " +
                          "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name as ModifiedByName " +
                          " ,PurchaseRequest.PurchaseRequestID As AssetNumber " +
                          "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                          " LEFT JOIN PurchaseRequest on PurchaseRequest.Id = att.FkId " +
                          " where  att.FkId in(@lstPRID) and  att.ModuleType = 'PR'";

                query = query + query1;
            }


            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<ExtAssetFile>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<ExtAssetFile> GetSectionDocumentList(string fkID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<ExtAssetFile> list = new List<ExtAssetFile>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;
            parameters.Add(new DBParameters()
            {
                Name = "FkID",
                Value = fkID,
                DBType = DbType.String
            });

            string query = "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name as ModifiedByName " +
                           "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                           " where  att.FkId = @FkID and  att.ModuleType IN('L','D')";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<ExtAssetFile>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }

        public IList<ExtAssetFile> GetViewLocationDiagram(string fkID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            IList<ExtAssetFile> list = new List<ExtAssetFile>();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string strWhereClause = string.Empty;

            parameters.Add(new DBParameters()
            {
                Name = "FkID",
                Value = fkID,
                DBType = DbType.String
            });

            string query = "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name as ModifiedByName " +
                           "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                           " where  att.FkId = @FkID and  att.ModuleType IN('D')";

            SearchFieldService objSearchField = new SearchFieldService();

            strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

            if (!string.IsNullOrEmpty(strWhereClause))
            {
                query += " AND " + strWhereClause;
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                query += " Order by " + sortExpression;
                if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                    query += " Asc";
                else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                    query += " desc";

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<ExtAssetFile>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }
            }

            return list;
        }
    }
}
