﻿//-----------------------------------------------------------------------
// <copyright file="ServiceContext.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.Service
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using CMMS.DAL;
    using System.Data.SqlClient;
    using CMMS.Infrastructure;

    /// <summary>
    /// This class is used to Define Database object for Save, Search, Delete
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    public sealed class ServiceContext : DapperDBContext
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the ServiceContext class.
        /// </summary>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public ServiceContext()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = false;
        }

        /// <summary>
        /// Initializes a new instance of the ServiceContext class.
        /// </summary>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public ServiceContext(bool useTransaction)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = false;
            
            this.Connection = OpenConnection();
        }

        /// <summary>
        /// Initializes a new instance of the ServiceContext class for checking duplicate value for one column
        /// </summary>
        /// <param name="col1Name">column Name</param>
        public ServiceContext(string col1Name)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.CombinationCheckRequired = false;
        }

        /// <summary>
        /// Initializes a new instance of the ServiceContext class for checking duplicate value for two column with combination
        /// </summary>
        /// <param name="col1Name">first column name</param>
        /// <param name="col2Name">second column name</param>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public ServiceContext(string col1Name, string col2Name)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.CombinationCheckRequired = true;
        }

        /// <summary>
        /// Initializes a new instance of the ServiceContext class for checking duplicate value for two column
        /// </summary>
        /// <param name="col1Name">first column name</param>
        /// <param name="col2Name">second column name</param>
        /// <param name="combinationCheckRequired">Combination Check Required</param>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public ServiceContext(string col1Name, string col2Name, bool combinationCheckRequired)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.CombinationCheckRequired = combinationCheckRequired;
        }

        public ServiceContext(string col1Name, string col2Name, string parentColumnName, bool combinationCheckRequired)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.ParentColumnName = parentColumnName;

            this.CombinationCheckRequired = combinationCheckRequired;
        }

        #endregion

        #region Custom Methods
        
        /// <summary>
        /// Initializes a new instance of the ServiceContext class.
        /// </summary>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public ServiceContext(int clientID)
        {
            this.PagingInformation = new Pagination() { PageSize = 15, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = false;
        }
        

        #endregion
    }
}
