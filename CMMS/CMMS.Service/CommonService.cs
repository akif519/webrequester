﻿using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class CommonService<T> : IDisposable, ICommonService<T> where T : class
    {
        #region Fields

        //private readonly IDbContext _context;
        //private IDbSet<T> _entities;
        private CMMSDBContext _dbContext = null;
        private IDbSet<T> _entities;

        #endregion

        #region Ctor

        public CommonService()
        {
            if (_dbContext == null)
            { 
                _dbContext = new CMMSDBContext();
                
            }
        }


        #endregion

        public List<T> GetAllEntities()
        {
            return Entities.AsNoTracking().ToList();
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                ((IDisposable)_dbContext).Dispose();
                _dbContext = null;
            }
        }

        private IDbSet<T> Entities
        {
            get { return _entities ?? (_entities = _dbContext.Set<T>()); }
        }

        /// <summary>
        /// Gets the table.
        /// </summary>
        /// <value>
        /// The table.
        /// </value>
        public IQueryable<T> Table
        {
            get
            {
                return Entities;
            }
        }
    }
}
