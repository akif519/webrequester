﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class PermissionWebService : DBExecute
    {
        public PermissionWebService()
        {
        }

        #region "Client Wise Methods"

        public PermissionMappingResult GetPermissionMapping(PermissionMappingResult model, string clientCode)
        {
            PermissionMappingResult objPermissionMappingResult = new PermissionMappingResult();

            StringBuilder sqlSelectQuery = new StringBuilder();
            sqlSelectQuery.Append(" 	SELECT p.PermissionId,p.ModuleName,p.SubModuleName,p.PermissionDesc,ISNULL(p3.AllowAccess, 0) AS	");
            sqlSelectQuery.Append(" 	AllowAccessByUserGroup,ISNULL(p2.AllowAccess, 0) AS AllowAccessByEmployee	");
            sqlSelectQuery.AppendFormat(" 	FROM [permissions] p LEFT JOIN permissionsmappings p2 ON p2.PermissionId = p.PermissionId AND p2.EmployeeId = {0}	", model.EmployeeId);
            sqlSelectQuery.AppendFormat(" 	LEFT JOIN permissionsmappings p3 ON p3.PermissionId = p.PermissionId AND p3.UserGroupId = {0}	", model.UserGroupId);
            sqlSelectQuery.Append(" 	WHERE p.ModuleName IN('Job Request','Job Order','Assets','Mobile App')	");
            
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    model.PermissionTable = objContext.ExecuteQueryWithClientID<Permission>(sqlSelectQuery.ToString(), parameters, clientCode).ToList();                   
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return model;
        }

        #endregion

    }
}
