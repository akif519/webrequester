﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class AssetWebService : DBExecute
    {
        /// <summary>
        /// Get Asset List with Associated Tab-Data
        /// </summary>
        /// <param name="IsCentral"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="AssetIdToRefresh"></param>
        /// <param name="JobOrderHistoryCount"></param>
        /// <returns></returns>
        public GetAssetListResult GetAssetList(bool IsCentral, int EmployeeID, int PageIndex, int PageSize, int? AssetIdToRefresh, int JobOrderHistoryCount, string strCurrentTimeStamp, string clientCode)
        {

            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);

            GetAssetListResult objAssetListResult = new GetAssetListResult();
            EmployeeDetailService objEmpDetailService = new EmployeeDetailService();

            //Check For User is Active or Not
            if (!objEmpDetailService.IsUserActive(EmployeeID, clientCode))
            {
                objAssetListResult.IsUserActive = 0;
                return objAssetListResult;
            }
            else
            {
                objAssetListResult.IsUserActive = 1;
            }

             employees objemployees = new employees();                

                using (ServiceContext objContext = new ServiceContext(0))                
                {
                    objemployees.EmployeeID  = EmployeeID;
                    objemployees = objContext.SearchAllWithClientID<employees>(objemployees, clientCode).FirstOrDefault();
                }

            string strWhere;
            if (AssetIdToRefresh > 0)
            {
                strWhere = " AND a.AssetID = " + AssetIdToRefresh;
            }
            else
            {
                strWhere = string.Empty;
            }

            StringBuilder sqlSelectQuery = new StringBuilder();
            StringBuilder sqlPMScheduleQuery = new StringBuilder();
            StringBuilder sqlJOHistoryQuery = new StringBuilder();

            string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            Account objAccount = GetAccountType(clientCode);
            if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
            {
                sqlSelectQuery.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
            }

            #region Get Asset List With Basic Info

            //sqlSelectQuery.Append("	SELECT a.AssetNumber,a.AssetDescription,a.AssetAltDescription,a.AssetID,	  ");
            //sqlSelectQuery.Append("	l2.L2Code,l2.L2name,l2.L2Altname,	  ");
            //sqlSelectQuery.Append("	sec.SectorCode,sec.SectorName,sec.SectorAltname,	  ");
            //sqlSelectQuery.Append("	       l.LocationNo,l.LocationDescription,l.LocationAltDescription,	  ");
            //sqlSelectQuery.Append("	       l3.L3No,l3.L3Desc,l3.L3AltDesc,	  ");
            //sqlSelectQuery.Append("	       l4.L4No,l4.L4Description,l4.L4AltDescription,	  ");
            //sqlSelectQuery.Append("	       l5.L5No,l5.L5Description,l5.L5AltDescription,	  ");
            //sqlSelectQuery.Append("	       a2.AssetCatCode,a2.AssetCategory,a2.AltAssetCategory,	  ");
            //sqlSelectQuery.Append("	       a3.AssetSubCatCode,a3.AssetSubCategory,a3.AltAssetSubCategory,	  ");
            //sqlSelectQuery.Append("	       a4.AssetClassCode,a4.AssetClass,a4.AltAssetClass,	  ");
            //sqlSelectQuery.Append("	       a5.AssetConditionCode,a5.AssetCondition,a5.AltAssetCondition,	  ");
            //sqlSelectQuery.Append("	       a6.AssetStatusDesc,a6.AltAssetStatusDesc,	  ");
            //sqlSelectQuery.Append("	       c.Criticality,c.AltCriticality,	  ");
            //sqlSelectQuery.Append("	       w.WorkTrade,w.AltWorkTrade,	  ");
            //sqlSelectQuery.Append("	       e.EmployeeNO AS EmployeeNo,e.Name AS EmployeeName,e.AltName AS EmployeeAltName,	  ");
            //sqlSelectQuery.Append("	       w2.Warranty_contract,w2.AltWarranty_contract,a.Warranty_ContractExpiry,a.Warranty_ContractNotes,a.Comments AS AssetNotes,	  ");
            //sqlSelectQuery.Append("	       s.SupplierNo,s.SupplierName,s.AltSupplierName,	  ");
            //sqlSelectQuery.Append("	       a.PurchasePrice,a.ModelNumber,a.SerialNumber,a.Manufacturer,	  ");
            //sqlSelectQuery.Append("	       a.DateAcquired AS CommissionedDate,a.DataDisposed,a.EstLife,a.NotesToTech,a.PartsListID	  ");
            //sqlSelectQuery.Append("	FROM assets a	  ");

            sqlSelectQuery.Append(" SELECT ");
            sqlSelectQuery.Append("  a2.AltAssetCategory, ");
            sqlSelectQuery.Append("  a4.AltAssetClass, ");
            sqlSelectQuery.Append("  a5.AltAssetCondition, ");
            sqlSelectQuery.Append("  a6.AltAssetStatusDesc,        ");
            sqlSelectQuery.Append("  a3.AltAssetSubCategory, ");
            sqlSelectQuery.Append("  c.AltCriticality, ");
            sqlSelectQuery.Append("  s.AltSupplierName, ");
            sqlSelectQuery.Append("  w2.AltWarranty_contract, ");
            sqlSelectQuery.Append("  w.AltWorkTrade, ");
            sqlSelectQuery.Append("  a.AssetAltDescription, ");
            sqlSelectQuery.Append("  a2.AssetCatCode, ");
            sqlSelectQuery.Append("  a2.AssetCategory, ");
            sqlSelectQuery.Append("  a4.AssetClass, ");
            sqlSelectQuery.Append("  a4.AssetClassCode, ");
            sqlSelectQuery.Append("  a5.AssetConditionCode, ");
            sqlSelectQuery.Append("  a.AssetDescription, ");
            sqlSelectQuery.Append("  a.AssetID, ");
            sqlSelectQuery.Append("  a.Comments AssetNotes, ");
            sqlSelectQuery.Append("  a.AssetNumber, ");
            sqlSelectQuery.Append("  a6.AssetStatusDesc, ");
            sqlSelectQuery.Append("  a3.AssetSubCatCode, ");
            sqlSelectQuery.Append("  a3.AssetSubCategory, ");
            sqlSelectQuery.Append("  a.DateAcquired CommissionedDate, ");
            sqlSelectQuery.Append("  c.Criticality,  ");
            sqlSelectQuery.Append("  a.DataDisposed, ");
            sqlSelectQuery.Append("  e.AltName EmployeeAltName, ");
            sqlSelectQuery.Append("  e.Name EmployeeName, ");
            sqlSelectQuery.Append("  e.EmployeeNO EmployeeNo, ");
            sqlSelectQuery.Append("  a.EstLife, ");
            sqlSelectQuery.Append("  l2.L2Altname, ");
            sqlSelectQuery.Append("  l2.L2Code, ");
            sqlSelectQuery.Append("  l2.L2name, ");
            sqlSelectQuery.Append("  l3.L3AltDesc, ");
            sqlSelectQuery.Append("  l3.L3Desc, ");
            sqlSelectQuery.Append("  l3.L3No, ");
            sqlSelectQuery.Append("  l4.L4AltDescription, ");
            sqlSelectQuery.Append("  l4.L4Description, ");
            sqlSelectQuery.Append("  l4.L4No, ");
            sqlSelectQuery.Append("  l5.L5AltDescription, ");
            sqlSelectQuery.Append("  l5.L5Description, ");
            sqlSelectQuery.Append("  l5.L5No, ");
            sqlSelectQuery.Append("  l.LocationAltDescription, ");
            sqlSelectQuery.Append("  l.LocationDescription, ");
            sqlSelectQuery.Append("  l.LocationNo, ");
            sqlSelectQuery.Append("  a.Manufacturer, ");
            sqlSelectQuery.Append("  a.ModelNumber, ");
            sqlSelectQuery.Append("  a.NotesToTech, ");
            sqlSelectQuery.Append("  a.PartsListID, ");
            sqlSelectQuery.Append("  a.PurchasePrice, ");
            sqlSelectQuery.Append("  sec.SectorAltname, ");
            sqlSelectQuery.Append("  sec.SectorCode, ");
            sqlSelectQuery.Append("  sec.SectorName, ");
            sqlSelectQuery.Append("  a.SerialNumber, ");
            sqlSelectQuery.Append("  s.SupplierName, ");
            sqlSelectQuery.Append("  s.SupplierNo, ");
            sqlSelectQuery.Append("  a.Warranty_ContractExpiry, ");
            sqlSelectQuery.Append("  a.Warranty_ContractNotes, ");
            sqlSelectQuery.Append("  w2.Warranty_contract, ");
            sqlSelectQuery.Append("  w.WorkTrade, ");
            sqlSelectQuery.Append("  a5.AssetCondition,    ");
            sqlSelectQuery.Append("  a.AssetStatusID, ");
            sqlSelectQuery.Append("  a.LocationID, ");
            sqlSelectQuery.Append("  a.EmployeeID ");
            sqlSelectQuery.Append("  FROM assets a ");
            sqlSelectQuery.Append("	INNER JOIN LOCATION l ON l.LocationID = a.LocationID	  ");
            sqlSelectQuery.Append("	INNER JOIN L2 l2 ON l2.L2ID = l.L2ID	  ");
            sqlSelectQuery.Append("	INNER JOIN sector sec ON sec.SectorID = l2.SectorID	  ");
            sqlSelectQuery.Append(" LEFT JOIN L2Classes ON l2.L2ClassID = L2Classes.L2ClassID	 ");
            sqlSelectQuery.Append("	LEFT JOIN assetcategories a2 ON a2.AssetCatID = a.AssetCatID	  ");
            sqlSelectQuery.Append("	LEFT JOIN assetsubcategories a3 ON a3.AssetSubCatID = a.AssetSubCatID	  ");
            sqlSelectQuery.Append("	LEFT JOIN assetclasses a4 ON a4.AssetClassID = a.AssetClassID	  ");
            sqlSelectQuery.Append("	LEFT JOIN assetconditions a5 ON a5.AssetConditionID = a.AssetConditionID	  ");
            sqlSelectQuery.Append("	INNER JOIN assetstatus a6 ON a6.AssetStatusID = a.AssetStatusID	  ");
            sqlSelectQuery.Append("	LEFT JOIN criticality c ON c.id = a.CriticalityID	  ");
            sqlSelectQuery.Append("	LEFT JOIN worktrade w ON w.WorkTradeID = a.WorkTradeID	  ");
            sqlSelectQuery.Append("	LEFT JOIN warrantycontract w2 ON w2.Warranty_contractID = a.Warranty_ContractID	  ");
            sqlSelectQuery.Append("	LEFT JOIN L3 l3 ON l3.L3ID = l.L3ID	  ");
            sqlSelectQuery.Append("	INNER JOIN L4 l4 ON l4.L4ID = l.L4ID	  ");
            sqlSelectQuery.Append("	LEFT JOIN L5 l5 ON l5.L5ID = l.L5ID	  ");
            sqlSelectQuery.Append("	LEFT JOIN employees e ON e.EmployeeID = a.EmployeeID	  ");
            sqlSelectQuery.Append("	LEFT JOIN partslist p ON p.PartsListID = a.PartsListID	  ");
            sqlSelectQuery.Append("	LEFT JOIN MaintGroup mg ON mg.GroupID = a.GroupID	  ");
            sqlSelectQuery.Append("	LEFT JOIN suppliers s ON s.SupplierID = a.SupplierID	  ");
            sqlSelectQuery.Append("	LEFT JOIN Suppliers s2 ON s2.SupplierID = a.ContractorID");
            sqlSelectQuery.AppendFormat(" where 1=1 {0}", strWhere);

            PermissionAccess objPermissionAccess = SetEmployeePermission(objemployees, clientCode, objAccount.DbType);

            if (currentTimeStamp != null)
            {
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.Append("AND ((a.CreatedDate >= @currentTimeStamp OR a.ModifiedDate >= @currentTimeStamp)");


                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( ( a.LocationID in (select LocationID from Employees_Location where empID = {0})) ", EmployeeID);
                        sqlSelectQuery.AppendFormat(" 	        Or ( a.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp)   )) ) ", EmployeeID);

                    }

                    sqlSelectQuery.AppendFormat(" 	    Or  ( l.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ", EmployeeID);

                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectQuery.Append(" AND ( (a.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR a.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                    
                    //sqlSelectQuery.AppendFormat(" 	    Or  ( l.L2ID IN	 ");
                    //sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    //sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    //sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", EmployeeID);
                    //sqlSelectQuery.AppendFormat(" 	        UNION SELECT L2Id	 ");
                    //sqlSelectQuery.AppendFormat(" 	        FROM employees	 ");
                    //sqlSelectQuery.AppendFormat(" 	        WHERE employeeId = {0})) ", EmployeeID);

                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( ( a.LocationID in (select LocationID from Employees_Location where empID = {0}) ) ", EmployeeID);
                        sqlSelectQuery.AppendFormat(" 	        Or ( a.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')))   ) ) ", EmployeeID);

                    }

                    sqlSelectQuery.AppendFormat(" 	    Or ( l.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) )  ");
                }

            }
            if (!IsCentral)
            {
                sqlSelectQuery.AppendFormat(" 	    AND  ( l.L2ID IN	 ");
                sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}	 ", EmployeeID);
                sqlSelectQuery.AppendFormat(" 	        UNION SELECT L2Id	 ");
                sqlSelectQuery.AppendFormat(" 	        FROM employees	 ");
                sqlSelectQuery.AppendFormat(" 	        WHERE employeeId = {0})	 ", EmployeeID);


                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.AppendFormat(" 	    Or  ( l.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");

                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectQuery.AppendFormat(" 	    Or ( ( l.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }


                

                if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    sqlSelectQuery.AppendFormat(" 	        AND ( a.LocationID in (select LocationID from Employees_Location where empID = {0}) ",EmployeeID);

                    if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( a.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp)   )) ) ", EmployeeID);
                    }
                    else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( a.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')))  ) ) ", EmployeeID);
                    }

                }
            }

           

            #endregion

            //var resultAssetList = DbContext.ObjectContext.ExecuteStoreQuery<AssetListModel>(sqlSelectQuery.ToString()).ToList();

            List<_Asset> resultAssetList = new List<_Asset>();
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    List<Asset> _resultAssetList = new List<Asset>();

                    _resultAssetList = objContext.ExecuteQueryWithClientID<Asset>(sqlSelectQuery.ToString(), parameters, clientCode).ToList();
                    objAssetListResult.TotalCount = _resultAssetList.Count();

                    if (_resultAssetList.Count > 0)
                    {
                        objAssetListResult.TotalPages = (_resultAssetList.Count / PageSize);
                        if (_resultAssetList.Count % PageSize != 0) objAssetListResult.TotalPages++;
                        _resultAssetList = objContext.GetPageList<Asset>(_resultAssetList, PageIndex, PageSize);

                        resultAssetList = _resultAssetList.Select(item => new _Asset()
                        {
                            AltAssetCategory = item.AltAssetCategory,
                            AltAssetClass = item.AltAssetClass,
                            AltAssetCondition = item.AltAssetCondition,
                            AltAssetStatusDesc = item.AltAssetStatusDesc,
                            AltAssetSubCategory = item.AltAssetSubCategory,
                            AltCriticality = item.AltCriticality,
                            AltSupplierName = item.AltSupplierName,
                            AltWarranty_contract = item.AltWarranty_contract,
                            AltWorkTrade = item.AltWorkTrade,
                            AssetAltDescription = item.AssetAltDescription,
                            AssetCatCode = item.AssetCatCode,
                            AssetCategory = item.AssetCategory,
                            AssetClass = item.AssetClass,
                            AssetClassCode = item.AssetClassCode,
                            AssetConditionCode = item.AssetConditionCode,
                            AssetDescription = item.AssetDescription,
                            AssetID = item.AssetID,
                            AssetNotes = item.AssetNotes,
                            AssetNumber = item.AssetNumber,
                            AssetStatusDesc = item.AssetStatusDesc,
                            AssetSubCatCode = item.AssetSubCatCode,
                            AssetSubCategory = item.AssetSubCategory,
                            CommissionedDate = item.CommissionedDate,
                            Criticality = item.Criticality,
                            DataDisposed = item.DataDisposed,
                            EmployeeAltName = item.EmployeeAltName,
                            EmployeeName = item.EmployeeName,
                            EmployeeNO = item.EmployeeNO,                             
                            EstLife = item.EstLife,
                            L2AltName = item.L2AltName,
                            L2Code = item.L2Code,
                            L2Name = item.L2Name,
                            L3AltDesc = item.L3AltDesc,
                            L3Desc = item.L3Desc,
                            L3No = item.L3No,
                            L4AltDescription = item.L4AltDescription,
                            L4Description = item.L4Description,
                            L4No = item.L4No,
                            L5AltDescription = item.L5AltDescription,
                            L5Description = item.L5Description,
                            L5No = item.L5No,
                            LocationAltDescription = item.LocationAltDescription,
                            LocationDescription = item.LocationDescription,
                            LocationNo = item.LocationNo,
                            Manufacturer = item.Manufacturer,
                            ModelNumber = item.ModelNumber,
                            NotesToTech = item.NotesToTech,
                            PartsListID = item.PartsListID,
                            PurchasePrice = item.PurchasePrice,
                            SectorAltName = item.SectorAltName,
                            SectorCode = item.SectorCode,
                            SectorName = item.SectorName,
                            SerialNumber = item.SerialNumber,
                            SupplierName = item.SupplierName,
                            SupplierNo = item.SupplierNo,
                            Warranty_ContractExpiry = item.Warranty_ContractExpiry,
                            Warranty_ContractNotes = item.Warranty_ContractNotes,
                            Warranty_contract = item.Warranty_contract,
                            WorkTrade = item.WorkTrade,
                            AssetCondition = item.AssetCondition,                            
                            AssetStatusID = item.AssetStatusID,
                            LocationID = item.LocationID,
                            EmployeeID = item.EmployeeID
                        }).ToList();

                    }
                    else
                    {
                        objAssetListResult.TotalPages = 0;
                        resultAssetList = new List<_Asset>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            objAssetListResult.AssetList = resultAssetList;
            objAssetListResult.PageSize = PageSize;
            objAssetListResult.PageIndex = PageIndex;
            objAssetListResult.SynchTime = currentTimeStamp.ToString();
            objAssetListResult.EmployeeID = EmployeeID;
            objAssetListResult.IsCentral = IsCentral;
            objAssetListResult.AssetIdToRefresh = AssetIdToRefresh;
            objAssetListResult.JobOrderHistoryCount = JobOrderHistoryCount;

            if (objAssetListResult.AssetList.Count > 0)
            {
                List<int?> partListIds = objAssetListResult.AssetList.Select(a => a.PartsListID).ToList();
                string commaSepratedpartListIds = string.Join("','", partListIds);

                List<int> AssetIds = objAssetListResult.AssetList.Select(a => a.AssetID).ToList();
                string commaSepratedAssetIds = string.Join("','", AssetIds);

                List<PartsListDetail> BOMListResult = new List<PartsListDetail>();
                List<AssetRelationship> AssetRelationshipResult = new List<AssetRelationship>();
                List<Reading> PMMeterResult = new List<Reading>();

                string strBOMListResultQuery = "SELECT * FROM partslistdetails where PartsListID IN ( '" + commaSepratedpartListIds + "' )";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        BOMListResult = objContext.ExecuteQueryWithClientID<PartsListDetail>(strBOMListResultQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string strAssetRelationshipResultQuery = "SELECT assetrelationship.* FROM assetrelationship where assetrelationship.AssetID IN ( '" + commaSepratedAssetIds + "' )";

                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        AssetRelationshipResult = objContext.ExecuteQueryWithClientID<AssetRelationship>(strAssetRelationshipResultQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string strPMMeterResultQuery = "SELECT reading.readingdate, reading.readingid, reading.currentreading, pmmetermaster.meterno,pmmetermaster.AssetID FROM   reading INNER JOIN pmmetermaster ON pmmetermaster.metermasterid = reading.metermasterid WHERE  pmmetermaster.AssetID IN ( '" + commaSepratedAssetIds + "' )";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        PMMeterResult = objContext.ExecuteQueryWithClientID<Reading>(strPMMeterResultQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                sqlPMScheduleQuery.Append("	SELECT meterid, 	 ");
                sqlPMScheduleQuery.Append("	       pmmeters.meterno, 	 ");
                sqlPMScheduleQuery.Append("	       pmmeters.meterdescription, 	 ");
                sqlPMScheduleQuery.Append("	       incfreq, 	 ");
                sqlPMScheduleQuery.Append("	       incpmdue, 	 ");
                sqlPMScheduleQuery.Append("	       pmactive, 	 ");
                sqlPMScheduleQuery.Append("	       pmmetermaster.AssetID  assetid,	 ");
                sqlPMScheduleQuery.Append("	       worktrade.worktrade WorkTradeName, 	 ");
                sqlPMScheduleQuery.Append("	       worktrade.altworktrade, 	 ");
                sqlPMScheduleQuery.Append("	       mainenancedivision.maintdivisioncode, 	 ");
                sqlPMScheduleQuery.Append("	       maintenancedepartment.maintdeptcode, 	 ");
                sqlPMScheduleQuery.Append("	       maintsubdept.maintsubdeptcode 	 ");
                sqlPMScheduleQuery.Append("	FROM   pmmeters 	 ");
                sqlPMScheduleQuery.Append("	       INNER JOIN mainenancedivision 	 ");
                sqlPMScheduleQuery.Append("	               ON mainenancedivision.maintdivisionid = pmmeters.maintdivisionid 	 ");
                sqlPMScheduleQuery.Append("	       INNER JOIN maintenancedepartment 	 ");
                sqlPMScheduleQuery.Append("	               ON maintenancedepartment.maintdeptid = pmmeters.maintdeptid 	 ");
                sqlPMScheduleQuery.Append("	       INNER JOIN maintsubdept 	 ");
                sqlPMScheduleQuery.Append("	               ON maintsubdept.maintsubdeptid = pmmeters.maintsubdeptid 	 ");
                sqlPMScheduleQuery.Append("	       INNER JOIN worktrade 	 ");
                sqlPMScheduleQuery.Append("	               ON worktrade.worktradeid = pmmeters.worktradeid 	 ");
                sqlPMScheduleQuery.Append("	       INNER JOIN pmmetermaster 	 ");
                sqlPMScheduleQuery.Append("	               ON pmmeters.metermasterid = pmmetermaster.metermasterid 	 ");
                sqlPMScheduleQuery.Append("	WHERE  pmmeters.metermasterid IN (SELECT metermasterid 	 ");
                sqlPMScheduleQuery.Append("	                                  FROM   pmmetermaster 	 ");
                sqlPMScheduleQuery.AppendFormat("	                                  WHERE  assetid IN (  '" + commaSepratedAssetIds + "' ) 	 ");
                sqlPMScheduleQuery.Append("	       AND pmmeters.maintsubdeptid IN (SELECT maintsubdeptid 	 ");
                sqlPMScheduleQuery.Append("	                                       FROM   employees_maintsubdept 	 ");
                sqlPMScheduleQuery.AppendFormat("	                                       WHERE  empid = {0} 	 ", EmployeeID);
                sqlPMScheduleQuery.Append("	                                       UNION 	 ");
                sqlPMScheduleQuery.Append("	                                       SELECT maintsubdeptid 	 ");
                sqlPMScheduleQuery.Append("	                                       FROM   employees 	 ");
                sqlPMScheduleQuery.AppendFormat("	                                       WHERE  employeeid = {0}))  ", EmployeeID);

                List<PmMeter> resultPMMeterScheduleList = new List<PmMeter>();
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        resultPMMeterScheduleList = objContext.ExecuteQueryWithClientID<PmMeter>(sqlPMScheduleQuery.ToString(), parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                //var JOResult = DbContext.ObjectContext.ExecuteStoreQuery<JoHistoryModel>("SELECT  * from workorders WHERE assetid IN (  '" + commaSepratedAssetIds + "' ) ORDER BY CreatedDate DESC,ModifiedDate DESC").Take(JobOrderHistoryCount).ToList();

                sqlJOHistoryQuery.Append("SELECT workorders.*, " +
                                        "Isnull((SELECT Sum(workorderlabor.tothour) " +
                                               " FROM   workorderlabor " +
                                                "WHERE  workorders.workorderno = workorderlabor.workorderno), 0) " +
                                        " " +
                                        "ManHours, " +
                                        "Isnull((SELECT Sum(workorderlabor.totcost) " +
                                                "FROM   workorderlabor " +
                                                "WHERE  workorders.workorderno = workorderlabor.workorderno), 0) " +
                                        " " +
                                        "LabourCost, " +
                                        "Isnull((SELECT Sum(direct.qty * direct.price) " +
                                                "FROM   direct " +
                                                "WHERE  direct.wono = workorders.workorderno), 0) " +
                                        " DICost, " +
                                        "( Isnull((SELECT Sum(issue.qtyissue * issue.price3) " +
                                                  "FROM   issue " +
                                                  "WHERE  issue.wono = workorders.workorderno), 0) - " +
                                            "Isnull((SELECT Sum([return].qty * [return].retprice) " +
                                                    "FROM   [return] " +
                                                    "WHERE  [return].wono = workorders.workorderno), 0) ) " +
                                        " ItemCost " +
                                        "FROM   workorders WHERE  workorders.assetid IN (  '" + commaSepratedAssetIds + "' ) ORDER BY CreatedDate DESC,ModifiedDate DESC");


                List<_WorkOrder> JOResult = new List<_WorkOrder>();
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        List<WorkOrder> _JOResult = new List<WorkOrder>();
                        _JOResult = objContext.ExecuteQueryWithClientID<WorkOrder>(sqlJOHistoryQuery.ToString(), parameters, clientCode).ToList();
                        
                        JOResult = _JOResult.Select(item => new _WorkOrder()
                          {
                              AssetID = item.AssetID,
                              DateReceived = item.DateReceived,
                              DICost = item.DICost,
                              ItemCost = item.ItemCost,
                              LabourCost = item.LabourCost,
                              ManHours = item.ManHours,
                              ProblemDescription = item.ProblemDescription,
                              WorkorderNo = item.WorkorderNo,
                              WorkStatusID = item.WorkStatusID
                          }).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                string strfileAttachmentResultQuery = "SELECT E.* FROM extassetfiles E WHERE E.FkId IN (  '" + commaSepratedAssetIds + "' ) AND E.ModuleType = 'A' ";
                List<ExtAssetFile> fileAttachmentResult = new List<ExtAssetFile>();
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        fileAttachmentResult = objContext.ExecuteQueryWithClientID<ExtAssetFile>(strfileAttachmentResultQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                #region Set Assosiated BOM List

                objAssetListResult.AssetList.ForEach(a =>
                {
                    a.PartslistdetailsTable = new List<PartsListDetail>();
                });


                if (BOMListResult.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {
                        if (BOMListResult.Any(BOM => BOM.PartsListID == a.PartsListID))
                        {
                            a.PartslistdetailsTable = BOMListResult.Where(BOM => BOM.PartsListID == a.PartsListID).ToList();
                        }
                    });
                }
                #endregion

                #region Set Assosiated Relationship

                objAssetListResult.AssetList.ForEach(a =>
                {
                    a.AssetrelationshipTable = new List<AssetRelationship>();
                });

                if (AssetRelationshipResult.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {
                        if (AssetRelationshipResult.Any(AR => AR.AssetID == a.AssetID))
                        {
                            a.AssetrelationshipTable = AssetRelationshipResult.Where(AR => AR.AssetID == a.AssetID).ToList();
                        }
                    });
                }
                #endregion

                #region Set Assosiated PM-Meter History

                objAssetListResult.AssetList.ForEach(a =>
                {
                    a.PMMeterHistory = new List<Reading>();
                });

                if (PMMeterResult.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {
                        if (PMMeterResult.Any(PM => PM.AssetID == a.AssetID))
                        {
                            a.PMMeterHistory = PMMeterResult.Where(PM => PM.AssetID == a.AssetID).ToList();
                        }
                    });
                }
                #endregion

                #region Set Assosiated PM-Meter Schedule

                objAssetListResult.AssetList.ForEach(a =>
                {
                    a.PMMeterSchedule = new List<PmMeter>();
                });

                if (resultPMMeterScheduleList.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {
                        if (resultPMMeterScheduleList.Any(PM => PM.AssetID == a.AssetID))
                        {
                            a.PMMeterSchedule = resultPMMeterScheduleList.Where(PM => PM.AssetID == a.AssetID).ToList();
                        }
                    });
                }
                #endregion

                #region Set Assosiated JO History

                objAssetListResult.AssetList.ForEach(a =>
                {
                    a.JoHistory = new List<_WorkOrder>();
                });

                if (JOResult.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {
                        if (JOResult.Any(JO => JO.AssetID == a.AssetID))
                        {
                            a.JoHistory = JOResult.Where(JO => JO.AssetID == a.AssetID).Take(JobOrderHistoryCount).ToList();
                        }
                    });
                }
                #endregion

                #region Set Assosiated Attachments

                objAssetListResult.AssetList.ForEach(w =>
                {
                    w.ExtassetfileTable = new List<ExtAssetFile>();
                });

                if (fileAttachmentResult.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {

                        if (fileAttachmentResult.Any(attachment => attachment.FkId == a.AssetID.ToString()))
                        {
                            a.ExtassetfileTable = fileAttachmentResult.Where(att => att.FkId == a.AssetID.ToString()).ToList();
                        }
                    });
                }
                #endregion


                List<AssetSpecification> specificationTableList = new List<AssetSpecification>();

                string strSpecificationTableQuery = @"select  AssetSpecification.*,Specifications.SpecDesc,Specifications.SpecAltDesc,Specifications.UOM 
                            from AssetSpecification 
                            inner join Specifications on Specifications.SpecID = AssetSpecification.SpecID
                            where assetID in ('" + commaSepratedAssetIds + "' )";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        specificationTableList = objContext.ExecuteQueryWithClientID<AssetSpecification>(strSpecificationTableQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objAssetListResult.AssetList.ForEach(w =>
                {
                    w.SpecificationTable = new List<AssetSpecification>();
                });

                if (specificationTableList.Count > 0)
                {
                    objAssetListResult.AssetList.ForEach(a =>
                    {

                        if (specificationTableList.Any(w => w.AssetID == a.AssetID))
                        {
                            a.SpecificationTable = specificationTableList.Where(w => w.AssetID == a.AssetID).ToList();
                        }
                    });
                }


            }

            return objAssetListResult;
        }

        public static Account GetAccountType(string clientCode)
        {
            using (ServiceContext context = new ServiceContext(0))
            {
                Account objAccount = new Account();
                objAccount.ClientCode = clientCode;
                return context.GetMasterAccountDetail(clientCode);
            }

        }

        public static PermissionAccess SetEmployeePermission(employees emp,string clientCode,int? objAccountDbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();


                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = emp.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "UserGroupId",
                    Value = emp.UserGroupId,
                    DBType = DbType.Int32
                });

                string query = string.Empty;
                PermissionAccess objPermission = new PermissionAccess();

                using (ServiceContext objDapperContext = new ServiceContext(0))
                {
                    if (objAccountDbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = @"SELECT  
 (REPLACE(REPLACE(ModuleName,' ',''),'/','') ||  '_' ||  REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') ||  '_' ||  REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(AllowAccess) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";
                    }
                    else
                    {
                        query = @"SELECT  
 CONCAT(REPLACE(REPLACE(ModuleName,' ',''),'/','') , '_' , REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') , '_' , REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(CAST(AllowAccess AS DECIMAL)) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";

                    }

                    List<Permission> lst = objDapperContext.ExecuteQueryWithClientID<Permission>(query, parameters,clientCode).ToList();

                    foreach (Permission lstPermission in lst)
                    {
                        PropertyInfo info = objPermission.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstPermission.PermissionName.ToString().ToLower());
                        if (info != null)
                        {
                            /*Set the Value to Model*/
                            info.SetValue(objPermission, lstPermission.AllowAccess);
                        }
                    }
                }
                return objPermission;

            }
            catch
            {
                throw;
            }
        }

    }
}
