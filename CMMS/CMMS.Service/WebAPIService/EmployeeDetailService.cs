﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class EmployeeDetailService : DBExecute
    {

        #region "Client Wise Methods"

        public static string GetEmployeeStatus(string clientCode, string UserID, string Password)
        {
            try
            {
                List<Employee> objemployees = GetEmployeeByUserIdPassword(clientCode, UserID, Password);

                if (objemployees.Count > 0)
                {
                    EmployeeStatuse objemployeeStatuse = new EmployeeStatuse();
                    objemployeeStatuse.EmployeeStatusId = Convert.ToInt32(objemployees[0].EmployeeStatusId);                     
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        List<EmployeeStatuse> lst = objContext.SearchWithClientID<EmployeeStatuse>(objemployeeStatuse, 0, null, null, clientCode).ToList<EmployeeStatuse>();
                        if (lst.Count > 0)
                        {
                            return lst[0].EmployeeStatus;
                        }
                        return string.Empty;
                    }
                }
                else {
                    return string.Empty;
                }

                //string Query = "select employeeStatuses.EmployeeStatus from Employees inner join employeeStatuses on Employees.EmployeeStatusId = employeeStatuses.EmployeeStatusId ";
                //Collection<DBParameters> parameters = new Collection<DBParameters>();

                //if (!string.IsNullOrWhiteSpace(clientCode))
                //{
                //    parameters.Add(
                //        new DBParameters()
                //        {
                //            Name = "UserID",
                //            Value = UserID,
                //            DBType = DbType.String
                //        }
                //    );

                //    parameters.Add(
                //        new DBParameters()
                //        {
                //            Name = "Password",
                //            Value = Password,
                //            DBType = DbType.String
                //        }
                //    );

                //    Query += " Where Employees.UserID = @UserID and Employees.Password=@Password";
                //}

                //using (ServiceContext objContext = new ServiceContext(0))
                //{
                //    return objContext.ExecuteQueryWithClientID<string>(Query, parameters, clientCode).ToList().FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Employee> GetEmployeeByUserIdPassword(string clientCode, string UserID, string Password)
        {
            try
            {

                Employee objemployees = new Employee();
                //// objemployees = obj.CheckUserSignIn(username, password);
               // string Query = "select * from Employees ";
                //Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "UID",
                    //        Value = UserID,
                    //        DBType = DbType.String
                    //    }
                    //);

                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "Pwd",
                    //        Value = Password,
                    //        DBType = DbType.String
                    //    }
                    //);

                    //Query += " Where AccStatus = @AccStatus";
                    //Query += " Where Employees.UserID = '" + UserID + "' and Employees.Password='" + Password + "'";
                    //Query += " Where Employees.UserID = @UID and Employees.Password=@Pwd";

                    objemployees.UserID = UserID;
                    objemployees.Password = Password;

                }

                using (ServiceContext objContext = new ServiceContext(0))
                {

                    //objemployees.UserID = username;
                    //objemployees.Password = password;
                    return objContext.SearchWithClientID<Employee>(objemployees,0, null, null, clientCode).ToList<Employee>();

                    //return objContext.ExecuteQueryWithClientID<Employee>(Query, parameters, clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Employee> GetEmployeeByEmployeeIdPassword(string clientCode, int UserID, string Password)
        {
            try
            {
                string Query = "select * from Employees ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "UID",
                    //        Value = UserID,
                    //        DBType = DbType.String
                    //    }
                    //);

                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "Pwd",
                    //        Value = Password,
                    //        DBType = DbType.String
                    //    }
                    //);

                    //Query += " Where AccStatus = @AccStatus";
                    Query += " Where Employees.EmployeeID = " + UserID + " and Employees.Password='" + Password + "'";
                }

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    return objContext.ExecuteQueryWithClientID<Employee>(Query, parameters, clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static int DeleteMobileSession(string strTime, string clientCode)
        {

            try
            {
                string Query = "Delete from mobileSession ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "strTime",
                    //        Value = strTime,
                    //        DBType = DbType.String
                    //    }

                    //);

                    Account objAccount = GetAccountType(clientCode);
                    if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    {
                        Query += " Where CONVERT(datetime,lastheartbeat)  <= CONVERT(datetime,'" + strTime + "')";
                    }
                    else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())                    
                    {

                        strTime = Convert.ToDateTime(strTime).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);

                        //Query += " Where TO_CHAR(lastheartbeat, 'DD-MM-YYYY HH:MI:SS AM') <= TO_CHAR('" + strTime + "', 'DD-MM-YYYY HH:MI:SS AM')";
                        Query += " Where lastheartbeat <= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')";
                    }
                }

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    return objContext.ExecuteWithClientID(Query, parameters, clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static List<mobilesession> GetSessionsByEmployeeId(int employeeId, string clientCode)
        {
            List<mobilesession> msQuery = new List<mobilesession>();
            try
            {
                string Query = "select * from mobileSession ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if (!string.IsNullOrWhiteSpace(clientCode ))
                {
                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "employeeId",
                    //        Value = employeeId,
                    //        DBType = DbType.Int32
                    //    }
                    //);

                    if (employeeId > 0)
                    {
                        Query += " Where EmployeeId = " + employeeId;
                    }
                }


                using (ServiceContext objContext = new ServiceContext(0))
                {
                    msQuery = objContext.ExecuteQueryWithClientID<mobilesession>(Query, parameters, clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return msQuery;

        }

        public static List<L2> GetAllL2(string clientCode)
        {
            List<L2> msQuery = new List<L2>();
            try
            {
                string Query = "select * from L2 ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    msQuery = objContext.ExecuteQueryWithClientID<L2>(Query, parameters, clientCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return msQuery;

        }

        public static string CheckConlimits(string clientCode, List<mobilesession> lst)
        {
            StringBuilder sqlSelectQuery = new StringBuilder();
            MobileConlimit objMobileConlimitModel = new MobileConlimit();
            Account objAccount = GetAccountType(clientCode);

            objMobileConlimitModel.Conn = Convert.ToString(objAccount.MobAppLicCon);
            objMobileConlimitModel.ConnectionKey = Convert.ToString(objAccount.MobAppLicNamed);
            
            //try
            //{
            //    string Query = "select * from mobileConlimit ";
            //    Collection<DBParameters> parameters = new Collection<DBParameters>();

            //    using (ServiceContext objContext = new ServiceContext(0))
            //    {
            //        objMobileConlimitModel = objContext.ExecuteQueryWithClientID<MobileConlimit>(Query, parameters, clientCode).ToList().FirstOrDefault();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            if (objMobileConlimitModel != null)
            {
                //1.0 Check License Status
                string conType = string.Empty;
                conType = objMobileConlimitModel.Conn;
                //1.1. If license is by concurrent users, do a count check and if it exceeds then return false and exit function

                if (conType.Equals("0"))
                {
                    int cnt = string.IsNullOrEmpty(Convert.ToString(objMobileConlimitModel.ConnectionKey)) ? 0 : Convert.ToInt32(Convert.ToString(objMobileConlimitModel.ConnectionKey));
                    lst = GetSessionsByEmployeeId(0, clientCode);
                    
                    int noOfSession = lst.Count;
                    if (noOfSession >= cnt)
                    {
                        return ConstatntMessages.ConlimitExceedMessage;
                    }
                }
                else if (conType.Equals("1"))
                {
                    //1.2 Check if license is by site, if by site, enforce that only one site can exist in sessions, if that is not ok, return false and exit function
                    if (CheckAlreadyExceededSites(objMobileConlimitModel, clientCode))
                    {
                        return ConstatntMessages.DeptLimitExceedMessage;
                    }
                }
                else
                {
                    //1.3 If not "1" or "0" then problem   
                    return string.Empty;
                }
                return string.Empty;
            }
            return string.Empty;
        }

        public static bool CheckAlreadyExceededSites(MobileConlimit objMobileConlimitModel, string clientCode)
        {
            StringBuilder sqlSelectQuery = new StringBuilder();

            if ((objMobileConlimitModel != null))
            {
                if (objMobileConlimitModel.Conn.Equals("1"))
                {
                    int cnt = Convert.ToInt32(objMobileConlimitModel.ConnectionKey);
                    //int noOfDepts = DbContext.L2.Count();
                    int noOfDepts = GetAllL2(clientCode).Count;
                    if (noOfDepts > cnt + 1)
                    {
                        return true;
                    }
                }
                if (!objMobileConlimitModel.Conn.Equals("0") && !objMobileConlimitModel.Conn.Equals("1"))
                {
                    return false;
                }
            }
            return false;
        }

        public static int InsertMobileSession(int EmployeeId, string clientCode)
        {
            try
            {
                string Query = string.Empty;

                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = "Insert into mobileSession(EmployeeId,logintime,lastheartbeat) values (" + EmployeeId.ToString() + ",getdate(),getdate()) ";
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = "Insert into mobileSession(EmployeeId,logintime,lastheartbeat) values (" + EmployeeId.ToString() + ",sysDate,sysDate) ";
                }

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    return objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static int InsertLoginLog(LoginLog objLoginLog, string clientCode)
        {
            try
            {
                string Query = string.Empty;


                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = "Insert into LoginLog (EmployeeId,LoginDateTime,IPAddress,Latitude,Longitude) values (" + objLoginLog.EmployeeId.ToString() + ",getdate(),'" + objLoginLog.IPAddress + "','" + objLoginLog.Latitude + "','" + objLoginLog.Longitude + "')";
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = "Insert into LoginLog (EmployeeId,LoginDateTime,IPAddress,Latitude,Longitude) values (" + objLoginLog.EmployeeId.ToString() + ",sysDate,'" + objLoginLog.IPAddress + "','" + objLoginLog.Latitude + "','" + objLoginLog.Longitude + "')";
                }


                using (ServiceContext objContext = new ServiceContext(0))
                {
                    return objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Logout Service
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int Logout(LogoutResult model, string clientCode)
        {

            try
            {
                string Query = string.Empty;
                Query = "Delete from mobileSession where EmployeeId = " + model.EmployeeID;

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    int DeleteFromMobileSession = objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);

                    if (DeleteFromMobileSession == 0)
                    {
                        SaveLoginLogOnLogout(model.EmployeeID, clientCode);
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveLoginLogOnLogout(int EmployeeId, string clientCode)
        {
            try
            {
                string Query = string.Empty;
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = "Update LoginLog set LogoutDateTime = '" + DBExecute.GetEnglishDateSecond(System.DateTime.Now) + "' where employeeId = " + EmployeeId.ToString() + " and LogoutDateTime is null";
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = "Update LoginLog set LogoutDateTime = sysdate where employeeId = " + EmployeeId.ToString() + " and LogoutDateTime is null";
                }

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check If User is Active or not
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public bool IsUserActive(int? EmployeeID, string clientCode)
        {
            if (EmployeeID > 0)
            {
                string queryEmpStatusCheck = string.Empty;
                string Query = "select employeeStatuses.EmployeeStatus from Employees inner join employeeStatuses on Employees.EmployeeStatusId = employeeStatuses.EmployeeStatusId ";
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                if ( !string.IsNullOrWhiteSpace(clientCode))
                {
                    //parameters.Add(
                    //    new DBParameters()
                    //    {
                    //        Name = "UserID",
                    //        Value = EmployeeID,
                    //        DBType = DbType.Int32
                    //    }
                    //);
                    Query += " Where Employees.employeeID = " + EmployeeID;
                }

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    queryEmpStatusCheck = objContext.ExecuteQueryWithClientID<string>(Query, parameters, clientCode).ToList().FirstOrDefault();
                }


                //Check if User is Terminated or In-Active               
                if (!string.IsNullOrWhiteSpace(queryEmpStatusCheck))
                {
                    if ((queryEmpStatusCheck.ToLower() == "terminated") || (queryEmpStatusCheck.ToLower() == "in-active"))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Keep Last Heartbeat Updated
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public KeepLastHeartBeatUpdated KeepLastHeartBeatUpdated(int employeeId, string clientCode)
        {
            KeepLastHeartBeatUpdated ObjKeepLastHeartBeatUpdated = new KeepLastHeartBeatUpdated();
            try
            {
                employees empRow = null;
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    empRow = objContext.SearchWithClientID<employees>(new employees(), 0, string.Empty, string.Empty, clientCode).Where(s => s.EmployeeID == employeeId).FirstOrDefault();
                }

                //var empRow = DbContext.employees.FirstOrDefault(e => e.EmployeeID == employeeId);
                if (empRow != null)
                {
                    //DbContext.ObjectContext.ExecuteStoreCommand("Update mobileSession set lastheartbeat = '" + DBExecute.GetEnglishDateSecond(DateTime.Now) + "' where employeeId = " + employeeId.ToString());
                    try
                    {
                        string Query = string.Empty;
                        Query = "Update mobileSession set lastheartbeat = '" + DBExecute.GetEnglishDateSecond(DateTime.Now) + "' where employeeId = " + employeeId.ToString();
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    ObjKeepLastHeartBeatUpdated.HeartbeatUpdated = 1;
                    ObjKeepLastHeartBeatUpdated.LastConfigChangeDateTime = empRow.LastConfigChangeDateTime;
                }
                else
                {
                    ObjKeepLastHeartBeatUpdated.HeartbeatUpdated = 0;
                    ObjKeepLastHeartBeatUpdated.LastConfigChangeDateTime = null;
                }
                ObjKeepLastHeartBeatUpdated.employeeId = employeeId;
                return ObjKeepLastHeartBeatUpdated;
            }
            catch (Exception ex)
            {
                ObjKeepLastHeartBeatUpdated.HeartbeatUpdated = 0;
                ObjKeepLastHeartBeatUpdated.employeeId = employeeId;
                ObjKeepLastHeartBeatUpdated.LastConfigChangeDateTime = null;
                return ObjKeepLastHeartBeatUpdated;
            }
        }

        public static Account GetAccountType(string clientCode)
        {
            using (ServiceContext context = new ServiceContext(0))
            {
                Account objAccount = new Account();
                objAccount.ClientCode = clientCode;
                return context.GetMasterAccountDetail(clientCode);
            }

        }

        #endregion

        public int testreturnvalue()
        {
            string clientCode = "client6";

            try
            {

                test objTest = new test();
                objTest.testname = "Dipan2";

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objContext.SaveWithClientID2<test>(objTest, clientCode, true);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return 1;
        }



    }
}



