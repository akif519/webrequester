﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Extensions;
using System.Reflection;

namespace CMMS.Service
{
    public class WorkRequestService : DBExecute
    {

        #region "Client Wise Methods"

        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ChangePasswordModel ChangePassword(ChangePasswordModel model, string clientCode)
        {
            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            if (!obj.IsUserActive(model.EmployeeID, clientCode))
            {
                model.IsUserActive = 0;
                return model;
            }
            else
            {
                model.IsUserActive = 1;
            }

            //employee objEmployee = DbContext.employees.FirstOrDefault(e => e.EmployeeID == model.EmployeeID && e.Password == model.OldPassword.Trim());

            Employee objEmployee = EmployeeDetailService.GetEmployeeByEmployeeIdPassword(clientCode, model.EmployeeID, model.OldPassword.Trim()).FirstOrDefault();

            if (objEmployee != null)
            {
                string empID = Convert.ToString(model.EmployeeID);
                string Query = string.Empty;

                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    Query = "update employees set Password='" + model.NewPassword + "',ModifiedBy=" + empID + ",ModifiedDate=GETDATE() where EmployeeID=" + empID;

                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = "update employees set Password='" + model.NewPassword + "',ModifiedBy=" + empID + ",ModifiedDate=SysDate where EmployeeID=" + empID;
                }



                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objContext.ExecuteWithClientID(Query, new Collection<DBParameters>(), clientCode);
                }
                model.Success = 1;
                model.Message = ConstatntMessages.SuccessPasswordMessage;
            }
            else
            {
                model.Success = 0;
                model.Message = ConstatntMessages.WrongPasswordMessage;
            }
            return model;
        }

        /// <summary>
        /// To get Work Request Master for Basic Info
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestMasterResult GetWorkRequestMaster(string strCurrentTimeStamp, string clientCode)
        {

            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);

            GetWorkRequestMasterResult objWorkRequestMasterResult = new GetWorkRequestMasterResult();

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<L2> queryL2 = objContext.SearchAllWithClientID<L2>(new L2(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryL2.Count > 0)
                {
                    objWorkRequestMasterResult.L2Table = queryL2;
                }

                List<Worktype> queryworktype = objContext.SearchAllWithClientID<Worktype>(new Worktype(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryworktype.Count > 0)
                {
                    objWorkRequestMasterResult.WorktypeTable = queryworktype;
                }

                List<Workpriority> queryworkpriority = objContext.SearchAllWithClientID<Workpriority>(new Workpriority(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryworkpriority.Count > 0)
                {
                    objWorkRequestMasterResult.WorkpriorityTable = queryworkpriority;
                }

                List<Worktrade> queryworktrade = objContext.SearchAllWithClientID<Worktrade>(new Worktrade(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryworktrade.Count > 0)
                {
                    objWorkRequestMasterResult.WorktradeTable = queryworktrade;
                }

                List<Sector> querysector = objContext.SearchAllWithClientID<Sector>(new Sector(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (querysector != null)
                {
                    objWorkRequestMasterResult.SectorTable = querysector;
                }

                List<MainenanceDivision> queryMainenanceDivision = objContext.SearchAllWithClientID<MainenanceDivision>(new MainenanceDivision(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryMainenanceDivision.Count > 0)
                {
                    objWorkRequestMasterResult.MainenanceDivisionTable = queryMainenanceDivision;
                }

                List<MaintenanceDepartment> queryMaintenanceDepartment = objContext.SearchAllWithClientID<MaintenanceDepartment>(new MaintenanceDepartment(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryMaintenanceDepartment.Count > 0)
                {
                    objWorkRequestMasterResult.MaintenanceDepartmentTable = queryMaintenanceDepartment;
                }

                List<MaintSubDept> queryMaintSubDept = objContext.SearchAllWithClientID<MaintSubDept>(new MaintSubDept(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryMaintSubDept.Count > 0)
                {
                    objWorkRequestMasterResult.MaintSubDeptTable = queryMaintSubDept;
                }

                List<Workstatu> queryworkstatus = objContext.SearchAllWithClientID<Workstatu>(new Workstatu(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryworkstatus.Count > 0)
                {
                    objWorkRequestMasterResult.WorkStatusTable = queryworkstatus;
                }
            }
            return objWorkRequestMasterResult;
        }

        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestAssetInfoMasterResult GetWorkRequestAssetInfoMaster(string strCurrentTimeStamp, string clientCode, int employeeID)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestAssetInfoMasterResult objWorkRequestAssetInfoMasterResult = new GetWorkRequestAssetInfoMasterResult();

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<Employee> _queryemployee = objContext.SearchAllWithClientID<Employee>(new Employee(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (_queryemployee.Count > 0)
                {
                    List<_Employee> queryemployee = _queryemployee.Select(item => new _Employee()
                    {
                        Address = item.Address,
                        AltName = item.AltName,
                        CategoryID = item.CategoryID,
                        Central = item.Central,
                        Email = item.Email,
                        CreatedBy = item.CreatedBy,
                        CreatedDate = item.CreatedDate,
                        EmployeeID = item.EmployeeID,
                        EmployeeNO = item.EmployeeNO,
                        Fax = item.Fax,
                        HandPhone = item.HandPhone,
                        L2ID = item.L2ID,
                        UserGroupId = item.UserGroupId,
                        WorkPhone = item.WorkPhone,
                        Name = item.Name,
                        EmployeeStatusId = item.EmployeeStatusId,
                        HeartbeatValInSec = item.HeartbeatValInSec,
                        MaintSubDeptID = item.MaintSubDeptID,
                        maintDeptID = item.maintDeptID,
                        MaintDivisionID = item.MaintDivisionID
                    }).ToList();

                    objWorkRequestAssetInfoMasterResult.EmployeeTable = queryemployee;
                }

                //List<Asset> queryasset = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).Take(1000).ToList();
                //if (queryasset.Count > 0)
                //{
                //    objWorkRequestAssetInfoMasterResult.AssetTable = queryasset;
                //}

                //List<Location> querylocation = objContext.SearchAllWithClientID<Location>(new Location(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).Take(1000).ToList();
                //if (querylocation.Count > 0)
                //{
                //    objWorkRequestAssetInfoMasterResult.LocationTable = querylocation;
                //}

                List<L3> queryL3 = objContext.SearchAllWithClientID<L3>(new L3(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryL3.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.L3Table = queryL3;
                }

                List<L4> queryL4 = objContext.SearchAllWithClientID<L4>(new L4(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryL4.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.L4Table = queryL4;
                }

                List<L5> queryL5 = objContext.SearchAllWithClientID<L5>(new L5(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryL5.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.L5Table = queryL5;
                }

                List<EmployeeStatuse> queryemployeeStatus = objContext.SearchAllWithClientID<EmployeeStatuse>(new EmployeeStatuse(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryemployeeStatus.Count > 0)
                {

                    List<EmployeeStatuse> queryemployeeNewStatus = queryemployeeStatus.Select(item => new EmployeeStatuse()
                    {
                        CreatedBy = item.CreatedBy,
                        CreatedByName = item.CreatedByName,
                        CreatedDate = item.CreatedDate,
                        DisplayInModule = item.DisplayInModule,
                        EmployeeAltStatus = item.EmployeeAltStatus,
                        EmployeeStatus = item.EmployeeStatus,
                        EmployeeStatus1 = item.EmployeeStatus,
                        EmployeeStatusId = item.EmployeeStatusId,
                        ModifiedBy = item.ModifiedBy,
                        ModifiedByName = item.ModifiedByName,
                        ModifiedDate = item.ModifiedDate,
                        RowIndex = item.RowIndex,
                        TotalRecords = item.TotalRecords
                    }).ToList();


                    objWorkRequestAssetInfoMasterResult.EmployeeStatusTable = queryemployeeNewStatus;
                }

                //List<Employees_L2> queryemployees_L2 = objContext.SearchAllWithClientID<Employees_L2>(new Employees_L2(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).Take(1000).ToList();
                //if (queryemployees_L2.Count > 0)
                //{
                //    objWorkRequestAssetInfoMasterResult.Employee_L2Table = queryemployees_L2;
                //}

                Account objAccount = objContext.GetAccountDetailsByClientCode(clientCode);
                int clientID = objAccount.ClientID;

                //string strQueryFeatureTable = "Select * from Tbl_Feature Where clientID = @clientID";
                string strQueryFeatureTable = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebRequesterLicence)) AS 'WebRequesterLicence'FROM [dbo].[Tbl_Feature] WHERE ClientID = @ClientID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
                Tbl_Feature objTblFeature = objContext.ExecuteQueryWithClientID<Tbl_Feature>(strQueryFeatureTable, parameters, "").FirstOrDefault();

                if (objTblFeature != null)
                {
                    objWorkRequestAssetInfoMasterResult.Table_FeatureTable = new List<Tbl_Feature>();
                    objWorkRequestAssetInfoMasterResult.Table_FeatureTable.Add(objTblFeature);
                }


                //List<Tbl_Feature> queryTbl_Feature = objContext.SearchWithClientID<Tbl_Feature>(new Tbl_Feature(), 0, string.Empty, string.Empty, clientCode).ToList();
                //if (queryTbl_Feature.Count > 0)
                //{
                //    objWorkRequestAssetInfoMasterResult.Table_FeatureTable = queryTbl_Feature;
                //}

                //List<Translation> querytranslation = objContext.SearchAllWithClientID<Translation>(new Translation(), clientCode).Where(t => t.Labelname == "TextBlockCity" || t.Labelname == "TextBlockAreaCode" || t.Labelname == "TextBlockStreetCode" || t.Labelname == "TextBlockBuildingNo" || t.Labelname == "TextBlockLocationNo").Take(1000).ToList();
                //if (querytranslation.Count > 0)
                //{
                //    objWorkRequestAssetInfoMasterResult.TranslationTable = querytranslation;
                //}

                List<AssetStatus> queryassetstatus = objContext.SearchAllWithClientID<AssetStatus>(new AssetStatus(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (queryassetstatus.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.AssetStatusTable = queryassetstatus;
                }

                string strqueryEmployees_MaintSubDept = string.Empty;
                strqueryEmployees_MaintSubDept = strqueryEmployees_MaintSubDept + " select * from Employees_MaintSubDept WHERE EmpID=@EmpID ";
                Collection<DBParameters> parametersMaintSubDept = new Collection<DBParameters>();
                parametersMaintSubDept.Add(new DBParameters() { Name = "EmpID", Value = employeeID, DBType = DbType.Int32 });
                List<Employees_MaintSubDept> queryEmployees_MaintSubDept = objContext.ExecuteQueryWithClientID<Employees_MaintSubDept>(strqueryEmployees_MaintSubDept, parametersMaintSubDept, clientCode).ToList();
                //List<Employees_MaintSubDept> queryEmployees_MaintSubDept = objContext.SearchAllWithClientID<Employees_MaintSubDept>(new Employees_MaintSubDept(), clientCode).Where(x => (x.EmpId == employeeID)).ToList();
                if (queryEmployees_MaintSubDept.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.Employees_MaintSubDeptTable = queryEmployees_MaintSubDept;
                }
            }

            return objWorkRequestAssetInfoMasterResult;
        }


        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestAssetInfoMasteAssetTablerResult GetWorkRequestAssetInfoMasterAssetTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestAssetInfoMasteAssetTablerResult objWorkRequestAssetInfoMasterResult = new GetWorkRequestAssetInfoMasteAssetTablerResult();
            objWorkRequestAssetInfoMasterResult.PageIndex = pageIndex;
            objWorkRequestAssetInfoMasterResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {

                List<Asset> queryasset = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();

                objWorkRequestAssetInfoMasterResult.TotalCount = queryasset.Count;
                if (queryasset.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = (queryasset.Count / pageSize);
                    if (queryasset.Count % pageSize != 0) objWorkRequestAssetInfoMasterResult.TotalPages++;
                    queryasset = objContext.GetPageList<Asset>(queryasset, pageIndex, pageSize);
                }
                else
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = 0;
                    queryasset = new List<Asset>();
                }
                objWorkRequestAssetInfoMasterResult.AssetTable = queryasset;
            }

            return objWorkRequestAssetInfoMasterResult;
        }


        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestAssetInfoMasterEmployeesL2Result GetWorkRequestAssetInfoEmployeesL2Master(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestAssetInfoMasterEmployeesL2Result objWorkRequestAssetInfoMasterResult = new GetWorkRequestAssetInfoMasterEmployeesL2Result();
            objWorkRequestAssetInfoMasterResult.PageIndex = pageIndex;
            objWorkRequestAssetInfoMasterResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<Employees_L2> queryemployees_L2 = objContext.SearchAllWithClientID<Employees_L2>(new Employees_L2(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                objWorkRequestAssetInfoMasterResult.TotalCount = queryemployees_L2.Count;
                if (queryemployees_L2.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = (queryemployees_L2.Count / pageSize);
                    if (queryemployees_L2.Count % pageSize != 0) objWorkRequestAssetInfoMasterResult.TotalPages++;
                    queryemployees_L2 = objContext.GetPageList<Employees_L2>(queryemployees_L2, pageIndex, pageSize);
                }
                else
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = 0;
                    queryemployees_L2 = new List<Employees_L2>();
                }

                objWorkRequestAssetInfoMasterResult.Employee_L2Table = queryemployees_L2;
            }

            return objWorkRequestAssetInfoMasterResult;
        }


        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetEmployeesLocationResult GetEmployeesLocationMasterTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize, int? EmployeeId)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetEmployeesLocationResult objMasterResult = new GetEmployeesLocationResult();
            objMasterResult.PageIndex = pageIndex;
            objMasterResult.PageSize = pageSize;
            List<_Employees_Location> queryemployees_Location = new List<_Employees_Location>();

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<Employees_Location> _queryemployees_Location = objContext.SearchAllWithClientID<Employees_Location>(new Employees_Location(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                if (!string.IsNullOrWhiteSpace(Convert.ToString(EmployeeId)))
                {
                    _queryemployees_Location = _queryemployees_Location.Where(x => x.EmpID == EmployeeId).ToList();
                }


                if (_queryemployees_Location.Count > 0)
                {
                    queryemployees_Location = _queryemployees_Location.Select(item => new _Employees_Location()
                    {
                        LocationID = item.LocationID,
                        EmployeeID = item.EmpID,
                        CreatedBy = item.CreatedBy,
                        CreatedDate = item.CreatedDate,
                        ModifiedBy = item.ModifiedBy,
                        ModifiedDate = item.ModifiedDate
                    }).ToList();

                    objMasterResult.TotalCount = queryemployees_Location.Count;
                }

                if (queryemployees_Location.Count > 0)
                {
                    objMasterResult.TotalPages = (queryemployees_Location.Count / pageSize);
                    if (queryemployees_Location.Count % pageSize != 0) objMasterResult.TotalPages++;
                    queryemployees_Location = objContext.GetPageList<_Employees_Location>(queryemployees_Location, pageIndex, pageSize);
                }
                else
                {
                    objMasterResult.TotalPages = 0;
                    queryemployees_Location = new List<_Employees_Location>();
                }

                objMasterResult.Employee_LocationTable = queryemployees_Location;
            }

            return objMasterResult;
        }


        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestAssetInfoMasterLocationResult GetWorkRequestAssetInfoLocationTableMaster(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestAssetInfoMasterLocationResult objWorkRequestAssetInfoMasterResult = new GetWorkRequestAssetInfoMasterLocationResult();
            objWorkRequestAssetInfoMasterResult.PageIndex = pageIndex;
            objWorkRequestAssetInfoMasterResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {

                List<Location> querylocation = objContext.SearchAllWithClientID<Location>(new Location(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                objWorkRequestAssetInfoMasterResult.TotalCount = querylocation.Count;
                if (querylocation.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = (querylocation.Count / pageSize);
                    if (querylocation.Count % pageSize != 0) objWorkRequestAssetInfoMasterResult.TotalPages++;
                    querylocation = objContext.GetPageList<Location>(querylocation, pageIndex, pageSize);
                }
                else
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = 0;
                    querylocation = new List<Location>();
                }
                objWorkRequestAssetInfoMasterResult.LocationTable = querylocation;
            }
            return objWorkRequestAssetInfoMasterResult;
        }



        /// <summary>
        /// Get Work Request Asset Info Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkRequestAssetInfoMasterTranslationTableResult GetWorkRequestAssetInfoTranslationTableMaster(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestAssetInfoMasterTranslationTableResult objWorkRequestAssetInfoMasterResult = new GetWorkRequestAssetInfoMasterTranslationTableResult();
            objWorkRequestAssetInfoMasterResult.PageIndex = pageIndex;
            objWorkRequestAssetInfoMasterResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {

                List<Translation> querytranslation = objContext.SearchAllWithClientID<Translation>(new Translation(), clientCode).Where(t => t.Labelname == "TextBlockCity" || t.Labelname == "TextBlockAreaCode" || t.Labelname == "TextBlockStreetCode" || t.Labelname == "TextBlockBuildingNo" || t.Labelname == "TextBlockLocationNo").ToList();


                Account objAccount = objContext.GetAccountDetailsByClientCode(clientCode);
                FirstLevelTbl FirstLevelTbl = objContext.GetFirstLevelTbl(objAccount.ClientID);
                LevelNamesTbl LevelNamesTbl = new LevelNamesTbl();
                if (FirstLevelTbl != null)
                {
                    LevelNamesTbl = objContext.GetLevelNamesTbl(FirstLevelTbl.L1_ID);
                }

                foreach (Translation item in querytranslation)
                {
                    item.FromWord = item.FromWord.Replace("[L1_en]", FirstLevelTbl.L1_en).Replace("[L2_en]", LevelNamesTbl.L2_en).Replace("[L3_en]", LevelNamesTbl.L3_en).Replace("[L4_en]", LevelNamesTbl.L4_en).Replace("[L5_en]", LevelNamesTbl.L5_en);
                    item.FromWord = item.FromWord.Replace("[L1_ar]", FirstLevelTbl.L1_ar).Replace("[L2_ar]", LevelNamesTbl.L2_ar).Replace("[L3_ar]", LevelNamesTbl.L3_ar).Replace("[L4_ar]", LevelNamesTbl.L4_ar).Replace("[L5_ar]", LevelNamesTbl.L5_ar);
                }

                objWorkRequestAssetInfoMasterResult.TotalCount = querytranslation.Count;

                if (querytranslation.Count > 0)
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = (querytranslation.Count / pageSize);
                    if (querytranslation.Count % pageSize != 0) objWorkRequestAssetInfoMasterResult.TotalPages++;
                    querytranslation = objContext.GetPageList<Translation>(querytranslation, pageIndex, pageSize);
                }
                else
                {
                    objWorkRequestAssetInfoMasterResult.TotalPages = 0;
                    querytranslation = new List<Translation>();
                }
                objWorkRequestAssetInfoMasterResult.TranslationTable = querytranslation;
            }
            return objWorkRequestAssetInfoMasterResult;
        }



        /// <summary>
        /// Get Existing Work Request MasterIDs for Basic Info
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkRequestMasterIDsResult GetExistingWorkRequestMasterIDs(string clientCode)
        {
            GetExistingWorkRequestMasterIDsResult objExistingWorkRequestMasterIDsResult = new GetExistingWorkRequestMasterIDsResult();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                objExistingWorkRequestMasterIDsResult.lstL2ID = objContext.SearchAllWithClientID<L2>(new L2(), clientCode).Select(s => s.L2ID).ToList();
                objExistingWorkRequestMasterIDsResult.lstWorkTypeID = objContext.SearchAllWithClientID<Worktype>(new Worktype(), clientCode).Select(s => s.WorkTypeID).ToList();
                objExistingWorkRequestMasterIDsResult.lstWorkPriorityID = objContext.SearchAllWithClientID<Workpriority>(new Workpriority(), clientCode).Select(s => s.WorkPriorityID).ToList();
                objExistingWorkRequestMasterIDsResult.lstWorkTradeID = objContext.SearchAllWithClientID<Worktrade>(new Worktrade(), clientCode).Select(s => s.WorkTradeID).ToList();
                objExistingWorkRequestMasterIDsResult.lstSectorID = objContext.SearchAllWithClientID<Sector>(new Sector(), clientCode).Select(s => s.SectorID).ToList();
                objExistingWorkRequestMasterIDsResult.lstMaintDivisionID = objContext.SearchAllWithClientID<MainenanceDivision>(new MainenanceDivision(), clientCode).Select(s => s.MaintDivisionID).ToList();
                objExistingWorkRequestMasterIDsResult.lstMaintDeptID = objContext.SearchAllWithClientID<MaintenanceDepartment>(new MaintenanceDepartment(), clientCode).Select(s => s.MaintDeptID).ToList();
                objExistingWorkRequestMasterIDsResult.lstMaintSubDeptID = objContext.SearchAllWithClientID<MaintSubDept>(new MaintSubDept(), clientCode).Select(s => s.MaintSubDeptID).ToList();
            }
            return objExistingWorkRequestMasterIDsResult;
        }

        /// <summary>
        /// Get Existing Work Request Asset Info MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkRequestAssetInfoMasterIDsResult GetExistingWorkRequestAssetInfoMasterIDs(int employeeID, string clientCode)
        {
            GetExistingWorkRequestAssetInfoMasterIDsResult objExistingWorkRequestAssetInfoMasterIDsResult = new GetExistingWorkRequestAssetInfoMasterIDsResult();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                objExistingWorkRequestAssetInfoMasterIDsResult.lstEmployeeID = objContext.SearchAllWithClientID<Employee>(new Employee(), clientCode).Select(s => s.EmployeeID).ToList();

                //  objExistingWorkRequestAssetInfoMasterIDsResult.lstAssetID = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Select(s => s.AssetID).ToList();

                //                objExistingWorkRequestAssetInfoMasterIDsResult.lstLocationID = objContext.SearchAllWithClientID<Location>(new Location(), clientCode).Select(s => s.LocationID).ToList();

                objExistingWorkRequestAssetInfoMasterIDsResult.lstL3ID = objContext.SearchAllWithClientID<L3>(new L3(), clientCode).Select(s => s.L3ID).ToList();

                objExistingWorkRequestAssetInfoMasterIDsResult.lstL4ID = objContext.SearchAllWithClientID<L4>(new L4(), clientCode).Select(s => s.L4ID).ToList();

                objExistingWorkRequestAssetInfoMasterIDsResult.lstL5ID = objContext.SearchAllWithClientID<L5>(new L5(), clientCode).Select(s => s.L5ID).ToList();

                objExistingWorkRequestAssetInfoMasterIDsResult.lstEmployeeStatusId = objContext.SearchAllWithClientID<EmployeeStatuse>(new EmployeeStatuse(), clientCode).Select(s => s.EmployeeStatusId).ToList();

                objExistingWorkRequestAssetInfoMasterIDsResult.lstAssetStatusID = objContext.SearchAllWithClientID<AssetStatus>(new AssetStatus(), clientCode).Select(s => s.AssetStatusID).ToList();

                //List<Employees_MaintSubDept> queryEmployees_MaintSubDept = objContext.SearchAllWithClientID<Employees_MaintSubDept>(new Employees_MaintSubDept(), clientCode).ToList();
                //string strqueryEmployees_MaintSubDept = string.Empty;
                //strqueryEmployees_MaintSubDept = strqueryEmployees_MaintSubDept + " select * from Employees_MaintSubDept WHERE EmpID=@EmpID ";
                //Collection<DBParameters> parametersMaintSubDept = new Collection<DBParameters>();
                //parametersMaintSubDept.Add(new DBParameters() { Name = "EmpID", Value = employeeID, DBType = DbType.Int32 });
                //List<Employees_MaintSubDept> queryEmployees_MaintSubDept = objContext.ExecuteQueryWithClientID<Employees_MaintSubDept>(strqueryEmployees_MaintSubDept, parametersMaintSubDept, clientCode).ToList();
                //if (queryEmployees_MaintSubDept.Count > 0)
                //{
                //    objExistingWorkRequestAssetInfoMasterIDsResult.Employees_MaintSubDeptTable = queryEmployees_MaintSubDept;
                //}

                //List<Employees_L2> queryemployees_L2 = objContext.SearchAllWithClientID<Employees_L2>(new Employees_L2(), clientCode).ToList();
                //if (queryemployees_L2.Count > 0)
                //{
                //    objExistingWorkRequestAssetInfoMasterIDsResult.Employee_L2Table = queryemployees_L2;
                //}
            }
            return objExistingWorkRequestAssetInfoMasterIDsResult;
        }


        /// <summary>
        /// Get Existing Work Request Asset Info MasterIDs EmployeeL2Table
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2Table(string clientCode, int pageIndex, int pageSize)
        {
            GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult objExistingWorkRequestAssetInfoMasterIDsResult = new GetExistingWorkRequestAssetInfoMasterIDsEmployeeL2TableResult();
            objExistingWorkRequestAssetInfoMasterIDsResult.PageIndex = pageIndex;
            objExistingWorkRequestAssetInfoMasterIDsResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<Employees_L2> queryemployees_L2 = objContext.SearchAllWithClientID<Employees_L2>(new Employees_L2(), clientCode).ToList();
                objExistingWorkRequestAssetInfoMasterIDsResult.TotalCount = queryemployees_L2.Count;
                if (queryemployees_L2.Count > 0)
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = (queryemployees_L2.Count / pageSize);
                    if (queryemployees_L2.Count % pageSize != 0) objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages++;
                    queryemployees_L2 = objContext.GetPageList<Employees_L2>(queryemployees_L2, pageIndex, pageSize);
                }
                else
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = 0;
                    queryemployees_L2 = new List<Employees_L2>();
                }
                objExistingWorkRequestAssetInfoMasterIDsResult.Employee_L2Table = queryemployees_L2;
            }
            return objExistingWorkRequestAssetInfoMasterIDsResult;
        }

        /// <summary>
        /// Get Existing Work Request Asset Info MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult GetExistingWorkRequestAssetInfoMasterIDsLstAssetID(string clientCode, int pageIndex, int pageSize)
        {
            GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult objExistingWorkRequestAssetInfoMasterIDsResult = new GetExistingWorkRequestAssetInfoMasterIDsLstAssetIDResult();
            objExistingWorkRequestAssetInfoMasterIDsResult.PageIndex = pageIndex;
            objExistingWorkRequestAssetInfoMasterIDsResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<int> lst = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Select(s => s.AssetID).ToList();
                objExistingWorkRequestAssetInfoMasterIDsResult.TotalCount = lst.Count;

                if (lst.Count > 0)
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = (lst.Count / pageSize);
                    if (lst.Count % pageSize != 0) objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages++;
                    lst = objContext.GetPageList<int>(lst, pageIndex, pageSize);
                }
                else
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = 0;
                    lst = new List<int>();
                }
                objExistingWorkRequestAssetInfoMasterIDsResult.lstAssetID = lst;
            }
            return objExistingWorkRequestAssetInfoMasterIDsResult;
        }


        /// <summary>
        /// Get Existing Work Request Asset Info MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult GetExistingWorkRequestAssetInfoMasterIDsLstLocationID(string clientCode, int pageIndex, int pageSize)
        {
            GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult objExistingWorkRequestAssetInfoMasterIDsResult = new GetExistingWorkRequestAssetInfoMasterIDsLstLocationIDResult();
            objExistingWorkRequestAssetInfoMasterIDsResult.PageIndex = pageIndex;
            objExistingWorkRequestAssetInfoMasterIDsResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<int> lst = objContext.SearchAllWithClientID<Location>(new Location(), clientCode).Select(s => s.LocationID).ToList();
                objExistingWorkRequestAssetInfoMasterIDsResult.TotalCount = lst.Count;

                if (lst.Count > 0)
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = (lst.Count / pageSize);
                    if (lst.Count % pageSize != 0) objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages++;
                    lst = objContext.GetPageList<int>(lst, pageIndex, pageSize);
                }
                else
                {
                    objExistingWorkRequestAssetInfoMasterIDsResult.TotalPages = 0;
                    lst = new List<int>();
                }
                objExistingWorkRequestAssetInfoMasterIDsResult.lstLocationID = lst;
            }
            return objExistingWorkRequestAssetInfoMasterIDsResult;
        }




        /// <summary>
        /// Get List Of Work RequestList with Assosiated Attachment (Including Paging)
        /// </summary>
        /// <param name="IsCentral"></param>
        /// <param name="EmployeeID"></param>
        /// <param name="RequestStatusIds"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public GetWorkRequestListResult GetWorkRequestList(bool IsCentral, int EmployeeID, string RequestStatusIds, int PageIndex, int PageSize, string RequestNoToRefresh, string strCurrentTimeStamp, string clientCode)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkRequestListResult objWorkRequestListResult = new GetWorkRequestListResult();
            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            if (!obj.IsUserActive(EmployeeID, clientCode))
            {
                objWorkRequestListResult.IsUserActive = 0;
                return objWorkRequestListResult;
            }
            else
            {
                objWorkRequestListResult.IsUserActive = 1;
            }



            string strWhere;
            if (RequestNoToRefresh != null && !string.IsNullOrEmpty(RequestNoToRefresh))
            {
                //strWhere = "worequest.RequestNo = '" + RequestNoToRefresh + "'and";
                strWhere = "AND worequest.RequestNo = '" + RequestNoToRefresh + "'"; ;
            }
            else
            {
                strWhere = string.Empty;
            }


            StringBuilder sqlSelectQuery = new StringBuilder();

            Account objAccount = GetAccountType(clientCode);
            int clientID = objAccount.ClientID;
            Tbl_Feature objTblFeature = new Tbl_Feature();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                string strQueryFeatureTable = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebRequesterLicence)) AS 'WebRequesterLicence'FROM [dbo].[Tbl_Feature] WHERE ClientID = @ClientID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
                objTblFeature = objContext.ExecuteQueryWithClientID<Tbl_Feature>(strQueryFeatureTable, parameters, "").FirstOrDefault();
            }


            if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
            {
                sqlSelectQuery.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));

            }
            else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {

            }


            //sqlSelectQuery.Append(" Select * from worequest ");
            sqlSelectQuery.Append(" SELECT worequest.*,w2.ProjectNumber,w2.WorkStatusID FROM worequest  LEFT JOIN workorders w2 ON w2.WorkorderNo = worequest.WorkorderNo ");
            sqlSelectQuery.AppendFormat(" where 1=1 {0}", strWhere);
            if (currentTimeStamp == null)
            {
                sqlSelectQuery.AppendFormat(" AND worequest.RequestStatusID IN(1,8) ");
            }
            employees objemployees = new employees();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                objemployees.EmployeeID = EmployeeID;
                objemployees = objContext.SearchAllWithClientID<employees>(objemployees, clientCode).FirstOrDefault();
            }
            PermissionAccess objPermissionAccess = SetEmployeePermission(objemployees, clientCode, objAccount.DbType);

            if (currentTimeStamp != null)
            {

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.Append(" AND ( (worequest.CreatedDate >= @currentTimeStamp OR worequest.ModifiedDate >= @currentTimeStamp)");

                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" and ( worequest.LocationID in  (select LocationID from Employees_Location where empID = {0}) ", EmployeeID);
                        sqlSelectQuery.AppendFormat(" or worequest.LocationID in  (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp)) ) ", EmployeeID);
                    }

                    sqlSelectQuery.AppendFormat(" 	    Or  ( worequest.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");


                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);

                    sqlSelectQuery.Append(" AND ( (worequest.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR worequest.ModifiedDate >= TO_DATE('" + DBExecute.GetEnglishDateWithTime(currentTimeStamp) + "', 'dd/mon/yyyy hh24:mi:ss'))");

                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" and ( worequest.LocationID in  (select LocationID from Employees_Location where empID = {0}) ", EmployeeID);
                        sqlSelectQuery.AppendFormat(" or worequest.LocationID in  (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ) ) ", EmployeeID);
                    }

                    sqlSelectQuery.AppendFormat(" 	    Or  ( worequest.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }
            }
            if (!IsCentral)
            {
                sqlSelectQuery.AppendFormat(" 	and ( worequest.L2ID in (select L2Id from employees_L2 where empID = {0} union Select L2Id from employees where employeeId = {0} )	", EmployeeID);

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.AppendFormat(" 	    Or  ( worequest.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");

                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);

                    sqlSelectQuery.AppendFormat(" 	    Or  ( worequest.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", EmployeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }

                sqlSelectQuery.AppendFormat(" 	and worequest.MainSubDeptId in (select MaintSubDeptID from Employees_MaintSubDept where empID = {0} union Select MaintSubDeptID from employees where employeeId = {0} )	", EmployeeID);

                if (objTblFeature != null)
                {
                    if (objTblFeature.Area)
                        sqlSelectQuery.AppendFormat(" and (worequest.L3ID in (select L3Id from Employees_L3 where empID = {0}) OR (worequest.L3ID IS NULL)) ", EmployeeID);
                }

                if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    sqlSelectQuery.AppendFormat(" and (worequest.LocationID in  (select LocationID from Employees_Location where empID = {0}) ", EmployeeID);

                    if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( worequest.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp)   )) ) ", EmployeeID);
                    }
                    else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                        sqlSelectQuery.AppendFormat(" 	        Or ( worequest.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')))  ) ) ", EmployeeID);
                    }
                }
            }

            sqlSelectQuery.Append(" order by worequest.CreatedDate desc ");

            List<Worequest> resultWorkRequest = new List<Worequest>(); ;
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    resultWorkRequest = objContext.ExecuteQueryWithClientID<Worequest>(sqlSelectQuery.ToString(), parameters, clientCode).ToList();
                    objWorkRequestListResult.TotalCount = resultWorkRequest.Count();

                    if (resultWorkRequest.Count > 0)
                    {
                        objWorkRequestListResult.TotalPages = (resultWorkRequest.Count / PageSize);
                        if (resultWorkRequest.Count % PageSize != 0) objWorkRequestListResult.TotalPages++;
                        resultWorkRequest = objContext.GetPageList<Worequest>(resultWorkRequest, PageIndex, PageSize);
                    }
                    else
                    {
                        objWorkRequestListResult.TotalPages = 0;
                        resultWorkRequest = new List<Worequest>();
                    }
                    objWorkRequestListResult.WorkRequestTable = resultWorkRequest;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            objWorkRequestListResult.PageIndex = PageIndex;
            objWorkRequestListResult.PageSize = PageSize;
            objWorkRequestListResult.SynchTime = currentTimeStamp.ToString();
            objWorkRequestListResult.EmployeeID = EmployeeID;
            //objWorkRequestListResult.RequestStatusIds = RequestStatusIds;
            objWorkRequestListResult.IsCentral = IsCentral;
            objWorkRequestListResult.RequestNoToRefresh = RequestNoToRefresh;

            //Set Assosiated Attachments in WorkRequest List
            if (objWorkRequestListResult.WorkRequestTable.Count > 0)
            {
                List<string> workRequestNo = objWorkRequestListResult.WorkRequestTable.Select(w => w.RequestNo).ToList();
                string commaSepratedworkRequestNo = string.Join("','", workRequestNo);

                string strfileAttachedForWRQuery = "SELECT * FROM extassetfiles E WHERE E.FkId IN ( '" + commaSepratedworkRequestNo + "' ) AND E.ModuleType = 'R' ";
                List<ExtAssetFile> fileAttachedForWR = new List<ExtAssetFile>();
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        fileAttachedForWR = objContext.ExecuteQueryWithClientID<ExtAssetFile>(strfileAttachedForWRQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objWorkRequestListResult.WorkRequestTable.ForEach(w =>
                {
                    w.ExtassetfileTable = new List<ExtAssetFile>();
                });


                if (fileAttachedForWR.Count > 0)
                {
                    objWorkRequestListResult.WorkRequestTable.ForEach(w =>
                    {

                        if (fileAttachedForWR.Any(attachment => attachment.FkId == w.RequestNo))
                        {
                            w.ExtassetfileTable = fileAttachedForWR.Where(att => att.FkId == w.RequestNo).ToList();
                        }
                    });
                }
            }

            return objWorkRequestListResult;
        }

        /// <summary>
        /// Add/Update Work request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public AddUpdateWorkRequestResult AddUpdateWorkRequest(Worequest model, string attachmentPath, string clientCode)
        {            

            try
            {

                Account objAC = EmployeeDetailService.GetAccountType(clientCode);
                attachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\";

                AddUpdateWorkRequestResult objAddUpdateWorkRequestResult = new AddUpdateWorkRequestResult();
                Worequest objWorkRequest = new Worequest();

                //Check For User is Active or Not
                EmployeeDetailService obj = new EmployeeDetailService();
                if (!obj.IsUserActive(model.EmployeeID, clientCode))
                {
                    objAddUpdateWorkRequestResult.IsUserActive = 0;
                    return objAddUpdateWorkRequestResult;
                }
                else
                {
                    objAddUpdateWorkRequestResult.IsUserActive = 1;
                }
                //Check For WorkRequest Status Close/Cancel
                //var WorkRequestStatus = DbContext.worequests.Where(w => w.RequestNo == model.RequestNo).Select(s => s.RequestStatusID).FirstOrDefault();
                int? WorkRequestStatus;
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    WorkRequestStatus = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.RequestNo == model.RequestNo).Select(s => s.RequestStatusID).FirstOrDefault();
                }

                if (WorkRequestStatus != null && (WorkRequestStatus == 2 || WorkRequestStatus == 3))
                {
                    objAddUpdateWorkRequestResult.IsStatusCloseAtServer = 1;
                    return objAddUpdateWorkRequestResult;
                }

                //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
                //{

                #region For Update WR

                if (!string.IsNullOrEmpty(model.RequestNo))
                {

                    Worequest objNewWorequestnew = new Worequest
                    {
                        RequestNo = model.RequestNo,
                        L2ID = model.L2ID,
                        L3ID = model.L3ID,
                        L4ID = model.L4ID,
                        LocationID = model.LocationID,
                        AssetID = model.AssetID,
                        WorkorderNo = model.WorkorderNo,
                        Worktradeid = model.Worktradeid,
                        WorkTypeID = model.WorkTypeID,
                        Workpriorityid = model.Workpriorityid,
                        CancelledByID = model.CancelledByID,
                        RequesterID = model.RequesterID,
                        CreatedByID = model.CreatedByID,
                        RequestStatusID = model.RequestStatusID,
                        MaintdeptId = model.MaintdeptId,
                        MainSubDeptId = model.MainSubDeptId,
                        MaintdivId = model.MaintdivId,
                        ProblemDesc = model.ProblemDesc,
                        ReceivedDate = model.ReceivedDate,
                        RequiredDate = model.RequiredDate,
                        Remarks = model.Remarks,
                        //CreatedBy = model.CreatedBy,
                        //CreatedDate = model.CreatedDate,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedDate = System.DateTime.Now,
                        L5ID = model.L5ID,
                        AuthorisedEmployeeId = model.AuthorisedEmployeeId,
                        ExternalRequesterName = model.ExternalRequesterName
                    };




                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.UpdateWithClientID<Worequest>(objNewWorequestnew, "  RequestNo = '" + model.RequestNo + "'", clientCode);
                    }
                    //DbContext.worequests.Where(w => w.RequestNo == model.RequestNo).Update(s => 

                    //DbContext.SaveChanges();

                    objAddUpdateWorkRequestResult.Message = ConstatntMessages.SuccessUpdateWRMessage;
                }

                #endregion

                #region For Add WR

                else
                {

                    // Create RequestNo For New WR

                    Int64 MinRequestNo = Convert.ToInt64(DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));
                    List<Worequest> TempListWR = new List<Worequest>();

                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        TempListWR = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(x => x.L2ID == model.L2ID).Select(s => s).ToList();

                    }

                    Int64 requestNoWithoutCityAndR = 0;
                    foreach (var item in TempListWR)
                    {
                        requestNoWithoutCityAndR = Convert.ToInt64(item.RequestNo.Substring(item.RequestNo.IndexOf("R") + 1, (item.RequestNo.Length - (item.RequestNo.IndexOf("R") + 1))));
                        if (Convert.ToInt64(requestNoWithoutCityAndR) > MinRequestNo)
                        {
                            MinRequestNo = requestNoWithoutCityAndR;
                        }
                    }

                    // Create RequestNo For New WR

                    objWorkRequest.RequestNo = model.L2ID.ToString().PadLeft(3, '0') + "R" + (MinRequestNo + 1).ToString().PadLeft(8, '0');
                    objWorkRequest.L2ID = model.L2ID;
                    objWorkRequest.L3ID = model.L3ID;
                    objWorkRequest.L4ID = model.L4ID;
                    objWorkRequest.LocationID = model.LocationID;
                    objWorkRequest.AssetID = model.AssetID;
                    objWorkRequest.WorkorderNo = model.WorkorderNo;
                    objWorkRequest.Worktradeid = model.Worktradeid;
                    objWorkRequest.WorkTypeID = model.WorkTypeID;
                    objWorkRequest.Workpriorityid = model.Workpriorityid;
                    objWorkRequest.CancelledByID = model.CancelledByID;
                    objWorkRequest.RequesterID = model.RequesterID;
                    objWorkRequest.CreatedByID = model.CreatedByID;
                    objWorkRequest.RequestStatusID = model.RequestStatusID;
                    objWorkRequest.MaintdeptId = model.MaintdeptId;
                    objWorkRequest.MainSubDeptId = model.MainSubDeptId;
                    objWorkRequest.MaintdivId = model.MaintdivId;
                    objWorkRequest.ProblemDesc = model.ProblemDesc;
                    objWorkRequest.ReceivedDate = model.ReceivedDate;
                    objWorkRequest.RequiredDate = model.RequiredDate;
                    objWorkRequest.Remarks = model.Remarks;
                    objWorkRequest.CreatedBy = model.CreatedBy;
                    objWorkRequest.CreatedDate = System.DateTime.Now;
                    //objWorkRequest.ModifiedBy = model.ModifiedBy;
                    //objWorkRequest.ModifiedDate = model.ModifiedDate;
                    objWorkRequest.L5ID = model.L5ID;
                    objWorkRequest.AuthorisedEmployeeId = model.AuthorisedEmployeeId;
                    objWorkRequest.ExternalRequesterName = model.ExternalRequesterName;

                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.SaveWithClientID<Worequest>(objWorkRequest, clientCode, true);
                    }
                    //DbContext.worequests.Add(objWorkRequest);
                    //DbContext.SaveChanges();
                    model.RequestNo = objWorkRequest.RequestNo;

                    #region Insert into WRStatusAudit Table when WR is created
                    if (!string.IsNullOrEmpty(model.RequestNo))
                    {
                        var wrStatusAuditObj = new WRStatusAudit();
                        wrStatusAuditObj.WRNo = model.RequestNo;

                        wrStatusAuditObj.WRStatus = model.RequestStatusID;
                        wrStatusAuditObj.Date = DateTime.Now;
                        wrStatusAuditObj.EmpID = Convert.ToInt32(model.CreatedBy);
                        wrStatusAuditObj.source = model.Source;

                        wrStatusAuditObj.Latitude = model.Latitude;
                        wrStatusAuditObj.Longitude = model.Longitude;

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.SaveWithClientID<WRStatusAudit>(wrStatusAuditObj, clientCode, true);
                        }

                        //DbContext.WRStatusAudits.Add(wrStatusAuditObj);
                        //DbContext.SaveChanges();
                    }
                    #endregion

                    objAddUpdateWorkRequestResult.Message = ConstatntMessages.SuccessAddWRMessage;
                }

                #endregion

                #region For Add/Delete Attachment

                if (model.ExtassetfileTable != null)
                {

                    //For Delete Attachment
                    List<ExtAssetFile> lstExtAssetFileDelete = model.ExtassetfileTable.Where(att => att.FlagForAddUpdateDelete == 3).ToList();

                    foreach (var item in lstExtAssetFileDelete)
                    {
                        DeleteAttachment(attachmentPath, item, clientCode);
                    }
                }

                objAddUpdateWorkRequestResult.WorkRequestTable = model;
                objAddUpdateWorkRequestResult.Success = 1;

                #endregion

                //    scope.Complete();
                //}

                return objAddUpdateWorkRequestResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        /// <summary>
        /// Delete Attachment By FkId and ModuleType
        /// </summary>
        /// <param name="FkId"></param>
        /// <param name="ModuleType"></param>
        public void DeleteAttachment(string attachmentPath, ExtAssetFile model, string clientCode)
        {
            int deleteFromPhysicalFolder = DeleteAttachmentFile(attachmentPath, model.FileLink);
            if (deleteFromPhysicalFolder > 0)
            {

                string strextassetfilesForWRQuery = "delete FROM extassetfiles WHERE AutoId = " + model.AutoId + "";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.ExecuteQueryWithClientID<ExtAssetFile>(strextassetfilesForWRQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //DbContext.extassetfiles.Delete(e => e.AutoId == model.AutoId);
                //DbContext.SaveChanges();
                model.FileSaveMessage = ConstatntMessages.FileSuccessfullyDeleteMessage;
            }
            else if (deleteFromPhysicalFolder == -1)
            {

                string strextassetfilesForWRQuery = "delete FROM extassetfiles WHERE AutoId = " + model.AutoId + "";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.ExecuteQueryWithClientID<ExtAssetFile>(strextassetfilesForWRQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //DbContext.extassetfiles.Delete(e => e.AutoId == model.AutoId);
                //DbContext.SaveChanges();
                model.FileSaveMessage = ConstatntMessages.FileSuccessfullyDeleteMessage;
                //model.FileSaveMessage = ConstatntMessages.FileDeleteNotExistMessage;
            }
            else
            {
                model.FileSaveMessage = ConstatntMessages.ErrorMessage;
            }

        }

        /// <summary>
        /// Delete Attachment From Physical Folder
        /// </summary>
        /// <param name="attachmentPath"></param>
        /// <param name="FileLink"></param>
        public int DeleteAttachmentFile(string attachmentPath, string FileLink)
        {
            string pathTodeleteFileFrom = attachmentPath + FileLink;
            try
            {
                if (File.Exists(pathTodeleteFileFrom))
                {
                    File.Delete(pathTodeleteFileFrom);
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception)
            {

                return 0;
            }
        }

        /// <summary>
        /// For Uploading file
        /// </summary>
        /// <param name="model"></param>
        /// <param name="attachmentPath"></param>
        /// <returns></returns>
        public AddAttachmentResult AddFileAttachment(List<ExtAssetFile> model, string attachmentPath, int EmployeeID, string clientCode)
        {
            AddAttachmentResult objAddAttachmentResult = new AddAttachmentResult();

            Account objAC = EmployeeDetailService.GetAccountType(clientCode);
            attachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\";

            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            if (!obj.IsUserActive(EmployeeID, clientCode))
            {
                objAddAttachmentResult.IsUserActive = 0;
                return objAddAttachmentResult;
            }
            else
            {
                objAddAttachmentResult.IsUserActive = 1;
            }
            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            if (model != null)
            {
                //For Add Attachment
                List<ExtAssetFile> lstExtAssetFileAdd = model.Where(att => att.FlagForAddUpdateDelete == 1).ToList();
                foreach (var item in model)
                {
                    //Check If WorkRequest/WorkOrder/Asset is not closed/Canceled   
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkRequest))
                    {
                        int? WorkRequestStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            WorkRequestStatus = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.RequestNo == item.FkId).Select(s => s.RequestStatusID).FirstOrDefault();
                        }

                        if (WorkRequestStatus != null && (WorkRequestStatus == 2 || WorkRequestStatus == 3))
                        {
                            objAddAttachmentResult.IsStatusCloseAtServer = 1;
                            return objAddAttachmentResult;
                        }
                    }
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkOrder))
                    {
                        int? WorkOrderStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            WorkOrderStatus = objContext.SearchAllWithClientID<WorkOrder>(new WorkOrder(), clientCode).Where(w => w.WorkorderNo == item.FkId).Select(s => s.WorkStatusID).FirstOrDefault();
                        }

                        //var WorkOrderStatus = DbContext.workorders.Where(w => w.WorkorderNo == item.FkId).Select(s => s.WorkStatusID).FirstOrDefault();
                        if (WorkOrderStatus != null && (WorkOrderStatus == 2 || WorkOrderStatus == 3))
                        {
                            objAddAttachmentResult.IsStatusCloseAtServer = 1;
                            return objAddAttachmentResult;
                        }
                    }
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.Asset))
                    {
                        int assetId = Convert.ToInt32(item.FkId);
                        //var AssetStatus = DbContext.assets.Where(w => w.AssetID == assetId).Select(s => s.AssetStatusID).FirstOrDefault();
                        int? AssetStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            AssetStatus = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Where(w => w.AssetID == assetId).Select(s => s.AssetStatusID).FirstOrDefault();
                        }

                        if (AssetStatus != null && (AssetStatus == 2 || AssetStatus == 3))
                        {
                            objAddAttachmentResult.IsStatusCloseAtServer = 1;
                            return objAddAttachmentResult;
                        }
                    }
                    //Check If WorkRequest/WorkOrder/Asset is not closed/Canceled/Completed   

                    AddAttachment(lstExtAssetFileAdd, item.FkId, attachmentPath, clientCode);
                    //set null to base-64 response to avoid load
                    item.strFileToAttach = null;
                }

            }
            //   scope.Complete();
            objAddAttachmentResult.ExtassetfileTable = model;
            objAddAttachmentResult.Success = 1;
            //}
            return objAddAttachmentResult;
        }

        /// <summary>
        /// For Deleting File
        /// </summary>
        /// <param name="model"></param>
        /// <param name="attachmentPath"></param>
        /// <returns></returns>
        public DeleteAttachmentResult DeleteFileAttachment(List<ExtAssetFile> model, string attachmentPath, int EmployeeID, string clientCode)
        {
            DeleteAttachmentResult objDeleteAttachmentResult = new DeleteAttachmentResult();
            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            Account objAC = EmployeeDetailService.GetAccountType(clientCode);
            attachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\";

            if (!obj.IsUserActive(EmployeeID, clientCode))
            {
                objDeleteAttachmentResult.IsUserActive = 0;
                return objDeleteAttachmentResult;
            }
            else
            {
                objDeleteAttachmentResult.IsUserActive = 1;
            }
            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            if (model != null)
            {
                //For Delete Attachment
                List<ExtAssetFile> lstExtAssetFileDelete = model.Where(att => att.FlagForAddUpdateDelete == 3).ToList();
                foreach (var item in lstExtAssetFileDelete)
                {
                    //Check If WorkRequest/WorkOrder/Asset is not closed/Canceled  
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkRequest))
                    {

                        int? WorkRequestStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            WorkRequestStatus = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.RequestNo == item.FkId).Select(s => s.RequestStatusID).FirstOrDefault();
                        }

                        if (WorkRequestStatus != null && (WorkRequestStatus == 2 || WorkRequestStatus == 3))
                        {
                            objDeleteAttachmentResult.IsStatusCloseAtServer = 1;
                            return objDeleteAttachmentResult;
                        }
                    }
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkOrder))
                    {
                        int? WorkOrderStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            WorkOrderStatus = objContext.SearchAllWithClientID<WorkOrder>(new WorkOrder(), clientCode).Where(w => w.WorkorderNo == item.FkId).Select(s => s.WorkStatusID).FirstOrDefault();
                        }

                        if (WorkOrderStatus != null && (WorkOrderStatus == 2 || WorkOrderStatus == 3))
                        {
                            objDeleteAttachmentResult.IsStatusCloseAtServer = 1;
                            return objDeleteAttachmentResult;
                        }
                    }
                    if (item.ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.Asset))
                    {
                        int assetId = Convert.ToInt32(item.FkId);
                        int? AssetStatus;
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            AssetStatus = objContext.SearchAllWithClientID<Asset>(new Asset(), clientCode).Where(w => w.AssetID == assetId).Select(s => s.AssetStatusID).FirstOrDefault();
                        }

                        if (AssetStatus != null && (AssetStatus == 2 || AssetStatus == 3))
                        {
                            objDeleteAttachmentResult.IsStatusCloseAtServer = 1;
                            return objDeleteAttachmentResult;
                        }
                    }
                    //Check If WorkRequest/WorkOrder/Asset is not closed/Canceled/Completed 
                    DeleteAttachment(attachmentPath, item, clientCode);
                }
            }
            // scope.Complete();
            objDeleteAttachmentResult.ExtassetfileTable = model;
            objDeleteAttachmentResult.Success = 1;
            // }
            return objDeleteAttachmentResult;
        }


        /// <summary>
        /// Change WorkRequest Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ChangeWorkRequestStatusResult ChangeWorkRequestStatus(ChangeWorkRequestStatusResult model, string clientCode)
        {
            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            if (!obj.IsUserActive(model.EmployeeID, clientCode))
            {
                model.IsUserActive = 0;
                return model;
            }
            else
            {
                model.IsUserActive = 1;
            }
            //Check For WorkRequest Status Close/Cancel
            int? WorkRequestStatus;
            using (ServiceContext objContext = new ServiceContext(0))
            {
                WorkRequestStatus = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.RequestNo == model.RequestNo).Select(s => s.RequestStatusID).FirstOrDefault();
            }

            if (WorkRequestStatus != null && (WorkRequestStatus == 2 || WorkRequestStatus == 3))
            {
                model.IsStatusCloseAtServer = 1;
                return model;
            }

            string empID = Convert.ToString(model.EmployeeID);
            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            Worequest objNewWorequestnew = new Worequest
            {
                RequestStatusID = model.RequestStatusId,
                ModifiedBy = empID,
                Remarks = model.Remarks,
                ModifiedDate = DateTime.Now
            };
            using (ServiceContext objContext = new ServiceContext(0))
            {
                objContext.UpdateWithClientID<Worequest>(objNewWorequestnew, "  RequestNo = '" + model.RequestNo + "'", clientCode);
            }

            //  DbContext.SaveChanges();
            //  scope.Complete();
            //}
            model.Success = 1;
            model.Message = ConstatntMessages.SuccessWorkRequestStatus;
            return model;
        }


        /// <summary>
        /// Generate WorkOrdre From Work Request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public GenerateWorkOrderResult GenerateWorkOrdreFromWorkRequest(Worequest model, string clientCode)
        {
            GenerateWorkOrderResult objGenerateWorkOrderResult = new GenerateWorkOrderResult();
            WorkOrder objWorkOrder = new WorkOrder();

            //Check For User is Active or Not
            EmployeeDetailService obj = new EmployeeDetailService();
            if (!obj.IsUserActive(model.EmployeeID, clientCode))
            {
                objGenerateWorkOrderResult.IsUserActive = 0;
                return objGenerateWorkOrderResult;
            }
            else
            {
                objGenerateWorkOrderResult.IsUserActive = 1;
            }
            //Check For WorkRequest Status Close/Cancel
            int? WorkRequestStatus;
            using (ServiceContext objContext = new ServiceContext(0))
            {
                WorkRequestStatus = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.RequestNo == model.RequestNo).Select(s => s.RequestStatusID).FirstOrDefault();
            }

            if (WorkRequestStatus != null && (WorkRequestStatus == 2 || WorkRequestStatus == 3))
            {
                objGenerateWorkOrderResult.IsStatusCloseAtServer = 1;
                return objGenerateWorkOrderResult;
            }

            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
            //{
            #region Insert Into Work Order
            //Create Work Order No for New WO

            Int64 CurrentMinNo = Convert.ToInt64(model.L2ID.ToString().PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));
            //var TempListJO = DbContext.workorders.Where(x => (!x.WorkorderNo.Contains("PC") && !x.WorkorderNo.Contains("PM") && !x.WorkorderNo.Contains("PCM")) & x.L2ID == model.L2ID).ToList();

            List<WorkOrder> TempListJO = new List<WorkOrder>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                TempListJO = objContext.SearchAllWithClientID<WorkOrder>(new WorkOrder(), clientCode).Where(x => (!x.WorkorderNo.Contains("PC") && !x.WorkorderNo.Contains("PM") && !x.WorkorderNo.Contains("PCM")) & x.L2ID == model.L2ID).ToList();
            }

            foreach (var item in TempListJO)
            {
                if (Convert.ToInt64(item.WorkorderNo) > CurrentMinNo)
                {
                    CurrentMinNo = Convert.ToInt64(item.WorkorderNo);
                }
            }
            objWorkOrder.WorkorderNo = (CurrentMinNo + 1).ToString().PadLeft(11, '0');

            //Create Work Order No for New WO
            objWorkOrder.RequestorID = model.RequesterID;
            objWorkOrder.L2ID = model.L2ID;
            objWorkOrder.L3ID = model.L3ID;
            objWorkOrder.L4ID = model.L4ID;
            objWorkOrder.L5ID = model.L5ID;
            objWorkOrder.LocationID = model.LocationID;
            objWorkOrder.AssetID = model.AssetID;
            objWorkOrder.WorkStatusID = 1;
            objWorkOrder.WorkPriorityID = model.Workpriorityid;
            objWorkOrder.WorkTypeID = model.WorkTypeID;
            objWorkOrder.WOTradeID = model.Worktradeid;
            objWorkOrder.MaintDivisionID = model.MaintdivId;
            objWorkOrder.MaintdeptID = model.MaintdeptId;
            objWorkOrder.MaintsubdeptID = model.MainSubDeptId;
            objWorkOrder.ProblemDescription = model.ProblemDesc;
            objWorkOrder.DateReceived = model.ReceivedDate;
            objWorkOrder.DateRequired = model.RequiredDate;
            objWorkOrder.Notes = model.Remarks;
            objWorkOrder.CreatedBy = model.CreatedBy;
            objWorkOrder.CreatedDate = System.DateTime.Now;
            objWorkOrder.AuthorisedEmployeeId = model.AuthorisedEmployeeId;
            objWorkOrder.ProjectNumber = model.ProjectNumber;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                objContext.SaveWithClientID<WorkOrder>(objWorkOrder, clientCode, true);
            }

            //DbContext.workorders.Add(objWorkOrder);
            //DbContext.SaveChanges();
            model.WorkorderNo = objWorkOrder.WorkorderNo;

            objGenerateWorkOrderResult.WorkOrderResult = objWorkOrder.CastTo<WorkOrder, GenerateWorkOrderModel>();
            #endregion

            #region Update Work Request

            //AddUpdateWorkRequestResult objAddUpdateWorkRequestResult = AddUpdateWorkRequest(model, AppDomain.CurrentDomain.BaseDirectory, clientCode);
            AddUpdateWorkRequestResult objAddUpdateWorkRequestResult = AddUpdateWorkRequest(model, Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AttachmentPath"]), clientCode);

            #endregion

            #region Add Attachment from WR to WO

            //string currPhysPathJobRequest = AppDomain.CurrentDomain.BaseDirectory + "Attachments\\JobRequest" + @"\" + model.RequestNo;
            //string currPhysPathJobOrder = AppDomain.CurrentDomain.BaseDirectory + "Attachments\\JobOrder" + @"\" + model.WorkorderNo;

            Account objAC = EmployeeDetailService.GetAccountType(clientCode);
            string strAttachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\";

            string currPhysPathJobRequest = Convert.ToString(strAttachmentPath) + "Attachments\\JobRequest" + @"\" + model.RequestNo;
            string currPhysPathJobOrder = Convert.ToString(strAttachmentPath) + "Attachments\\JobOrder" + @"\" + model.WorkorderNo;

            string moduleType = GetEnumDescription(SystemEnum.WebServiceModuleType.WorkRequest);
            //List<ExtAssetFile> lstExtAssetFileAdd = DbContext.extassetfiles.Where(w => w.FkId == model.RequestNo && w.ModuleType == moduleType).Select(s => s).ToList().CastTo<ExtAssetFile, ExtassetfileModel>();

            List<ExtAssetFile> lstExtAssetFileAdd = new List<ExtAssetFile>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                lstExtAssetFileAdd = objContext.SearchAllWithClientID<ExtAssetFile>(new ExtAssetFile(), clientCode).Where(w => w.FkId.Trim() == model.RequestNo && w.ModuleType.Trim() == moduleType).Select(s => s).ToList();
            }

            if (lstExtAssetFileAdd.Count > 0 && Directory.Exists(currPhysPathJobRequest))
            {
                //Add Attachment Record to Database Table
                lstExtAssetFileAdd.ForEach(attachment =>
                {
                    ExtAssetFile objectextassetfile;
                    objectextassetfile = new ExtAssetFile()
                    {
                        FkId = model.WorkorderNo,
                        FileLink = attachment.FileLink.Replace("JobRequest", "JobOrder").Replace(attachment.FkId, model.WorkorderNo),
                        FileDescription = attachment.FileDescription,
                        FileName = attachment.FileName,
                        ModuleType = GetEnumDescription(SystemEnum.WebServiceModuleType.WorkOrder),
                        CreatedDate = DateTime.Now,
                        CreatedBy = attachment.CreatedBy,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = attachment.ModifiedBy,
                    };
                    //DbContext.extassetfiles.Add(objectextassetfile);
                    //DbContext.SaveChanges();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        List<ExtAssetFile> lst = objContext.SaveWithClientID<ExtAssetFile>(objectextassetfile, clientCode, true).ToList();
                        objectextassetfile.AutoId = lst[0].AutoId;
                    }

                    attachment.AutoId = objectextassetfile.AutoId;
                    attachment.FkId = objectextassetfile.FkId;
                    attachment.FileLink = objectextassetfile.FileLink;
                    attachment.FileDescription = objectextassetfile.FileDescription;
                    attachment.FileName = objectextassetfile.FileName;
                    attachment.ModuleType = objectextassetfile.ModuleType;
                    attachment.CreatedDate = objectextassetfile.CreatedDate;
                    attachment.CreatedBy = objectextassetfile.CreatedBy;
                    attachment.ModifiedDate = objectextassetfile.ModifiedDate;
                    attachment.ModifiedBy = objectextassetfile.ModifiedBy;
                    attachment.FileSaveMessage = ConstatntMessages.FileSuccessfullyUploadMessage;

                });

                //Copy Attachment from WR to WO(Physically)

                if (!Directory.Exists(currPhysPathJobOrder))
                {
                    Directory.CreateDirectory(currPhysPathJobOrder);
                }

                foreach (string fileAttach in Directory.GetFiles(currPhysPathJobRequest))
                {
                    File.Copy(fileAttach, Path.Combine(currPhysPathJobOrder, Path.GetFileName(fileAttach)), true);
                }
            }

            #endregion

            //   scope.Complete();

            objGenerateWorkOrderResult.WorkRequestResult = objAddUpdateWorkRequestResult.WorkRequestTable;
            objGenerateWorkOrderResult.WorkRequestResult.ExtassetfileTable = lstExtAssetFileAdd;
            objGenerateWorkOrderResult.Message = ConstatntMessages.SuccessGeneratedWOMessage;
            // }

            return objGenerateWorkOrderResult;
        }


        #endregion


        public static string GetEnumDescription(SystemEnum.WebServiceModuleType objEnum)
        {
            Enum enumValue = objEnum;
            string output = null;
            var type = enumValue.GetType();
            var fi = type.GetField(enumValue.ToString());
            var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
            if (attrs != null && attrs.Length > 0) output = attrs[0].Description;
            return output;
        }


        /// <summary>
        /// Add Attachment by passing lstAttachment and FKId
        /// </summary>
        /// <param name="lstExtAssetFileAdd"></param>
        /// <param name="FKId"></param>
        public void AddAttachment(List<ExtAssetFile> lstExtAssetFileAdd, string FKId, string attachmentPath, string clientCode)
        {
            if (lstExtAssetFileAdd != null && lstExtAssetFileAdd.Count > 0)
            {

                if (lstExtAssetFileAdd.FirstOrDefault().ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkRequest))
                {
                    attachmentPath = attachmentPath + "Attachments\\JobRequest" + @"\";
                }

                if (lstExtAssetFileAdd.FirstOrDefault().ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.WorkOrder))
                {
                    attachmentPath = attachmentPath + "Attachments\\JobOrder" + @"\";
                }

                if (lstExtAssetFileAdd.FirstOrDefault().ModuleType == GetEnumDescription(SystemEnum.WebServiceModuleType.Asset))
                {
                    attachmentPath = attachmentPath + "Attachments\\Asset" + @"\";
                }

                ExtAssetFile objectextassetfile;
                lstExtAssetFileAdd.ForEach(attachment =>
                {
                    //Add Attachment to Physical Folder
                    //check if File is not attached
                    if (attachment.FileToAttach.Length > 0)
                    {
                        //Check if same file do not exists in DB 
                        int addedToPhysicalFolder = SaveAttachmentFile(attachment.FileToAttach, FKId, attachment.FileName, attachmentPath, attachment);
                        if (addedToPhysicalFolder > 0)
                        {
                            //Add Attachment Record to Database Table
                            objectextassetfile = new ExtAssetFile()
                            {
                                FkId = FKId,
                                FileLink = attachment.FileLink,
                                FileDescription = attachment.FileDescription,
                                FileName = attachment.FileName,
                                ModuleType = attachment.ModuleType,
                                CreatedDate = System.DateTime.Now,
                                CreatedBy = attachment.CreatedBy,
                                ModifiedDate = System.DateTime.Now,
                                ModifiedBy = attachment.ModifiedBy,
                            };

                            using (ServiceContext objContext = new ServiceContext(0))
                            {
                                List<ExtAssetFile> lst = objContext.SaveWithClientID<ExtAssetFile>(objectextassetfile, clientCode, true).ToList();
                                objectextassetfile.AutoId = lst[0].AutoId;
                            }

                            //DbContext.extassetfiles.Add(objectextassetfile);
                            //DbContext.SaveChanges();
                            attachment.AutoId = objectextassetfile.AutoId;
                            attachment.FileLink = objectextassetfile.FileLink;
                            attachment.FileSaveMessage = ConstatntMessages.FileSuccessfullyUploadMessage;
                        }
                        else if (addedToPhysicalFolder == -1)
                        {
                            attachment.FileSaveMessage = ConstatntMessages.FileAlreadyExistMessage;
                        }
                        else
                        {
                            attachment.FileSaveMessage = ConstatntMessages.ErrorMessage;
                        }
                    }
                    else
                    {
                        attachment.FileSaveMessage = ConstatntMessages.FileNoAttachedMessage;
                    }
                });
            }
        }

        /// <summary>
        /// Save Attachment to Physical folder
        /// </summary>
        /// <param name="fileContents"></param>
        /// <param name="strFolderName"></param>
        /// <param name="strFileName"></param>
        /// <param name="destinationPath"></param>
        public int SaveAttachmentFile(byte[] fileContents, string strFolderName, string strFileName, string destinationPath, ExtAssetFile model)
        {
            try
            {
                string strFolderPath = destinationPath + strFolderName;
                string strFilePath = strFolderPath + @"\" + strFileName;

                //set File Link from DB
                int index = strFilePath.IndexOf("Attachments");
                string pathForDBTable = strFilePath.Substring(index);
                model.FileLink = pathForDBTable.Replace(@"\", @"/");

                //check if folder exists or not
                if (!Directory.Exists(strFolderPath))
                {
                    Directory.CreateDirectory(strFolderPath);
                }
                //check if file exists or not
                if (!File.Exists(strFilePath))
                {
                    using (FileStream stream = new FileStream(strFilePath, FileMode.CreateNew))
                    {
                        System.IO.BinaryWriter writer = new BinaryWriter(stream);
                        writer.Write(fileContents, 0, fileContents.Length);
                        writer.Close();
                    }
                }
                else
                {
                    return -1;
                }
                return 1;

            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static Account GetAccountType(string clientCode)
        {
            using (ServiceContext context = new ServiceContext(0))
            {
                Account objAccount = new Account();
                objAccount.ClientCode = clientCode;
                return context.GetMasterAccountDetail(clientCode);
            }

        }

        public static PermissionAccess SetEmployeePermission(employees emp, string clientCode, int? objAccountDbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();


                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = emp.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "UserGroupId",
                    Value = emp.UserGroupId,
                    DBType = DbType.Int32
                });

                string query = string.Empty;
                PermissionAccess objPermission = new PermissionAccess();

                using (ServiceContext objDapperContext = new ServiceContext(0))
                {
                    if (objAccountDbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = @"SELECT  
 (REPLACE(REPLACE(ModuleName,' ',''),'/','') ||  '_' ||  REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') ||  '_' ||  REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(AllowAccess) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";
                    }
                    else
                    {
                        query = @"SELECT  
 CONCAT(REPLACE(REPLACE(ModuleName,' ',''),'/','') , '_' , REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') , '_' , REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(CAST(AllowAccess AS DECIMAL)) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";

                    }

                    List<Permission> lst = objDapperContext.ExecuteQueryWithClientID<Permission>(query, parameters, clientCode).ToList();

                    foreach (Permission lstPermission in lst)
                    {
                        PropertyInfo info = objPermission.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstPermission.PermissionName.ToString().ToLower());
                        if (info != null)
                        {
                            /*Set the Value to Model*/
                            info.SetValue(objPermission, lstPermission.AllowAccess);
                        }
                    }
                }
                return objPermission;
            }
            catch
            {
                throw;
            }
        }


    }
}