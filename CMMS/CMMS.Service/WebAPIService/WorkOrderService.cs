﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class WorkOrderService : DBExecute
    {
        #region "Client Wise Methods"


        /// <summary>
        /// Get Work Order List with associated Tab Data
        /// </summary>
        /// <param name="isCentral">Whether is Employee Central or not </param>
        /// <param name="employeeID"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="workOrderNoToRefresh"></param>
        /// <returns></returns>
        public GetJobOrderListResult GetJobOrderList(bool isCentral, int employeeID, int pageIndex, int pageSize, string workOrderNoToRefresh, bool FromJoHistory, string strCurrentTimeStamp, string clientCode)
        {
            var objJobOrderListResult = new GetJobOrderListResult();
            var sqlSelectQuery = new StringBuilder();

            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);

            //Check For User is Active or Not
            EmployeeDetailService objEmpDetailService = new EmployeeDetailService();
            if (!objEmpDetailService.IsUserActive(employeeID, clientCode))
            {
                objJobOrderListResult.IsUserActive = 0;
                return objJobOrderListResult;
            }
            else
            {
                objJobOrderListResult.IsUserActive = 1;
            }

            string strWhere;
            if (!string.IsNullOrEmpty(workOrderNoToRefresh))
            {
                strWhere = "workorders.WorkorderNo = '" + workOrderNoToRefresh + "'";
            }
            else
            {
                strWhere = string.Empty;
            }

            string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            

            Account objAccount = GetAccountType(clientCode);
            int clientID = objAccount.ClientID;
            Tbl_Feature objTblFeature = new Tbl_Feature();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                string strQueryFeatureTable = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(bit, DecryptByKey(Area)) AS 'Area',CONVERT(bit, DecryptByKey(CMProject)) AS 'CMProject',CONVERT(bit, DecryptByKey(CMProjectNoMandatory)) AS 'CMProjectNoMandatory',CONVERT(bit, DecryptByKey(WebRequesterLicence)) AS 'WebRequesterLicence'FROM [dbo].[Tbl_Feature] WHERE ClientID = @ClientID";
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                parameters.Add(new DBParameters() { Name = "clientID", Value = clientID, DBType = DbType.Int32 });
                objTblFeature = objContext.ExecuteQueryWithClientID<Tbl_Feature>(strQueryFeatureTable, parameters, "").FirstOrDefault();
            }

            if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
            {
                sqlSelectQuery.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
            }
            else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
            }
            //00317000001

            sqlSelectQuery.Append(
                " SELECT workorders.*," + 
                //" a.EmployeeID AssignedEmployeeId," +                
                " p.TypePMgenID,pmschedule.PMNo,pmmeters.MeterNo,worequest.RequestNo WorkReqNo," +
                "(SELECT NAME FROM employees e2 WHERE e2.EmployeeID = (SELECT Location.EmployeeID FROM Location  Where Location.LocationID = workorders.LocationID)) LocationAuthEmployeeName,LOCATION.Latitude,LOCATION.Longitude " +
                "FROM workorders INNER JOIN workstatus ON workorders.WorkStatusID=WorkStatus.WorkStatusID " +
                //" LEFT JOIN assigntoworkorder a ON a.WorkorderNo = workorders.WorkorderNo " +
                "INNER JOIN worktype ON worktype.WorkTypeID = workorders.WorkTypeID INNER JOIN workpriority ON workpriority.WorkPriorityID = workorders.WorkPriorityID  " +
                "LEFT OUTER JOIN worequest ON worequest.WorkOrderNo = workorders.WorkOrderNo LEFT OUTER JOIN assets asset ON asset.assetid = workorders.assetid  " +
                "LEFT OUTER JOIN dbo.cleaningInspectionGroup CIG ON CIG.CleaningInspectionGroupId = workorders.CIGID LEFT OUTER JOIN LOCATION ON LOCATION.locationid = workorders.locationid  " +
                "INNER JOIN L2 ON L2.L2ID = workorders.L2ID INNER JOIN Sector ON Sector.SectorID = L2.SectorID LEFT JOIN L2Classes ON L2.L2ClassID = L2Classes.L2ClassID  " +
                "LEFT OUTER JOIN L3 ON L3.L3ID = workorders.L3ID LEFT OUTER JOIN L4 ON L4.L4ID = workorders.L4ID LEFT OUTER JOIN l5 ON l5.l5id = workorders.l5id  " +
                "INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID= workorders.MaintDivisionID  " +
                "INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = workorders.maintdeptID INNER JOIN MaintSubDept ON MaintSubDept.MaintSubDeptID = workorders.maintsubdeptID  " +
                "LEFT OUTER JOIN MaintGroup ON MaintGroup.GroupID = workorders.groupID LEFT OUTER JOIN failurecause ON failurecause.FailureCauseID = workorders.FailureCauseID  " +
                "LEFT OUTER JOIN pmschedule ON pmschedule.PMID = workorders.PMID LEFT OUTER JOIN pmmeters ON pmmeters.meterid = workorders.meterid  " +
                "LEFT OUTER JOIN pmchecklist ON pmchecklist.ChecklistID = workorders.PMChecklistID LEFT OUTER JOIN employees ON employees.EmployeeID = workorders.RequestorID LEFT Outer JOIN pmschedule p ON p.PMID = workorders.PMID ");
            if (!FromJoHistory)
            {
                //sqlSelectQuery.AppendFormat(" where (a.EmployeeID = {0}  OR workorders.CreatedBy =  {0})  ", employeeID);
                sqlSelectQuery.Append(" where 1=1 ");
            }
            

            employees objemployees = new employees();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                objemployees.EmployeeID = employeeID;
                objemployees = objContext.SearchAllWithClientID<employees>(objemployees, clientCode).FirstOrDefault();
            }
            PermissionAccess objPermissionAccess = SetEmployeePermission(objemployees, clientCode, objAccount.DbType);

            //if ((!objPermissionAccess.MobileApp_WO_Allowaccess) && (objPermissionAccess.JobOrder_JobOrder_Allowaccess))
            //{
            //    sqlSelectQuery.Append(" AND 1=1 ");
            //}
            //else if (objPermissionAccess.MobileApp_WO_Allowaccess)
            //{
            //    if (!FromJoHistory)
            //    {
            //        sqlSelectQuery.AppendFormat(" and (workorders.EmployeeID = {0}  OR workorders.CreatedBy =  {0})  ", employeeID);
            //    }
            //}
            //else
            //{
            //    sqlSelectQuery.Append(" AND 1=0 ");
            //}

            
            if (!FromJoHistory && currentTimeStamp != null)
            {

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.Append(" AND ( (workorders.CreatedDate >= @currentTimeStamp OR workorders.ModifiedDate >= @currentTimeStamp) ");

                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" and (workorders.LocationID in  (select LocationID from Employees_Location where empID = {0} ) ", employeeID);
                        sqlSelectQuery.AppendFormat(" Or workorders.LocationID in  (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp))) ", employeeID);
                    }

                    sqlSelectQuery.AppendFormat(" 	    Or  ( workorders.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", employeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectQuery.Append(" AND ( (workorders.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR workorders.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");

                    if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                    {
                        sqlSelectQuery.AppendFormat(" and (workorders.LocationID in  (select LocationID from Employees_Location where empID = {0} ) ", employeeID);
                        sqlSelectQuery.AppendFormat(" or workorders.LocationID in  (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ) )", employeeID);
                    }

                    sqlSelectQuery.AppendFormat(" 	    Or  ( workorders.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", employeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }

            }
                        
            //if ((!objPermissionAccess.MobileApp_WO_Allowaccess) && (objPermissionAccess.JobOrder_JobOrder_Allowaccess))
            //{
            //    sqlSelectQuery.Append(" AND 1=1 AND workorders.workstatusid <> 2 AND workorders.workstatusid <> 3 ");
            //}
            //else if (objPermissionAccess.MobileApp_WO_Allowaccess)
            //{
            //    if (!FromJoHistory && currentTimeStamp == null)
            //    {
            //        sqlSelectQuery.Append(" AND 1=1 AND workorders.workstatusid <> 2 AND workorders.workstatusid <> 3 ");
            //    }
            //}
            //else
            //{
            //    sqlSelectQuery.Append(" AND 1=0 ");
            //}

            if (!FromJoHistory && currentTimeStamp == null)
            {
                sqlSelectQuery.Append(" AND 1=1 AND workorders.workstatusid <> 2 AND workorders.workstatusid <> 3 ");
            }
            if (FromJoHistory && currentTimeStamp == null)
            {
                sqlSelectQuery.Append(" ");
            }


            if (!string.IsNullOrEmpty(workOrderNoToRefresh))
                sqlSelectQuery.AppendFormat(" and {0}", strWhere);
                       
            if (!isCentral)
            {
                sqlSelectQuery.AppendFormat(
                    " 	and ( workorders.L2ID in (select L2Id from employees_L2 where empID = {0} union Select L2Id from employees where employeeId = {0} )	",
                    employeeID);


                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectQuery.AppendFormat(" 	    Or  ( workorders.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= @currentTimeStamp OR employees_L2.ModifiedDate >= @currentTimeStamp)  ", employeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");

                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectQuery.AppendFormat(" 	    Or  ( workorders.L2ID IN	 ");
                    sqlSelectQuery.AppendFormat(" 	       (SELECT L2Id	 ");
                    sqlSelectQuery.AppendFormat(" 	        FROM employees_L2	 ");
                    sqlSelectQuery.AppendFormat(" 	        WHERE empID = {0}  AND (employees_L2.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR employees_L2.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss'))  ", employeeID);
                    sqlSelectQuery.AppendFormat(" 	        )) ) ");
                }


                sqlSelectQuery.AppendFormat(
                    " 	and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = {0} union Select MaintSubDeptID from employees where employeeId = {0} )	",
                    employeeID);

               


                if (objTblFeature != null)
                {
                    if(objTblFeature.Area)
                        sqlSelectQuery.AppendFormat(" AND (workorders.L3ID in (select L3Id from Employees_L3 where empID = {0}) OR (workorders.L3ID IS NULL)) ", employeeID);
                }

                
                if (objPermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    sqlSelectQuery.AppendFormat(" 	        AND (workorders.LocationID in (select LocationID from Employees_Location where empID = {0}) ", employeeID);

                    if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    {
                        sqlSelectQuery.AppendFormat(" 	        Or ( workorders.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= @currentTimeStamp OR Employees_Location.ModifiedDate >= @currentTimeStamp)   )) ) ", employeeID);
                    }
                    else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {                        
                        sqlSelectQuery.AppendFormat(" 	        Or ( workorders.LocationID in (select LocationID from Employees_Location where empID = {0} AND (Employees_Location.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR Employees_Location.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')))  ) ) ", employeeID);
                    }
                }
            }

            sqlSelectQuery.Append(" ORDER BY workorders.CreatedDate DESC,workorders.WorkorderNo Desc ");

            List<WorkOrder> resultWorkRequest;
            //DbContext.ObjectContext.ExecuteStoreQuery<WorkOrderModel>(sqlSelectQuery.ToString()).ToList();

            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    resultWorkRequest = objContext.ExecuteQueryWithClientID<WorkOrder>(sqlSelectQuery.ToString(), parameters, clientCode).ToList();
                    objJobOrderListResult.TotalCount = resultWorkRequest.Count();

                    if (resultWorkRequest.Count > 0)
                    {
                        objJobOrderListResult.TotalPages = (resultWorkRequest.Count / pageSize);
                        if (resultWorkRequest.Count % pageSize != 0) objJobOrderListResult.TotalPages++;
                        resultWorkRequest = objContext.GetPageList<WorkOrder>(resultWorkRequest, pageIndex, pageSize);
                    }
                    else
                    {
                        objJobOrderListResult.TotalPages = 0;
                        resultWorkRequest = new List<WorkOrder>();
                    }
                    objJobOrderListResult.WorkOrderTable = resultWorkRequest;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            
            objJobOrderListResult.SynchTime = currentTimeStamp.ToString();            
            objJobOrderListResult.EmployeeID = employeeID;
            objJobOrderListResult.IsCentral = isCentral;
            objJobOrderListResult.WorkOrderNoToRefresh = workOrderNoToRefresh;
            objJobOrderListResult.PageIndex = pageIndex;
            objJobOrderListResult.PageSize = pageSize;

            //Set Assosiated Table in WorkOrder List
            if (objJobOrderListResult.WorkOrderTable.Count > 0)
            {
                var workOrderNo = objJobOrderListResult.WorkOrderTable.Select(w => w.WorkorderNo).ToList();
                var commaSepratedworkOrderNo = string.Join("','", workOrderNo);

                #region AssignTo

                string strAssignToQuery = "SELECT * FROM assigntoworkorder E WHERE E.WorkorderNo IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Assigntoworkorder> assignToWorkOrder = new List<Assigntoworkorder>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        assignToWorkOrder = objContext.ExecuteQueryWithClientID<Assigntoworkorder>(strAssignToQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.AssignToWorkOrderTable = new List<Assigntoworkorder>();
                });

                if (assignToWorkOrder.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (assignToWorkOrder.Any(wo => wo.WorkorderNo == w.WorkorderNo))
                        {
                            w.AssignToWorkOrderTable =
                                assignToWorkOrder.Where(assignToWO => assignToWO.WorkorderNo == w.WorkorderNo).
                                    ToList();

                        }
                    });
                }

                #endregion

                #region Direct Issue

                string strdirectIssueListQuery = "SELECT * FROM [direct] d WHERE d.WONo IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Direct> directIssueList = new List<Direct>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        directIssueList = objContext.ExecuteQueryWithClientID<Direct>(strdirectIssueListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.DirectTable = new List<Direct>();
                });


                if (directIssueList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {

                        if (directIssueList.Any(wo => wo.WONo == w.WorkorderNo))
                        {
                            w.DirectTable = directIssueList.Where(x => x.WONo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Issues Item


                string strissuesItemListQuery = "SELECT * FROM [issue] d WHERE d.[WONo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Issue> issuesItemList = new List<Issue>();
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        issuesItemList = objContext.ExecuteQueryWithClientID<Issue>(strissuesItemListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }



                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.IssueTable = new List<Issue>();
                });


                if (issuesItemList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (issuesItemList.Any(wo => wo.WONo == w.WorkorderNo))
                        {
                            w.IssueTable = issuesItemList.Where(x => x.WONo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Return Item


                string strreturnItemListQuery = "SELECT * FROM [return] d WHERE d.[WONo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Return> returnItemList = new List<Return>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        returnItemList = objContext.ExecuteQueryWithClientID<Return>(strreturnItemListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.ReturnTable = new List<Return>();
                });


                if (returnItemList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (returnItemList.Any(wo => wo.WONo == w.WorkorderNo))
                        {
                            w.ReturnTable = returnItemList.Where(x => x.WONo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Job Plan

                string strjobPlanListQuery = "SELECT * FROM [jobtask] d WHERE d.[WoNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Jobtask> jobPlanList = new List<Jobtask>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        jobPlanList = objContext.ExecuteQueryWithClientID<Jobtask>(strjobPlanListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }



                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.JobTaskTable = new List<Jobtask>();
                });


                if (jobPlanList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (jobPlanList.Any(wo => wo.WoNo == w.WorkorderNo))
                        {
                            w.JobTaskTable = jobPlanList.Where(x => x.WoNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Safety Instruction

                string strsafetyInstructionListQuery = "SELECT * FROM [SITask] d WHERE d.[WoNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<SITask> safetyInstructionList = new List<SITask>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        safetyInstructionList = objContext.ExecuteQueryWithClientID<SITask>(strsafetyInstructionListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.SITaskTable = new List<SITask>();
                });

                if (safetyInstructionList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (safetyInstructionList.Any(wo => wo.WoNo == w.WorkorderNo))
                        {
                            w.SITaskTable = safetyInstructionList.Where(x => x.WoNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region PM Checklist

                //DataGridPMChecklistItem

                string strworkOrderElementListQuery = "SELECT * FROM [workorderelements] d WHERE d.[WorkorderNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<Workorderelement> workOrderElementList = new List<Workorderelement>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        workOrderElementList = objContext.ExecuteQueryWithClientID<Workorderelement>(strworkOrderElementListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.WorkOrderElementTable = new List<Workorderelement>();
                });

                if (workOrderElementList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (workOrderElementList.Any(wo => wo.WorkorderNo == w.WorkorderNo))
                        {
                            w.WorkOrderElementTable =
                                workOrderElementList.Where(x => x.WorkorderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                //DataGridRequiredItems
                string strworkOrderItemsListQuery = "SELECT * FROM [WorkOrderItems] d WHERE d.[WorkOrderNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<WorkOrderItem> workOrderItemsList = new List<WorkOrderItem>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        workOrderItemsList = objContext.ExecuteQueryWithClientID<WorkOrderItem>(strworkOrderItemsListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.WorkOrderItemTable = new List<WorkOrderItem>();
                });

                if (workOrderItemsList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (workOrderItemsList.Any(wo => wo.WorkOrderNo == w.WorkorderNo))
                        {
                            w.WorkOrderItemTable =
                                workOrderItemsList.Where(x => x.WorkOrderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                //DataGridRequiredTools
                string strworkOrderToolsListQuery = "SELECT * FROM [WorkOrderTools] d WHERE d.[WorkOrderNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<WorkOrderTool> workOrderToolsList = new List<WorkOrderTool>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        workOrderToolsList = objContext.ExecuteQueryWithClientID<WorkOrderTool>(strworkOrderToolsListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.WorkOrderToolTable = new List<WorkOrderTool>();
                });

                if (workOrderToolsList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (workOrderToolsList.Any(wo => wo.WorkOrderNo == w.WorkorderNo))
                        {
                            w.WorkOrderToolTable =
                                workOrderToolsList.Where(x => x.WorkOrderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                //DataGridRequiredPPE
                string strworkOrderPPEsListQuery = "SELECT * FROM [WorkOrderPPE] d WHERE d.[WorkOrderNo] IN ( '" + commaSepratedworkOrderNo + "' ) ";
                List<WorkOrderPPE> workOrderPPEsList = new List<WorkOrderPPE>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        workOrderPPEsList = objContext.ExecuteQueryWithClientID<WorkOrderPPE>(strworkOrderPPEsListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.WorkOrderPPETable = new List<WorkOrderPPE>();
                });

                if (workOrderPPEsList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (workOrderPPEsList.Any(wo => wo.WorkOrderNo == w.WorkorderNo))
                        {
                            w.WorkOrderPPETable =
                                workOrderPPEsList.Where(x => x.WorkOrderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Purchase Order/Request

                //DataGridPRItem
                string strpurchaseRequestItemListQuery = "SELECT pri.*,pr.PurchaseRequestID RequestNo,pr.PurchaseRequestID PurchaseRequestNo,pas.auth_status_desc,pas.Altauth_status_desc,s.StockNo ItemNo,s.StockNo FROM PurchaseRequestItems pri LEFT JOIN PurchaseRequest pr ON pr.Id = pri.PurchaseRequestID LEFT JOIN pr_authorisation_status pas ON pas.auth_status_id = pr.auth_status LEFT JOIN stockcode s ON s.StockID = pri.PartNumber WHERE pri.[JoborderID] IN ( '" + commaSepratedworkOrderNo + "' )";
                List<PurchaseRequestItem> purchaseRequestItemList = new List<PurchaseRequestItem>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        purchaseRequestItemList = objContext.ExecuteQueryWithClientID<PurchaseRequestItem>(strpurchaseRequestItemListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.PurchaseRequestItemTable = new List<PurchaseRequestItem>();
                });

                if (purchaseRequestItemList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (purchaseRequestItemList.Any(wo => wo.JoborderID == w.WorkorderNo))
                        {
                            w.PurchaseRequestItemTable =
                                purchaseRequestItemList.Where(x => x.JoborderID == w.WorkorderNo).ToList();
                        }
                    });
                }

                //DataGridPOItems
                string strpurchaseOrderItemListQuery = "SELECT poi.*,po.OrderStatus,po.DeliveryStatus,po.PurchaseOrderNo,s.StockNo ItemNo,s.StockNo FROM PurchaseOrderItems poi LEFT JOIN PurchaseOrder po ON po.ID = poi.PurchaseOrderID LEFT JOIN stockcode s ON s.StockID = poi.PartNumber WHERE poi.[JobOrderID] IN ( '" + commaSepratedworkOrderNo + "' )";
                List<PurchaseOrderItem> purchaseOrderItemList = new List<PurchaseOrderItem>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        purchaseOrderItemList = objContext.ExecuteQueryWithClientID<PurchaseOrderItem>(strpurchaseOrderItemListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.PurchaseOrderItemsTable = new List<PurchaseOrderItem>();
                });

                if (purchaseOrderItemList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (purchaseOrderItemList.Any(wo => wo.JobOrderID == w.WorkorderNo))
                        {
                            w.PurchaseOrderItemsTable =
                                purchaseOrderItemList.Where(x => x.JobOrderID == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Set Assosiated Attachments in WorkOrder List

                Enum enumValue = SystemEnum.WebServiceModuleType.WorkOrder;
                string output = null;
                var type = enumValue.GetType();
                var fi = type.GetField(enumValue.ToString());
                var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
                if (attrs != null && attrs.Length > 0) output = attrs[0].Description;


                string strfileAttachedForWOQuery = "SELECT * FROM extassetfiles E WHERE E.FkId IN ( '" + commaSepratedworkOrderNo + "' ) AND E.ModuleType ='" + output + "' ";

                List<ExtAssetFile> fileAttachedForWO = new List<ExtAssetFile>(); ;
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        fileAttachedForWO = objContext.ExecuteQueryWithClientID<ExtAssetFile>(strfileAttachedForWOQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }



                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.ExtassetfileTable = new List<ExtAssetFile>();
                });

                if (fileAttachedForWO.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {

                        if (fileAttachedForWO.Any(attachment => attachment.FkId == w.WorkorderNo))
                        {
                            w.ExtassetfileTable = fileAttachedForWO.Where(att => att.FkId == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Set Associated PM-Schedule

                //List<int?> PMIDs = objJobOrderListResult.WorkOrderTable.Select(w => w.PMID).ToList();
                //string commaSepratedPMIDs = string.Join("','", PMIDs);

                //var PMScheduleList =
                //    DbContext.ObjectContext.ExecuteStoreQuery<PMscheduleModel>(
                //        "SELECT * FROM [pmschedule] p WHERE p.[PMID] IN ( '" + commaSepratedPMIDs +
                //        "' ) ").ToList();

                //objJobOrderListResult.WorkOrderTable.ForEach(w =>
                //{
                //    w.PMScheduleTable = new List<PMscheduleModel>();
                //});

                //if (PMScheduleList.Count > 0)
                //{
                //    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                //    {
                //        if (PMScheduleList.Any(wo => wo.PMID == w.PMID))
                //        {
                //            w.PMScheduleTable =
                //                PMScheduleList.Where(x => x.PMID == w.PMID).ToList();
                //        }
                //    });
                //}

                #endregion

                #region Status Track

                string statusTrackItemListQuery = "SELECT jst.JOStatusTrackID,jst.WorkorderNo,jst.Old_WorkStatusID,w.WorkStatus AS OldWorkStatus, w.AltWorkStatus AS OldAltWorkStatus,jst.New_WorkStatusID,w1.WorkStatus AS NewWorkStatus,w1.AltWorkStatus AS NewAltWorkStatus" +
                              " ,ISNULL(jst.Latitude,0) Latitude,ISNULL(jst.Longitude,0) Longitude,jst.CreatedBy,jst.CreatedDate,e.Name,e.AltName  " +
                              " FROM JOStatusTrack jst " +
                              " LEFT JOIN workstatus w ON w.WorkStatusID = jst.Old_WorkStatusID " +
                              " LEFT JOIN workstatus w1 ON w1.WorkStatusID = jst.New_WorkStatusID " +
                              " LEFT JOIN employees e ON e.EmployeeID = jst.CreatedBy " +
                              " WHERE jst.WorkorderNo IN ( '" + commaSepratedworkOrderNo + "' ) ";

                List<JOStatusTrack> statusTrackItemList = new List<JOStatusTrack>();

                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        statusTrackItemList = objContext.ExecuteQueryWithClientID<JOStatusTrack>(statusTrackItemListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.StatusTrackTable = new List<JOStatusTrack>();
                });


                if (statusTrackItemList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (statusTrackItemList.Any(wo => wo.WorkorderNo == w.WorkorderNo))
                        {
                            w.StatusTrackTable = statusTrackItemList.Where(x => x.WorkorderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Item Requisition


                string itemRequisitionTableListQuery = " Select mrd.mr_id,mrd.PartId, case when mrd.PartId > 0 then 1 else 0 End isStock, " +
                      " mrd.mr_no,s.StockNo,mrd.part_desc,mrd.Altpart_desc,mrd.workorderno,mrd.uom,mrd.qty,mrd.ReceivedQty, " +
                      " mrd.CanceledQty,mrd.LocationId,location.LocationNo,location.LocationDescription, " +
                      " mrd.AssetId,assets.AssetNumber,assets.AssetDescription,assets.AssetAltDescription, " +
                      " mrd.SubWHSupId,mrd.SubWHIssueTechId,sup.Employeeno supNo,sup.Name supName,issue.Employeeno issueNo,issue.Name  issueName, " +
                      " (mrd.qty - mrd.ReceivedQty + mrd.CanceledQty) AvailableQty " +
                      " from material_req_details  mrd left outer join StockCode s on mrd.PartId=s.StockID left outer join location  on location.locationId=mrd.locationid left outer join assets on assets.AssetId=mrd.AssetId " +
                      " left outer join employees sup on sup.employeeid=mrd.SubWHSupId " +
                      " left outer join employees issue on issue.employeeid=mrd.SubWHSupId " +
                      " where 1=1 and mrd.WorkOrderNo IN ( '" + commaSepratedworkOrderNo + "' ) ";


                List<Material_req_detail> itemRequisitionTableList = new List<Material_req_detail>();

                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        itemRequisitionTableList = objContext.ExecuteQueryWithClientID<Material_req_detail>(itemRequisitionTableListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.ItemRequisitionTable = new List<Material_req_detail>();
                });


                if (itemRequisitionTableList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (itemRequisitionTableList.Any(wo => wo.WorkorderNo == w.WorkorderNo))
                        {
                            w.ItemRequisitionTable = itemRequisitionTableList.Where(x => x.WorkorderNo == w.WorkorderNo).ToList();
                        }
                    });
                }

                #endregion

                #region Diagram Table

                var locationId = objJobOrderListResult.WorkOrderTable.Where(y => y.LocationID != null).Select(w => w.LocationID).Distinct().ToList();
                var commaSepratedlocationId = string.Join("','", locationId);

                string diagramTableListQuery = "Select AutoId,FkId,FileLink,FileDescription,FileName,ModuleType, att.CreatedBy,att.CreatedDate,att.ModifiedBy,att.ModifiedDate,emp.Name ModifiedByName " +
                       "from extassetfiles att inner join employees  emp on att.ModifiedBy = emp.EmployeeID " +
                       " where  att.FkId IN ( '" + commaSepratedlocationId + "' ) and  att.ModuleType IN ('D')";


                List<ExtAssetFile> diagramTableList = new List<ExtAssetFile>();

                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        diagramTableList = objContext.ExecuteQueryWithClientID<ExtAssetFile>(diagramTableListQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                objJobOrderListResult.WorkOrderTable.ForEach(w =>
                {
                    w.DiagramTable = new List<ExtAssetFile>();
                });


                if (diagramTableList.Count > 0)
                {
                    objJobOrderListResult.WorkOrderTable.ForEach(w =>
                    {
                        if (diagramTableList.Any(wo => wo.FkId == w.LocationID.ToString()))
                        {
                            w.DiagramTable = diagramTableList.Where(x => x.FkId == w.LocationID.ToString()).ToList();
                        }
                    });
                }

                #endregion

            }
            return objJobOrderListResult;
        }

        /// <summary>
        /// Add/Update Work Order Signature Upload
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public WorkOrderUploadSignatureResult AddUpdateWorkOrderUploadSignature(WorkOrderUploadSignatureResult model, string clientCode)        
        {
            WorkOrderUploadSignatureResult objWorkOrderUploadSignatureResult = new WorkOrderUploadSignatureResult();
            Account objAC = EmployeeDetailService.GetAccountType(clientCode);
            try
            {

                WorkOrder objWorkOrder = new WorkOrder();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objWorkOrder = objContext.SearchAllWithClientID<WorkOrder>(new WorkOrder(), clientCode).Where(w => w.WorkorderNo == model.WorkorderNo).FirstOrDefault();
                }

                if (objWorkOrder != null)
                {

                    //string attachmentPath = physicalAttachmentPath + "Attachments\\JobOrder" + @"\";
                    string attachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\Attachments\\JobOrder\\" + model.WorkorderNo + @"\";

                    string fileName = model.UserSignatureFileName;
                    fileName = objWorkOrder.WorkorderNo + "_" + model.UserSignatureFileName;
                    //objWorkOrder.SignatureLink = "Attachments\\JobOrder\\UserSignature" + @"\" + fileName;

                    bool isFileExist = false;
                    if (objWorkOrder.ImageRaw != null)
                        isFileExist = true;


                    objWorkOrder.ImageRaw = model.ImageRaw;

                    //int intFileUploadFlag = DBExecute.SaveAttachmentFile(model.ImageRaw, "UserSignature", fileName, attachmentPath, true);

                    if (!isFileExist)
                    {
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.UpdateWithClientID<WorkOrder>(objWorkOrder, "  WorkorderNo = '" + model.WorkorderNo + "'", clientCode);
                        }

                        //objWorkOrderUploadSignatureResult.NewFilePath = objWorkOrder.SignatureLink;
                        objWorkOrderUploadSignatureResult.Success = 1;
                        objWorkOrderUploadSignatureResult.Message = ConstatntMessages.FileSuccessfullyUploadMessage;
                    }
                    else
                    {
                        objWorkOrderUploadSignatureResult.Success = 0;
                        objWorkOrderUploadSignatureResult.Message = ConstatntMessages.FileAlreadyExistMessage;
                    }
                }
                else
                {
                    objWorkOrderUploadSignatureResult.Success = 0;
                    objWorkOrderUploadSignatureResult.Message = ConstatntMessages.WorkOrderDoesNotExists;
                }
            }
            catch (Exception ex)
            {
                objWorkOrderUploadSignatureResult.Success = 0;
                objWorkOrderUploadSignatureResult.Message = ConstatntMessages.ErrorMessage;
            }
            return objWorkOrderUploadSignatureResult;


        }

        /// <summary>
        /// To get Work Order Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkOrderMasterResult GetWorkOrderMaster(string strCurrentTimeStamp, string clientCode)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkOrderMasterResult objWorkOrderMasterResult = new GetWorkOrderMasterResult();


            List<Failurecause> queryfailurecause = new List<Failurecause>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                queryfailurecause = objContext.SearchAllWithClientID<Failurecause>(new Failurecause(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (queryfailurecause.Count > 0)
            {
                objWorkOrderMasterResult.FailurecauseTable = queryfailurecause;
            }

            List<Rating> queryrating = new List<Rating>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                queryrating = objContext.SearchAllWithClientID<Rating>(new Rating(), clientCode).ToList();
            }

            if (queryrating.Count > 0)
            {
                objWorkOrderMasterResult.RatingTable = queryrating;
            }


            List<CostCenter> queryCostCenter = new List<CostCenter>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                queryCostCenter = objContext.SearchAllWithClientID<CostCenter>(new CostCenter(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (queryCostCenter.Count > 0)
            {
                objWorkOrderMasterResult.CostCenterTable = queryCostCenter;
            }

            List<Accountcode> queryaccountcodes = new List<Accountcode>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                queryaccountcodes = objContext.SearchAllWithClientID<Accountcode>(new Accountcode(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (queryaccountcodes.Count > 0)
            {
                objWorkOrderMasterResult.AccountcodeTable = queryaccountcodes;
            }

            //List<StockCode> querystockcode = new List<StockCode>();
            //using (ServiceContext objContext = new ServiceContext(0))
            //{
            //    querystockcode = objContext.SearchAllWithClientID<StockCode>(new StockCode(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).Take(1000).ToList();
            //}

            //if (querystockcode.Count > 0)
            //{
            //    objWorkOrderMasterResult.StockcodeTable = querystockcode;
            //}

            List<Substore> querysubstore = new List<Substore>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                querysubstore = objContext.SearchAllWithClientID<Substore>(new Substore(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (querysubstore.Count > 0)
            {
                objWorkOrderMasterResult.SubstoreTable = querysubstore;
            }

            List<Type_pm_gen> querytype_pm_gens = new List<Type_pm_gen>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                querytype_pm_gens = objContext.SearchAllWithClientID<Type_pm_gen>(new Type_pm_gen(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (querytype_pm_gens.Count > 0)
            {
                objWorkOrderMasterResult.Type_Pm_GensTable = querytype_pm_gens;
            }

            List<Usergroup> queryusergroup = new List<Usergroup>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                queryusergroup = objContext.SearchAllWithClientID<Usergroup>(new Usergroup(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
            }

            if (queryusergroup.Count > 0)
            {
                objWorkOrderMasterResult.UserGroupTable = queryusergroup;
            }

            //StringBuilder sqlSelectpmchecklist = new StringBuilder();
            //StringBuilder sqlSelectchecklistelements = new StringBuilder();
            StringBuilder sqlSelectchecklistInv = new StringBuilder();
            StringBuilder sqlSelectChecklistPPE = new StringBuilder();
            //StringBuilder sqlSelectChecklistTools = new StringBuilder();

            if (currentTimeStamp != null)
            {
                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
              //      sqlSelectpmchecklist.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                    // sqlSelectchecklistelements.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                    sqlSelectchecklistInv.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                    sqlSelectChecklistPPE.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                //    sqlSelectChecklistTools.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                }

                ////Get pmchecklist                
                //sqlSelectpmchecklist.Append(" SELECT * FROM pmchecklist pc");
                //if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                //{
                //    sqlSelectpmchecklist.AppendFormat(" where @currentTimeStamp = null ");
                //    sqlSelectpmchecklist.AppendFormat(" OR (pc.CreatedDate >= @currentTimeStamp OR pc.ModifiedDate >= @currentTimeStamp) ");
                //}
                //else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                //{
                //    sqlSelectpmchecklist.AppendFormat(" where (pc.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR pc.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                //}

                //Get checklistelements                
                //sqlSelectchecklistelements.Append(" SELECT * FROM checklistelements cle");
                //if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                //{
                //    sqlSelectchecklistelements.AppendFormat(" where @currentTimeStamp = null ");
                //    sqlSelectchecklistelements.AppendFormat(" OR (cle.CreatedDate >= @currentTimeStamp OR cle.ModifiedDate >= @currentTimeStamp) ");
                //}
                //else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                //{

                //    sqlSelectchecklistelements.AppendFormat(" where (cle.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cle.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                //}


                //Get checklistInv                
                sqlSelectchecklistInv.Append(" SELECT * FROM checklistInv cinv");

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectchecklistInv.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectchecklistInv.AppendFormat(" OR (cinv.CreatedDate >= @currentTimeStamp OR cinv.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectchecklistInv.AppendFormat(" Where (cinv.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cinv.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }


                //Get ChecklistPPE                
                sqlSelectChecklistPPE.Append(" SELECT * FROM ChecklistPPE cppe");

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectChecklistPPE.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectChecklistPPE.AppendFormat(" OR (cppe.CreatedDate >= @currentTimeStamp OR cppe.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectChecklistPPE.AppendFormat(" where (cppe.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cppe.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }

                ////Get ChecklistTools
                //sqlSelectChecklistTools.Append(" SELECT * FROM ChecklistTools ctool");

                //if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                //{
                //    sqlSelectChecklistTools.AppendFormat(" where @currentTimeStamp = null ");
                //    sqlSelectChecklistTools.AppendFormat(" OR (ctool.CreatedDate >= @currentTimeStamp OR ctool.ModifiedDate >= @currentTimeStamp) ");
                //}
                //else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                //{
                //    sqlSelectChecklistTools.AppendFormat(" where (ctool.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR ctool.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                //}

            }
            else
            {
                //Get pmchecklist
               // sqlSelectpmchecklist.Append(" SELECT * FROM pmchecklist pc");
                //Get checklistelements
                // sqlSelectchecklistelements.Append(" SELECT * FROM checklistelements cle");
                //Get checklistInv
                sqlSelectchecklistInv.Append(" SELECT * FROM checklistInv cinv");
                //Get ChecklistPPE
                sqlSelectChecklistPPE.Append(" SELECT * FROM ChecklistPPE cppe");
                //Get ChecklistTools
               // sqlSelectChecklistTools.Append(" SELECT * FROM ChecklistTools ctool");
            }


            // List<Checklistelement> resultchecklistelements = new List<Checklistelement>();
            List<PMCheckList> resultpmchecklist = new List<PMCheckList>();
            List<ChecklistInv> resultchecklistInv = new List<ChecklistInv>();
            List<ChecklistPPE> resultChecklistPPE = new List<ChecklistPPE>();
            List<ChecklistTool> resultChecklistTools = new List<ChecklistTool>();
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    //  resultchecklistelements = objContext.ExecuteQueryWithClientID<Checklistelement>(sqlSelectchecklistelements.ToString(), parameters, clientCode).Take(1000).ToList();
                    //resultpmchecklist = objContext.ExecuteQueryWithClientID<PMCheckList>(sqlSelectpmchecklist.ToString(), parameters, clientCode).Take(1000).ToList();
                    resultchecklistInv = objContext.ExecuteQueryWithClientID<ChecklistInv>(sqlSelectchecklistInv.ToString(), parameters, clientCode).ToList();
                    resultChecklistPPE = objContext.ExecuteQueryWithClientID<ChecklistPPE>(sqlSelectChecklistPPE.ToString(), parameters, clientCode).ToList();
                    //resultChecklistTools = objContext.ExecuteQueryWithClientID<ChecklistTool>(sqlSelectChecklistTools.ToString(), parameters, clientCode).Take(1000).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //var resultchecklistelements = DbContext.ObjectContext.ExecuteStoreQuery<checklistelementsModel>(sqlSelectchecklistelements.ToString()).ToList();
            //var resultchecklistInv = DbContext.ObjectContext.ExecuteStoreQuery<checklistInvModel>(sqlSelectchecklistInv.ToString()).ToList();
            //var resultChecklistPPE = DbContext.ObjectContext.ExecuteStoreQuery<ChecklistPPEModel>(sqlSelectChecklistPPE.ToString()).ToList();
            //var resultChecklistTools = DbContext.ObjectContext.ExecuteStoreQuery<ChecklistToolsModel>(sqlSelectChecklistTools.ToString()).ToList();
           // objWorkOrderMasterResult.pmchecklistTable = resultpmchecklist;
            //objWorkOrderMasterResult.checklistelementsTable = resultchecklistelements;
            objWorkOrderMasterResult.checklistInvTable = resultchecklistInv;
            objWorkOrderMasterResult.ChecklistPPETable = resultChecklistPPE;
           // objWorkOrderMasterResult.ChecklistToolsTable = resultChecklistTools;

            return objWorkOrderMasterResult;
        }


        /// <summary>
        /// To get Work Order Master ChecklistelementsTable
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkOrderMasterChecklistelementsTableResult GetWorkOrderMasterChecklistelementsTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkOrderMasterChecklistelementsTableResult objWorkOrderMasterResult = new GetWorkOrderMasterChecklistelementsTableResult();
            objWorkOrderMasterResult.PageIndex = pageIndex;
            objWorkOrderMasterResult.PageSize = pageSize;

            StringBuilder sqlSelectchecklistelements = new StringBuilder();

            if (currentTimeStamp != null)
            {
                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectchecklistelements.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                }

                //Get checklistelements                
                sqlSelectchecklistelements.Append(" SELECT * FROM checklistelements cle");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectchecklistelements.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectchecklistelements.AppendFormat(" OR (cle.CreatedDate >= @currentTimeStamp OR cle.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectchecklistelements.AppendFormat(" where (cle.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cle.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }
            }
            else
            {
                //Get checklistelements
                sqlSelectchecklistelements.Append(" SELECT * FROM checklistelements cle");
            }

            List<Checklistelement> resultchecklistelements = new List<Checklistelement>();
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    resultchecklistelements = objContext.ExecuteQueryWithClientID<Checklistelement>(sqlSelectchecklistelements.ToString(), parameters, clientCode).ToList();
                    objWorkOrderMasterResult.TotalCount = resultchecklistelements.Count;
                    if (resultchecklistelements.Count > 0)
                    {
                        objWorkOrderMasterResult.TotalPages = (resultchecklistelements.Count / pageSize);
                        if (resultchecklistelements.Count % pageSize != 0) objWorkOrderMasterResult.TotalPages++;

                        resultchecklistelements = objContext.GetPageList<Checklistelement>(resultchecklistelements, pageIndex, pageSize);
                    }
                    else
                    {
                        objWorkOrderMasterResult.TotalPages = 0;
                        resultchecklistelements = new List<Checklistelement>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            objWorkOrderMasterResult.checklistelementsTable = resultchecklistelements;
            return objWorkOrderMasterResult;
        }


        /// <summary>
        /// To get Work Order Master ChecklistToolsTable
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkOrderMasterChecklistToolsTableResult GetWorkOrderMasterChecklistToolsTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkOrderMasterChecklistToolsTableResult objWorkOrderMasterResult = new GetWorkOrderMasterChecklistToolsTableResult();
            objWorkOrderMasterResult.PageIndex = pageIndex;
            objWorkOrderMasterResult.PageSize = pageSize;

            StringBuilder sqlSelectChecklistTools = new StringBuilder();

            if (currentTimeStamp != null)
            {
                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectChecklistTools.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                }
                //Get ChecklistTools
                sqlSelectChecklistTools.Append(" SELECT * FROM ChecklistTools ctool");

                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectChecklistTools.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectChecklistTools.AppendFormat(" OR (ctool.CreatedDate >= @currentTimeStamp OR ctool.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectChecklistTools.AppendFormat(" where (ctool.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR ctool.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }

            }
            else
            {
                //Get ChecklistTools
                sqlSelectChecklistTools.Append(" SELECT * FROM ChecklistTools ctool");
            }

            List<ChecklistTool> resultChecklistTools = new List<ChecklistTool>();
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    resultChecklistTools = objContext.ExecuteQueryWithClientID<ChecklistTool>(sqlSelectChecklistTools.ToString(), parameters, clientCode).ToList();
                    objWorkOrderMasterResult.TotalCount = resultChecklistTools.Count;
                    if (resultChecklistTools.Count > 0)
                    {
                        objWorkOrderMasterResult.TotalPages = (resultChecklistTools.Count / pageSize);
                        if (resultChecklistTools.Count % pageSize != 0) objWorkOrderMasterResult.TotalPages++;
                        resultChecklistTools = objContext.GetPageList<ChecklistTool>(resultChecklistTools, pageIndex, pageSize);
                    }
                    else
                    {
                        objWorkOrderMasterResult.TotalPages = 0;
                        resultChecklistTools = new List<ChecklistTool>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            objWorkOrderMasterResult.ChecklistToolsTable = resultChecklistTools;

            return objWorkOrderMasterResult;
        }


        /// <summary>
        /// To get Work Order Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkOrderMasterPMChecklistTableResult GetWorkOrderMasterPMchecklistTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkOrderMasterPMChecklistTableResult objWorkOrderMasterResult = new GetWorkOrderMasterPMChecklistTableResult();
            objWorkOrderMasterResult.PageIndex = pageIndex;
            objWorkOrderMasterResult.PageSize = pageSize;

            StringBuilder sqlSelectpmchecklist = new StringBuilder();

            if (currentTimeStamp != null)
            {
                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectpmchecklist.AppendFormat(" DECLARE @currentTimeStamp DATETIME = CAST('{0}' AS DATETIME)", DBExecute.GetEnglishDateWithTime(currentTimeStamp));
                }

                sqlSelectpmchecklist.Append(" SELECT * FROM pmchecklist pc");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectpmchecklist.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectpmchecklist.AppendFormat(" OR (pc.CreatedDate >= @currentTimeStamp OR pc.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectpmchecklist.AppendFormat(" where (pc.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR pc.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }
            }
            else
            {
                sqlSelectpmchecklist.Append(" SELECT * FROM pmchecklist pc");
            }

            List<PMCheckList> resultpmchecklist = new List<PMCheckList>();
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    resultpmchecklist = objContext.ExecuteQueryWithClientID<PMCheckList>(sqlSelectpmchecklist.ToString(), parameters, clientCode).ToList();
                    objWorkOrderMasterResult.TotalCount = resultpmchecklist.Count;
                    if (resultpmchecklist.Count > 0)
                    {
                        objWorkOrderMasterResult.TotalPages = (resultpmchecklist.Count / pageSize);
                        if (resultpmchecklist.Count % pageSize != 0) objWorkOrderMasterResult.TotalPages++;
                        resultpmchecklist = objContext.GetPageList<PMCheckList>(resultpmchecklist, pageIndex, pageSize);
                    }
                    else
                    {
                        objWorkOrderMasterResult.TotalPages = 0;
                        resultpmchecklist = new List<PMCheckList>();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            objWorkOrderMasterResult.pmchecklistTable = resultpmchecklist;

            return objWorkOrderMasterResult;
        }

        /// <summary>
        /// To get Work Order Master
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetWorkOrderMasterStockcodeTableResult GetWorkOrderMasterStockcodeTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetWorkOrderMasterStockcodeTableResult objWorkOrderMasterResult = new GetWorkOrderMasterStockcodeTableResult();
            objWorkOrderMasterResult.PageIndex = pageIndex;
            objWorkOrderMasterResult.PageSize = pageSize;

            List<StockCode> querystockcode = new List<StockCode>();
            using (ServiceContext objContext = new ServiceContext(0))
            {
                querystockcode = objContext.SearchAllWithClientID<StockCode>(new StockCode(), clientCode).Where(s => (currentTimeStamp == null) || (s.CreatedDate >= currentTimeStamp.Value || s.ModifiedDate >= currentTimeStamp.Value)).ToList();
                objWorkOrderMasterResult.TotalCount = querystockcode.Count;
                if (querystockcode.Count > 0)
                {
                    objWorkOrderMasterResult.TotalPages = (querystockcode.Count / pageSize);
                    if (querystockcode.Count % pageSize != 0) objWorkOrderMasterResult.TotalPages++;
                    querystockcode = objContext.GetPageList<StockCode>(querystockcode, pageIndex, pageSize);
                }
                else
                {
                    objWorkOrderMasterResult.TotalPages = 0;
                    querystockcode = new List<StockCode>();
                }
            }
            objWorkOrderMasterResult.StockcodeTable = querystockcode;
            return objWorkOrderMasterResult;
        }

        /// <summary>
        /// Get Existing Work Order MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkOrderMasterIDsResult GetExistingWorkOrdertMasterIDs(string clientCode)
        {
            GetExistingWorkOrderMasterIDsResult objExistingWorkOrderMasterIDsResult = new GetExistingWorkOrderMasterIDsResult();

            using (ServiceContext objContext = new ServiceContext(0))
            {
                objExistingWorkOrderMasterIDsResult.lstFailureCauseID = objContext.SearchAllWithClientID<Failurecause>(new Failurecause(), clientCode).Select(s => s.FailureCauseID).ToList();

                objExistingWorkOrderMasterIDsResult.lstRateID = objContext.SearchAllWithClientID<Rating>(new Rating(), clientCode).Select(s => s.RateID).ToList();

                objExistingWorkOrderMasterIDsResult.lstCostCenterId = objContext.SearchAllWithClientID<CostCenter>(new CostCenter(), clientCode).Select(s => s.CostCenterId).ToList();

                //objExistingWorkOrderMasterIDsResult.lstStockID = objContext.SearchAllWithClientID<StockCode>(new StockCode(), clientCode).Select(s => s.StockID).ToList();

                objExistingWorkOrderMasterIDsResult.lstAccCodeid = objContext.SearchAllWithClientID<Accountcode>(new Accountcode(), clientCode).Select(s => s.AccCodeid).ToList();

                objExistingWorkOrderMasterIDsResult.lstSubStoreID = objContext.SearchAllWithClientID<Substore>(new Substore(), clientCode).Select(s => s.SubStoreID).ToList();

                objExistingWorkOrderMasterIDsResult.lstType_Pm_GensID = objContext.SearchAllWithClientID<Type_pm_gen>(new Type_pm_gen(), clientCode).Select(s => s.Id).ToList();

                objExistingWorkOrderMasterIDsResult.lstUserGroupId = objContext.SearchAllWithClientID<Usergroup>(new Usergroup(), clientCode).Select(s => s.UserGroupId).ToList();
            }

            return objExistingWorkOrderMasterIDsResult;
        }

        /// <summary>
        /// Get Existing Work Order MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingWorkOrderMasterIDsLstStockIDResult GetExistingWorkOrderMasterIDsLstStockID(string clientCode, int pageIndex, int pageSize)
        {
            GetExistingWorkOrderMasterIDsLstStockIDResult objExistingWorkOrderMasterIDsResult = new GetExistingWorkOrderMasterIDsLstStockIDResult();
            objExistingWorkOrderMasterIDsResult.PageIndex = pageIndex;
            objExistingWorkOrderMasterIDsResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {
                List<int> lst = objContext.SearchAllWithClientID<StockCode>(new StockCode(), clientCode).Select(s => s.StockID).ToList();
                objExistingWorkOrderMasterIDsResult.TotalCount = lst.Count;

                if (lst.Count > 0)
                {
                    objExistingWorkOrderMasterIDsResult.TotalPages = (lst.Count / pageSize);
                    if (lst.Count % pageSize != 0) objExistingWorkOrderMasterIDsResult.TotalPages++;
                    lst = objContext.GetPageList<int>(lst, pageIndex, pageSize);
                }
                else
                {
                    objExistingWorkOrderMasterIDsResult.TotalPages = 0;
                    lst = new List<int>();
                }
                objExistingWorkOrderMasterIDsResult.lstStockID = lst;
            }
            return objExistingWorkOrderMasterIDsResult;
        }

        
        /// <summary>
        /// Get Project No Masters
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetProjectNoMasterResult GetProjectNoMaster(string strCurrentTimeStamp, string clientCode)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);

            GetProjectNoMasterResult objProjectNoMasterResult = new GetProjectNoMasterResult();
            StringBuilder sqlSelectcmApprovedProjectRequest = new StringBuilder();
            StringBuilder sqlSelectcmprojectrequest = new StringBuilder();
            StringBuilder sqlSelectcmprojectsite = new StringBuilder();
            StringBuilder sqlSelectcmProjectStatus = new StringBuilder();
            //StringBuilder sqlSelectcmProjectPerformanceEvaluation = new StringBuilder();

            if (currentTimeStamp != null)
            {

                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectcmApprovedProjectRequest.AppendFormat(" DECLARE @currentTimeStamp DATE = CAST('{0}' AS DATE)", DBExecute.GetEnglishDateWithoutTime(currentTimeStamp));
                    sqlSelectcmprojectrequest.AppendFormat(" DECLARE @currentTimeStamp DATE = CAST('{0}' AS DATE)", DBExecute.GetEnglishDateWithoutTime(currentTimeStamp));
                    sqlSelectcmProjectStatus.AppendFormat(" DECLARE @currentTimeStamp DATE = CAST('{0}' AS DATE)", DBExecute.GetEnglishDateWithoutTime(currentTimeStamp));
                    //sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" DECLARE @currentTimeStamp DATE = CAST('{0}' AS DATE)", DBExecute.GetEnglishDateWithoutTime(currentTimeStamp));
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                }

                //Get cmApprovedProjectRequest                
                sqlSelectcmApprovedProjectRequest.Append(" SELECT * FROM cmApprovedProjectRequest capr");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectcmApprovedProjectRequest.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectcmApprovedProjectRequest.AppendFormat(" OR (capr.CreatedDate >= @currentTimeStamp OR capr.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectcmApprovedProjectRequest.AppendFormat(" where (capr.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR capr.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }


                //Get cmprojectrequest
                sqlSelectcmprojectrequest.Append(" SELECT * FROM cmprojectrequest cpr");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectcmprojectrequest.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectcmprojectrequest.AppendFormat(" OR (cpr.CreatedDate >= @currentTimeStamp OR cpr.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectcmprojectrequest.AppendFormat(" where (cpr.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cpr.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }

                //Get cmProjectStatus
                sqlSelectcmProjectStatus.Append(" SELECT * FROM cmProjectStatus cps");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectcmProjectStatus.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectcmProjectStatus.AppendFormat(" OR (cps.CreatedDate >= @currentTimeStamp OR cps.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectcmProjectStatus.AppendFormat(" where (cps.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cps.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }

                //Get cmprojectsite
                sqlSelectcmprojectsite.Append(" SELECT * FROM cmprojectsite cpsi");

                ////Get cmProjectPerformanceEvaluation
                //sqlSelectcmProjectPerformanceEvaluation.Append(" SELECT * FROM cmProjectPerformanceEvaluation cppe");
                //if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                //{
                //    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" where @currentTimeStamp = null ");
                //    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" OR (cppe.CreatedDate >= @currentTimeStamp OR cppe.ModifiedDate >= @currentTimeStamp) ");
                //}
                //else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                //{
                //    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" where (cppe.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cppe.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                //}

            }
            else
            {
                //Get cmApprovedProjectRequest
                sqlSelectcmApprovedProjectRequest.Append(" SELECT * FROM cmApprovedProjectRequest capr");
                //Get cmprojectrequest
                sqlSelectcmprojectrequest.Append(" SELECT * FROM cmprojectrequest cpr");
                //Get cmProjectStatus
                sqlSelectcmProjectStatus.Append(" SELECT * FROM cmProjectStatus cps");
                //Get cmprojectsite
                sqlSelectcmprojectsite.Append(" SELECT * FROM cmprojectsite cpsi");
                //Get cmProjectPerformanceEvaluation
                //sqlSelectcmProjectPerformanceEvaluation.Append(" SELECT * FROM cmProjectPerformanceEvaluation cppe");
            }

            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objProjectNoMasterResult.cmApprovedProjectRequestTable = objContext.ExecuteQueryWithClientID<CmApprovedProjectRequest>(sqlSelectcmApprovedProjectRequest.ToString(), parameters, clientCode).ToList();

                    objProjectNoMasterResult.cmprojectrequestTable = objContext.ExecuteQueryWithClientID<Cmprojectrequest>(sqlSelectcmprojectrequest.ToString(), parameters, clientCode).ToList();

                    objProjectNoMasterResult.cmprojectsiteTable = objContext.ExecuteQueryWithClientID<Cmprojectsite>(sqlSelectcmprojectsite.ToString(), parameters, clientCode).ToList();

                    objProjectNoMasterResult.cmProjectStatusTable = objContext.ExecuteQueryWithClientID<CmProjectStatu>(sqlSelectcmProjectStatus.ToString(), parameters, clientCode).ToList();

                    //objProjectNoMasterResult.cmProjPerformanceEvalutionTable = objContext.ExecuteQueryWithClientID<CmProjectPerformanceEvaluation>(sqlSelectcmProjectPerformanceEvaluation.ToString(), parameters, clientCode).ToList();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


            //var resultcmApprovedProjectRequest = DbContext.ObjectContext.ExecuteStoreQuery<cmApprovedProjectRequestModel>(sqlSelectcmApprovedProjectRequest.ToString()).ToList();
            //var resultcmprojectrequest = DbContext.ObjectContext.ExecuteStoreQuery<cmprojectrequestModel>(sqlSelectcmprojectrequest.ToString()).ToList();
            //var resultcmprojectsite = DbContext.ObjectContext.ExecuteStoreQuery<cmprojectsiteModel>(sqlSelectcmprojectsite.ToString()).ToList();
            //var resultcmProjectStatus = DbContext.ObjectContext.ExecuteStoreQuery<cmProjectStatusModel>(sqlSelectcmProjectStatus.ToString()).ToList();
            //var resultcmProjectPerformanceEvaluation = DbContext.ObjectContext.ExecuteStoreQuery<cmProjPerformanceEvalutionModel>(sqlSelectcmProjectPerformanceEvaluation.ToString()).ToList();
            //objProjectNoMasterResult.cmApprovedProjectRequestTable = resultcmApprovedProjectRequest;
            // objProjectNoMasterResult.cmprojectrequestTable = resultcmprojectrequest;
            //objProjectNoMasterResult.cmprojectsiteTable = resultcmprojectsite;
            //objProjectNoMasterResult.cmProjectStatusTable = resultcmProjectStatus;
            //objProjectNoMasterResult.cmProjPerformanceEvalutionTable = resultcmProjectPerformanceEvaluation;

            return objProjectNoMasterResult;

        }


        /// <summary>
        /// Get Project No Masters
        /// </summary>
        /// <param name="currentTimeStamp"></param>
        /// <returns></returns>
        public GetProjectNoMasterCMProjectPerformanceEvaluationResult GetProjectNoMasterCMProjPerformanceEvalutionTable(string strCurrentTimeStamp, string clientCode, int pageIndex, int pageSize)
        {
            DateTime? currentTimeStamp = DBExecute.ConvertStringToDataTime(strCurrentTimeStamp);
            GetProjectNoMasterCMProjectPerformanceEvaluationResult objProjectNoMasterResult = new GetProjectNoMasterCMProjectPerformanceEvaluationResult();
            objProjectNoMasterResult.PageIndex = pageIndex;
            objProjectNoMasterResult.PageSize = pageSize;
            StringBuilder sqlSelectcmProjectPerformanceEvaluation = new StringBuilder();

            if (currentTimeStamp != null)
            {
                string strTime = Convert.ToDateTime(currentTimeStamp).ToString("dd/MMM/yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
                Account objAccount = GetAccountType(clientCode);
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {                    
                    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" DECLARE @currentTimeStamp DATE = CAST('{0}' AS DATE)", DBExecute.GetEnglishDateWithoutTime(currentTimeStamp));
                }     
                
                //Get cmProjectPerformanceEvaluation
                sqlSelectcmProjectPerformanceEvaluation.Append(" SELECT * FROM cmProjectPerformanceEvaluation cppe");
                if (objAccount.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" where @currentTimeStamp = null ");
                    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" OR (cppe.CreatedDate >= @currentTimeStamp OR cppe.ModifiedDate >= @currentTimeStamp) ");
                }
                else if (objAccount.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlSelectcmProjectPerformanceEvaluation.AppendFormat(" where (cppe.CreatedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss') OR cppe.ModifiedDate >= TO_DATE('" + strTime + "', 'dd/mon/yyyy hh24:mi:ss')) ");
                }
            }
            else
            {                
                sqlSelectcmProjectPerformanceEvaluation.Append(" SELECT * FROM cmProjectPerformanceEvaluation cppe");
            }

            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();
                using (ServiceContext objContext = new ServiceContext(0))
                {                    
                    List<CmProjectPerformanceEvaluation> lst = objContext.ExecuteQueryWithClientID<CmProjectPerformanceEvaluation>(sqlSelectcmProjectPerformanceEvaluation.ToString(), parameters, clientCode).ToList();
                    objProjectNoMasterResult.TotalCount = lst.Count;
                    if (lst.Count > 0)
                    {
                        objProjectNoMasterResult.TotalPages = (lst.Count / pageSize);
                        if (lst.Count % pageSize != 0) objProjectNoMasterResult.TotalPages++;
                        lst = objContext.GetPageList<CmProjectPerformanceEvaluation>(lst, pageIndex, pageSize);
                    }
                    else
                    {
                        objProjectNoMasterResult.TotalPages = 0;
                        lst = new List<CmProjectPerformanceEvaluation>();
                    }
                    objProjectNoMasterResult.cmProjPerformanceEvalutionTable = lst;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objProjectNoMasterResult;
        }

        
        /// <summary>
        /// Get Existing Project No MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingProjectNoMasterIdsResult GetExistingProjectNoMasterIds(string clientCode)
        {
            GetExistingProjectNoMasterIdsResult objExistingProjectNoMasterIDsResult = new GetExistingProjectNoMasterIdsResult();

            using (ServiceContext objContext = new ServiceContext(0))
            {
                objExistingProjectNoMasterIDsResult.lstProjectNumber = objContext.SearchAllWithClientID<CmApprovedProjectRequest>(new CmApprovedProjectRequest(), clientCode).Select(s => s.ProjectNumber).ToList();

                objExistingProjectNoMasterIDsResult.lstProjectReqNo = objContext.SearchAllWithClientID<Cmprojectrequest>(new Cmprojectrequest(), clientCode).Select(s => s.ProjectReqNo).ToList();

                objExistingProjectNoMasterIDsResult.lstcmprojectsiteIds = objContext.SearchAllWithClientID<Cmprojectsite>(new Cmprojectsite(), clientCode).Select(s => s.Id).ToList();

                objExistingProjectNoMasterIDsResult.lstProjectStatusIds = objContext.SearchAllWithClientID<CmProjectStatu>(new CmProjectStatu(), clientCode).Select(s => s.ProjectStatusId).ToList();

                //objExistingProjectNoMasterIDsResult.lstcmProjectPerformanceEvaluationIds = objContext.SearchAllWithClientID<CmProjectPerformanceEvaluation>(new CmProjectPerformanceEvaluation(), clientCode).Select(s => s.ID).ToList();
            }

            //objExistingProjectNoMasterIDsResult.lstProjectNumber = DbContext.cmApprovedProjectRequests.Select(s => s.ProjectNumber).ToList();
            //objExistingProjectNoMasterIDsResult.lstProjectReqNo = DbContext.cmprojectrequests.Select(s => s.ProjectReqNo).ToList();
            //objExistingProjectNoMasterIDsResult.lstcmprojectsiteIds = DbContext.cmprojectsites.Select(s => s.Id).ToList();
            //objExistingProjectNoMasterIDsResult.lstProjectStatusIds = DbContext.cmProjectStatus.Select(s => s.ProjectStatusId).ToList();
            //objExistingProjectNoMasterIDsResult.lstcmProjectPerformanceEvaluationIds = DbContext.cmProjectPerformanceEvaluations.Select(s => s.ID).ToList();

            return objExistingProjectNoMasterIDsResult;
        }




        /// <summary>
        /// Get Existing Project No MasterIDs
        /// </summary>
        /// <returns></returns>
        public GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIds(string clientCode, int pageIndex, int pageSize)
        {
            GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult objExistingProjectNoMasterIDsResult = new GetExistingProjectNoMasterIdsLstCMProjectPerformanceEvaluationIdsResult();
            objExistingProjectNoMasterIDsResult.PageIndex = pageIndex;
            objExistingProjectNoMasterIDsResult.PageSize = pageSize;

            using (ServiceContext objContext = new ServiceContext(0))
            {       
                List<int> lst = objContext.SearchAllWithClientID<CmProjectPerformanceEvaluation>(new CmProjectPerformanceEvaluation(), clientCode).Select(s => s.ID).ToList();
                objExistingProjectNoMasterIDsResult.TotalCount = lst.Count;

                if (lst.Count > 0)
                {
                    objExistingProjectNoMasterIDsResult.TotalPages = (lst.Count / pageSize);
                    if (lst.Count % pageSize != 0) objExistingProjectNoMasterIDsResult.TotalPages++;
                    lst = objContext.GetPageList<int>(lst, pageIndex, pageSize);
                }
                else
                {
                    objExistingProjectNoMasterIDsResult.TotalPages = 0;
                    lst = new List<int>();
                }

                objExistingProjectNoMasterIDsResult.lstcmProjectPerformanceEvaluationIds = lst;
                    
            }

            //objExistingProjectNoMasterIDsResult.lstProjectNumber = DbContext.cmApprovedProjectRequests.Select(s => s.ProjectNumber).ToList();
            //objExistingProjectNoMasterIDsResult.lstProjectReqNo = DbContext.cmprojectrequests.Select(s => s.ProjectReqNo).ToList();
            //objExistingProjectNoMasterIDsResult.lstcmprojectsiteIds = DbContext.cmprojectsites.Select(s => s.Id).ToList();
            //objExistingProjectNoMasterIDsResult.lstProjectStatusIds = DbContext.cmProjectStatus.Select(s => s.ProjectStatusId).ToList();
            //objExistingProjectNoMasterIDsResult.lstcmProjectPerformanceEvaluationIds = DbContext.cmProjectPerformanceEvaluations.Select(s => s.ID).ToList();

            return objExistingProjectNoMasterIDsResult;
        }

        
        /// <summary>
        /// Add/Update/Delete Work order        
        /// </summary>
        /// <param name="model"></param>
        /// <param name="attachmentPath"></param>
        /// <returns></returns>
        public AddUpdateWorkOrderResult AddUpdateWorkOrder(WorkOrder model, string attachmentPath, string clientCode)
        {
            var objAddUpdateWorkOrder = new AddUpdateWorkOrderResult();
            //Check For User is Active or Not
            EmployeeDetailService objEmpDetailService = new EmployeeDetailService();
            Account objAC = EmployeeDetailService.GetAccountType(clientCode);
            attachmentPath = objAC.SharedFolderPath + "\\" + clientCode + "\\";
            if (!objEmpDetailService.IsUserActive(model.LoggedInEmployeeID, clientCode))
            {
                objAddUpdateWorkOrder.IsUserActive = 0;
                return objAddUpdateWorkOrder;
            }
            else
            {
                objAddUpdateWorkOrder.IsUserActive = 1;
            }
            //Check For WorkOrder Status Close/Cancel

            int? WorkOrderStatus;
            using (ServiceContext objContext = new ServiceContext(0))
            {
                WorkOrderStatus = objContext.SearchAllWithClientID<WorkOrder>(new WorkOrder(), clientCode).Where(w => w.WorkorderNo == model.WorkorderNo).Select(s => s.WorkStatusID).FirstOrDefault();
            }

            if (WorkOrderStatus != null && (WorkOrderStatus == 2 || WorkOrderStatus == 3))
            {
                objAddUpdateWorkOrder.IsStatusCloseAtServer = 1;
                return objAddUpdateWorkOrder;
            }
            //using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            //{
            #region For Add/Update/Delete Basic Info
            if (!string.IsNullOrEmpty(model.WorkorderNo))
            {
                //DbContext.workorders.Where(w => w.WorkorderNo == model.WorkorderNo).Update(s => new workorder
                //{
                WorkOrder objWorkOrder = new WorkOrder
                {
                    WorkorderNo = model.WorkorderNo,
                    RequestorID = model.RequestorID,
                    EmployeeID = model.EmployeeID,
                    L2ID = model.L2ID,
                    L3ID = model.L3ID,
                    L4ID = model.L4ID,
                    L5ID = model.L5ID,
                    LocationID = model.LocationID,
                    AssetID = model.AssetID,
                    WorkStatusID = model.WorkStatusID,
                    WorkPriorityID = model.WorkPriorityID,
                    FailureCauseID = model.FailureCauseID,
                    WorkTypeID = model.WorkTypeID,
                    WOTradeID = model.WOTradeID,
                    MeterID = model.MeterID,
                    PMID = model.PMID,
                    MaintDivisionID = model.MaintDivisionID,
                    MaintdeptID = model.MaintdeptID,
                    MaintsubdeptID = model.MaintsubdeptID,
                    GroupID = model.GroupID,
                    SupplierId = model.SupplierId,
                    AcceptedbyID = model.AcceptedbyID,
                    PMChecklistID = model.PMChecklistID,
                    RatingsID = model.RatingsID,
                    WOclosebyID = model.WOclosebyID,
                    CostCenterId = model.CostCenterId,
                    ProblemDescription = model.ProblemDescription,
                    DateReceived = model.DateReceived,
                    EstDateStart = model.EstDateStart,
                    EstDateEnd = model.EstDateEnd,
                    ActDateStart = model.ActDateStart,
                    ActDateEnd = model.ActDateEnd,
                    DateRequired = model.DateRequired,
                    DateHandover = model.DateHandover,
                    EstDuration = model.EstDuration,
                    ActionTaken = model.ActionTaken,
                    CauseDescription = model.CauseDescription,
                    PreventionTaken = model.PreventionTaken,
                    WOCost = model.WOCost,
                    PMTarStartDate = model.PMTarStartDate,
                    PMTarCompDate = model.PMTarCompDate,
                    AstartDate = model.AstartDate,
                    AEndDate = model.AEndDate,
                    WOLaborCost = model.WOLaborCost,
                    WODICost = model.WODICost,
                    WOPartCost = model.WOPartCost,
                    WOPMtype = model.WOPMtype,
                    PMJOGeneratedReading = model.PMJOGeneratedReading,
                    PMMetReading = model.PMMetReading,
                    DownTime = model.DownTime,
                    LineDowntime = model.LineDowntime,
                    MTask = model.MTask,
                    Notes = model.Notes,
                    MPMID = model.MPMID,
                    Print_date = model.Print_date,
                    AssignTo = model.AssignTo,
                    TelNo = model.TelNo,
                    Accountcodeid = model.Accountcodeid,
                    Documentstatusid = model.Documentstatusid,
                    //CreatedBy = model.CreatedBy,
                    //CreatedDate = model.CreatedDate,
                    ModifiedBy = model.ModifiedBy,
                    ModifiedDate = System.DateTime.Now,
                    AuthorisedEmployeeId = model.AuthorisedEmployeeId,
                    CIGID = model.CIGID,
                    IsCleaningModule = model.IsCleaningModule,
                    ProjectNumber = model.ProjectNumber
                };
                //DbContext.SaveChanges();

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objContext.UpdateWithClientID<WorkOrder>(objWorkOrder, "  WorkorderNo = '" + model.WorkorderNo + "'", clientCode);
                }

            }
            #endregion

            #region For Add/Update/Delete Direct Issue
            if (model.DirectTable != null)
            {
                var directsAddList = model.DirectTable.Where(x => x.FlagForAddUpdateDelete == 1).ToList();

                foreach (var directModel in directsAddList)
                {
                    Direct direct = new Direct();
                    direct.L2ID = directModel.L2ID;
                    direct.WONo = directModel.WONo;
                    direct.Supplierid = directModel.Supplierid;
                    direct.PRNo = directModel.PRNo;
                    direct.StockDesc = directModel.StockDesc;
                    direct.AltStockDesc = directModel.AltStockDesc;
                    direct.Date = directModel.Date;
                    direct.Qty = directModel.Qty;
                    direct.Price = directModel.Price;
                    direct.Invoicenumber = directModel.Invoicenumber;
                    direct.CreatedBy = directModel.CreatedBy;
                    direct.CreatedDate = System.DateTime.Now;
                    //direct.ModifiedBy = directModel.ModifiedBy;
                    //direct.ModifiedDate = directModel.ModifiedDate;
                    //DbContext.directs.Add(direct);
                    //DbContext.SaveChanges();

                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.SaveWithClientID<Direct>(direct, clientCode, true);
                    }

                    directModel.DirectID = direct.DirectID;
                }

                var directsDeleteList = model.DirectTable.Where(x => x.FlagForAddUpdateDelete == 3);
                foreach (var directD in directsDeleteList)
                {
                    //DbContext.directs.Delete(d => d.DirectID == directD.DirectID);

                    string strDirectForWRQuery = "delete FROM Direct WHERE DirectID = " + directD.DirectID + "";
                    try
                    {
                        Collection<DBParameters> parameters = new Collection<DBParameters>();
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.ExecuteQueryWithClientID<Direct>(strDirectForWRQuery, parameters, clientCode).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                //DbContext.SaveChanges();
                model.DirectTable = directsAddList;
            }
            #endregion

            #region For Add/Update/Delete Issues Item
            if (model.IssueTable != null)
            {
                var issueList = model.IssueTable.Where(x => x.FlagForAddUpdateDelete == 1).ToList();

                foreach (var issueModel in issueList)
                {
                    Issue issue = new Issue();
                    issue.StockID = issueModel.StockID;
                    issue.LocationID = issueModel.LocationID;
                    issue.L2ID = issueModel.L2ID;
                    issue.SubStoreID = issueModel.SubStoreID;
                    issue.ToSubStoreID = issueModel.ToSubStoreID;
                    issue.ToL2ID = issueModel.ToL2ID;
                    issue.WONo = issueModel.WONo;
                    issue.Description = issueModel.Description;
                    issue.DateIssue = issueModel.DateIssue;
                    issue.IWTNo = issueModel.IWTNo;
                    issue.Warehouse = issueModel.Warehouse;
                    issue.UoM = issueModel.UoM;
                    issue.QtyIssue = issueModel.QtyIssue;
                    issue.Price3 = issueModel.Price3;
                    issue.BatchNo = issueModel.BatchNo;
                    issue.IsTransfer = issueModel.IsTransfer;
                    issue.CreatedBy = issueModel.CreatedBy;
                    issue.CreatedDate = System.DateTime.Now;
                    //issue.ModifiedBy = issueModel.ModifiedBy;
                    //issue.ModifiedDate = issueModel.ModifiedDate;
                    //DbContext.issues.Add(issue);
                    //DbContext.SaveChanges();

                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        List<Issue> lstIssue = objContext.SaveWithClientID<Issue>(issue, clientCode, true).ToList();
                        issue.IssueID = lstIssue[0].IssueID;
                    }

                    issueModel.IssueID = issue.IssueID;
                }
                model.IssueTable = issueList;
            }

            #endregion

            #region For Add/Update/Delete Issue - Return Item
            if (model.ReturnTable != null)
            {
                var returnList = model.ReturnTable.Where(x => x.FlagForAddUpdateDelete == 1).ToList();
                foreach (var returnModel in returnList)
                {
                    Return returnItm = new Return();
                    returnItm.L2ID = returnModel.L2ID;
                    returnItm.StockID = returnModel.StockID;
                    returnItm.LocationID = returnModel.LocationID;
                    returnItm.SubStoreID = returnModel.SubStoreID;
                    returnItm.WONo = returnModel.WONo;
                    returnItm.Qty = returnModel.Qty;
                    returnItm.Date = returnModel.Date;
                    returnItm.Person = returnModel.Person;
                    returnItm.Comment = returnModel.Comment;
                    returnItm.RetPrice = returnModel.RetPrice;
                    returnItm.BatchNo = returnModel.BatchNo;
                    returnItm.CreatedBy = returnModel.CreatedBy;
                    returnItm.CreatedDate = System.DateTime.Now;
                    //returnItm.ModifiedBy = returnModel.ModifiedBy;
                    //returnItm.ModifiedDate = returnModel.ModifiedDate;
                    //DbContext.returns.Add(returnItm);
                    //DbContext.SaveChanges();

                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        List<Return> lstIssue = objContext.SaveWithClientID<Return>(returnItm, clientCode, true).ToList();
                        returnItm.ReturnID = lstIssue[0].ReturnID;
                    }

                    returnModel.ReturnID = returnItm.ReturnID;
                }
                model.ReturnTable = returnList;
            }
            #endregion

            #region For Add/Update PM-Checklist

            #region For Deleting records from PM-Checklist

            if (model.FlagForPMCheckList == 2)
            {
                //DbContext.workorderelements.Where(d => d.WorkorderNo == model.WorkorderNo).Delete();
                //DbContext.WorkOrderItems.Where(d => d.WorkOrderNo == model.WorkorderNo).Delete();
                //DbContext.WorkOrderTools.Where(d => d.WorkOrderNo == model.WorkorderNo).Delete();
                //DbContext.WorkOrderPPEs.Where(d => d.WorkOrderNo == model.WorkorderNo).Delete();
                //DbContext.SaveChanges();



                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        string strQuery = "delete FROM workorderelements WHERE WorkorderNo = '" + model.WorkorderNo + "'";
                        objContext.ExecuteQueryWithClientID<Workorderelement>(strQuery, parameters, clientCode).ToList();

                        strQuery = "delete FROM WorkOrderItems WHERE WorkOrderNo = '" + model.WorkorderNo + "'";
                        objContext.ExecuteQueryWithClientID<WorkOrderItem>(strQuery, parameters, clientCode).ToList();

                        strQuery = "delete FROM WorkOrderTools WHERE WorkOrderNo = '" + model.WorkorderNo + "'";
                        objContext.ExecuteQueryWithClientID<WorkOrderTool>(strQuery, parameters, clientCode).ToList();

                        strQuery = "delete FROM WorkOrderPPE WHERE WorkOrderNo = '" + model.WorkorderNo + "'";
                        objContext.ExecuteQueryWithClientID<WorkOrderPPE>(strQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }

            #endregion

            #region Add-Update in WorkOrderElementTable

            if (model.WorkOrderElementTable != null)
            {
                //For New Added Items - Insert into DB
                List<Workorderelement> lstWorkOrderElementAdd = model.WorkOrderElementTable.Where(woe => woe.FlagForAddUpdateDelete == 1).ToList();
                if (lstWorkOrderElementAdd != null && lstWorkOrderElementAdd.Count > 0)
                {
                    foreach (var item in lstWorkOrderElementAdd)
                    {
                        Workorderelement Woelement = new Workorderelement();
                        Woelement.WorkorderNo = item.WorkorderNo;
                        Woelement.SeqNo = item.SeqNo;
                        Woelement.TaskDesc = item.TaskDesc;
                        Woelement.AltTaskDesc = item.AltTaskDesc;
                        Woelement.Remarks = item.Remarks;
                        Woelement.CreatedBy = item.CreatedBy;
                        Woelement.CreatedDate = System.DateTime.Now;
                        //Woelement.ModifiedBy = item.ModifiedBy;
                        //Woelement.ModifiedDate = item.ModifiedDate;
                        //DbContext.workorderelements.Add(Woelement);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.SaveWithClientID<Workorderelement>(Woelement, clientCode, true);
                        }
                    }
                    model.WorkOrderElementTable = lstWorkOrderElementAdd;
                }

                //For Updated Items-Delete from table first and then Insert into DB
                List<Workorderelement> lstWorkOrderElementUpdate = model.WorkOrderElementTable.Where(woe => woe.FlagForAddUpdateDelete == 2).ToList();
                if (lstWorkOrderElementUpdate != null && lstWorkOrderElementUpdate.Count > 0)
                {
                    foreach (var item in lstWorkOrderElementUpdate)
                    {
                        Workorderelement Woelement = new Workorderelement();
                        Woelement.WorkorderNo = item.WorkorderNo;
                        Woelement.SeqNo = item.SeqNo;
                        Woelement.TaskDesc = item.TaskDesc;
                        Woelement.AltTaskDesc = item.AltTaskDesc;
                        Woelement.Remarks = item.Remarks;
                        Woelement.CreatedBy = item.CreatedBy;
                        Woelement.CreatedDate = System.DateTime.Now;
                        //Woelement.ModifiedBy = item.ModifiedBy;
                        //Woelement.ModifiedDate = item.ModifiedDate;
                        //DbContext.workorderelements.Add(Woelement);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.SaveWithClientID<Workorderelement>(Woelement, clientCode);
                        }


                    }
                    model.WorkOrderElementTable = lstWorkOrderElementUpdate;
                }
            }

            #endregion

            #region Add-Update in WorkOrderItemTable

            if (model.WorkOrderItemTable != null)
            {
                //For New Added Items - Insert into DB
                List<WorkOrderItem> lstWorkOrderItemAdd = model.WorkOrderItemTable.Where(woi => woi.FlagForAddUpdateDelete == 1).ToList();
                if (lstWorkOrderItemAdd != null && lstWorkOrderItemAdd.Count > 0)
                {
                    foreach (var item in lstWorkOrderItemAdd)
                    {
                        WorkOrderItem Woitem = new WorkOrderItem();
                        Woitem.StockID = item.StockID;
                        Woitem.WorkOrderNo = item.WorkOrderNo;
                        Woitem.SeqNo = item.SeqNo;
                        Woitem.Qty = item.Qty;
                        //DbContext.WorkOrderItems.Add(Woitem);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            objContext.SaveWithClientID<WorkOrderItem>(Woitem, clientCode, true);
                        }

                    }
                    model.WorkOrderItemTable = lstWorkOrderItemAdd;
                }

                //For Updated Items-Delete from table first and then Insert into DB
                List<WorkOrderItem> lstWorkOrderItemUpdate = model.WorkOrderItemTable.Where(woi => woi.FlagForAddUpdateDelete == 2).ToList();
                if (lstWorkOrderItemUpdate != null && lstWorkOrderItemUpdate.Count > 0)
                {
                    foreach (var item in lstWorkOrderItemUpdate)
                    {
                        WorkOrderItem Woitem = new WorkOrderItem();
                        Woitem.StockID = item.StockID;
                        Woitem.WorkOrderNo = item.WorkOrderNo;
                        Woitem.SeqNo = item.SeqNo;
                        Woitem.Qty = item.Qty;
                        //DbContext.WorkOrderItems.Add(Woitem);
                        //DbContext.SaveChanges();

                        string strQuery = "Update WorkOrderItems set Qty=" + item.Qty + "  WHERE StockID = " + item.StockID + " and WorkOrderNo='" + item.WorkOrderNo + "' and SeqNo=" + item.SeqNo;
                        try
                        {
                            Collection<DBParameters> parameters = new Collection<DBParameters>();
                            using (ServiceContext objContext = new ServiceContext(0))
                            {
                                objContext.ExecuteQueryWithClientID<WorkOrderItem>(strQuery, parameters, clientCode).ToList();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }


                    }
                    model.WorkOrderItemTable = lstWorkOrderItemUpdate;
                }
            }

            #endregion

            #region Add-Update in WorkOrderToolTable

            if (model.WorkOrderToolTable != null)
            {
                //For New Added Items - Insert into DB
                List<WorkOrderTool> lstWorkOrderToolAdd = model.WorkOrderToolTable.Where(wot => wot.FlagForAddUpdateDelete == 1).ToList();
                if (lstWorkOrderToolAdd != null && lstWorkOrderToolAdd.Count > 0)
                {
                    foreach (var item in lstWorkOrderToolAdd)
                    {
                        WorkOrderTool Wotool = new WorkOrderTool();
                        Wotool.WorkOrderNo = item.WorkOrderNo;
                        Wotool.SeqNo = item.SeqNo;
                        Wotool.ToolDescription = item.ToolDescription;
                        Wotool.AltToolDescription = item.AltToolDescription;
                        Wotool.Quantity = item.Quantity;
                        //DbContext.WorkOrderTools.Add(Wotool);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<WorkOrderTool> lst = objContext.SaveWithClientID<WorkOrderTool>(Wotool, clientCode, true).ToList();
                            Wotool.WorkOrderToolID = lst[0].WorkOrderToolID;
                        }

                        item.WorkOrderToolID = Wotool.WorkOrderToolID;
                    }
                    model.WorkOrderToolTable = lstWorkOrderToolAdd;
                }

                //For Updated Items-Delete from table first and then Insert into DB
                List<WorkOrderTool> lstWorkOrderToolUpdate = model.WorkOrderToolTable.Where(wot => wot.FlagForAddUpdateDelete == 2).ToList();
                if (lstWorkOrderToolUpdate != null && lstWorkOrderToolUpdate.Count > 0)
                {
                    foreach (var item in lstWorkOrderToolUpdate)
                    {
                        WorkOrderTool Wotool = new WorkOrderTool();
                        Wotool.WorkOrderNo = item.WorkOrderNo;
                        Wotool.SeqNo = item.SeqNo;
                        Wotool.ToolDescription = item.ToolDescription;
                        Wotool.AltToolDescription = item.AltToolDescription;
                        Wotool.Quantity = item.Quantity;
                        //DbContext.WorkOrderTools.Add(Wotool);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<WorkOrderTool> lst = objContext.SaveWithClientID<WorkOrderTool>(Wotool, clientCode, true).ToList();
                            Wotool.WorkOrderToolID = lst[0].WorkOrderToolID;
                        }

                        item.WorkOrderToolID = Wotool.WorkOrderToolID;
                    }
                    model.WorkOrderToolTable = lstWorkOrderToolUpdate;
                }
            }

            #endregion


            #region Add-Update in WorkOrderStatusTrack

            if (model.StatusTrackTable != null)
            {
                //For New Added Items - Insert into DB
                List<JOStatusTrack> lstJOStatusTrackAdd = model.StatusTrackTable.Where(wot => wot.FlagForAddUpdateDelete == 1).ToList();
                if (lstJOStatusTrackAdd != null && lstJOStatusTrackAdd.Count > 0)
                {
                    foreach (var item in lstJOStatusTrackAdd)
                    {
                        JOStatusTrack objJOStatusTrack = new JOStatusTrack();
                        objJOStatusTrack.Old_WorkStatusID = item.Old_WorkStatusID;
                        objJOStatusTrack.New_WorkStatusID = item.New_WorkStatusID;
                        objJOStatusTrack.Latitude = item.Latitude;
                        objJOStatusTrack.Longitude = item.Longitude;
                        objJOStatusTrack.CreatedBy = item.CreatedBy;
                        objJOStatusTrack.CreatedDate = System.DateTime.Now;
                        objJOStatusTrack.WorkorderNo = item.WorkorderNo;

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<JOStatusTrack> lst = objContext.SaveWithClientID<JOStatusTrack>(objJOStatusTrack, clientCode, true).ToList();
                            objJOStatusTrack.JOStatusTrackID = lst[0].JOStatusTrackID;
                        }

                        item.JOStatusTrackID = objJOStatusTrack.JOStatusTrackID;
                    }
                    model.StatusTrackTable = lstJOStatusTrackAdd;
                }

                //For Updated Items-Delete from table first and then Insert into DB                
                List<JOStatusTrack> lstJOStatusTrackUpdate = model.StatusTrackTable.Where(wot => wot.FlagForAddUpdateDelete == 2).ToList();

                if (lstJOStatusTrackUpdate != null && lstJOStatusTrackUpdate.Count > 0)
                {
                    foreach (var item in lstJOStatusTrackUpdate)
                    {
                        JOStatusTrack objJOStatusTrack = new JOStatusTrack();
                        objJOStatusTrack.JOStatusTrackID = item.JOStatusTrackID;
                        objJOStatusTrack.Old_WorkStatusID = item.Old_WorkStatusID;
                        objJOStatusTrack.New_WorkStatusID = item.New_WorkStatusID;
                        objJOStatusTrack.Latitude = item.Latitude;
                        objJOStatusTrack.Longitude = item.Longitude;                        
                        objJOStatusTrack.CreatedDate = System.DateTime.Now;
                        objJOStatusTrack.WorkorderNo = item.WorkorderNo;
                        
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<JOStatusTrack> lst = objContext.SaveWithClientID<JOStatusTrack>(objJOStatusTrack, clientCode, true).ToList();
                            objJOStatusTrack.JOStatusTrackID = lst[0].JOStatusTrackID;
                        }

                        item.JOStatusTrackID = objJOStatusTrack.JOStatusTrackID;
                    }
                    model.StatusTrackTable = lstJOStatusTrackUpdate;
                }
            }

            #endregion



            #region Add-Update in WorkOrderPPETable

            if (model.WorkOrderPPETable != null)
            {
                //For New Added Items - Insert into DB
                List<WorkOrderPPE> lstWorkOrderPPEAdd = model.WorkOrderPPETable.Where(wop => wop.FlagForAddUpdateDelete == 1).ToList();
                if (lstWorkOrderPPEAdd != null && lstWorkOrderPPEAdd.Count > 0)
                {
                    foreach (var item in lstWorkOrderPPEAdd)
                    {
                        WorkOrderPPE Woppe = new WorkOrderPPE();
                        Woppe.WorkOrderNo = item.WorkOrderNo;
                        Woppe.SeqNo = item.SeqNo;
                        Woppe.PPEDescription = item.PPEDescription;
                        Woppe.AltPPEDescription = item.AltPPEDescription;
                        //DbContext.WorkOrderPPEs.Add(Woppe);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<WorkOrderPPE> lst = objContext.SaveWithClientID<WorkOrderPPE>(Woppe, clientCode, true).ToList();
                            Woppe.WorkOrderPPEID = lst[0].WorkOrderPPEID;
                        }

                        item.WorkOrderPPEID = Woppe.WorkOrderPPEID;
                    }
                    model.WorkOrderPPETable = lstWorkOrderPPEAdd;
                }

                //For Updated Items-Delete from table first and then Insert into DB
                List<WorkOrderPPE> lstWorkOrderPPEUpdate = model.WorkOrderPPETable.Where(wop => wop.FlagForAddUpdateDelete == 2).ToList();
                if (lstWorkOrderPPEUpdate != null && lstWorkOrderPPEUpdate.Count > 0)
                {
                    foreach (var item in lstWorkOrderPPEUpdate)
                    {
                        var Woppe = new WorkOrderPPE();
                        Woppe.WorkOrderNo = item.WorkOrderNo;
                        Woppe.SeqNo = item.SeqNo;
                        Woppe.PPEDescription = item.PPEDescription;
                        Woppe.AltPPEDescription = item.AltPPEDescription;
                        //DbContext.WorkOrderPPEs.Add(Woppe);
                        //DbContext.SaveChanges();

                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            List<WorkOrderPPE> lst = objContext.SaveWithClientID<WorkOrderPPE>(Woppe, clientCode, true).ToList();
                            Woppe.WorkOrderPPEID = lst[0].WorkOrderPPEID;
                        }

                        item.WorkOrderPPEID = Woppe.WorkOrderPPEID;
                    }
                    model.WorkOrderPPETable = lstWorkOrderPPEUpdate;
                }
            }

            #endregion

            #endregion

            #region For Add/Delete Attachment
            if (model.ExtassetfileTable != null)
            {
                //For Delete Attachment
                var lstExtAssetFileDelete = model.ExtassetfileTable.Where(att => att.FlagForAddUpdateDelete == 3).ToList();

                foreach (var item in lstExtAssetFileDelete)
                {
                    DeleteAttachment(attachmentPath, item, clientCode);
                }
            }
            #endregion

            #region Change RequestStatusID in worequest if WO is Cancelled Or Closed

            if (model.WorkStatusID == 2 || model.WorkStatusID == 3 && (!string.IsNullOrEmpty(model.WorkorderNo)))
            {
                //var WorkRequest = DbContext.worequests.Where(w => w.WorkorderNo == model.WorkorderNo).FirstOrDefault();
                Worequest WorkRequest = null;
                using (ServiceContext objContext = new ServiceContext(0))
                {
                    WorkRequest = objContext.SearchAllWithClientID<Worequest>(new Worequest(), clientCode).Where(w => w.WorkorderNo == model.WorkorderNo).FirstOrDefault();
                }

                if (WorkRequest != null)
                {
                    try
                    {
                        Collection<DBParameters> parameters = new Collection<DBParameters>();
                        using (ServiceContext objContext = new ServiceContext(0))
                        {
                            string strQuery = "Update worequest set RequestStatusID = " + model.WorkStatusID + " , ModifiedBy = '" + model.ModifiedBy + "' , ModifiedDate = '" + DBExecute.GetEnglishDateWithoutTime(System.DateTime.Now) + "' where RequestNo = '" + WorkRequest.RequestNo + "'";
                            objContext.ExecuteQueryWithClientID<Worequest>(strQuery, parameters, clientCode).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }

            #endregion

            #region Insert into WOStatusAudit Table when WO is closed or completed
            if (Convert.ToInt64(model.WorkStatusID) == 2 || Convert.ToInt64(model.WorkStatusID) == 7)
            {
                WOStatusAudit woStatusAuditObj = new WOStatusAudit();
                woStatusAuditObj.WONo = model.WorkorderNo;

                woStatusAuditObj.WOStatus = model.WorkStatusID;
                woStatusAuditObj.Date = DateTime.Now;
                woStatusAuditObj.EmpID = Convert.ToInt32(model.CreatedBy);
                woStatusAuditObj.Source = model.Source;

                woStatusAuditObj.Latitude = model.Latitude;
                woStatusAuditObj.Longitude = model.Longitude;

                using (ServiceContext objContext = new ServiceContext(0))
                {
                    objContext.SaveWithClientID<WOStatusAudit>(woStatusAuditObj, clientCode, true);
                }
                //DbContext.WOStatusAudits.Add(woStatusAuditObj);
                //DbContext.SaveChanges();
            }
            #endregion

            objAddUpdateWorkOrder.WorkOrderTable = model;
            objAddUpdateWorkOrder.Message = ConstatntMessages.SuccessUpdateWOMessage;
            //   scope.Complete();
            //}
            return objAddUpdateWorkOrder;
        }
        
        #endregion
        
        /// <summary>
        /// Delete Attachment By FkId and ModuleType
        /// </summary>
        /// <param name="FkId"></param>
        /// <param name="ModuleType"></param>
        public void DeleteAttachment(string attachmentPath, ExtAssetFile model, string clientCode)
        {
            int deleteFromPhysicalFolder = DeleteAttachmentFile(attachmentPath, model.FileLink);
            if (deleteFromPhysicalFolder > 0)
            {

                string strextassetfilesForWRQuery = "delete FROM extassetfiles WHERE AutoId = " + model.AutoId + "";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.ExecuteQueryWithClientID<ExtAssetFile>(strextassetfilesForWRQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //DbContext.extassetfiles.Delete(e => e.AutoId == model.AutoId);
                //DbContext.SaveChanges();
                model.FileSaveMessage = ConstatntMessages.FileSuccessfullyDeleteMessage;
            }
            else if (deleteFromPhysicalFolder == -1)
            {

                string strextassetfilesForWRQuery = "delete FROM extassetfiles WHERE AutoId = " + model.AutoId + "";
                try
                {
                    Collection<DBParameters> parameters = new Collection<DBParameters>();
                    using (ServiceContext objContext = new ServiceContext(0))
                    {
                        objContext.ExecuteQueryWithClientID<ExtAssetFile>(strextassetfilesForWRQuery, parameters, clientCode).ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                //DbContext.extassetfiles.Delete(e => e.AutoId == model.AutoId);
                //DbContext.SaveChanges();
                model.FileSaveMessage = ConstatntMessages.FileSuccessfullyDeleteMessage;
                //model.FileSaveMessage = ConstatntMessages.FileDeleteNotExistMessage;
            }
            else
            {
                model.FileSaveMessage = ConstatntMessages.ErrorMessage;
            }

        }

        /// <summary>
        /// Delete Attachment From Physical Folder
        /// </summary>
        /// <param name="attachmentPath"></param>
        /// <param name="FileLink"></param>
        public int DeleteAttachmentFile(string attachmentPath, string FileLink)
        {
            string pathTodeleteFileFrom = attachmentPath + FileLink;
            try
            {
                if (File.Exists(pathTodeleteFileFrom))
                {
                    File.Delete(pathTodeleteFileFrom);
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception)
            {

                return 0;
            }
        }

        public static Account GetAccountType(string clientCode)
        {
            using (ServiceContext context = new ServiceContext(0))
            {
                Account objAccount = new Account();
                objAccount.ClientCode = clientCode;
                return context.GetMasterAccountDetail(clientCode);
            }

        }

        public static PermissionAccess SetEmployeePermission(employees emp, string clientCode, int? objAccountDbType)
        {
            try
            {
                Collection<DBParameters> parameters = new Collection<DBParameters>();


                parameters.Add(new DBParameters()
                {
                    Name = "EmployeeId",
                    Value = emp.EmployeeID,
                    DBType = DbType.Int32
                });

                parameters.Add(new DBParameters()
                {
                    Name = "UserGroupId",
                    Value = emp.UserGroupId,
                    DBType = DbType.Int32
                });

                string query = string.Empty;
                PermissionAccess objPermission = new PermissionAccess();

                using (ServiceContext objDapperContext = new ServiceContext(0))
                {
                    if (objAccountDbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        query = @"SELECT  
 (REPLACE(REPLACE(ModuleName,' ',''),'/','') ||  '_' ||  REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') ||  '_' ||  REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(AllowAccess) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";
                    }
                    else
                    {
                        query = @"SELECT  
 CONCAT(REPLACE(REPLACE(ModuleName,' ',''),'/','') , '_' , REPLACE(REPLACE(REPLACE(SubModuleName,' ','') ,'/',''),'-','') , '_' , REPLACE(REPLACE(REPLACE(PermissionDesc,' ',''),'/',''),'-','')) AS PermissionName , 
 min(CAST(AllowAccess AS DECIMAL)) AllowAccess 
 FROM permissions 
  Inner JOIN permissionsmappings ON permissions.PermissionId = permissionsmappings.PermissionId 
  where EmployeeId = @EmployeeID OR UserGroupId = @UserGroupID 
  GROUP BY permissions.PermissionId,ModuleName,SubModuleName,PermissionDesc";

                    }

                    List<Permission> lst = objDapperContext.ExecuteQueryWithClientID<Permission>(query, parameters, clientCode).ToList();

                    foreach (Permission lstPermission in lst)
                    {
                        PropertyInfo info = objPermission.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lstPermission.PermissionName.ToString().ToLower());
                        if (info != null)
                        {
                            /*Set the Value to Model*/
                            info.SetValue(objPermission, lstPermission.AllowAccess);
                        }
                    }
                }
                return objPermission;
            }
            catch
            {
                throw;
            }
        }

        
    }
}
