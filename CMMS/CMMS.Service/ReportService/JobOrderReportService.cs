﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CMMS.Service.ReportService
{
    public class JobOrderReportService : DBExecute
    {
        #region "Constructor"

        public JobOrderReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<JobOrderListModel> GetJobOrderList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTypeId, string workTradeId, string workPriorityId, string workStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workorders.WorkorderNo, workorders.ProblemDescription, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, location.LocationNo + CHAR(13) + location.LocationDescription As LocationDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, ";
            else
                query += "worktype.AltWorkTypeDescription + CHAR(13) + workstatus.AltWorkStatus + CHAR(13) + workpriority.AltWorkPriority + CHAR(13) + worktrade.AltWorkTrade As WorkDesc, location.LocationNo + CHAR(13) + location.LocationAltDescription As LocationDesc, assets.AssetNumber + CHAR(13) + assets.AssetAltDescription As AssetDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code,'') + CHAR(13) + isnull(L3.L3No,'') + CHAR(13) + isnull(L4.L4No,'') + CHAR(13) + isnull(L5.L5No,'') As LDesc, ";
            query += "workorders.DateReceived, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarStartDate AS DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarCompDate AS DATE)), '') As TarDateRange, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateStart AS DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateEnd AS DATE)), '') As ActDateRange ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkPriorityID in (select item from dbo.fnSplit('" + workPriorityId + "', ',')) or ISNULL('" + workPriorityId + "', '') = '') AND ";
            query += "((workorders.WorkTypeID IN (2, 7) AND (workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "')) ";
            query += "OR (Workorders.worktypeId not in (2, 7) AND (workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'))) AND ";
            query += "(workorders.WorkStatusID in (select item from dbo.fnSplit('" + workStatusId + "', ',')) or ISNULL('" + workStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY L2.L2Code, workorders.DateReceived";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderStatusStatisticsModel> GetJobOrderStatusStatistics(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTypeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderStatusStatisticsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workstatus.WorkStatusID, COUNT(*) As TotalCount, CAST(ROUND((((1.0 * COUNT(*)) / (1.0 * SUM(Count(*)) over())) * 100.00), 2) as numeric(5,2)) As Percentage, ";
            if (lang)
                query += "workstatus.WorkStatus ";
            else
                query += "workstatus.AltWorkStatus As WorkStatus ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "((workorders.WorkTypeID IN (2, 7) AND (workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "')) ";
            query += "OR (Workorders.WorkTypeID not in (2, 7) AND (workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'))) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group By workstatus.WorkStatus, workstatus.AltWorkStatus, workstatus.WorkStatusID ";
            query += "Order By workstatus.WorkStatusID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderStatusStatisticsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderAgingModel> GetJobOrderAging(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTypeId, string workTradeId, string workPriorityId, int aging)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderAgingModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workorders.WorkorderNo, workorders.ProblemDescription, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, location.LocationNo + CHAR(13) + location.LocationDescription As LocationDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, ";
            else
                query += "ISNULL(worktype.AltWorkTypeDescription, '') + CHAR(13) + ISNULL(workstatus.AltWorkStatus, '') + CHAR(13) + ISNULL(workpriority.AltWorkPriority, '') + CHAR(13) + ISNULL(worktrade.AltWorkTrade, '') As WorkDesc, location.LocationNo + CHAR(13) + ISNULL(location.LocationAltDescription, '') As LocationDesc, assets.AssetNumber + CHAR(13) + ISNULL(assets.AssetAltDescription, '') As AssetDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code,'') + CHAR(13) + isnull(L3.L3No,'') + CHAR(13) + isnull(L4.L4No,'') + CHAR(13) + isnull(L5.L5No,'') As LDesc, ";
            query += "workorders.DateReceived, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarStartDate As DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarCompDate As DATE)), '') As TarDateRange, ";
            query += "DATEDIFF(DAY, workorders.DateReceived, GETDATE()) As Aging ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkPriorityID in (select item from dbo.fnSplit('" + workPriorityId + "', ',')) or ISNULL('" + workPriorityId + "', '') = '') AND ";
            query += "((workorders.WorkTypeID IN (2, 7) AND (workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "')) ";
            query += "OR (Workorders.worktypeId not in (2, 7) AND (workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'))) AND ";
            query += "(DATEDIFF(DAY, workorders.DateReceived, GETDATE()) >= " + aging + ") ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY L2.L2Code, workorders.DateReceived";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderAgingModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderCostByLocationChartModel> GetJobOrderCostByLocationChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo, bool includeLabourCost)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderCostByLocationChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT MainenanceDivision.MaintDivisionID, ";
            if (lang)
                query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MainenanceDivision.MaintDivisionName As MaintDesc, ";
            else
                query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + ISNULL(MainenanceDivision.MaintDivisionAltName, '') As MaintDesc, ";
            query += "ROUND((ISNULL((SUM(workorderlabor.TotCost)), 0) + ISNULL((SUM(direct.Qty * direct.Price)), 0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)), 0) - ISNULL((SUM([return].Qty * [return].RetPrice)), 0))), 3) As TotalCostWithLabour, ";
            query += "ROUND((ISNULL((SUM(direct.Qty * direct.Price)), 0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)), 0) - ISNULL((SUM([return].Qty * [return].RetPrice)),0))), 3) As TotalCost ";
            query += "from workorders inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID left outer join ";
            query += "workorderlabor on workorders.WorkorderNo = workorderlabor.WorkorderNo left outer join ";
            query += "direct on workorders.WorkorderNo = direct.WONo left outer join ";
            query += "issue on workorders.WorkorderNo = issue.WONo left outer join ";
            query += "[return] on workorders.WorkorderNo = [return].WONo ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.LocationId = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(workorders.ActDateEnd between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "GROUP BY MainenanceDivision.MaintDivisionID, MainenanceDivision.MaintDivisionCode, MainenanceDivision.MaintDivisionName, MainenanceDivision.MaintDivisionAltName ";
            query += "Order by TotalCostWithLabour desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderCostByLocationChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<FailurePercentageAnalysisChartModel> GetFailurePercentageAnalysisChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo, string workTypeId, string assetCategoryId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<FailurePercentageAnalysisChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT failurecause.FailureCauseID, COUNT(*) As TotalCount, CAST(ROUND((((1.0 * COUNT(*)) / (1.0 * SUM(Count(*)) over())) * 100.00), 2) as numeric(5,2)) As Percentage, ";
            if (lang)
                query += "failurecause.FailureCauseDescription As FailureDesc ";
            else
                query += "failurecause.FailureCauseAltDescription As FailureDesc ";
            query += "from workorders inner join ";
            query += "failurecause on workorders.FailureCauseID = failurecause.FailureCauseID inner join ";
            query += "assets on workorders.AssetID = assets.AssetID left outer join ";
            query += "assetcategories on assets.AssetCatID = assetcategories.AssetCatID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.LocationId = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "(assetcategories.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "', ',')) or ISNULL('" + assetCategoryId + "', '') = '') AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group By failurecause.FailureCauseDescription, failurecause.FailureCauseAltDescription, failurecause.FailureCauseID ";
            query += "Order By failurecause.FailureCauseID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<FailurePercentageAnalysisChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderManHoursByLocationChartModel> GetJobOrderManHoursByLocationChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderManHoursByLocationChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT Location.LocationNo, isnull(Sum(TotHour),0) as TotalManHours ";
            query += "from workorders inner join ";
            query += "Location on Location.LocationId = workorders.LocationId left outer join ";
            query += "workorderlabor on workorders.WorkorderNo = workorderlabor.WorkorderNo ";
            query += "where WorkStatusId <> 3 AND ";
            query += "(workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.LocationId = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(workorderlabor.StartDate >= '" + dateFrom.ToShortDateString() + "' and workorderlabor.EndDate <= '" + dateTo.ToShortDateString() + "') ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "GROUP BY Location.LocationNo ";
            query += "Order by TotalManHours desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderManHoursByLocationChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderCostByJobTypeChartModel> GetJobOrderCostByJobTypeChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, bool includeLabourCost)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderCostByJobTypeChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT worktype.WorkTypeID, ";
            if (lang)
                query += "worktype.WorkTypeDescription, ";
            else
                query += "worktype.AltWorkTypeDescription As WorkTypeDescription, ";
            query += "(ISNULL((SUM(workorderlabor.TotCost)),0) + ISNULL((SUM(direct.Qty * direct.Price)),0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)),0) - ISNULL((SUM([return].Qty * [return].RetPrice)),0))) As TotalCostWithLabour, ";
            query += "(ISNULL((SUM(direct.Qty * direct.Price)),0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)),0) - ISNULL((SUM([return].Qty * [return].RetPrice)),0))) As TotalCost ";
            query += "from workorders inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID left outer join ";
            query += "workorderlabor on workorders.WorkorderNo = workorderlabor.WorkorderNo left outer join ";
            query += "direct on workorders.WorkorderNo = direct.WONo left outer join ";
            query += "issue on workorders.WorkorderNo = issue.WONo left outer join ";
            query += "[return] on workorders.WorkorderNo = [return].WONo ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "GROUP BY worktype.WorkTypeID, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription ";
            query += "Order by worktype.WorkTypeID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderCostByJobTypeChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderTypeDistributionChartModel> GetJobOrderTypeDistributionChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderTypeDistributionChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT worktype.WorkTypeID, COUNT(*) As TotalCount, CAST(ROUND((((1.0 * COUNT(*)) / (1.0 * SUM(Count(*)) over())) * 100.00), 2) as numeric(5,2)) As Percentage, ";
            if (lang)
                query += "worktype.WorkTypeDescription ";
            else
                query += "worktype.AltWorkTypeDescription As WorkTypeDescription ";
            query += "from workorders inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "((workorders.WorkTypeID IN (2, 7) AND (workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "')) ";
            query += "OR (Workorders.worktypeId not in (2, 7) AND (workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'))) AND ";
            query += "(workorders.WorkStatusID != 3) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "GROUP BY worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, worktype.WorkTypeID ";
            query += "Order by worktype.WorkTypeID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderTypeDistributionChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<FailureDowntimeAnalysisChartModel> GetFailureDowntimeAnalysisChart(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo, string workTypeId, string assetCategoryId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<FailureDowntimeAnalysisChartModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT failurecause.FailureCauseID, T.Downtime, ";
            if (lang)
                query += "failurecause.FailureCauseDescription As FailureDesc ";
            else
                query += "failurecause.FailureCauseAltDescription As FailureDesc ";
            query += "from (Select isnull(FailureCauseID, 0) as FailureCauseID, SUM(workorders.DownTime) as Downtime ";
            query += "from workorders inner join ";
            query += "assets on workorders.AssetID = assets.AssetID left outer join ";
            query += "assetcategories on assets.AssetCatID = assetcategories.AssetCatID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.LocationId = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "(assetcategories.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "', ',')) or ISNULL('" + assetCategoryId + "', '') = '') AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "group by FailureCauseID) as T ";
            query += "inner join failurecause on failurecause.FailureCauseID = T.FailureCauseID ";
            query += "order by t.Downtime Desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<FailureDowntimeAnalysisChartModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderCostByCostCenterModel> GetJobOrderCostByCostCenter(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string costCenterId, bool includeLabourCost)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderCostByCostCenterModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ";
            if (lang)
                query += "costcenter.CostCenterNo + CHAR(13) + costcenter.CostCenterName As CostCenterDesc, ";
            else
                query += "costcenter.CostCenterNo + CHAR(13) + costcenter.AltCostCenterName As CostCenterDesc, ";
            query += "(ISNULL((SUM(workorderlabor.TotCost)), 0) + ISNULL((SUM(direct.Qty * direct.Price)), 0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)), 0) - ISNULL((SUM([return].Qty * [return].RetPrice)), 0))) As TotalCostWithLabour, ";
            query += "(ISNULL((SUM(direct.Qty * direct.Price)), 0) + (ISNULL((SUM(issue.QtyIssue * issue.Price3)), 0) - ISNULL((SUM([return].Qty * [return].RetPrice)), 0))) As TotalCost ";
            query += "from workorders inner join ";
            query += "costcenter on workorders.CostCenterId = costcenter.CostCenterId left outer join ";
            query += "workorderlabor on workorders.WorkorderNo = workorderlabor.WorkorderNo left outer join ";
            query += "direct on workorders.WorkorderNo = direct.WONo left outer join ";
            query += "issue on workorders.WorkorderNo = issue.WONo left outer join ";
            query += "[return] on workorders.WorkorderNo = [return].WONo ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkStatusID = 2) AND ";
            query += "(workorders.CostCenterId in (select item from dbo.fnSplit('" + costCenterId + "', ',')) or ISNULL('" + costCenterId + "', '') = '') ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "GROUP BY costcenter.CostCenterNo, costcenter.CostCenterId, costcenter.CostCenterName, costcenter.AltCostCenterName ";
            query += "Order by costcenter.CostCenterId";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderCostByCostCenterModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrdersByEmployeesModel> GetJobOrdersByEmployees(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTypeId, string workStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrdersByEmployeesModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT employees.EmployeeID, ";
            if (lang)
                query += "employees.Name As EmployeeName, worktype.WorkTypeDescription, ";
            else
                query += "employees.AltName As EmployeeName, worktype.AltWorkTypeDescription As WorkTypeDescription, ";
            query += "ISNULL(SUM(workorderlabor.TotHour), 0) As TotalHours ";
            query += "from workorderlabor inner join ";
            query += "employees on workorderlabor.EmployeeID = employees.EmployeeID inner join ";
            query += "workorders on workorderlabor.WorkorderNo = workorders.WorkorderNo inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorderlabor.StartDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "(workorders.WorkStatusID in (select item from dbo.fnSplit('" + workStatusId + "', ',')) or ISNULL('" + workStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group By employees.Name, employees.AltName, worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, employees.EmployeeID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrdersByEmployeesModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderRatingsModel> GetJobOrderRatings(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTypeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderRatingsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT rating.RateID, COUNT(*) As TotalCount, CAST(ROUND((((1.0 * COUNT(*)) / (1.0 * SUM(Count(*)) over())) * 100.00), 2) as numeric(5,2)) As Percentage, ";
            if (lang)
                query += "rating.Description As RatingDesc ";
            else
                query += "rating.AltDescription As RatingDesc ";
            query += "from workorders inner join ";
            query += "rating on workorders.RatingsID = rating.RateID or isnull(workorders.RatingsID, 6) = rating.RateID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') AND ";
            query += "((workorders.WorkTypeID IN (2, 7) AND (workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "')) ";
            query += "OR (Workorders.worktypeId not in (2, 7) AND (workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'))) AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group By rating.Description, rating.AltDescription, rating.RateID ";
            query += "ORDER BY rating.Description";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderRatingsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMJobOrderAgingModel> GetPMJobOrderAging(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTradeId, string workPriorityId, int aging)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMJobOrderAgingModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workorders.WorkorderNo, workorders.ProblemDescription, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, location.LocationNo + CHAR(13) + location.LocationDescription As LocationDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, pmchecklist.ChecklistNo + CHAR(13) + pmchecklist.CheckListName As PMChecklistDesc, ";
            else
                query += "ISNULL(worktype.AltWorkTypeDescription, '') + CHAR(13) + ISNULL(workstatus.AltWorkStatus, '') + CHAR(13) + ISNULL(workpriority.AltWorkPriority, '') + CHAR(13) + ISNULL(worktrade.AltWorkTrade, '') As WorkDesc, location.LocationNo + CHAR(13) + ISNULL(location.LocationAltDescription, '') As LocationDesc, assets.AssetNumber + CHAR(13) + ISNULL(assets.AssetAltDescription, '') As AssetDesc, pmchecklist.ChecklistNo + CHAR(13) + ISNULL(pmchecklist.AltCheckListName, '') As PMChecklistDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code,'') + CHAR(13) + isnull(L3.L3No,'') + CHAR(13) + isnull(L4.L4No,'') + CHAR(13) + isnull(L5.L5No,'') As LDesc, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarStartDate As DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarCompDate As DATE)), '') As TarDateRange, ";
            query += "DATEDIFF(DAY, workorders.PMTarCompDate, GETDATE()) As Aging ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID left outer join ";
            query += "pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WorkTypeID = 2 or workorders.WorkTypeID = 7) AND ";
            query += "(workorders.WorkStatusID Not IN (2,3)) AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkPriorityID in (select item from dbo.fnSplit('" + workPriorityId + "', ',')) or ISNULL('" + workPriorityId + "', '') = '') AND ";
            query += "(DATEDIFF(DAY, workorders.PMTarCompDate, GETDATE()) >= " + aging + ") ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY L2.L2Code, DATEDIFF(DAY, workorders.PMTarCompDate, GETDATE()) Desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMJobOrderAgingModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<JobOrderStatusStatisticsModel> GetPMJobOrderStatusStatistics(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTradeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<JobOrderStatusStatisticsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workstatus.WorkStatusID, COUNT(*) As TotalCount, CAST(ROUND((((1.0 * COUNT(*)) / (1.0 * SUM(Count(*)) over())) * 100.00), 2) as numeric(5,2)) As Percentage, ";
            if (lang)
                query += "workstatus.WorkStatus ";
            else
                query += "workstatus.AltWorkStatus As WorkStatus ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarStartDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkTypeID = 2 Or workorders.WorkTypeID = 7) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group By workstatus.WorkStatus, workstatus.AltWorkStatus, workstatus.WorkStatusID ";
            query += "Order By workstatus.WorkStatusID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<JobOrderStatusStatisticsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMJobOrderListModel> GetPMJobOrderList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTradeId, string workPriorityId, string workStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMJobOrderListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT workorders.WorkorderNo, workorders.ProblemDescription, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, location.LocationNo + CHAR(13) + location.LocationDescription As LocationDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, pmchecklist.ChecklistNo + CHAR(13) + pmchecklist.CheckListName As PMChecklistDesc, ";
            else
                query += "ISNULL(worktype.AltWorkTypeDescription, '') + CHAR(13) + ISNULL(workstatus.AltWorkStatus, '') + CHAR(13) + ISNULL(workpriority.AltWorkPriority, '') + CHAR(13) + ISNULL(worktrade.AltWorkTrade, '') As WorkDesc, location.LocationNo + CHAR(13) + ISNULL(location.LocationAltDescription, '') As LocationDesc, assets.AssetNumber + CHAR(13) + ISNULL(assets.AssetAltDescription, '') As AssetDesc, pmchecklist.ChecklistNo + CHAR(13) + ISNULL(pmchecklist.AltCheckListName, '') As PMChecklistDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code,'') + CHAR(13) + isnull(L3.L3No,'') + CHAR(13) + isnull(L4.L4No,'') + CHAR(13) + isnull(L5.L5No,'') As LDesc, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarStartDate As DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.PMTarCompDate As DATE)), '') As TarDateRange, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateStart As DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateEnd As DATE)), '') As ActDateRange ";
            query += "from workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID left outer Join ";
            query += "pmchecklist on workorders.PMChecklistID = pmchecklist.ChecklistID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarStartDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkPriorityID in (select item from dbo.fnSplit('" + workPriorityId + "', ',')) or ISNULL('" + workPriorityId + "', '') = '') AND ";
            query += "(workorders.WorkStatusID in (select item from dbo.fnSplit('" + workStatusId + "', ',')) or ISNULL('" + workStatusId + "', '') = '') AND ";
            query += "(workorders.WorkTypeID = 2 OR workorders.WorkTypeID = 7) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY L2.L2Code, workorders.DateReceived";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMJobOrderListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class JobOrderAgingModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string LocationDesc { get; set; }
        public string AssetDesc { get; set; }
        public DateTime? DateReceived { get; set; }
        public string TarDateRange { get; set; }
        public int Aging { get; set; }
    }

    public class PMJobOrderAgingModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string LocationDesc { get; set; }
        public string AssetDesc { get; set; }
        public string PMChecklistDesc { get; set; }
        public string TarDateRange { get; set; }
        public int Aging { get; set; }
    }

    public class JobOrderListModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string LocationDesc { get; set; }
        public string AssetDesc { get; set; }
        public DateTime? DateReceived { get; set; }
        public string TarDateRange { get; set; }
        public string ActDateRange { get; set; }
    }

    public class PMJobOrderListModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string LocationDesc { get; set; }
        public string AssetDesc { get; set; }
        public string PMChecklistDesc { get; set; }
        public string TarDateRange { get; set; }
        public string ActDateRange { get; set; }
    }

    public class JobOrderStatusStatisticsModel
    {
        public string WorkStatus { get; set; }
        public int WorkStatusID { get; set; }
        public int TotalCount { get; set; }
        public decimal Percentage { get; set; }
    }

    public class JobOrderCostByLocationChartModel
    {
        public int MaintDivisionID { get; set; }
        public string MaintDesc { get; set; }
        public decimal TotalCostWithLabour { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class FailurePercentageAnalysisChartModel
    {
        public int FailureCauseID { get; set; }
        public string FailureDesc { get; set; }
        public int TotalCount { get; set; }
        public decimal Percentage { get; set; }
    }

    public class FailureDowntimeAnalysisChartModel
    {
        public int FailureCauseID { get; set; }
        public string FailureDesc { get; set; }
        public decimal Downtime { get; set; }
    }

    public class JobOrderManHoursByLocationChartModel
    {
        public string LocationNo { get; set; }
        public decimal TotalManHours { get; set; }
    }

    public class JobOrderCostByJobTypeChartModel
    {
        public int WorkTypeID { get; set; }
        public string WorkTypeDescription { get; set; }
        public decimal TotalCostWithLabour { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class JobOrderCostByCostCenterModel
    {
        public string CostCenterDesc { get; set; }
        public decimal TotalCostWithLabour { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class JobOrderTypeDistributionChartModel
    {
        public string WorkTypeDescription { get; set; }
        public int WorkTypeID { get; set; }
        public int TotalCount { get; set; }
        public decimal Percentage { get; set; }
    }

    public class JobOrdersByEmployeesModel
    {
        public string EmployeeName { get; set; }
        public string WorkTypeDescription { get; set; }
        public int EmployeeID { get; set; }
        public decimal TotalHours { get; set; }
    }

    public class JobOrderRatingsModel
    {
        public string RatingDesc { get; set; }
        public int RateID { get; set; }
        public int TotalCount { get; set; }
        public decimal Percentage { get; set; }
    }
}
