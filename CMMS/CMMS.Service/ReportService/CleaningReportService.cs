﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class CleaningReportService : DBExecute
    {
        #region "Constructor"

        public CleaningReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<CleaningInspectionSummaryModel> GetCleaningInspectionSummary(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, DateTime dateFrom, DateTime dateTo, int inspectedBy, int groupId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string strWhere = "";
            if (!string.IsNullOrEmpty(l2Id))
                strWhere = strWhere + " and CIM.L2ID = " + l2Id;
            
            if ((dateFrom != null) & (dateTo != null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "', InspectionDate) >= 0 and DATEDIFF(DD,'" + dateTo.ToShortDateString() + "',InspectionDate) <= 0)";
            else if ((dateFrom != null) & (dateTo == null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "', InspectionDate) >= 0)";
            else if ((dateFrom == null) & (dateTo != null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "',InspectionDate) <= 0)";
            
            if (groupId > 0)
                strWhere = strWhere + " and CleaningGroupId = " + groupId;
            
            if (inspectedBy > 0)
                strWhere = strWhere + " and InspectionBy = " + inspectedBy;
            
            IList<CleaningInspectionSummaryModel> list = new List<CleaningInspectionSummaryModel>();

            using (DapperContext objDapperContext = new DapperContext())
            {
                CleaningInspectionSummaryModel objCleaningInspectionSummaryModel = new CleaningInspectionSummaryModel();
                string query = string.Empty;

                IList<int> lstTotalNoOfInspection;
                query = " Select Count(*) ";
                query += " from cleaninginspectiondetails ";
                query += " Inner Join cleaninginspectionmaster CIM on CIM.CleaningInspectionId = cleaninginspectiondetails.CleaningInspectionId ";
                query += " where 1 = 1 " + strWhere;
                lstTotalNoOfInspection = objDapperContext.AdvanceSearch<int>(query, parameters, 0, DbType, ConnectionString);
                if (lstTotalNoOfInspection.Count > 0)
                    objCleaningInspectionSummaryModel.TotalNoOfInspection = lstTotalNoOfInspection[0];

                IList<int> lstTotalAcceptable;
                query = " Select Count(*) ";
                query += " from cleaninginspectiondetails ";
                query += " Inner Join cleaninginspectionmaster CIM on CIM.CleaningInspectionId = cleaninginspectiondetails.CleaningInspectionId ";
                query += " where 1 = 1 and SatisfactionResult = 'Acceptable' " + strWhere;
                lstTotalAcceptable = objDapperContext.AdvanceSearch<int>(query, parameters, 0, DbType, ConnectionString);
                if (lstTotalAcceptable.Count > 0)
                    objCleaningInspectionSummaryModel.TotalAcceptable = lstTotalAcceptable[0];

                IList<int> lstTotalNonAcceptable;
                query = " Select Count(*) ";
                query += " from cleaninginspectiondetails ";
                query += " Inner Join cleaninginspectionmaster CIM on CIM.CleaningInspectionId = cleaninginspectiondetails.CleaningInspectionId ";
                query += " where 1 = 1 and SatisfactionResult = 'Un Acceptable' " + strWhere;
                lstTotalNonAcceptable = objDapperContext.AdvanceSearch<int>(query, parameters, 0, DbType, ConnectionString);
                if (lstTotalNonAcceptable.Count > 0)
                    objCleaningInspectionSummaryModel.TotalNonAcceptable = lstTotalNonAcceptable[0];

                if (objCleaningInspectionSummaryModel.TotalNoOfInspection == 0)
                    objCleaningInspectionSummaryModel.PercentageAcceptable = 0;
                else
                    objCleaningInspectionSummaryModel.PercentageAcceptable = Convert.ToDouble((objCleaningInspectionSummaryModel.TotalAcceptable / objCleaningInspectionSummaryModel.TotalNoOfInspection) * 100);

                list.Add(objCleaningInspectionSummaryModel);
            }

            return list;
        }

        public IList<CleaningInspectionDetailListModel> GetCleaningInspectionDetailList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, DateTime dateFrom, DateTime dateTo, int inspectedBy, int groupId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<CleaningInspectionDetailListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string strWhere = "";
            if (!string.IsNullOrEmpty(l2Id))
                strWhere = strWhere + " and CIM.L2ID = " + l2Id;
            
            if ((dateFrom != null) & (dateTo != null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "', InspectionDate) >= 0 and DATEDIFF(DD,'" + dateTo.ToShortDateString() + "',InspectionDate) <= 0)";
            else if ((dateFrom != null) & (dateTo == null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "', InspectionDate) >= 0)";
            else if ((dateFrom == null) & (dateTo != null))
                strWhere = strWhere + " and (DATEDIFF(DD,'" + dateFrom.ToShortDateString() + "',InspectionDate) <= 0)";
            
            if (groupId > 0)
                strWhere = strWhere + " and CleaningGroupId = " + groupId;
            
            if (inspectedBy > 0)
                strWhere = strWhere + " and InspectionBy = " + inspectedBy;
            
            query += "Select ReferenceNo, InspectionDate, LocationNo, DateInspection 'InspectedDate', TotalPercentage, SatisfactionResult, Name 'InspectedBy', [Floor], Wall, Window, [Ceiling], Furniture, WasteBag, Consumables, ";
            if (lang)
                query += "LocationDescription, GroupDesc ";
            else
                query += "LocationAltDescription As LocationDescription, AltGroupDesc As GroupDesc ";
            query += "FROM cleaninginspectiondetails CID INNER JOIN ";
            query += "cleaninginspectionmaster CIM on CIM.CleaningInspectionId = CID.CleaningInspectionId INNER JOIN ";
            query += "employees Emp on Emp.EmployeeID = CIM.InspectionBy INNER JOIN ";
            query += "location l on l.LocationID = CID.LocationID INNER JOIN ";
            query += "cleaninggroup CG on CG.CleaningGroupId = CID.CleaningGroupId ";
            query += "where 1 = 1 " + strWhere;
            
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<CleaningInspectionDetailListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class CleaningInspectionSummaryModel
    {
        public int TotalNoOfInspection { get; set; }
        public int TotalAcceptable { get; set; }
        public int TotalNonAcceptable { get; set; }
        public double PercentageAcceptable { get; set; }
    }

    public class CleaningInspectionDetailListModel
    {
        public string ReferenceNo { get; set; }
        public DateTime? InspectionDate { get; set; }
        public string LocationNo { get; set; }
        public string LocationDescription { get; set; }
        public string GroupDesc { get; set; }
        public DateTime? InspectedDate { get; set; }
        public double TotalPercentage { get; set; }
        public string SatisfactionResult { get; set; }
        public string InspectedBy { get; set; }
        public int Floor { get; set; }
        public int Wall { get; set; }
        public int Window { get; set; }
        public int Ceiling { get; set; }
        public int Furniture { get; set; }
        public int WasteBag { get; set; }
        public int Consumables { get; set; }
    }
}
