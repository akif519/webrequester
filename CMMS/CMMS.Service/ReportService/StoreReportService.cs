﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class StoreReportService : DBExecute
    {
        #region "Constructor"

        public StoreReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<ItemListModel> GetItemList(int accountId, bool isCentral, bool lang, int employeeId, int itemTypeId, int itemSubTypeId, int itemStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<ItemListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT stockcode.StockNo, stockcode.Specification, stockcode.Manufacturer, stockcode.Price2, stockcode.AvgPrice, ";
            if (lang)
                query += "stockcode.StockDescription, attrgroup.AttrName, attrsubgroup.AttrSubName, ";
            else
                query += "stockcode.AltStockDescription As StockDescription, attrgroup.AltAttrName As AttrName, attrsubgroup.AltAttrSubName As AttrSubName, ";
            query += "case when stockcode.Status = '1' then 'Active' else 'Not Active' End As Status ";
            query += "from stockcode LEFT OUTER JOIN ";
            query += "attrgroup ON stockcode.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN ";
            query += "attrsubgroup ON stockcode.AttrSubID = attrsubgroup.AttrSubID ";
            query += "where (stockcode.AttrGroupID = '" + itemTypeId + "' or ISNULL('" + itemTypeId + "', 0) = 0) AND ";
            query += "(stockcode.AttrSubID = '" + itemSubTypeId + "' or ISNULL('" + itemSubTypeId + "', 0) = 0) AND ";
            query += "(stockcode.Status  = '" + itemStatusId + "' or ISNULL('" + itemStatusId + "', 0) = 0) ";
            query += "ORDER BY stockcode.StockDescription ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ItemListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<StockTakeListModel> GetStockTakeList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int subStoreId, int itemTypeId, int itemSubTypeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<StockTakeListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT StockNo, Specification, Manufacturer, SubStoreCode, ISNULL(sl.Balance, 0) as Balance, ";
            if (lang)
                query += "StockDescription, L2Name ";
            else
                query += "AltStockDescription As StockDescription, L2Altname As L2Name ";
            query += "from stockcode sc LEFT OUTER JOIN ";
            query += "stockcode_levels sl on sl.stock_id = sc.StockID LEFT OUTER JOIN ";
            query += "substore on substore.SubStoreID = sl.sub_store_id LEFT OUTER JOIN ";
            query += "L2 on L2.L2ID = substore.L2ID ";
            query += "where (sc.AttrGroupID = '" + itemTypeId + "' or ISNULL('" + itemTypeId + "', 0) = 0) AND ";
            query += "(sc.AttrSubID = '" + itemSubTypeId + "' or ISNULL('" + itemSubTypeId + "', 0) = 0) AND ";
            query += "(substore.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(substore.SubStoreID  = '" + subStoreId + "' or ISNULL('" + subStoreId + "', 0) = 0) ";
            query += "ORDER BY StockNo ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<StockTakeListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<StockBalanceListModel> GetStockBalanceList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int subStoreId, int itemTypeId, int itemSubTypeId, DateTime? transactionDate)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<StockBalanceListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT tbl.SubStoreID, tbl.L2Id, tbl.StockID, ";
            if (lang)
                query += "StockDescription, ";
            else
                query += "AltStockDescription As StockDescription, ";
            query += "Sum(ReceiveQty) as ReceiveQty, Sum(IssueQty) as IssueQty, Sum(ReturnQty) as ReturnQty, Sum(AdjustmentQty) as AdjustmentQty, ";
            query += "(Sum(ReceiveQty) + Sum(IssueQty) + Sum(ReturnQty) + Sum(AdjustmentQty)) as Total, ";
            query += "StockNo, L2.L2Code, substore.SubStoreCode, Specification, Manufacturer, AvgPrice, IsNull(Balance, 0) as Balance, ";
            query += "(AvgPrice * (Sum(ReceiveQty) + Sum(IssueQty) + Sum(ReturnQty) + Sum(AdjustmentQty))) as TotalPrice ";
            query += "from ( ";
            query += "Select SubStoreID, L2Id, StockID, QtyRec as ReceiveQty, 0 as IssueQty, 0 as ReturnQty, 0 as AdjustmentQty from [receive] WHERE 1 = 1 ";
            if (transactionDate.HasValue)
            {
                if (transactionDate.Value != DateTime.MinValue)
                {
                    query += "AND Cast([Date] as Date)  = Cast('" + transactionDate.Value.ToShortDateString() + "' as Date) ";
                }
            }
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, 0 as ReceiveQty, QtyIssue as IssueQty, 0 as ReturnQty, 0 as AdjustmentQty from [issue] WHERE 1 = 1 ";
            if (transactionDate.HasValue)
            {
                if (transactionDate.Value != DateTime.MinValue)
                {
                    query += "AND Cast(DateIssue as Date)  = Cast('" + transactionDate.Value.ToShortDateString() + "' as Date) ";
                }
            }
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, 0 as ReceiveQty, 0 as IssueQty, Qty as ReturnQty, 0 as AdjustmentQty from [return] WHERE 1 = 1 ";
            if (transactionDate.HasValue)
            {
                if (transactionDate.Value != DateTime.MinValue)
                {
                    query += "AND Cast([Date] as Date)  = Cast('" + transactionDate.Value.ToShortDateString() + "' as Date) ";
                }
            }
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, 0 as ReceiveQty, 0 as IssueQty, 0 as ReturnQty, QtyAdj as AdjustmentQty from [adjustment] WHERE 1 = 1 ";
            if (transactionDate.HasValue)
            {
                if (transactionDate.Value != DateTime.MinValue)
                {
                    query += "AND Cast([Date] as Date)  = Cast('" + transactionDate.Value.ToShortDateString() + "' as Date) ";
                }
            }
            query += ") tbl Inner Join ";

            query += "stockcode SC on SC.StockID = tbl.StockID Inner Join ";
            query += "L2 on L2.L2Id = tbl.L2Id Inner Join ";
            query += "substore on substore.SubStoreID = tbl.SubStoreID LEFT OUTER JOIN ";
            query += "stockcode_levels on stockcode_levels.sub_store_id = tbl.SubStoreID and stockcode_levels.stock_id = tbl.StockID ";
            query += "where (SC.AttrGroupID = '" + itemTypeId + "' or ISNULL('" + itemTypeId + "', 0) = 0) AND ";
            query += "(SC.AttrSubID = '" + itemSubTypeId + "' or ISNULL('" + itemSubTypeId + "', 0) = 0) AND ";
            query += "(tbl.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(tbl.SubStoreID  = '" + subStoreId + "' or ISNULL('" + subStoreId + "', 0) = 0) ";
            query += "group by tbl.SubStoreID, tbl.L2Id, tbl.StockID, StockNo, L2.L2Code, substore.SubStoreCode, StockDescription,AltStockDescription, Specification, Manufacturer, AvgPrice, Balance ";
            query += "ORDER BY StockNo, tbl.L2Id, tbl.SubStoreID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<StockBalanceListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<TransactionListModel> GetTransactionList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int subStoreId, DateTime? transactionDate)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<TransactionListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select StockNo, SubStoreCode, Specification, Manufacturer, Quantity, TransactionType, Price2 As Price, TransactionDate, TotalValue, ";
            if (lang)
                query += "StockDescription, L2Name ";
            else
                query += "AltStockDescription As StockDescription, L2Altname As L2Name ";
            query += "From ( ";
            query += "Select tbl.SubStoreID, tbl.L2Id, tbl.StockID, Quantity, TransactionType, tbl.Price2, TransactionDate, (Quantity * tbl.Price2) as TotalValue, ";
            query += "StockNo, substore.SubStoreCode, StockDescription, AltStockDescription, Specification, Manufacturer, L2Name, L2Altname ";
            query += "From ( ";
            query += "Select SubStoreID, L2Id, StockID, [Date] as TransactionDate, ISNULL(QtyRec, 0) as Quantity, 'Receive' as TransactionType, ISNULL(Price, 0) 'Price2' from [receive] ";
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, DateIssue as TransactionDate, ISNULL(QtyIssue, 0) as Quantity, 'Issue' as TransactionType, ISNULL(Price3, 0) 'Price2' from [issue] ";
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, [Date] as TransactionDate, ISNULL(Qty, 0) as Quantity, 'Return' as TransactionType, ISNULL(RetPrice, 0) 'Price2' from [return] ";
            query += "Union ";
            query += "Select SubStoreID, L2Id, StockID, [Date] as TransactionDate, ISNULL(QtyAdj, 0) as Quantity, 'Adjustment' as TransactionType, ISNULL(Price4, 0) 'Price2' from [adjustment] ";
            query += ") tbl Inner Join ";
            query += "stockcode SC on SC.StockID = tbl.StockID Inner Join ";
            query += "L2 on L2.L2Id = tbl.L2Id Inner Join ";
            query += "substore on substore.SubStoreID = tbl.SubStoreID ";
            query += ")T where (L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(SubStoreID  = '" + subStoreId + "' or ISNULL('" + subStoreId + "', 0) = 0) ";
            query += "ORDER BY StockNo, T.L2Name, T.SubStoreCode ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<TransactionListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseProposalListModel> GetPurchaseProposalListNormal(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int subStoreId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseProposalListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select StockNo, Manufacturer, Balance, min_level As Level, reorder_qty As ReOrderQty, ";
            if (lang)
                query += "StockDescription, SubStoreDesc ";
            else
                query += "AltStockDescription As StockDescription, AltSubStoreDesc As SubStoreDesc ";
            query += "From vwPurchaseProposal ";
            query += "Where (L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(SubStoreID  = '" + subStoreId + "' or ISNULL('" + subStoreId + "', 0) = 0) AND ";
            query += "Balance < re_order_level And Balance >= min_level";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseProposalListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseProposalListModel> GetPurchaseProposalListHigh(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int subStoreId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseProposalListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select StockNo, Manufacturer, Balance, min_level As Level, reorder_qty As ReOrderQty, ";
            if (lang)
                query += "StockDescription, SubStoreDesc ";
            else
                query += "AltStockDescription As StockDescription, AltSubStoreDesc As SubStoreDesc ";
            query += "From vwPurchaseProposal ";
            query += "Where (L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(SubStoreID  = '" + subStoreId + "' or ISNULL('" + subStoreId + "', 0) = 0) AND ";
            query += "Balance < vw.min_level";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseProposalListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PartListModel> GetPartReceiveQtyList(int accountId, bool isCentral, bool lang, int employeeId, int month, int year)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PartListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            DateTime dateFrom;
            DateTime dateTo;
            dateFrom = new DateTime(year, month, 1);
            dateTo = dateFrom.AddMonths(12).AddDays(-1);

            query += "Select StockNo, TotalQty, Month1, Month2, Month3, Month4, Month5, Month6, Month7, Month8, Month9, Month10, Month11, Month12, ";
            if (lang)
                query += "StockDescription, AttrName, AttrSubName ";
            else
                query += "AltStockDescription As StockDescription, AltAttrName As AttrName, AltAttrSubName As AttrSubName ";
            query += "From ( ";
            query += "Select StockNo, StockDescription, AltStockDescription, AttrName, AttrSubName, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where [Date] Between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'  and r.StockID = sc.StockID),0) as TotalQty, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month('" + dateFrom.ToShortDateString() + "') and Year([Date]) = Year('" + dateTo.ToShortDateString() + "') and sc.StockID = r.StockID), 0) as Month1, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 1, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 1, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month2, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 2, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 2, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month3, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 3, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 3, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month4, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 4, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 4, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month5, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 5, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 5, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month6, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 6, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 6, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month7, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 7, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 7, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month8, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 8, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 8, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month9, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 9, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 9, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month10, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 10, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 10, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month11, ";
            query += "ISNULL((Select Sum(QtyRec) from [receive] r where Month([Date]) = Month(DateAdd(mm, 11, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 11, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month12 ";
            query += "From stockcode sc LEFT OUTER JOIN  ";
            query += "attrgroup ON sc.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN ";
            query += "attrsubgroup ON sc.AttrSubID = attrsubgroup.AttrSubID ";
            query += ") T Order By StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PartListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PartListModel> GetPartIssueQtyList(int accountId, bool isCentral, bool lang, int employeeId, int month, int year)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PartListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            DateTime dateFrom;
            DateTime dateTo;
            dateFrom = new DateTime(year, month, 1);
            dateTo = dateFrom.AddMonths(12).AddDays(-1);

            query += "Select StockNo, TotalQty, Month1, Month2, Month3, Month4, Month5, Month6, Month7, Month8, Month9, Month10, Month11, Month12, ";
            if (lang)
                query += "StockDescription, AttrName, AttrSubName ";
            else
                query += "AltStockDescription As StockDescription, AltAttrName As AttrName, AltAttrSubName As AttrSubName ";
            query += "From ( ";
            query += "Select StockNo, StockDescription, AltStockDescription, AttrName, AttrSubName, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where [DateIssue] Between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'  and i.StockID = sc.StockID), 0) as TotalQty, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month('" + dateFrom.ToShortDateString() + "') and Year([DateIssue]) = Year('" + dateTo.ToShortDateString() + "') and sc.StockID = i.StockID), 0) as Month1, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 1, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 1, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month2, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 2, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 2, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month3, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 3, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 3, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month4, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 4, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 4, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month5, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 5, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 5, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month6, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 6, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 6, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month7, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 7, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 7, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month8, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 8, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 8, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month9, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 9, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 9, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month10, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 10, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 10, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month11, ";
            query += "ISNULL((Select Sum(QtyIssue) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 11, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 11, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month12 ";
            query += "From stockcode sc LEFT OUTER JOIN  ";
            query += "attrgroup ON sc.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN ";
            query += "attrsubgroup ON sc.AttrSubID = attrsubgroup.AttrSubID ";
            query += ") T Order By StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PartListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PartListModel> GetPartReceiveTotalCostList(int accountId, bool isCentral, bool lang, int employeeId, int month, int year)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PartListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            DateTime dateFrom;
            DateTime dateTo;
            dateFrom = new DateTime(year, month, 1);
            dateTo = dateFrom.AddMonths(12).AddDays(-1);

            query += "Select StockNo, TotalQty, Month1, Month2, Month3, Month4, Month5, Month6, Month7, Month8, Month9, Month10, Month11, Month12, ";
            if (lang)
                query += "StockDescription, AttrName, AttrSubName ";
            else
                query += "AltStockDescription As StockDescription, AltAttrName As AttrName, AltAttrSubName As AttrSubName ";
            query += "From ( ";
            query += "Select StockNo, StockDescription, AltStockDescription, AttrName, AttrSubName, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where [Date] Between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'  and r.StockID = sc.StockID),0) as TotalQty, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month('" + dateFrom.ToShortDateString() + "') and Year([Date]) = Year('" + dateTo.ToShortDateString() + "') and sc.StockID = r.StockID), 0) as Month1, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 1, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 1, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month2, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 2, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 2, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month3, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 3, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 3, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month4, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 4, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 4, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month5, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 5, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 5, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month6, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 6, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 6, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month7, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 7, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 7, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month8, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 8, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 8, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month9, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 9, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 9, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month10, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 10, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 10, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month11, ";
            query += "ISNULL((Select Sum(QtyRec * Price) from [receive] r where Month([Date]) = Month(DateAdd(mm, 11, '" + dateFrom.ToShortDateString() + "')) and Year([Date]) = Year((DateAdd(mm, 11, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = r.StockID), 0) as Month12 ";
            query += "From stockcode sc LEFT OUTER JOIN  ";
            query += "attrgroup ON sc.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN ";
            query += "attrsubgroup ON sc.AttrSubID = attrsubgroup.AttrSubID ";
            query += ") T Order By StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PartListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PartListModel> GetPartIssueTotalCostList(int accountId, bool isCentral, bool lang, int employeeId, int month, int year)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PartListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            DateTime dateFrom;
            DateTime dateTo;
            dateFrom = new DateTime(year, month, 1);
            dateTo = dateFrom.AddMonths(12).AddDays(-1);

            query += "Select StockNo, TotalQty, Month1, Month2, Month3, Month4, Month5, Month6, Month7, Month8, Month9, Month10, Month11, Month12, ";
            if (lang)
                query += "StockDescription, AttrName, AttrSubName ";
            else
                query += "AltStockDescription As StockDescription, AltAttrName As AttrName, AltAttrSubName As AttrSubName ";
            query += "From ( ";
            query += "Select StockNo, StockDescription, AltStockDescription, AttrName, AttrSubName, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where [DateIssue] Between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "'  and i.StockID = sc.StockID), 0) as TotalQty, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month('" + dateFrom.ToShortDateString() + "') and Year([DateIssue]) = Year('" + dateTo.ToShortDateString() + "') and sc.StockID = i.StockID), 0) as Month1, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 1, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 1, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month2, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 2, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 2, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month3, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 3, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 3, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month4, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 4, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 4, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month5, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 5, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 5, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month6, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 6, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 6, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month7, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 7, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 7, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month8, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 8, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 8, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month9, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 9, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 9, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month10, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 10, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 10, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month11, ";
            query += "ISNULL((Select Sum(QtyIssue * Price3) from [issue] i where Month([DateIssue]) = Month(DateAdd(mm, 11, '" + dateFrom.ToShortDateString() + "')) and Year([DateIssue]) = Year((DateAdd(mm, 11, '" + dateTo.ToShortDateString() + "'))) and sc.StockID = i.StockID), 0) as Month12 ";
            query += "From stockcode sc LEFT OUTER JOIN  ";
            query += "attrgroup ON sc.AttrGroupID = attrgroup.AttrID LEFT OUTER JOIN ";
            query += "attrsubgroup ON sc.AttrSubID = attrsubgroup.AttrSubID ";
            query += ") T Order By StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PartListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<DormantPartsModel> GetDormantParts(int accountId, bool isCentral, bool lang, int employeeId, int days)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<DormantPartsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select sc.StockNo, sc.Specification , sc.Manufacturer, ";
            if (lang)
                query += "sc.StockDescription, ag.AttrName, asg.AttrSubName, ss.SubStoreDesc, l.L2Name, ";
            else
                query += "sc.AltStockDescription As StockDescription, ag.AltAttrName As AttrName, asg.AltAttrSubName As AttrSubName, ss.AltSubStoreDesc As SubStoreDesc, l.L2Altname As L2Name, ";
            query += "isnull(sl.Balance, 0) as Balance, sc.AvgPrice, isnull((sc.AvgPrice * sl.Balance), 0) as TotalPrice ";
            query += "from stockcode_levels sl INNER JOIN ";
            query += "stockcode sc ON sl.stock_id = sc.StockID LEFT JOIN  ";
            query += "attrgroup ag ON sc.AttrGroupID = ag.AttrID LEFT JOIN ";
            query += "attrsubgroup asg ON sc.AttrSubID = asg.AttrSubID INNER JOIN ";
            query += "substore ss on sl.sub_store_id = ss.SubStoreID INNER JOIN ";
            query += "L2 l on ss.L2ID = l.L2ID ";
            query += "where sl.stock_id NOT IN ( ";
            query += "select StockID from receive where [Date] between DATEADD(day, -" + days.ToString() + ", GetDate()) and GETDATE() ";
            query += "Union ";
            query += "select StockID from adjustment where [Date] between DATEADD(day, -" + days.ToString() + ", GetDate()) and GETDATE() ";
            query += "Union ";
            query += "select StockID from issue where [DateIssue] between DATEADD(day, -" + days.ToString() + ", GetDate()) and GETDATE() ";
            query += "Union ";
            query += "select StockID from [return] where [Date] between DATEADD(day, -" + days.ToString() + ", GetDate()) and GETDATE()) ";
            query += "order by sc.StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<DormantPartsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<ItemIssueByJobOrderModel> GetItemIssueByJobOrder(int accountId, bool isCentral, bool lang, int employeeId, string JobOrderNo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<ItemIssueByJobOrderModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select s.StockNo, sum(i.QtyIssue) as TotalQty, i.WONo as WorkOrderNo, ";
            if (lang)
                query += "s.StockDescription ";
            else
                query += "s.AltStockDescription As StockDescription ";
            query += "From issue i inner join ";
            query += "stockcode s on i.StockId = s.StockId ";
            query += "where i.woNo is not null AND ";
            query += "(i.woNo = '" + JobOrderNo + "' or ISNULL('" + JobOrderNo + "', 0) = 0) ";
            query += "group by s.StockNo, s.StockDescription, s.AltStockDescription, i.WONo ";
            query += "order by s.StockNo";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ItemIssueByJobOrderModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class ItemListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AttrName { get; set; }
        public string AttrSubName { get; set; }
        public string Specification { get; set; }
        public string Manufacturer { get; set; }
        public decimal? Price2 { get; set; }
        public decimal? AvgPrice { get; set; }
        public string Status { get; set; }
    }

    public class StockTakeListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string Specification { get; set; }
        public string Manufacturer { get; set; }
        public string SubStoreCode { get; set; }
        public decimal? Balance { get; set; }
        public string L2Name { get; set; }
    }

    public class StockBalanceListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string Specification { get; set; }
        public string Manufacturer { get; set; }
        public string SubStoreCode { get; set; }
        public decimal? AvgPrice { get; set; }
        public string L2Code { get; set; }
        public decimal? ReceiveQty { get; set; }
        public decimal? IssueQty { get; set; }
        public decimal? ReturnQty { get; set; }
        public decimal? AdjustmentQty { get; set; }
        public decimal? Total { get; set; }
        public decimal? TotalPrice { get; set; }
    }

    public class TransactionListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string Specification { get; set; }
        public string Manufacturer { get; set; }
        public string SubStoreCode { get; set; }
        public string L2Name { get; set; }
        public string TransactionType { get; set; }
        public DateTime? TransactionDate { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Price { get; set; }
        public decimal? TotalValue { get; set; }
    }

    public class PurchaseProposalListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string Manufacturer { get; set; }
        public string SubStoreDesc { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Level { get; set; }
        public decimal? ReOrderQty { get; set; }
    }

    public class PartListModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AttrName { get; set; }
        public string AttrSubName { get; set; }
        public decimal? TotalQty { get; set; }
        public decimal? Month1 { get; set; }
        public decimal? Month2 { get; set; }
        public decimal? Month3 { get; set; }
        public decimal? Month4 { get; set; }
        public decimal? Month5 { get; set; }
        public decimal? Month6 { get; set; }
        public decimal? Month7 { get; set; }
        public decimal? Month8 { get; set; }
        public decimal? Month9 { get; set; }
        public decimal? Month10 { get; set; }
        public decimal? Month11 { get; set; }
        public decimal? Month12 { get; set; }
    }

    public class DormantPartsModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string AttrName { get; set; }
        public string AttrSubName { get; set; }
        public string Specification { get; set; }
        public string Manufacturer { get; set; }
        public string L2Name { get; set; }
        public string SubStoreDesc { get; set; }
        public decimal? Balance { get; set; }
        public decimal? AvgPrice { get; set; }
        public decimal? TotalPrice { get; set; }
    }

    public class ItemIssueByJobOrderModel
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string WorkOrderNo { get; set; }
        public decimal? TotalQty { get; set; }
    }
}
