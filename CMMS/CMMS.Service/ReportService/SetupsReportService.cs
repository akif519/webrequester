﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class SetupsReportService : DBExecute
    {
        #region "Constructor"

        public SetupsReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<LocationListModel> GetLocationList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<LocationListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ROW_NUMBER() OVER (ORDER BY L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo) AS RowNo, location.LocationNo, ";
            if (lang)
                query += "location.LocationDescription, isnull(L3.L3No, '') + CHAR(13) + isnull(L3.L3Desc, '') As L3Desc, isnull(L4.L4No, '') + CHAR(13) + isnull(L4.L4Description, '') As L4Desc, isnull(L5.L5No, '') + CHAR(13) + isnull(L5.L5Description, '') As L5Desc, ";
            else
                query += "location.LocationAltDescription As LocationDescription, isnull(L3.L3No, '') + CHAR(13) + isnull(L3.L3AltDesc, '') As L3Desc, isnull(L4.L4No, '') + CHAR(13) + isnull(L4.L4AltDescription, '') As L4Desc, isnull(L5.L5No, '') + CHAR(13) + isnull(L5.L5AltDescription, '') As L5Desc, ";
            query += "L2.L2Code ";
            query += "from location INNER JOIN ";
            query += "L2 ON location.L2ID = L2.L2ID LEFT OUTER JOIN ";
            query += "L3 ON location.L3ID = L3.L3ID INNER JOIN ";
            query += "L4 ON location.L4ID = L4.L4ID LEFT OUTER JOIN ";
            query += "L5 ON location.L5ID = L5.L5ID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') ";
            
            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<LocationListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<SupplierListModel> GetSupplierList(int accountId, bool isCentral, bool lang, int employeeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<SupplierListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ROW_NUMBER() over (order by SupplierName) as RowNo, SupplierNo, ";
            if (lang)
                query += "ISNULL(SupplierName, ' ') AS SupplierName, ";
            else
                query += "ISNULL(AltSupplierName, ' ') AS SupplierName, ";
            query += "ISNULL(Address, ' ') + ', ' + ISNULL(StateOrProvince, ' ') + ', ' + ISNULL(City, ' ') + ', ' + ISNULL(Country, ' ') AS FullAddress, ContactName, PhoneNumber, FaxNumber ";
            query += "from suppliers ";
            query += "ORDER BY SupplierName";
            
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<SupplierListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class LocationListModel
    {
        public int RowNo { get; set; }
        public string LocationNo { get; set; }
        public string LocationDescription { get; set; }
        public string L2Code { get; set; }
        public string L3Desc { get; set; }
        public string L4Desc { get; set; }
        public string L5Desc { get; set; }
    }

    public class SupplierListModel
    {
        public int RowNo { get; set; }
        public string SupplierNo { get; set; }
        public string SupplierName { get; set; }
        public string FullAddress { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
    }
}