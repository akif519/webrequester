﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class PurchasingReportService : DBExecute
    {
        #region "Constructor"

        public PurchasingReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<PurchaseRequestListModel> GetPurchaseRequestList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string requestStatusId, string approvalStatusId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseRequestListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select PurchaseRequestID, PR.CreatedDate, DateRequired, emp.Name, ";
            if (lang)
                query += "L2.L2name As L2Name, status_desc, auth_status_desc ";
            else
                query += "L2.L2Altname As L2Name, altstatus_desc As status_desc, Altauth_status_desc As auth_status_desc ";
            query += "From PurchaseRequest PR LEFT OUTER JOIN ";
            query += "L2 on L2.L2ID = PR.L2ID LEFT OUTER JOIN ";
            query += "employees emp on emp.EmployeeID = PR.RequestedBy LEFT OUTER JOIN ";
            query += "purchase_req_status ReqStatus on ReqStatus.pr_status_id = PR.status_id LEFT OUTER JOIN ";
            query += "pr_authorisation_status AuthStatus on AuthStatus.auth_status_id = PR.auth_status ";
            query += "where (DATEDIFF(DD, '" + dateFrom.ToShortDateString() + "', PR.CreatedDate) >= 0 and DATEDIFF(DD, '" + dateTo.ToShortDateString() + "', PR.CreatedDate) <= 0) AND ";
            query += "(PR.L2Id = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(PR.status_id in (select item from dbo.fnSplit('" + requestStatusId + "', ',')) or ISNULL('" + requestStatusId + "', '') = '') AND ";
            query += "(PR.auth_status in (select item from dbo.fnSplit('" + approvalStatusId + "', ',')) or ISNULL('" + approvalStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY PurchaseRequestID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseRequestListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseRequestDetailsListModel> GetPurchaseRequestDetailsList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string requestStatusId, string approvalStatusId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseRequestDetailsListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select PR.PurchaseRequestID, PR.CreatedDate, StockNo, PRI.UOM, PRI.Quantity, WorkorderNo, ";
            if (lang)
                query += "StockDescription, status_desc, auth_status_desc ";
            else
                query += "AltStockDescription As StockDescription, altstatus_desc As status_desc, Altauth_status_desc As auth_status_desc ";
            query += "From PurchaseRequest PR LEFT OUTER JOIN ";
            query += "PurchaseRequestItems PRI on PRI.PurchaseRequestID = PR.ID LEFT OUTER JOIN ";
            query += "stockcode SC on SC.StockID = PRI.PartNumber LEFT OUTER JOIN ";
            query += "workorders WO on WO.WorkorderNo = PRI.JobOrderID LEFT OUTER JOIN ";
            query += "purchase_req_status ReqStatus on ReqStatus.pr_status_id = PR.status_id LEFT OUTER JOIN ";
            query += "pr_authorisation_status AuthStatus on AuthStatus.auth_status_id = PR.auth_status ";
            query += "where (DATEDIFF(DD, '" + dateFrom.ToShortDateString() + "', PR.CreatedDate) >= 0 and DATEDIFF(DD, '" + dateTo.ToShortDateString() + "', PR.CreatedDate) <= 0) AND ";
            query += "(PR.L2Id = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(PR.status_id in (select item from dbo.fnSplit('" + requestStatusId + "', ',')) or ISNULL('" + requestStatusId + "', '') = '') AND ";
            query += "(PR.auth_status in (select item from dbo.fnSplit('" + approvalStatusId + "', ',')) or ISNULL('" + approvalStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (PR.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Order by PurchaseRequestID ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseRequestDetailsListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseOrderListModel> GetPurchaseOrderList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string orderStatusId, string deliveryStatusId, int supplierId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseOrderListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select PurchaseOrderNo, PO.CreatedDate, emp.Name, AccountCode, OrderStatus, DeliveryStatus, ";
            query += "CurrencyCode + ' ' + CAST(OrderTotal as nvarchar(25)) 'Currency_TotalCost', TotalBaseCost 'TotalCost_BaseCurrency', ";
            if (lang)
                query += "L2.L2name As L2Name, CostCenterName, SupplierName ";
            else
                query += "L2.L2Altname As L2Name, AltCostCenterName As CostCenterName, AltSupplierName As SupplierName ";
            query += "From PurchaseOrder PO LEFT OUTER JOIN ";
            query += "accountcode AC on AC.AccCodeid = PO.AccountCodeID LEFT OUTER JOIN ";
            query += "CostCenter CC on CC.CostCenterId = AC.CostCenterId LEFT OUTER JOIN ";
            query += "Suppliers on Suppliers.SupplierID = PO.SupplierID LEFT OUTER JOIN ";
            query += "L2 on L2.L2ID = PO.L2ID LEFT OUTER JOIN ";
            query += "Currency on Currency.CurrencyID = PO.CurrencyID LEFT OUTER JOIN ";
            query += "employees emp on emp.EmployeeID = PO.Orderby ";
            query += "where (DATEDIFF(DD, '" + dateFrom.ToShortDateString() + "', PO.CreatedDate) >= 0 and DATEDIFF(DD, '" + dateTo.ToShortDateString() + "', PO.CreatedDate) <= 0) AND ";
            query += "(PO.L2Id = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(PO.OrderStatus in (select item from dbo.fnSplit('" + orderStatusId + "', ',')) or ISNULL('" + orderStatusId + "', '') = '') AND ";
            query += "(PO.DeliveryStatus in (select item from dbo.fnSplit('" + deliveryStatusId + "', ',')) or ISNULL('" + deliveryStatusId + "', '') = '') AND ";
            query += "(PO.SupplierID = '" + supplierId + "' or ISNULL('" + supplierId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Order by PurchaseOrderNo ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseOrderListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseOrderDetailsListModel> GetPurchaseOrderDetailsList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string orderStatusId, string deliveryStatusId, int supplierId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseOrderDetailsListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select PurchaseOrderNo, OrderStatus, DeliveryStatus, C.CurrencyCode + ' ' + CAST(OrderTotal as nvarchar(25)) 'Currency_TotalCost', TotalBaseCost 'TotalCost_BaseCurrency', ";
            query += "StockNo, POI.UOM, POI.Quantity, LineCost, BaseCost, WorkorderNo, ";
            if (lang)
                query += "StockDescription ";
            else
                query += "AltStockDescription As StockDescription ";
            query += "From PurchaseOrder PO LEFT OUTER JOIN ";
            query += "Currency on Currency.CurrencyID = PO.CurrencyID LEFT OUTER JOIN ";
            query += "PurchaseOrderItems POI on POI.PurchaseOrderID = PO.ID LEFT OUTER JOIN ";
            query += "stockcode SC on SC.StockID = POI.PartNumber LEFT OUTER JOIN ";
            query += "workorders WO on WO.WorkorderNo = POI.JobOrderID LEFT OUTER JOIN ";
            query += "Currency C on C.CurrencyID = PO.CurrencyID ";
            query += "where (DATEDIFF(DD, '" + dateFrom.ToShortDateString() + "', PO.CreatedDate) >= 0 and DATEDIFF(DD, '" + dateTo.ToShortDateString() + "', PO.CreatedDate) <= 0) AND ";
            query += "(PO.L2Id = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(PO.OrderStatus in (select item from dbo.fnSplit('" + orderStatusId + "', ',')) or ISNULL('" + orderStatusId + "', '') = '') AND ";
            query += "(PO.DeliveryStatus in (select item from dbo.fnSplit('" + deliveryStatusId + "', ',')) or ISNULL('" + deliveryStatusId + "', '') = '') AND ";
            query += "(PO.SupplierID = '" + supplierId + "' or ISNULL('" + supplierId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (PO.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Order by PurchaseOrderNo ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseOrderDetailsListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PurchaseOrderCostModel> GetPurchaseOrderCost(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string orderStatusId, string deliveryStatusId, int supplierId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PurchaseOrderCostModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select PurchaseOrderNo, AccountCode, OrderStatus, DeliveryStatus, ";
            query += "CurrencyCode + ' ' + CAST(OrderTotal as nvarchar(25)) 'Currency_TotalCost', TotalBaseCost 'TotalCost_BaseCurrency', ";
            if (lang)
                query += "L2name As L2Name, TermofSaleDesc, ShippedViaDesc, PaymentTermsDesc, CostCenterName, SupplierName ";
            else
                query += "L2Altname As L2Name, AltTermofSaleDesc As TermofSaleDesc, AltShippedViaDesc As ShippedViaDesc, AltPaymentTermsDesc As PaymentTermsDesc, AltCostCenterName As CostCenterName, AltSupplierName As SupplierName ";
            query += "From PurchaseOrder PO LEFT OUTER JOIN ";
            query += "accountcode AC on AC.AccCodeid = PO.AccountCodeID LEFT OUTER JOIN ";
            query += "CostCenter CC on CC.CostCenterId = AC.CostCenterId LEFT OUTER JOIN ";
            query += "Suppliers on Suppliers.SupplierID = PO.SupplierID LEFT OUTER JOIN ";
            query += "L2 on L2.L2ID = PO.L2ID LEFT OUTER JOIN ";
            query += "Currency on Currency.CurrencyID = PO.CurrencyID LEFT OUTER JOIN ";
            query += "PaymentTerms on PaymentTerms.ID = PO.PaymentTermsID LEFT OUTER JOIN ";
            query += "ShippedVia on ShippedVia.ID = PO.ShippedViaID LEFT OUTER JOIN ";
            query += "TermofSale on TermofSale.ID = PO.TermOfSaleID ";
            query += "where (DATEDIFF(DD, '" + dateFrom.ToShortDateString() + "', PO.CreatedDate) >= 0 and DATEDIFF(DD, '" + dateTo.ToShortDateString() + "', PO.CreatedDate) <= 0) AND ";
            query += "(PO.L2Id = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(PO.OrderStatus in (select item from dbo.fnSplit('" + orderStatusId + "', ',')) or ISNULL('" + orderStatusId + "', '') = '') AND ";
            query += "(PO.DeliveryStatus in (select item from dbo.fnSplit('" + deliveryStatusId + "', ',')) or ISNULL('" + deliveryStatusId + "', '') = '') AND ";
            query += "(PO.SupplierID = '" + supplierId + "' or ISNULL('" + supplierId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PurchaseOrderCostModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
        public IList<POHeaderFooter> GetPOHeaderFooter(int accountId, bool isCentral, bool lang, int employeeId, int POId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<POHeaderFooter> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select p.PurchaseOrderNo,p.CreatedDate,p.OrderStatus,p.DeliveryStatus,L2.L2Name,MainenanceDivision.MaintDivisionName";
            //if (lang)
            //    query += "L2.L2name As L2Name, status_desc, auth_status_desc ";
            //else
            //    query += "L2.L2Altname As L2Name, altstatus_desc As status_desc, Altauth_status_desc As auth_status_desc ";
            if (lang)
            {
                query += ",(Select Top 1 Costcenter.CostCenterNo + ' ' + Costcenter.CostCenterName from CostCenter where  CostCenter.CostCenterId = accountcode.CostCenterId) AS CostCenter ,Currency.CurrencyCode + ' ' + Currency.CurrencyDescription as Currency";
                //query += ", L4.L4No + ' '+ L4.L4Description as ProjectNumber ";
                query += ", (Select top 1 L4.L4No + ' ' + L4.L4Description from L4 where L4.L2Id = L2.L2Id) AS ProjectNumber ";
            }
            else
            {

                query += ",(Select Top 1 Costcenter.CostCenterNo + ' ' + Costcenter.AltCostCenterName from CostCenter where CostCenter.CostCenterId = accountcode.CostCenterId) AS CostCenter,Currency.CurrencyCode + ' ' + Currency.AltCurrencyDescription as Currency ";
                //query += ", L4.L4No + ' '+ L4.L4AltDescription as ProjectNumber ";
                query += ", (Select top 1 L4.L4No + ' ' + L4.L4AltDescription from L4 where L4.L2Id = L2.L2Id) AS ProjectNumber ";
            }
            query += " ,MaintenanceDepartment.MaintDeptdesc,MaintSubDept.MaintSubDeptDesc,o.Name as OrderBy,iTo.Name as InvoiceTo, ";
            query += "TermofSale.TermofSaleDesc,AccountCodeDesc as accountcode,ShippedVia.ShippedViaDesc ,Suppliers.SupplierName,";
            query += "PaymentTerms.PaymentTermsDesc as Paymentterm,p.OrderedDate,p.QuotationDate as RequiredDate,p.ExpDeliveryDate,p.QuotationReference,p.DeliverTo as Deliveryto,p.Remarks,";
            query += "p.OrderTotal,p.DiscountPercentage,p.GSTPercentage,p.FreightCharges,p.TaxPercentage,p.TotalBaseCost ";
            query += " from PurchaseOrder p ";
            query += " inner join L2 on L2.L2Id = p.L2Id";            
            query += " Inner join MainenanceDivision on MainenanceDivision.MaintDivisionID = p.MaintDivisionID";
            query += " Inner join MaintenanceDepartment on MaintenanceDepartment.maintDeptID = p.maintDeptID";
            query += " Inner join MaintSubDept on MaintSubDept.MaintSubDeptID = p.maintsubdeptID";
            query += " left outer join employees o on o.EmployeeId = p.OrderBy";
            query += " left outer join employees iTo on iTo.EmployeeId = p.InvoiceTo";
            query += " left outer join TermofSale on TermofSale.ID = p.TermofSaleId";
            query += " Left outer join accountcode on accountcode.AccCodeid = p.AccountCodeID";            
            query += " left outer join ShippedVia on ShippedVia.ID = p.ShippedViaID";
            query += " left outer join Suppliers on Suppliers.SupplierId = p.SupplierID";
            query += " left outer join PaymentTerms on PaymentTerms.ID = p.PaymentTermsID";
            query += " left outer join Currency on Currency.CurrencyId = p.CurrencyID";
            query += " where p.Id = " + POId;


            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<POHeaderFooter>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }


        public IList<POItems> GetPOItems(int accountId, bool isCentral, bool lang, int employeeId, int POId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<POItems> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select * from PurchaseOrderItems where PurchaseorderId = " + POId;


            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<POItems>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PRHeaderFooter> GetPRHeaderFooter(int accountId, bool isCentral, bool lang, int employeeId, int POId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PRHeaderFooter> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select  PurchaseRequest.PurchaseRequestID as PRNo,PurchaseRequest.CreatedDate,PurchaseRequest.DateRequired,PurchaseRequest.Note";
            query += " , PurchaseRequest.cancel_date,PurchaseRequest.CancelNotes ";
            if (lang)
            {
                query += ", L2.L2Code + ' - ' + L2.L2name As L2Name, status_desc, auth_status_desc ";
                query += ", MainenanceDivision.MaintDivisionCode + ' - ' + MainenanceDivision.MaintDivisionName as MaintDivisionName ";
                query += ", MaintenanceDepartment.MaintDeptCode + ' - ' + MaintenanceDepartment.MaintDeptdesc as MaintDeptdesc ";
                query += ", maintSubDept.MaintSubDeptCode + ' - ' + maintSubDept.MaintSubDeptDesc as MaintSubDeptDesc ";
                query += " ,CreateByemployees.NAME AS CreatedByEmployeeName ";
                query += " ,ReqByemployees.NAME AS RequestedByEmployeeName ";
                query += " ,CancelByemployees.NAME AS Cancel_byName ";
            }
            else
            {
                query += ",L2.L2Code + ' - ' + L2.L2Altname As L2Name, altstatus_desc As status_desc, Altauth_status_desc As auth_status_desc ";
                query += ", MainenanceDivision.MaintDivisionCode + ' - ' + MainenanceDivision.MaintDivisionAltName as MaintDivisionName ";
                query += ", MaintenanceDepartment.MaintDeptCode + ' - ' + MaintenanceDepartment.MaintDeptAltdesc as MaintDeptdesc ";
                query += ", maintSubDept.MaintSubDeptCode + ' - ' + maintSubDept.MaintSubDeptDesc as MaintSubDeptAltDesc ";
                query += " ,CreateByemployees.AltName AS CreatedByEmployeeName ";
                query += " ,ReqByemployees.AltName AS RequestedByEmployeeName ";
                query += " ,CancelByemployees.AltName AS Cancel_byName ";

            }
            query += " FROM PurchaseRequest LEFT OUTER JOIN employees ReqByemployees ON ReqByemployees.EmployeeID = purchaserequest.RequestedBy ";
            query += " LEFT OUTER JOIN employees CreateByemployees ON CreateByemployees.EmployeeID = purchaserequest.CreatedBy ";
            query += " LEFT OUTER JOIN employees CancelByemployees ON CancelByemployees.EmployeeID = purchaserequest.cancel_by ";
            query += " LEFT JOIN purchase_req_status ON purchase_req_status.pr_status_id = purchaserequest.status_id ";
            query += " LEFT JOIN pr_authorisation_status ON pr_authorisation_status.auth_status_id = purchaserequest.auth_status";
            query += " INNER JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = PurchaseRequest.MaintdivId ";
            query += " INNER JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = PurchaseRequest.MaintdeptId ";
            query += " INNER JOIN maintSubDept ON maintSubDept.MaintSubDeptID = PurchaseRequest.MainSubDeptId ";
            query += " inner join L2 on L2.L2Id = PurchaseRequest.L2Id ";
            query += " where PurchaseRequest.Id = " + POId;


            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PRHeaderFooter>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
        public IList<PRItems> GetPRItems(int accountId, bool isCentral, bool lang, int employeeId, int POId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PRItems> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select stockcode.StockNo ";
            if (lang)
            {
                query += ",prItems.PartDescription ";
            }
            else
            {
                query += ",prItems.Altpart_desc as PartDescription ";
            }
            query += ",prItems.uom ,prItems.Quantity ,ISNULL(prItems.JoborderID, '') AS WorkorderNo ,prItems.ID ,prItems.PurchaseRequestID ";
            query += " FROM PurchaseRequestItems prItems LEFT OUTER JOIN stockcode ON stockcode.StockID = prItems.PartNumber ";
            query += " where prItems.PurchaseRequestID = " + POId;

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PRItems>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class PurchaseRequestListModel
    {
        public string PurchaseRequestID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DateRequired { get; set; }
        public string L2Name { get; set; }
        public string Name { get; set; }
        public string status_desc { get; set; }
        public string auth_status_desc { get; set; }
    }

    public class PurchaseRequestDetailsListModel
    {
        public string PurchaseRequestID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string UOM { get; set; }
        public decimal Quantity { get; set; }
        public string WorkorderNo { get; set; }
        public string status_desc { get; set; }
        public string auth_status_desc { get; set; }
    }

    public class PurchaseOrderListModel
    {
        public string PurchaseOrderNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string AccountCode { get; set; }
        public string CostCenterName { get; set; }
        public string Name { get; set; }
        public string OrderStatus { get; set; }
        public string DeliveryStatus { get; set; }
        public string SupplierName { get; set; }
        public string L2Name { get; set; }
        public string Currency_TotalCost { get; set; }
        public decimal TotalCost_BaseCurrency { get; set; }
    }

    public class PurchaseOrderDetailsListModel
    {
        public string PurchaseOrderNo { get; set; }
        public string OrderStatus { get; set; }
        public string DeliveryStatus { get; set; }
        public string Currency_TotalCost { get; set; }
        public decimal TotalCost_BaseCurrency { get; set; }
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string UOM { get; set; }
        public decimal Quantity { get; set; }
        public decimal LineCost { get; set; }
        public decimal BaseCost { get; set; }
        public string WorkorderNo { get; set; }
    }

    public class PurchaseOrderCostModel
    {
        public string PurchaseOrderNo { get; set; }
        public string AccountCode { get; set; }
        public string CostCenterName { get; set; }
        public string OrderStatus { get; set; }
        public string DeliveryStatus { get; set; }
        public string SupplierName { get; set; }
        public string L2Name { get; set; }
        public string TermofSaleDesc { get; set; }
        public string ShippedViaDesc { get; set; }
        public string PaymentTermsDesc { get; set; }
        public string Currency_TotalCost { get; set; }
        public decimal TotalCost_BaseCurrency { get; set; }
    }
    public class POHeaderFooter
    {
        public string PurchaseOrderNo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string OrderStatus { get; set; }
        public string DeliveryStatus { get; set; }
        public string L2Name { get; set; }
        public string ProjectNumber { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDeptDesc { get; set; }
        public string MaintSubDeptDesc { get; set; }

        public string OrderBy { get; set; }
        public string TermofSaleDesc { get; set; }
        public string CostCenter { get; set; }

        public string InvoiceTo { get; set; }
        public string AccountCode { get; set; }
        public string ShippedViaDesc { get; set; }
        public string SupplierName { get; set; }
        public string Paymentterm { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? OrderedDate { get; set; }
        public DateTime? ExpDeliveryDate { get; set; }

        public string QuotationReference { get; set; }
        public string Currency { get; set; }

        public string Deliveryto { get; set; }
        public string Remarks { get; set; }

        public decimal OrderTotal { get; set; }
        public decimal DiscountPercentage { get; set; }

        public decimal GSTPercentage { get; set; }
        public decimal FreightCharges { get; set; }
        public decimal TaxPercentage { get; set; }
        public decimal TotalBaseCost { get; set; }

    }
    public class POItems
    {
        public int ID { get; set; }
        public int PurchaseOrderID { get; set; }

        public int PartNumber { get; set; }
        public string PartDescription { get; set; }
        public string UoM { get; set; }
        public decimal Quantity { get; set; }
        public string JobOrderID { get; set; }

        public decimal UnitPrice { get; set; }
        public decimal Discount { get; set; }
        public decimal Tax { get; set; }
        public decimal LineCost { get; set; }

        public decimal BaseCost { get; set; }
        public string Source { get; set; }
        public int PurchaseRequestItemID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string PurchaseRequestNo { get; set; }

    }        

    public class PRHeaderFooter
    {
        public string PRNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string status_desc { get; set; }
        public string auth_status_desc { get; set; }

        public string L2Name { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDeptdesc { get; set; }

        public string MaintSubDeptdesc { get; set; }

        public string CreatedByEmployeeName { get; set; }
        public string RequestedByEmployeeName { get; set; }
        public DateTime DateRequired { get; set; }
        public string Note { get; set; }

        public string Cancel_byName { get; set; }
        public string CancelNotes { get; set; }
        public DateTime Cancel_Date { get; set; }
    }
    public class PRItems
    {
        public int ID { get; set; }
        public string PurchaseRequestID { get; set; }

        public string StockNo { get; set; }
        public string PartDescription { get; set; }

        public string uom { get; set; }

        public decimal Quantity { get; set; }
        public string JobOrderID { get; set; }

        public decimal RefPrice { get; set; }
        public string Source { get; set; }

    }

}

