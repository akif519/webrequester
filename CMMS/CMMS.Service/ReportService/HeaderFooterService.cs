﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;

namespace CMMS.Service.ReportService
{
    public sealed class HeaderFooterService : DBExecute
    {
        #region "Constructor"

        public static int _AccountId { get; set; }
        public static bool _lang { get; set; }
        public HeaderFooterService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<HeaderFooterModel> getHeaderFooter(int AccountId, bool Lang)
        {
            if (AccountId > 0)
            {
                _AccountId = AccountId;
                _lang = Lang;
            }
            else
            {
                AccountId = _AccountId;
                Lang = _lang;
            }
            Account objAccount = CMMS.Service.UserAuthentication.UserAuthenticationData.GetAccountWithConnection(AccountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);

            //bool Lang = true; // ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;
            IList<HeaderFooterModel> list;
            string query = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            string url = ConfigurationManager.AppSettings["Url"];
            if (Lang)
            {
                query += "Select top 1 Country,District,MODA,GDMW,Branch_Name as BranchName,OMName as OMName,MaintDept as MaintDept, '" + url + "' + Replace(Branch_Logo,'..','') as BranchLogo, DeptOfficerPosition,DeptOfficerName,MaintDeptPosition,MaintDeptSprvsr ,ShowReportFooter from OrganizationDetails  ";
            }
            else
            {
                query += "Select top 1 Country_A as Country,District_A as District,MODA_A as MODA ,GDMW_A as GDMW,Branch_Name_A as BranchName,OMName_A as OMName,MaintDept_A as MaintDept, '" + url + "' + Replace(Branch_Logo,'..','')  as BranchLogo, DeptOfficerPosition_A as DeptOfficerPosition,DeptOfficerName_A as DeptOfficerName,MaintDeptPosition_A as MaintDeptPosition,MaintDeptSprvsr_A as MaintDeptSprvsr ,ShowReportFooter from OrganizationDetails  ";
            }
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<HeaderFooterModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;


        }
    }
    public class HeaderFooterModel
    {
        public string Country { get; set; }
        public string District { get; set; }
        public string BranchName { get; set; }
        public string OMName { get; set; }
        public string MaintDept { get; set; }
        public string GDMW { get; set; }
        public string MODA { get; set; }
        public string BranchLogo { get; set; }
        public string DeptOfficerPosition { get; set; }
        public string DeptOfficerName { get; set; }
        public string MaintDeptPosition { get; set; }
        public string MaintDeptSprvsr { get; set; }

        public bool ShowReportFooter { get; set; }
    }
}
