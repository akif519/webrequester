﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class PreventiveReportService : DBExecute
    {
        #region "Constructor"

        public PreventiveReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<PMChecklistListModel> GetPMChecklistList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int maintDivisionId, string maintDeptId, string maintSubDeptId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMChecklistListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ROW_NUMBER() OVER (ORDER BY L2.L2Code, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, pmchecklist.ChecklistNo) AS RowNo, ";
            if (lang)
                query += "pmchecklist.CheckListName, ";
            else
                query += "pmchecklist.AltCheckListName As CheckListName, ";
            query += "pmchecklist.ChecklistNo, pmchecklist.EstimatedLaborHours, L2.L2Code, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode ";
            query += "from pmchecklist LEFT OUTER JOIN ";
            query += "L2 ON pmchecklist.L2ID = L2.L2ID LEFT OUTER JOIN ";
            query += "MainenanceDivision ON pmchecklist.MaintdivId = MainenanceDivision.MaintDivisionID LEFT OUTER JOIN ";
            query += "MaintenanceDepartment ON pmchecklist.maintDeptID = MaintenanceDepartment.maintDeptID LEFT JOIN ";
            query += "MaintSubDept ON pmchecklist.MainSubDeptId = MaintSubDept.MaintSubDeptID ";
            query += "where (L2.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(MainenanceDivision.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(MaintenanceDepartment.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(MaintSubDept.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "IsCleaningModule = 0 ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMChecklistListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMChecklistTasks1Model> GetPMChecklistTasks1(int accountId, bool isCentral, bool lang, int employeeId, int checklistId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMChecklistTasks1Model> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT checklistelements.SeqNo, ";
            if (lang)
                query += "checklistelements.TaskDesc ";
            else
                query += "checklistelements.AltTaskDesc As TaskDesc ";
            query += "FROM checklistelements ";
            query += "where (checklistelements.ChecklistID = '" + checklistId + "' or ISNULL('" + checklistId + "', 0) = 0) ";
            query += "order by checklistelements.SeqNo ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMChecklistTasks1Model>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMChecklistTasks2Model> GetPMChecklistTasks2(int accountId, bool isCentral, bool lang, int employeeId, int checklistId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMChecklistTasks2Model> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT stockcode.StockNo, stockcode.UOM, checklistInv.Qty, ";
            if (lang)
                query += "stockcode.StockDescription ";
            else
                query += "stockcode.AltStockDescription As StockDescription ";
            query += "FROM checklistInv LEFT OUTER JOIN ";
            query += "stockcode ON checklistInv.PartID = stockcode.StockID ";
            query += "where (checklistInv.ChecklistID = '" + checklistId + "' or ISNULL('" + checklistId + "', 0) = 0) ";
            query += "ORDER BY stockcode.StockNo ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMChecklistTasks2Model>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMComplianceModel> GetPMCompliance(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, string workTradeId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMComplianceModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT COUNT(*) as Cnt, 1 As ID, ";
            if (lang)
                query += "'Scheduled' As Name ";
            else
                query += "'من المقرر' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "UNION All SELECT COUNT(*) as Cnt, 2 As ID, ";
            if (lang)
                query += "'Closed' As Name ";
            else
                query += "'مغلق' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
            query += "(workorders.WorkStatusID = 2) AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "UNION All SELECT COUNT(*) as Cnt, 3 As ID, ";
            if (lang)
                query += "'Closed/Overdue' As Name ";
            else
                query += "'مغلق / المتأخرة' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
            query += "(workorders.WorkStatusID = 2) AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "UNION All SELECT COUNT(*) as Cnt, 4 As ID, ";
            if (lang)
                query += "'Pending' As Name ";
            else
                query += "'ريثما' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd < workorders.PMTarCompDate) AND ";
            query += "(workorders.WorkStatusID != 2) AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "UNION All SELECT COUNT(*) as Cnt, 5 As ID, ";
            if (lang)
                query += "'Overdue' As Name ";
            else
                query += "'تأخر' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.ActDateEnd > workorders.PMTarCompDate) AND ";
            query += "(workorders.WorkStatusID != 2) AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "UNION All SELECT COUNT(*) as Cnt, 6 As ID, ";
            if (lang)
                query += "'Cancelled' As Name ";
            else
                query += "'ألغي' As Name ";
            query += "from workorders ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(workorders.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(workorders.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(workorders.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(workorders.PMTarCompDate between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(workorders.WOTradeID in (select item from dbo.fnSplit('" + workTradeId + "', ',')) or ISNULL('" + workTradeId + "', '') = '') AND ";
            query += "(workorders.WorkStatusID = 3) AND ";
            query += "(workorders.WorkTypeID in (2,7)) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMComplianceModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<PMMaterialRequirementsModel> GetPMMaterialRequirements(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, DateTime dateFrom, DateTime dateTo, int forecastOption)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<PMMaterialRequirementsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            string strWhereClauseWithEmployee = "";
            if (!isCentral)
                strWhereClauseWithEmployee = " and w.L2ID in (select L2Id from employees_L2 where empID = " + employeeId + " union Select L2Id from employees where employeeId = " + employeeId + " ) ";

            if (employeeId > 0)
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + employeeId + " union Select MaintSubDeptID from employees where employeeId = " + employeeId + " ) ";

            if (!string.IsNullOrEmpty(l2Id))
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.L2ID = " + l2Id.ToString();

            if (maintDivisionId > 0)
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.MaintDivisionID = " + maintDivisionId.ToString();

            if (!string.IsNullOrEmpty(maintDeptId))
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) ";

            if (!string.IsNullOrEmpty(maintSubDeptId))
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) ";

            if (dateFrom != DateTime.MinValue)
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.PMTarStartDate >= '" + dateFrom.ToShortDateString() + "' ";

            if (dateTo != DateTime.MinValue)
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and w.PMTarCompDate <= '" + dateTo.ToShortDateString() + "' ";

            if (forecastOption == 1)
            {
                query += " SELECT Convert(int, ROW_NUMBER() OVER(ORDER BY s.StockNo)) AS RowNo, s.StockNo, s.UOM, ";
                if (lang)
                    query += "s.StockDescription, ";
                else
                    query += "s.AltStockDescription As StockDescription, ";
                query += " Convert(decimal(10, 3), SUM(woi.qty)) AS RequiredQty, Convert(decimal(10, 3), isnull((SELECT SUM(ISNULL(sl.Balance, 0)) FROM stockcode_levels sl WHERE sl.stock_id = s.StockID), 0)) AS CurrentBalance,";
                query += " (Convert(decimal(10, 3), isnull((SELECT SUM(ISNULL(sl.Balance, 0)) FROM stockcode_levels sl WHERE sl.stock_id = s.StockID), 0)) - Convert(decimal(10, 3), SUM(woi.qty))) As Variance, ";
                query += " SUM(s.LeadTime) AS LeadTime FROM stockcode s ";
                query += " INNER JOIN WorkOrderItems woi ON s.StockID = woi.StockID ";
                query += " WHERE woi.WorkOrderNo IN (SELECT w.WorkorderNo  FROM workorders w WHERE w.WorkStatusID NOT IN (2,3)  ";
                query += strWhereClauseWithEmployee;
                query += " ) GROUP BY s.StockID, s.StockNo, s.StockDescription, s.AltStockDescription, s.UOM ";
            }
            else
            {
                query += " select Convert(int, ROW_NUMBER() OVER(ORDER BY s.StockNo)) AS RowNo, s.StockNo, s.UOM, ";
                if (lang)
                    query += "s.StockDescription, ";
                else
                    query += "s.AltStockDescription As StockDescription, ";
                query += " Convert(decimal(10, 3), SUM(checklistinv.Qty)) AS RequiredQty, Convert(decimal(10, 3),isnull((SELECT SUM(ISNULL(sl.Balance, 0)) FROM stockcode_levels sl WHERE sl.stock_id = s.StockID), 0)) AS CurrentBalance, ";
                query += " (Convert(decimal(10, 3), isnull((SELECT SUM(ISNULL(sl.Balance, 0)) FROM stockcode_levels sl WHERE sl.stock_id = s.StockID), 0)) - Convert(decimal(10, 3), SUM(checklistinv.qty))) As Variance, ";
                query += " SUM(s.LeadTime) AS LeadTime from intermediate_pmgenerate INNER JOIN checklistinv ON intermediate_pmgenerate.ChecklistID = checklistinv.ChecklistID   ";
                query += " INNER JOIN stockcode s ON s.StockID = checklistinv.PartID ";
                query += " WHERE intermediate_pmgenerate.Userid = " + employeeId + " " + " GROUP BY s.StockID, s.StockNo, s.StockDescription, s.AltStockDescription, s.UOM ";
            }

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMMaterialRequirementsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public List<PMScheduleTable> GetAnnualPMSchedule(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, int Year)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            List<PMScheduleTable> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT WorkStatus.WorkStatus,WorkStatus.AltWorkStatus,WorkStatus.Color,w.PMTarStartDate, w.PMTarCompDate,Year(w.PMTarStartDate) as ZYear, ";
            query += "  ISNULL(Assets.AssetNumber, ' ') AS AssetNumber, ISNULL(Assets.AssetDescription , ' ') AS AssetDescription , ISNULL(LocationNo , ' ') AS LocationNo , ISNULL(LocationDescription , ' ') AS LocationDescription , ISNULL(WorkTrade , ' ') AS WorkTrade ,AltWorkTrade,pmschedule.PMNo,pmchecklist.ChecklistNo as TaskNo, ";
            query += "  datepart(ww,w.PMTarStartDate) as Week_No,pmchecklist.EstimatedLaborHours as Man_Hours  ";
            query += " from workorders as w inner join WorkStatus on  WorkStatus.WorkStatusID=w.WorkStatusID ";
            query += " left outer join Assets on w.assetid= Assets.assetid left outer join Location on  Location.LocationID=w.LocationID ";
            query += " inner join worktrade on worktrade.WorkTradeID= w.WOTradeID inner join pmschedule on pmschedule.PMId= w.PMID inner join pmchecklist on pmchecklist.ChecklistID= w.PMChecklistID ";
            query += " where pmschedule.IsCleaningModule=0  and (w.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(w.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(w.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(w.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "Year(w.PMTarStartDate)= " + Year ;
            if (!isCentral)
                query += "AND (w.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += " Union All ";
            query += " SELECT 'Schedule' WorkStatus,'Schedule' AltWorkStatus,'257' Color,TargetStartDate as PMTarStartDate,TargetCompDate as PMTarCompDate, ";
            query += " Year(TargetStartDate) as ZYear,ISNULL(AssetNumber, ' ') AS AssetNumber, ISNULL(AssetDescription , ' ') AS AssetDescription , ISNULL(LocationNo , ' ') AS LocationNo , ISNULL(LocationDescription , ' ') AS LocationDescription , ISNULL(WorkTrade , ' ') AS WorkTrade ,AltWorkTrade,PMNo,w.ChecklistNo as TaskNo, ";
            query += " datepart(ww,TargetStartDate) as Week_No,pmchecklist.EstimatedLaborHours as Man_Hours from intermediate_pmgenerate w inner join pmchecklist on pmchecklist.ChecklistID= w.ChecklistID  ";
            query += " WHERE w.Userid = " + employeeId + " and Year(TargetStartDate)= " + Year + " ";
            List<PMScheduleTable> lstPMScheduleTable = new List<PMScheduleTable>();
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<PMScheduleTable>(query, parameters, 0, DbType, ConnectionString) as List<PMScheduleTable>;
                PagingInformation = objDapperContext.PagingInformation;
            }
            lstPMScheduleTable = list;
            if (lstPMScheduleTable.Count > 0)
            {
                PMScheduleTable item = lstPMScheduleTable.ToArray()[0];
                int i = 1;
                for (i = 1; i <= 53; i++)
                {
                    if ((lstPMScheduleTable.Find(x => x.AssetNumber == item.AssetNumber & x.LocationNo == item.LocationNo & x.WorkTrade == item.WorkTrade & x.PMNo == item.PMNo & x.TaskNo == item.TaskNo & x.Week_No == i) == null))
                    {
                        PMScheduleTable itemInsert = new PMScheduleTable();
                        itemInsert.PMNo = item.PMNo;
                        itemInsert.ZYear = item.ZYear;
                        itemInsert.AssetNumber = item.AssetNumber;
                        itemInsert.AssetDescription = item.AssetDescription;
                        itemInsert.LocationNo = item.LocationNo;
                        itemInsert.LocationDescription = item.LocationDescription;
                        itemInsert.WorkTrade = item.WorkTrade;
                        itemInsert.AltWorkTrade = item.AltWorkTrade;
                        itemInsert.PMNo = item.PMNo;
                        itemInsert.TaskNo = item.TaskNo;
                        itemInsert.Week_No = i;
                        lstPMScheduleTable.Add(itemInsert);
                    }
                }

            }


            return lstPMScheduleTable;
        }
    }

    public class PMMaterialRequirementsModel
    {
        public int RowNo { get; set; }
        public string StockNo { get; set; }
        public string UOM { get; set; }
        public decimal RequiredQty { get; set; }
        public string StockDescription { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal Variance { get; set; }
        public int LeadTime { get; set; }
    }

    public class PMChecklistListModel
    {
        public int RowNo { get; set; }
        public string CheckListName { get; set; }
        public string ChecklistNo { get; set; }
        public decimal EstimatedLaborHours { get; set; }
        public string L2Code { get; set; }
        public string MaintDivisionCode { get; set; }
        public string MaintDeptCode { get; set; }
        public string MaintSubDeptCode { get; set; }
    }

    public class PMChecklistTasks1Model
    {
        public int SeqNo { get; set; }
        public string TaskDesc { get; set; }
    }

    public class PMChecklistTasks2Model
    {
        public string StockNo { get; set; }
        public string StockDescription { get; set; }
        public string UOM { get; set; }
        public decimal Qty { get; set; }
    }

    public class PMComplianceModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Cnt { get; set; }
    }

    public class PMScheduleTable
    {
        public string WorkStatus { get; set; }
        public string AltWorkStatus { get; set; }
        public string Color { get; set; }
        public DateTime? PMTarStartDate { get; set; }
        public DateTime? PMTarCompDate { get; set; }
        public int ZYear { get; set; }
        public string AssetNumber { get; set; }
        public string AssetDescription { get; set; }
        public string LocationNo { get; set; }
        public string LocationDescription { get; set; }
        public string WorkTrade { get; set; }
        public string AltWorkTrade { get; set; }
        public string PMNo { get; set; }
        public string TaskNo { get; set; }
        public int? Week_No { get; set; }
        public decimal? Man_Hours { get; set; }
    }
}
