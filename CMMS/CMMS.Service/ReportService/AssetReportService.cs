﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class AssetReportService : DBExecute
    {
        #region "Constructor"

        public AssetReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<AssetListModel> GetAssetList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "select assets.AssetNumber, L2.L2Code, assets.ModelNumber, assets.SerialNumber, assetcategories.AssetCatCode, ";
            if (lang)
                query += "assets.AssetDescription As AssetDesc, location.LocationNo + CHAR(13) + location.LocationDescription As LocationDesc, L3.L3No + CHAR(13) + L3.L3Desc As L3Desc, L4.L4No + CHAR(13) + L4.L4Description As L4Desc, L5.L5No + CHAR(13) + L5.L5Description As L5Desc ";
            else
                query += "assets.AssetAltDescription As AssetDesc, location.LocationNo + CHAR(13) + isnull(location.LocationAltDescription, '') As LocationDesc, L3.L3No + CHAR(13) + isnull(L3.L3AltDesc, '') As L3Desc, L4.L4No + CHAR(13) + isnull(L4.L4AltDescription, '') As L4Desc, L5.L5No + CHAR(13) + isnull(L5.L5AltDescription, '') As L5Desc ";
            query += "FROM assets INNER JOIN ";
            query += "location on assets.LocationID = location.LocationID Left outer join ";
            query += "L2 on location.L2ID = L2.L2ID left outer join ";
            query += "L3 on location.L3ID = L3.L3ID INNER JOIN ";
            query += "L4 on location.L4ID = L4.L4ID left outer join ";
            query += "L5 on location.L5ID = L5.L5ID left outer join ";
            query += "assetcategories on assets.AssetCatID = assetcategories.AssetCatID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "ORDER BY L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, assets.AssetNumber";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetDetailsModel> GetAssetDetails(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId)
        {
            Account objAccount = CMMS.Service.UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            string query = string.Empty;
            IList<AssetDetailsModel> list;
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            query += "select   assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, L2.L2Code, ";
            if (lang)
                query += "L2.L2name, location.LocationDescription, L3.L3Desc, L4.L4Description, L5.L5Description, ";
            else
                query += "L2.L2Altname As L2name, isnull(location.LocationAltDescription, '') As LocationDescription, isnull(L3.L3AltDesc, '') As L3Desc, isnull(L4.L4AltDescription, '') As L4Description, isnull(L5.L5AltDescription, '') As L5Description, ";
            query += " location.LocationNo, L3.L3No, L4.L4No, L5.L5No, ";
            query += " assetcategories.AssetCatCode, assetstatus.AssetStatusDesc, criticality.Criticality, worktrade.WorkTrade, ";
            query += " employees.EmployeeNO, assets.ModelNumber, assets.SerialNumber, assets.Manufacturer, suppliers.SupplierNo, ";
            query += " warrantycontract.Warranty_contract, assets.Warranty_ContractExpiry, assets.Warranty_ContractNotes, ";
            query += " assets.Comments, assets.PurchasePrice, assets.DateAcquired, assets.DataDisposed, assets.EstLife, assets.NotesToTech";
            query += " FROM assets INNER JOIN ";
            query += " location on assets.LocationID = location.LocationID Left outer join L2 ON location.L2ID = L2.L2ID LEFT OUTER JOIN ";
            query += " L3 ON location.L3ID = L3.L3ID INNER JOIN L4 ON location.L4ID = L4.L4ID LEFT OUTER JOIN L5 ON location.L5ID = L5.L5ID LEFt OUTER JOIN ";
            query += " assetcategories on assets.AssetCatID = assetcategories.AssetCatID Left outer join assetstatus on assets.AssetStatusID = assetstatus.AssetStatusID left outer join ";
            query += " criticality on assets.CriticalityID = criticality.id left outer join worktrade on assets.WorkTradeID = worktrade.WorkTradeID left outer join ";
            query += " employees on assets.EmployeeID = employees.EmployeeID left outer join suppliers on assets.SupplierID = suppliers.SupplierID left outer join ";
            query += " warrantycontract on assets.Warranty_ContractID = warrantycontract.Warranty_contractID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetDetailsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetMaintenanceHistoryModel> GetAssetMaintenanceHistory(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int assetId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetMaintenanceHistoryModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "select workorders.WorkorderNo, workorders.ProblemDescription, workorders.ActionTaken, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, ";
            else
                query += "worktype.AltWorkTypeDescription + CHAR(13) + workstatus.AltWorkStatus + CHAR(13) + workpriority.AltWorkPriority + CHAR(13) + worktrade.AltWorkTrade As WorkDesc, assets.AssetNumber + CHAR(13) + assets.AssetAltDescription As AssetDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code,'') + CHAR(13) + isnull(L3.L3No,'') + CHAR(13) + isnull(L4.L4No,'') + CHAR(13) + isnull(L5.L5No,'') As LDesc, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.DateReceived AS DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateEnd AS DATE)), '') As DateRange, ";
            query += "CAST(ISNULL((select SUM(workorderlabor.TotCost) FROM workorderlabor where workorders.WorkorderNo = workorderlabor.WorkorderNo), 0) + ";
            query += "ISNULL((select SUM(direct.Qty * direct.Price) FROM direct where direct.WONo = workorders.WorkorderNo), 0) + ";
            query += "(ISNULL((select SUM(issue.QtyIssue * issue.Price3) FROM issue where issue.WONo = workorders.WorkorderNo), 0) - ";
            query += "ISNULL((select  SUM([return].Qty * [return].RetPrice) FROM [return] where [return].WONo = workorders.WorkorderNo ), 0)) AS DECIMAL(18, 3)) as TotalCost, ";
            query += "CONVERT(VARCHAR, CAST(ISNULL((select SUM(workorderlabor.TotCost) FROM workorderlabor where workorders.WorkorderNo = workorderlabor.WorkorderNo), 0) AS DECIMAL(18, 3))) + CHAR(13) + ";
            query += "CONVERT(VARCHAR, CAST(ISNULL((select SUM(direct.Qty * direct.Price) FROM direct where direct.WONo = workorders.WorkorderNo), 0) AS DECIMAL(18, 3))) + CHAR(13) + ";
            query += "CONVERT(VARCHAR, CAST(ISNULL((select SUM(issue.QtyIssue * issue.Price3) FROM issue where issue.WONo = workorders.WorkorderNo), 0) - ";
            query += "ISNULL((select  SUM([return].Qty * [return].RetPrice) FROM [return] where [return].WONo = workorders.WorkorderNo ), 0) AS DECIMAL(18, 3))) As LabourCost ";
            query += "FROM workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID	left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(workorders.AssetID = '" + assetId + "' or ISNULL('" + assetId + "', 0) = 0) AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group by workorders.WorkorderNo, workorders.ProblemDescription, workorders.ActionTaken, ";
            query += "worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, workstatus.WorkStatus, workstatus.AltWorkStatus, ";
            query += "workpriority.WorkPriority, workpriority.AltWorkPriority, worktrade.WorkTrade, worktrade.AltWorkTrade, ";
            query += "MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, ";
            query += "workorders.DateReceived, workorders.ActDateEnd, L2Code, L3No, L4No, L5No, ";
            query += "assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, location.LocationNo ";
            query += "ORDER BY workorders.ActDateEnd desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetMaintenanceHistoryModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetDownTimeModel> GetTop10AssetDownTime(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            string query = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += " Select top 10 assets.AssetNumber, L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, ";
            if (lang)
                query += "assets.AssetDescription, ";
            else
                query += "assets.AssetAltDescription As AssetDescription, ";
            query += " Downtime, assets.AssetID from (Select isnull(AssetID, 0) as AssetID, SUM(workorders.DownTime) as Downtime ";
            query += " from workorders where DateReceived  between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "' ";
            query += " AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3 group by AssetID) as T ";
            query += " inner join assets on assets.AssetID = T.AssetID ";
            query += " inner join location on assets.LocationID = location.LocationID ";
            query += " left outer join L2 on location.L2ID = L2.L2ID ";
            query += " left outer join L3 on location.L3ID = L3.L3ID ";
            query += " left outer join L4 on location.L4ID = L4.L4ID ";
            query += " left outer join L5 on location.L5ID = L5.L5ID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            IList<AssetDownTimeModel> list;
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetDownTimeModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }
            
            return list;
        }

        public IList<AssetDownTimeModel> GetTop10AssetBreakDown(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            string query = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += " Select top 10 assets.AssetNumber, L2.L2Code, L3.L3No, L4.L4No, L5.L5No, location.LocationNo, ";
            if (lang)
                query += "assets.AssetDescription, ";
            else
                query += "assets.AssetAltDescription As AssetDescription, ";
            query += " Downtime, assets.AssetID from (Select isnull(AssetID, 0) as AssetID, COUNT(*) as Downtime ";
            query += " from workorders where DateReceived  between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "' ";
            query += " AND workorders.WorkStatusID != 3 AND WorkTypeID=3 group by AssetID) as T ";
            query += " inner join assets on assets.AssetID = T.AssetID ";
            query += " inner join location on assets.LocationID = location.LocationID ";
            query += " left outer join L2 on location.L2ID = L2.L2ID ";
            query += " left outer join L3 on location.L3ID = L3.L3ID ";
            query += " left outer join L4 on location.L4ID = L4.L4ID ";
            query += " left outer join L5 on location.L5ID = L5.L5ID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            IList<AssetDownTimeModel> list;
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetDownTimeModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetMTBFModel> GetAssetMTBF(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, string assetCategoryId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = CMMS.Service.UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            string query = string.Empty;
            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += " Select t.AssetID, t.AssetNumber, (DateRange / NoOfFailure) as MTBF from ";
            query += " (Select ISNULL(assets.AssetID, 0) As AssetID, ISNULL(assets.AssetNumber, 0) AS AssetNumber, ";
            query += " ((isnull(DATEDIFF(DAY, (Select case when assets.DateAcquired between '" + dateFrom.ToShortDateString() + "' and  '" + dateTo.ToShortDateString() + "' then assets.DateAcquired  else '" + dateFrom.ToShortDateString() + "' end), '" + dateTo.ToShortDateString() + "'), 0))) as DateRange ";
            query += " from workorders inner join assets on workorders.AssetID = assets.AssetID where ";
            query += " workorders.WorkStatusID != 3 AND (workorders.WorkTypeID Not in (2, 7)) group by assets.AssetID, assets.DateAcquired, assets.AssetNumber)  As T ";
            query += " inner join (select ISNULL(AssetID, 0) as AssetID, COUNT(workorders.AssetID) As NoOfFailure from workorders ";
            query += " where workorders.WorkTypeID not in (2, 7) AND (workorders.ActDateEnd between '" + dateFrom.ToShortDateString() + "' and  '" + dateTo.ToShortDateString() + "') Group By AssetID) as T1 on T.AssetID = T1.AssetID ";
            query += " inner join assets on T.AssetID = assets.AssetID ";
            query += " inner join location on assets.LocationID = location.LocationID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(assets.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "', ',')) or ISNULL('" + assetCategoryId + "', '') = '') ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";
            
            query += " order by MTBF desc ";

            IList<AssetMTBFModel> list;
            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetMTBFModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetBreakdownFrequencyModel> GetAssetBreakdownFrequency(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, string assetCategoryId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetBreakdownFrequencyModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select assets.AssetNumber, COUNT(workorders.WorkorderNo) as Total, assets.AssetID ";
            query += "FROM assets inner join ";
            query += "workorders on assets.AssetID = workorders.assetid inner join ";
            query += "location on assets.LocationID = location.LocationID ";
            query += "where assets.AssetID in (Select workorders.AssetID from workorders where workorders.DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "workorders.WorkStatusID != 3 AND workorders.WorkTypeID = 3 AND ";
            query += "(workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(workorders.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(workorders.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(workorders.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(assets.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "', ',')) or ISNULL('" + assetCategoryId + "', '') = '') ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group BY assets.AssetNumber, assets.AssetID";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetBreakdownFrequencyModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetBreakdownDowntimeModel> GetAssetBreakdownDowntime(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, string assetCategoryId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetBreakdownDowntimeModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select assets.AssetNumber, assets.AssetID, T.Downtime ";
            query += "from (Select isnull(AssetID,0) as AssetID, SUM(workorders.DownTime) as Downtime FROM workorders ";
            query += "where DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "' AND workorders.WorkStatusID = 2 AND workorders.WorkTypeID = 3 group by AssetID) as T ";
            query += "inner join assets on assets.AssetID = T.assetid ";
            query += "inner join location on assets.LocationID = location.LocationID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(assets.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "',',')) or ISNULL('" + assetCategoryId + "', '') = '') ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "order by t.Downtime Desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetBreakdownDowntimeModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<ManHoursByAssetModel> GetManHoursByAsset(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, string assetCategoryId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<ManHoursByAssetModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select AssetNumber, ";
            query += "(Select isnull(SUM(TotHour), 0) from workorderlabor where WorkorderNo in ";
            query += "(Select workorderNo from workorders where workorders.assetId = assets.assetId and WorkTypeId not in (2, 7) and workorders.WorkStatusID = 2)) as NonPreventive, ";
            query += "(Select isnull(SUM(TotHour), 0) from workorderlabor where WorkorderNo in ";
            query += "(Select workorderNo from workorders where workorders.assetId = assets.assetId and WorkTypeId in (2,7) and workorders.WorkStatusID = 2)) as Preventive, ";
            query += "(Select isnull(SUM(TotHour), 0) from workorderlabor where WorkorderNo in ";
            query += "(Select workorderNo from workorders where workorders.assetId = assets.assetId and workorders.WorkStatusID = 2)) as Total, assets.AssetID ";
            query += "from assets ";
            query += "inner join location on assets.LocationID = location.LocationID ";
            query += "where assets.AssetID in (Select workorders.AssetID from workorders where workorders.WorkStatusID = 2 AND ";
            query += "workorders.ActDateEnd between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "') AND ";
            query += "(location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(assets.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "',',')) or ISNULL('" + assetCategoryId + "', '') = '') ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<ManHoursByAssetModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<AssetMTTRModel> GetAssetMTTR(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, string l3Id, string l4Id, string l5Id, int locationId, string assetCategoryId, string workTypeId, DateTime dateFrom, DateTime dateTo)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetMTTRModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "Select assets.AssetNumber, assets.AssetID, T.MTTF ";
            query += "from (Select ISNULL(assetid, 0) As AssetID,(isnull(SUM(isnull(DATEDIFF(MINUTE, isnull(workorders.ActDateStart, 0),isnull(workorders.ActDateEnd, 0)), 0)), 0)/COUNT(workorders.WorkorderNo)) as MTTF ";
            query += "from workorders where DateReceived between '" + dateFrom.ToShortDateString() + "' and '" + dateTo.ToShortDateString() + "' ";
            query += "AND workorders.WorkStatusID = 2 AND (workorders.WorkTypeID in (select item from dbo.fnSplit('" + workTypeId + "', ',')) or ISNULL('" + workTypeId + "', '') = '') group by AssetID ) As T ";
            query += "inner join assets on assets.AssetID = T.AssetID ";
            query += "inner join location on assets.LocationID = location.LocationID ";
            query += "where (location.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(location.L3ID in (select item from dbo.fnSplit('" + l3Id + "', ',')) or ISNULL('" + l3Id + "', '') = '') AND ";
            query += "(location.L4ID in (select item from dbo.fnSplit('" + l4Id + "', ',')) or ISNULL('" + l4Id + "', '') = '') AND ";
            query += "(location.L5ID in (select item from dbo.fnSplit('" + l5Id + "', ',')) or ISNULL('" + l5Id + "', '') = '') AND ";
            query += "(assets.LocationID = '" + locationId + "' or ISNULL('" + locationId + "', 0) = 0) AND ";
            query += "(assets.AssetCatID in (select item from dbo.fnSplit('" + assetCategoryId + "', ',')) or ISNULL('" + assetCategoryId + "', '') = '') ";

            if (!isCentral)
                query += "AND (location.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "order by T.MTTF desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetMTTRModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
        
        public IList<AssetMaintenanceHistoryNoCostModel> GetAssetMaintenanceHistoryNoCost(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int assetId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<AssetMaintenanceHistoryNoCostModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "select workorders.WorkorderNo, workorders.ProblemDescription, workorders.ActionTaken, ";
            if (lang)
                query += "worktype.WorkTypeDescription + CHAR(13) + workstatus.WorkStatus + CHAR(13) + workpriority.WorkPriority + CHAR(13) + worktrade.WorkTrade As WorkDesc, assets.AssetNumber + CHAR(13) + assets.AssetDescription As AssetDesc, ";
            else
                query += "worktype.AltWorkTypeDescription + CHAR(13) + workstatus.AltWorkStatus + CHAR(13) + workpriority.AltWorkPriority + CHAR(13) + worktrade.AltWorkTrade As WorkDesc, assets.AssetNumber + CHAR(13) + assets.AssetAltDescription As AssetDesc, ";
            query += "MainenanceDivision.MaintDivisionCode + CHAR(13) + MaintenanceDepartment.MaintDeptCode + CHAR(13) + MaintSubDept.MaintSubDeptCode As MaintDesc, ";
            query += "ISNULL(L2.L2Code, '') + CHAR(13) + isnull(L3.L3No, '') + CHAR(13) + isnull(L4.L4No, '') + CHAR(13) + isnull(L5.L5No, '') As LDesc, ";
            query += "ISNULL(CONVERT(VARCHAR(19), CAST(workorders.DateReceived AS DATE)), '') + CHAR(13) + ISNULL(CONVERT(VARCHAR(19), CAST(workorders.ActDateEnd AS DATE)), '') As DateRange ";
            query += "FROM workorders inner join ";
            query += "workstatus on workorders.WorkStatusID = workstatus.WorkStatusID inner join ";
            query += "worktype on workorders.WorkTypeID = worktype.WorkTypeID inner join ";
            query += "worktrade on workorders.WOTradeID = worktrade.WorkTradeID inner join ";
            query += "workpriority on workorders.WorkPriorityID = workpriority.WorkPriorityID inner join ";
            query += "MainenanceDivision on workorders.MaintDivisionID = MainenanceDivision.MaintDivisionID inner join ";
            query += "MaintenanceDepartment on workorders.maintdeptID = MaintenanceDepartment.maintDeptID inner join ";
            query += "MaintSubDept on workorders.maintsubdeptID = MaintSubDept.MaintSubDeptID inner join ";
            query += "L2 on workorders.L2ID = L2.L2ID left outer join ";
            query += "L3 on workorders.L3ID = L3.L3ID left outer join ";
            query += "L4 on workorders.L4ID = L4.L4ID left outer join ";
            query += "L5 on workorders.L5ID = L5.L5ID left outer join ";
            query += "location on workorders.LocationID = location.LocationID left outer join ";
            query += "assets on workorders.AssetID = assets.AssetID ";
            query += "where (workorders.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', '') = '') AND ";
            query += "(workorders.AssetID = '" + assetId + "' or ISNULL('" + assetId + "', 0) = 0) AND ";
            query += "(workorders.WorkStatusID = 2) ";

            if (!isCentral)
                query += "AND (workorders.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            query += "Group by workorders.WorkorderNo, workorders.ProblemDescription, workorders.ActionTaken, ";
            query += "worktype.WorkTypeDescription, worktype.AltWorkTypeDescription, workstatus.WorkStatus, workstatus.AltWorkStatus, ";
            query += "workpriority.WorkPriority, workpriority.AltWorkPriority, worktrade.WorkTrade, worktrade.AltWorkTrade, ";
            query += "MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, ";
            query += "workorders.DateReceived, workorders.ActDateEnd, L2Code, L3No, L4No, L5No, ";
            query += "assets.AssetNumber, assets.AssetDescription, assets.AssetAltDescription, location.LocationNo ";
            query += "ORDER BY workorders.ActDateEnd desc";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<AssetMaintenanceHistoryNoCostModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class AssetMTBFModel
    {
        public int AssetId { get; set; }
        public string AssetNumber { get; set; }
        public int MTBF { get; set; }
    }

    public class AssetDownTimeModel
    {
        public int RowNo { get; set; }
        public string AssetDescription { get; set; }
        public string L2Code { get; set; }
        public string L3No { get; set; }
        public string L4No { get; set; }
        public string L5No { get; set; }
        public string LocationNo { get; set; }
        public int DownlTime { get; set; }
        public int AssetId { get; set; }
        public string AssetNumber { get; set; }
    }

    public class AssetListModel
    {
        public string AssetNumber { get; set; }
        public string AssetDesc { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string AssetCatCode { get; set; }
        public string L2Code { get; set; }
        public string L3Desc { get; set; }
        public string L4Desc { get; set; }
        public string L5Desc { get; set; }
        public string LocationDesc { get; set; }
    }

    public class AssetMaintenanceHistoryModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string ActionTaken { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string AssetDesc { get; set; }
        public string DateRange { get; set; }
        public string LabourCost { get; set; }
        public decimal TotalCost { get; set; }
    }

    public class AssetMaintenanceHistoryNoCostModel
    {
        public string WorkorderNo { get; set; }
        public string ProblemDescription { get; set; }
        public string ActionTaken { get; set; }
        public string WorkDesc { get; set; }
        public string MaintDesc { get; set; }
        public string LDesc { get; set; }
        public string AssetDesc { get; set; }
        public string DateRange { get; set; }
    }

    public class AssetBreakdownFrequencyModel
    {
        public string AssetNumber { get; set; }
        public int Total { get; set; }
        public int AssetID { get; set; }
    }

    public class AssetBreakdownDowntimeModel
    {
        public string AssetNumber { get; set; }
        public decimal? Downtime { get; set; }
        public int AssetID { get; set; }
    }

    public class ManHoursByAssetModel
    {
        public string AssetNumber { get; set; }
        public decimal? NonPreventive { get; set; }
        public decimal? Preventive { get; set; }
        public decimal? Total { get; set; }
        public int AssetID { get; set; }
    }

    public class AssetMTTRModel
    {
        public string AssetNumber { get; set; }
        public decimal? MTTF { get; set; }
        public int AssetID { get; set; }
    }
    public class AssetDetailsModel
    {
        public string AssetNumber { get; set; }
        public string AssetDescription { get; set; }
        public string AssetAltDescription { get; set; }
        public string L2Code { get; set; }
        public string L2name { get; set; }
        public string LocationNo { get; set; }
        public string LocationDescription { get; set; }
        public string L3No { get; set; }
        public string L3Desc { get; set; }
        public string L4No { get; set; }
        public string L4Description { get; set; }
        public string L5No { get; set; }
        public string L5Description { get; set; }
        public string AssetCatCode { get; set; }
        public string AssetStatusDesc { get; set; }
        public string Criticality { get; set; }
        public string WorkTrade { get; set; }
        public string EmployeeNO { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Manufacturer { get; set; }
        public string SupplierNo { get; set; }
        public string Warranty_contract { get; set; }
        public DateTime? Warranty_ContractExpiry { get; set; }
        public string Warranty_ContractNotes { get; set; }
        public string Comments { get; set; }
        public float? PurchasePrice { get; set; }
        public DateTime? DateAcquired { get; set; }
        public DateTime? DataDisposed { get; set; }
        public float? EstLife { get; set; }
        public string NotesToTech { get; set; }
    }
}
