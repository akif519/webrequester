﻿using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.SessionState;

namespace CMMS.Service.ReportService
{
    public class EmployeeReportService : DBExecute
    {
        #region "Constructor"

        public EmployeeReportService()
        {
            PagingInformation = new Pagination { PageSize = ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        #endregion

        public IList<EmployeeRequesterListModel> GetEmployeeRequesterList(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, string employeeCategoryId, string employeeStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<EmployeeRequesterListModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ROW_NUMBER() OVER (ORDER BY employees.Name, L2.L2name) AS RowNo, employees.EmployeeNO, employees.Name, ";
            query += "employees.Positions, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, ";
            if (lang)
                query += "employeecategories.CategoryName, employeeStatuses.EmployeeStatus, L2.L2name, ";
            else
                query += "employeecategories.AltCategoryName As CategoryName, employeeStatuses.EmployeeAltStatus As EmployeeStatus, L2.L2Altname As L2name, ";
            query += "L2.L2ID, L2.L2Code ";
            query += "FROM employees INNER JOIN ";
            query += "MainenanceDivision ON employees.MaintDivisionID = MainenanceDivision.MaintDivisionID INNER JOIN ";
            query += "MaintenanceDepartment ON employees.maintDeptID = MaintenanceDepartment.maintDeptID INNER JOIN ";
            query += "MaintSubDept ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID INNER JOIN ";
            query += "L2 ON employees.L2ID = L2.L2ID LEFT OUTER JOIN ";
            query += "employeecategories ON employees.CategoryID = employeecategories.CategoryID LEFT OUTER JOIN ";
            query += "employeeStatuses ON employees.EmployeeStatusId = employeeStatuses.EmployeeStatusId ";
            query += "where (L2.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(MainenanceDivision.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(MaintenanceDepartment.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(MaintSubDept.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(employeecategories.CategoryID in (select item from dbo.fnSplit('" + employeeCategoryId + "', ',')) or ISNULL('" + employeeCategoryId + "', '') = '') AND ";
            query += "(employeeStatuses.EmployeeStatusId in (select item from dbo.fnSplit('" + employeeStatusId + "', ',')) or ISNULL('" + employeeStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<EmployeeRequesterListModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }

        public IList<EmployeeRequesterDetailsModel> GetEmployeeRequesterDetails(int accountId, bool isCentral, bool lang, int employeeId, string l2Id, int maintDivisionId, string maintDeptId, string maintSubDeptId, string employeeCategoryId, string employeeStatusId)
        {
            Account objAccount = UserAuthentication.UserAuthenticationData.GetAccountWithConnection(accountId, 1);
            string ConnectionString = objAccount.DbConString;
            int DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
            IList<EmployeeRequesterDetailsModel> list;
            string query = string.Empty;

            Collection<DBParameters> parameters = new Collection<DBParameters>();

            query += "SELECT ROW_NUMBER() OVER (ORDER BY L2.L2name, employees.Name) AS RowNo, employees.EmployeeNO, employees.Name,employees.Address, ";
            query += "employees.Positions, employees.WorkPhone, employees.Extension, employees.Fax, employees.HandPhone, employees.Housephone, employees.OfficeLocation, ";
            query += "employees.Email, L2.L2Code, MainenanceDivision.MaintDivisionCode, MaintenanceDepartment.MaintDeptCode, MaintSubDept.MaintSubDeptCode, tbl_LanguageSelection.Language, ";
            query += "employees.HourlySalary, employees.OverTime1, employees.OverTime2, employees.OverTime3, ";
            if (lang)
                query += "L2.L2name, MainenanceDivision.MaintDivisionName, MaintenanceDepartment.MaintDeptdesc, MaintSubDept.MaintSubDeptDesc, worktrade.WorkTrade, employeecategories.CategoryName, employeeStatuses.EmployeeStatus, usergroups.GroupName ";
            else
                query += "L2.L2Altname As L2name, MainenanceDivision.MaintDivisionAltName As MaintDivisionName, MaintenanceDepartment.MaintDeptAltdesc As MaintDeptdesc, MaintSubDept.MaintSubDeptAltDesc As MaintSubDeptDesc, worktrade.AltWorkTrade As WorkTrade, employeecategories.AltCategoryName As CategoryName, employeeStatuses.EmployeeAltStatus As EmployeeStatus, usergroups.AltGroupName As GroupName ";
            query += "FROM employees INNER JOIN ";
            query += "MainenanceDivision ON employees.MaintDivisionID= MainenanceDivision.MaintDivisionID INNER JOIN ";
            query += "MaintenanceDepartment ON employees.maintDeptID = MaintenanceDepartment.maintDeptID INNER JOIN ";
            query += "MaintSubDept ON employees.MaintSubDeptID = MaintSubDept.MaintSubDeptID INNER JOIN ";
            query += "L2 ON employees.L2ID = L2.L2ID LEFT OUTER JOIN ";
            query += "employeecategories ON employees.CategoryID = employeecategories.CategoryID LEFT OUTER JOIN ";
            query += "employeeStatuses ON employees.EmployeeStatusId = employeeStatuses.EmployeeStatusId LEFT OUTER JOIN ";
            query += "tbl_LanguageSelection on employees.LanguageCode = tbl_LanguageSelection.LanguageCode LEFT OUTER JOIN ";
            query += "worktrade on employees.WorkTradeID = worktrade.WorkTradeID LEFT OUTER JOIN ";
            query += "usergroups on employees.UserGroupId = usergroups.UserGroupId ";
            query += "where (L2.L2ID = '" + l2Id + "' or ISNULL('" + l2Id + "', 0) = 0) AND ";
            query += "(MainenanceDivision.MaintDivisionID = '" + maintDivisionId + "' or ISNULL('" + maintDivisionId + "', 0) = 0) AND ";
            query += "(MaintenanceDepartment.maintdeptID in (select item from dbo.fnSplit('" + maintDeptId + "', ',')) or ISNULL('" + maintDeptId + "', '') = '') AND ";
            query += "(MaintSubDept.maintsubdeptID in (select item from dbo.fnSplit('" + maintSubDeptId + "', ',')) or ISNULL('" + maintSubDeptId + "', '') = '') AND ";
            query += "(employeecategories.CategoryID in (select item from dbo.fnSplit('" + employeeCategoryId + "', ',')) or ISNULL('" + employeeCategoryId + "', '') = '') AND ";
            query += "(employeeStatuses.EmployeeStatusId in (select item from dbo.fnSplit('" + employeeStatusId + "', ',')) or ISNULL('" + employeeStatusId + "', '') = '') ";

            if (!isCentral)
                query += "AND (L2.L2ID in (select L2Id from employees_L2 where empID = '" + employeeId + "' union Select L2Id from employees where employeeId = '" + employeeId + "')) ";

            using (DapperContext objDapperContext = new DapperContext())
            {
                list = objDapperContext.AdvanceSearch<EmployeeRequesterDetailsModel>(query, parameters, 0, DbType, ConnectionString);
                PagingInformation = objDapperContext.PagingInformation;
            }

            return list;
        }
    }

    public class EmployeeRequesterListModel
    {
        public int RowNo { get; set; }
        public string EmployeeNO { get; set; }
        public string Name { get; set; }
        public string Positions { get; set; }
        public string MaintDivisionCode { get; set; }
        public string MaintDeptCode { get; set; }
        public string MaintSubDeptCode { get; set; }
        public string CategoryName { get; set; }
        public string EmployeeStatus { get; set; }
        public string L2Name { get; set; }
        public int L2ID { get; set; }
        public string L2Code { get; set; }
    }

    public class EmployeeRequesterDetailsModel
    {
        public int RowNo { get; set; }
        public string EmployeeNO { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Positions { get; set; }
        public string WorkPhone { get; set; }
        public string Extension { get; set; }
        public string Fax { get; set; }
        public string HandPhone { get; set; }
        public string Housephone { get; set; }
        public string OfficeLocation { get; set; }
        public string Email { get; set; }
        public string L2Code { get; set; }
        public string L2name { get; set; }
        public string MaintDivisionCode { get; set; }
        public string MaintDivisionName { get; set; }
        public string MaintDeptCode { get; set; }
        public string MaintDeptdesc { get; set; }
        public string MaintSubDeptCode { get; set; }
        public string MaintSubDeptDesc { get; set; }
        public string Language { get; set; }
        public string WorkTrade { get; set; }
        public string HourlySalary { get; set; }
        public string OverTime1 { get; set; }
        public string OverTime2 { get; set; }
        public string OverTime3 { get; set; }
        public string CategoryName { get; set; }
        public string EmployeeStatus { get; set; }
        public string GroupName { get; set; }
    }
}
