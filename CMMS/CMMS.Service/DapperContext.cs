﻿using CMMS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using CMMS.DAL;


    public class DapperContext : DapperDBContext
    {
        
         #region Constructors

        /// <summary>
        /// Initializes a new instance of the DapperContext class.
        /// </summary>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public DapperContext()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = false;
        }

        /// <summary>
        /// Initializes a new instance of the DapperContext class for checking duplicate value for one column
        /// </summary>
        /// <param name="col1Name">column Name</param>
        public DapperContext(string col1Name)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.CombinationCheckRequired = false;
        }

        /// <summary>
        /// Initializes a new instance of the DapperContext class for checking duplicate value for two column with combination
        /// </summary>
        /// <param name="col1Name">first column name</param>
        /// <param name="col2Name">second column name</param>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public DapperContext(string col1Name, string col2Name)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.CombinationCheckRequired = true;
        }

        /// <summary>
        /// Initializes a new instance of the DapperContext class for checking duplicate value for two column
        /// </summary>
        /// <param name="col1Name">first column name</param>
        /// <param name="col2Name">second column name</param>
        /// <param name="combinationCheckRequired">Combination Check Required</param>
        /// <param name="isMasterDatabase">Is master database or not</param>
        public DapperContext(string col1Name, string col2Name, bool combinationCheckRequired)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.CombinationCheckRequired = combinationCheckRequired;
        }

        public DapperContext(string col1Name, string col2Name, string parentColumnName, bool combinationCheckRequired)
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };

            this.CheckForDuplicate = true;
            this.Col1Name = col1Name;
            this.Col2Name = col2Name;
            this.ParentColumnName = parentColumnName;

            this.CombinationCheckRequired = combinationCheckRequired;
        }

        #endregion

        #region Custom Methods

        #endregion
    }
}
