﻿using CMMS.Infrastructure;
using CMMS.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CMMS.Model;
using CMMS.DAL;
using System.Data;
using System.Collections.ObjectModel;
using System.Reflection;
using Kendo.Mvc;
using Kendo.Mvc.UI;


namespace CMMS.Service
{
    public class SearchFieldService : DBExecute
    {
        public SearchFieldService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        /// <summary>
        /// Gets the report fields.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns></returns>
        public static List<SearchField> GetSearchFields(int PageID)
        {
            List<SearchField> lstSearchFields = new List<SearchField>();

            using (ServiceContext reportFieldService = new ServiceContext())
            {
                SearchField objSearchField = new SearchField();
                Resources objResource = new Resources();
                objResource = ProjectSession.Resources;
                objSearchField.PageID = PageID;

                lstSearchFields = reportFieldService.Search<SearchField>(objSearchField).ToList();
                foreach (SearchField lst in lstSearchFields)
                {
                    PropertyInfo info = objResource.label.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lst.DisplayLabelName.ToString().ToLower());
                    if (info != null)
                    {
                        lst.DisplayName = Convert.ToString(info.GetValue(objResource.label));
                    }
                    else
                    {
                        info = objResource.message.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lst.DisplayLabelName.ToString().ToLower());
                        if (info != null)
                        {
                            lst.DisplayName = Convert.ToString(info.GetValue(objResource.message));
                        }
                        else
                        {
                            info = objResource.menu.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == lst.DisplayLabelName.ToString().ToLower());
                            if (info != null)
                            {
                                lst.DisplayName = Convert.ToString(info.GetValue(objResource.menu));
                            }
                            else
                            {
                                lst.DisplayName = lst.DisplayLabelName;
                            }
                        }
                    }
                }
            }
            return lstSearchFields;
        }

        public virtual string GetWhereClause(int PageID, int EmployeeID, bool IsSaved, out Collection<DBParameters> outParameters)
        {
            IList<SearchField> SearchFieldList;
            string strQuery = string.Empty;
            string whereClause = string.Empty;
            using (DapperContext objDapperContext = new DapperContext())
            {

                strQuery = "SELECT SFilter.SearchFieldID,OperatorID,Value1,Value2, ";
                strQuery += "[Condition]";
                strQuery += " ,TableName,DBColumnName,MainTableName  ";
                strQuery += "FROM SearchFilters SFilter ";
                strQuery += "INNER JOIN SearchFields SFields ON  SFilter.SearchFieldID = SFields.SearchFieldID  ";
                strQuery += "WHERE  SFields.PageID = @PageID And SFilter.EmployeeID = @EmployeeID AND SFilter.IsSaved = @IsSaved ";

                Collection<DBParameters> parameters = new Collection<DBParameters>();

                parameters.Add(new DBParameters() { Name = "PageID", Value = PageID, DBType = DbType.Int32 });
                parameters.Add(new DBParameters() { Name = "EmployeeID", Value = EmployeeID, DBType = DbType.Int32 });

                parameters.Add(new DBParameters() { Name = "IsSaved", Value = Convert.ToInt16(IsSaved), DBType = DbType.Int16 });

                SearchFieldList = objDapperContext.ExecuteQuery<SearchField>(strQuery, parameters);

                //objDapperContext.ex
            }

            int i = 1;
            string ParamName1, ParamName2, ParamValue1, ParamValue2;
            outParameters = new Collection<DBParameters>();

            foreach (SearchField searchFiled in SearchFieldList)
            {
                ParamName1 = "SearchFilterID" + i++;
                ParamValue1 = searchFiled.Value1;
                ParamValue2 = searchFiled.Value2;
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    whereClause += " lower("+ searchFiled.MainTableName + "." + searchFiled.DBColumnName + ") ";
                }
                else
                {
                    whereClause += searchFiled.MainTableName + "." + searchFiled.DBColumnName + "";
                }
                switch ((CMMS.Infrastructure.SystemEnum.SearchFieldsOperator)searchFiled.OperatorID)
                {
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.IsEqualTo:
                        whereClause += " = @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.IsNotEqualTo:
                        whereClause += " <> @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.GreaterThan:
                        whereClause += " > @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.LessThan:
                        whereClause += " < @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.Between:
                        ParamName2 = "SearchFilterID" + i++;
                        whereClause += " BETWEEN @" + ParamName1 + " AND @" + ParamName2 + "";
                        outParameters.Add(new DBParameters() { Name = ParamName2, Value = searchFiled.Value2 });
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.Contains:
                        whereClause += " LIKE @" + ParamName1 + "";
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            searchFiled.Value1 = searchFiled.Value1.ToString().ToLower();
                            ParamValue1 = "%" + searchFiled.Value1 + "%";
                        }
                        else
                        {
                            ParamValue1 = "%" + searchFiled.Value1 + "%";
                        }
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.StartsWith:
                        whereClause += " LIKE @" + ParamName1;
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            searchFiled.Value1 = searchFiled.Value1.ToString().ToLower();
                            ParamValue1 = searchFiled.Value1 + "%";
                        }
                        else
                        {
                            ParamValue1 = searchFiled.Value1 + "%";
                        }

                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.GreaterThanEqualTo:
                        whereClause += " >= @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.LessThanEqualTo:
                        whereClause += " <= @" + ParamName1 + "";
                        break;
                    case CMMS.Infrastructure.SystemEnum.SearchFieldsOperator.NotContains:
                        whereClause += " NOT IN (@" + ParamName1 + ")";
                        break;
                }
                outParameters.Add(new DBParameters() { Name = ParamName1, Value = ParamValue1 });
                whereClause += " " + searchFiled.Condition + " ";
            }

            return whereClause;
        }

        public string GetFilterCondition(FilterDescriptor f, ref Collection<DBParameters> outParameters, ref int i)
        {
            string whereClause = string.Empty;
            string ParamName1, ParamName2, ParamValue1, ParamValue2;



            ParamName1 = "FilterID" + i++;
            ParamValue1 = f.Value.ToString();
            //ParamValue2 = searchFiled.Value2;
            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(f.Member))
            {
                whereClause += "lower(" + f.Member + ") ";
            }
            else
            {
                whereClause += f.Member + "";
            }

            switch ((FilterOperator)f.Operator)
            {
                case FilterOperator.IsEqualTo:
                    whereClause += " = @" + ParamName1 + "";
                    break;
                case FilterOperator.IsNotEqualTo:
                    whereClause += " <> @" + ParamName1 + "";
                    break;
                case FilterOperator.IsGreaterThan:
                    whereClause += " > @" + ParamName1 + "";
                    break;
                case FilterOperator.IsLessThan:
                    whereClause += " < @" + ParamName1 + "";
                    break;
                case FilterOperator.EndsWith:
                    //ParamName2 = "SearchFilterID" + i++;
                    //whereClause += " BETWEEN @" + ParamName1 + " AND @" + ParamName2 + "";
                    //outParameters.Add(new DBParameters() { Name = ParamName2, Value = searchFiled.Value2 });
                    //break;
                    whereClause += " LIKE @" + ParamName1;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        f.Value = f.Value.ToString().ToLower();
                        ParamValue1 = "%" + f.Value;
                    }
                    else
                    {
                        ParamValue1 = "%" + f.Value;
                    }

                    break;
                case FilterOperator.IsContainedIn:
                    break;
                case FilterOperator.Contains:
                    whereClause += " LIKE @" + ParamName1 + "";
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        f.Value = f.Value.ToString().ToLower();
                        ParamValue1 = "%" + f.Value + "%";
                    }
                    else
                    {
                        ParamValue1 = "%" + f.Value + "%";
                    }
                    break;
                case FilterOperator.StartsWith:
                    whereClause += " LIKE @" + ParamName1;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        f.Value = f.Value.ToString().ToLower();
                        ParamValue1 = f.Value + "%";
                    }
                    else
                    {
                        ParamValue1 = f.Value + "%";
                    }

                    break;
                case FilterOperator.IsGreaterThanOrEqualTo:
                    whereClause += " >= @" + ParamName1 + "";
                    break;
                case FilterOperator.IsLessThanOrEqualTo:
                    whereClause += " <= @" + ParamName1 + "";
                    break;
                case FilterOperator.DoesNotContain:
                    whereClause += " NOT IN (@" + ParamName1 + ")";
                    break;
            }
            outParameters.Add(new DBParameters() { Name = ParamName1, Value = ParamValue1 });


            return whereClause;
        }

        public void GetCompositeFilter(CompositeFilterDescriptor fd, ref Collection<DBParameters> outParameters, ref string whereClause, ref int i)
        {
            int j = 0;

            foreach (IFilterDescriptor f in fd.FilterDescriptors)
            {
                if (f.GetType().Name == "FilterDescriptor")
                {
                    if (j == 0)
                        whereClause += "(" + GetFilterCondition((FilterDescriptor)f, ref outParameters, ref i) + " " + fd.LogicalOperator + " ";
                    else
                        whereClause += GetFilterCondition((FilterDescriptor)f, ref outParameters, ref i) + ") ";
                }
                else
                {
                    if (j == 0)
                        whereClause += "(";

                    GetCompositeFilter(((Kendo.Mvc.CompositeFilterDescriptor)(f)), ref outParameters, ref whereClause, ref i);
                    if (j == 0)
                    {
                        whereClause += " " + fd.LogicalOperator + " ";
                    }
                    else
                    {
                        whereClause += ")";
                    }
                }
                j++;
            }


        }

        public virtual IList<TEntity> AdvanceSearch<TEntity>(TEntity entity, int PageID, int EmployeeID, int pageNo, string sortExpression, string sortDirection, bool IsSaved = true, string extraWhereClause = "", [DataSourceRequest]DataSourceRequest request = null)
        {
            string whereClause = string.Empty;
            string query = string.Empty;
            string strWhereClause = string.Empty;
            IList<TEntity> list;
            try
            {
                Collection<DBParameters> outParameters = new Collection<DBParameters>();

                whereClause = GetWhereClause(PageID, ProjectSession.EmployeeID, IsSaved, out outParameters);
                //  strWhereClause = whereClause;
                int i = 1;
                if (request != null && request.Filters.Count > 0)
                {

                    foreach (IFilterDescriptor f in request.Filters)
                    {
                        if (f.GetType().Name == "FilterDescriptor")
                        {
                            strWhereClause += GetFilterCondition((FilterDescriptor)f, ref outParameters, ref i);

                        }
                        else
                        {
                            GetCompositeFilter(((CompositeFilterDescriptor)(f)), ref outParameters, ref strWhereClause, ref i);
                        }
                    }
                }

                Page page = new Page();

                using (DapperContext objDapperContext = new DapperContext())
                {
                    page = objDapperContext.SelectObject<Page>(PageID);
                }
                if (page != null)
                {
                    if (String.IsNullOrEmpty(whereClause))
                    {
                        whereClause = "1 = 1";
                        ProjectSession.IsAdvanceSearch = false;
                    }
                    else
                    {
                        ProjectSession.IsAdvanceSearch = true;
                    }

                    whereClause += extraWhereClause;

                    if (String.IsNullOrEmpty(strWhereClause))
                    {
                        query = "Select " + page.DisplayColumns + " " + page.TableJoins + " Where " + whereClause;
                    }
                    else
                    {
                        query = "Select * from (Select " + page.DisplayColumns + " " + page.TableJoins + " Where " + whereClause + ")A Where " + strWhereClause;
                    }


                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                //Collection<DBParameters> parameters = new Collection<DBParameters>();
                //parameters.Add(new DBParameters() { Name = "WhereClause", Value = whereClause, DBType = DbType.String });

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<TEntity>(query, outParameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;

            }

            finally
            {

            }
        }

        public string GetGridFilterWhereClause([DataSourceRequest]DataSourceRequest request = null, Collection<DBParameters> outParameters = null)
        {
            string strWhereClause = string.Empty;
            int i = 1;
            if (request != null && request.Filters.Count > 0)
            {

                foreach (IFilterDescriptor f in request.Filters)
                {
                    if (f.GetType().Name == "FilterDescriptor")
                    {
                        strWhereClause += GetFilterCondition((FilterDescriptor)f, ref outParameters, ref i);

                    }
                    else
                    {
                        GetCompositeFilter(((CompositeFilterDescriptor)(f)), ref outParameters, ref strWhereClause, ref i);
                    }
                }
            }
            return strWhereClause;
        }
    }
}
