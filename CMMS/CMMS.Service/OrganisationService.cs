﻿using CMMS.DAL;
using CMMS.Model;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Service
{
    public class OrganisationService : DBExecute
    {
        public OrganisationService()
        {
            this.PagingInformation = new Pagination() { PageSize = CMMS.Infrastructure.ProjectSession.PageSize, PagerSize = DefaultPagerSize };
        }

        public IList<L1> GetOraganisationByPermission(int logedInEmpID, int empID, int pageNo, string sortExpression, string sortDirection, [DataSourceRequest]DataSourceRequest request = null)
        {
            try
            {
                string strWhereClause = string.Empty;
                IList<L1> list = new List<L1>();
                Collection<DBParameters> parameters = new Collection<DBParameters>();

                string query = string.Empty;

                parameters.Add(new DBParameters()
                {
                    Name = "EMPID",
                    Value = empID,
                    DBType = DbType.Int32
                });

                if (logedInEmpID > 0)
                {
                    parameters.Add(new DBParameters()
                    {
                        Name = "LOGEDINEMPID",
                        Value = logedInEmpID,
                        DBType = DbType.Int32
                    });

                    query = @"Select t.*,Case When permissionCount >= totalCount AND totalCount > 0 Then 1 Else 0 End As isChecked
                              from 
                              (
                               Select L1.*,(Select Count(1) from (
	                            Select L2ID from employees_L2 Where EmpID =  @LOGEDINEMPID
	                            Union  Select L2ID from employees where employeeid = @LOGEDINEMPID )
	                            temp Inner JOin L2 On temp.L2ID = L2.L2ID AND L2.Status = 1 WHERE L2.L1ID = L1.L1ID ) totalCount,
	                            (Select Count(1) from (
	                            Select L2ID from employees_L2 Where EmpID =  @EMPID
	                            Union  Select L2ID from employees where employeeid = @EMPID )
	                            temp Inner JOin L2 On temp.L2ID = L2.L2ID AND L2.Status = 1 WHERE L2.L1ID = L1.L1ID 
                                AND L2.L2ID IN (Select Distinct L2.L2ID 
	                            from L2  
			                            Inner Join 
		                            Employees_L2 on L2.L2ID = Employees_L2.L2ID AND Employees_L2.EmpID = @LOGEDINEMPID
	                            Union 
		                            Select Distinct L2.L2ID 
	                            from L2 Where L2ID = (Select L2ID from employees Where EmployeeID = @LOGEDINEMPID)) 
                                ) permissionCount
                                from L1 Where L1ID In 
                              ( 
	                            Select Distinct L2.L1ID 
	                            from L2  
			                            Inner Join 
		                            Employees_L2 on L2.L2ID = Employees_L2.L2ID AND Employees_L2.EmpID = @LOGEDINEMPID
	                            Union 
		                            Select Distinct L2.L1ID 
	                            from L2 Where L2ID = (Select L2ID from employees Where EmployeeID = @LOGEDINEMPID)
                              )
                              ) t";

                    //query = "Select * from L1 Where L1ID In ( " +
                    //           " Select Distinct L1ID from L2  " +
                    //           " Inner Join Employees_L2 on L2.L2ID = Employees_L2.L2ID AND Employees_L2.EmpID = @EMPID) ";
                }
                else
                {
                    query = @"Select t.*,Case When permissionCount >= totalCount AND totalCount > 0 Then 1 Else 0 End As isChecked
                              from 
                              (
                                Select l1.*,
                                (Select count(1) from L2 Where L2.Status = 1 AND L2.L1ID = L1.L1ID) totalCount,
	                            (Select Count(1) from (
	                            Select L2ID from employees_L2 Where EmpID =  @EMPID
	                            Union  Select L2ID from employees where employeeid =  @EMPID )
	                            temp Inner JOin L2 On temp.L2ID = L2.L2ID AND L2.Status = 1 where L2.L1ID = L1.L1ID ) permissionCount
                                from L1 Where 1 = 1
                              ) t";
                    //query = "Select * from L1 where 1=1 ";
                }
               
                SearchFieldService objSearchField = new SearchFieldService();

                strWhereClause = objSearchField.GetGridFilterWhereClause(request, parameters);

                if (!string.IsNullOrEmpty(strWhereClause))
                {
                    query += " AND " + strWhereClause;
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    query += " Order by " + sortExpression;
                    if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
                        query += " Asc";
                    else if (sortDirection == System.ComponentModel.ListSortDirection.Descending.ToString())
                        query += " desc";
                }

                using (DapperContext objDapperContext = new DapperContext())
                {
                    list = objDapperContext.AdvanceSearch<L1>(query, parameters, pageNo);
                    this.PagingInformation = objDapperContext.PagingInformation;
                }

                return list;
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
