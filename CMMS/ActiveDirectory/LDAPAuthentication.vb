﻿Imports System.DirectoryServices
Imports System.Collections
Imports System.Text
Imports System.Configuration
Imports System.DirectoryServices.PropertyAccess


Public Class Users
    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(value As String)
            m_Email = Value
        End Set
    End Property
    Private m_Email As String
    Public Property UserName() As String
        Get
            Return m_UserName
        End Get
        Set(value As String)
            m_UserName = Value
        End Set
    End Property
    Private m_UserName As String
    Public Property DisplayName() As String
        Get
            Return m_DisplayName
        End Get
        Set(value As String)
            m_DisplayName = Value
        End Set
    End Property
    Private m_DisplayName As String
    Public Property isMapped() As Boolean
        Get
            Return m_isMapped
        End Get
        Set(value As Boolean)
            m_isMapped = Value
        End Set
    End Property
    Private m_isMapped As Boolean
End Class
Public Class LdapAuthentication
    Private _path As String
    Private _filterAttribute As String
    Private _UserDomain As String

    Public Function getAlUserList() As List(Of Users)
        Try
            Dim lstADUsers As New List(Of Users)
            Dim searchRoot As New DirectoryEntry(_path)
            Dim search As New DirectorySearcher(searchRoot)
            search.Filter = "(&(objectClass=user)(objectCategory=person))"
            search.PropertiesToLoad.Add("samaccountname")
            search.PropertiesToLoad.Add("mail")
            search.PropertiesToLoad.Add("usergroup")
            search.PropertiesToLoad.Add("displayname")
            'first name
            Dim result As SearchResult
            Dim resultCol As SearchResultCollection = search.FindAll()

            If resultCol IsNot Nothing Then
                For counter As Integer = 0 To resultCol.Count - 1
                    Dim UserNameEmailString As String = String.Empty
                    result = resultCol(counter)
                    Dim objSurveyUsers As New Users()
                    If result.Properties.Contains("samaccountname") Then
                        objSurveyUsers.UserName = DirectCast(result.Properties("samaccountname")(0), [String])
                    End If
                    If result.Properties.Contains("mail") Then
                        objSurveyUsers.Email = DirectCast(result.Properties("mail")(0), [String])
                    End If
                    If result.Properties.Contains("displayname") Then
                        objSurveyUsers.DisplayName = DirectCast(result.Properties("displayname")(0), [String])
                        End If
                        lstADUsers.Add(objSurveyUsers)
                Next
            End If
            Return lstADUsers
        Catch ex As Exception

        End Try
    End Function



    Public Sub New(ByVal UserDomain As String)
        _UserDomain = UserDomain
        _path = "LDAP://" + UserDomain
    End Sub

    Public Function IsAuthenticated(ByVal username As String, ByVal pwd As String) As Boolean

        Dim domainAndUsername As String = (_UserDomain & "\") + username
        Dim entry As New DirectoryEntry(_path, domainAndUsername, pwd)

        Try
            Dim obj As [Object] = entry.NativeObject
            Dim search As New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & username & ")"
            search.PropertiesToLoad.Add("cn")
            Dim result As SearchResult = search.FindOne()
            If result Is Nothing Then
                Return False
            End If
            _path = result.Path
            _filterAttribute = DirectCast(result.Properties("cn")(0), [String])
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function
    Public Function GetGroups() As String
        Dim search As New DirectorySearcher(_path)
        search.Filter = "(cn=" & _filterAttribute & ")"
        search.PropertiesToLoad.Add("memberOf")
        Dim groupNames As New StringBuilder()
        Try
            Dim result As SearchResult = search.FindOne()
            Dim propertyCount As Integer = result.Properties("memberOf").Count
            Dim dn As [String]
            Dim equalsIndex As Integer, commaIndex As Integer

            For propertyCounter As Integer = 0 To propertyCount - 1
                dn = DirectCast(result.Properties("memberOf")(propertyCounter), [String])

                equalsIndex = dn.IndexOf("=", 1)
                commaIndex = dn.IndexOf(",", 1)
                If -1 = equalsIndex Then
                    Return Nothing
                End If
                groupNames.Append(dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1))
                groupNames.Append("|")
            Next
        Catch ex As Exception
            Throw New Exception("Error obtaining group names. " & ex.Message)
        End Try
        Return groupNames.ToString()
    End Function
    Public Function IsExistInAD(ByVal loginName As String) As Boolean
        Dim userName As String = loginName
        Dim search As New DirectorySearcher()
        search.Filter = [String].Format("(SAMAccountName={0})", userName)
        search.PropertiesToLoad.Add("cn")
        Dim result As SearchResult = search.FindOne()

        If result Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function
    Public Function ExtractUserName(ByVal path As String) As String
        Dim userPath As String() = path.Split(New Char() {"\"c})
        Return userPath(userPath.Length - 1)
    End Function
    Public Function GetADUserGroups(ByVal userName As String) As String
        Dim search As New DirectorySearcher()
        search.Filter = [String].Format("(cn={0})", userName)
        search.PropertiesToLoad.Add("memberOf")
        Dim groupsList As New StringBuilder()

        Dim result As SearchResult = search.FindOne()
        If result IsNot Nothing Then
            Dim groupCount As Integer = result.Properties("memberOf").Count

            For counter As Integer = 0 To groupCount - 1
                groupsList.Append(DirectCast(result.Properties("memberOf")(counter), String))
                groupsList.Append("|")
            Next
        End If
        groupsList.Length -= 1

        Return groupsList.ToString()
    End Function

    Public Function GetADGroupUsers(ByVal groupName As String) As ArrayList
        Dim result As SearchResult
        Dim search As New DirectorySearcher()
        search.Filter = [String].Format("(cn={0})", groupName)
        search.PropertiesToLoad.Add("member")
        result = search.FindOne()

        Dim userNames As New ArrayList()
        If result IsNot Nothing Then
            For counter As Integer = 0 To result.Properties("member").Count - 1
                Dim user As String = DirectCast(result.Properties("member")(counter), String)
                userNames.Add(user)
            Next
        End If
        Return userNames
    End Function

    Public Function GetAllADDomainUsers() As ArrayList
        Dim allUsers As New ArrayList()

        Dim searchRoot As New DirectoryEntry(_path)
        Dim search As New DirectorySearcher(searchRoot)
        search.Filter = "(&(objectClass=user)(objectCategory=person))"
        search.PropertiesToLoad.Add("samaccountname")

        Dim result As SearchResult
        Dim resultCol As SearchResultCollection = search.FindAll()
        If resultCol IsNot Nothing Then
            For counter As Integer = 0 To resultCol.Count - 1
                result = resultCol(counter)
                If result.Properties.Contains("samaccountname") Then
                    allUsers.Add(DirectCast(result.Properties("samaccountname")(0), [String]))
                    allUsers.Add(DirectCast(result.Properties("telephoneNumber")(0), [String]))

                End If
            Next
        End If
        Return allUsers
    End Function
End Class