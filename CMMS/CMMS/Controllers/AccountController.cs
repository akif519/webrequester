﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CMMS.Models;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.UserAuthentication;
using Microsoft.Web.Administration;
using ActiveDirectory;

namespace CMMS.Controllers
{
    /// <summary>
    /// Account Controller
    /// </summary>    
    public class AccountController : Controller
    {
        #region Login-Logout

        /// <summary>
        /// Logins this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [ActionName(Actions.Login)]
        public ActionResult Login()
        {
            //this.ClearAllMessages();
            LoginModel model = new LoginModel();
            //if (logout)
            //{
            //    model.isActiveDirectory = "0";
            //    return View(Pages.Views.Login, model);
            //}
            model.isActiveDirectory = ProjectConfiguration.IsActiveDirectory;
            if (ProjectConfiguration.IsActiveDirectory == "1")
            {
                try
                {
                    string CurUser = String.Empty;
                    try
                    {
                        string Aut_CurUser = Request.ServerVariables["AUTH_USER"];
                        string Logon_CurUser = Request.ServerVariables["LOGON_USER"];
                        String LoginUser = string.Empty;
                        if (!string.IsNullOrEmpty(Logon_CurUser))
                        {
                            LoginUser = Logon_CurUser;
                        }
                        if (string.IsNullOrEmpty(Logon_CurUser) && !string.IsNullOrEmpty(Aut_CurUser))
                        {
                            LoginUser = Aut_CurUser;
                        }
                        string[] words = LoginUser.Split('\\');
                        CurUser = words[1].ToString();
                        //CurUser = "Administrator2";
                    }
                    catch (Exception ex)
                    {
                        //return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Not able to get the current user" }, JsonRequestBehavior.AllowGet);
                        TempData["Message"] = "Not able to get the current user";
                        var data = "Server not available";
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        ViewBag.data = data;
                        return View(Pages.Views.Login,model);
                    }
                    employees objEmployee = new employees();
                    Account objAccount = UserAuthenticationData.GetAccountWithClientCode(ProjectConfiguration.ClientCode);
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        ProjectSession.DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
                        ProjectSession.ConnectionString = objAccount.DbConString;

                        var Result = objDapperContext.SearchAll<employees>(objEmployee);
                        if (Result.Any(x => x.UserID == CurUser))
                        {

                        }
                        else
                        {
                            LdapAuthentication objLdap = new LdapAuthentication(ProjectConfiguration.DomainName);
                            List<Users> objUserlist = objLdap.getAlUserList();
                            List<employees> modelList = new List<employees>();
                            int[] listItem = ProjectConfiguration.DefaultEmployeeValues;
                            if (objUserlist != null)
                            {
                                //foreach (Users item in objUserlist)
                                //{
                                employees modelitem = new employees();
                                modelitem.UserID = CurUser.Trim();
                                modelitem.Name = CurUser.Trim();
                                //modelitem.WorkPhone = item.UserName;
                                modelitem.EmployeeNO = CurUser;
                                modelitem.L2ID = listItem[0];
                                modelitem.CategoryID = listItem[1];
                                modelitem.WorkTradeID = listItem[2];
                                modelitem.UserGroupId = listItem[3];
                                modelitem.MaintDeptID = listItem[4];
                                modelitem.MaintSubDeptID = listItem[5];
                                modelitem.MaintDivisionID = listItem[6];
                                modelitem.EmployeeStatusId = listItem[7];
                                modelitem.LanguageCode = "ar-sa";
                                modelList.Add(modelitem);
                                EmployeeService objService = new EmployeeService();
                                objService.SaveWebreqEmployee(modelitem);
                                //}
                            }
                            else
                            {
                                model.isActiveDirectory = "0";
                                var data = "Server not available";
                                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), data }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        model.Username = CurUser.Trim();
                        //model.Username = Request.ServerVariables["AUTH_USER"];
                        model.Password = "1234";
                        model.ClientCode = ProjectConfiguration.ClientCode;
                    }

                }
                catch (Exception ex)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Cannot connect to Active Directory" }, JsonRequestBehavior.AllowGet);
                }

                return View(Pages.Views.Login, model);
            }
            else
            {
                return View(Pages.Views.Login, model);
            }
        }

        /// <summary>
        /// Logins the specified username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="account">The account.</param>
        /// <param name="isRememberMe">if set to <c>true</c> [is remember me].</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="isAlreadyChecked">if set to <c>true</c> [is already checked].</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.Login)]
        //public ActionResult Login(string username, string password, int account, bool isRememberMe, string latitude, string longitude, bool isAlreadyChecked = false)
        public ActionResult Login(string username, string password, string account, bool isRememberMe, string latitude, string longitude, bool isAlreadyChecked = false)
        {
            try
            {
                ////string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                ////if (string.IsNullOrEmpty(ipAddress))
                ////{
                ////    ipAddress = Request.ServerVariables["REMOTE_ADDR"];
                ////}

                ////System.Net.IPHostEntry host;
                ////string localIP = "?";
                ////host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
                ////foreach (System.Net.IPAddress ip in host.AddressList)
                ////{
                ////    if (ip.AddressFamily.ToString() == "InterNetwork")
                ////    {
                ////        localIP = ip.ToString();
                ////    }
                ////}

                ////string apiKey = "8b502d4bd5cb2d1144ac4f7b49a4ee641e164f67f9263804558d8f7c4c25dd64";
                ////string url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", apiKey, localIP);

                ////string json = new System.Net.WebClient().DownloadString(url);

                ////LocationByAddress location = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<LocationByAddress>(json);

                ////latitude = location.Latitude;
                ////longitude = location.Longitude;

                string message = string.Empty;
                string errMessage = string.Empty;

                Account objAccount = UserAuthenticationData.GetAccountWithClientCode(account);

                if (!isAlreadyChecked)
                {
                    if (objAccount == null)
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "ClientID is not valid. Please contact administrator." });
                    }

                    if (objAccount.AccStatus == null || objAccount.AccStatus == false)
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Your Account is Inactive. Please contact administrator." });
                    }

                    if (objAccount.ExpDate < DateTime.Now.Date)
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Your account is expired. Please contact administrator." });
                    }
                }

                // // CMMS.Infrastructure.Common.WriteLog((objAccount.DbConString + " " + account + " <== ClientCode ");

                ProjectSession.ConnectionString = objAccount.DbConString;
                ProjectSession.DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);
                ProjectSession.AccountCode  = objAccount.ClientCode;
                ProjectSession.AccountID = objAccount.ClientID;
                ProjectSession.AllowedFileTypes = objAccount.AllowedFileTypes;
                ProjectSession.AllowedMaxFilesize = objAccount.AllowedMaxFilesize;
                ProjectSession.AllowUploadFileFormats = objAccount.AllowedFileTypes;                

                Tbl_Feature objFeature = UserAuthenticationData.GetTblFeatureData(ProjectSession.AccountID);
                if (objFeature != null)
                {
                    ProjectSession.IsAreaFieldsVisible = objFeature.Area;
                    ProjectSession.IsCMProjectFieldsVisible = objFeature.CMProject;
                    ProjectSession.IsCMProjectFieldMandatory = objFeature.CMProjectNoMandatory;
                }

                CEUML_config objCemulConfig = UserAuthenticationData.GetCemulData(ProjectSession.AccountID);

                if (objCemulConfig != null)
                {
                    if (!string.IsNullOrEmpty(objCemulConfig.Token))
                    {
                        ProjectSession.CEMULToken = objCemulConfig.Token;
                    }
                }

                //ServerManager iisManager = new ServerManager();
                //Application app;
                //if (ProjectConfiguration.TestMode)
                //{
                //    app = iisManager.Sites[0].Applications["/CMMS_Dev"];
                //}
                //else
                //{
                //    app = iisManager.Sites[ProjectConfiguration.IISSiteName].Applications[0];
                //} 

                //if (string.IsNullOrWhiteSpace(Convert.ToString(app.VirtualDirectories["/" + objAccount.ClientCode])))
                //{
                //    if (!Directory.Exists(objAccount.SharedFolderPath))
                //    {
                //        Directory.CreateDirectory(objAccount.SharedFolderPath);
                //    }
                //    app.VirtualDirectories.Add("/" + objAccount.ClientCode, objAccount.SharedFolderPath);
                //}
                //else
                //{
                //    if (!Directory.Exists(objAccount.SharedFolderPath))
                //    {
                //        Directory.CreateDirectory(objAccount.SharedFolderPath);
                //    }
                //    app.VirtualDirectories["/" + objAccount.ClientCode].PhysicalPath = objAccount.SharedFolderPath;
                //}
                //iisManager.CommitChanges();



                //ProjectSession.UploadPath = objAccount.SharedFolderPath + "\\" + objAccount.ClientCode;
                //ProjectSession.UploadPath = objAccount.ClientCode;
                if (!string.IsNullOrEmpty(objAccount.HdrLogo))
                {
                    if (!string.IsNullOrWhiteSpace(objAccount.HdrLogo))
                    {
                        ProjectSession.HeaderLogo = "~/" + objAccount.ClientCode + "/" + objAccount.ClientCode + "/HeaderImages/" + objAccount.HdrLogo;
                    }
                    else
                    {
                        ProjectSession.HeaderLogo = ProjectConfiguration.HeaderImagePath + "\\HeaderImages\\logo.png";
                    }
                }

                ProjectSession.HeaderNameEn = objAccount.HdrNameEn;
                ProjectSession.HeaderNameAr = objAccount.HdrNameAr;

                ////var timeout = Session.Timeout;
                if (Convert.ToInt32(objAccount.SessionTimeOut) > 0)
                {
                    Session.Timeout = Convert.ToInt32(objAccount.SessionTimeOut);
                }

                ProjectSession.DashboardAutoRefreshMinute = 1;
                if (Convert.ToInt32(objAccount.DashboardRefreshTime) > 0)
                {
                    ProjectSession.DashboardAutoRefreshMinute = Convert.ToInt32(objAccount.DashboardRefreshTime);
                }

                ProjectSession.IsItemNoAutoIncrement = objAccount.IsItemNoAutoIncrement.HasValue ? Convert.ToBoolean(objAccount.IsItemNoAutoIncrement.Value) : false;
                                
                using (ServiceContext objContext = new ServiceContext())
                {
                    Configurations objCofig = new Configurations();                                      
                    ProjectSession.LSTConfigs = objContext.SearchAll<Configurations>(objCofig);                    
                }

                UserAuthenticationData obj = new UserAuthenticationData();
                employees objemployees = new employees();
                //// objemployees = obj.CheckUserSignIn(username, password);

                using (DapperContext objDapperContext = new DapperContext())
                {
                    objemployees.UserID = username;
                    //ignor password if login with AD
                    if (ProjectConfiguration.IsActiveDirectory == "1")
                    {
                        objemployees.Password = null;
                    }
                    else
                    {
                        objemployees.Password = password;
                    }
                    objemployees = objDapperContext.Search<employees>(objemployees).FirstOrDefault();
                }

                if (objemployees != null)
                {
                    if (!isAlreadyChecked)
                    {
                        Session objSession2 = new Session();
                        objSession2.EmployeeId = objemployees.EmployeeID;
                        using (DapperContext objDapperContext = new DapperContext())
                        {
                            objSession2 = objDapperContext.Search<Session>(objSession2).ToList().LastOrDefault();

                            if (objSession2 != null && objSession2.Lastheartbeat != null)
                            {
                                UserAuthenticationData.UpdateLoginLog(objemployees.EmployeeID, Convert.ToDateTime(objSession2.Lastheartbeat));
                            }
                        }

                        UserAuthenticationData.DeleteSessions(Convert.ToInt32(objAccount.SessionTimeOut));

                        if (objemployees.EmployeeStatusId != SystemEnum.employeeStatus.Active.GetHashCode())
                        {
                            return Json(new object[] { 0, "danger", "User is not active. Please contact administrator." });
                        }

                        Session objSession1 = new Session();
                        objSession1.EmployeeId = objemployees.EmployeeID;
                        using (DapperContext objDapperContext = new DapperContext())
                        {
                            int loginCount = objDapperContext.Search<Session>(objSession1).ToList().Count;
                            if (loginCount > 0)
                            {
                                return Json(new object[] { 3, "danger", objemployees.EmployeeID });
                            }
                        }
                    }

                    //Session objOldSession = new Session();
                    //List<Session> lstSession = new List<Session>();

                    //using (DapperContext objContext = new DapperContext())
                    //{  
                    //    lstSession = objContext.SearchAll(objOldSession).ToList();                        
                    //}

                    //if (lstSession.Count >= objAccount.ConCurrentUsers)
                    //{
                    //    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "You have reach max number of concurrent user. Please contact administrator." });
                    //}

                    ProjectSession.EmployeeID = objemployees.EmployeeID;
                    ProjectSession.EmployeeName = objemployees.Name;
                    ProjectSession.EmployeeNo = objemployees.EmployeeNO;
                    ProjectSession.IsCentral = objemployees.Central.HasValue ? Convert.ToBoolean(1) : false;
                    //ProjectSession.IsCentral = objemployees.Central.HasValue ? Convert.ToBoolean(objemployees.Central.Value) : false;
                    ProjectSession.Culture = objemployees.LanguageCode;
                    ProjectSession.UserImage = objemployees.ImagePath;
                    ProjectSession.DateCulture = ProjectConfiguration.GregorianDate;
                    //ProjectSession.TabAccess = UserAuthenticationData.GetTabAccess(account);
                    ProjectSession.TabAccess = UserAuthenticationData.GetAccountWithTabAccess(ProjectSession.AccountID);
                    //ProjectSession.ModulesAccess = UserAuthenticationData.GetModulesAccess(account);
                    ProjectSession.ModulesAccess = UserAuthenticationData.GetAccountWithModuleAccess(ProjectSession.AccountID);
                    //ProjectSession.FirstLevelTbl = UserAuthenticationData.GetFirstLevelTbl(account);
                    ProjectSession.FirstLevelTbl = UserAuthenticationData.GetFirstLevelTbl(ProjectSession.AccountID);
                    ProjectSession.ReportAccess = UserAuthenticationData.GetAccountWithReportAccess(ProjectSession.AccountID);

                    if (ProjectSession.FirstLevelTbl != null)
                    {
                        ProjectSession.LevelNamesTbl = UserAuthenticationData.GetLevelNamesTbl(ProjectSession.FirstLevelTbl.L1_ID);
                    }

                    if (ProjectSession.TabAccess == null)
                    {
                        ProjectSession.TabAccess = new TabAccess();
                    }

                    if (ProjectSession.ModulesAccess == null)
                    {
                        ProjectSession.ModulesAccess = new ModulesAccess();
                    }

                    Tbl_LanguageSelection objLanguage = TranslationsService.GetLanguages().Where(x => x.LanguageCode.ToLower() == objemployees.LanguageCode.ToLower()).FirstOrDefault();
                    if (obj != null)
                    {
                        ProjectSession.CultureKendoClass = objLanguage.FlowDirection == true ? ProjectConfiguration.EnglishCultureKendoClass : ProjectConfiguration.ArabicCultureKendoClass;
                        ProjectSession.LayOutLanguage = objLanguage.FlowDirection == true ? ProjectConfiguration.EnglishLayOut : ProjectConfiguration.ArabicLayout;
                        ProjectSession.Direction = objLanguage.FlowDirection == true ? ProjectConfiguration.DirectionEng : ProjectConfiguration.DirectionAra;
                    }

                    TranslationsService objTranslationsService = new TranslationsService();
                    ProjectSession.Resources = objTranslationsService.GetResourceValues(ProjectSession.Culture, "en-us");
                    ProjectSession.PermissionAccess = PermissionService.SetEmployeePermission(objemployees);

                    using (ServiceContext context = new ServiceContext())
                    {
                        ////LoginLog
                        LoginLog objLoginLog = new LoginLog();
                        objLoginLog.LoginLogIn = 0;
                        objLoginLog.EmployeeId = ProjectSession.EmployeeID;
                        objLoginLog.LoginDateTime = DateTime.Now;
                        objLoginLog.IPAddress = Convert.ToString(System.Web.HttpContext.Current.Request.UserHostAddress);
                        int result = context.Save(objLoginLog);

                        ////Session
                        Session objSession = new Session();
                        objSession.SessionId = 0;
                        objSession.EmployeeId = ProjectSession.EmployeeID;
                        objSession.Logintime = DateTime.Now;
                        objSession.Lastheartbeat = DateTime.Now;
                        int result1 = context.Save(objSession);
                        ProjectSession.SessionID = result1;
                    }

                    ////Remember me
                    HttpCookie cookie = new HttpCookie("UserLogin");
                    if (isRememberMe)
                    {
                        cookie.Expires = DateTime.Now.AddDays(ProjectConfiguration.CookiesValidity);
                    }
                    else
                    {
                        cookie.Expires = DateTime.Now.AddDays(-1);
                    }

                    cookie.Values.Add("UserName", username.Trim());
                    cookie.Values.Add("Password", password);
                    cookie.Values.Add("Account", Convert.ToString(account));
                    cookie.Values.Add("clientID", Convert.ToString(account));
                    cookie.Values.Add("IsRememberMe", isRememberMe.ToString());
                    Response.Cookies.Add(cookie);

                    if (latitude != string.Empty)
                    {
                        ProjectSession.Latitude = (float)ConvertTo.Decimal(latitude);
                    }

                    if (longitude != string.Empty)
                    {
                        ProjectSession.Longitude = (float)ConvertTo.Decimal(longitude);
                    }

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), string.Empty });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Username or password is not valid" });
                }
            }
            catch (Exception ex)
            {
                return Json(new object[] { 2, SystemEnum.MessageType.danger.ToString(), ex.ToString() });
            }
        }

        /// <summary>
        /// Logouts this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Logout)]
        public ActionResult Logout()
        {
            if (ProjectSession.EmployeeID != 0)
            {
                using (ServiceContext context = new ServiceContext())
                {
                    ////Update Logout time in LoginLog
                    UserAuthenticationData.UpdateLoginLog(ProjectSession.EmployeeID, DateTime.Now);

                    ////Delete Session
                    UserAuthenticationData.DeleteSession(ProjectSession.EmployeeID);
                }
            }

            Session.Clear();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return RedirectToAction(Pages.Views.Login);
        }



        /// <summary>
        /// Logouts this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SessionTimeOutLogout)]
        public JsonResult SessionTimeOutLogout()
        {
            if (ProjectSession.EmployeeID != 0)
            {
                using (ServiceContext context = new ServiceContext())
                {
                    ////Update Logout time in LoginLog
                    UserAuthenticationData.UpdateLoginLog(ProjectSession.EmployeeID, DateTime.Now);

                    ////Delete Session
                    UserAuthenticationData.DeleteSession(ProjectSession.EmployeeID);
                }
            }

            Session.Clear();
            Session.Abandon();
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Deletes the session.
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        public JsonResult DeleteSession(int employeeID)
        {
            UserAuthenticationData.DeleteSession(employeeID);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Forgot Password"
        /// <summary>
        /// Forgot the password.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="account">The account.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.ForgotPassword)]
        public ActionResult ForgotPassword(string username, int account)
        {
            try
            {
                Account objAccount = UserAuthenticationData.GetAccount(account);

                if (objAccount == null)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "ClientID is not valid. Please contact to administrator." });
                }

                ProjectSession.ConnectionString = objAccount.DbConString;
                ProjectSession.DbType = objAccount.DbType == null ? 1 : Convert.ToInt32(objAccount.DbType);

                employees objemployees = new employees();
                objemployees.UserID = username;

                using (ServiceContext context = new ServiceContext())
                {
                    objemployees = context.Search(objemployees, null, null, null).FirstOrDefault();
                    if (objemployees != null)
                    {
                        string emailTemplatePath = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplate/ForgotPassword.html");

                        using (StreamReader reader = new StreamReader(emailTemplatePath))
                        {
                            string htmlTemplate = reader.ReadToEnd();
                            string logo = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/Images/logo.png";
                            ////string logo = System.Web.HttpContext.Current.Server.MapPath("~/Images/logo.png"); 
                            htmlTemplate = htmlTemplate.Replace("$LOGOURL$", logo);
                            htmlTemplate = htmlTemplate.Replace("$NAME$", objemployees.Name);
                            htmlTemplate = htmlTemplate.Replace("$EMAIL$", objemployees.Email);
                            htmlTemplate = htmlTemplate.Replace("$USERNAME$", objemployees.UserID);
                            htmlTemplate = htmlTemplate.Replace("$PASSWORD$", objemployees.Password);
                            bool result = EmailServices.Send(objemployees.Email, null, null, "Forgot Password", htmlTemplate, null);
                            if (result == true)
                            {
                                return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Password sent successfully" });
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "There are some error.Please contact to administrator" });
                            }
                        }
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "User is not registered." });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Access Denied"

        /// <summary>
        /// Accesses the denied.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.AccessDenied)]
        public ActionResult AccessDenied()
        {
            return View(Views.AccessDenied);
        }

        /// <summary>
        /// Accesses the denied.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.DisplayError)]
        public ActionResult DisplayError()
        {
            ViewBag.ErrorMessage = ProjectSession.ErrorMessage;
            ProjectSession.ErrorMessage = string.Empty;
            return View(Views.DisplayError);
        }

        /// <summary>
        /// Checks the advance search.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckAdvanceSearch()
        {
            return Json(new { ProjectSession.IsAdvanceSearch }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Session Expired On Ajax Call"

        /// <summary>
        /// Check Session Expired from Client Side (Java Script)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SessionExpiredForCustomer)]
        public JsonResult SessionExpiredForCustomer()
        {
            string returnUrl = string.Empty;
            returnUrl = Url.Action(Actions.Login, Pages.Controllers.Account, new { area = string.Empty });
            return Json(new { IsExpired = ProjectSession.EmployeeID <= 0 ? true : false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        /// <summary>
        /// Location By Address
        /// </summary>
        public class LocationByAddress
        {
            /// <summary>
            /// Gets or sets the IP address.
            /// </summary>
            /// <value>
            /// The IP address.
            /// </value>
            public string IPAddress { get; set; }

            /// <summary>
            /// Gets or sets the name of the country.
            /// </summary>
            /// <value>
            /// The name of the country.
            /// </value>
            public string CountryName { get; set; }

            /// <summary>
            /// Gets or sets the country code.
            /// </summary>
            /// <value>
            /// The country code.
            /// </value>
            public string CountryCode { get; set; }

            /// <summary>
            /// Gets or sets the name of the city.
            /// </summary>
            /// <value>
            /// The name of the city.
            /// </value>
            public string CityName { get; set; }

            /// <summary>
            /// Gets or sets the name of the region.
            /// </summary>
            /// <value>
            /// The name of the region.
            /// </value>
            public string RegionName { get; set; }

            /// <summary>
            /// Gets or sets the zip code.
            /// </summary>
            /// <value>
            /// The zip code.
            /// </value>
            public string ZipCode { get; set; }

            /// <summary>
            /// Gets or sets the latitude.
            /// </summary>
            /// <value>
            /// The latitude.
            /// </value>
            public string Latitude { get; set; }

            /// <summary>
            /// Gets or sets the longitude.
            /// </summary>
            /// <value>
            /// The longitude.
            /// </value>
            public string Longitude { get; set; }

            /// <summary>
            /// Gets or sets the time zone.
            /// </summary>
            /// <value>
            /// The time zone.
            /// </value>
            public string TimeZone { get; set; }
        }
    }
}