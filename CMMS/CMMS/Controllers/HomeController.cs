﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using CMMS.Service;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Pages;
using CMMS.Service.DashboardService;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;

namespace CMMS.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : BaseController
    {
        #region "Home Method"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Selects the language.
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="currency">The currency.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Pages.Actions.SelectLanguage)]
        public JsonResult SelectLanguage(string language = "", string currency = "")
        {
            if (!string.IsNullOrEmpty(language))
            {
                ProjectSession.Culture = language;
                Tbl_LanguageSelection obj = TranslationsService.GetLanguages().Where(x => x.LanguageCode == language).FirstOrDefault();
                if (obj != null)
                {
                    ProjectSession.CultureKendoClass = obj.FlowDirection == true ? ProjectConfiguration.EnglishCultureKendoClass : ProjectConfiguration.ArabicCultureKendoClass;
                    ProjectSession.LayOutLanguage = obj.FlowDirection == true ? ProjectConfiguration.EnglishLayOut : ProjectConfiguration.ArabicLayout;
                    ProjectSession.Direction = obj.FlowDirection == true ? ProjectConfiguration.DirectionEng : ProjectConfiguration.DirectionAra;
                }

                TranslationsService objTranslationsService = new TranslationsService();
                ProjectSession.Resources = objTranslationsService.GetResourceValues(ProjectSession.Culture, "en-us");
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Selects the theme.
        /// </summary>
        /// <param name="theme">The theme.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SelectTheme(string theme)
        {
            if (!string.IsNullOrEmpty(theme))
            {
                ProjectSession.UserTheme = theme;
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Selects the date culture.
        /// </summary>
        /// <param name="dateCulture">The date culture.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Pages.Actions.SelectDateCulture)]
        public JsonResult SelectDateCulture(string dateCulture)
        {
            if (!string.IsNullOrEmpty(dateCulture))
            {
                ProjectSession.DateCulture = dateCulture;
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the enum description.
        /// </summary>
        /// <param name="enumName">Name of the enum.</param>
        /// <param name="decValue">The decimal value.</param>
        /// <returns></returns>
        public JsonResult GetEnumDescription(string enumName, int decValue)
        {
            Enum value;
            switch (enumName)
            {
                case "SupplierContractorCategory":
                    value = (SystemEnum.SupplierContractorCategory)decValue;
                    break;
                case "WarrentyStatus":
                    value = (SystemEnum.WarrentyStatus)decValue;
                    break;
                default:
                    value = (SystemEnum.SupplierContractorCategory)decValue;
                    break;
            }

            string description;
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null && attributes.Length > 0)
            {
                description = attributes[0].Description;
            }
            else
            {
                description = value.ToString();
            }

            return Json(description);
        }

        /// <summary>
        /// save uploaded Files into Given Path
        /// </summary>
        /// <param name="model">Instance Of Model</param>
        /// <param name="files">Files Uploaded</param>
        /// <param name="moduleType">Module type</param>
        public void SaveUploadedFiles(dynamic model, IEnumerable<HttpPostedFileBase> files, string moduleType)
        {
            try
            {
                if (files != null)
                {
                    model.lstAttachment = new List<clsextassetfiles>();
                    foreach (var file in files)
                    {
                        if (file.ContentLength > 0)
                        {
                            string filePath = null;
                            string subPath = ProjectConfiguration.UploadPath + "Attachments/";
                            bool folderExists = Directory.Exists(subPath);
                            if (!folderExists)
                            {
                                Directory.CreateDirectory(subPath);
                            }

                            string fileUniqueName = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
                            string fileName = fileUniqueName + "_" + Path.GetFileName(file.FileName);
                            filePath = subPath + @"\" + fileName;
                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            file.SaveAs(filePath);

                            model.lstAttachment.Add(
                                new clsextassetfiles
                                {
                                    guidId = Guid.NewGuid(),
                                    FileLink = "Attachments/" + fileName,
                                    FileDescription = string.Empty,
                                    FileName = fileName,
                                    ModuleType = moduleType,
                                    CreatedDate = DateTime.Now,
                                });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Download Attachments 
        /// </summary>
        /// <param name="attachmentID">Attachment Identifier</param>
        /// <returns></returns>
        [ActionName(Actions.DownloadAttachment)]
        public FileResult DownloadAttachment(string attachmentID)
        {
            try
            {
                if (!string.IsNullOrEmpty(attachmentID) && attachmentID != "0")
                {
                    int attID = Convert.ToInt32(attachmentID);

                    CmAttachment objCmAttachment = new CmAttachment();
                    using (DapperContext objContext = new DapperContext())
                    {
                        objCmAttachment = objContext.SearchAllByID<CmAttachment>(objCmAttachment, attID).FirstOrDefault();
                    }

                    string fileName = objCmAttachment.FileName;
                    string downloadName = objCmAttachment.FileName;
                    string filePath = ProjectConfiguration.UploadPath + objCmAttachment.FileLink;
                    string extension = Path.GetExtension(filePath);
                    string contentType = "application/" + extension;
                    return File(filePath, contentType, downloadName);
                }
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        #endregion

        #region "Dashboard Chart"

        /// <summary>
        /// Dashboards this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Dashboard)]
        public ActionResult Dashboard()
        {
            CustomDashboard obj = new CustomDashboard();
            IList<DashboardReportMenu> lstDashboardReportMenu = new List<DashboardReportMenu>();
            lstDashboardReportMenu = obj.GetDashBoardReportMenu();
            return View(Pages.Views.Dashboard, lstDashboardReportMenu);
        }

        /// <summary>
        /// Changes the widget class.
        /// </summary>
        /// <param name="classId">The class identifier.</param>
        /// <returns></returns>
        [ActionName("ChangeWidgetClass")]
        public ActionResult ChangeWidgetClass(string classId)
        {
            ProjectSession.WidhetClass = classId;
            return Json(new { SystemEnum.MessageType.success }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// _s the chart search filter.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._ChartSearchFilter)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _ChartSearchFilter(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(reportId, dashboardMenuId);
            if (objReportBindJobOrder.JoDateFrom == DateTime.MinValue && objReportBindJobOrder.DashboardRepotMenuID == 0)
            {
                objReportBindJobOrder.DashboardRepotMenuID = dashboardMenuId;
                objReportBindJobOrder.reportID = reportId;
                objReportBindJobOrder.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objReportBindJobOrder.JoDateTo = DateTime.Now.Date;
            }

            return PartialView(Pages.PartialViews.ChartSearchFilter, objReportBindJobOrder);
        }

        /// <summary>
        /// _s the job order type search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._JobOrderTypeSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _JobOrderTypeSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal = new JobOrderTypeChartFilterModal();
            objJobOrderTypeChartFilterModal = obj.GetJobOrderTypeFilterSearch(SystemEnum.DashboardModule.JobOrderTypeDistribution.GetHashCode(), dashboardMenuId);
            if (objJobOrderTypeChartFilterModal.JoDateFrom == DateTime.MinValue && objJobOrderTypeChartFilterModal.DashboardRepotMenuID == 0)
            {
                objJobOrderTypeChartFilterModal.DashboardRepotMenuID = dashboardMenuId;
                objJobOrderTypeChartFilterModal.reportID = reportId;
                objJobOrderTypeChartFilterModal.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objJobOrderTypeChartFilterModal.JoDateTo = DateTime.Now.Date;
            }

            return PartialView(Pages.PartialViews.JobOrderTypeSearch, objJobOrderTypeChartFilterModal);
        }

        /// <summary>
        /// Saves the search data.
        /// </summary>
        /// <param name="objReportBindJobOrder">The object report bind job order.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveSearchData)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveSearchData(ReportBindJobOrder objReportBindJobOrder)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveReportData(objReportBindJobOrder);
            if (flag)
            {
                return Json(new object[] { 1, objReportBindJobOrder.reportID, objReportBindJobOrder.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Saves the job order type search.
        /// </summary>
        /// <param name="objJobOrderTypeChartFilterModal">The object job order type chart filter modal.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobOrderTypeSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJobOrderTypeSearch(JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveReporForJobOrderTypeCriteria(objJobOrderTypeChartFilterModal);
            if (flag)
            {
                return Json(new object[] { 1, objJobOrderTypeChartFilterModal.reportID, objJobOrderTypeChartFilterModal.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Jobs the trade distribution chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobOrderTypeDistributionChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal = new JobOrderTypeChartFilterModal();
            objJobOrderTypeChartFilterModal = obj.GetJobOrderTypeFilterSearch(SystemEnum.DashboardModule.JobOrderTypeDistribution.GetHashCode(), dashboardReportMenuID);
            JobOrderTypeChartFilterModal objNew = new JobOrderTypeChartFilterModal();
            if (objJobOrderTypeChartFilterModal != null && objJobOrderTypeChartFilterModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objJobOrderTypeChartFilterModal.L1ID;
                objNew.L2ID = objJobOrderTypeChartFilterModal.L2ID;
                objNew.L3ID = objJobOrderTypeChartFilterModal.L3ID == null ? string.Empty : objJobOrderTypeChartFilterModal.L3ID;
                objNew.L4ID = objJobOrderTypeChartFilterModal.L4ID;
                objNew.L5ID = objJobOrderTypeChartFilterModal.L5ID;
                objNew.MaintDivID = objJobOrderTypeChartFilterModal.MaintDivID;
                objNew.MaintDeptID = objJobOrderTypeChartFilterModal.MaintDeptID;
                objNew.MaintSubDeptID = objJobOrderTypeChartFilterModal.MaintSubDeptID;
                objNew.JoDateFrom = objJobOrderTypeChartFilterModal.JoDateFrom;
                objNew.JoDateTo = objJobOrderTypeChartFilterModal.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            if (objNew.L2ID != 0)
            {
                ViewBag.JobOrderTypeDist = objJobOrderTypeChartFilterModal.L2Code;
            }
            else
            {
                ViewBag.JobOrderTypeDist = "All";
            }

            var viewModel = obj.GetJobOrderTypeDistributionChartDetail(ProjectSession.IsCentral, false, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo);

            return Json(viewModel);
        }

        /// <summary>
        /// Outstanding the type of the job.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OutstandingJobTypeChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal = new JobOrderTypeChartFilterModal();
            objJobOrderTypeChartFilterModal = obj.GetJobOrderTypeFilterSearch(SystemEnum.DashboardModule.JobOrderTypeDistribution.GetHashCode(), dashboardReportMenuID);
            JobOrderTypeChartFilterModal objNew = new JobOrderTypeChartFilterModal();
            if (objJobOrderTypeChartFilterModal != null && objJobOrderTypeChartFilterModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objJobOrderTypeChartFilterModal.L1ID;
                objNew.L2ID = objJobOrderTypeChartFilterModal.L2ID;
                objNew.L3ID = objJobOrderTypeChartFilterModal.L3ID == null ? string.Empty : objJobOrderTypeChartFilterModal.L3ID;
                objNew.L4ID = objJobOrderTypeChartFilterModal.L4ID;
                objNew.L5ID = objJobOrderTypeChartFilterModal.L5ID;
                objNew.MaintDivID = objJobOrderTypeChartFilterModal.MaintDivID;
                objNew.MaintDeptID = objJobOrderTypeChartFilterModal.MaintDeptID;
                objNew.MaintSubDeptID = objJobOrderTypeChartFilterModal.MaintSubDeptID;
                objNew.JoDateFrom = objJobOrderTypeChartFilterModal.JoDateFrom;
                objNew.JoDateTo = objJobOrderTypeChartFilterModal.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            var viewModel = obj.GetJobOrderTypeDistributionChartDetail(ProjectSession.IsCentral, true, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo);
            return Json(viewModel);
        }

        /// <summary>
        /// Pm the job order status chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PMJobOrderStatusChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderTypeChartFilterModal objJobOrderTypeChartFilterModal = new JobOrderTypeChartFilterModal();
            objJobOrderTypeChartFilterModal = obj.GetJobOrderTypeFilterSearch(SystemEnum.DashboardModule.JobOrderTypeDistribution.GetHashCode(), dashboardReportMenuID);
            JobOrderTypeChartFilterModal objNew = new JobOrderTypeChartFilterModal();
            if (objJobOrderTypeChartFilterModal != null && objJobOrderTypeChartFilterModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objJobOrderTypeChartFilterModal.L1ID;
                objNew.L2ID = objJobOrderTypeChartFilterModal.L2ID;
                objNew.L3ID = objJobOrderTypeChartFilterModal.L3ID == null ? string.Empty : objJobOrderTypeChartFilterModal.L3ID;
                objNew.L4ID = objJobOrderTypeChartFilterModal.L4ID;
                objNew.L5ID = objJobOrderTypeChartFilterModal.L5ID;
                objNew.MaintDivID = objJobOrderTypeChartFilterModal.MaintDivID;
                objNew.MaintDeptID = objJobOrderTypeChartFilterModal.MaintDeptID;
                objNew.MaintSubDeptID = objJobOrderTypeChartFilterModal.MaintSubDeptID;
                objNew.JoDateFrom = objJobOrderTypeChartFilterModal.JoDateFrom;
                objNew.JoDateTo = objJobOrderTypeChartFilterModal.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            var viewModel = obj.GetPMJobOrderStatusChartDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo);
            return Json(viewModel);
        }

        /// <summary>
        /// Jobs the order status chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobOrderStatusChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(SystemEnum.DashboardModule.JobOrderStatus.GetHashCode(), dashboardReportMenuID);
            ReportBindJobOrder objNew = new ReportBindJobOrder();
            if (objReportBindJobOrder != null && objReportBindJobOrder.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objReportBindJobOrder.L1ID;
                objNew.L2ID = objReportBindJobOrder.L2ID;
                objNew.L3ID = objReportBindJobOrder.L3ID == null ? string.Empty : objReportBindJobOrder.L3ID;
                objNew.L4ID = objReportBindJobOrder.L4ID;
                objNew.L5ID = objReportBindJobOrder.L5ID;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = objReportBindJobOrder.MaintDeptID;
                objNew.MaintSubDeptID = objReportBindJobOrder.MaintSubDeptID;
                objNew.JoDateFrom = objReportBindJobOrder.JoDateFrom;
                objNew.JoDateTo = objReportBindJobOrder.JoDateTo;
                objNew.JobTypeID = objReportBindJobOrder.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobOrderStatusChartDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, objNew.JobTypeID);
            return Json(viewModel);
        }

        /// <summary>
        /// Jobs the priority distribution chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobPriorityDistributionChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(SystemEnum.DashboardModule.JobPriorityDistribution.GetHashCode(), dashboardReportMenuID);
            ReportBindJobOrder objNew = new ReportBindJobOrder();
            if (objReportBindJobOrder != null && objReportBindJobOrder.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objReportBindJobOrder.L1ID;
                objNew.L2ID = objReportBindJobOrder.L2ID;
                objNew.L3ID = objReportBindJobOrder.L3ID == null ? string.Empty : objReportBindJobOrder.L3ID;
                objNew.L4ID = objReportBindJobOrder.L4ID;
                objNew.L5ID = objReportBindJobOrder.L5ID;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = objReportBindJobOrder.MaintDeptID;
                objNew.MaintSubDeptID = objReportBindJobOrder.MaintSubDeptID;
                objNew.JoDateFrom = objReportBindJobOrder.JoDateFrom;
                objNew.JoDateTo = objReportBindJobOrder.JoDateTo;
                objNew.JobTypeID = objReportBindJobOrder.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobPriorityDistributionChartDetail(ProjectSession.IsCentral, false, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, objNew.JobTypeID);
            return Json(viewModel);
        }

        /// <summary>
        /// Jobs the trade distribution chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobTradeDistibutionChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(SystemEnum.DashboardModule.JobTradeDistribution.GetHashCode(), dashboardReportMenuID);
            ReportBindJobOrder objNew = new ReportBindJobOrder();
            if (objReportBindJobOrder != null && objReportBindJobOrder.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objReportBindJobOrder.L1ID;
                objNew.L2ID = objReportBindJobOrder.L2ID;
                objNew.L3ID = objReportBindJobOrder.L3ID == null ? string.Empty : objReportBindJobOrder.L3ID;
                objNew.L4ID = objReportBindJobOrder.L4ID;
                objNew.L5ID = objReportBindJobOrder.L5ID;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = objReportBindJobOrder.MaintDeptID;
                objNew.MaintSubDeptID = objReportBindJobOrder.MaintSubDeptID;
                objNew.JoDateFrom = objReportBindJobOrder.JoDateFrom;
                objNew.JoDateTo = objReportBindJobOrder.JoDateTo;
                objNew.JobTypeID = objReportBindJobOrder.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobTradeDistributionChartDetail(ProjectSession.IsCentral, false, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, objNew.JobTypeID);
            return Json(viewModel);
        }

        /// <summary>
        /// Outstanding the job priority chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OutstandingJobPriorityChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(SystemEnum.DashboardModule.OutstandingJobsByPriority.GetHashCode(), dashboardReportMenuID);
            ReportBindJobOrder objNew = new ReportBindJobOrder();
            if (objReportBindJobOrder != null && objReportBindJobOrder.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objReportBindJobOrder.L1ID;
                objNew.L2ID = objReportBindJobOrder.L2ID;
                objNew.L3ID = objReportBindJobOrder.L3ID == null ? string.Empty : objReportBindJobOrder.L3ID;
                objNew.L4ID = objReportBindJobOrder.L4ID;
                objNew.L5ID = objReportBindJobOrder.L5ID;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = objReportBindJobOrder.MaintDeptID;
                objNew.MaintSubDeptID = objReportBindJobOrder.MaintSubDeptID;
                objNew.JoDateFrom = objReportBindJobOrder.JoDateFrom;
                objNew.JoDateTo = objReportBindJobOrder.JoDateTo;
                objNew.JobTypeID = objReportBindJobOrder.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobPriorityDistributionChartDetail(ProjectSession.IsCentral, true, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, objNew.JobTypeID);
            return Json(viewModel);
        }

        /// <summary>
        /// Outstanding the job trade chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OutstandingJobTradeChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            ReportBindJobOrder objReportBindJobOrder = new ReportBindJobOrder();
            objReportBindJobOrder = obj.GetReportFilterData(SystemEnum.DashboardModule.OutstandingJobsByTrade.GetHashCode(), dashboardReportMenuID);
            ReportBindJobOrder objNew = new ReportBindJobOrder();
            if (objReportBindJobOrder != null && objReportBindJobOrder.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objReportBindJobOrder.L1ID;
                objNew.L2ID = objReportBindJobOrder.L2ID;
                objNew.L3ID = objReportBindJobOrder.L3ID == null ? string.Empty : objReportBindJobOrder.L3ID;
                objNew.L4ID = objReportBindJobOrder.L4ID;
                objNew.L5ID = objReportBindJobOrder.L5ID;
                objNew.MaintDivID = objReportBindJobOrder.MaintDivID;
                objNew.MaintDeptID = objReportBindJobOrder.MaintDeptID;
                objNew.MaintSubDeptID = objReportBindJobOrder.MaintSubDeptID;
                objNew.JoDateFrom = objReportBindJobOrder.JoDateFrom;
                objNew.JoDateTo = objReportBindJobOrder.JoDateTo;
                objNew.JobTypeID = objReportBindJobOrder.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobTradeDistributionChartDetail(ProjectSession.IsCentral, true, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, objNew.JobTypeID);
            return Json(viewModel);
        }

        #region "PM Compilance Chart"

        /// <summary>
        /// _s the pm compliance search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._PMComplianceSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _PMComplianceSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            PMComplianceModal objPMComplianceModal = new PMComplianceModal();
            objPMComplianceModal = obj.GetReportFilterDataForPMComplianceModal(reportId, dashboardMenuId);
            if (objPMComplianceModal.JoDateFrom == DateTime.MinValue && objPMComplianceModal.DashboardRepotMenuID == 0)
            {
                objPMComplianceModal.DashboardRepotMenuID = dashboardMenuId;
                objPMComplianceModal.reportID = reportId;
                objPMComplianceModal.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objPMComplianceModal.JoDateTo = DateTime.Now.Date;
            }

            return PartialView(Pages.PartialViews.PMComplianceSearch, objPMComplianceModal);
        }

        /// <summary>
        /// Saves the pm compliance search.
        /// </summary>
        /// <param name="objPMComplianceModal">The object pm compliance modal.</param>
        /// <returns></returns>
        [ActionName(Actions.SavePMComplianceSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SavePMComplianceSearch(PMComplianceModal objPMComplianceModal)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SavePMComplianceData(objPMComplianceModal);
            if (flag)
            {
                return Json(new object[] { 1, objPMComplianceModal.reportID, objPMComplianceModal.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Pm the compliance chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PMComplianceChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            PMComplianceModal objPMComplianceModal = new PMComplianceModal();
            objPMComplianceModal = obj.GetReportFilterDataForPMComplianceModal(SystemEnum.DashboardModule.OutstandingJobsByTrade.GetHashCode(), dashboardReportMenuID);
            PMComplianceModal objNew = new PMComplianceModal();
            if (objPMComplianceModal != null && objPMComplianceModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objPMComplianceModal.L1ID;
                objNew.L2ID = objPMComplianceModal.L2ID;
                objNew.L3ID = objPMComplianceModal.L3ID == null ? string.Empty : objPMComplianceModal.L3ID;
                objNew.L4ID = objPMComplianceModal.L4ID;
                objNew.L5ID = objPMComplianceModal.L5ID;
                objNew.MaintDivID = objPMComplianceModal.MaintDivID;
                objNew.MaintDeptID = objPMComplianceModal.MaintDeptID;
                objNew.MaintSubDeptID = objPMComplianceModal.MaintSubDeptID;
                objNew.JoDateFrom = objPMComplianceModal.JoDateFrom;
                objNew.JoDateTo = objPMComplianceModal.JoDateTo;
                objNew.JobTradeID = objPMComplianceModal.JobTradeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTradeID = string.Empty;
            }

            var viewModel = obj.GetPMComplianceChartDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTradeID, objNew.JoDateFrom, objNew.JoDateTo);
            return Json(viewModel);
        }

        /// <summary>
        /// Noes the of job orders by type chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult NoOfJobOrdersByTypeChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            PMComplianceModal objPMComplianceModal = new PMComplianceModal();
            objPMComplianceModal = obj.GetReportFilterDataForPMComplianceModal(SystemEnum.DashboardModule.NoOfJobOrdersByType.GetHashCode(), dashboardReportMenuID);
            PMComplianceModal objNew = new PMComplianceModal();
            if (objPMComplianceModal != null && objPMComplianceModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objPMComplianceModal.L1ID;
                objNew.L2ID = objPMComplianceModal.L2ID;
                objNew.L3ID = objPMComplianceModal.L3ID == null ? string.Empty : objPMComplianceModal.L3ID;
                objNew.L4ID = objPMComplianceModal.L4ID;
                objNew.L5ID = objPMComplianceModal.L5ID;
                objNew.MaintDivID = objPMComplianceModal.MaintDivID;
                objNew.MaintDeptID = objPMComplianceModal.MaintDeptID;
                objNew.MaintSubDeptID = objPMComplianceModal.MaintSubDeptID;
                objNew.JoDateFrom = objPMComplianceModal.JoDateFrom;
                objNew.JoDateTo = objPMComplianceModal.JoDateTo;
                objNew.JobTradeID = objPMComplianceModal.JobTradeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTradeID = string.Empty;
            }

            var viewModel = obj.GetNoOfJobOrderByTypeDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTradeID, objNew.JoDateFrom, objNew.JoDateTo);
            return Json(viewModel);
        }

        /// <summary>
        /// Jobs the order and request trending chart.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobOrderAndRequestTrendingChart(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            JobTradeingModal objJobTradingModal = new JobTradeingModal();
            objJobTradingModal = obj.GetReportFilterDataForJobTradingModal(SystemEnum.DashboardModule.JobOrderAndRequestTrending.GetHashCode(), dashboardReportMenuID);
            JobTradeingModal objNew = new JobTradeingModal();
            if (objJobTradingModal != null && objJobTradingModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objJobTradingModal.L1ID;
                objNew.L2ID = objJobTradingModal.L2ID;
                objNew.L3ID = objJobTradingModal.L3ID == null ? string.Empty : objJobTradingModal.L3ID;
                objNew.L4ID = objJobTradingModal.L4ID;
                objNew.L5ID = objJobTradingModal.L5ID;
                objNew.MaintDivID = objJobTradingModal.MaintDivID;
                objNew.MaintDeptID = objJobTradingModal.MaintDeptID;
                objNew.MaintSubDeptID = objJobTradingModal.MaintSubDeptID;
                objNew.JoDateFrom = objJobTradingModal.JoDateFrom;
                objNew.JoDateTo = objJobTradingModal.JoDateTo;
                objNew.JobTradeID = objJobTradingModal.JobTradeID;
                objNew.JobTypeID = objJobTradingModal.JobTypeID;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
                objNew.JobTradeID = string.Empty;
                objNew.JobTypeID = string.Empty;
            }

            var viewModel = obj.GetJobOrderAndRequestTrendingDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTypeID, objNew.JobTradeID, objNew.JoDateFrom, objNew.JoDateTo);
            return Json(viewModel);
        }

        /// <summary>
        /// _s the job trading search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._JobTradingSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _JobTradingSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            JobTradeingModal objJobTradingModal = new JobTradeingModal();
            objJobTradingModal = obj.GetReportFilterDataForJobTradingModal(reportId, dashboardMenuId);
            if (objJobTradingModal.JoDateFrom == DateTime.MinValue && objJobTradingModal.DashboardRepotMenuID == 0)
            {
                objJobTradingModal.DashboardRepotMenuID = dashboardMenuId;
                objJobTradingModal.reportID = reportId;
                objJobTradingModal.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objJobTradingModal.JoDateTo = DateTime.Now.Date;
            }

            return PartialView(Pages.PartialViews.JobTradingSearch, objJobTradingModal);
        }

        /// <summary>
        /// Saves the job trading search.
        /// </summary>
        /// <param name="objJobTradingModal">The object job trading modal.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobTradingSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJobTradingSearch(JobTradeingModal objJobTradingModal)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveJobTradingData(objJobTradingModal);
            if (flag)
            {
                return Json(new object[] { 1, objJobTradingModal.reportID, objJobTradingModal.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        #endregion

        #region "Job Order Status By Site"

        /// <summary>
        /// Jobs the order status by site.
        /// </summary>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult JobOrderStatusBySite(int dashboardReportMenuID)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderStatusBySiteSearchData objJobOrderStatusBySiteSearchData = new JobOrderStatusBySiteSearchData();
            objJobOrderStatusBySiteSearchData = obj.GetJobOrderBySiteReportData(SystemEnum.DashboardModule.JobOrderStatusBySite.GetHashCode(), dashboardReportMenuID);
            JobOrderStatusBySiteSearchData objNew = new JobOrderStatusBySiteSearchData();
            if (objJobOrderStatusBySiteSearchData != null && objJobOrderStatusBySiteSearchData.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objJobOrderStatusBySiteSearchData.L1ID;
                objNew.L2ID = objJobOrderStatusBySiteSearchData.L2ID;
                objNew.JoDateFrom = objJobOrderStatusBySiteSearchData.JoDateFrom;
                objNew.JoDateTo = objJobOrderStatusBySiteSearchData.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            var data = obj.GetJobOrderStatusBySiteDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.JoDateFrom, objNew.JoDateTo);
            var result = new DataSourceResult()
            {
                Data = data,
                Total = data.Count()
            };
            return Json(result);
        }

        /// <summary>
        /// _s the job order status by site search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._JobOrderStatusBySiteSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _JobOrderStatusBySiteSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            JobOrderStatusBySiteSearchData objJobOrderStatusBySiteSearchData = new JobOrderStatusBySiteSearchData();
            objJobOrderStatusBySiteSearchData = obj.GetJobOrderBySiteReportData(reportId, dashboardMenuId);
            if (objJobOrderStatusBySiteSearchData.JoDateFrom == DateTime.MinValue && objJobOrderStatusBySiteSearchData.DashboardRepotMenuID == 0)
            {
                objJobOrderStatusBySiteSearchData.DashboardRepotMenuID = dashboardMenuId;
                objJobOrderStatusBySiteSearchData.reportID = reportId;
                objJobOrderStatusBySiteSearchData.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objJobOrderStatusBySiteSearchData.JoDateTo = DateTime.Now.Date;
            }

            return PartialView(Pages.PartialViews.JobOrderStatusBySiteSearch, objJobOrderStatusBySiteSearchData);
        }

        /// <summary>
        /// Saves the job order status by site search.
        /// </summary>
        /// <param name="objJobOrderStatusBySiteSearchData">The object job order status by site search data.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobOrderStatusBySiteSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJobOrderStatusBySiteSearch(JobOrderStatusBySiteSearchData objJobOrderStatusBySiteSearchData)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveJobOrderStatusBySiteReport(objJobOrderStatusBySiteSearchData);
            if (flag)
            {
                return Json(new object[] { 1, objJobOrderStatusBySiteSearchData.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        #endregion

        #region "Top 10 Asset Grid"

        /// <summary>
        /// _s the top ten asset search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._TopTenAssetSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _TopTenAssetSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            TopTenAssetModal objTopTenAssetModal = new TopTenAssetModal();
            objTopTenAssetModal = obj.GetReportFilterDataForTopTenAsset(reportId, dashboardMenuId);
            if (objTopTenAssetModal.JoDateFrom == DateTime.MinValue && objTopTenAssetModal.DashboardRepotMenuID == 0)
            {
                objTopTenAssetModal.DashboardRepotMenuID = dashboardMenuId;
                objTopTenAssetModal.reportID = reportId;
                objTopTenAssetModal.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objTopTenAssetModal.JoDateTo = DateTime.Now.Date;
                objTopTenAssetModal.LocationID = string.Empty;
            }

            if (objTopTenAssetModal.LocationID != string.Empty)
            {
                objTopTenAssetModal.LocationNo = obj.GetLocationNoByID(objTopTenAssetModal.LocationID);
            }

            return PartialView(Pages.PartialViews.TopTenAssetSearch, objTopTenAssetModal);
        }

        /// <summary>
        /// Top10s the asset breakdowns.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Top10AssetBreakdowns([DataSourceRequest]DataSourceRequest request, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();

            TopTenAssetModal objTopTenAssetModal = new TopTenAssetModal();
            objTopTenAssetModal = obj.GetReportFilterDataForTopTenAsset(SystemEnum.DashboardModule.Top10AssetBreakdown.GetHashCode(), dashboardReportMenuID);
            TopTenAssetModal objNew = new TopTenAssetModal();
            if (objTopTenAssetModal != null && objTopTenAssetModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objTopTenAssetModal.L1ID;
                objNew.L2ID = objTopTenAssetModal.L2ID;
                objNew.L3ID = objTopTenAssetModal.L3ID == null ? string.Empty : objTopTenAssetModal.L3ID;
                objNew.L4ID = objTopTenAssetModal.L4ID;
                objNew.L5ID = objTopTenAssetModal.L5ID;
                objNew.LocationID = objTopTenAssetModal.LocationID;
                objNew.JoDateFrom = objTopTenAssetModal.JoDateFrom;
                objNew.JoDateTo = objTopTenAssetModal.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.LocationID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetTop10AssetBreakdownsDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.LocationID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Top10s the asset downtime.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Top10AssetDowntime([DataSourceRequest]DataSourceRequest request, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CustomDashboard obj = new CustomDashboard();
            TopTenAssetModal objTopTenAssetModal = new TopTenAssetModal();
            objTopTenAssetModal = obj.GetReportFilterDataForTopTenAsset(SystemEnum.DashboardModule.Top10AssetDowntime.GetHashCode(), dashboardReportMenuID);
            TopTenAssetModal objNew = new TopTenAssetModal();
            if (objTopTenAssetModal != null && objTopTenAssetModal.JoDateFrom != DateTime.MinValue)
            {
                objNew.L1ID = objTopTenAssetModal.L1ID;
                objNew.L2ID = objTopTenAssetModal.L2ID;
                objNew.L3ID = objTopTenAssetModal.L3ID == null ? string.Empty : objTopTenAssetModal.L3ID;
                objNew.L4ID = objTopTenAssetModal.L4ID;
                objNew.L5ID = objTopTenAssetModal.L5ID;
                objNew.LocationID = objTopTenAssetModal.LocationID;
                objNew.JoDateFrom = objTopTenAssetModal.JoDateFrom;
                objNew.JoDateTo = objTopTenAssetModal.JoDateTo;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.LocationID = string.Empty;
                objNew.JoDateFrom = DateTime.Now.Date.AddMonths(-1).AddDays(-DateTime.Now.Date.Day - 1);
                objNew.JoDateTo = DateTime.Now.Date;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetTop10AssetDowntimeDetail(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.LocationID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Saves the top ten asset search.
        /// </summary>
        /// <param name="objTopTenAssetModal">The object top ten asset modal.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveTopTenAssetSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTopTenAssetSearch(TopTenAssetModal objTopTenAssetModal)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveReporForTopTenAssetData(objTopTenAssetModal);
            if (flag)
            {
                return Json(new object[] { 1, objTopTenAssetModal.reportID, objTopTenAssetModal.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Gets all location drop down.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllLocationDropDown)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetAllLocationDropDown([DataSourceRequest]DataSourceRequest request)
        {
            IList<Location> lstBuildings = new List<Location>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                LocationService objService = new LocationService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetLocationPage(string.Empty, empID, ProjectSession.IsCentral, 0, sortExpression, sortDirection),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the chart div.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChartDiv(int reportId, int dashboardMenuId)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.DeleteChartByDashboardMenuID(reportId, dashboardMenuId);
            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        #endregion

        #region "Work Status Report"

        /// <summary>
        /// _s the work status search.
        /// </summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._WorkStatusSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _WorkStatusSearch(int reportId, int dashboardMenuId)
        {
            CustomDashboard obj = new CustomDashboard();
            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(reportId, dashboardMenuId);
            objWorkReportFilterModal.DashboardRepotMenuID = dashboardMenuId;
            objWorkReportFilterModal.reportID = reportId;

            return PartialView(Pages.PartialViews.WorkStatusReportSearch, objWorkReportFilterModal);
        }

        /// <summary>
        /// Saves the work status search.
        /// </summary>
        /// <param name="objWorkReportFilterModal">The object work report filter modal.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveWorkStatusSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveWorkStatusSearch(WorkReportFilterModal objWorkReportFilterModal)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.SaveWorkStatusReportData(objWorkReportFilterModal);
            if (flag)
            {
                return Json(new object[] { 1, objWorkReportFilterModal.reportID, objWorkReportFilterModal.DashboardRepotMenuID, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// _s the work status detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="workStatusID">The work status identifier.</param>
        /// <param name="dashboardId">The dashboard identifier.</param>
        /// <returns></returns>
        [ActionName("_WorkStatusDetail")]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _WorkStatusDetail(int id, int workStatusID, int dashboardId)
        {
            ViewBag.WorkStatusID = workStatusID;
            ViewBag.DashboardReportId = dashboardId;
            if (id == 1)
            {
                ViewBag.PMID = 0;
                return PartialView(Pages.PartialViews.WorkOrderStatus);
            }
            else if (id == 2)
            {
                return PartialView(Pages.PartialViews.WorkRequestStatus);
            }
            else if (id == 3)
            {
                ViewBag.PMID = 1;
                return PartialView(Pages.PartialViews.WorkOrderStatus);
            }
            else if (id == 4)
            {
                return PartialView(Pages.PartialViews.PurchaseOrderStatus);
            }
            else
            {
                return PartialView(Pages.PartialViews.PurchaseRequestStatus);
            }
        }

        /// <summary>
        /// Works the status report details.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param> 
        /// <returns></returns>
        [HttpPost]
        public ActionResult WorkStatusReportDetails([DataSourceRequest]DataSourceRequest request, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();
            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(SystemEnum.DashboardModule.WorkStatusReport.GetHashCode(), dashboardReportMenuID);
            WorkReportFilterModal objNew = new WorkReportFilterModal();
            if (objWorkReportFilterModal.DashboardRepotMenuID != 0)
            {
                objNew.L1ID = objWorkReportFilterModal.L1ID;
                objNew.L2ID = objWorkReportFilterModal.L2ID;
                objNew.L3ID = objWorkReportFilterModal.L3ID != null ? objWorkReportFilterModal.L3ID : string.Empty;
                objNew.L4ID = objWorkReportFilterModal.L4ID != null ? objWorkReportFilterModal.L4ID : string.Empty;
                objNew.L5ID = objWorkReportFilterModal.L5ID != null ? objWorkReportFilterModal.L5ID : string.Empty;
                objNew.MaintDivID = objWorkReportFilterModal.MaintDivID;
                objNew.MaintDeptID = objWorkReportFilterModal.MaintDeptID != null ? objWorkReportFilterModal.MaintDeptID : string.Empty;
                objNew.MaintSubDeptID = objWorkReportFilterModal.MaintSubDeptID != null ? objWorkReportFilterModal.MaintSubDeptID : string.Empty;
                objNew.JobTradeID = objWorkReportFilterModal.JobTradeID != null ? objWorkReportFilterModal.JobTradeID : string.Empty;
                objNew.JobTypeID = objWorkReportFilterModal.JobTypeID != null ? objWorkReportFilterModal.JobTypeID : string.Empty;
            }
            else
            {
                objNew.L1ID = 0;
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JobTradeID = string.Empty;
                objNew.JobTypeID = string.Empty;
            }

            int datePickerId = objWorkReportFilterModal.DatePickerID;
            if (datePickerId > 0)
            {
                DateTime now = DateTime.Now;
                if (datePickerId == 4)
                {
                    objNew.JoDateFrom = objWorkReportFilterModal.JoDateFrom;
                    objNew.JoDateTo = objWorkReportFilterModal.JoDateTo;
                }
                else if (datePickerId == 1)
                {
                    objNew.JoDateFrom = now;
                    objNew.JoDateTo = now;
                }
                else if (datePickerId == 2)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, now.Month, 1);
                    objNew.JoDateTo = objNew.JoDateFrom.AddMonths(1).AddDays(-1);
                }
                else if (datePickerId == 3)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, 1, 1);
                    objNew.JoDateTo = new DateTime(now.Year, 12, 31);
                }
            }
            else
            {
                objNew.JoDateFrom = DateTime.Now;
                objNew.JoDateTo = DateTime.Now;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetWorkStatusReport(ProjectSession.IsCentral, objNew.L1ID, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTradeID, objNew.JobTypeID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the work order status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workStatusId">The work status identifier.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <param name="reportPM">The report pm.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetWorkOrderStatus([DataSourceRequest]DataSourceRequest request, int workStatusId, int dashboardReportMenuID, int reportPM)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();

            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(SystemEnum.DashboardModule.WorkStatusReport.GetHashCode(), dashboardReportMenuID);
            WorkReportFilterModal objNew = new WorkReportFilterModal();
            if (objWorkReportFilterModal.DashboardRepotMenuID != 0)
            {
                objNew.L2ID = objWorkReportFilterModal.L2ID;
                objNew.L3ID = objWorkReportFilterModal.L3ID != null ? objWorkReportFilterModal.L3ID : string.Empty;
                objNew.L4ID = objWorkReportFilterModal.L4ID != null ? objWorkReportFilterModal.L4ID : string.Empty;
                objNew.L5ID = objWorkReportFilterModal.L5ID != null ? objWorkReportFilterModal.L5ID : string.Empty;
                objNew.MaintDivID = objWorkReportFilterModal.MaintDivID;
                objNew.MaintDeptID = objWorkReportFilterModal.MaintDeptID != null ? objWorkReportFilterModal.MaintDeptID : string.Empty;
                objNew.MaintSubDeptID = objWorkReportFilterModal.MaintSubDeptID != null ? objWorkReportFilterModal.MaintSubDeptID : string.Empty;
                objNew.JobTradeID = objWorkReportFilterModal.JobTradeID != null ? objWorkReportFilterModal.JobTradeID : string.Empty;
                objNew.JobTypeID = objWorkReportFilterModal.JobTypeID != null ? objWorkReportFilterModal.JobTypeID : string.Empty;
            }
            else
            {
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JobTradeID = string.Empty;
                objNew.JobTypeID = string.Empty;
            }

            int datePickerId = objWorkReportFilterModal.DatePickerID;
            if (datePickerId > 0)
            {
                DateTime now = DateTime.Now;
                if (datePickerId == 4)
                {
                    objNew.JoDateFrom = objWorkReportFilterModal.JoDateFrom;
                    objNew.JoDateTo = objWorkReportFilterModal.JoDateTo;
                }
                else if (datePickerId == 1)
                {
                    objNew.JoDateFrom = now;
                    objNew.JoDateTo = now;
                }
                else if (datePickerId == 2)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, now.Month, 1);
                    objNew.JoDateTo = objNew.JoDateFrom.AddMonths(1).AddDays(-1);
                }
                else if (datePickerId == 3)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, 1, 1);
                    objNew.JoDateTo = new DateTime(now.Year, 12, 31);
                }
            }
            else
            {
                objNew.JoDateFrom = DateTime.Now;
                objNew.JoDateTo = DateTime.Now;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetWorkOrderStatus(ProjectSession.IsCentral, workStatusId, reportPM, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTradeID, objNew.JobTypeID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the work request status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workStatusId">The work status identifier.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetWorkRequestStatus([DataSourceRequest]DataSourceRequest request, int workStatusId, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();

            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(SystemEnum.DashboardModule.WorkStatusReport.GetHashCode(), dashboardReportMenuID);
            WorkReportFilterModal objNew = new WorkReportFilterModal();
            if (objWorkReportFilterModal.DashboardRepotMenuID != 0)
            {
                objNew.L2ID = objWorkReportFilterModal.L2ID;
                objNew.L3ID = objWorkReportFilterModal.L3ID != null ? objWorkReportFilterModal.L3ID : string.Empty;
                objNew.L4ID = objWorkReportFilterModal.L4ID != null ? objWorkReportFilterModal.L4ID : string.Empty;
                objNew.L5ID = objWorkReportFilterModal.L5ID != null ? objWorkReportFilterModal.L5ID : string.Empty;
                objNew.MaintDivID = objWorkReportFilterModal.MaintDivID;
                objNew.MaintDeptID = objWorkReportFilterModal.MaintDeptID != null ? objWorkReportFilterModal.MaintDeptID : string.Empty;
                objNew.MaintSubDeptID = objWorkReportFilterModal.MaintSubDeptID != null ? objWorkReportFilterModal.MaintSubDeptID : string.Empty;
                objNew.JobTradeID = objWorkReportFilterModal.JobTradeID != null ? objWorkReportFilterModal.JobTradeID : string.Empty;
                objNew.JobTypeID = objWorkReportFilterModal.JobTypeID != null ? objWorkReportFilterModal.JobTypeID : string.Empty;
            }
            else
            {
                objNew.L2ID = 0;
                objNew.L3ID = string.Empty;
                objNew.L4ID = string.Empty;
                objNew.L5ID = string.Empty;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
                objNew.JobTradeID = string.Empty;
                objNew.JobTypeID = string.Empty;
            }

            int datePickerId = objWorkReportFilterModal.DatePickerID;
            if (datePickerId > 0)
            {
                DateTime now = DateTime.Now;
                if (datePickerId == 4)
                {
                    objNew.JoDateFrom = objWorkReportFilterModal.JoDateFrom;
                    objNew.JoDateTo = objWorkReportFilterModal.JoDateTo;
                }
                else if (datePickerId == 1)
                {
                    objNew.JoDateFrom = now;
                    objNew.JoDateTo = now;
                }
                else if (datePickerId == 2)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, now.Month, 1);
                    objNew.JoDateTo = objNew.JoDateFrom.AddMonths(1).AddDays(-1);
                }
                else if (datePickerId == 3)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, 1, 1);
                    objNew.JoDateTo = new DateTime(now.Year, 12, 31);
                }
            }
            else
            {
                objNew.JoDateFrom = DateTime.Now;
                objNew.JoDateTo = DateTime.Now;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetWorkRequestStatus(ProjectSession.IsCentral, workStatusId, objNew.L2ID, objNew.L3ID, objNew.L4ID, objNew.L5ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JobTradeID, objNew.JobTypeID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchase order status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workStatusId">The work status identifier.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPurchaseOrderStatus([DataSourceRequest]DataSourceRequest request, int workStatusId, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();

            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(SystemEnum.DashboardModule.WorkStatusReport.GetHashCode(), dashboardReportMenuID);
            WorkReportFilterModal objNew = new WorkReportFilterModal();
            if (objWorkReportFilterModal.DashboardRepotMenuID != 0)
            {
                objNew.L2ID = objWorkReportFilterModal.L2ID;
                objNew.MaintDivID = objWorkReportFilterModal.MaintDivID;
                objNew.MaintDeptID = objWorkReportFilterModal.MaintDeptID != null ? objWorkReportFilterModal.MaintDeptID : string.Empty;
                objNew.MaintSubDeptID = objWorkReportFilterModal.MaintSubDeptID != null ? objWorkReportFilterModal.MaintSubDeptID : string.Empty;
            }
            else
            {
                objNew.L2ID = 0;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
            }

            int datePickerId = objWorkReportFilterModal.DatePickerID;
            if (datePickerId > 0)
            {
                DateTime now = DateTime.Now;
                if (datePickerId == 4)
                {
                    objNew.JoDateFrom = objWorkReportFilterModal.JoDateFrom;
                    objNew.JoDateTo = objWorkReportFilterModal.JoDateTo;
                }
                else if (datePickerId == 1)
                {
                    objNew.JoDateFrom = now;
                    objNew.JoDateTo = now;
                }
                else if (datePickerId == 2)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, now.Month, 1);
                    objNew.JoDateTo = objNew.JoDateFrom.AddMonths(1).AddDays(-1);
                }
                else if (datePickerId == 3)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, 1, 1);
                    objNew.JoDateTo = new DateTime(now.Year, 12, 31);
                }
            }
            else
            {
                objNew.JoDateFrom = DateTime.Now;
                objNew.JoDateTo = DateTime.Now;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetPurchaseOrderStatus(ProjectSession.IsCentral, workStatusId, objNew.L2ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchase request status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workStatusId">The work status identifier.</param>
        /// <param name="dashboardReportMenuID">The dashboard report menu identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPurchaseRequestStatus([DataSourceRequest]DataSourceRequest request, int workStatusId, int dashboardReportMenuID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomDashboard obj = new CustomDashboard();

            WorkReportFilterModal objWorkReportFilterModal = new WorkReportFilterModal();
            objWorkReportFilterModal = obj.GetReportFilterDataForWorkStatusModal(SystemEnum.DashboardModule.WorkStatusReport.GetHashCode(), dashboardReportMenuID);
            WorkReportFilterModal objNew = new WorkReportFilterModal();
            if (objWorkReportFilterModal.DashboardRepotMenuID != 0)
            {
                objNew.L2ID = objWorkReportFilterModal.L2ID;
                objNew.MaintDivID = objWorkReportFilterModal.MaintDivID;
                objNew.MaintDeptID = objWorkReportFilterModal.MaintDeptID != null ? objWorkReportFilterModal.MaintDeptID : string.Empty;
                objNew.MaintSubDeptID = objWorkReportFilterModal.MaintSubDeptID != null ? objWorkReportFilterModal.MaintSubDeptID : string.Empty;
            }
            else
            {
                objNew.L2ID = 0;
                objNew.MaintDivID = 0;
                objNew.MaintDeptID = string.Empty;
                objNew.MaintSubDeptID = string.Empty;
            }

            int datePickerId = objWorkReportFilterModal.DatePickerID;
            if (datePickerId > 0)
            {
                DateTime now = DateTime.Now;
                if (datePickerId == 4)
                {
                    objNew.JoDateFrom = objWorkReportFilterModal.JoDateFrom;
                    objNew.JoDateTo = objWorkReportFilterModal.JoDateTo;
                }
                else if (datePickerId == 1)
                {
                    objNew.JoDateFrom = now;
                    objNew.JoDateTo = now;
                }
                else if (datePickerId == 2)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, now.Month, 1);
                    objNew.JoDateTo = objNew.JoDateFrom.AddMonths(1).AddDays(-1);
                }
                else if (datePickerId == 3)
                {
                    objNew.JoDateFrom = new DateTime(now.Year, 1, 1);
                    objNew.JoDateTo = new DateTime(now.Year, 12, 31);
                }
            }
            else
            {
                objNew.JoDateFrom = DateTime.Now;
                objNew.JoDateTo = DateTime.Now;
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetPurchaseRequestStatus(ProjectSession.IsCentral, workStatusId, objNew.L2ID, objNew.MaintDivID, objNew.MaintDeptID, objNew.MaintSubDeptID, objNew.JoDateFrom, objNew.JoDateTo, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Updates the date picker.
        /// </summary>
        /// <param name="datePickerID">The date picker identifier.</param>
        /// <param name="dashboardID">The dashboard identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateDatePicker(int datePickerID, int dashboardID)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.UpdateDatePickerID(datePickerID, dashboardID);
            if (flag)
            {
                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Changes the range date.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="dashboardID">The dashboard identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ChangeRangeDate(DateTime startDate, DateTime endDate, int dashboardID)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.ChangeDateRangeSelection(startDate, endDate, dashboardID);
            if (flag)
            {
                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        #endregion

        #endregion

        #region "Add / Edit Widget"

        /// <summary>
        /// _s the add widget.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions._AddWidget)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _AddWidget()
        {
            IEnumerable<SelectListItem> dashboardModule = Enum.GetValues(typeof(SystemEnum.DashboardModule)).Cast<SystemEnum.DashboardModule>().Select(x => new SelectListItem()
            {
                Text = SystemEnum.GetEnumName(typeof(SystemEnum.DashboardModule), (int)x),
                Value = ((int)x).ToString()
            }).ToList();
            ViewBag.DashboardModule = dashboardModule;

            return PartialView(Pages.PartialViews.AddWidget);
        }

        /// <summary>
        /// Saves the widget.
        /// </summary>
        /// <param name="isChecked">if set to <c>true</c> [is checked].</param>
        /// <param name="reportId">The report identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveWidget)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveWidget(bool isChecked, int reportId)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.InsertWidet(isChecked, reportId);
            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Gets all widget.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAllWidget([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ReportUniqueName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            DashboardReportMenu objDashboardReportMenu = new DashboardReportMenu();

            CustomDashboard objCustomDashboard = new CustomDashboard();

            var result = new DataSourceResult()
            {
                Data = objCustomDashboard.GetDashBoardReportMenuForGrid(pageNumber, sortExpression, sortDirection),
                Total = objCustomDashboard.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        #endregion

        #region "Drop Down Bind Method"

        /// <summary>
        /// Gets the city by organisation identifier.
        /// </summary>
        /// <param name="orgId">The city identifier.</param>
        /// <returns></returns>
        [ActionName("GetCityByOrgID")]
        public ActionResult GetCityByOrgID(int orgId)
        {
            var result = CustomDashboard.GetCityByOrgID(orgId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the area by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <returns></returns>
        [ActionName("GetAreaByCityID")]
        public ActionResult GetAreaByCityID(int cityId, int? orgID = 0)
        {
            ////var result = CustomDashboard.GetAreaByCityID(cityId);
            AreaService objService = new AreaService();
            var result = objService.GetAreaDetailsByL2ID(cityId, 1, Convert.ToInt32(orgID));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the zone by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        [ActionName("GetZoneByCityID")]
        public ActionResult GetZoneByCityID(int cityId, string areaId, int? orgID = 0)
        {
            var result = CustomDashboard.GetZoneByCityID(cityId, areaId, Convert.ToInt32(orgID));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the building by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <param name="zoneId">The zone identifier.</param>
        /// <returns></returns>
        [ActionName("GetBuildingByCityID")]
        public ActionResult GetBuildingByCityID(int cityId, string areaId, string zoneId, int? orgID = 0)
        {
            var result = CustomDashboard.GetBuildingByCityID(cityId, areaId, zoneId, Convert.ToInt32(orgID));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the location by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <param name="zoneId">The zone identifier.</param>
        /// <returns></returns>
        [ActionName("GetLocationByCityID")]
        public ActionResult GetLocationByCityID(int cityId, string areaId, string zoneId, string buildingId)
        {
            var result = CustomDashboard.GetLocationByCityID(cityId, areaId, zoneId, buildingId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// Gets the asset by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <returns></returns>
        [ActionName("GetAssetByCityID")]
        public ActionResult GetAssetByCityID(int cityId)
        {
            var result = CustomDashboard.GetAssetByCityID(cityId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// Gets the employee by city identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <returns></returns>
        [ActionName("GetEmployeeByCityID")]
        public ActionResult GetEmployeeByCityID(int cityId)
        {
            var result = CustomDashboard.GetEmployeeByCityID(cityId);
            var jsonResult = Json(result, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        /// <summary>
        /// Gets the Maintenance sub Department by Maintenance Department identifier.
        /// </summary>
        /// <param name="maintDeptid">The Maintenance Department id.</param> 
        /// <returns></returns>
        [ActionName("GetMaintSubDeptByDeptsID")]
        public ActionResult GetMaintSubDeptByDeptsID(string maintDeptid)
        {
            var result = CustomDashboard.GetSubDeptByDeptsId(maintDeptid);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the Maintenance Department by divisions identifier.
        /// </summary>
        /// <param name="maintDivisionId">The Maintenance division identifier.</param>
        /// <returns></returns>
        [ActionName("GetMaintDeptByDivisionsID")]
        public ActionResult GetMaintDeptByDivisionsID(int maintDivisionId)
        {
            var result = CustomDashboard.GetMaintDeptByDivisionsId(maintDivisionId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Changes the type of the chart.
        /// </summary>
        /// <param name="chartTypeId">The chart type identifier.</param>
        /// <param name="dashboardMenuId">The dashboard menu identifier.</param>
        /// <returns></returns>
        [ActionName("ChangeChartType")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ChangeChartType(int chartTypeId, int dashboardMenuId)
        {
            string validations = string.Empty;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            bool flag = objCustomDashboard.ChangeChartTypeByDashboardMenuID(chartTypeId, dashboardMenuId);
            if (flag)
            {
                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
            }
        }

        /// <summary>
        /// Updates the display order of widget.
        /// </summary>
        /// <param name="lstDashboardMenu">The LST dashboard menu.</param>
        /// <returns></returns>
        [ActionName("UpdateDisplayOrderofWidget")]
        public ActionResult UpdateDisplayOrderofWidget(List<DashboardReportMenu> lstDashboardMenu)
        {
            bool flag = false;
            CustomDashboard objCustomDashboard = new CustomDashboard();
            if (lstDashboardMenu.Count > 0)
            {
                flag = objCustomDashboard.UpdateDisplayOrderByWidget(lstDashboardMenu);
            }

            return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
        }

        #endregion
    }
}