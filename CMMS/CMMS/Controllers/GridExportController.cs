﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMMS.Controllers
{
    /// <summary>
    /// Grid Export controller
    /// </summary>
    public class GridExportController : Controller
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Excel_s the export_ save.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <param name="base64">The base64.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }
    }
}