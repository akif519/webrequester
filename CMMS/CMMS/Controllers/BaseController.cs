﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Globalization;
using CMMS.Infrastructure;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Model;
using CMMS.Service.UserAuthentication;

namespace CMMS.Controllers
{
    /// <summary>
    /// Base Controller
    /// </summary>
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    [SessionExpire]
    public abstract partial class BaseController : Controller
    {
        /////// <summary>
        /////// Called before the action method is invoked.
        /////// </summary>
        /////// <param name="filterContext">Information about the current request and action.</param>
        ////protected override void OnActionExecuting(ActionExecutingContext filterContext)
        ////{
        ////    try
        ////    {
        ////        if (ProjectSession.EmployeeID <= 0)
        ////        {
        ////            filterContext.Result = RedirectToAction(Actions.Login, Pages.Controllers.Account, new { area = string.Empty });                    
        ////            base.OnActionExecuting(filterContext);
        ////        }

        ////        base.OnActionExecuting(filterContext);
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        throw ex;
        ////    }
        ////}

        /// <summary>
        /// Initializes data that might not be available when the constructor is called.
        /// </summary>
        /// <param name="requestContext">The HTTP context and route data.</param>
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (ProjectSession.DateCulture == ProjectConfiguration.GregorianDate)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(ProjectConfiguration.EnglishCultureCode);
            }
            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo(ProjectConfiguration.ArabicCultureCode);
            }
        }
    }

    /// <summary>
    /// Session Expire Attribute
    /// </summary>
    public class SessionExpireAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                bool isAllow = true;
                if (ProjectSession.EmployeeID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.Login.ToLower())
                {
                    isAllow = true;
                }
                else if (ProjectSession.EmployeeID == 0 && filterContext.ActionDescriptor.ActionName.ToLower() == Actions.ForgotPassword.ToLower())
                {
                    isAllow = true;
                }
                else if (ProjectSession.EmployeeID > 0)
                {
                    isAllow = true;
                    UserAuthenticationData.UpdateSessionsLastHeartBeat(ProjectSession.EmployeeID);

                    if (ProjectSession.SessionID > 0)
                    {
                        using (ServiceContext context = new ServiceContext())
                        {
                            Session objSession = new Session();
                            objSession.EmployeeId = ProjectSession.EmployeeID;
                            objSession.SessionId = ProjectSession.SessionID;
                            int count = context.Search<Session>(objSession).Count;
                            if (count > 0)
                            {
                                isAllow = true;
                            }
                            else
                            {
                                isAllow = false;
                            }
                        }
                    }
                }
                else
                {
                    isAllow = false;
                }

                if (!isAllow)
                {
                    filterContext.Result = new RedirectResult("~/Account/Login");
                    return;
                }

                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }
    }
}