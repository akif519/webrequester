﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMMS.Controllers
{
    /// <summary>
    /// class for Error Handling
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Errors the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult Error(Exception exception)
        {
            ////Response.ContentType = "text/html";
            return View("Error", new HandleErrorInfo(exception, "Error", "Error"));
        }

        /// <summary>
        /// Pages the not found.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns></returns>
        [HttpGet]
        public ViewResult PageNotFound(Exception exception)
        {
            ////Response.ContentType = "text/html";
            return View("PageNotFound", new HandleErrorInfo(exception, "Error", "PageNotFound"));
            ////return View(exception);
        }
    }
}