﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    /// Declare Actions names
    /// </summary>
    public class Actions
    {
        #region "Front-Side"

        /// <summary>
        /// The login
        /// </summary>
        public const string Login = "login";

        /// <summary>
        /// The forgot password
        /// </summary>
        public const string ForgotPassword = "forgot-password";

        /// <summary>
        /// The logout
        /// </summary>
        public const string Logout = "logout";

        public const string SessionTimeOutLogout = "session-timeout-logout";

        /// <summary>
        /// The access denied
        /// </summary>
        public const string AccessDenied = "access-denied";

        /// <summary>
        /// The access denied
        /// </summary>
        public const string DisplayError = "display-error";

        /// <summary>
        /// The session expired for customer
        /// </summary>
        public const string SessionExpiredForCustomer = "session-expired-for-customer";

        #endregion

        #region "Home"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string Dashboard = "dashboard";

        /// <summary>
        /// The select language
        /// </summary>
        public const string SelectLanguage = "select-language";

        /// <summary>
        /// The select date culture
        /// </summary>
        public const string SelectDateCulture = "select-date-culture";

        #region "Dashboard Chart"

        /// <summary>
        /// The _ chart search filter
        /// </summary>
        public const string _ChartSearchFilter = "_chart-search-filter";

        /// <summary>
        /// The _ add widget
        /// </summary>
        public const string _AddWidget = "_add-widget";

        /// <summary>
        /// The save widget
        /// </summary>
        public const string SaveWidget = "save-widget";

        /// <summary>
        /// The save search data
        /// </summary>
        public const string SaveSearchData = "save-search-data";

        /// <summary>
        /// The _ job order status by site search
        /// </summary>
        public const string _JobOrderStatusBySiteSearch = "_job-order-status-by-site-search";

        /// <summary>
        /// The save job order status by site search
        /// </summary>
        public const string SaveJobOrderStatusBySiteSearch = "save-job-order-status-by-site-search";

        /// <summary>
        /// The _ job order type search
        /// </summary>
        public const string _JobOrderTypeSearch = "_job-order-type-search";

        /// <summary>
        /// The save job order type search
        /// </summary>
        public const string SaveJobOrderTypeSearch = "save-job-order-type-search";

        /// <summary>
        /// The _ top ten asset search
        /// </summary>
        public const string _TopTenAssetSearch = "_top-ten-asset-search";

        /// <summary>
        /// The save top ten asset search
        /// </summary>
        public const string SaveTopTenAssetSearch = "save-top-ten-asset-search";

        /// <summary>
        /// The get all location drop down
        /// </summary>
        public const string GetAllLocationDropDown = "get-all-location-dropdown";

        /// <summary>
        /// The _ pm compliance search
        /// </summary>
        public const string _PMComplianceSearch = "_pm-compliance-search";

        /// <summary>
        /// The save pm compliance search
        /// </summary>
        public const string SavePMComplianceSearch = "save-pm-compliance-search";

        /// <summary>
        /// The _ job trading search
        /// </summary>
        public const string _JobTradingSearch = "_job-trading-search";

        /// <summary>
        /// The save job trading search
        /// </summary>
        public const string SaveJobTradingSearch = "save-job-trading-search";

        /// <summary>
        /// The _ work status search
        /// </summary>
        public const string _WorkStatusSearch = "_work-status-search";

        /// <summary>
        /// The save work status search
        /// </summary>
        public const string SaveWorkStatusSearch = "save-work-status-search";

        #endregion

        #endregion

        #region "Configurations"

        #region "Sector"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string SectorList = "sector-list";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetSectorList = "get-sector-list";

        /// <summary>
        /// The get sector list by identifier
        /// </summary>
        public const string GetSectorListByID = "get-sector-list-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageSector = "manage-sector";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteSector = "remove-sector";

        /// <summary>
        /// The get sectors
        /// </summary>
        public const string GetSectors = "get-sectors";

        #endregion

        #region "Organisation"

        /// <summary>
        /// The Org
        /// </summary>
        public const string Organisation = "organisation";

        /// <summary>
        /// The get Org
        /// </summary>
        public const string GetOrganisation = "get-organisation";

        /// <summary>
        /// The get Organization by Permission
        /// </summary>
        public const string GetOrganisationByPermission = "get-organisation-by-permission";

        /// <summary>
        /// The get Org by identifier
        /// </summary>
        public const string GetOrganisationByID = "get-organisation-by-id";

        /// <summary>
        /// The manage Org
        /// </summary>
        public const string ManageOrganisation = "manage-organisation";

        /// <summary>
        /// The delete Org
        /// </summary>
        public const string DeleteOrganisation = "remove-organisation";

        /// <summary>
        /// The get all Org
        /// </summary>
        public const string GetAllOrganisation = "get-all-organisation";

        /// <summary>
        /// The get all enable organization
        /// </summary>
        public const string GetAllEnableOrganisation = "get-all-enable-organisation";

        #endregion

        #region "Area"

        /// <summary>
        /// The area
        /// </summary>
        public const string Area = "area";

        /// <summary>
        /// The get area
        /// </summary>
        public const string GetArea = "get-area";

        /// <summary>
        /// The get area by Permission and L2ID
        /// </summary>
        public const string GetAreaByPermissionAndL2ID = "get-area-by-permission";

        /// <summary>
        /// The get area by identifier
        /// </summary>
        public const string GetAreaByID = "get-area-by-id";

        /// <summary>
        /// The manage area
        /// </summary>
        public const string ManageArea = "manage-area";

        /// <summary>
        /// The delete area
        /// </summary>
        public const string DeleteArea = "remove-area";

        /// <summary>
        /// The get all area
        /// </summary>
        public const string GetAllArea = "get-all-area";

        /// <summary>
        /// The get area by L2(city) ID
        /// </summary>
        public const string GetAreabyL2ID = "get-all-area-by-l2id";

        #endregion

        #region "Maintenance Division"

        /// <summary>
        /// The maintenance division
        /// </summary>
        public const string MaintenanceDivision = "maintenance-division";

        /// <summary>
        /// The get maintenance division
        /// </summary>
        public const string GetMaintenanceDivision = "get-maintenance-division";

        /// <summary>
        /// The get maintenance division by identifier
        /// </summary>
        public const string GetMaintenanceDivisionByID = "get-maintenance-division-by-id";

        /// <summary>
        /// The manage maintenance division
        /// </summary>
        public const string ManageMaintenanceDivision = "manage-maintenance-division";

        /// <summary>
        /// The delete maintenance division
        /// </summary>
        public const string DeleteMaintenanceDivision = "remove-maintenance-division";

        /// <summary>
        /// The get maintenance divisions
        /// </summary>
        public const string GetMaintenanceDivisions = "get-maintenance-divisions";

        /// <summary>
        /// The get maintenance divisions by permission
        /// </summary>
        public const string GetMaintenanceDivisionsByPermissions = "get-maintenance-divisions-by-permission";

        /// <summary>
        /// The get maintenance All divisions
        /// </summary>
        public const string GetAllMaintenanceDivisions = "get-all-maintenance-divisions";

        /// <summary>
        /// The get Job Trades
        /// </summary>
        public const string GetJobTrades = "get-job-trades";

        /// <summary>
        /// The get Employee Categories
        /// </summary>
        public const string GetEmpCategories = "get-emp-categories";

        /// <summary>
        /// The get Employee Status
        /// </summary>
        public const string GetEmpStatues = "get-emp-status";

        /// <summary>
        /// The get Employee Profession
        /// </summary>
        public const string GetEmpProfession = "get-emp-profession";

        /// <summary>
        /// The get User Groups
        /// </summary>
        public const string GetUserGroups = "get-user-group";

        /// <summary>
        /// The get Languages
        /// </summary>
        public const string GetLanguage = "get-language";

        /// <summary>
        /// The get Nationalities
        /// </summary>
        public const string GetNationalities = "get-nationalities";
        #endregion

        #region "Maintenance Department"

        /// <summary>
        /// The maintenance department
        /// </summary>
        public const string MaintenanceDepartment = "maintenance-department";

        /// <summary>
        /// The get maintenance department
        /// </summary>
        public const string GetMaintenanceDepartment = "get-maintenance-department";

        /// <summary>
        /// The get maintenance department
        /// </summary>
        public const string GetMaintenanceDepartmentbyPermission = "get-maintenance-department-by-permission";

        /// <summary>
        /// The get maintenance department by identifier
        /// </summary>
        public const string GetMaintenanceDepartmentByID = "get-maintenance-department-by-id";

        /// <summary>
        /// The manage maintenance department
        /// </summary>
        public const string ManageMaintenanceDepartment = "manage-maintenance-department";

        /// <summary>
        /// The delete maintenance department
        /// </summary>
        public const string DeleteMaintenanceDepartment = "remove-maintenance-department";

        /// <summary>
        /// The get maintenance department by division identifier
        /// </summary>
        public const string GetMaintDeptByDivisionID = "get-maintenance-department-by-division-id";

        #endregion

        #region "Failure Cause"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string FailureCauseList = "failure-cause-list";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetFailureCauseList = "get-failure-cause-list";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetFailureCauseListByID = "get-failure-cause-list-by-id";

        /// <summary>
        /// The get sector list by identifier
        /// </summary>
        public const string GetAllFailureCauseListForDDL = "get-failure-cause-list";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageFailureCause = "manage-failure-cause";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteFailureCause = "remove-failure-cause";

        #endregion

        #region "Supplier"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string Suppliers = "suppliers";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetSuppliersList = "get-suppliers-list";

        /// <summary>
        /// The get sector list by identifier
        /// </summary>
        public const string GetSuppliersListByID = "get-suppliers-list-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageSuppliers = "manage-suppliers";

        /// <summary>
        /// The manage suppliers contact
        /// </summary>
        public const string ManageSuppliersContact = "manage-suppliers-contact";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteSuppliers = "remove-suppliers";

        /// <summary>
        /// The _ edit suppliers
        /// </summary>
        public const string _EditSuppliers = "suppliersdetail";

        /// <summary>
        /// The get Auto Complete Supplier
        /// </summary>
        public const string GetSupplierAutoDDL = "get-supplier-auto-ddl";

        /// <summary>
        /// The get Auto Complete Supplier
        /// </summary>
        public const string GetAllSupplierList = "get-supplier-all";

        /// <summary>
        /// The _ supplier documents
        /// </summary>
        public const string _SupplierDocuments = "_SupplierDocuments";

        /// <summary>
        /// The get supplier document
        /// </summary>
        public const string GetSupplierDocument = "GetSupplierDocument";

        /// <summary>
        /// The get supplier document by identifier
        /// </summary>
        public const string GetSupplierDocumentByID = "GetSupplierDocumentByID";

        /// <summary>
        /// The get attachment by identifier
        /// </summary>
        public const string GetAttachmentByID = "GetAttachmentByID";

        /// <summary>
        /// The save supplier document
        /// </summary>
        public const string SaveSupplierDoc = "SaveSupplierDoc";

        /// <summary>
        /// The delete supplier document
        /// </summary>
        public const string DeleteSupplierDoc = "DeleteSupplierDoc";

        /// <summary>
        /// The get supplier category
        /// </summary>
        public const string GetSupplierCategory = "GetSupplierCategory";
        #endregion

        #region "Maintenance Sub Department"

        /// <summary>
        /// The maintenance sub department
        /// </summary>
        public const string MaintenanceSubDepartment = "maintenance-sub-department";

        /// <summary>
        /// The get maintenance sub department
        /// </summary>
        public const string GetMaintenanceSubDepartment = "get-maintenance-sub-department";

        /// <summary>
        /// The get maintenance sub department
        /// </summary>
        public const string GetMaintenanceSubDepartmentWithCheckBox = "get-maintenance-sub-department-checkbox";

        /// <summary>
        /// The get maintenance sub department by identifier
        /// </summary>
        public const string GetMaintenanceSubDepartmentByID = "get-maintenance-sub-department-by-id";

        /// <summary>
        /// The manage maintenance sub department
        /// </summary>
        public const string ManageMaintenanceSubDepartment = "manage-maintenance-sub-department";

        /// <summary>
        /// The delete maintenance sub department
        /// </summary>
        public const string DeleteMaintenanceSubDepartment = "remove-maintenance-sub-department";

        /// <summary>
        /// The get maintenance sub department by department identifier 
        /// </summary>
        public const string GetMaintenanceSubDepartmentByDeptsID = "get-maint-sub-dept-by-depts-id";

        /// <summary>
        /// The get maintenance sub departments
        /// </summary>
        public const string GetMaintenanceSubDepartments = "get-maintenance-sub-departments";

        #endregion

        #region "Maintenance Group"

        /// <summary>
        /// The maintenance group
        /// </summary>
        public const string MaintenanceGroup = "maintenance-group";

        /// <summary>
        /// The get maintenance group
        /// </summary>
        public const string GetMaintenanceGroup = "get-maintenance-group";

        /// <summary>
        /// The get maintenance group having technicians
        /// </summary>
        public const string GetMaintenanceGroupHavingTechnicians = "get-maintenance-group-having-technicians";

        /// <summary>
        /// The get maintenance group by identifier
        /// </summary>
        public const string GetMaintenanceGroupByID = "get-maintenance-group-by-id";

        /// <summary>
        /// The manage maintenance group
        /// </summary>
        public const string ManageMaintenanceGroup = "manage-maintenance-group";

        /// <summary>
        /// The delete maintenance group
        /// </summary>
        public const string DeleteMaintenanceGroup = "remove-maintenance-group";

        /// <summary>
        /// The get L2S
        /// </summary>
        public const string GetL2s = "get-l2s";

        /// <summary>
        /// The get employee maintenance group
        /// </summary>
        public const string GetEmployeeMaintenanceGroup = "get-employee-maintenance-group";

        /// <summary>
        /// The _ edit maintenance group
        /// </summary>
        public const string _EditMaintenanceGroup = "_edit-maintenance-group";

        /// <summary>
        /// The manage employee maintenance group
        /// </summary>
        public const string ManageEmployeeMaintenanceGroup = "manage-employee-maintenance-group";

        /// <summary>
        /// The delete employee maintenance group
        /// </summary>
        public const string DeleteEmployeeMaintenanceGroup = "remove-employee-maintenance-group";

        #endregion

        #region "Zone"

        /// <summary>
        /// The zone
        /// </summary>
        public const string Zone = "zone";

        /// <summary>
        /// The get zone
        /// </summary>
        public const string GetZone = "get-zone";

        /// <summary>
        /// The get zone by identifier
        /// </summary>
        public const string GetZoneByID = "get-zone-by-id";

        /// <summary>
        /// The manage zone
        /// </summary>
        public const string ManageZone = "manage-zone";

        /// <summary>
        /// The delete zone
        /// </summary>
        public const string DeleteZone = "remove-zone";

        /// <summary>
        /// The get zone by l2 identifier
        /// </summary>
        public const string GetZoneByL2ID = "get-zone-l2-by-id";

        #endregion

        #region "Building"

        /// <summary>
        /// The building
        /// </summary>
        public const string Building = "building";

        /// <summary>
        /// The get building
        /// </summary>
        public const string GetBuilding = "get-building";

        /// <summary>
        /// The get building by identifier
        /// </summary>
        public const string GetBuildingByID = "get-building-by-id";

        /// <summary>
        /// The manage building
        /// </summary>
        public const string ManageBuilding = "manage-building";

        /// <summary>
        /// The delete building
        /// </summary>
        public const string DeleteBuilding = "remove-building";

        /// <summary>
        /// The Get Building Details
        /// </summary>
        public const string GetBuildingDetails = "get-building-details";

        /// <summary>
        /// The get building by city identifier
        /// </summary>
        public const string GetBuildingByL2ID = "get-building-by-l2id";
        #endregion

        #region "location"

        /// <summary>
        /// The get locations
        /// </summary>
        public const string GetLocationPage = "get-locations";

        /// <summary>
        /// The get locations
        /// </summary>
        public const string GetLocationAutoDDL = "get-locations-auto-ddl";

        /// <summary>
        /// The get unlinked locations
        /// </summary>
        public const string GetUnLinkedLocationsList = "get-unlinked-locations";

        #region "Location Type"

        /// <summary>
        /// The location type
        /// </summary>
        public const string LocationType = "location-type";

        /// <summary>
        /// The get location type
        /// </summary>
        public const string GetLocationType = "get-location-type";

        /// <summary>
        /// The get location type by identifier
        /// </summary>
        public const string GetLocationTypeByID = "get-location-type-by-id";

        /// <summary>
        /// The manage location type
        /// </summary>
        public const string ManageLocationType = "manage-location-type";

        /// <summary>
        /// The delete location type
        /// </summary>
        public const string DeleteLocationType = "remove-location-type";

        /// <summary>
        /// The get location types
        /// </summary>
        public const string GetLocationTypes = "get-location-types";

        #endregion

        #endregion

        #region "L2"

        /// <summary>
        /// The get l2 by identifier
        /// </summary>
        public const string GetL2ByID = "get-l2-by-id";

        /// <summary>
        /// The get l2 information by identifier
        /// </summary>
        public const string GetL2InformationById = "get-l2-information-by-id";

        #endregion

        #region "Cost Center"

        /// <summary>
        /// The cost center
        /// </summary>
        public const string CostCenter = "cost-center";

        /// <summary>
        /// The get cost center
        /// </summary>
        public const string GetCostCenter = "get-cost-center";

        /// <summary>
        /// The get cost center by identifier
        /// </summary>
        public const string GetCostCenterByID = "get-cost-center-by-id";

        /// <summary>
        /// The get all cost center
        /// </summary>
        public const string GetAllCostCenterForDDL = "get-all-cost-center";

        /// <summary>
        /// The manage cost center
        /// </summary>
        public const string ManageCostCenter = "manage-cost-center";

        /// <summary>
        /// The delete cost center
        /// </summary>
        public const string DeleteCostCenter = "remove-cost-center";

        /// <summary>
        /// The get all cost center
        /// </summary>
        public const string GetCostCenters = "get-cost-centers";
        #endregion

        #region "Employee / User Setup"

        #region "Employee / User Category"

        /// <summary>
        /// The employee category
        /// </summary>
        public const string EmployeeCategory = "employee-category";

        /// <summary>
        /// The get employee category
        /// </summary>
        public const string GetEmployeeCategory = "get-employee-category";

        /// <summary>
        /// The get employee category by identifier
        /// </summary>
        public const string GetEmployeeCategoryByID = "get-employee-category-by-id";

        /// <summary>
        /// The manage employee category
        /// </summary>
        public const string ManageEmployeeCategory = "manage-employee-category";

        /// <summary>
        /// The delete employee category
        /// </summary>
        public const string DeleteEmployeeCategory = "remove-employee-category";

        /// <summary>
        /// To load/save employee documents
        /// </summary>
        public const string _GetEmployeeDocuments = "_get-employee-documents";

        /// <summary>
        /// To load/save employee user login.
        /// </summary>
        public const string _GetEmployeeLoginPage = "get-employee-logininfo";
        #endregion

        #region "Employee / User Status"

        /// <summary>
        /// The employee status
        /// </summary>
        public const string EmployeeStatus = "employee-status";

        /// <summary>
        /// The get employee status
        /// </summary>
        public const string GetEmployeeStatus = "get-employee-status";

        /// <summary>
        /// The get employee status by identifier
        /// </summary>
        public const string GetEmployeeStatusByID = "get-employee-status-by-id";

        /// <summary>
        /// The manage employee status
        /// </summary>
        public const string ManageEmployeeStatus = "manage-employee-status";

        /// <summary>
        /// The delete employee status
        /// </summary>
        public const string DeleteEmployeeStatus = "remove-employee-status";

        #endregion

        #endregion

        #region "Site Setups"

        #region "Site Class"

        /// <summary>
        /// The site class
        /// </summary>
        public const string SiteClass = "site-class";

        /// <summary>
        /// The get site class
        /// </summary>
        public const string GetSiteClass = "get-site-class";

        /// <summary>
        /// The get site class by identifier
        /// </summary>
        public const string GetSiteClassByID = "get-site-class-by-id";

        /// <summary>
        /// The manage site class
        /// </summary>
        public const string ManageSiteClass = "manage-site-class";

        /// <summary>
        /// The delete site class
        /// </summary>
        public const string DeleteSiteClass = "remove-site-class";

        /// <summary>
        /// The get site classes
        /// </summary>
        public const string GetSiteClasses = "get-site-classes";

        #endregion

        #region "Site"

        /// <summary>
        /// The site 
        /// </summary>
        public const string Site = "site";

        /// <summary>
        /// The get site by permission
        /// </summary>
        public const string GetSiteByPermissionAndL1ID = "get-site-by-permission";

        /// <summary>
        /// The get site
        /// </summary>
        public const string GetSite = "get-site";

        /// <summary>
        /// The get site by identifier
        /// </summary>
        public const string GetSiteByID = "get-site-by-id";

        /// <summary>
        /// The manage site
        /// </summary>
        public const string ManageSite = "manage-site";

        /// <summary>
        /// The delete site
        /// </summary>
        public const string DeleteSite = "remove-site";

        /// <summary>
        /// The get cities by employee
        /// </summary>
        public const string GetCitiesByEmployee = "get-cities-by-employee";
        #endregion

        #endregion

        #region "Asset Categories"

        /// <summary>
        /// The asset categories list
        /// </summary>
        public const string AssetCategoriesList = "asset-categories-list";

        /// <summary>
        /// The get asset categories list
        /// </summary>
        public const string GetAssetCategoriesList = "get-asset-categories-list";

        /// <summary>
        /// The get asset categories list by identifier
        /// </summary>
        public const string GetAssetCategoriesListByID = "get-asset-categories-list-by-id";

        /// <summary>
        /// The manage asset categories
        /// </summary>
        public const string ManageAssetCategories = "manage-asset-categories";

        /// <summary>
        /// The delete asset categories
        /// </summary>
        public const string DeleteAssetCategories = "remove-asset-categories";

        /// <summary>
        /// The get all asset categories
        /// </summary>
        public const string GetAllAssetCategories = "get-asset-categories";

        #endregion

        #region "Asset Sub Categories"

        /// <summary>
        /// The asset sub categories list
        /// </summary>
        public const string AssetSubCategoriesList = "asset-sub-categories-list";

        /// <summary>
        /// The get asset sub categories list
        /// </summary>
        public const string GetAssetSubCategoriesList = "get-sub-asset-categories-list";

        /// <summary>
        /// The get asset sub categories list by identifier
        /// </summary>
        public const string GetAssetSubCategoriesListByID = "get-sub-asset-categories-list-by-id";

        /// <summary>
        /// The manage asset sub categories
        /// </summary>
        public const string ManageAssetSubCategories = "manage-asset-sub-categories";

        /// <summary>
        /// The delete asset sub categories
        /// </summary>
        public const string DeleteAssetSubCategories = "remove-asset-sub-categories";

        /// <summary>
        /// The get sub asset categories by category identifier
        /// </summary>
        public const string GetSubAssetCategoriesByCategoryID = "get-sub-asset-categories-by-category-id";
        #endregion

        #region "Asset Class"

        /// <summary>
        /// The asset categories list
        /// </summary>
        public const string AssetClassList = "asset-class-list";

        /// <summary>
        /// The get asset categories list
        /// </summary>
        public const string GetAssetClassList = "get-asset-class-list";

        /// <summary>
        /// The get asset categories list by identifier
        /// </summary>
        public const string GetAssetClassListByID = "get-asset-class-list-by-id";

        /// <summary>
        /// The manage asset categories
        /// </summary>
        public const string ManageAssetClass = "manage-asset-class";

        /// <summary>
        /// The delete asset categories
        /// </summary>
        public const string DeleteAssetClass = "remove-asset-class";

        /// <summary>
        /// The get all asset class
        /// </summary>
        public const string GetAllAssetClass = "get-asset-class";

        /// <summary>
        /// The get all asset class by asset sub cat identifier
        /// </summary>
        public const string GetAllAssetClassByAssetSubCatID = "get-all-asset-class-by-asset-sub-cat-id";
        #endregion

        #region "Asset Conditions"

        /// <summary>
        /// The asset categories list
        /// </summary>
        public const string AssetConditionsList = "asset-conditions-list";

        /// <summary>
        /// The get asset categories list
        /// </summary>
        public const string GetAssetConditionsList = "get-asset-conditions-list";

        /// <summary>
        /// The get asset categories list by identifier
        /// </summary>
        public const string GetAssetConditionsListByID = "get-asset-conditions-list-by-id";

        /// <summary>
        /// The manage asset categories
        /// </summary>
        public const string ManageAssetConditions = "manage-asset-conditions";

        /// <summary>
        /// The delete asset categories
        /// </summary>
        public const string DeleteAssetConditions = "remove-asset-conditions";

        /// <summary>
        /// The get all asset conditions
        /// </summary>
        public const string GetAllAssetConditions = "get-asset-conditions";

        #endregion

        #region "Asset Status"

        /// <summary>
        /// The asset status list
        /// </summary>
        public const string AssetStatusList = "asset-status-list";

        /// <summary>
        /// The get asset status list
        /// </summary>
        public const string GetAssetStatusList = "get-asset-status-list";

        /// <summary>
        /// The get asset status list by identifier
        /// </summary>
        public const string GetAssetStatusListByID = "get-asset-status-list-by-id";

        /// <summary>
        /// The manage asset status
        /// </summary>
        public const string ManageAssetStatus = "manage-asset-status";

        /// <summary>
        /// The delete asset status
        /// </summary>
        public const string DeleteAssetStatus = "remove-asset-status";

        /// <summary>
        /// The get all asset status
        /// </summary>
        public const string GetAllAssetStatus = "get-all-asset-atatus";

        #endregion

        #region "Asset Criticality"

        /// <summary>
        /// The criticality list
        /// </summary>
        public const string CriticalityList = "criticality-list";

        /// <summary>
        /// The get criticality list
        /// </summary>
        public const string GetCriticalityList = "get-criticality-list";

        /// <summary>
        /// The get criticality list by identifier
        /// </summary>
        public const string GetCriticalityListByID = "get-criticality-list-by-id";

        /// <summary>
        /// The manage criticality
        /// </summary>
        public const string ManageCriticality = "manage-criticality";

        /// <summary>
        /// The delete criticality
        /// </summary>
        public const string DeleteCriticality = "remove-criticality";

        /// <summary>
        /// The get all criticalities
        /// </summary>
        public const string GetAllCriticalities = "get-all-criticalities";

        #endregion

        #region "Asset Warranty Contract"

        /// <summary>
        /// The warranty contract list
        /// </summary>
        public const string WarrantyContractList = "warranty-contract-list";

        /// <summary>
        /// The get warranty contract list
        /// </summary>
        public const string GetWarrantyContractList = "get-warranty-contract-list";

        /// <summary>
        /// The get warranty contract list by identifier
        /// </summary>
        public const string GetWarrantyContractListByID = "get-warranty-contract-list-by-id";

        /// <summary>
        /// The manage warranty contract
        /// </summary>
        public const string ManageWarrantyContract = "manage-warranty-contract";

        /// <summary>
        /// The delete warranty contract
        /// </summary>
        public const string DeleteWarrantyContract = "remove-warranty-contract";

        /// <summary>
        /// The get all warranty contract
        /// </summary>
        public const string GetAllWarrantyContract = "get-all-warranty-contract";

        #endregion

        #region "Currency"

        /// <summary>
        /// The get currencies
        /// </summary>
        public const string GetCurrencies = "get-currencied";

        #endregion

        #region "Stores Setup"

        #region "Item Type"

        /// <summary>
        /// The item type
        /// </summary>
        public const string ItemType = "item-type";

        /// <summary>
        /// The get item type
        /// </summary>
        public const string GetItemType = "get-item-type";

        /// <summary>
        /// The get item type by identifier
        /// </summary>
        public const string GetItemTypeByID = "get-item-type-by-id";

        /// <summary>
        /// The manage item type
        /// </summary>
        public const string ManageItemType = "manage-item-type";

        /// <summary>
        /// The delete item type
        /// </summary>
        public const string DeleteItemType = "remove-item-type";

        /// <summary>
        /// The get item types
        /// </summary>
        public const string GetItemTypes = "get-item-types";

        /// <summary>
        /// The item status list
        /// </summary>
        public const string GetItemStatusList = "get-item-status-list";
        #endregion

        #region "Item Sub Type"

        /// <summary>
        /// The item sub type
        /// </summary>
        public const string ItemSubType = "item-sub-type";

        /// <summary>
        /// The get item type
        /// </summary>
        public const string GetItemSubType = "get-item-sub-type";

        /// <summary>
        /// The get item type by identifier
        /// </summary>
        public const string GetItemSubTypeByID = "get-item-sub-type-by-id";

        /// <summary>
        /// The manage item type
        /// </summary>
        public const string ManageItemSubType = "manage-item-sub-type";

        /// <summary>
        /// The delete item type
        /// </summary>
        public const string DeleteItemSubType = "remove-item-sub-type";

        /// <summary>
        /// The get item types
        /// </summary>
        public const string GetItemSubTypes = "get-item-sub-types";

        #endregion

        #region "Stores List"

        /// <summary>
        /// The stores
        /// </summary>
        public const string Stores = "stores";

        /// <summary>
        /// The get stores
        /// </summary>
        public const string GetStores = "get-stores";

        /// <summary>
        /// The get stores by identifier
        /// </summary>
        public const string GetStoresByID = "get-stores-by-id";

        /// <summary>
        /// The manage stores
        /// </summary>
        public const string ManageStores = "manage-stores";

        /// <summary>
        /// The delete stores
        /// </summary>
        public const string DeleteStores = "remove-stores";

        /// <summary>
        /// The get stores list
        /// </summary>
        public const string GetStoresList = "get-stores-list";

        /// <summary>
        /// The get stores list by l2 identifier
        /// </summary>
        public const string GetStoresListByL2ID = "get-stores-list-by-l2-id";

        #endregion

        #region "Item Section"

        /// <summary>
        /// The item section
        /// </summary>
        public const string ItemSection = "item-section";

        /// <summary>
        /// The get item section
        /// </summary>
        public const string GetItemSection = "get-item-section";

        /// <summary>
        /// The get item section by identifier
        /// </summary>
        public const string GetItemSectionByID = "get-item-section-by-id";

        /// <summary>
        /// The manage item section
        /// </summary>
        public const string ManageItemSection = "manage-item-section";

        /// <summary>
        /// The delete item section
        /// </summary>
        public const string DeleteItemSection = "remove-section-type";

        #endregion

        #endregion

        #region "Job Management Setups"

        #region "Job Priority"

        /// <summary>
        /// The job priority
        /// </summary>
        public const string JobPriority = "job-priority";

        /// <summary>
        /// The get job priority
        /// </summary>
        public const string GetJobPriority = "get-job-priority";

        /// <summary>
        /// The get job priority by identifier
        /// </summary>
        public const string GetJobPriorityByID = "get-job-priority-by-id";

        /// <summary>
        /// The manage job priority
        /// </summary>
        public const string ManageJobPriority = "manage-job-priority";

        /// <summary>
        /// The delete job priority
        /// </summary>
        public const string DeleteJobPriority = "remove-job-priority";

        #endregion

        #region "Job Status"

        /// <summary>
        /// The job status
        /// </summary>
        public const string JobStatus = "job-status";

        /// <summary>
        /// The get job status
        /// </summary>
        public const string GetJobStatus = "get-job-status";

        /// <summary>
        /// The get job status
        /// </summary>
        public const string GetAllJobStatusForDDL = "get-all-job-status";

        public const string GetAllJobStatusForDDLForJO = "get-all-job-status-jo";

        public const string GetAllJobStatusForDDLForJR = "get-all-job-status-jr";

        /// <summary>
        /// The get job status by identifier
        /// </summary>
        public const string GetJobStatusByID = "get-job-status-by-id";

        /// <summary>
        /// The manage job status
        /// </summary>
        public const string ManageJobStatus = "manage-job-status";

        /// <summary>
        /// The delete job status
        /// </summary>
        public const string DeleteJobStatus = "remove-job-status";

        #endregion

        #region "Job Type"

        /// <summary>
        /// The job type
        /// </summary>
        public const string JobType = "job-type";

        /// <summary>
        /// The get job type
        /// </summary>
        public const string GetJobType = "get-job-type";

        /// <summary>
        /// The get job type by identifier
        /// </summary>
        public const string GetJobTypeByID = "get-job-type-by-id";

        /// <summary>
        /// The manage job type
        /// </summary>
        public const string ManageJobType = "manage-job-type";

        /// <summary>
        /// The delete job type
        /// </summary>
        public const string DeleteJobType = "remove-job-type";

        /// <summary>
        /// To Get maintenance divisions by employee id
        /// </summary>
        public const string GetMaintDivByEmpIdOrProjectNumber = "get-maint-div-by-empid-projectNo";
        #endregion

        #region "Job Trade"

        /// <summary>
        /// The job trade
        /// </summary>
        public const string JobTrade = "job-trade";

        /// <summary>
        /// The get job trade
        /// </summary>
        public const string GetJobTrade = "get-job-trade";

        /// <summary>
        /// The get job trade by identifier
        /// </summary>
        public const string GetJobTradeByID = "get-job-trade-by-id";

        /// <summary>
        /// The manage job trade
        /// </summary>
        public const string ManageJobTrade = "manage-job-trade";

        /// <summary>
        /// The delete job trade
        /// </summary>
        public const string DeleteJobTrade = "remove-job-trade";

        #endregion

        #endregion

        #region "Item Requisition"

        #region "IR Req Status"

        /// <summary>
        /// The item requisition request status
        /// </summary>
        public const string IRReqStatus = "ir-req-status";

        /// <summary>
        /// The get item requisition request status
        /// </summary>
        public const string GetIRReqStatus = "get-ir-req-status";

        /// <summary>
        /// The get item requisition request status by identifier
        /// </summary>
        public const string GetIRReqStatusByID = "get-ir-req-status-by-id";

        /// <summary>
        /// The manage item requisition request status
        /// </summary>
        public const string ManageIRReqStatus = "manage-ir-req-status";

        /// <summary>
        /// The delete item requisition request status
        /// </summary>
        public const string DeleteIRReqStatus = "remove-ir-req-status";

        #endregion

        #region "IR Status"

        /// <summary>
        /// The item requisition status
        /// </summary>
        public const string IRStatus = "ir-status";

        /// <summary>
        /// The get item requisition status
        /// </summary>
        public const string GetIRStatus = "get-ir-status";

        /// <summary>
        /// The get item requisition status by identifier
        /// </summary>
        public const string GetIRStatusByID = "get-ir-status-by-id";

        /// <summary>
        /// The manage item requisition status
        /// </summary>
        public const string ManageIRStatus = "manage-ir-status";

        /// <summary>
        /// The delete item requisition status
        /// </summary> 
        public const string DeleteIRStatus = "remove-ir-status";

        /// <summary>
        /// The get item requisition all status
        /// </summary>
        public const string GetIRAllStatus = "get-ir-all-status";

        #endregion

        #region "IR Approval Levels"

        /// <summary>
        /// The item requisition approval levels
        /// </summary>
        public const string IRApprovalLevels = "ir-approval-levels";

        /// <summary>
        /// The get item requisition approval levels
        /// </summary>
        public const string GetIRApprovalLevels = "get-ir-approval-levels";

        /// <summary>
        /// The get item requisition approval levels by identifier
        /// </summary>
        public const string GetIRApprovalLevelsByID = "get-ir-approval-levels-by-id";

        /// <summary>
        /// The manage item requisition approval levels
        /// </summary>
        public const string ManageIRApprovalLevels = "manage-ir-approval-levels";

        /// <summary>
        /// The delete item requisition approval levels
        /// </summary>
        public const string DeleteIRApprovalLevels = "remove-ir-approval-levels";

        /// <summary>
        /// The get item requisition approval all levels
        /// </summary>
        public const string GetIRApprovalAllLevels = "get-ir-approval-all-levels";

        #endregion

        #region "IR WorkFlow"

        /// <summary>
        /// The item requisition workflow
        /// </summary>
        public const string IRWorkFlow = "ir-workflow";

        /// <summary>
        /// The get item requisition workflow
        /// </summary>
        public const string GetIRWorkFlow = "get-ir-workflow";

        /// <summary>
        /// The get item requisition workflow by identifier
        /// </summary>
        public const string GetIRWorkFlowByID = "get-ir-workflow-by-id";

        /// <summary>
        /// The manage item requisition workflow
        /// </summary>
        public const string ManageIRWorkFlow = "manage-ir-workflow";

        /// <summary>
        /// The delete item requisition workflow
        /// </summary>
        public const string DeleteIRWorkFlow = "remove-ir-workflow";

        /// <summary>
        /// The _ edit item requisition work flow
        /// </summary>
        public const string _EditIRWorkFlow = "_edit-ir-workflow";

        /// <summary>
        /// The get employee item requisition work flow
        /// </summary>
        public const string GetEmployeeIRWorkFlow = "get-employee-ir-workflow";

        /// <summary>
        /// The manage employee item requisition  work flow
        /// </summary>
        public const string ManageEmployeeIRWorkFlow = "manage-employee-ir-workflow";

        /// <summary>
        /// The delete employee item requisition  work flow
        /// </summary>
        public const string DeleteEmployeeIRWorkFlow = "remove-employee-ir-workflow";

        #endregion

        #endregion

        #region "Purchase Requisition"

        #region "PR Status"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string PRStatusList = "pr-status-list";

        /// <summary>
        /// The get PR Status list
        /// </summary>
        public const string GetPRStatusList = "get-pr-status-list";

        /// <summary>
        /// The get PR Status list by identifier
        /// </summary>
        public const string GetPRStatusListByID = "get-pr-status-list-by-id";

        /// <summary>
        /// The manage PR Status
        /// </summary>
        public const string ManagePRStatus = "manage-pr-status";

        /// <summary>
        /// The delete PRStatus
        /// </summary>
        public const string DeletePRStatus = "remove-pr-status";

        /// <summary>
        /// The get PRStatus
        /// </summary>
        public const string GetPRStatus = "get-pr-status";

        #endregion

        #region "PR Authorisation Status"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string PRAuthStatusList = "pr-auth-status-list";

        /// <summary>
        /// The get PR Authorization Status list
        /// </summary>
        public const string GetPRAuthStatusList = "get-pr-auth-status-list";

        /// <summary>
        /// The get PR Authorization Status list by identifier
        /// </summary>
        public const string GetPRAuthStatusListByID = "get-pr-auth-status-list-by-id";

        /// <summary>
        /// The manage PR Authorization Status
        /// </summary>
        public const string ManagePRAuthStatus = "manage-pr-auth-status";

        /// <summary>
        /// The delete PR Authorization Status
        /// </summary>
        public const string DeletePRAuthStatus = "remove-pr-auth-status";

        /// <summary>
        /// The get PR Authorization Status
        /// </summary>
        public const string GetPRAuthStatus = "get-pr-auth-status";

        #endregion

        #region "PR Approval Levels"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string PRApprovalLevelList = "pr-approval-level-list";

        /// <summary>
        /// The get PR Approval Levels list
        /// </summary>
        public const string GetPRApprovalLevelList = "get-pr-approval-level-list";

        /// <summary>
        /// The get PR Approval Level list by identifier
        /// </summary>
        public const string GetPRApprovalLevelListByID = "get-pr-approval-level-list-by-id";

        /// <summary>
        /// The manage PR Approval Level
        /// </summary>
        public const string ManagePRApprovalLevel = "manage-pr-approval-level";

        /// <summary>
        /// The delete PR Approval Level
        /// </summary>
        public const string DeletePRApprovalLevel = "remove-pr-approval-level";

        /// <summary>
        /// The get PR Approval Level
        /// </summary>
        public const string GetPRApprovalLevel = "get-pr-approval-level";

        #endregion

        #region "PR WorkFlow"

        /// <summary>
        /// The purchase requisition workflow
        /// </summary>
        public const string PRWorkFlow = "pr-workflow";

        /// <summary>
        /// The get purchase requisition workflow
        /// </summary>
        public const string GetPRWorkFlow = "get-pr-workflow";

        /// <summary>
        /// The get purchase requisition workflow by identifier
        /// </summary>
        public const string GetPRWorkFlowByID = "get-pr-workflow-by-id";

        /// <summary>
        /// The manage purchase requisition workflow
        /// </summary>
        public const string ManagePRWorkFlow = "manage-pr-workflow";

        /// <summary>
        /// The delete purchase requisition workflow
        /// </summary>
        public const string DeletePRWorkFlow = "remove-pr-workflow";

        /// <summary>
        /// The _ edit purchase requisition work flow
        /// </summary>
        public const string _EditPRWorkFlow = "_edit-pr-workflow";

        /// <summary>
        /// The get employee purchase requisition work flow
        /// </summary>
        public const string GetEmployeePRWorkFlow = "get-employee-pr-workflow";

        #endregion

        #region "Purchase Order Status"

        public const string GetOrderStatusList = "get-order-status-list";
        public const string GetDeliveryStatusList = "get-delivery-status-list";

        #endregion

        #endregion

        #region "BOM"

        /// <summary>
        /// The bo m list
        /// </summary>
        public const string BoMList = "bom-list";

        /// <summary>
        /// The get bo m list
        /// </summary>
        public const string GetBoMList = "get-bom-list";

        /// <summary>
        /// The get bo m list by identifier
        /// </summary>
        public const string GetBoMListByID = "get-bom-list-by-id";

        /// <summary>
        /// The manage bo m
        /// </summary>
        public const string ManageBoMList = "manage-bom-list";

        /// <summary>
        /// The delete bo m
        /// </summary>
        public const string DeleteBoMList = "remove-bom-list";

        /// <summary>
        /// The get bo m items list
        /// </summary>
        public const string GetBoMItemsList = "get-bom-iteams-list";

        /// <summary>
        /// The _ edit bo m list
        /// </summary>
        public const string _EditBoMList = "_edit-bom-list";

        /// <summary>
        /// The get stock code item
        /// </summary>
        public const string GetStockCodeItem = "get-stock-code-item";

        /// <summary>
        /// The manage bo m list item
        /// </summary>
        public const string ManageBoMListItem = "manage-bom-list-item";

        /// <summary>
        /// The delete bo m list item
        /// </summary>
        public const string DeleteBoMListItem = "remove-bom-list-item";
        #endregion

        #region "Cleaning Setup"

        #region "Cleaning Group"

        /// <summary>
        /// The cleaning group
        /// </summary>
        public const string CleaningGroup = "cleaning-group";

        /// <summary>
        /// The get cleaning group
        /// </summary>
        public const string GetCleaningGroup = "get-cleaning-group";

        /// <summary>
        /// The get cleaning group by identifier
        /// </summary>
        public const string GetCleaningGroupByID = "get-cleaning-group-by-id";

        /// <summary>
        /// The manage cleaning group
        /// </summary>
        public const string ManageCleaningGroup = "manage-cleaning-group";

        /// <summary>
        /// The delete cleaning group
        /// </summary>
        public const string DeleteCleaningGroup = "remove-cleaning-group";

        #endregion

        #region "Cleaning Classification"

        /// <summary>
        /// The cleaning classification
        /// </summary>
        public const string CleaningClassification = "cleaning-classification";

        /// <summary>
        /// The get cleaning classification
        /// </summary>
        public const string GetCleaningClassification = "get-cleaning-classification";

        /// <summary>
        /// The get cleaning classification by identifier
        /// </summary>
        public const string GetCleaningClassificationByID = "get-cleaning-classification-by-id";

        /// <summary>
        /// The manage cleaning classification
        /// </summary>
        public const string ManageCleaningClassification = "manage-cleaning-classification";

        /// <summary>
        /// The delete cleaning classification
        /// </summary>
        public const string DeleteCleaningClassification = "remove-cleaning-classification";

        /// <summary>
        /// The get all cleaning classification
        /// </summary>
        public const string GetAllCleaningClassification = "get-all-cleaning-classification";
        #endregion

        #region "Cleaning Element"

        /// <summary>
        /// The get cleaning elements
        /// </summary>
        public const string GetCleaningElements = "get-cleaning-elements";

        #endregion

        #region "Cleaning Weightage"

        /// <summary>
        /// The cleaning weightage
        /// </summary>
        public const string CleaningWeightage = "cleaning-weightage";

        /// <summary>
        /// The get cleaning weightage by site identifier
        /// </summary>
        public const string GetCleaningWeightageByL2ID = "cleaning-weightage-by-l2-id";

        /// <summary>
        /// The manage cleaning weightage
        /// </summary>
        public const string ManageCleaningWeightage = "manage-cleaning-weightage";
        #endregion

        #endregion

        #region "Job Plan"

        /// <summary>
        /// The job plan
        /// </summary>
        public const string JobPlan = "job-plan";

        /// <summary>
        /// The get job plan
        /// </summary>
        public const string GetJobPlan = "get-job-plan";

        /// <summary>
        /// The get job plan by identifier
        /// </summary>
        public const string GetJobPlanByID = "get-job-plan-by-id";

        /// <summary>
        /// The manage job plan
        /// </summary>
        public const string ManageJobPlan = "manage-job-plan";

        /// <summary>
        /// The delete job plan
        /// </summary>
        public const string DeleteJobPlan = "remove-job-plan";

        /// <summary>
        /// The _ edit job plan
        /// </summary>
        public const string _EditJobPlan = "_edit-job-plan";

        /// <summary>
        /// The get job plan item
        /// </summary>
        public const string GetJobPlanItem = "get-job-plan-item";

        /// <summary>
        /// The manage job plan item
        /// </summary>
        public const string ManageJobPlanItem = "manage-job-plan-item";

        /// <summary>
        /// The delete job plan item
        /// </summary>
        public const string DeleteJobPlanItem = "remove-job-plan-item";

        #endregion

        #region "Safety Instruction"

        /// <summary>
        /// The safety instruction
        /// </summary>
        public const string SafetyInstruction = "safety-instruction";

        /// <summary>
        /// The get safety instruction
        /// </summary>
        public const string GetSafetyInstruction = "get-safety-instruction";

        /// <summary>
        /// The get safety instruction Without filter
        /// </summary>
        public const string GetSafetyInstructionAll = "get-safety-instruction-all";

        /// <summary>
        /// The get safety instruction by identifier
        /// </summary>
        public const string GetSafetyInstructionByID = "get-safety-instruction-by-id";

        /// <summary>
        /// The manage safety instruction
        /// </summary>
        public const string ManageSafetyInstruction = "manage-safety-instruction";

        /// <summary>
        /// The delete safety instruction
        /// </summary>
        public const string DeleteSafetyInstruction = "remove-safety-instruction";

        /// <summary>
        /// The _ edit safety instruction
        /// </summary>
        public const string _EditSafetyInstruction = "_edit-safety-instruction";

        /// <summary>
        /// The get safety instruction item
        /// </summary>
        public const string GetSafetyInstructionItem = "get-safety-instruction-item";

        /// <summary>
        /// The manage safety instruction item
        /// </summary>
        public const string ManageSafetyInstructionItem = "manage-safety-instruction-item";

        /// <summary>
        /// The delete safety instruction item
        /// </summary>
        public const string DeleteSafetyInstructionItem = "remove-safety-instruction-item";

        #endregion

        #region  "Email Configurations"
        /// <summary>
        /// The email server configuration
        /// </summary>
        public const string EmailServerConfiguration = "email-server-configuration";

        /// <summary>
        /// The update email server configurations
        /// </summary>
        public const string UpdateEmailServerConfiguration = "update-email-server-configuration";

        /// <summary>
        /// The email templates
        /// </summary>
        public const string EmailTemplates = "email-templates";

        /// <summary>
        /// The get email templates
        /// </summary>
        public const string GetEmailTemplates = "get-email-templates";

        /// <summary>
        /// The manage email templates
        /// </summary>
        public const string ManageEmailTemplates = "manage-email-templates";

        /// <summary>
        /// The _ edit email templates
        /// </summary>
        public const string _EditEmailTemplates = "_edit-email-templates";

        /// <summary>
        /// The email rules
        /// </summary>
        public const string EmailRules = "email-rules";

        /// <summary>
        /// The delete email rules
        /// </summary>
        public const string DeleteEmailRules = "delete-email-rules";

        /// <summary>
        /// The manage email rules
        /// </summary>
        public const string ManageEmailRules = "manage-email-rules";

        /// <summary>
        /// The _ edit email rules
        /// </summary>
        public const string _EditEmailRules = "_edit-email-rules";

        /// <summary>
        /// The get email template fields
        /// </summary>
        public const string GetEmailTemplateFields = "get-email-template-fields";

        /// <summary>
        /// The get email rules
        /// </summary>
        public const string GetEmailRules = "get-email-rules";

        /// <summary>
        /// The get manage email rules
        /// </summary>
        public const string ManageEmailNotificationRules = "manage-email-notification-rules";

        /// <summary>
        /// The get email rules for L2
        /// </summary>
        public const string GetEmailRulesL2 = "get-email-rules-l2";

        /// <summary>
        /// The get email rules L5
        /// </summary>
        public const string GetEmailRulesL5 = "get-email-rules-l5";

        /// <summary>
        /// The get email rules Location
        /// </summary>
        public const string GetEmailRulesLocation = "get-email-rules-location";

        public const string GetEmailRulesLocationType = "get-email-rules-location-type";

        public const string GetEmailRulesSubCategory = "get-email-rules-sub-category";

        public const string GetEmailRulesCriticality = "get-email-rules-criticality";

        /// <summary>
        /// The get email rules assets
        /// </summary>
        public const string GetEmailRulesAssets = "get-email-rules-assets";

        /// <summary>
        /// The get email rules employees
        /// </summary>
        public const string GetEmailRulesEmployees = "get-email-rules-employees";

        public const string GetEmailRulesSupplier = "get-email-rules-supplier";

        /// <summary>
        /// The get email rules escalation
        /// </summary>
        public const string GetEmailRulesEscalation = "get-email-rules-escalation";

        /// <summary>
        /// The get email rules events
        /// </summary>
        public const string GetEmailRulesEvents = "get-email-rules-events";

        /// <summary>
        /// The get email rules Maintain sub department
        /// </summary>
        public const string GetEmailRulesMaintsubdept = "get-email-rules-events-maintsubdept";

        /// <summary>
        /// The get email rules Modules
        /// </summary>
        public const string GetEmailRulesModules = "get-email-rules-events-modules";

        /// <summary>
        /// The get email rules Processed rule
        /// </summary>
        public const string GetEmailRulesProcessedrule = "get-email-rules-events-processedrule";

        /// <summary>
        /// The get email rules work priority
        /// </summary>
        public const string GetEmailRulesWorkpriority = "get-email-rules-events-workpriority";

        /// <summary>
        /// The get email rules work type
        /// </summary>
        public const string GetEmailRulesWorkType = "get-email-rules-events-worktype";

        /// <summary>
        /// The get email rules work status
        /// </summary>
        public const string GetEmailRulesWorkStatus = "get-email-rules-events-workstatus";

        /// <summary>
        /// The get email rules work trade
        /// </summary>
        public const string GetEmailRulesWorkTrade = "get-email-rules-events-worktrade";

        /// <summary>
        /// The get details for all building
        /// </summary>
        public const string GetBuildingDetailsAll = "get-building-details-all";

        /// <summary>
        /// The get details for all locations
        /// </summary>
        public const string GetLocationDetailsAll = "get-location-details-all";

        /// <summary>
        /// The get details for all assets
        /// </summary>
        public const string GetAssetsDetailsAll = "get-assets-details-all";

        public const string GetLocationTypeAll = "get-location-type-all";

        public const string GetCriticalityAll = "get-criticality-all";

        public const string GetSubCategoryAll = "get-sub-category-all";
        #endregion

        #region "SMS Configurations"
        /// <summary>
        /// The short messages service server configuration
        /// </summary>
        public const string SMSServerConfiguration = "sms-server-configuration";

        /// <summary>
        /// The update short messages service server configurations
        /// </summary>
        public const string UpdateSMSServerConfiguration = "sms-email-server-configuration";
        #endregion

        #region "Account Code"

        /// <summary>
        /// The account code
        /// </summary>
        public const string AccountCode = "account-code";

        /// <summary>
        /// The get account code
        /// </summary>
        public const string GetAccountCode = "get-account-code";

        /// <summary>
        /// The get account code for pop up
        /// </summary>
        public const string GetAccountCodeForPopUp = "get-account-code-for-popup";

        /// <summary>
        /// The get all account code
        /// </summary>
        public const string GetAllAccountCodesForDDL = "get-all-account-code";

        /// <summary>
        /// The get account code by identifier
        /// </summary>
        public const string GetAccountCodeByID = "get-account-code-by-id";

        /// <summary>
        /// The manage account code
        /// </summary>
        public const string ManageAccountCode = "manage-account-code";

        /// <summary>
        /// The delete account code
        /// </summary>
        public const string DeleteAccountCode = "remove-account-code";

        /// <summary>
        /// The _ edit account code
        /// </summary>
        public const string _EditAccountCode = "_edit-account-code";

        #endregion

        #region "Purchasing Setups"

        #region "Purchasing Currency"

        /// <summary>
        /// The purchasing currency
        /// </summary>
        public const string PurchasingCurrency = "purchasing-currency";

        /// <summary>
        /// The get purchasing currency
        /// </summary>
        public const string GetPurchasingCurrency = "get-purchasing-currency";

        /// <summary>
        /// The get purchasing currency by identifier
        /// </summary>
        public const string GetPurchasingCurrencyByID = "get-purchasing-currency-by-id";

        /// <summary>
        /// The manage purchasing currency
        /// </summary>
        public const string ManagePurchasingCurrency = "manage-purchasing-currency";

        /// <summary>
        /// The delete purchasing currency
        /// </summary>
        public const string DeletePurchasingCurrency = "remove-purchasing-currency";

        /// <summary>
        /// The get currency for pop up
        /// </summary>
        public const string GetCurrencyForPopUp = "get-currency-for-popup";

        #endregion

        #region "Purchasing Term of Sale"

        /// <summary>
        /// The purchasing term of sale
        /// </summary>
        public const string PurchasingTermofSale = "purchasing-term-of-sale";

        /// <summary>
        /// The get purchasing term of sale
        /// </summary>
        public const string GetPurchasingTermofSale = "get-purchasing-term-of-sale";

        /// <summary>
        /// The get purchasing term of sale by identifier
        /// </summary>
        public const string GetPurchasingTermofSaleByID = "get-purchasing-term-of-sale-by-id";

        /// <summary>
        /// The manage purchasing term of sale
        /// </summary>
        public const string ManagePurchasingTermofSale = "manage-purchasing-term-of-sale";

        /// <summary>
        /// The delete purchasing term of sale
        /// </summary>
        public const string DeletePurchasingTermofSale = "remove-purchasing-term-of-sale";

        /// <summary>
        /// The get term of sales
        /// </summary>
        public const string GetTermofSales = "Get-Term-of-Sales";

        #endregion

        #region "Purchasing Shipped Via"

        /// <summary>
        /// The purchasing shipped via
        /// </summary>
        public const string PurchasingShippedVia = "purchasing-shipped-via";

        /// <summary>
        /// The get purchasing shipped via
        /// </summary>
        public const string GetPurchasingShippedVia = "get-purchasing-shipped-via";

        /// <summary>
        /// The get purchasing shipped via by identifier
        /// </summary>
        public const string GetPurchasingShippedViaByID = "get-purchasing-shipped-via-by-id";

        /// <summary>
        /// The manage purchasing shipped via
        /// </summary>
        public const string ManagePurchasingShippedVia = "manage-purchasing-shipped-via";

        /// <summary>
        /// The delete purchasing shipped via
        /// </summary>
        public const string DeletePurchasingShippedVia = "remove-purchasing-shipped-via";

        /// <summary>
        /// The get shipped via
        /// </summary>
        public const string GetShippedVias = "Get-Shipped-Vias";

        #endregion

        #region "Purchasing Payment Terms"

        /// <summary>
        /// The purchasing payment terms
        /// </summary>
        public const string PurchasingPaymentTerms = "purchasing-payment-terms";

        /// <summary>
        /// The get purchasing payment terms
        /// </summary>
        public const string GetPurchasingPaymentTerms = "get-purchasing-payment-terms";

        /// <summary>
        /// The get purchasing payment terms by identifier
        /// </summary>
        public const string GetPurchasingPaymentTermsByID = "get-purchasing-payment-terms-by-id";

        /// <summary>
        /// The manage purchasing payment terms
        /// </summary>
        public const string ManagePurchasingPaymentTerms = "manage-purchasing-payment-terms";

        /// <summary>
        /// The delete purchasing payment terms
        /// </summary>
        public const string DeletePurchasingPaymentTerms = "remove-purchasing-payment-terms";

        /// <summary>
        /// The get payment terms
        /// </summary>
        public const string GetPaymentTerms = "get-payment-terms";
        #endregion

        #region "Default Setups"

        /// <summary>
        /// The default setups
        /// </summary>
        public const string DefaultSetups = "default-setups";

        /// <summary>
        /// The get default setups
        /// </summary>
        public const string GetDefaultSetups = "get-default-setups";

        /// <summary>
        /// The manage default setups
        /// </summary>
        public const string ManageDefaultSetups = "manage-default-setups";

        #endregion

        #endregion

        #region "Tool Setups"

        #region "Tool Category"

        /// <summary>
        /// The tool category
        /// </summary>
        public const string ToolCategory = "tool-category";

        /// <summary>
        /// The get tool category
        /// </summary>
        public const string GetToolCategory = "get-tool-category";

        /// <summary>
        /// The get tool category by identifier
        /// </summary>
        public const string GetToolCategoryByID = "get-tool-category-by-id";

        /// <summary>
        /// The manage tool category
        /// </summary>
        public const string ManageToolCategory = "manage-tool-category";

        /// <summary>
        /// The delete tool category
        /// </summary>
        public const string DeleteToolCategory = "remove-tool-category";

        /// <summary>
        /// The get tool categories
        /// </summary>
        public const string GetToolCategories = "get-tool-categories";

        #endregion

        #region "Tool Sub Category"

        /// <summary>
        /// The tool sub category
        /// </summary>
        public const string ToolSubCategory = "tool-sub-category";

        /// <summary>
        /// The get tool sub category
        /// </summary>
        public const string GetToolSubCategory = "get-tool-sub-category";

        /// <summary>
        /// The get tool sub category by identifier
        /// </summary>
        public const string GetToolSubCategoryByID = "get-tool-sub-category-by-id";

        /// <summary>
        /// The manage tool sub category
        /// </summary>
        public const string ManageToolSubCategory = "manage-tool-sub-category";

        /// <summary>
        /// The delete tool sub category
        /// </summary>
        public const string DeleteToolSubCategory = "remove-tool-sub-category";

        /// <summary>
        /// The get tool sub categories
        /// </summary>
        public const string GetToolSubCategories = "get-tool-sub-categories";

        /// <summary>
        /// The get tool sub category by category identifier
        /// </summary>
        public const string GetToolSubCategoryByCategoryID = "get-tool-sub-category-by-categoryid";

        #endregion

        #region "Tool"

        /// <summary>
        /// The tool
        /// </summary>
        public const string Tool = "tool";

        /// <summary>
        /// The get tool
        /// </summary>
        public const string GetTool = "get-tool";

        /// <summary>
        /// The get tool by identifier
        /// </summary>
        public const string GetToolByID = "get-tool-by-id";

        /// <summary>
        /// The manage tool
        /// </summary>
        public const string ManageTool = "manage-tool";

        /// <summary>
        /// The delete tool
        /// </summary>
        public const string DeleteTool = "remove-tool";

        /// <summary>
        /// The get tools by cat and sub cat
        /// </summary>
        public const string GetToolsByCatAndSubCat = "get-tools-by-cat-subcat";

        #endregion

        #endregion

        #region "Specifications"

        /// <summary>
        /// The dashboard
        /// </summary>
        public const string SpecificationsList = "specifications-list";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetSpecificationList = "get-specification-list";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetSpecificationListByID = "get-specification-list-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageSpecification = "manage-Specification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteSpecification = "remove-Specification";

        /// <summary>
        /// The get all specification for DDL
        /// </summary>
        public const string GetAllSpecificationForDDL = "getallspecificationforddl";

        #endregion

        #region "Error Log"

        /// <summary>
        /// The error log
        /// </summary>
        public const string ErrorLog = "error-log";

        /// <summary>
        /// The get error log list
        /// </summary>
        public const string GetErrorLogList = "get-error-log-list";

        /// <summary>
        /// The delete error log
        /// </summary>
        public const string DeleteErrorLog = "remove-error-log";

        /// <summary>
        /// The _ error log detail
        /// </summary>
        public const string _ErrorLogDetail = "_error-log-detail";

        #endregion

        #region "Help Module"

        /// <summary>
        /// The help list
        /// </summary>
        public const string HelpList = "help-list";

        /// <summary>
        /// The get help modules list
        /// </summary>
        public const string GetHelpModulesList = "get-help-modules-list";

        /// <summary>
        /// The get help list
        /// </summary>
        public const string GetHelpList = "get-help-list";

        /// <summary>
        /// The help detail
        /// </summary>
        public const string HelpDetail = "help-detail";

        /// <summary>
        /// The get help page detail
        /// </summary>
        public const string GetHelpPageDetail = "get-help-detail";

        /// <summary>
        /// The get chapter
        /// </summary>
        public const string GetChapter = "get-chapter";

        /// <summary>
        /// The get chapter list
        /// </summary>
        public const string GetChapterList = "get-chapter-list";

        /// <summary>
        /// The manage chapter
        /// </summary>
        public const string ManageChapter = "manage-chapter";

        /// <summary>
        /// The delete chapter
        /// </summary>
        public const string DeleteChapter = "remove-chapter";

        /// <summary>
        /// The get language list
        /// </summary>
        public const string GetLanguageList = "get-language-list";

        /// <summary>
        /// The manage help detail
        /// </summary>
        public const string ManageHelpDetail = "manage-help-detail";

        /// <summary>
        /// The get help related topics
        /// </summary>
        public const string GetHelpRelatedTopics = "get-help-related-topics";

        /// <summary>
        /// The get help related topic list
        /// </summary>
        public const string GetHelpRelatedTopicList = "get-help-related-topic-list";

        /// <summary>
        /// The get related topic list
        /// </summary>
        public const string GetRelatedTopicList = "get-related-topic-list";

        /// <summary>
        /// The manage help related topics
        /// </summary>
        public const string ManageHelpRelatedTopics = "manage-help-related-topics";

        /// <summary>
        /// The delete help related topics
        /// </summary>
        public const string DeleteHelpRelatedTopics = "remove-help-related-topics";

        /// <summary>
        /// The get module access list
        /// </summary>
        public const string GetModuleAccessList = "get-module-access-list";

        /// <summary>
        /// The manage help module
        /// </summary>
        public const string ManageHelpModule = "manage-help-module";

        /// <summary>
        /// The get help module
        /// </summary>
        public const string GetHelpModules = "get-help-modules";

        /// <summary>
        /// The delete help modules
        /// </summary>
        public const string DeleteHelpModules = "remove-help-modules";

        #endregion

        #region "General Configurations"

        /// <summary>
        /// The general configuration
        /// </summary>
        public const string GeneralConfiguration = "general-configuration";

        /// <summary>
        /// The update general configuration
        /// </summary>
        public const string UpdateGeneralConfiguration = "update-general-configuration";

        #endregion

        #region "Holiday Setting"

        /// <summary>
        /// The holiday setting
        /// </summary>
        public const string HolidaySetting = "holiday-setting";

        /// <summary>
        /// The get working day list
        /// </summary>
        public const string GetWorkingDayList = "get-working-day-list";

        /// <summary>
        /// The get working day by identifier
        /// </summary>
        public const string GetWorkingDayByID = "get-working-day-by-id";

        /// <summary>
        /// The manage working day
        /// </summary>
        public const string ManageWorkingDay = "manage-working-day";

        /// <summary>
        /// The delete working day
        /// </summary>
        public const string DeleteWorkingDay = "remove-working-day";

        /// <summary>
        /// The get holiday list
        /// </summary>
        public const string GetHolidayList = "get-holiday-list";

        /// <summary>
        /// The get holiday by identifier
        /// </summary>
        public const string GetHolidayByID = "get-holiday-by-id";

        /// <summary>
        /// The manage holiday
        /// </summary>
        public const string ManageHoliday = "manage-holiday";

        /// <summary>
        /// The delete holiday
        /// </summary>
        public const string DeleteHoliday = "remove-holiday";

        /// <summary>
        /// The get working hour list
        /// </summary>
        public const string GetWorkingHourList = "get-working-hour-list";

        /// <summary>
        /// The get working hour by identifier
        /// </summary>
        public const string GetWorkingHourByID = "get-working-hour-by-id";

        /// <summary>
        /// The manage working hour
        /// </summary>
        public const string ManageWorkingHour = "manage-working-hour";

        /// <summary>
        /// The delete working hour
        /// </summary>
        public const string DeleteWorkingHour = "remove-working-hour";

        /// <summary>
        /// The _ edit working day hours
        /// </summary>
        public const string _EditWorkingDayHours = "_edit-working-day-hours";

        /// <summary>
        /// The get org code by city identifier
        /// </summary>
        public const string GetOrgCodeByCityID = "get-org-code-by-city-id";

        /// <summary>
        /// The _ edit holiday
        /// </summary>
        public const string _EditHoliday = "_edit-holiday";

        #endregion

        #region AssetCategoryNotification

        public const string AssetCategoryNotificationList = "asset-category-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetAssetCategoryNotificationList = "get-asset-category-notification";

          /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetAssetCategoryNotificationEmployeeList = "get-asset-category-notification-employee";

        public const string SaveNotificationEmployee = "save-notification-employee";

        public const string InActiveNotification = "inactive-notification";
        public const string GetNotificationList = "get-notification-list";
        public const string GetNotificationListTop5 = "get-notification-list-top5";
        public const string GetUnreadNotificationListCount = "get-unread-notification-list-count";
        public const string GetTotalNotificationListCount = "get-total-notification-list-count";
        public const string NotificationList = "notification-list";
        public const string MarkAsReadNotification = "mark-as-read-notification";

        public const string NotificationTypeList = "notification-type-list";
        

        

        public const string DeleteNotificationEmployee = "remove-notification-employee";
        
        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetAssetCategoryNotificationByID = "get-asset-category-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageAssetCategoryNotification = "manage-asset-category-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteAssetCategoryNotification = "remove-asset-category-notification";

        #endregion

        #region CriticalityNotification

        public const string CriticalityNotificationList = "criticality-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetCriticalityNotificationList = "get-criticality-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetCriticalityNotificationByID = "get-criticality-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageCriticalityNotification = "manage-criticality-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteCriticalityNotification = "remove-criticality-notification";

        #endregion

        #region JOStatusNotification

        public const string JOStatusNotificationList = "jostatus-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetJOStatusNotificationList = "get-jostatus-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetJOStatusNotificationByID = "get-jostatus-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageJOStatusNotification = "manage-jostatus-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteJOStatusNotification = "remove-jostatus-notification";

        #endregion

        #region JRStatusNotification

        public const string JRStatusNotificationList = "jrstatus-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetJRStatusNotificationList = "get-jrstatus-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetJRStatusNotificationByID = "get-jrstatus-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageJRStatusNotification = "manage-jrstatus-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteJRStatusNotification = "remove-jrstatus-notification";

        #endregion

        #region LocationTypeNotification

        public const string LocationTypeNotificationList = "location-type-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetLocationTypeNotificationList = "get-location-type-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetLocationTypeNotificationByID = "get-location-type-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageLocationTypeNotification = "manage-location-type-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteLocationTypeNotification = "remove-location-type-notification";

        #endregion

        #region WorkPriorityNotification

        public const string WorkPriorityNotificationList = "work-priority-notification";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetWorkPriorityNotificationList = "get-work-priority-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetWorkPriorityNotificationByID = "get-work-priority-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageWorkPriorityNotification = "manage-work-priority-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteWorkPriorityNotification = "remove-work-priority-notification";

        #endregion

        #region WorkTypeNotification

        public const string WorkTypeNotificationList = "work-type-notification";

        public const string NotificationTypesList = "notification-types-list";

        public const string GetNotificationTypesList = "get-notification-types-list";

        public const string GetNotificationTypesListById = "get-notification-types-list-by-id";

        public const string ManageNotificationTypes = "manage-notification-types";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetWorkTypeNotificationList = "get-work-type-notification";

        /// <summary>
        /// The get failure cause list by identifier
        /// </summary>
        public const string GetWorkTypeNotificationByID = "get-work-type-notification-by-id";

        /// <summary>
        /// The manage sector
        /// </summary>
        public const string ManageWorkTypeNotification = "manage-work-type-notification";

        /// <summary>
        /// The delete sector
        /// </summary>
        public const string DeleteWorkTypeNotification = "remove-work-type-notification";

        #endregion

        #endregion

        #region "Management"

        #region "User List"

        /// <summary>
        /// The UserList
        /// </summary>
        public const string UserList = "user-list";

        /// <summary>
        /// The GetUserList
        /// </summary>
        public const string GetUserList = "get-user-list";

        /// <summary>
        /// The GetEmployeeListByID
        /// </summary>
        public const string GetEmployeeListByID = "get-employee-by-id";

        /// <summary>
        /// The Employee
        /// </summary>
        public const string GetAllCities = "get-all-city";

        /// <summary>
        /// The get-maintenance-sub-department-by-department-id
        /// </summary>
        public const string GetMaintSubDeptByDeptID = "get-maint-sub-dept-by-dept-id";

        /// <summary>
        /// The GetEmployeeByID
        /// </summary>
        public const string GetEmployeeByID = "employee";

        /// <summary>
        /// To Save Employee
        /// </summary>
        public const string SaveEmployee = "employee";

        /// <summary>
        /// To Save Employee
        /// </summary>
        public const string SaveEmployeeLogin = "save-employee-login";

        /// <summary>
        /// To Save Employee Documents
        /// </summary>
        public const string SaveEmployeeDocuments = "save-employee-documents";

        /// <summary>
        /// To Copy User permission to Employee
        /// </summary>
        public const string CopyUserPermissionToEmployee = "copy-group-pemission-to-employee";

        /// <summary>
        /// To Delete Employee
        /// </summary>
        public const string DeleteEmployee = "delete-employee";

        /// <summary>
        /// To Delete Employee
        /// </summary>
        public const string GetDocumentsByEmployee = "get-documents-by-employee";

        /// <summary>
        /// To Delete Attachment
        /// </summary>
        public const string DeleteAttachment = "delete-attachment-by-id";

        /// <summary>
        /// To Download Attachment
        /// </summary>
        public const string DownloadAttachment = "download-attachment";

        /// <summary>
        /// To Download Attachment
        /// </summary>
        public const string DownloadEmpAttachment = "download-employee-attachment";

        /// <summary>
        /// To Delete Employee Document
        /// </summary>
        public const string DeleteEmpDocument = "delete-employee-document";

        /// <summary>
        /// To Get Employee Assigns
        /// </summary>
        public const string GetEmployeeAssignPage = "get-employee-assign-page";

        /// <summary>
        /// To Change Employee Photo
        /// </summary>
        public const string ChangeEmployeePhoto = "changeemployeephoto";

        /// <summary>
        /// The GetEmployeeListByL2ID
        /// </summary>
        public const string GetEmployeePage = "get-employee-page";

        /// <summary>
        /// The get employees
        /// </summary>
        public const string GetEmployees = "GetEmployees";

        /// <summary>
        /// Gets the City Permission 
        /// </summary>
        public const string GetCityPermissions = "get-city-permission-list";

        /// <summary>
        /// Gets the Location Permission 
        /// </summary>
        public const string GetLocationPermissions = "get-location-permission-list";

        /// <summary>
        /// To save permissions by city
        /// </summary>
        public const string SaveL2Permissions = "save-l2-permissions";

        /// <summary>
        /// To save permissions by location
        /// </summary>
        public const string SaveLocationPermissions = "save-location-permissions";

        /// <summary>
        /// To save permissions by Sub Department
        /// </summary>
        public const string SaveSubDeptPermissions = "save-subdept-permissions";

        /// <summary>
        /// To save permissions level wise
        /// </summary>
        public const string SaveLevelWisePermissions = "save-levelwise-permissions";

        #endregion

        #region "Active Session"

        /// <summary>
        /// The active session
        /// </summary>
        public const string ActiveSession = "active-session";

        /// <summary>
        /// The get active session
        /// </summary>
        public const string GetActiveSession = "get-active-session";

        /// <summary>
        /// The delete active session
        /// </summary>
        public const string DeleteActiveSession = "reomve-active-session";

        #endregion

        #region "Login History"

        /// <summary>
        /// The login history
        /// </summary>
        public const string LoginHistory = "login-history";

        /// <summary>
        /// The get login history
        /// </summary>
        public const string GetLoginHistory = "get-login-history";

        /// <summary>
        /// The delete login history
        /// </summary>
        public const string DeleteLoginHistory = "reomve-login-history";

        #endregion

        #region "User Group"

        /// <summary>
        /// gets user group by id
        /// </summary>
        public const string GetUserGroupById = "get-user-group-by-id";

        /// <summary>
        /// The delete user group
        /// </summary>
        public const string DeleteUserGroupById = "delete-user-group";

        /// <summary>
        /// To Save User Group
        /// </summary>
        public const string SaveUserGroup = "save-user-group";

        /// <summary>
        /// To List Out All user Groups With Paging
        /// </summary>
        public const string UserGroupList = "user-group-list";

        /// <summary>
        /// To Get Out All user Groups With Paging
        /// </summary>
        public const string GetUserGroupList = "get-user-group-list";
        #endregion

        #region "Permissions"

        /// <summary>
        /// To get permissions
        /// </summary>
        public const string _GetPermissions = "get-permissions";

        /// <summary>
        /// To get all permissions
        /// </summary>
        public const string Permissions = "permissions";

        /// <summary>
        /// To get permissions
        /// </summary>
        public const string GetPermissionsByEmpAndGroup = "get-permissions-by-emp-group";

        /// <summary>
        /// To save permissions
        /// </summary>
        public const string SavePermissions = "save-permissions";
        #endregion

        #region "Language Translation"

        /// <summary>
        /// The language translation
        /// </summary>
        public const string LanguageTranslation = "language-translation";

        /// <summary>
        /// The get language translation
        /// </summary>
        public const string GetLanguageTranslation = "get-language-translation";

        /// <summary>
        /// The get language translation by identifier
        /// </summary>
        public const string GetLanguageTranslationByID = "get-language-translation-by-id";

        /// <summary>
        /// The manage language translation
        /// </summary>
        public const string ManageLanguageTranslation = "manage-language-translation";

        /// <summary>
        /// The delete language translation
        /// </summary>
        public const string DeleteLanguageTranslation = "reomve-language-translation";

        /// <summary>
        /// The _ un translated word list
        /// </summary>
        public const string _UnTranslatedWordList = "_un-translated-word-list";

        /// <summary>
        /// The get language code
        /// </summary>
        public const string GetLanguageCode = "get-language-code";

        /// <summary>
        /// The get un translated word list by code
        /// </summary>
        public const string GetUnTranslatedWordListByCode = "get-un-translated-word-list-by-code";

        /// <summary>
        /// The new translations
        /// </summary>
        public const string NewTranslations = "NewTranslations";

        #endregion

        #region "Database Audit"

        /// <summary>
        /// Database Audit Trail
        /// </summary>
        public const string DatabaseAuditTrail = "databaseaudittrail";

        /// <summary>
        /// Database Audit Trail table details
        /// </summary>
        public const string GetDatabaseAuditTableDetails = "GetDatabaseAuditTableDetails";

        /// <summary>
        /// Get Property Name
        /// </summary>
        public const string GetPropertyName = "GetPropertyName";

        /// <summary>
        /// Delete CDC Tables All Records
        /// </summary>
        public const string DeleteAuditTablesAllRecords = "DeleteAuditTablesAllRecords";

        #endregion

        #endregion

        #region "Transaction"       
        
        #region "Asset"

        /// <summary>
        /// The asset list
        /// </summary>
        public const string AssetList = "asset-list";

        /// <summary>
        /// The get asset list
        /// </summary>
        public const string GetAssetList = "get-asset-list";

        /// <summary>
        /// The get asset list by identifier
        /// </summary>
        public const string GetAssetByID = "asset-detail";

        /// <summary>
        /// The get asset detail by asset identifier
        /// </summary>
        public const string GetAssetDetailByAssetID = "GetAssetDetailByAssetID";

        /// <summary>
        /// The manage asset
        /// </summary>
        public const string ManageAsset = "asset-detail";

        /// <summary>
        /// The delete asset
        /// </summary>
        public const string DeleteAsset = "remove-asset";

        /// <summary>
        /// The _ get Assets by location identifier
        /// </summary>
        public const string GetAssetsByLocationID = "get-assets-by-locationid";

        #region "Relation Tab"
        /// <summary>
        /// The _ get relation ship
        /// </summary>
        public const string _GetRelationShip = "_get-relationship";

        /// <summary>
        /// The get assets for location
        /// </summary>
        public const string GetAssetsByLocation = "get-assets-by-location";

        /// <summary>
        /// The get asset childs
        /// </summary>
        public const string GetAssetChilds = "get-asset-childs";

        /// <summary>
        /// The delete child asset
        /// </summary>
        public const string DeleteChildAsset = "delete-child-asset";

        /// <summary>
        /// The add child asset
        /// </summary>
        public const string AddChildAsset = "add-child-asset";

        /// <summary>
        /// The get assets for location for job request
        /// </summary>
        public const string GetAssetsByLocationForJobRequest = "get-assets-by-location-job-request";
        #endregion

        #region "Job Order History"

        /// <summary>
        /// The _ get job order history
        /// </summary>
        public const string _GetJobOrderHistory = "_get-job-order-history";

        /// <summary>
        /// The _ get job order by asset identifier
        /// </summary>
        public const string GetJobOrderByAssetID = "get-job-order-by-assetID";

        /// <summary>
        /// The _ get job order by location identifier
        /// </summary>
        public const string GetJobOrderByLocationID = "get-job-order-by-locationid";

        /// <summary>
        /// The get job order detail
        /// </summary>
        public const string GetJobOrderDetail = "get-job-order-detail";

        /// <summary>
        /// The get job order Count
        /// </summary>
        public const string GetJobOrderCountByLocationAndAseetID = "get-job-order-count-by-asset-and-locationid";
        #endregion

        #region "PM Meter"
        /// <summary>
        /// The _ get pm meter detail
        /// </summary>
        public const string _GetPmMeterDetail = "_get-pm-meter-detail";

        /// <summary>
        /// The get pm meter history by asset identifier
        /// </summary>
        public const string GetPMMeterHistoryByAssetId = "get-pm-meter-history-by-assetid";

        /// <summary>
        /// The get pm meter schedule history by asset identifier
        /// </summary>
        public const string GetPMMeterScheduleHistoryByAssetId = "get-pm-meter-schedule-history-by-assetid";

        #endregion

        #region "Asset Transfer"

        /// <summary>
        /// The _ get asset transfer detail
        /// </summary>
        public const string _GetAssetTransferDetail = "_get-asset-transfer-detail";

        /// <summary>
        /// The get assets by location and asset identifier
        /// </summary>
        public const string GetAssetsByLocationAndAssetID = "GetAssetsByLocationAndAssetID";

        /// <summary>
        /// The get asset transfer list
        /// </summary>
        public const string GetAssetTransferList = "GetAssetTransferList";

        /// <summary>
        /// The manage asset Transfer
        /// </summary>
        public const string ManageAssetTransfer = "ManageAssetTransfer";
        #endregion

        #region "Document Tab"

        /// <summary>
        /// The _ get asset documents
        /// </summary>
        public const string _GetAssetDocuments = "Get-Asset-Documents";

        /// <summary>
        /// The get document list
        /// </summary>
        public const string GetDocumentList = "GetDocumentList";

        /// <summary>
        /// The get document list by identifier
        /// </summary>
        public const string GetDocumentByID = "get-document-by-id";

        /// <summary>
        /// The save document
        /// </summary>
        public const string SaveDocument = "SaveDocument";

        /// <summary>
        /// The delete document
        /// </summary>
        public const string DeleteDocument = "DeleteDocument";
        #endregion

        #region "Specification"

        /// <summary>
        /// The _ get asset transfer detail
        /// </summary>
        public const string _AssetSpecification = "_assetspecification";

        /// <summary>
        /// The get asset specification list
        /// </summary>
        public const string GetAssetSpecificationList = "getassetspecificationlist";

        /// <summary>
        /// The get asset specification by identifier
        /// </summary>
        public const string GetAssetSpecificationByID = "getassetspecificationbyid";

        /// <summary>
        /// The manage asset specification
        /// </summary>
        public const string ManageAssetSpecification = "manageassetspecification";

        /// <summary>
        /// The delete asset specification
        /// </summary>
        public const string DeleteAssetSpecification = "DeleteAssetSpecification";

        #endregion
        /// <summary>
        /// The get assets by l2 identifier
        /// </summary>
        public const string GetAssetsByL2ID = "GetAssetsByL2ID";
        #endregion

        #region "AssetTree"
        /// <summary>
        /// The asset tree
        /// </summary>
        public const string AssetTree = "asset-tree";

        /// <summary>
        /// The get asset tree
        /// </summary>
        public const string GetAssetTree = "GetAssetTree";
        #endregion

        #region "Job Request"

        /// <summary>
        /// The get Problem description type for WebReq By: Akif519
        /// </summary>
        public const string GetProblemDescType = "get-problem-type";

        /// <summary>
        /// The get Problem description for WebReq By: Akif519
        /// </summary>
        public const string GetProblemDesc = "get-problem-desc";

        /// <summary>
        /// The job request list
        /// </summary>
        public const string JobRequestList = "job-request-list";

        /// <summary>
        /// The Job Request Escalation List
        /// </summary>
        public const string JobRequestEscalationList = "job-request-escalation-list";

        /// <summary>
        /// The Job Order Escalation List
        /// </summary>

        public const string JobOrderEscalationList = "job-order-escalation-list";

        /// <summary>
        /// The get job requests
        /// </summary>
        public const string GetJobRequests = "get-job-requests";

        /// <summary>
        /// The get feedback job requests list
        /// </summary>
        public const string FeedbackJobRequestList = "get-feedback-job-requests-list";

        /// <summary>
        /// The save feedback
        /// </summary>
        public const string ManageFeedback = "manage-feedback";

        /// <summary>
        /// The get job closed requests
        /// </summary>
        public const string GetFeedbackJobRequests = "get-feedback-job-requests";

        /// <summary>
        /// The get job closed requests
        /// </summary>
        public const string GetJRFeedbackById = "get-JR-feedback-byid";

        /// <summary>
        /// The get job escalation requests
        /// </summary>
        public const string GetJobEscalationRequests = "get-job-escalation-requests";


        /// <summary>
        /// Job Request
        /// </summary>
        public const string JobRequests = "job-requests";

        /// <summary>
        /// Job Request by id
        /// </summary>
        public const string GetJobRequestById = "job-request";

        /// <summary>
        /// Job Request by Requester Id
        /// </summary>
        public const string GetAllWRByRequesterID = "get-all-wr-by-requesterid";

        /// <summary>
        /// Job Request by Asset Id
        /// </summary>
        public const string GetAllWRByAssetID = "get-all-wr-by-asset-id";

        /// <summary>
        /// Save job Request
        /// </summary>
        public const string SaveJobRequest = "save-workrequest";

        /// <summary>
        /// Get Job request documents
        /// </summary>
        public const string _GetJobRequestDocuments = "Get-JobRequest-Documents";

        /// <summary>
        /// Get Approved Projects
        /// </summary>
        public const string GetApprovedProjectListPage = "get-approved-project-list";

        #endregion

        #region "Job Order"

        /// <summary>
        /// The Job Order By Asset Id
        /// </summary>
        public const string GetAllJOByAssetID = "job-order-by-asset-id";

        /// <summary>
        /// The Job Orders By Asset ID and Location ID
        /// </summary>
        public const string GetJOByAssetIDLocationID = "job-order-by-asset-id-and-location-id";

        /// <summary>
        /// The Job Orders By Job Order No
        /// </summary>
        public const string GetJOByJobOrderNo = "joborders";

        /// <summary>
        /// The Job Orders By Job Order No
        /// </summary>
        public const string GetFeedbackJOByJONo = "feedbackjoborders";

        /// <summary>
        /// The Job Orders By Job Order No
        /// </summary>
        public const string SaveJOFeedBack = "SaveJOFeedBack";

        /// <summary>
        /// The Job Orders List
        /// </summary>
        public const string GetJobOrderList = "get-job-order-list";

        /// <summary>
        /// The Job Orders Escalation List
        /// </summary>
        public const string GetJobOrderEscalationList = "get-job-order-escalation-list";

        /// <summary>
        /// The job order pop up
        /// </summary>
        public const string JobOrderPopUp = "JobOrderPopUp";

        /// <summary>
        /// The Job Orders Cost
        /// </summary>
        public const string _GetJobOrderCost = "get-job-cost";

        /// <summary>
        /// The Job Orders Cost
        /// </summary>
        public const string _GetJobOrderPMCheckList = "get-job-pmcheck-list";

        /// <summary>
        /// The Job Orders Notes
        /// </summary>
        public const string _GetJobOrderNotes = "get-job-notes";

        /// <summary>
        /// The Job Orders JOPlan
        /// </summary>
        public const string _GetJobOrderJOPlan = "get-job-order-plan";

        /// <summary>
        /// The Job Orders Safety Instructions
        /// </summary>
        public const string _GetJobOrderJOSafetyInstr = "get-job-order-safetyinstruction";

        /// <summary>
        /// The Job Orders List
        /// </summary>
        public const string _GetPurchaseOrderRequest = "purchase-order-request";

        /// <summary>
        /// The Job Orders List
        /// </summary>
        public const string _GetIRPage = "get-item-requisition";

        /// <summary>
        /// The Job Orders
        /// </summary>
        public const string JobOrderList = "job-order-list";

        /// <summary>
        /// The Purchase Order Request List
        /// </summary>
        public const string GetPurchaseRequestList = "purchase-request-list";

        /// <summary>
        /// The Purchase Order List
        /// </summary>
        public const string GetPurchaseOrderList = "purchase-order-list";

        /// <summary>
        /// The direct issue List view
        /// </summary>
        public const string _GetDirectIssue = "direct-issue";

        /// <summary>
        /// The direct issue List
        /// </summary>
        public const string GetDirectIssueList = "direct-issue-list";

        /// <summary>
        ///  Delete direct issue
        /// </summary>
        public const string DeleteDirectIssue = "delete-direct-issue";

        /// <summary>
        ///  Add direct issue
        /// </summary>
        public const string AddNewDirectIssue = "add-direct-issue";

        /// <summary>
        /// Add/Edit Direct Issue
        /// </summary>
        public const string AddEditDirectIssue = "Add-Edit-Direct-Issue";

        /// <summary>
        /// The Assign To View
        /// </summary>
        public const string _GetWOAssignTO = "wo-assignto";

        /// <summary>
        /// The Assign To List
        /// </summary>
        public const string GetWOAssignTOList = "wo-assignto-list";

        /// <summary>
        /// To Save Assign To WorkOrder
        /// </summary>
        public const string SaveAssignToWorkOrders = "save-assignto-wo";

        /// <summary>
        /// To Delete Assign To WorkOrder
        /// </summary>
        public const string DeleteAssignToWorkOrders = "delete-assignto-wo";

        /// <summary>
        /// To Update WorkOrder Status To Assigned
        /// </summary>
        public const string UpdateWOStatusToAssigned = "update-wo-status-assigned";

        /// <summary>
        /// To Update WorkOrder Status To Closed
        /// </summary>
        public const string UpdateWOStatusToClosed = "update-wo-status-closed";

        /// <summary>
        /// To Save Work Order
        /// </summary>
        public const string SaveJobOrder = "save-workorder";

        /// <summary>
        /// To Save Supplier For Work Order
        /// </summary>
        public const string SaveAssignToSupplierToWorkOrder = "save-supplier-wo";

        /// <summary>
        /// To Get Jo Labor partial View
        /// </summary>
        public const string _GetJoLabor = "jo-labor";

        /// <summary>
        /// To Get Jo Labor partial View
        /// </summary>
        public const string _GetJoItems = "jo-Items";

        /// <summary>
        /// To Get Jo Tools
        /// </summary>
        public const string _GetJoTools = "jo-tools";

        /// <summary>
        /// To Get Jo Issues partial View
        /// </summary>
        public const string _GetJoReturns = "jo-Returns";

        /// <summary>
        /// To Get Jo Issues partial View
        /// </summary>
        public const string _GetJoIssues = "jo-Issues";

        /// <summary>
        /// To Get Ratings
        /// </summary>
        public const string GetAllRatingsForDDL = "get-all-ratings";

        /// <summary>
        /// To Compare Date/Time
        /// </summary>
        public const string CompareDateTime = "compare-datetime";

        /// <summary>
        /// To validate Date in Working days
        /// </summary>
        public const string ValidateDateInWorkingHours = "validate-date-in-workinghours";

        /// <summary>
        /// To Validate JOClosing
        /// </summary>
        public const string ValidateJOClosing = "validate-joclosing";

        /// <summary>
        /// To Save Jo Closing
        /// </summary>
        public const string SaveJOClosing = "save-jo-closing";

        /// <summary>
        /// To Save Jo Closing
        /// </summary>
        public const string SaveWOLabor = "save-jo-labor";

        /// <summary>
        /// The WorkOrder labor List
        /// </summary>
        public const string GetWorkOrderLabours = "wo-labour-list";

        /// <summary>
        /// The WorkOrder labor Pm Check List Items List
        /// </summary>
        public const string GetWorkOrderPmCheckListTasks = "wo-pmc-task-list";

        /// <summary>
        /// The WorkOrder Issues List
        /// </summary>
        public const string GetWorkOrderIssues = "wo-issues-list";

        /// <summary>
        /// To delete labor
        /// </summary>
        public const string DeleteWorkOrderLabour = "delete-wo-labour";

        /// <summary>
        /// To Get Return Part List
        /// </summary>
        public const string GetWorkOrderParts = "wo-returnparts-list";

        /// <summary>
        /// To Get Job Items
        /// </summary>
        public const string GetWorkOrderItems = "wo-items-list";

        /// <summary>
        /// To Get Job Tools
        /// </summary>
        public const string GetWorkOrderTools = "wo-tools-list";

        /// <summary>
        /// To Get Job PPE
        /// </summary>
        public const string GetWorkOrderPPE = "wo-ppe-list";

        /// <summary>
        /// To Get Job Items
        /// </summary>
        public const string GetStockCodes = "stock-code-list";

        /// <summary>
        /// To Save Job Order Issues
        /// </summary>
        public const string SaveJOIssue = "save-jo-issue";

        /// <summary>
        /// To Save Job Order Tools
        /// </summary>
        public const string SaveJOTools = "save-jo-tools";

        /// <summary>
        /// To Save Job Order Tools
        /// </summary>
        public const string SavePMCheckListTools = "save-pmchecklist-tools";

        /// <summary>
        /// To Save Job Order Returns
        /// </summary>
        public const string SaveJOReturn = "save-jo-return";

        /// <summary>
        /// To Get Balance
        /// </summary>
        public const string GetStockLevelBalance = "get-stock-balance";

        /// <summary>
        /// To Get Stock Balance By Store
        /// </summary>
        public const string GetStockBalanceByStore = "get-stock-balance-store";

        /// <summary>
        /// To Save Notes For Work Order
        /// </summary>
        public const string SaveJoNotes = "save-wo-notes";

        /// <summary>
        /// The WorkOrder Job Plan Items List by employee
        /// </summary>
        public const string GetJobPlansByEmployee = "wo-plan-items-by-employee-list";

        /// <summary>
        /// The WorkOrder Job Plan Items List
        /// </summary>
        public const string GetWorkOrderJOPlanItems = "wo-plan-items-list";

        /// <summary>
        /// The WorkOrder Job Plan Safety Instruction List
        /// </summary>
        public const string GetWorkOrderJOSafetyInstrItems = "wo-safety-instruction-items-list";

        /// <summary>
        /// To Copy job Plan Item to WorkOrder
        /// </summary>
        public const string CopyJobPlanItemsToWorkOrder = "copy-jobplan-item-to-workorder";

        /// <summary>
        /// To Copy Safety Instruction Item to WorkOrder
        /// </summary>
        public const string CopySafetyInstrItemsToWorkOrder = "copy-safetyinstr-item-to-workorder";

        /// <summary>
        /// To Delete job Plan Item
        /// </summary>
        public const string DeleteJoPlanItem = "delete-jobplan-item";

        /// <summary>
        /// The print job plan order
        /// </summary>
        public const string PrintJobPlanOrder = "print-jobplan-order";

        /// <summary>
        /// The print job plan orders
        /// </summary>
        public const string PrintJobPlanOrders = "print-jobplan-orders";

        /// <summary>
        /// The print
        /// </summary>
        public const string Print = "print";

        /// <summary>
        /// The print consumables
        /// </summary>
        public const string PrintConsumables = "print-consumables";

        /// <summary>
        /// The print safety instruction
        /// </summary>
        public const string PrintSafetyInstruction = "print-safety-instruction";

        /// <summary>
        /// The job order print j o1
        /// </summary>
        public const string JobOrderPrintJO1 = "job-order-print-jo1";

        /// <summary>
        /// The job order print j o2
        /// </summary>
        public const string JobOrderPrintJO2 = "job-order-print-jo2";

        /// <summary>
        /// The Job Order List Report
        /// </summary>
        public const string JobOrderListPrint = "job-order-list-print";

        /// <summary>
        /// The print pm jo
        /// </summary>
        public const string PrintPMJO = "print-pm-jo";

        /// <summary>
        /// The print IR
        /// </summary>
        public const string PrintItemReq = "print-item-req";

        /// <summary>
        /// To Delete Work Order Safety Instruction Item
        /// </summary>
        public const string DeletWoSafetyInstrItem = "delete-wosafety-item";

        /// <summary>
        /// To Delete Work Order Tool Item
        /// </summary>
        public const string DeleteWOToolItem = "delete-wotool-item";

        /// <summary>
        /// The WorkOrder Job Plan List
        /// </summary>
        public const string GetWorkOrderJOPlanList = "wo-plan-list";

        /// <summary>
        /// The WorkOrder Job Order Safety Instruction List
        /// </summary>
        public const string GetWorkOrderSafetyInstrList = "wo-safety-instr-list";

        /// <summary>
        /// Manage WorkOrder Job Plan Item List
        /// </summary>
        public const string ManagejobTask = "manage-job-task";

        /// <summary>
        /// Manage Work Order Safety Instruction Items
        /// </summary>
        public const string ManageWOSafetyInstr = "manage-wo-safetyinstr";

        /// <summary>
        /// Get Job Order documents
        /// </summary>
        public const string _GetJobOrderDocuments = "Get-JobOrder-Documents";

        /// <summary>
        /// Save Job Order Pm CheckList
        /// </summary>
        public const string SaveJobOrderPmCheckList = "save-job-pm-checklist-task";

        /// <summary>
        /// Change Job Order Status By Work Order No
        /// </summary>
        public const string ChangeJobOrderStatusByWorkorderNo = "set-jostatus-by-wono";

        /// <summary>
        /// The _ view location diagram
        /// </summary>
        public const string _ViewLocationDiagram = "_view-location-diagram";

        /// <summary>
        /// The get view location diagram list
        /// </summary>
        public const string GetViewLocationDiagramList = "get-view-location-diagram-list";

        /// <summary>
        /// The _ jo status track tab
        /// </summary>
        public const string _JOStatusTrackTab = "_jo-status-track-tab";

        /// <summary>
        /// The _ get jo status track list
        /// </summary>
        public const string GetJOStatusTrackList = "get-jo-status-track-list";

        #endregion

        #region "Section"
        /// <summary>
        /// The section list
        /// </summary>
        public const string SectionList = "section-list";

        /// <summary>
        /// The get section list
        /// </summary>
        public const string GetSectionList = "get-section-list";

        /// <summary>
        /// The get section list by identifier
        /// </summary>
        public const string GetSectionByID = "section-detail";

        /// <summary>
        /// The get section detail by asset identifier
        /// </summary>
        public const string GetSectionDetailBySectionID = "get-sectiondetail-by-sectionid";

        /// <summary>
        /// The manage section
        /// </summary>
        public const string ManageSection = "section-detail";

        /// <summary>
        /// The delete section
        /// </summary>
        public const string DeleteSection = "remove-section";

        /// <summary>
        /// The _ get section documents
        /// </summary>
        public const string _GetSectionDocuments = "get-section-documents";

        /// <summary>
        /// Get Coordinate By LocationID
        /// </summary>
        public const string GetCoordinateByLocationID = "GetCoordinateByLocationID";

        #region "Document Tab"

        /// <summary>
        /// The get section document list
        /// </summary>
        public const string GetSectionDocumentList = "get-section-document-list";

        /// <summary>
        /// The get section document by identifier
        /// </summary>
        public const string GetSectionDocumentByID = "get-section-document-by-id";

        /// <summary>
        /// The save section document
        /// </summary>
        public const string SaveSectionDocument = "save-section-document";

        /// <summary>
        /// The delete section document
        /// </summary>
        public const string DeleteSectionDocument = "remove-section-document";
        #endregion

        #region "Linked Locations"

        /// <summary>
        /// To Get Linked Locations partial View
        /// </summary>
        public const string _GetLinkedLocationsPage = "linked-locations";

        /// <summary>
        /// Gets the Linked Locations 
        /// </summary>
        public const string GetLinkedLocationsList = "get-linked-locations-list";

        /// <summary>
        /// To save Linked Locations
        /// </summary>
        public const string SaveLinkedLocations = "save-linked-locations";

        #endregion

        #region

        /// <summary>
        /// The _ pm meter by location
        /// </summary>
        public const string _PmMeterByLocation = "_PmMeterByLocation";

        /// <summary>
        /// The get pm meter history by location identifier
        /// </summary>
        public const string GetPMMeterHistoryByLocationId = "GetPMMeterHistoryByLocationId";

        /// <summary>
        /// The get pm meter schedule history by location identifier
        /// </summary>
        public const string GetPMMeterScheduleHistoryByLocationId = "GetPMMeterScheduleHistoryByLocationId";

        /// <summary>
        /// To Delete Linked Location
        /// </summary>
        public const string DeletLinkedLocation = "delete-linked-location";

        #endregion

        #endregion

        #region "PM Schedule"

        /// <summary>
        /// The pm schedule list
        /// </summary>
        public const string PMScheduleList = "pm-schedule";

        /// <summary>
        /// The get pm schedules
        /// </summary>
        public const string GetPMSchedules = "GetPMSchedules";

        /// <summary>
        /// The get pm schedule by identifier
        /// </summary>
        public const string GetPMScheduleByID = "pm-schedule-detail";

        /// <summary>
        /// The _ get multiple check list
        /// </summary>
        public const string _GetMultipleCheckList = "_GetMultipleCheckList";

        /// <summary>
        /// The get multiple pm list
        /// </summary>
        public const string GetMultiplePMList = "GetMultiplePMList";

        /// <summary>
        /// The manage pm schedule
        /// </summary>
        public const string ManagePMSchedule = "pm-schedule-detail";

        /// <summary>
        /// The get pm plan list
        /// </summary>
        public const string GetPMPlanList = "GetPMPlanList";

        /// <summary>
        /// The manage multiple pm
        /// </summary>
        public const string ManageMultiplePM = "ManageMultiplePM";

        /// <summary>
        /// The delete multiple pm
        /// </summary>
        public const string DeleteMultiplePM = "DeleteMultiplePM";

        #region "Assign To"

        /// <summary>
        /// The _ pm assign to
        /// </summary>
        public const string _PMAssignTo = "_PMAssignTo";

        /// <summary>
        /// The get pm assign to list
        /// </summary>
        public const string GetPMAssignToList = "getpmassigntolist";

        /// <summary>
        /// The manage pm assign to employee
        /// </summary>
        public const string ManagePMAssignToEmployee = "ManagePMAssignToEmployee";

        /// <summary>
        /// The manage pm assign to employee group
        /// </summary>
        public const string ManagePMAssignToEmployeeGroup = "ManagePMAssignToEmployeeGroup";

        /// <summary>
        /// The delete pm schedule assign to
        /// </summary>
        public const string DeletePmScheduleAssignto = "DeletePmScheduleAssignto";
        #endregion

        #endregion

        #region "PM Schedule By Asset Sub Type"

        /// <summary>
        /// The pm schedule list
        /// </summary>
        public const string PMScheduleListByAssetSubType = "pm-schedule-by-asset";

        /// <summary>
        /// The get pm schedules
        /// </summary>
        public const string GetPMSchedulesByAssetSubType = "GetPMSchedulesByAssetSubType";

        /// <summary>
        /// The get pm schedule by identifier
        /// </summary>
        public const string GetPMScheduleAssetSubTypeByID = "PM-Schedule-Detail-ByAsset";

        /// <summary>
        /// The get pm asset schedule by identifier
        /// </summary>
        public const string GetPMAssetScheduleByID = "pm-asset-schedule-detail";

        /// <summary>
        /// The get assets for pm asset schedule
        /// </summary>
        public const string GetAssetsForPMAssetSchedule = "GetAssetsForPMAssetSchedule";

        /// <summary>
        /// The manage pm schedule by asset
        /// </summary>
        public const string ManagePMScheduleByAsset = "pm-asset-schedule-detail";

        #region Multiple PM ByAsset
        /// <summary>
        /// The _ get multiple check list for asset
        /// </summary>
        public const string _GetMultipleCheckListForAsset = "_GetMultipleCheckListForAsset";

        /// <summary>
        /// The manage multiple pm for asset
        /// </summary>
        public const string ManageMultiplePMForAsset = "ManageMultiplePMForAsset";

        /// <summary>
        /// The delete multiple pm for asset
        /// </summary>
        public const string DeleteMultiplePMForAsset = "DeleteMultiplePMForAsset";
        #endregion

        #region "Assign To"

        /// <summary>
        /// The _ pm by asset assign to
        /// </summary>
        public const string _PMByAssetAssignTo = "_PMByAssetAssignTo";

        /// <summary>
        /// The manage pm assign to employee
        /// </summary>
        public const string ManagePMAssignToEmployeeForAsset = "ManagePMAssignToEmployeeForAsset";

        /// <summary>
        /// The manage pm assign to employee group
        /// </summary>
        public const string ManagePMAssignToEmployeeGroupForAsset = "ManagePMAssignToEmployeeGroupForAsset";

        /// <summary>
        /// The delete pm schedule assign to for asset
        /// </summary>
        public const string DeletePmScheduleAssigntoForAsset = "DeletePmScheduleAssigntoForAsset";

        #endregion

        #endregion

        #region "Cleaning Schedule"

        /// <summary>
        /// The cleaning schedule list
        /// </summary>
        public const string CleaningScheduleList = "cleaning-schedule";

        /// <summary>
        /// The get cleaning schedules
        /// </summary>
        public const string GetCleaningSchedules = "GetCleaningSchedules";

        /// <summary>
        /// The get pm schedule by cleaning identifier
        /// </summary>
        public const string GetPMScheduleByCleaningID = "cleaningscheduledetail";

        /// <summary>
        /// The manage cleaning schedule
        /// </summary>
        public const string ManageCleaningSchedule = "cleaningscheduledetail";
        #endregion

        #region "PM Jo Generation"

        /// <summary>
        /// The pm jo generation
        /// </summary>
        public const string PMJoGeneration = "pm-jo-generation";

        /// <summary>
        /// The get list pm gen dates
        /// </summary>
        public const string GetListPMGenDates = "GetListPMGenDates";

        /// <summary>
        /// The view pm
        /// </summary>
        public const string ViewPM = "ViewPM";

        /// <summary>
        /// The get intermediate data by user identifier
        /// </summary>
        public const string GetIntermediateDataByUserId = "GetIntermediateDataByUserId";

        /// <summary>
        /// The open pm jo
        /// </summary>
        public const string OpenPMJo = "OpenPMJo";
        #endregion

        #region "Cleaning Jo Generation"

        /// <summary>
        /// The pm jo generation
        /// </summary>
        public const string CleaningJoGeneration = "cleaning-jo-generation";

        /// <summary>
        /// The get list pm gen dates
        /// </summary>
        public const string GetListCleaningGenDates = "GetListCleaningGenDates";

        /// <summary>
        /// The view pm
        /// </summary>
        public const string ViewCleaning = "ViewCleaning";
               
        /// <summary>
        /// The open pm jo
        /// </summary>
        public const string OpenCleaningJo = "OpenCleaningJo";

        public const string PrintCleaningJO = "PrintCleaningJO";

        public const string GetIntermediateDataByUserIdForCleaning = "GetIntermediateDataByUserIdForCleaning";
        #endregion

        #region "PM CheckList"
        /// <summary>
        /// The pm checklist
        /// </summary>
        public const string PMCheckList = "pm-check-list";

        /// <summary>
        /// The get pm checklist
        /// </summary>
        public const string GetPMChecklist = "GetPMChecklist";

        /// <summary>
        /// The get pm checklist
        /// </summary>
        public const string GetPMChecklistAdvanceSearch = "GetPMChecklistAdvanceSearch";

        /// <summary>
        /// The get pm checklist by identifier
        /// </summary>
        public const string GetPMChecklistByID = "get-pm-checklist-by-id";

        /// <summary>
        /// The manage pm checklist
        /// </summary>
        public const string ManagePMChecklist = "get-pm-checklist-by-id";

        /// <summary>
        /// The delete pm checklist
        /// </summary>
        public const string DeletePMChecklist = "remove-pm-checklist";

        /// <summary>
        /// The manage checklist Element
        /// </summary>
        public const string ManageCheckListElements = "manage-checklist-elements";

        /// <summary>
        /// The Get checklist Items
        /// </summary>
        public const string GetCheckListInv = "get-checklist-item";

        /// <summary>
        /// The Get checklist Elements
        /// </summary>
        public const string GetCheckListElements = "get-checklist-elements";

        /// <summary>
        /// The Get checklist Elements DropDown
        /// </summary>
        public const string GetCheckListElementsDDL = "get-checklist-elements-ddl";

        /// <summary>
        /// The Get checklist tools
        /// </summary>
        public const string GetCheckListTools = "get-checklist-tools";

        /// <summary>
        /// The Manage checklist tools
        /// </summary>
        public const string ManageCheckListTools = "manage-checklist-tools";

        /// <summary>
        /// The Get checklist PPE
        /// </summary>
        public const string GetCheckListPPE = "get-checklist-ppe";

        /// <summary>
        /// The Manage checklist tools
        /// </summary>
        public const string ManageCheckListPPE = "manage-checklist-ppe";

        /// <summary>
        /// The Delete checklist Tools
        /// </summary>
        public const string DeleteChecklistTools = "delete-checklist-tools";

        /// <summary>
        /// The Delete checklist Element
        /// </summary>
        public const string DeleteChecklistElement = "delete-checklist-element";

        /// <summary>
        /// The Delete checklist PPE
        /// </summary>
        public const string DeleteChecklistPPE = "delete-checklist-ppe";

        /// <summary>
        /// The Manage checklist Items
        /// </summary>
        public const string ManageCheckListInv = "manage-checklist-inv";

        /// <summary>
        /// The Delete checklist Item
        /// </summary>
        public const string DeleteChecklistInv = "delete-checklist-inv";

        /// <summary>
        /// The Update PM CheckList Safety ID
        /// </summary>
        public const string UpdateSafetyIDForPMCheckList = "update-safety-id-for-pmchecklist";

        #endregion

        #region "PM CheckList"


        public const string GetCleaningCheckList = "Cleaning-check-list";

        /// <summary>
        /// The pm check list for cleaning
        /// </summary>
        public const string PMCheckListForCleaning = "pm-check-list-for-cleaning";

        /// <summary>
        /// The get pm checklist for cleaning page
        /// </summary>
        public const string GetPMChecklistForCleaningPage = "getpmchecklistforcleaningpage";

        /// <summary>
        /// The get pm checklist for cleaning by identifier
        /// </summary>
        public const string GetPMChecklistForCleaningByID = "get-pm-checklist-for-cleaning-by-id";

        /// <summary>
        /// The delete pm checklist for cleaning
        /// </summary>
        public const string DeletePMChecklistForCleaning = "remove-pm-checklist-for-cleaning";

        /// <summary>
        /// The get pm checklist detail for cleaning
        /// </summary>
        public const string GetPMChecklistDetailForCleaning = "get-pm-check-list-detail-for-cleaning";

        /// <summary>
        /// The manage pm checklist for cleaning
        /// </summary>
        public const string ManagePMChecklistForCleaning = "get-pm-check-list-detail-for-cleaning";


        #endregion

        #region "PM Meter Master List"
        /// <summary>
        /// get PM Meter Master List View
        /// </summary>
        public const string PMMeterMasterList = "pmmetermasterlist";

        /// <summary>
        /// get PM Meter Master List
        /// </summary>
        public const string GetPMMeterMasterListGrid = "GetPMMeterMasterListGrid";

        /// <summary>
        /// Delete PMMeter Master item
        /// </summary>
        public const string DeletePMMeterMaster = "DeletePMMeterMaster";

        /// <summary>
        /// Delete PMMeter Master item
        /// </summary>
        public const string PMMeterMasterEdit = "pmmetermasteredit";

        /// <summary>
        /// get PM Meter List View
        /// </summary>
        public const string GetPMMeterListGrid = "GetPMMeterListGrid";

        /// <summary>
        /// insert Update PM Meter Master
        /// </summary>
        public const string ManagePMMeterMaster = "pmmetermasteredit";

        /// <summary>
        /// Get Asset Details By AssetID
        /// </summary>
        public const string GetAssetDetailsByAssetID = "GetAssetDetailsByAssetID";

        /// <summary>
        /// insert Update PM Meter
        /// </summary>
        public const string PMMeterAdd = "pmmeteradd";

        /// <summary>
        /// insert Update PM Meter
        /// </summary>
        public const string ManagePMMeter = "pmmeteradd";

        #region "PM Meter - Assign To"

        /// <summary>
        /// The _ pm meter assign to
        /// </summary>
        public const string _PMMeterAssignTo = "_PMMeterAssignTo";

        /// <summary>
        /// The get pm assign to list
        /// </summary>
        public const string GetPMMeterAssignToList = "getpmmeterassigntolist";

        /// <summary>
        /// The manage pm assign to employee
        /// </summary>
        public const string ManagePMMeterAssignToEmployee = "ManagePMMeterAssignToEmployee";

        /// <summary>
        /// The manage pm assign to employee group
        /// </summary>
        public const string ManagePMMeterAssignToEmployeeGroup = "ManagePMMeterAssignToEmployeeGroup";

        /// <summary>
        /// The delete pm schedule assign to
        /// </summary>
        public const string DeletePmMeterAssignto = "DeletePmMeterAssignto";

        #endregion
        #endregion

        #region "CEMUL"
        /// <summary>
        /// The CEMUL
        /// </summary>
        public const string CEMULPage = "cemul-page";
        #endregion

        #region "Reading"
        /// <summary>
        /// load Reading page
        /// </summary>
        public const string Reading = "reading";

        /// <summary>
        /// Get PMMeter Reading List
        /// </summary>
        public const string GetPMMeterReadingList = "GetPMMeterReadingList";

        /// <summary>
        /// Delete PMMeter Reading
        /// </summary>
        public const string DeletePMMeterReading = "DeletePMMeterReading";

        /// <summary>
        /// update PMMeter Reading
        /// </summary>
        public const string UpdatePMMeterReading = "UpdatePMMeterReading";

        /// <summary>
        /// Reading Schedule List
        /// </summary>
        public const string ReadingScheduleList = "ReadingScheduleList";

        /// <summary>
        /// PM Meter Master List By City
        /// </summary>
        public const string GetPMMeterMasterListByCity = "GetPMMeterMasterListByCity";

        /// <summary>
        /// Add PMMeter Reading
        /// </summary>
        public const string InsertUpdatePMMeterReading = "InsertUpdatePMMeterReading";

        /// <summary>
        /// get Job Orders By MeterMasterID
        /// </summary>
        public const string GetJobOrdersByMeterMasterID = "getJobOrdersByMeterMasterID";

        /// <summary>
        /// The export reading
        /// </summary>
        public const string ExportReading = "ExportReading";

        /// <summary>
        /// The export reading count
        /// </summary>
        public const string ExportReadingCount = "ExportReadingCount";
        #endregion

        #region "PM Meter Jo Generation"
        /// <summary>
        /// PM Meter Jo Generation
        /// </summary>
        public const string PMMeterJoGeneration = "pmmeterjogeneration";

        /// <summary>
        /// The get meter intermediate data by user identifier
        /// </summary>
        public const string GetMeterIntermediateDataByUserId = "GetMeterIntermediateDataByUserId";

        /// <summary>
        /// The view pm meter
        /// </summary>
        public const string ViewPMMeter = "ViewPMMeter";

        /// <summary>
        /// The open pm meter jo
        /// </summary>
        public const string OpenPMMeterJo = "OpenPMMeterJo";
        #endregion

        #region "Purchasing"
        /// <summary>
        /// Purchase Request
        /// </summary>
        public const string GetPurchaseRequestPage = "purchaserequest";

        /// <summary>
        /// Purchase Order
        /// </summary>
        public const string GetPurchaseOrderPage = "purchaseorder";
        public const string POPrint = "POPrint";
        #endregion

        #region "Purchase Request"

        /// <summary>
        /// The purchase request list
        /// </summary>
        public const string PurchaseRequestList = "purchase-request-list";

        /// <summary>
        /// The get purchase request
        /// </summary>
        public const string GetPurchaseRequest = "get-purchase-request";

        /// <summary>
        /// The get purchase request items by Purchase Request id
        /// </summary>
        public const string GetPurchaseRequestItemsByPRID = "GetPurchaseRequestItemsByPRID";

        /// <summary>
        /// The manage purchase request
        /// </summary>
        public const string ManagePurchaseRequest = "ManagePurchaseRequest";

        /// <summary>
        /// The manage purchase request items
        /// </summary>
        public const string ManagePurchaseRequestItems = "ManagePurchaseRequestItems";

        /// <summary>
        /// The delete purchase request items
        /// </summary>
        public const string DeletePurchaseRequestItems = "DeletePurchaseRequestItems";

        /// <summary>
        /// The get approve identifier
        /// </summary>
        public const string GetApprovePRByprID = "GetApprovePRByprID";

        /// <summary>
        /// The get purchase request approval levels by l2 
        /// </summary>
        public const string GetPRApprovalLevelsByL2IDPRID = "GetPRApprovalLevelsByL2IDPRID";

        /// <summary>
        /// The get employees by purchase approval level identifier
        /// </summary>
        public const string GetEmployeesByPRApprovalLevelId = "GetEmployeesByPRApprovalLevelId";

        /// <summary>
        /// The new approve purchase request
        /// </summary>
        public const string NewApprovePR = "NewApprovePR";

        /// <summary>
        /// The cancel purchase request
        /// </summary>
        public const string CancelPurchaseRequest = "CancelPurchaseRequest";

        /// <summary>
        /// The re open purchase request
        /// </summary>
        public const string ReOpenPurchaseRequest = "ReOpenPurchaseRequest";

        /// <summary>
        /// The _ get purchase request documents
        /// </summary>
        public const string _GetPRDocuments = "_GetPRDocuments";

        /// <summary>
        /// The print purchase request
        /// </summary>
        public const string PrintPurchaseRequest = "print-purchase-request";
        #endregion

        #region "Purchase Order"

        /// <summary>
        /// The purchase order list
        /// </summary>
        public const string PurchaseOrderList = "purchase-order-list";

        /// <summary>
        /// The get purchase order
        /// </summary>
        public const string GetPurchaseOrder = "get-purchase-order";

        /// <summary>
        /// The get purchase order items by purchase id with out load
        /// </summary>
        public const string GetPurchaseOrderItemsByPRIDWithOutLoad = "GetPurchaseOrderItemsByPRIDWithOutLoad";

        /// <summary>
        /// The get purchase request for purchase order
        /// </summary>
        public const string GetPRForPO = "GetPRForPO";

        /// <summary>
        /// The get Item Requisition for purchase order
        /// </summary>
        public const string GetIRForPO = "GetIRForPO";

        /// <summary>
        /// The manage purchase order
        /// </summary>
        public const string ManagePurchaseOrder = "ManagePurchaseOrder";

        /// <summary>
        /// The manage purchase order items
        /// </summary>
        public const string ManagePurchaseOrderItems = "ManagePurchaseOrderItems";

        /// <summary>
        /// The delete purchase order items
        /// </summary>
        public const string DeletePurchaseOrderItems = "DeletePurchaseOrderItems";

        /// <summary>
        /// The update delivery and order status po
        /// </summary>
        public const string UpdateDeliverynOrderStatusPO = "UpdateDeliverynOrderStatusPO";

        /// <summary>
        /// The get po authentication history
        /// </summary>
        public const string GetPOAuthHistory = "GetPOAuthHistory";

        /// <summary>
        /// The validate employee
        /// </summary>
        public const string ValidateEmployee = "ValidateEmployee";

        /// <summary>
        /// The _ get purchase order documents
        /// </summary>
        public const string _GetPODocuments = "_GetPODocuments";

        /// <summary>
        /// The get po document list
        /// </summary>
        public const string GetPODocumentList = "GetPODocumentList";

        /// <summary>
        /// The print purchase order
        /// </summary>
        public const string PrintPurchaseOrder = "print-purchase-order";

        #endregion

        #region "Good Receipt"

        /// <summary>
        /// The good receipt list
        /// </summary>
        public const string GoodReceiptList = "good-receipt-list";

        /// <summary>
        /// The get good receipt
        /// </summary>
        public const string GetGoodReceipt = "get-good-receipt";

        /// <summary>
        /// The good receipt detail
        /// </summary>
        public const string GoodReceiptDetail = "good-receipt-detail";

        /// <summary>
        /// The get po item transactions
        /// </summary>
        public const string GetPOItemTransactions = "get-po-item-transactions";

        /// <summary>
        /// The get po items by po id
        /// </summary>
        public const string GetPOItemsByPOID = "get-po-item-by-po-id";

        /// <summary>
        /// The get po item list by po item identifier
        /// </summary>
        public const string GetPOItemListByPOItemID = "get-po-item-list-by-po-item-id";

        /// <summary>
        /// The calculate all items summary
        /// </summary>
        public const string CalculateAllItemsSummary = "calculate-all-items-summary";

        /// <summary>
        /// The get po store by po id
        /// </summary>
        public const string GetPOStoreByPOID = "get-po-store-by-po-id";

        /// <summary>
        /// The manage ra receive all
        /// </summary>
        public const string ManageRAReceiveAll = "manage-ra-receive-all";

        /// <summary>
        /// The cancel po item
        /// </summary>
        public const string CancelPOItem = "cancel-po-item";

        /// <summary>
        /// The manage new receive item
        /// </summary>
        public const string ManageNewReceiveItem = "manage-new-receive-item";

        #endregion

        #region "Stock Code"

        /// <summary>
        /// The get purchase proposal high priority
        /// </summary>
        public const string GetPurchaseProposalHighPriority = "GetPurchaseProposalHighPriority";

        /// <summary>
        /// The get purchase proposal normal Priority
        /// </summary>
        public const string GetPurchaseProposalNormalPriority = "GetPurchaseProposalNormalPriority";

        #endregion

        #region "Request Cards"

        #region "Consumables"

        /// <summary>
        /// The consumables list
        /// </summary>
        public const string ConsumablesList = "consumables-list";

        /// <summary>
        /// The get consumables list
        /// </summary>
        public const string GetConsumablesList = "get-consumables-list";

        /// <summary>
        /// The consumables detail
        /// </summary>
        public const string ConsumablesDetail = "consumables-detail";

        /// <summary>
        /// The _ get consumables documents
        /// </summary>
        public const string _GetConsumablesDocuments = "get-consumables-documents";

        /// <summary>
        /// The get sub store status
        /// </summary>
        public const string GetSubStoreStatus = "get-sub-store-status";

        /// <summary>
        /// The get sub store card form detail by card identifier
        /// </summary>
        public const string GetSubStoreCardFormDetailByCardID = "get-sub-store-card-from-detail-by-cardid";

        /// <summary>
        /// The manage sub store card form detail
        /// </summary>
        public const string ManageSubStoreCardFormDetail = "manage-sub-store-card-from-detail";

        /// <summary>
        /// The get employees by sub department identifier
        /// </summary>
        public const string GetEmployeesBySubDeptID = "get-employees-by-sup-dept-id";

        /// <summary>
        /// The manage consumables
        /// </summary>
        public const string ManageConsumables = "manage-consumables";

        /// <summary>
        /// The manage lock issue
        /// </summary>
        public const string ManageLockIssue = "manage-lock-issue";

        /// <summary>
        /// The _ store card return popup
        /// </summary>
        public const string _StoreCardReturnPopup = "_store-card-return-popup";

        /// <summary>
        /// The manage store card return
        /// </summary>
        public const string ManageStoreCardReturn = "manage-store-card-return-popup";

        /// <summary>
        /// The delete sub store card detail
        /// </summary>
        public const string DeleteSubStoreCardDetail = "remove-sub-store-card-detail";

        #endregion

        #region "Personal"

        /// <summary>
        /// The personal list
        /// </summary>
        public const string PersonalList = "personal-list";

        /// <summary>
        /// The get personal list
        /// </summary>
        public const string GetPersonalList = "get-personal-list";

        /// <summary>
        /// The personal detail
        /// </summary>
        public const string PersonalDetail = "personal-detail";

        /// <summary>
        /// The _ store card transfer popup
        /// </summary>
        public const string _StoreCardTransferPopup = "_store-card-transfer-popup";

        /// <summary>
        /// The get employees by l2 identifier
        /// </summary>
        public const string GetEmployeesByL2ID = "get-employees-by-l2-id";

        /// <summary>
        /// The get card no by employee identifier
        /// </summary>
        public const string GetCardNobyEmployeeId = "get-card-no-by-employee-id";

        /// <summary>
        /// The manage store card transfer
        /// </summary>
        public const string ManageStoreCardTransfer = "manage-store-card-transfer";

        #endregion

        #endregion

        #region "Cleaning Inspection"

        /// <summary>
        /// Cleaning Inspection Page
        /// </summary>
        public const string CleaningInspection = "cleaninginspection";

        /// <summary>
        /// Cleaning Inspection List 
        /// </summary>
        public const string CleaningInspectionList = "CleaningInspectionList";

        /// <summary>
        /// Delete Cleaning Inspection
        /// </summary>
        public const string DeleteCleaningInspection = "DeleteCleaningInspection";

        /// <summary>
        /// Cleaning Inspection add/edit Page
        /// </summary>
        public const string CleaningInspectionEdit = "cleaninginspectionedit";

        /// <summary>
        /// Cleaning Inspection Insert/Update
        /// </summary>
        public const string ManageCleaningInspection = "ManageCleaningInspection";

        /// <summary>
        /// load CleaningList partial view
        /// </summary>
        public const string CleaningList = "CleaningList";

        /// <summary>
        /// bind Cleaning Details List grid
        /// </summary>
        public const string CleaningDetailsList = "CleaningDetailsList";

        /// <summary>
        /// bind Cleaning Group List
        /// </summary>
        public const string GetCleaningGroupList = "GetCleaningGroupList";

        /// <summary>
        /// bind Cleaning location List
        /// </summary>
        public const string GetCleaningLocationList = "GetCleaningLocationList";

        /// <summary>
        /// Cleaning List Insert/Update
        /// </summary>
        public const string ManageCleaningList = "CleaningList";

        /// <summary>
        /// Cleaning Detail delete
        /// </summary>
        public const string DeleteCleaningDetails = "DeleteCleaningDetails";

        /// <summary>
        /// Cleaning Inspection Group Page
        /// </summary>
        public const string CleaningInspectionGroup = "cleaninginspectiongroup";

        /// <summary>
        /// Cleaning Inspection Group List 
        /// </summary>
        public const string CleaningInspectionGroupList = "CleaningInspectionGroupList";

        /// <summary>
        /// Delete Cleaning Inspection Group Record
        /// </summary>
        public const string DeleteCleaningInspectionGroup = "DeleteCleaningInspectionGroup";

        /// <summary>
        /// Cleaning Inspection Group add/edit Page
        /// </summary>
        public const string CleaningInspectionGroupEdit = "cleaninginspectiongroupedit";

        /// <summary>
        /// Cleaning Inspection Group Sequence Number List
        /// </summary>
        public const string CIGroupSequenceList = "CIGroupSequenceList";

        /// <summary>
        ///  Cleaning Inspection Group Sequence Number delete
        /// </summary>
        public const string DeleteCIGroupSequence = "DeleteCIGroupSequence";

        /// <summary>
        /// Cleaning List Group Insert/Update
        /// </summary>
        public const string ManageCleaningInspectionGroup = "ManageCleaningInspectionGroup";

        /// <summary>
        ///  Cleaning Inspection Group Sequence Number add/edit
        /// </summary>
        public const string AddEditCIGroupSequence = "AddEditCIGroupSequence";

        /// <summary>
        /// To Download Cleaning Inspection Attachment
        /// </summary>
        public const string DownloadCIAttachment = "DownloadCIAttachment";

        #endregion

        #region "Store"

        /// <summary>
        /// Gets the Items List Page
        /// </summary>
        public const string StoreItemsList = "items-list";

        /// <summary>
        /// Gets the Items List Page
        /// </summary>
        public const string GetStoreItemsList = "get-items-list";

        /// <summary>
        /// Gets the Item by id
        /// </summary>
        public const string GetItemByStockID = "items";

        /// <summary>
        /// Gets Delete Item by Id
        /// </summary>
        public const string DeleteItemById = "delete-item-by-id";

        /// <summary>
        /// The get all items type
        /// </summary>
        public const string GetAllItemTypesDDL = "get-all-item-types";

        /// <summary>
        /// The get all items sub type based on item type
        /// </summary>
        public const string GetItemSubTypesByItemTypeDDL = "get-all-item-sub-types-by-type";

        /// <summary>
        /// To Get Alternative Items Tab
        /// </summary>
        public const string _GetAlternativeItems = "get-alternative-items";

        /// <summary>
        /// To Get Alternative Suppliers Tab
        /// </summary>
        public const string _GetAlternativeSuppliers = "get-alternative-suppliers";

        /// <summary>
        /// To Delete Alternative item
        /// </summary>
        public const string DeleteAlternativeItem = "delete-alternative-item";

        /// <summary>
        /// The Alternative Items List
        /// </summary>
        public const string GetAlternativeItemList = "get-alternative-list";

        /// <summary>
        /// The Alternative Suppliers List
        /// </summary>
        public const string GetAlternativeSuppliersList = "get-alt-suppliers-list";

        /// <summary>
        /// To Add Alternative Item
        /// </summary>
        public const string SaveAlternativeItem = "save-alt-item";

        /// <summary>
        /// To Add Alternative Supplier
        /// </summary>
        public const string SaveAlternativeSupplier = "save-alt-supplier";

        /// <summary>
        /// To Delete Alternative Supplier
        /// </summary>
        public const string DeleteAlternativeSupplier = "delete-alternative-supplier";

        /// <summary>
        /// Get Items documents
        /// </summary>
        public const string _GetItemsDocuments = "Get-items-Documents";

        /// <summary>
        /// The Stock Code Levels
        /// </summary>
        public const string GetStockCodeLevelByStockId = "get-stockcodelevels-list-by-stockid";

        /// <summary>
        /// The Stock Code City Levels
        /// </summary>
        public const string GetStockCodeLevelCityByStockId = "get-sl-city-list-by-stockid";

        /// <summary>
        /// To Delete Stock Code Level
        /// </summary>
        public const string DeleteStockCodeLevel = "delete-stockcodelevel";

        /// <summary>
        /// To Save Stock Code Level
        /// </summary>
        public const string SaveStockCodeLevel = "save-stock-code-level";

        /// <summary>
        /// To Get Part Locations by Sub Store Id
        /// </summary>
        public const string GetPartLocationsBySubStore = "get-part-locations-by-substoreid";

        /// <summary>
        /// To Save Store Items
        /// </summary>
        public const string SaveItem = "save-item";

        /// <summary>
        /// To Get last Stock no
        /// </summary>
        public const string GetLastStocCodeStoreNo = "get-last-stockcodeno";

        /// <summary>
        /// Gets the Transaction List Page
        /// </summary>
        public const string GetTransactionListPage = "transactions-list";

        /// <summary>
        /// Gets the Transaction List
        /// </summary>
        public const string GetTransactionList = "get-transactions-list";

        /// <summary>
        /// Gets the Transaction Issue Page
        /// </summary>
        public const string GetTransactionIssuePage = "transactions-issue";

        /// <summary>
        /// Gets the Transaction return Page
        /// </summary>
        public const string GetTransactionReturnPage = "transactions-return";

        /// <summary>
        /// Gets the Transaction receive Page
        /// </summary>
        public const string GetTransactionReceivePage = "transactions-receive";

        /// <summary>
        /// Gets the Transaction Adjustment Page
        /// </summary>
        public const string GetTransactionAdjustmentPage = "transactions-adjustment";

        /// <summary>
        /// Gets the Transaction transfer Page
        /// </summary>
        public const string GetTransactionTransferPage = "transactions-transfer";

        /// <summary>
        /// Gets the Transaction Purchase Proposal Page
        /// </summary>
        public const string GetTransactionPurchaseProposalPage = "transactions-purchaseproposal";

        /// <summary>
        /// To Get Store Items Auto Complete DDL
        /// </summary>
        public const string GetStockCodesAutoDDL = "stock-code-auto-ddl";

        /// <summary>
        /// To Get Job Order Items Auto Complete DDL
        /// </summary>
        public const string GetJobOrderAutoDDL = "job-order-auto-ddl";

        /// <summary>
        /// The Issues List By Item, City, Store
        /// </summary>
        public const string GetIssueListByItemCityStore = "issues-list-by-item";

        /// <summary>
        /// The Receive List By Item, City, Store
        /// </summary>
        public const string GetReceiveListByItemCityStore = "receives-list-by-item";

        /// <summary>
        /// The Return List By Item, City, Store
        /// </summary>
        public const string GetReturnByStockCodeandSubStore = "return-list-by-item";

        /// <summary>
        /// The Adjustment List By Item, City, Store
        /// </summary>
        public const string GetAdjustmentByStockCodeandSubStore = "adjustment-list-by-item";

        /// <summary>
        /// To Save Temp Issues
        /// </summary>
        public const string SaveTempIssue = "save-temp-issue";

        /// <summary>
        /// To Save Temp Adjustment
        /// </summary>
        public const string SaveTempAdjustment = "save-temp-adj";

        /// <summary>
        /// To Save Temp Receive
        /// </summary>
        public const string SaveTempReceive = "save-temp-receive";

        /// <summary>
        /// To Save Temp Return
        /// </summary>
        public const string SaveTempReturn = "save-temp-return";

        /// <summary>
        /// To Save Temp Transfer
        /// </summary>
        public const string SaveTempTransfer = "save-temp-transfer";

        /// <summary>
        /// To Save Transaction Issues
        /// </summary>
        public const string SaveTransactionIssue = "save-transaction-issue";

        /// <summary>
        /// To Save Transaction Receive
        /// </summary>
        public const string SaveTransactionReceive = "save-transaction-receive";

        /// <summary>
        /// To Save Transaction Return
        /// </summary>
        public const string SaveTransactionReturn = "save-transaction-return";

        /// <summary>
        /// To Save Transaction Adjustment
        /// </summary>
        public const string SaveTransactionAdjustment = "save-transaction-adjustment";

        /// <summary>
        /// To Get return balance
        /// </summary>
        public const string GetReturnBalance = "get-return-balance";

        /// <summary>
        /// To Get return balance
        /// </summary>
        public const string GetIssueUnitPriceByStockStoreWOandLOC = "get-issue-unit-price-ddl";

        /// <summary>
        /// The Purchase Proposal Normal Priority
        /// </summary>
        public const string GetPurchaseProposalNormalpriority = "get-purchase-proposal-normal-priority";

        /// <summary>
        /// The Purchase Proposal High Priority
        /// </summary>
        public const string GetPurchaseProposalHighpriority = "get-purchase-proposal-high-priority";

        /// <summary>
        /// Gets the Transaction by id Page
        /// </summary>
        public const string GetTransactionByIdPage = "transactions";

        /// <summary>
        /// Gets the Transaction by id
        /// </summary>
        public const string GetTransactionById = "transactions-by-id";

        /// <summary>
        /// To Change Item Photo
        /// </summary>
        public const string ChangeItemPhoto = "ChangeItemPhoto";

        #endregion
        #endregion

        #region "Advanced Search"

        /// <summary>
        /// The Get Search Data By Page ID
        /// </summary>
        public const string GetSearchDataByPageID = "get-search-data-by-page-id";

        /// <summary>
        /// The Save And Advanced Search
        /// </summary>
        public const string SaveAndAdvancedSearch = "save-and-advanced-search";

        /// <summary>
        /// The Search without Save
        /// </summary>
        public const string SaveSearch = "save-search";

        /// <summary>
        /// The delete advanced search
        /// </summary>
        public const string DeleteAdvancedSearch = "remove-advanced-search";

        /// <summary>
        /// The advanced search
        /// </summary>
        public const string _AdvancedSearch = "_advanced-search";

        #endregion

        #region "Item Requisition"

        /// <summary>
        /// Item Requisition List Page
        /// </summary>
        public const string ItemRequisition = "itemrequisition";

        /// <summary>
        /// Get Item Requisition List 
        /// </summary>
        public const string ItemRequisitionList = "ItemRequisitionList";

        /// <summary>
        /// Item Requisition Details List Page
        /// </summary>
        public const string ItemRequisitionDetails = "itemrequisitiondetails";

        /// <summary>
        /// Manage Item Requisition
        /// </summary>
        public const string ManageItemRequisition = "ManageItemRequisition";

        /// <summary>
        /// The get Item Requisition Items By IRID
        /// </summary>
        public const string GetItemRequisitionItemsByIRID = "GetItemRequisitionItemsByIRID";

        /// <summary>
        /// The get Item Requisition Items By WorkOrder No
        /// </summary>
        public const string GetItemRequisitionItemsByWoNo = "GetItemRequisitionItemsByWoNo";

        /// <summary>
        /// Manage Item Requisition Items
        /// </summary>
        public const string ManageItemRequisitionItems = "ManageItemRequisitionItems";

        /// <summary>
        /// The delete Item Requisition items
        /// </summary>
        public const string DeleteItemRequisitionItems = "DeleteItemRequisitionItems";

        /// <summary>
        /// The Cancel Item Requisition
        /// </summary>
        public const string CancelItemRequisition = "CancelItemRequisition";

        /// <summary>
        /// The re open Item Requisition
        /// </summary>
        public const string ReOpenItemRequisition = "ReOpenItemRequisition";

        /// <summary>
        /// The get approve identifier
        /// </summary>
        public const string GetApproveIRByIRID = "GetApproveIRByIRID";

        /// <summary>
        /// The get purchase request approval levels by sub store id
        /// </summary>
        public const string GetPRApprovalLevelsByStoreIDIRID = "GetPRApprovalLevelsByStoreIDIRID";

        /// <summary>
        /// The get employees by IR approval level identifier
        /// </summary>
        public const string GetEmployeesByIRApprovalLevelId = "GetEmployeesByIRApprovalLevelId";

        /// <summary>
        /// The new approve item Requisition
        /// </summary>
        public const string NewApproveIR = "NewApproveIR";

        /// <summary>
        /// The _ edit item issue from ItemRequisition
        /// </summary>
        public const string _IssueItemFromIR = "IssueItemIR";

        /// <summary>
        /// To Save Item Issue from Item Requisition
        /// </summary>
        public const string SaveItemIssue = "save-item-issue-ir";
        #endregion

        #region "Help"

        /// <summary>
        /// Help Page
        /// </summary>
        public const string Help = "Help";

        /// <summary>
        /// Load Help Tree
        /// </summary>
        public const string LoadHelpTree = "LoadHelpTree";

        /// <summary>
        /// Get Help Tree
        /// </summary>
        public const string GetHelpTree = "GetHelpTree";

        /// <summary>
        /// Get Help Detail
        /// </summary>
        public const string GetHelpDetail = "GetHelpDetail";

        /// <summary>
        /// Get Help Tree Index vise
        /// </summary>
        public const string GetHelpTreeIndex = "GetHelpTreeIndex";

        /// <summary>
        /// Get Help Bookmarks
        /// </summary>
        public const string GetHelpBookmarks = "GetHelpBookmarks";

        /// <summary>
        /// Add/Remove Bookmark
        /// </summary>
        public const string BookmarHelp = "BookmarHelp";

        /// <summary>
        /// Search Help
        /// </summary>
        public const string HelpSearch = "HelpSearch";

        #endregion

        #region "Auto Complete"

        /// <summary>
        /// get employee for auto complete
        /// </summary>
        public const string GetEmployeeAssignPageAutoDDL = "GetEmployeeAssignPageAutoDDL";

        /// <summary>
        /// The Get Building Details for Auto Complete
        /// </summary>
        public const string GetBuildingDetailsAutoDDL = "GetBuildingDetailsAutoDDL";

        /// <summary>
        /// The get locations for Auto Complete
        /// </summary>
        public const string GetLocationPageAutoDDL = "GetLocationPageAutoDDL";

        /// <summary>
        /// The get assets for location for job request Auto Complete
        /// </summary>
        public const string GetAssetsByLocationForJobRequestAutoDDL = "GetAssetsByLocationForJobRequestAutoDDL";

        /// <summary>
        /// The get Auto Complete Supplier
        /// </summary>
        public const string GetAllSupplierListAutoDDL = "GetAllSupplierListAutoDDL";

        /// <summary>
        /// The get maintenance group having technicians
        /// </summary>
        public const string GetMaintenanceGroupHavingTechniciansAutoDDL = "GetMaintenanceGroupHavingTechniciansAutoDDL";

        /// <summary>
        /// The WorkOrder Job Order Safety Instruction List
        /// </summary>
        public const string GetWorkOrderSafetyInstrListAutoDDL = "GetWorkOrderSafetyInstrListAutoDDL";

        /// <summary>
        /// The get sector list
        /// </summary>
        public const string GetSuppliersListAutoDDL = "GetSuppliersListAutoDDL";

        /// <summary>
        /// The get pm checklist
        /// </summary>
        public const string GetPMChecklistAutoDDL = "GetPMChecklistAutoDDL";

        public const string GetCleaningChecklistAutoDDL = "GetCleaningChecklistAutoDDL";

        /// <summary>
        /// The get safety instruction
        /// </summary>
        public const string GetSafetyInstructionAutoDDL = "GetSafetyInstructionAutoDDL";

        /// <summary>
        /// The get stock code item
        /// </summary>
        public const string GetStockCodeItemAutoDDL = "GetStockCodeItemAutoDDL";

        /// <summary>
        /// The get maintenance group
        /// </summary>
        public const string GetMaintenanceGroupAutoDDL = "GetMaintenanceGroupAutoDDL";

        /// <summary>
        /// The get bo m list
        /// </summary>
        public const string GetBoMListAutoDDL = "GetBoMListAutoDDL";

        /// <summary>
        /// The get assets for location
        /// </summary>
        public const string GetAssetsByLocationAutoDDL = "GetAssetsByLocationAutoDDL";

        /// <summary>
        /// The get assets by location and asset identifier
        /// </summary>
        public const string GetAssetsByLocationAndAssetIDAutoDDL = "GetAssetsByLocationAndAssetIDAutoDDL";

        /// <summary>
        /// The get assets by l2 identifier
        /// </summary>
        public const string GetAssetsByL2IDAutoDDL = "GetAssetsByL2IDAutoDDL";

        /// <summary>
        /// PM Meter Master List By City
        /// </summary>
        public const string GetPMMeterMasterListByCityAutoDDL = "GetPMMeterMasterListByCityAutoDDL";

        /// <summary>
        /// Gets the Items List Page
        /// </summary>
        public const string GetStoreItemsListAutoDDL = "GetStoreItemsListAutoDDL";

        /// <summary>
        /// The get employees
        /// </summary>
        public const string GetEmployeesAutoDDL = "GetEmployeesAutoDDL";

        /// <summary>
        /// The get account code for pop up
        /// </summary>
        public const string GetAccountCodeForPopUpAutoDDL = "GetAccountCodeForPopUpAutoDDL";

        /// <summary>
        /// The get currency for pop up
        /// </summary>
        public const string GetCurrencyForPopUpAutoDDL = "GetCurrencyForPopUpAutoDDL";

        /// <summary>
        /// Get Approved Projects
        /// </summary>
        public const string GetApprovedProjectListPageAutoDDL = "GetApprovedProjectListPageAutoDDL";

        /// <summary>
        /// The get employee maintenance group
        /// </summary>
        public const string GetEmployeeMaintenanceGroupAutoDDL = "GetEmployeeMaintenanceGroupAutoDDL";

        /// <summary>
        /// The WorkOrder Job Plan List
        /// </summary>
        public const string GetWorkOrderJOPlanListAutoDDL = "GetWorkOrderJOPlanListAutoDDL";

        /// <summary>
        /// The get employees by sub department identifier
        /// </summary>
        public const string GetEmployeesBySubDeptIDAutoDDL = "GetEmployeesBySubDeptIDAutoDDL";

        #endregion

        #region "Report"
        public const string RptNone = "None";
        public const string _ReportSearch = "_ReportSearch";
        public const string RptListFilter = "ListFilter";

        #region "AssetReport"
        public const string RptAssetList = "List";
        public const string RptAssetListFilter = "ListFilter";
        public const string RptAssetDetails = "AssetDetails";
        public const string RptTop10AssetDownTime = "Top10AssetDownTime";
        public const string RptTop10AssetDownTimeFilter = "Top10AssetDownTimeFilter";
        public const string RptTop10AssetBreakDown = "Top10AssetBreakDown";
        public const string RptTop10AssetBreakDownFilter = "Top10AssetBreakDownFilter";
        public const string RptAssetMTBF = "AssetMTBF";

        /// <summary>
        /// The _ job order type search
        /// </summary>
        public const string _AssetReportSearch = "_asset-report-search";

        public const string RptAssetMaintenanceHistoryList = "AssetMaintenanceHistoryList";
        public const string _AssetMaintenanceHistoryReportSearch = "_asset-maintenance-history-report-search";
        public const string RptAssetMaintenanceHistoryListFilter = "RptAssetMaintenanceHistoryListFilter";

        public const string RptAssetMaintenanceHistoryNoCostList = "AssetMaintenanceHistoryNoCostList";
        public const string _AssetMaintenanceHistoryNoCostReportSearch = "_asset-maintenance-history-no-cost-report-search";
        public const string RptAssetMaintenanceHistoryNoCostListFilter = "RptAssetMaintenanceHistoryNoCostListFilter";

        public const string _Top10AssetDownTimeSearch = "_top-10-asset-down-time-search";
        public const string _Top10AssetBreakDownSearch = "_top-10-asset-break-down-search";

        public const string RptAssetBreakdownFrequencyList = "AssetBreakdownFrequencyList";
        public const string _AssetBreakdownFrequencyReportSearch = "_asset-breakdown-frequency-report-search";
        public const string RptAssetBreakdownFrequencyListFilter = "RptAssetBreakdownFrequencyListFilter";

        public const string RptAssetBreakdownDowntimeList = "AssetBreakdownDowntimeList";
        public const string _AssetBreakdownDowntimeReportSearch = "_asset-breakdown-downtime-report-search";
        public const string RptAssetBreakdownDowntimeListFilter = "RptAssetBreakdownDowntimeListFilter";

        public const string RptManHoursByAssetList = "ManHoursByAssetList";
        public const string _ManHoursByAssetReportSearch = "_man-hours-by-asset-report-search";
        public const string RptManHoursByAssetListFilter = "RptManHoursByAssetListFilter";

        public const string RptAssetMTTRList = "AssetMTTRList";
        public const string _AssetMTTRReportSearch = "_asset-mttr-report-search";
        public const string RptAssetMTTRListFilter = "RptAssetMTTRListFilter";
        #endregion

        #region "Job Order Report"

        public const string _JobOrderListReportSearch = "_job-order-list-report-search";
        public const string RptJobOrderList = "JobOrderList";
        public const string RptJobOrderListFilter = "JobOrderListFilter";

        public const string _JobOrdersByEmployeesReportSearch = "_job-orders-by-employees-report-search";
        public const string RptJobOrdersByEmployees = "JobOrdersByEmployees";
        public const string RptJobOrdersByEmployeesFilter = "JobOrdersByEmployeesFilter";

        public const string _JobOrderRatingsReportSearch = "_job-order-ratings-report-search";
        public const string RptJobOrderRatings = "JobOrderRatings";
        public const string RptJobOrderRatingsFilter = "JobOrderRatingsFilter";

        public const string _JobOrderAgingReportSearch = "_job-order-aging-report-search";
        public const string RptJobOrderAging = "JobOrderAging";
        public const string RptJobOrderAgingFilter = "JobOrderAgingFilter";

        public const string _JobOrderCostByLocationChartReportSearch = "_job-order-cost-by-location-chart-report-search";
        public const string RptJobOrderCostByLocationChart = "JobOrderCostByLocationChart";
        public const string RptJobOrderCostByLocationChartFilter = "JobOrderCostByLocationChartFilter";

        public const string _JobOrderCostByJobTypeChartReportSearch = "_job-order-cost-by-job-type-chart-report-search";
        public const string RptJobOrderCostByJobTypeChart = "JobOrderCostByJobTypeChart";
        public const string RptJobOrderCostByJobTypeChartFilter = "JobOrderCostByJobTypeChartFilter";

        public const string _JobOrderCostByCostCenterReportSearch = "_job-order-cost-by-cost-center-report-search";
        public const string RptJobOrderCostByCostCenter = "JobOrderCostByCostCenter";
        public const string RptJobOrderCostByCostCenterFilter = "JobOrderCostByCostCenterFilter";

        public const string _JobOrderTypeDistributionChartReportSearch = "_job-order-type-distribution-chart-report-search";
        public const string RptJobOrderTypeDistributionChart = "JobOrderTypeDistributionChart";
        public const string RptJobOrderTypeDistributionChartFilter = "JobOrderTypeDistributionChartFilter";

        public const string _JobOrderManHoursByLocationChartReportSearch = "_job-order-man-hours-by-location-chart-report-search";
        public const string RptJobOrderManHoursByLocationChart = "JobOrderManHoursByLocationChart";
        public const string RptJobOrderManHoursByLocationChartFilter = "JobOrderManHoursByLocationChartFilter";

        public const string _JobOrderStatusStatisticsReportSearch = "_job-order-status-statistics-report-search";
        public const string RptJobOrderStatusStatistics = "JobOrderStatusStatistics";
        public const string RptJobOrderStatusStatisticsFilter = "JobOrderStatusStatisticsFilter";

        public const string _PMJobOrderAgingReportSearch = "_pm-job-order-aging-report-search";
        public const string RptPMJobOrderAging = "PMJobOrderAging";
        public const string RptPMJobOrderAgingFilter = "PMJobOrderAgingFilter";

        public const string _FailurePercentageAnalysisChartReportSearch = "_failure-percentage-analysis-chart-report-search";
        public const string RptFailurePercentageAnalysisChart = "FailurePercentageAnalysisChart";
        public const string RptFailurePercentageAnalysisChartFilter = "FailurePercentageAnalysisChartFilter";

        public const string _FailureDowntimeAnalysisChartReportSearch = "_failure-downtime-analysis-chart-report-search";
        public const string RptFailureDowntimeAnalysisChart = "FailureDowntimeAnalysisChart";
        public const string RptFailureDowntimeAnalysisChartFilter = "FailureDowntimeAnalysisChartFilter";

        public const string _PMJobOrderStatusStatisticsReportSearch = "_pm-job-order-status-statistics-report-search";
        public const string RptPMJobOrderStatusStatistics = "PMJobOrderStatusStatistics";
        public const string RptPMJobOrderStatusStatisticsFilter = "PMJobOrderStatusStatisticsFilter";

        public const string _PMJobOrderListReportSearch = "_pm-job-order-list-report-search";
        public const string RptPMJobOrderList = "PMJobOrderList";
        public const string RptPMJobOrderListFilter = "PMJobOrderListFilter";

        #endregion

        #region "Preventive Report"

        public const string _PMChecklistListReportSearch = "_pm-checklist-list-report-search";
        public const string RptPMChecklistList = "PMChecklistList";
        public const string RptPMChecklistListFilter = "PMChecklistListFilter";

        public const string _PMChecklistTasksReportSearch = "_pm-checklist-tasks-report-search";
        public const string RptPMChecklistTasks = "PMChecklistTasks";
        public const string RptPMChecklistTasksFilter = "PMChecklistTasksFilter";

        public const string _PMComplianceReportSearch = "_pm-compliance-report-search";
        public const string RptPMCompliance = "PMCompliance";
        public const string RptPMComplianceFilter = "PMComplianceFilter";

        public const string _PMMaterialRequirementsReportSearch = "_pm-material-requirements-report-search";
        public const string RptPMMaterialRequirements = "PMMaterialRequirements";
        public const string RptPMMaterialRequirementsFilter = "PMMaterialRequirementsFilter";

        public const string _PMAnualScheduleReportSearch = "_pm-anual-schedule-report-search";
        public const string RptPMAnualSchedule = "PMAnualSchedule";
        public const string RptPMAnualScheduleFilter = "PMAnualScheduleFilter";



        #endregion

        #region "Employee Report"

        public const string _EmployeeRequesterListReportSearch = "_employee-requester-list-report-search";
        public const string RptEmployeeRequesterList = "EmployeeRequesterList";
        public const string RptEmployeeRequesterListFilter = "EmployeeRequesterListFilter";

        public const string _EmployeeRequesterDetailsReportSearch = "_employee-requester-details-report-search";
        public const string RptEmployeeRequesterDetails = "EmployeeRequesterDetails";
        public const string RptEmployeeRequesterDetailsFilter = "EmployeeRequesterDetailsFilter";

        #endregion

        #region "Setups Report"

        public const string _LocationListReportSearch = "_location-list-report-search";
        public const string RptLocationList = "LocationList";
        public const string RptLocationListFilter = "LocationListFilter";

        public const string _SupplierListReportSearch = "_supplier-list-report-search";
        public const string RptSupplierList = "SupplierList";
        public const string RptSupplierListFilter = "SupplierListFilter";

        #endregion

        #region "Cleaning Report"

        public const string _CleaningInspectionSummaryReportSearch = "_cleaning-inspection-summary-report-search";
        public const string RptCleaningInspectionSummary = "CleaningInspectionSummary";
        public const string RptCleaningInspectionSummaryFilter = "CleaningInspectionSummaryFilter";

        public const string _CleaningInspectionDetailListReportSearch = "_cleaning-inspection-detail-list-report-search";
        public const string RptCleaningInspectionDetailList = "CleaningInspectionDetailList";
        public const string RptCleaningInspectionDetailListFilter = "CleaningInspectionDetailListFilter";

        #endregion

        #region "Purchasing Report"

        public const string _PurchaseRequestListReportSearch = "_purchase-request-list-report-search";
        public const string RptPurchaseRequestList = "PurchaseRequestList";
        public const string RptPurchaseRequestListFilter = "PurchaseRequestListFilter";

        public const string _PurchaseRequestDetailsListReportSearch = "_purchase-request-details-list-report-search";
        public const string RptPurchaseRequestDetailsList = "PurchaseRequestDetailsList";
        public const string RptPurchaseRequestDetailsListFilter = "PurchaseRequestDetailsListFilter";

        public const string _PurchaseOrderListReportSearch = "_purchase-order-list-report-search";
        public const string RptPurchaseOrderList = "PurchaseOrderList";
        public const string RptPurchaseOrderListFilter = "PurchaseOrderListFilter";

        public const string _PurchaseOrderDetailsListReportSearch = "_purchase-order-details-list-report-search";
        public const string RptPurchaseOrderDetailsList = "PurchaseOrderDetailsList";
        public const string RptPurchaseOrderDetailsListFilter = "PurchaseOrderDetailsListFilter";

        public const string _PurchaseOrderCostReportSearch = "_purchase-order-cost-report-search";
        public const string RptPurchaseOrderCost = "PurchaseOrderCost";
        public const string RptPurchaseOrderCostFilter = "PurchaseOrderCostFilter";

        #endregion

        #region "Store Report"

        public const string _ItemListReportSearch = "_item-list-report-search";
        public const string RptItemList = "ItemList";

        public const string _StockTakeListReportSearch = "_stock-take-list-report-search";
        public const string RptStockTakeList = "StockTakeList";

        public const string _StockBalanceListReportSearch = "_stock-balance-list-report-search";
        public const string RptStockBalanceList = "StockBalanceList";

        public const string _TransactionListReportSearch = "_transaction-list-report-search";
        public const string RptTransactionList = "TransactionList";

        public const string _PurchaseProposalListNormalReportSearch = "_purchase-proposal-list-normal-report-search";
        public const string RptPurchaseProposalListNormal = "PurchaseProposalListNormal";

        public const string _PurchaseProposalListHighReportSearch = "_purchase-proposal-list-high-report-search";
        public const string RptPurchaseProposalListHigh = "PurchaseProposalListHigh";

        public const string _PartReceiveQtyListReportSearch = "_part-receive-qty-list-report-search";
        public const string RptPartReceiveQtyList = "PartReceiveQtyList";

        public const string _PartIssueQtyListReportSearch = "_part-issue-qty-list-report-search";
        public const string RptPartIssueQtyList = "PartIssueQtyList";

        public const string _PartReceiveTotalCostListReportSearch = "_part-receive-total-cost-list-report-search";
        public const string RptPartReceiveTotalCostList = "PartReceiveTotalCostList";

        public const string _PartIssueTotalCostListReportSearch = "_part-issue-total-cost-list-report-search";
        public const string RptPartIssueTotalCostList = "PartIssueTotalCostList";

        public const string _DormantPartsReportSearch = "_dormant-parts-report-search";
        public const string RptDormantParts = "DormantParts";

        public const string _ItemIssueByJobOrderReportSearch = "_item-issue-by-job-order-report-search";
        public const string RptItemIssueByJobOrder = "ItemIssueByJobOrder";

        #endregion

        #endregion

        #region SetupReport

        public const string ManageOrganizationDetails = "edit-organization-details";

        /// <summary>
        /// To Change Moda Photo
        /// </summary>
        public const string ChangeModaPhoto = "ChangeModaLogo";


        /// <summary>
        /// To Change Branch Photo
        /// </summary>
        public const string ChangeBranchPhoto = "ChangeBranchLogo";

        public const string SaveOrganizationDetails = "SaveOrganizationDetails";

        #endregion
    }
}
