﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Pages
{
    /// <summary>
    ///  Declare Actions PartialViews
    /// </summary>
    public class PartialViews
    {
        /// <summary>
        /// The header
        /// </summary>
        public const string Header = "_header";

        /// <summary>
        /// The footer
        /// </summary>
        public const string Footer = "_footer";

        /// <summary>
        /// The left menu
        /// </summary>
        public const string LeftMenu = "_leftmenu";

        /// <summary>
        /// The bread crumb
        /// </summary>
        public const string BreadCrumb = "_breadcrumb";

        /// <summary>
        /// The edit maintenance group
        /// </summary>
        public const string EditMaintenanceGroup = "_editmaintenancegroup";

        /// <summary>
        /// The advanced search
        /// </summary>
        public const string AdvancedSearch = "_advancedsearch";

        /// <summary>
        /// The edit suppliers
        /// </summary>
        public const string EditSuppliers = "_editsuppliers";

        /// <summary>
        /// The supplier documents
        /// </summary>
        public const string SupplierDocuments = "_SupplierDocuments";

        /// <summary>
        /// The Employee Documents Tab
        /// </summary>
        public const string EmployeeDocuments = "_employeedocuments";

        /// <summary>
        /// The Employee Login Tab
        /// </summary>
        public const string EmployeeLogin = "_Employeelogin";

        /// <summary>
        /// The Employee City Permission Tab
        /// </summary>
        public const string EmployeeCityPermission = "_EmployeeCityPermission";

        /// <summary>
        /// The Employee Level City Permission Tab
        /// </summary>
        public const string EmployeeLevelWiseCityPermission = "_EmployeeLevelWiseCityPermission";

        /// <summary>
        /// The Employee Department Permission Tab
        /// </summary>
        public const string EmployeeDeptPermission = "_EmployeeDeptPermission";

        /// <summary>
        /// The Employee Location Permission Tab
        /// </summary>
        public const string EmployeeLocationPermission = "_EmployeeLocationPermission";

        /// <summary>
        /// The Permission Tab
        /// </summary>
        public const string Permissions = "_Permissions";

        /// <summary>
        /// The edit item requisition work flow
        /// </summary>
        public const string EditIRWorkFlow = "_editirworkflow";

        /// <summary>
        /// The asset relationship
        /// </summary>
        public const string AssetRelationship = "_assetrelationship";

        /// <summary>
        /// The job order history
        /// </summary>
        public const string JobOrderHistory = "_joborderhistory";

        /// <summary>
        /// The pm meter
        /// </summary>
        public const string PMMeter = "_pmmeter";

        /// <summary>
        /// The pm meter location
        /// </summary>
        public const string PMMeterLocation = "_pmmeterlocation";

        /// <summary>
        /// The edit job plan
        /// </summary>
        public const string EditJobPlan = "_editjobplan";

        /// <summary>
        /// The edit safety instruction
        /// </summary>
        public const string EditSafetyInstruction = "_editsafetyinstruction";

        /// <summary>
        /// The edit purchase requisition work flow
        /// </summary>
        public const string EditPRWorkFlow = "_editprworkflow";

        /// <summary>
        /// The un translated word list
        /// </summary>
        public const string UnTranslatedWordList = "_untranslatedwordlist";

        /// <summary>
        /// The edit account code
        /// </summary>
        public const string EditAccountCode = "_editaccountcode";

        /// <summary>
        /// The edit bo m list
        /// </summary>
        public const string EditBoMList = "_editbomlist";

        /// <summary>
        /// The edit Email Templates
        /// </summary>
        public const string EditEmailTemplates = "_editemailtemplates";

        /// <summary>
        /// The edit Email Rules
        /// </summary>
        public const string EditEmailRules = "_editemailrules";

        /// <summary>
        /// The _ document list
        /// </summary>
        public const string _DocumentList = "_documentlist";

        /// <summary>
        /// The _ purchase order document list
        /// </summary>
        public const string _PurchaseOrderDocumentList = "_PurchaseOrderDocumentList";

        /// <summary>
        /// The _ Section list
        /// </summary>
        public const string _EditSection = "_editsection";

        /// <summary>
        /// The _ multiple check list
        /// </summary>
        public const string _MultipleCheckList = "_multiplechecklist";

        /// <summary>
        /// The _ pm by asset multiple check list
        /// </summary>
        public const string _PMByAssetMultipleCheckList = "_pmbyassetmultiplechecklist";

        /// <summary>
        /// The _ pm assign to
        /// </summary>
        public const string _PMAssignTo = "_pmassignto";

        /// <summary>
        /// The _ pm meter assign to
        /// </summary>
        public const string _PMMeterAssignTo = "_pmmeterassignto";

        /// <summary>
        /// The _ pm by asset assign to
        /// </summary>
        public const string _PMByAssetAssignTo = "_pmbyassetassignto";

        /// <summary>
        /// The Purchase Order Request  Tab
        /// </summary>
        public const string PurchaseOrderRequest = "_PurchaseOrderRequest";

        /// <summary>
        /// The chart search filter
        /// </summary>
        public const string ChartSearchFilter = "_chartsearchfilter";

        /// <summary>
        /// The direct issue Tab
        /// </summary>
        public const string DirectIssueList = "_DirectIssueList";

        /// <summary>
        /// Add/Edit Direct Issue pop pup
        /// </summary>
        public const string AddEditDirectIssue = "_AddEditDirectIssue";

        /// <summary>
        /// Add/Edit Assign To For WorkFlow
        /// </summary>
        public const string WOAssignTO = "_WOAssignTO";

        /// <summary>
        /// Add/Edit JOClosing
        /// </summary>
        public const string JOClosing = "_JOClosing";

        /// <summary>
        /// Add/Edit JOLabor
        /// </summary>
        public const string JOLabor = "_JOLabor";

        /// <summary>
        /// Add/Edit Jo Cost
        /// </summary>
        public const string JOCost = "_JOCost";

        /// <summary>
        /// Add/Edit Jo PMCheck List
        /// </summary>
        public const string JOPMCheckList = "_JOPMCheckList";

        /// <summary>
        /// Add/Edit JOItems
        /// </summary>
        public const string JOItems = "_JOItems";

        /// <summary>
        /// Add/Edit JOTools
        /// </summary>
        public const string JOTools = "_JOTools";

        /// <summary>
        /// Add/Edit JOIssues
        /// </summary>
        public const string JOIssues = "_JOIssues";

        /// <summary>
        /// Add/Edit JOReturns
        /// </summary>
        public const string JOReturns = "_JOReturns";

        /// <summary>
        /// Add/Edit Jo Notes
        /// </summary>
        public const string JONotes = "_JONotes";

        /// <summary>
        /// Add/Edit Jo Notes
        /// </summary>
        public const string JOPlan = "_JOPlan";

        /// <summary>
        /// Add/Edit Jo Safety instructions
        /// </summary>
        public const string JOSafetyInstructions = "_JOSafetyInstructions";

        /// <summary>
        /// Add/Edit JOIR
        /// </summary>
        public const string JOIR = "_JOIR";

        /// <summary>
        /// The add widget
        /// </summary>
        public const string AddWidget = "_addwidget";

        /// <summary>
        /// The job order status by site search
        /// </summary>
        public const string JobOrderStatusBySiteSearch = "_joborderstatusbysitesearch";

        /// <summary>
        /// The job order type search
        /// </summary>
        public const string JobOrderTypeSearch = "_jobordertypesearch";

        /// <summary>
        /// The top ten asset search
        /// </summary>
        public const string TopTenAssetSearch = "_toptenassetsearch";

        /// <summary>
        /// The pm compliance search
        /// </summary>
        public const string PMComplianceSearch = "_pmcompliancesearch";

        /// <summary>
        /// The job trading search
        /// </summary>
        public const string JobTradingSearch = "_jobtradingsearch";

        /// <summary>
        /// The work order status
        /// </summary>
        public const string WorkOrderStatus = "_workorderstatus";

        /// <summary>
        /// The work request status
        /// </summary>
        public const string WorkRequestStatus = "_workrequeststatus";

        /// <summary>
        /// The work status report search
        /// </summary>
        public const string WorkStatusReportSearch = "_workstatusreportsearch";

        /// <summary>
        /// The _ location document list
        /// </summary>
        public const string _LocationDocumentList = "_locationdocumentlist";

        /// <summary>
        /// The _ view location diagram
        /// </summary>
        public const string _ViewLocationDiagram = "_viewlocationdiagram";

        /// <summary>
        /// The jo status track tab
        /// </summary>
        public const string _JOStatusTrackTab = "_jostatustracktab";

        /// <summary>
        /// The Cleaning List tab
        /// </summary>
        public const string _CleaningList = "_CleaningList";

        /// <summary>
        /// The store card return popup
        /// </summary>
        public const string StoreCardReturnPopup = "_storecardreturnpopup";

        /// <summary>
        /// Add/Edit Alternative Items 
        /// </summary>
        public const string AlterNativeItems = "_AlterNativeItems";

        /// <summary>
        /// Add/Edit Alternative Suppliers 
        /// </summary>
        public const string AlterNativeSuppliers = "_AlterNativeSuppliers";

        /// <summary>
        /// The store card transfer popup
        /// </summary>
        public const string StoreCardTransferPopup = "_storecardtransferpopup";

        /// <summary>
        /// The purchase order status
        /// </summary>
        public const string PurchaseOrderStatus = "_purchaseorderstatus";

        /// <summary>
        /// The purchase request status
        /// </summary>
        public const string PurchaseRequestStatus = "_purchaserequeststatus";

        /// <summary>
        /// The error log
        /// </summary>
        public const string ErrorLogTrace = "_errorlogtrace";

        /// <summary>
        /// load Help Details
        /// </summary>
        public const string _HelpDetailTemplate = "_HelpDetailTemplate";

        /// <summary>
        /// load Help tree
        /// </summary>
        public const string _HelpTree = "_HelpTree";
		
		public const string ReportSearch = "_ReportSearch";

        /// <summary>
        /// The edit working day hours
        /// </summary>
        public const string EditWorkingDayHours = "_editworkingdayhours";

        /// <summary>
        /// The edit holiday
        /// </summary>
        public const string EditHoliday = "_editholiday";

        /// <summary>
        /// The Asset Report search
        /// </summary>
        public const string AssetReportSearch = "_AssetReportSearch";
        /// <summary>
        /// The Linked Locations
        /// </summary>
        public const string LinkedLocations = "_LinkedLocations";

        /// <summary>
        /// The Issue Item Partial View
        /// </summary>
        public const string IssueItemFromIR = "_IssueItemPopUp";

        public const string AssetMaintenanceHistoryReportSearch = "_AssetMaintenanceHistoryReportSearch";
        public const string AssetMaintenanceHistoryNoCostReportSearch = "_AssetMaintenanceHistoryNoCostReportSearch";
        public const string Top10AssetDownTimeSearch = "_Top10AssetDownTimeSearch";
        public const string Top10AssetBreakDownSearch = "_Top10AssetBreakDownSearch";
        public const string JobOrderAgingReportSearch = "_JobOrderAgingReportSearch";
        public const string PMJobOrderAgingReportSearch = "_PMJobOrderAgingReportSearch";
        public const string JobOrderListReportSearch = "_JobOrderListReportSearch";
        public const string JobOrdersByEmployeesReportSearch = "_JobOrdersByEmployeesReportSearch";
        public const string JobOrderRatingsReportSearch = "_JobOrderRatingsReportSearch";
        public const string PMJobOrderListReportSearch = "_PMJobOrderListReportSearch";
        public const string JobOrderStatusStatisticsReportSearch = "_JobOrderStatusStatisticsReportSearch";
        public const string FailurePercentageAnalysisChartReportSearch = "_FailurePercentageAnalysisChartReportSearch";
        public const string FailureDowntimeAnalysisChartReportSearch = "_FailureDowntimeAnalysisChartReportSearch";
        public const string PMJobOrderStatusStatisticsReportSearch = "_PMJobOrderStatusStatisticsReportSearch";
        public const string JobOrderCostByLocationChartReportSearch = "_JobOrderCostByLocationChartReportSearch";
        public const string JobOrderCostByJobTypeChartReportSearch = "_JobOrderCostByJobTypeChartReportSearch";
        public const string JobOrderCostByCostCenterReportSearch = "_JobOrderCostByCostCenterReportSearch";
        public const string JobOrderTypeDistributionChartReportSearch = "_JobOrderTypeDistributionChartReportSearch";
        public const string JobOrderManHoursByLocationChartReportSearch = "_JobOrderManHoursByLocationChartReportSearch";
        public const string AssetBreakdownFrequencyReportSearch = "_AssetBreakdownFrequencyReportSearch";
        public const string AssetBreakdownDowntimeReportSearch = "_AssetBreakdownDowntimeReportSearch";
        public const string ManHoursByAssetReportSearch = "_ManHoursByAssetReportSearch";
        public const string AssetMTTRReportSearch = "_AssetMTTRReportSearch";
        public const string PMChecklistListReportSearch = "_PMChecklistListReportSearch";
        public const string PMComplianceReportSearch = "_PMComplianceReportSearch";
        public const string PMMaterialRequirementsReportSearch = "_PMMaterialRequirementsReportSearch";
        public const string EmployeeRequesterListReportSearch = "_EmployeeRequesterListReportSearch";
        public const string EmployeeRequesterDetailsReportSearch = "_EmployeeRequesterDetailsReportSearch";
        public const string CleaningInspectionSummaryReportSearch = "_CleaningInspectionSummaryReportSearch";
        public const string CleaningInspectionDetailListReportSearch = "_CleaningInspectionDetailListReportSearch";
        public const string LocationListReportSearch = "_LocationListReportSearch";
        public const string PurchaseRequestListReportSearch = "_PurchaseRequestListReportSearch";
        public const string PurchaseRequestDetailsListReportSearch = "_PurchaseRequestDetailsListReportSearch";
        public const string PurchaseOrderListReportSearch = "_PurchaseOrderListReportSearch";
        public const string PurchaseOrderDetailsListReportSearch = "_PurchaseOrderDetailsListReportSearch";
        public const string PurchaseOrderCostReportSearch = "_PurchaseOrderCostReportSearch";
        public const string PMAnualScheduleReportSearch = "_PMAnualScheduleReportSearch";
    }
}