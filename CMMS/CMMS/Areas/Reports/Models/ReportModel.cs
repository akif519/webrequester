﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.XtraReports.UI;

namespace CMMS.Areas.Reports.Models
{
    public class ReportModel
    {
        /// <summary>
        /// Report Id
        /// </summary>
        public string ReportID { get; set; }

        /// <summary>
        /// Report XtraReport
        /// </summary>
        public XtraReport Report { get; set; }
        // public MobileEmulatorModel EmulatorModel { get; set; }
        /// <summary>
        /// Current Viewer
        /// </summary>
        public string CurrentViewer { get; set; }

        /// <summary>
        /// Is ASPViewer
        /// </summary>
        //public bool IsASPViewer
        //{
        //    get
        //    {
        //        return CurrentViewer == ViewerSelectorState.ClassicViewer;
        //    }
        //}

        /// <summary>
        /// IS HTML 5 Viewer
        /// </summary>
        public bool IsHTML5Viewer
        {
            get
            {
                return CurrentViewer == null;
            }
        }

        /// <summary>
        /// Mobile viewer
        /// </summary>
        //public bool IsMobileViewer
        //{
        //    get
        //    {
        //        return CurrentViewer == ViewerSelectorState.MobileViewer;

        //    }
        //}
    }
}