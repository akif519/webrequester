﻿using System.Web.Mvc;

namespace CMMS.Areas.Reports
{
    /// <summary>
    /// Report area registration 
    /// </summary>
    public class ReportsAreaRegistration : AreaRegistration 
    {
        /// <summary>
        /// Area name
        /// </summary>
        public override string AreaName 
        {
            get 
            {
                return Pages.Areas.Report;
            }
        }

        /// <summary>
        /// Register Area
        /// </summary>
        /// <param name="context">Area Registration Context</param>
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
            name: "Report_default",
            url: Pages.Areas.Report + "/{action}",
            defaults: new { controller = Pages.Controllers.ReportAsset , action = "Index" });

            context.MapRoute(
                "Reports_default",
                "Reports/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });
        }
    }
}