﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Reports;
using CMMS.Model;
using CMMS.Services;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service.DashboardService;
using CMMS.Service.ReportService;
using CMMS.Reports.Asset;

namespace CMMS.Areas.Reports.Controllers
{
    public class StoreReportController : BaseController
    {
        // GET: Reports/JobOrder
        public ActionResult Index()
        {
            return View();
        }

        #region Item List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptItemList)]
        public ActionResult ItemList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptItemList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                FilterHelper.AddSearchFilterInDictionary(Actions.RptItemList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            ItemList rpt = new ItemList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockItemList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["itemTypeId"].Value = objReportSearchModel.ItemTypeId;
            rpt.Parameters["itemSubTypeId"].Value = objReportSearchModel.ItemSubTypeId;
            rpt.Parameters["itemStatusId"].Value = objReportSearchModel.ItemStatusId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Stock Take List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptStockTakeList)]
        public ActionResult StockTakeList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptStockTakeList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptStockTakeList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            StockTakeList rpt = new StockTakeList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockStockTakeList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["subStoreId"].Value = objReportSearchModel.SubStoreId;
            rpt.Parameters["itemTypeId"].Value = objReportSearchModel.ItemTypeId;
            rpt.Parameters["itemSubTypeId"].Value = objReportSearchModel.ItemSubTypeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Stock Balance List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptStockBalanceList)]
        public ActionResult StockBalanceList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptStockBalanceList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptStockBalanceList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            StockBalanceList rpt = new StockBalanceList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockStockBalanceList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["subStoreId"].Value = objReportSearchModel.SubStoreId;
            rpt.Parameters["itemTypeId"].Value = objReportSearchModel.ItemTypeId;
            rpt.Parameters["itemSubTypeId"].Value = objReportSearchModel.ItemSubTypeId;
            rpt.Parameters["transactionDate"].Value = objReportSearchModel.TransactionDate;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Transaction List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptTransactionList)]
        public ActionResult TransactionList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptTransactionList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptTransactionList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            TransactionList rpt = new TransactionList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockTransactionList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["subStoreId"].Value = objReportSearchModel.SubStoreId;
            rpt.Parameters["transactionDate"].Value = objReportSearchModel.TransactionDate;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Purchase Proposal List Normal Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPurchaseProposalListNormal)]
        public ActionResult PurchaseProposalListNormal()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPurchaseProposalListNormal);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPurchaseProposalListNormal, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PurchaseProposalListNormal rpt = new PurchaseProposalListNormal();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPPNormalpriority;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["subStoreId"].Value = objReportSearchModel.SubStoreId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Purchase Proposal List High Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPurchaseProposalListHigh)]
        public ActionResult PurchaseProposalListHigh()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPurchaseProposalListHigh);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPurchaseProposalListHigh, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PurchaseProposalListHigh rpt = new PurchaseProposalListHigh();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPPHighpriority;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["subStoreId"].Value = objReportSearchModel.SubStoreId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Part Receive Qty List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPartReceiveQtyList)]
        public ActionResult PartReceiveQtyList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPartReceiveQtyList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.Month = DateTime.Now.Month;
                objReportSearchModel.Year = DateTime.Now.Year;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPartReceiveQtyList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PartReceiveQtyList rpt = new PartReceiveQtyList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPartReceiveQtyList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["month"].Value = objReportSearchModel.Month;
            rpt.Parameters["year"].Value = objReportSearchModel.Year;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Part Issue Qty List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPartIssueQtyList)]
        public ActionResult PartIssueQtyList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPartIssueQtyList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.Month = DateTime.Now.Month;
                objReportSearchModel.Year = DateTime.Now.Year;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPartIssueQtyList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PartIssueQtyList rpt = new PartIssueQtyList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPartIssueQtyList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["month"].Value = objReportSearchModel.Month;
            rpt.Parameters["year"].Value = objReportSearchModel.Year;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Part Receive Total Cost List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPartReceiveTotalCostList)]
        public ActionResult PartReceiveTotalCostList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPartReceiveTotalCostList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.Month = DateTime.Now.Month;
                objReportSearchModel.Year = DateTime.Now.Year;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPartReceiveTotalCostList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PartReceiveTotalCostList rpt = new PartReceiveTotalCostList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPartReceiveTotalCostList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["month"].Value = objReportSearchModel.Month;
            rpt.Parameters["year"].Value = objReportSearchModel.Year;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Part Issue Total Cost List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPartIssueTotalCostList)]
        public ActionResult PartIssueTotalCostList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPartIssueTotalCostList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.Month = DateTime.Now.Month;
                objReportSearchModel.Year = DateTime.Now.Year;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPartIssueTotalCostList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PartIssueTotalCostList rpt = new PartIssueTotalCostList();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockPartIssueTotalCostList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["month"].Value = objReportSearchModel.Month;
            rpt.Parameters["year"].Value = objReportSearchModel.Year;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Dormant parts Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptDormantParts)]
        public ActionResult DormantParts()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptDormantParts);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.Aging = 30;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptDormantParts, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            DormantParts rpt = new DormantParts();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockDormantParts;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["days"].Value = objReportSearchModel.Aging;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Item Issue By Job Order Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptItemIssueByJobOrder)]
        public ActionResult ItemIssueByJobOrder()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptItemIssueByJobOrder);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                FilterHelper.AddSearchFilterInDictionary(Actions.RptItemIssueByJobOrder, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            ItemIssueByJobOrder rpt = new ItemIssueByJobOrder();

            rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Store_TextBlockIssue;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["JobOrderNo"].Value = objReportSearchModel.JobOrderNo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion
    }
}