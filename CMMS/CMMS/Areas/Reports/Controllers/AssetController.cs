﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Reports;
using CMMS.Model;
using CMMS.Services;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service.DashboardService;
using CMMS.Service.ReportService;
using CMMS.Reports.Asset;

namespace CMMS.Areas.Reports.Controllers
{
    public class AssetController : BaseController
    {
        // GET: Reports/Asset
        public ActionResult Index()
        {
            return View();
        }

        #region Asset Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetList)]
        public ActionResult List()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            AssetList rpt = new AssetList();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetList;

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            #region "Filter Text & Value " 
            //rpt.FindControl("lblSiteCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblSiteCodeValue", false).Text = "";
            //rpt.FindControl("lblBuilding", false).Text = ProjectSession.Resources.label.Building_TextBlockBuildingCode;
            //rpt.FindControl("buildingvalue", false).Text = "";
            //rpt.FindControl("lblStreetCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("StreetCodeValue", false).Text = "";
            //rpt.FindControl("lblAreaCode", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("AreaCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationNo", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationNoValue", false).Text = "";
            #endregion

            #region "Colum header"
            rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            if (lang)
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            else
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
                DevExpress.XtraReports.UI.XRSubreport subreport = (DevExpress.XtraReports.UI.XRSubreport)rpt.FindControl("xrSubreport1", false);
                subreport.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.label.Asset_TextBlockAssetList;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["reportBy"].Value = ProjectSession.EmployeeName;
            rpt.Parameters["dateRange"].Value = Common.GetDateRangeText(objReportSearchModel.DateFrom, objReportSearchModel.DateTo);

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset Details

        [ActionName(Actions.RptAssetDetails)]
        public ActionResult AssetDetails()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetDetails);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetDetails, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            AssetDetails rpt = new AssetDetails();
            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Report_TextBlockAssetDetails;

            #region "Header Text & Value " 

            #endregion

            #region "Colum header"

            rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            rpt.FindControl("lblArAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.JobRequest_TextBlockCityCode;
            rpt.FindControl("lblLocationNo", false).Text = ProjectSession.Resources.label.JobRequest_TextBlockLocationNo;
            rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Building_TextBlockBuildingCode;
            rpt.FindControl("lblAssetCategory", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCategory;
            rpt.FindControl("lblAssetStatus", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetStatus;
            rpt.FindControl("lblAssetCritcality", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCriticality;
            rpt.FindControl("lblTrade", false).Text = ProjectSession.Resources.label.Employee_TextBlockTrade;
            rpt.FindControl("lblAuthEmp", false).Text = ProjectSession.Resources.label.Employee_TextBlockAuthEmployee;
            rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            rpt.FindControl("lblManufactuer", false).Text = ProjectSession.Resources.label.Asset_TextBlockManufacturer;
            rpt.FindControl("lblContractor", false).Text = ProjectSession.Resources.label.Asset_TextBlockContractor;
            rpt.FindControl("lblWarranty", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetWarrantyContract;
            rpt.FindControl("lblWarrantyExpiryDate", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetWarrantyExpiryDate;

            rpt.FindControl("lblWarrantyStatus", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetWarrantyExpiryStatus;
            rpt.FindControl("lblContractNotes", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetWarrantyContractNotes;
            rpt.FindControl("lblAssetNotes", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNotes;
            rpt.FindControl("lblSupplier", false).Text = ProjectSession.Resources.label.Supplier_TextBlockSupplier;

            rpt.FindControl("lblPurchasePrice", false).Text = ProjectSession.Resources.label.Asset_TextBlockPurchasePrice;
            rpt.FindControl("lblDateCommissioned", false).Text = ProjectSession.Resources.label.Asset_TextBlockDateCommissioned;
            rpt.FindControl("lblDateDisposed", false).Text = ProjectSession.Resources.label.Asset_TextBlockDateDisposed;
            rpt.FindControl("lblEstimatedLife", false).Text = ProjectSession.Resources.label.Asset_TextBlockEstimatedLife;
            rpt.FindControl("lblCurrentValue", false).Text = ProjectSession.Resources.label.Asset_TextBlockCurrentValue;
            rpt.FindControl("lblNotesToTechnician", false).Text = ProjectSession.Resources.label.Asset_TextBlockNotesToTechnician;

            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["header"].Value = ProjectSession.Resources.label.Report_TextBlockAssetDetails;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset Maintenance History Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetMaintenanceHistoryList)]
        public ActionResult AssetMaintenanceHistoryList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetMaintenanceHistoryList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetMaintenanceHistoryList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            AssetMaintenanceHistory rpt = new AssetMaintenanceHistory();

            //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetMaintenanceHistoryList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuAssetMaintenanceHistory;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["assetId"].Value = objReportSearchModel.AssetId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Top 10 Asset Down Time Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptTop10AssetDownTime)]
        public ActionResult Top10AssetDownTime()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptTop10AssetDownTime);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptTop10AssetDownTime, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            Top10AssetDownTime rpt = new Top10AssetDownTime();

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.menu.Reports_MenuTop10AssetDowntime;

            #region "Filter Text & Value " 
            //rpt.FindControl("lblSiteCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblSiteCodeValue", false).Text = "";
            //rpt.FindControl("lblBuilding", false).Text = ProjectSession.Resources.label.Building_TextBlockBuildingCode;
            //rpt.FindControl("buildingvalue", false).Text = "";
            //rpt.FindControl("lblStreetCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("StreetCodeValue", false).Text = "";
            //rpt.FindControl("lblAreaCode", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("AreaCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationNo", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationNoValue", false).Text = "";
            #endregion

            #region "Colum header"
            rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockRank;
            rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            if (lang)
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            else
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            rpt.FindControl("lblL4no", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            rpt.FindControl("lblDownTime", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockDowntime;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuTop10AssetDowntime;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["reportBy"].Value = ProjectSession.EmployeeName;
            rpt.Parameters["dateRange"].Value = Common.GetDateRangeText(objReportSearchModel.DateFrom, objReportSearchModel.DateTo);

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Top 10 Asset Break Down Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptTop10AssetBreakDown)]
        public ActionResult Top10AssetBreakDown()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptTop10AssetBreakDown);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptTop10AssetBreakDown, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            Top10AssetBreakDown rpt = new Top10AssetBreakDown();

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.menu.Reports_MenuTop10AssetBreakdowns;

            #region "Filter Text & Value " 
            //rpt.FindControl("lblSiteCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblSiteCodeValue", false).Text = "";
            //rpt.FindControl("lblBuilding", false).Text = ProjectSession.Resources.label.Building_TextBlockBuildingCode;
            //rpt.FindControl("buildingvalue", false).Text = "";
            //rpt.FindControl("lblStreetCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("StreetCodeValue", false).Text = "";
            //rpt.FindControl("lblAreaCode", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("AreaCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationNo", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationNoValue", false).Text = "";
            #endregion

            #region "Colum header"
            rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockRank;
            rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            if (lang)
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            else
                rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            rpt.FindControl("lblL4no", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            rpt.FindControl("lblDownTime", false).Text = ProjectSession.Resources.label.PM_TextBlockFrequency;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuTop10AssetBreakdowns;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset MTBF Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetMTBF)]
        public ActionResult AssetMTBF()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetMTBF);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetMTBF, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            AssetMTBF rpt = new AssetMTBF();

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.menu.Reports_MenuAssetsMTBF;

            //#region "Filter Text & Value " 
            //rpt.FindControl("lblSiteCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblSiteCodeValue", false).Text = "";
            //rpt.FindControl("lblBuilding", false).Text = ProjectSession.Resources.label.Building_TextBlockBuildingCode;
            //rpt.FindControl("buildingvalue", false).Text = "";
            //rpt.FindControl("lblStreetCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("StreetCodeValue", false).Text = "";
            //rpt.FindControl("lblAreaCode", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("AreaCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationNo", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationNoValue", false).Text = "";
            //#endregion

            //#region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockRank;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblL4no", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblDownTime", false).Text = ProjectSession.Resources.label.PM_TextBlockFrequency;
            //#endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuAssetsMTBF;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset Breakdown Frequency Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetBreakdownFrequencyList)]
        public ActionResult AssetBreakdownFrequencyList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetBreakdownFrequencyList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetBreakdownFrequencyList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            AssetBreakDownFrequency rpt = new AssetBreakDownFrequency();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.menu.Reports_MenuAssetBreakdownFrequency;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuAssetBreakdownFrequency;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset Breakdown Downtime Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetBreakdownDowntimeList)]
        public ActionResult AssetBreakdownDowntimeList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetBreakdownDowntimeList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetBreakdownDowntimeList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            AssetBreakdownDowntime rpt = new AssetBreakdownDowntime();

            //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.menu.Reports_MenuAssetBreakdownDowntime;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Report_MenuAssetBreakdownDowntime;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Man Hours By Asset Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptManHoursByAssetList)]
        public ActionResult ManHoursByAssetList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptManHoursByAssetList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptManHoursByAssetList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            ManHoursByAsset rpt = new ManHoursByAsset();

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockManHoursByAsset;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuManHoursByAsset;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;


            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset MTTR Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetMTTRList)]
        public ActionResult AssetMTTRList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetMTTRList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetMTTRList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            bool lang = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? false : true;

            AssetMTTR rpt = new AssetMTTR();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetMTTR;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = @ProjectSession.Resources.menu.Reports_MenuAssetMTTR;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Asset Maintenance History No Cost Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptAssetMaintenanceHistoryNoCostList)]
        public ActionResult AssetMaintenanceHistoryNoCostList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptAssetMaintenanceHistoryNoCostList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptAssetMaintenanceHistoryNoCostList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            AssetMaintenanceHistoryNoCost rpt = new AssetMaintenanceHistoryNoCost();

            // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetMaintenanceHistoryNoCostList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuAssetMaintHistorynocost;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["assetId"].Value = objReportSearchModel.AssetId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        public ActionResult ExportReportViewer()
        {
            return DevExpress.Web.Mvc.ReportViewerExtension.ExportTo(new AssetList());
        }
    }
}