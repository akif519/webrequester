﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Reports;
using CMMS.Model;
using CMMS.Services;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service.DashboardService;
using CMMS.Service.ReportService;
using CMMS.Reports.Asset;

namespace CMMS.Areas.Reports.Controllers
{
    public class JobOrderReportController : BaseController
    {
        // GET: Reports/JobOrder
        public ActionResult Index()
        {
            return View();
        }

        #region Job Order List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderList)]
        public ActionResult JobOrderList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderList rpt = new JobOrderList();

          //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.JobOrder_MenuJobOrderList;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;
            rpt.Parameters["workTradeId"].Value = objReportSearchModel.WorkTradeId;
            rpt.Parameters["workPriorityId"].Value = objReportSearchModel.WorkPriorityId;
            rpt.Parameters["workStatusId"].Value = objReportSearchModel.WorkStatusId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Status Statistics Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderStatusStatistics)]
        public ActionResult JobOrderStatusStatistics()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderStatusStatistics);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderStatusStatistics, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderStatusStatistics rpt = new JobOrderStatusStatistics();

          //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderStatusStatistics;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderStatusStatistics;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Aging Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderAging)]
        public ActionResult JobOrderAging()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderAging);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderAging, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderAging rpt = new JobOrderAging();

         //   rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderAging;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrdersAging;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;
            rpt.Parameters["workTradeId"].Value = objReportSearchModel.WorkTradeId;
            rpt.Parameters["workPriorityId"].Value = objReportSearchModel.WorkPriorityId;
            rpt.Parameters["aging"].Value = objReportSearchModel.Aging;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Cost By Location Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderCostByLocationChart)]
        public ActionResult JobOrderCostByLocationChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderCostByLocationChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderCostByLocationChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderCostByLocationChart rpt = new JobOrderCostByLocationChart();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderCostByLocationChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }

            
rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderCostByLocationChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["includeLabourCost"].Value = objReportSearchModel.IncludeLabourCost;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Failure Percentage Analysis Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptFailurePercentageAnalysisChart)]
        public ActionResult FailurePercentageAnalysisChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptFailurePercentageAnalysisChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptFailurePercentageAnalysisChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            FailurePercentageAnalysisChart rpt = new FailurePercentageAnalysisChart();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockFailurePercentageAnalysisChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuFailurePercentageAnalysisChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Man Hours By Location Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderManHoursByLocationChart)]
        public ActionResult JobOrderManHoursByLocationChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderManHoursByLocationChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderManHoursByLocationChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderManHoursByLocationChart rpt = new JobOrderManHoursByLocationChart();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderManHoursByLocationChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderMan_HoursByLocationChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;

            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Cost By Job Type Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderCostByJobTypeChart)]
        public ActionResult JobOrderCostByJobTypeChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderCostByJobTypeChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderCostByJobTypeChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderCostByJobTypeChart rpt = new JobOrderCostByJobTypeChart();

          //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderCostByJobTypeChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderCostbyJobTypeChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["includeLabourCost"].Value = objReportSearchModel.IncludeLabourCost;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Type Distribution Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderTypeDistributionChart)]
        public ActionResult JobOrderTypeDistributionChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderTypeDistributionChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderTypeDistributionChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderTypeDistributionChart rpt = new JobOrderTypeDistributionChart();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderTypeDistributionChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderDistributionChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Failure Downtime Analysis Chart Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptFailureDowntimeAnalysisChart)]
        public ActionResult FailureDowntimeAnalysisChart()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptFailureDowntimeAnalysisChart);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptFailureDowntimeAnalysisChart, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            FailureDowntimeAnalysisChart rpt = new FailureDowntimeAnalysisChart();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockFailureDowntimeAnalysisChart;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuFailureDowntimeAnaylsisChart;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["locationId"].Value = objReportSearchModel.LocationId;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["assetCategoryId"].Value = objReportSearchModel.AssetCategoryId;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Cost By Cost Center Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderCostByCostCenter)]
        public ActionResult JobOrderCostByCostCenter()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderCostByCostCenter);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderCostByCostCenter, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderCostByCostCenter rpt = new JobOrderCostByCostCenter();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderCostByCostCenter;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderCostByCostCenter;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["costCenterId"].Value = objReportSearchModel.CostCenterId;
            rpt.Parameters["includeLabourCost"].Value = objReportSearchModel.IncludeLabourCost;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Orders By Employees Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrdersByEmployees)]
        public ActionResult JobOrdersByEmployees()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrdersByEmployees);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrdersByEmployees, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrdersByEmployees rpt = new JobOrdersByEmployees();

         //   rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrdersByEmployees;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrdersByEmployees;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;
            rpt.Parameters["workStatusId"].Value = objReportSearchModel.WorkStatusId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Job Order Ratings Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptJobOrderRatings)]
        public ActionResult JobOrderRatings()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptJobOrderRatings);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptJobOrderRatings, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            JobOrderRatings rpt = new JobOrderRatings();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderRatings;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuJobOrderRatingsReport;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTypeId"].Value = objReportSearchModel.WorkTypeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region PM Job Order Aging Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPMJobOrderAging)]
        public ActionResult PMJobOrderAging()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPMJobOrderAging);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPMJobOrderAging, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PMJobOrderAging rpt = new PMJobOrderAging();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderAging;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuPMJobOrderAgingList;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTradeId"].Value = objReportSearchModel.WorkTradeId;
            rpt.Parameters["workPriorityId"].Value = objReportSearchModel.WorkPriorityId;
            rpt.Parameters["aging"].Value = objReportSearchModel.Aging;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region PM Job Order Status Statistics Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPMJobOrderStatusStatistics)]
        public ActionResult PMJobOrderStatusStatistics()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPMJobOrderStatusStatistics);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPMJobOrderStatusStatistics, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PMJobOrderStatusStatistics rpt = new PMJobOrderStatusStatistics();

          //  rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockJobOrderStatusStatistics;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuPMJobOrderStatus;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTradeId"].Value = objReportSearchModel.WorkTradeId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region PM Job Order List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptPMJobOrderList)]
        public ActionResult PMJobOrderList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptPMJobOrderList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                objReportSearchModel.DateTo = DateTime.Now;
                FilterHelper.AddSearchFilterInDictionary(Actions.RptPMJobOrderList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            PMJobOrderList rpt = new PMJobOrderList();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.JobOrder_TextBlockPMJobOrderList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuPMJobOrderList;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;
            rpt.Parameters["maintDeptId"].Value = objReportSearchModel.MaintDeptID;
            rpt.Parameters["maintDivisionId"].Value = objReportSearchModel.MaintDivID;
            rpt.Parameters["maintSubDeptId"].Value = objReportSearchModel.MaintSubDeptID;
            rpt.Parameters["dateFrom"].Value = objReportSearchModel.DateFrom;
            rpt.Parameters["dateTo"].Value = objReportSearchModel.DateTo;
            rpt.Parameters["workTradeId"].Value = objReportSearchModel.WorkTradeId;
            rpt.Parameters["workPriorityId"].Value = objReportSearchModel.WorkPriorityId;
            rpt.Parameters["workStatusId"].Value = objReportSearchModel.WorkStatusId;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion
    }
}