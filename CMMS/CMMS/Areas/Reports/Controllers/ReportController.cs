﻿using System.Web.Mvc;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using System;

namespace CMMS.Areas.Reports.Controllers
{
    public class ReportController : BaseController
    {
        public ActionResult index()
        {
            return View();
        }

        [ActionName(Actions._ReportSearch)]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult _ReportSearch(ReportSearchPageModel reportSearchPageModel)
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(reportSearchPageModel.ReloadAction);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();

                if (reportSearchPageModel.ReloadAction == Actions.RptAssetList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetDetails
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMaintenanceHistoryList
                    || reportSearchPageModel.ReloadAction == Actions.RptTop10AssetDownTime
                    || reportSearchPageModel.ReloadAction == Actions.RptTop10AssetBreakDown
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMTBF
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetBreakdownFrequencyList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetBreakdownDowntimeList
                    || reportSearchPageModel.ReloadAction == Actions.RptManHoursByAssetList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMTTRList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMaintenanceHistoryNoCostList
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderList
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderStatusStatistics
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderAging
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByLocationChart
                    || reportSearchPageModel.ReloadAction == Actions.RptFailurePercentageAnalysisChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderManHoursByLocationChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByJobTypeChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderTypeDistributionChart
                    || reportSearchPageModel.ReloadAction == Actions.RptFailureDowntimeAnalysisChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByCostCenter
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrdersByEmployees
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderRatings
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderAging
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderStatusStatistics
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderList
                    || reportSearchPageModel.ReloadAction == Actions.RptCleaningInspectionSummary
                    || reportSearchPageModel.ReloadAction == Actions.RptCleaningInspectionDetailList
                    || reportSearchPageModel.ReloadAction == Actions.RptEmployeeRequesterList
                    || reportSearchPageModel.ReloadAction == Actions.RptEmployeeRequesterDetails
                    || reportSearchPageModel.ReloadAction == Actions.RptPMChecklistList
                    || reportSearchPageModel.ReloadAction == Actions.RptPMCompliance
                    || reportSearchPageModel.ReloadAction == Actions.RptPMMaterialRequirements
                    || reportSearchPageModel.ReloadAction == Actions.RptPMAnualSchedule
                    || reportSearchPageModel.ReloadAction == Actions.RptLocationList
                    || reportSearchPageModel.ReloadAction == Actions.RptStockTakeList
                    || reportSearchPageModel.ReloadAction == Actions.RptStockBalanceList
                    || reportSearchPageModel.ReloadAction == Actions.RptTransactionList
                    || reportSearchPageModel.ReloadAction == Actions.RptPurchaseProposalListNormal
                    || reportSearchPageModel.ReloadAction == Actions.RptPurchaseProposalListHigh)
                {
                    objReportSearchModel.L1ID = 1;
                    objReportSearchModel.L2ID = "2";
                }

                if (reportSearchPageModel.ReloadAction == Actions.RptTop10AssetDownTime
                    || reportSearchPageModel.ReloadAction == Actions.RptTop10AssetBreakDown
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMTBF
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetBreakdownFrequencyList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetBreakdownDowntimeList
                    || reportSearchPageModel.ReloadAction == Actions.RptManHoursByAssetList
                    || reportSearchPageModel.ReloadAction == Actions.RptAssetMTTRList
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderList
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderStatusStatistics
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderAging
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByLocationChart
                    || reportSearchPageModel.ReloadAction == Actions.RptFailurePercentageAnalysisChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderManHoursByLocationChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByJobTypeChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderTypeDistributionChart
                    || reportSearchPageModel.ReloadAction == Actions.RptFailureDowntimeAnalysisChart
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderCostByCostCenter
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrdersByEmployees
                    || reportSearchPageModel.ReloadAction == Actions.RptJobOrderRatings
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderAging
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderStatusStatistics
                    || reportSearchPageModel.ReloadAction == Actions.RptPMJobOrderList
                    || reportSearchPageModel.ReloadAction == Actions.RptCleaningInspectionSummary
                    || reportSearchPageModel.ReloadAction == Actions.RptCleaningInspectionDetailList
                    || reportSearchPageModel.ReloadAction == Actions.RptPMCompliance
                    || reportSearchPageModel.ReloadAction == Actions.RptPMMaterialRequirements
                    || reportSearchPageModel.ReloadAction == Actions.RptPurchaseRequestList
                    || reportSearchPageModel.ReloadAction == Actions.RptPurchaseOrderCost)
                {
                    objReportSearchModel.DateFrom = DateTime.Now.AddMonths(-1);
                    objReportSearchModel.DateTo = DateTime.Now;
                }

                if (reportSearchPageModel.ReloadAction == Actions.RptPMMaterialRequirements)
                {
                    objReportSearchModel.ForecastOption = 1;
                }

                if (reportSearchPageModel.ReloadAction == Actions.RptPartReceiveQtyList
                    || reportSearchPageModel.ReloadAction == Actions.RptPartIssueQtyList
                    || reportSearchPageModel.ReloadAction == Actions.RptPartReceiveTotalCostList
                    || reportSearchPageModel.ReloadAction == Actions.RptPartIssueTotalCostList)
                {
                    objReportSearchModel.Month = DateTime.Now.Month;
                    objReportSearchModel.Year = DateTime.Now.Year;
                }

                if (reportSearchPageModel.ReloadAction == Actions.RptDormantParts)
                {
                    objReportSearchModel.Aging = 30;
                }

                objReportSearchModel.PageId = reportSearchPageModel.PageId;
                objReportSearchModel.PageName = reportSearchPageModel.PageName;
                FilterHelper.AddSearchFilterInDictionary(reportSearchPageModel.ReloadAction, objReportSearchModel);
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            ViewBag.RptReloadActionName = reportSearchPageModel.ReloadAction;
            ViewBag.RptReloadControllerName = reportSearchPageModel.ReloadController;
            return View(objReportSearchModel);
        }

        [ActionName(Actions.RptListFilter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult FilterList(ReportSearchModel objReportSearchModel, string reloadAction)
        {
            FilterHelper.AddSearchFilterInDictionary(reloadAction, objReportSearchModel);
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}