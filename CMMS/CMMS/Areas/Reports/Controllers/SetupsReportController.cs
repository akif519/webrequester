﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Reports;
using CMMS.Model;
using CMMS.Services;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service.DashboardService;
using CMMS.Service.ReportService;
using CMMS.Reports.Asset;

namespace CMMS.Areas.Reports.Controllers
{
    public class SetupsReportController : BaseController
    {
        // GET: Reports/JobOrder
        public ActionResult Index()
        {
            return View();
        }

        #region Location List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptLocationList)]
        public ActionResult LocationList()
        {
            ReportSearchModel objReportSearchModel;
            var objReportSearchModelTemp = FilterHelper.GetSearchFilterFromDictionary(Actions.RptLocationList);
            if (objReportSearchModelTemp == null)
            {
                objReportSearchModel = new ReportSearchModel();
                objReportSearchModel.L1ID = 1;
                objReportSearchModel.L2ID = "2";
                FilterHelper.AddSearchFilterInDictionary(Actions.RptLocationList, objReportSearchModel);
                ViewBag.IsFirstLoad = true;
                return View();
            }
            else
                objReportSearchModel = (ReportSearchModel)objReportSearchModelTemp;

            LocationList rpt = new LocationList();

           // rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Setups_TextBlockLocationList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuPhysicalLocationList;
            rpt.Parameters["l1Id"].Value = objReportSearchModel.L1ID;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = objReportSearchModel.L2ID;
            rpt.Parameters["l3Id"].Value = objReportSearchModel.L3ID;
            rpt.Parameters["l4Id"].Value = objReportSearchModel.L4ID;
            rpt.Parameters["l5Id"].Value = objReportSearchModel.L5ID;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion

        #region Supplier List Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.RptSupplierList)]
        public ActionResult SupplierList()
        {
            SupplierList rpt = new SupplierList();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Setups_TextBlockSupplierList;

            #region "Filter Text & Value "
            //rpt.FindControl("lblCityCode", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblCityCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetNoHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //rpt.FindControl("lblAssetNoHeaderValue", false).Text = "";
            //rpt.FindControl("lblZoneCode", false).Text = ProjectSession.Resources.label.Street_TextBlockStreetCode;
            //rpt.FindControl("lblZoneCodeValue", false).Text = "";
            //rpt.FindControl("lblAssetDescriptionHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //rpt.FindControl("lblAssetDescriptionHeaderValue", false).Text = "";
            //rpt.FindControl("lblBuildingCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblBuildingCodeValue", false).Text = "";
            //rpt.FindControl("lblLocationCode", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //rpt.FindControl("lblLocationCodeValue", false).Text = "";
            #endregion

            #region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (Lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            #endregion

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = ProjectSession.Resources.menu.Reports_MenuSupplierList;
            rpt.Parameters["l1Id"].Value = 0;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion
    }
}