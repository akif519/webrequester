﻿using System.Web.Mvc;

namespace CMMS.Areas.Transaction
{
    /// <summary>
    /// Transaction Area Registration
    /// </summary>
    public class TransactionAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Gets the name of the area to register.
        /// </summary>
        /// <returns>The name of the area to register.</returns>
        public override string AreaName
        {
            get
            {
                return Pages.Areas.Transaction;
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {           
            context.MapRoute(
            name: "Transaction_default",
            url: Pages.Areas.Transaction + "/{action}",
            defaults: new { controller = Pages.Controllers.Transaction, action = "Index" });

            context.MapRoute(
                name: "Transaction",
                url: Pages.Areas.Transaction + "/{controller}/{action}/{id}",
                defaults: new { controller = Pages.Controllers.Transaction, action = "Index", id = UrlParameter.Optional });
        }
    }
}