﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;
using CMMS.Pages;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Meter 
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "PM Meter By Asset"
        /// <summary>
        /// Gets the pm meter history by asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterHistoryByAssetId)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterHistoryByAssetId([DataSourceRequest]DataSourceRequest request, int assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterService obj = new PMMeterService();
            var result = new DataSourceResult()
            {
                Data = obj.GetPMMeterHistoryByAssetId(Convert.ToInt32(assetID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm meter schedule history by asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterScheduleHistoryByAssetId)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterScheduleHistoryByAssetId([DataSourceRequest]DataSourceRequest request, int assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterService obj = new PMMeterService();
            var result = new DataSourceResult()
            {
                Data = obj.GetPMMeterScheduleHistoryByAssetId(Convert.ToInt32(assetID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion

        #region "PM Meter By Location"

        /// <summary>
        /// Gets the pm meter history by asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterHistoryByLocationId)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterHistoryByLocationId([DataSourceRequest]DataSourceRequest request, int locationID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterService obj = new PMMeterService();
            var result = new DataSourceResult()
            {
                Data = obj.GetPMMeterHistoryByLocationId(Convert.ToInt32(locationID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm meter schedule history by asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterScheduleHistoryByLocationId)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterScheduleHistoryByLocationId([DataSourceRequest]DataSourceRequest request, int locationID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterService obj = new PMMeterService();
            var result = new DataSourceResult()
            {
                Data = obj.GetPMMeterScheduleHistoryByLocationId(Convert.ToInt32(locationID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion

        #region "PM Meter Master List"

        /// <summary>
        /// PM Meter Master List.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMMeterMasterList)]
        public ActionResult PMMeterMasterList()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Preventive_PMChecklists_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                return View(Views.PMMeterMasterList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// get pm meter master list
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterMasterListGrid)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterMasterListGrid([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterMaster objPMMeterMaster = new PMMeterMaster();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and PMMeterMaster.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (location.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + " ) OR (location.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and location.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            /*(End)Added By Pratik Telaviya on 25-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPMMeterMaster, SystemEnum.Pages.PMMeterMasterList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// delete PM Meter Master
        /// </summary>
        /// <param name="meterMasterID">The PMMeter Master identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePMMeterMaster)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePMMeterMaster(int meterMasterID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<PMMeterMaster>(meterMasterID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region "PM Meter Master Edit"

        /// <summary>
        /// PM Meter Master List.
        /// </summary>
        /// <param name="meterMasterID">The PMMeter identifier</param>
        /// <returns></returns>
        [ActionName(Actions.PMMeterMasterEdit)]
        public ActionResult PMMeterMasterEdit(int meterMasterID)
        {
            if (ProjectSession.PermissionAccess.Preventive_MeterSchedules_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                ViewBag.meterMasterID = meterMasterID;
                string strWhere = string.Empty;

                PMMeterMaster objPMMeterMaster = new PMMeterMaster();
                if (meterMasterID == 0)
                {
                    ////For New Request
                    employees objEmp = new employees();
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objEmp = objContext.SearchAllByID<employees>(objEmp, ProjectSession.EmployeeID).FirstOrDefault();
                    }

                    objPMMeterMaster.L2ID = Convert.ToInt32(objEmp.L2ID);
                    objPMMeterMaster.TypeMeter = "1";
                }

                if (meterMasterID > 0)
                {
                    objPMMeterMaster = PMMeterService.GetAssetDetailByMeterMasterID(meterMasterID);
                }

                return View(Views.PMMeterMasterEdit, objPMMeterMaster);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Get Asset Details By AssetID
        /// </summary>
        /// <param name="assetID">The AssetDetails identifier</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetDetailsByAssetID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetAssetDetailsByAssetID(int assetID)
        {
            string strWhere = string.Empty;
            strWhere = "and assets.AssetID = '" + assetID + "' ";

            PMMeterMaster objPMMeterMaster = new PMMeterMaster();
            SearchFieldService obj = new SearchFieldService();

            var result = new DataSourceResult()
           {
               Data = obj.AdvanceSearch(objPMMeterMaster, SystemEnum.Pages.AssetDetailsByAssetID.GetHashCode(), ProjectSession.EmployeeID, 1, string.Empty, string.Empty, true, strWhere)
           };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// get pm meter list
        /// </summary>
        /// <param name="request">The request</param>
        /// <param name="meterMasterID">The PMMeter identifier</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterListGrid)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterListGrid([DataSourceRequest]DataSourceRequest request, int meterMasterID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            strWhere = " and pmmeters.MeterMasterID = " + meterMasterID + " ";

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PmMeter objPmMeter = new PmMeter();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPmMeter, SystemEnum.Pages.PMMeterList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Insert Update PMMeterMaster
        /// </summary>
        /// <param name="objPMMeterMaster">The PMMeterMaster</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMMeterMaster)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePMMeterMaster(PMMeterMaster objPMMeterMaster)
        {
            try
            {
                int newPMMeterID = 0;
                string validations = string.Empty;

                ////1.Asset,2.location
                if (objPMMeterMaster.TypeMeter == "1")
                {
                    objPMMeterMaster.LocationId = objPMMeterMaster.LocationIDAsset;
                }
                else if (objPMMeterMaster.TypeMeter == "2")
                {
                    objPMMeterMaster.AssetID = null;
                }

                if (objPMMeterMaster.MeterMasterID > 0)
                {
                    objPMMeterMaster.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objPMMeterMaster.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objPMMeterMaster.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objPMMeterMaster.CreatedDate = DateTime.Now;
                    objPMMeterMaster.LastReading = objPMMeterMaster.StartMeterReading;
                }

                if (objPMMeterMaster.MaxDailyIncrement == null)
                {
                    objPMMeterMaster.MaxDailyIncrement = 0;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext("MeterNo"))
                    {
                        newPMMeterID = context.Save(objPMMeterMaster);
                        if (newPMMeterID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.PMMeter_MsgMeterNoAlreadyExists;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                            return View(Views.PMMeterMasterEdit, objPMMeterMaster);
                        }
                    }

                    return RedirectToAction(Actions.PMMeterMasterEdit, Pages.Controllers.Transaction, new { Area = Pages.Areas.Transaction, meterMasterID = newPMMeterID });
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.PMMeterMasterEdit, objPMMeterMaster);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "PM Meter Add"
        /// <summary>
        ///  PM Meter Add.
        /// </summary>
        /// <param name="meterMasterID">pm meter master identifier</param>
        /// <param name="meterID">pm meter identifier</param>
        /// <returns></returns>
        [ActionName(Actions.PMMeterAdd)]
        public ActionResult PMMeterAdd(int meterMasterID = 0, int meterID = 0)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            if (ProjectSession.PermissionAccess.Preventive_PMChecklists_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule && meterMasterID > 0)
            {
                PMMeterMaster objPMMeterMaster = new PMMeterMaster();
                objPMMeterMaster = PMMeterService.GetAssetDetailByMeterMasterID(meterMasterID);

                PmMeter objPmMeter = new PmMeter();
                PMCheckList objPMCheckList = new PMCheckList();
                if (meterID > 0)
                {
                    objPmMeter = PMMeterService.GetPmchecklistDetailByPMMeterID(meterID);

                    if (objPMMeterMaster.MeterType.ToLower() == "incremental")
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            objPMCheckList = context.SearchAll(objPMCheckList).Where(m => m.ChecklistID == objPmMeter.PMIncTaskID).FirstOrDefault();
                        }

                        objPmMeter.ChecklistNo = objPMCheckList.ChecklistNo;
                        objPmMeter.CheckListName = objPMCheckList.CheckListName;
                        objPmMeter.AltCheckListName = objPMCheckList.AltCheckListName;
                    }

                    if (objPMMeterMaster.MeterType.ToLower() == "threshold")
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            objPMCheckList = context.SearchAll(objPMCheckList).Where(m => m.ChecklistID == objPmMeter.PMLowTaskID).FirstOrDefault();
                        }

                        objPmMeter.ChecklistNo1 = objPMCheckList.ChecklistNo;
                        objPmMeter.CheckListName1 = objPMCheckList.CheckListName;
                        objPmMeter.AltCheckListName1 = objPMCheckList.AltCheckListName;

                        using (DapperContext context = new DapperContext())
                        {
                            objPMCheckList = context.SearchAll(objPMCheckList).Where(m => m.ChecklistID == objPmMeter.PMUppTaskID).FirstOrDefault();
                        }

                        objPmMeter.ChecklistNo2 = objPMCheckList.ChecklistNo;
                        objPmMeter.CheckListName2 = objPMCheckList.CheckListName;
                        objPmMeter.AltCheckListName2 = objPMCheckList.AltCheckListName;
                    }
                }
                else
                {
                    employees obj = new employees();

                    Worktrade objWorktrade = new Worktrade();
                    objWorktrade.IsDefault = true;

                    using (DapperContext context = new DapperContext())
                    {
                        obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                        objWorktrade = context.Search<Worktrade>(objWorktrade).FirstOrDefault();
                        if (objWorktrade != null)
                        {
                            objPmMeter.Worktradeid = objWorktrade.WorkTradeID;
                        }
                    }

                    objPmMeter.MaintDivisionID = obj.MaintDivisionID;
                    objPmMeter.MaintDeptID = obj.MaintDeptID;
                    objPmMeter.MaintSubDeptID = obj.MaintSubDeptID;
                }

                objPmMeter.MeterMasterID = meterMasterID;
                objPmMeter.MeterMasterNo = objPMMeterMaster.MeterNo;
                objPmMeter.MeterMasterDescription = objPMMeterMaster.MeterDescription;
                objPmMeter.AltMeterMasterDescription = objPMMeterMaster.AltMeterDescription;
                objPmMeter.MeterMasterUnit = objPMMeterMaster.MeterUnit;
                objPmMeter.AssetNumber = objPMMeterMaster.AssetNumber;
                objPmMeter.AssetDescription = objPMMeterMaster.AssetDescription;
                objPmMeter.AssetAltDescription = objPMMeterMaster.AssetAltDescription;
                objPmMeter.LastReadingMaster = objPMMeterMaster.LastReading;
                objPmMeter.MasterMeterType = objPMMeterMaster.MeterType;
                ViewBag.L2ID = objPMMeterMaster.L2ID;
                ViewBag.meterID = objPmMeter.MeterID;
                ViewBag.MeterMasterID = meterMasterID;
                return View(Views.PMMeterEdit, objPmMeter);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Manage PM Meter
        /// </summary>
        /// <param name="objPmMeter">Pm meter</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMMeter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManagePMMeter(PmMeter objPmMeter)
        {
            string validations = string.Empty;
            int newMeterID = 0;

            try
            {
                if (objPmMeter.MeterID > 0)
                {
                    objPmMeter.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objPmMeter.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objPmMeter.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objPmMeter.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext("MeterNo"))
                    {
                        newMeterID = context.Save(objPmMeter);
                    }

                    if (newMeterID > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                        return RedirectToAction(Actions.PMMeterAdd, Pages.Controllers.Transaction, new { meterMasterID = objPmMeter.MeterMasterID, meterID = newMeterID });
                    }
                    else
                    {
                        ViewBag.Message = ProjectSession.Resources.message.PMMeter_PMMeterNoExists;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                        return View(Views.PMMeterEdit, objPmMeter);
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.PMMeterEdit, objPmMeter);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "PM Meter - Assign To"
        /// <summary>
        /// _s the pm assign to.
        /// </summary>
        /// <param name="meterID">The meter identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._PMMeterAssignTo)]
        public ActionResult _PMMeterAssignTo(int meterID)
        {
            try
            {
                PmMeters_AssignTo objPMAssignTo = new PmMeters_AssignTo();
                objPMAssignTo.Meterid = meterID;

                using (ServiceContext context = new ServiceContext())
                {
                    PmMeter objPmMeter = context.SelectObject<PmMeter>(meterID);
                    objPMAssignTo.MeterNo = objPmMeter.MeterNo;
                    objPMAssignTo.MeterDescription = objPmMeter.MeterDescription;
                    objPMAssignTo.AltMeterDescription = objPmMeter.AltMeterDescription;
                }

                return PartialView(PartialViews._PMMeterAssignTo, objPMAssignTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the pm assign to list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="meterID">The meter identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterAssignToList)]
        [HttpPost]
        public ActionResult GetPMMeterAssignToList([DataSourceRequest]DataSourceRequest request, int meterID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "meterID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterService obj = new PMMeterService();

            var result = new DataSourceResult()
            {
                Data = obj.GetPMMeterAssignList(meterID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the pm assign to employee group.
        /// </summary>
        /// <param name="meterID">The meter identifier.</param>
        /// <param name="groupID">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMMeterAssignToEmployeeGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMMeterAssignToEmployeeGroup(int meterID, int groupID)
        {
            try
            {
                PMMeterService.ManagePMMeterAssignToEmployeeGroup(meterID, groupID);
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the pm assign to employee.
        /// </summary>
        /// <param name="meterID">The meter identifier.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMMeterAssignToEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMMeterAssignToEmployee(int meterID, int employeeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    PmMeters_AssignTo obj = new PmMeters_AssignTo();
                    obj.Meterid = meterID;
                    obj.Employeeid = employeeID;
                    int count = context.Search<PmMeters_AssignTo>(obj).ToList().Count();
                    if (count > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgEmployeeAlreadyExists });
                    }
                    else
                    {
                        PMMeterService.ManagePMMeterAssignToEmployee(meterID, employeeID);
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the pm schedule assign to.
        /// </summary>
        /// <param name="meterID">The meter identifier.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePmMeterAssignto)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePmMeterAssignto(int meterID, int employeeID)
        {
            bool result = PMMeterService.DeletePmMeterScheduleAssignto(meterID, employeeID);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Reading"
        /// <summary>
        /// load reading page
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Reading)]
        public ActionResult Reading()
        {
            ////if (saved == 1)
            ////{
            ////TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
            ////TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
            ////}

            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            ReadingSchedule objReadingSchedule = new ReadingSchedule();

            ////For New Request
            employees objEmp = new employees();
            using (ServiceContext objContext = new ServiceContext())
            {
                objEmp = objContext.SearchAllByID<employees>(objEmp, ProjectSession.EmployeeID).FirstOrDefault();
            }

            objReadingSchedule.L2ID = Convert.ToInt32(objEmp.L2ID);

            objReadingSchedule.TempReadingDate = DateTime.Now.ToString("yyyy/MM/dd");

            if (ProjectSession.PermissionAccess.Preventive_PMChecklists_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                return View(Views.Reading, objReadingSchedule);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// get list of pm meter reading
        /// </summary>
        /// <param name="request">the request</param>
        /// <param name="meterMasterID">pm meter reading identifier</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterReadingList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterReadingList([DataSourceRequest]DataSourceRequest request, int meterMasterID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            strWhere = " and  Reading.MeterMasterID = " + meterMasterID + " ";

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Reading objReading = new Reading();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objReading, SystemEnum.Pages.PMMeterReadingList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// delete PM Meter Reading
        /// </summary>
        /// <param name="readingID">The PMMeter Reading identifier.</param>
        /// <param name="meterMasterID">The meter master identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePMMeterReading)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePMMeterReading(int readingID, int meterMasterID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = PMMeterService.DeleteReading(readingID, meterMasterID);
                if (returnValue == -1)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "This reading cannot be deleted as pm generation is already done for this reading." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    PMMeterMaster objPMMeterMaster = new PMMeterMaster();
                    objPMMeterMaster = context.SearchAll(objPMMeterMaster).Where(m => m.MeterMasterID == meterMasterID).FirstOrDefault();
                    if (objPMMeterMaster.LastReading > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully, objPMMeterMaster.LastReading }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully, objPMMeterMaster.StartMeterReading }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }

        /// <summary>
        ///  update PM Meter Reading
        /// </summary>
        /// <param name="readingID">The PMMeter Reading identifier</param>
        /// <param name="currentReading">The PMMeter Reading currentReading value</param>
        /// <param name="meterMasterID">The MeterMasterID</param>
        /// <returns></returns>
        [ActionName(Actions.UpdatePMMeterReading)]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdatePMMeterReading(int readingID, float currentReading, int meterMasterID)
        {
            Reading objReading = new Reading();
            try
            {
                if (readingID > 0 && currentReading > 0)
                {
                    objReading.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objReading.ModifiedDate = DateTime.Now;
                    objReading.ReadingID = readingID;
                    objReading.CurrentReading = currentReading;
                    objReading.MeterMasterID = meterMasterID;
                    using (ServiceContext context = new ServiceContext())
                    {
                        objReading.ReadingDate = context.SelectObject<Reading>(readingID).ReadingDate;
                    }

                    using (ServiceContext context = new ServiceContext())
                    {
                        ReadingSchedule objReadingSchedule = new ReadingSchedule();
                        int count = (from c in context.SearchAll<ReadingSchedule>(objReadingSchedule) where c.ReadingID == readingID && c.GenStatus.ToLower() == "done" select c).Count();
                        if (count > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgCannotEditReading });
                        }
                    }

                    double result = PMMeterService.UpdatePMMeterReading(objReading);

                    PMMeterMaster objPMMeterMaster = new PMMeterMaster();
                    using (DapperContext context = new DapperContext())
                    {
                        objPMMeterMaster = context.SearchAll(objPMMeterMaster).Where(m => m.MeterMasterID == objReading.MeterMasterID).FirstOrDefault();
                    }

                    if (result == -2)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, objPMMeterMaster.LastReading });
                    }
                    else if (result == -1)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgEditedReadingLesser });
                    }
                    else
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgMaxDailyLimitExceed1 + objPMMeterMaster.MaxDailyIncrement + ProjectSession.Resources.message.Reading_MsgMaxDailyLimitExceed2 + " " + string.Format("{0:0.00}", result) }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Reading Schedule List
        /// </summary>
        /// <param name="request">the request </param>
        /// <param name="cityID">get list by cityID </param>
        /// <returns></returns>
        [ActionName(Actions.ReadingScheduleList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetReadingScheduleList([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "\"Date\"";
                sortDirection = "Ascending";
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                strWhere = " and TRUNC(ReadingSchedule.Enterdate) = to_date('" + DateTime.Today.ToString("yyyy/MMM/dd") + "','YYYY/MON/DD') and PMMeterMaster.L2ID = " + cityID + " ";
            }
            else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
            {
                strWhere = " and date(ReadingSchedule.Enterdate) = '" + Common.GetEnglishDate(DateTime.Today) + "' and PMMeterMaster.L2ID = " + cityID + " ";
            }
            else
            {
                strWhere = " and CONVERT(date,ReadingSchedule.Enterdate) = '" + Common.GetEnglishDate(DateTime.Today) + "' and PMMeterMaster.L2ID = " + cityID + " ";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ReadingSchedule objReadingSchedule = new ReadingSchedule();

            IList<ReadingSchedule> lstReadingSchedule = new List<ReadingSchedule>();

            SearchFieldService obj = new SearchFieldService();

            if (cityID > 0)
            {
                lstReadingSchedule = obj.AdvanceSearch(objReadingSchedule, SystemEnum.Pages.PMMeterReadingSchedule.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request);
            }

            var result = new DataSourceResult()
            {
                Data = lstReadingSchedule,
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// get pm meter master list
        /// </summary>
        /// <param name="request">The request</param>
        /// <param name="cityID">get list by cityID</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterMasterListByCity)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPMMeterMasterListByCity([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Ascending";
            }

            strWhere = " and PMMeterMaster.L2ID = " + cityID + " and PMMeterMaster.Active = 1 ";

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMMeterMaster objPMMeterMaster = new PMMeterMaster();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPMMeterMaster, SystemEnum.Pages.PMMeterMasterList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// update PM Meter Reading
        /// </summary>
        /// <param name="meterMasterID">The PMMeter Reading identifier</param>
        /// <param name="currentReading">The PMMeter Reading currentReading value</param>
        /// <param name="date">Reading date</param>
        /// <param name="time">Reading time</param>
        /// <returns></returns>
        [ActionName(Actions.InsertUpdatePMMeterReading)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InsertUpdatePMMeterReading(int meterMasterID, float currentReading, string date, string time)
        {
            Reading objReading = new Reading();
            Reading objTempReading = new Reading();
            PMMeterMaster objPMMeterMaster = new PMMeterMaster();

            try
            {
                if (meterMasterID > 0 && currentReading > 0)
                {
                    if ((date != null && date != "undefined") && (time != null && time != "undefined"))
                    {
                        objReading.CreatedBy = ProjectSession.EmployeeID.ToString();
                        objReading.CreatedDate = DateTime.Now;
                        objReading.MeterMasterID = meterMasterID;
                        objReading.CurrentReading = currentReading;

                        DateTime readingDate = Convert.ToDateTime(date);
                        DateTime readingTime = Convert.ToDateTime(time);
                        string dt1 = Convert.ToString(Common.GetEnglishDate(readingDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(readingTime));
                        objReading.ReadingDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));

                        if (objReading.ReadingDate > DateTime.Now)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgReadingDateLarger }, JsonRequestBehavior.AllowGet);
                        }
                        ////using (DapperContext context = new DapperContext())
                        ////{
                        ////    objTempReading = context.SearchAll(objTempReading).Where(m => m.MeterMasterID == objReading.MeterMasterID).OrderByDescending(x => x.ReadingDate).FirstOrDefault();
                        ////}

                        ////if (objTempReading != null && (objTempReading.ReadingDate > objReading.ReadingDate))
                        ////{
                        ////    ////-3
                        ////    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgReadingDateLesser }, JsonRequestBehavior.AllowGet);
                        ////}

                        ////using (DapperContext context = new DapperContext())
                        ////{
                        ////    objPMMeterMaster = context.SearchAll(objPMMeterMaster).Where(m => m.MeterMasterID == objReading.MeterMasterID).FirstOrDefault();
                        ////}

                        ////if (objPMMeterMaster.MaxDailyIncrement > 0)
                        ////{
                        ////    if (!(objTempReading == null))
                        ////    {
                        ////        var timeDiff = (objTempReading.ReadingDate - objReading.ReadingDate).Value.TotalDays;
                        ////        var readingDiff = objTempReading.CurrentReading - objReading.CurrentReading;
                        ////        var avgDailyInc = readingDiff / timeDiff;
                        ////        if (Convert.ToDouble(avgDailyInc) > Convert.ToDouble(objPMMeterMaster.MaxDailyIncrement))
                        ////        {
                        ////            string tempValue = objPMMeterMaster.MaxDailyIncrement.ToString();
                        ////            ////return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "The entered reading has exceeded the Maximum Daily Limit as specified in the Meter Master. The Maximum Daily Limit is " + objPMMeterMaster.MaxDailyIncrement }, JsonRequestBehavior.AllowGet);
                        ////        }
                        ////    }
                        ////}

                        double result = PMMeterService.AddPMMeterReading(objReading);
                        if (result == -2)
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == -3)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgReadingDateLesser }, JsonRequestBehavior.AllowGet);
                        }
                        else if (result == -1)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgCurrReadingLesser }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            using (DapperContext context = new DapperContext())
                            {
                                objPMMeterMaster = context.SearchAll(objPMMeterMaster).Where(m => m.MeterMasterID == objReading.MeterMasterID).FirstOrDefault();
                                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Reading_MsgMaxDailyLimitExceed1 + objPMMeterMaster.MaxDailyIncrement + ProjectSession.Resources.message.Reading_MsgMaxDailyLimitExceed2 + " " + string.Format("{0:0.00}", result) }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// get JobOrders By MeterMasterID
        /// </summary>
        /// <param name="meterMasterID">The Job Orders identifier</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrdersByMeterMasterID)]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult GetJobOrdersByMeterMasterID(int meterMasterID)
        {
            WorkOrder objWorkOrder = new WorkOrder();

            try
            {
                var result = PMMeterService.GetJobOrderDetailsByMeterMasterID(meterMasterID);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Exports the reading count.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ExportReadingCount)]
        public JsonResult ExportReadingCount(int cityID)
        {
            try
            {
                int count = PMMeterService.ExportReadingCount(cityID);
                return Json(count, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Exports the reading.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ExportReading)]
        [OutputCache(NoStore = true, Duration = 0)]
        public string ExportReading(int cityID)
        {
            try
            {
                List<PMMeterMaster> list = PMMeterService.ExportReading(cityID);
                if (list != null && list.Count > 0)
                {
                    using (ExcelPackage pckExport = new ExcelPackage())
                    {
                        ExcelWorksheet workSheetExport = pckExport.Workbook.Worksheets.Add("Reading");
                        int int_LastRowsInserted = 0;
                        int_LastRowsInserted++;
                        workSheetExport.Cells[int_LastRowsInserted, 1].Value = ProjectSession.Resources.label.PM_TextBlockMeterNo;
                        workSheetExport.Cells[int_LastRowsInserted, 2].Value = ProjectSession.Resources.label.JobRequest_TextBlockCity;
                        workSheetExport.Cells[int_LastRowsInserted, 3].Value = ProjectSession.Resources.label.PM_TextBlockMeterDesc;
                        workSheetExport.Cells[int_LastRowsInserted, 4].Value = ProjectSession.Resources.label.PM_TextBlockAltMeterDesc;
                        workSheetExport.Cells[int_LastRowsInserted, 5].Value = ProjectSession.Resources.label.PM_TextBlockMeterType;
                        workSheetExport.Cells[int_LastRowsInserted, 6].Value = ProjectSession.Resources.label.PM_TextBlockMeterUnits;
                        workSheetExport.Cells[int_LastRowsInserted, 7].Value = ProjectSession.Resources.label.PMReading_TextBlockLastReadingDateWithFormate;
                        workSheetExport.Cells[int_LastRowsInserted, 8].Value = ProjectSession.Resources.label.PMReading_TextBlockLastEnteredReading;
                        workSheetExport.Cells[int_LastRowsInserted, 9].Value = ProjectSession.Resources.label.PM_TextBlockReadingDateWithFormate;
                        workSheetExport.Cells[int_LastRowsInserted, 10].Value = ProjectSession.Resources.label.PM_TextBlockCurrentReading;

                        using (ExcelRange rngHeadersWsExport = workSheetExport.Cells[int_LastRowsInserted, 1, int_LastRowsInserted, 10])
                        {
                            rngHeadersWsExport.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            rngHeadersWsExport.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Yellow);
                            rngHeadersWsExport.Style.Font.Bold = true;
                        }

                        int_LastRowsInserted++;
                        for (int inttblRowNumber = 0; inttblRowNumber < list.Count; inttblRowNumber++)
                        {
                            workSheetExport.Cells[int_LastRowsInserted, 1].Value = Convert.ToString(list[inttblRowNumber].MeterNo);
                            workSheetExport.Cells[int_LastRowsInserted, 2].Value = Convert.ToString(list[inttblRowNumber].L2Code);
                            workSheetExport.Cells[int_LastRowsInserted, 3].Value = Convert.ToString(list[inttblRowNumber].MeterDescription);
                            workSheetExport.Cells[int_LastRowsInserted, 4].Value = Convert.ToString(list[inttblRowNumber].AltMeterDescription);
                            workSheetExport.Cells[int_LastRowsInserted, 5].Value = Convert.ToString(list[inttblRowNumber].MeterType);
                            workSheetExport.Cells[int_LastRowsInserted, 6].Value = Convert.ToString(list[inttblRowNumber].MeterUnit);
                            workSheetExport.Cells[int_LastRowsInserted, 7].Value = list[inttblRowNumber].LastReadingDate == null ? string.Empty : Convert.ToDateTime(list[inttblRowNumber].LastReadingDate).ToString();
                            workSheetExport.Cells[int_LastRowsInserted, 8].Value = Convert.ToDecimal(list[inttblRowNumber].LastReading);
                            workSheetExport.Cells[int_LastRowsInserted, 9].Value = string.Empty;
                            workSheetExport.Cells[int_LastRowsInserted, 10].Value = string.Empty;

                            int_LastRowsInserted++;
                            workSheetExport.Cells[1, 1, list.Count, 10].AutoFitColumns();
                        }

                        ////for (int inttblRowNumber = 0; inttblRowNumber < 1; inttblRowNumber++)
                        ////{
                        ////    wsExport.Cells[int_LastRowsInserted, 1].Value = "Total Service Charge";
                        ////    wsExport.Cells[int_LastRowsInserted, 2].Value = "$" + list[inttblRowNumber].AmountTotal;
                        ////}
                        ////using (ExcelRange alignment = workSheetExport.Cells[int_LastRowsInserted, 2])
                        ////{
                        ////    alignment.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        ////}

                        Response.Clear();
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;  filename=Reading" + DateTime.Now.Date.ToShortDateString() + ".xls");
                        Response.BinaryWrite(pckExport.GetAsByteArray());
                        Response.End();
                    }

                    return "1";
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Imports the reading.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0)]
        public JsonResult ImportReading()
        {
            try
            {
                HttpPostedFileBase taskFile;
                if (Request.Files.Count > 0)
                {
                    taskFile = Request.Files[0];

                    ////var fileName = Path.GetFileName(objModel.File.FileName);
                    ////var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
                    ////DataTable dt = new DataTable();
                    FileInfo fi = new FileInfo(taskFile.FileName);

                    ExcelWorkbook workBook = null;

                    ////using (ExcelPackage package = new ExcelPackage(fi))

                    using (ExcelPackage package = new ExcelPackage(taskFile.InputStream))
                    {
                        workBook = package.Workbook;
                        if (workBook != null)
                        {
                            if (workBook.Worksheets.Count > 0)
                            {
                                ExcelWorksheet currentWorksheet = workBook.Worksheets.FirstOrDefault();
                                var start = currentWorksheet.Dimension.Start;
                                var end = currentWorksheet.Dimension.End;

                                if (start.Row > 0)
                                {
                                    for (int row = start.Row; row < start.Row + 1; row++)
                                    {
                                        if (currentWorksheet.Cells[row, 1].Text != ProjectSession.Resources.label.PM_TextBlockMeterNo ||
                                            currentWorksheet.Cells[row, 2].Text != ProjectSession.Resources.label.JobRequest_TextBlockCity ||
                                            currentWorksheet.Cells[row, 3].Text != ProjectSession.Resources.label.PM_TextBlockMeterDesc ||
                                            currentWorksheet.Cells[row, 4].Text != ProjectSession.Resources.label.PM_TextBlockAltMeterDesc ||
                                            currentWorksheet.Cells[row, 5].Text != ProjectSession.Resources.label.PM_TextBlockMeterType ||
                                            currentWorksheet.Cells[row, 6].Text != ProjectSession.Resources.label.PM_TextBlockMeterUnits ||
                                            currentWorksheet.Cells[row, 7].Text != ProjectSession.Resources.label.PMReading_TextBlockLastReadingDateWithFormate ||
                                            currentWorksheet.Cells[row, 8].Text != ProjectSession.Resources.label.PMReading_TextBlockLastEnteredReading ||
                                            currentWorksheet.Cells[row, 9].Text != ProjectSession.Resources.label.PM_TextBlockReadingDateWithFormate ||
                                            currentWorksheet.Cells[row, 10].Text != ProjectSession.Resources.label.PM_TextBlockCurrentReading)
                                        {
                                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileNotAsPerFormate }, JsonRequestBehavior.AllowGet);
                                        }
                                    }

                                    List<PMMeterMaster> lst = new List<PMMeterMaster>();
                                    List<PMMeterMaster> lstValid0 = new List<PMMeterMaster>();
                                    List<PMMeterMaster> lstValid = new List<PMMeterMaster>();
                                    List<PMMeterMaster> lstnew2 = new List<PMMeterMaster>();

                                    for (int i = start.Row + 1; i <= end.Row; i++)
                                    {
                                        bool isValidReadingDate = false;
                                        try
                                        {
                                            Convert.ToDateTime(currentWorksheet.Cells[i, 9].Text);
                                            isValidReadingDate = true;
                                        }
                                        catch
                                        {
                                            isValidReadingDate = false;
                                        }

                                        bool isValidReading = false;
                                        try
                                        {
                                            float temp = (float)Convert.ToDouble(currentWorksheet.Cells[i, 10].Text);
                                            isValidReading = true;
                                            if (temp <= 0)
                                            {
                                                isValidReading = false;
                                            }
                                        }
                                        catch
                                        {
                                            isValidReading = false;
                                        }

                                        lst.Add(new PMMeterMaster()
                                        {
                                            MeterNo = currentWorksheet.Cells[i, 1].Text,
                                            L2Code = currentWorksheet.Cells[i, 2].Text,
                                            ////MeterDescription = currentWorksheet.Cells[i, 3].Text,
                                            ////AltMeterDescription = currentWorksheet.Cells[i, 4].Text,
                                            ////MeterType = currentWorksheet.Cells[i, 5].Text,
                                            ////MeterUnit = currentWorksheet.Cells[i, 6].Text,
                                            ////lastreadingdate = convert.todatetime(currentworksheet.cells[i, 7].text),
                                            ////lastreading = (float)convert.todouble(currentworksheet.cells[i, 8].text),
                                            IsValidReadingDate = isValidReadingDate,
                                            IsValidReading = isValidReading,
                                            ReadingDate = isValidReadingDate ? Convert.ToDateTime(currentWorksheet.Cells[i, 9].Text) : DateTime.MinValue,
                                            CurrentReading = isValidReading ? (float)Convert.ToDouble(currentWorksheet.Cells[i, 10].Text) : 0,
                                            IsValidGreaterReadingDate = isValidReadingDate ? (Convert.ToDateTime(currentWorksheet.Cells[i, 9].Text) > DateTime.Now ? false : true) : false
                                        });
                                    }

                                    var q = lst.GroupBy(x => x.MeterNo)
                                                .Select(x => new
                                                {
                                                    Count = x.Count(),
                                                    Name = x.Key,
                                                    ID = x.First().MeterNo
                                                })
                                                .OrderByDescending(x => x.Count);

                                    var q2 = q.Where(x => x.Count == 1).ToList();

                                    if (lst != null && lst.Count > 0)
                                    {
                                        lstValid0 = lst.Where(x => x.IsValidReadingDate == true && x.IsValidReading == true && x.IsValidGreaterReadingDate == true).ToList();
                                        if (lstValid0 != null && lstValid0.Count > 0)
                                        {
                                            string joinedMeterNo = string.Join("','", lstValid0.Select(x => x.MeterNo).ToList());
                                            List<PMMeterMaster> lstPMMeter = PMMeterService.GetValidPMMeter(joinedMeterNo);
                                            lstValid0.Where(p => lstPMMeter.Select(ep => ep.MeterNo).Any(ep => ep == p.MeterNo)).ToList().ForEach(x => { x.IsValidMeterNo = true; });

                                            string joinedCityCode = string.Join("','", lstValid0.Select(x => x.L2Code).Distinct().ToList());
                                            List<L2> lstL2 = PMMeterService.GetValidCity(joinedCityCode);
                                            lstValid0.Where(p => lstL2.Select(ep => ep.L2Code).Any(ep => ep == p.L2Code)).ToList().ForEach(x => { x.IsValidCityCode = true; });

                                            lstValid0.Where(p => q2.Select(ep => ep.ID).Any(ep => ep == p.MeterNo)).ToList().ForEach(x => { x.IsNotDuplicateMeterNo = true; });

                                            lstValid = lstValid0.Where(x => x.IsValidMeterNo == true && x.IsValidCityCode == true && x.IsNotDuplicateMeterNo == true).ToList();

                                            using (ServiceContext context = new ServiceContext())
                                            {
                                                PMMeterMaster objPMMeterMaster = new PMMeterMaster();
                                                Reading objReading = new Reading();

                                                lstnew2 = (from c in lstValid
                                                           join d in context.SearchAll(objPMMeterMaster) on c.MeterNo equals d.MeterNo
                                                           join e in context.SearchAll(objReading) on d.MeterMasterID equals e.MeterMasterID into gj
                                                           from subpet in gj.DefaultIfEmpty()
                                                           select new { d.MeterMasterID, d.MeterNo, d.MeterType, d.MaxDailyIncrement, d.LastReading, c.CurrentReading, c.ReadingDate, c.L2Code, LastReadingDate = subpet == null ? null : subpet.ReadingDate }).ToList()
                                                                               .GroupBy(x => new { x.MeterMasterID, x.MeterNo, x.MeterType, x.MaxDailyIncrement, x.LastReading, x.CurrentReading, x.ReadingDate, x.L2Code })
                                                                               .Select(f => new PMMeterMaster()
                                                                               {
                                                                                   MeterMasterID = f.Key.MeterMasterID,
                                                                                   L2Code = f.Key.L2Code,
                                                                                   MeterNo = f.Key.MeterNo,
                                                                                   MeterType = f.Key.MeterType,
                                                                                   MaxDailyIncrement = f.Key.MaxDailyIncrement,
                                                                                   LastReading = f.Key.LastReading,
                                                                                   ReadingDate = f.Key.ReadingDate,
                                                                                   LastReadingDate = f.ToList().OrderByDescending(x => x.LastReadingDate).Select(x => x.LastReadingDate).FirstOrDefault(),
                                                                                   CurrentReading = f.Key.CurrentReading,
                                                                                   IsValidMeterNo = true,
                                                                                   IsValidCityCode = true,
                                                                                   IsNotDuplicateMeterNo = true,
                                                                                   IsGreaterReading = f.Key.CurrentReading > f.Key.LastReading ? true : false,
                                                                                   IsGreaterReadingDate = f.Key.ReadingDate > (f.ToList().OrderByDescending(x => x.LastReadingDate).Select(x => x.LastReadingDate).FirstOrDefault() == null ? DateTime.MinValue : f.ToList().OrderByDescending(x => x.LastReadingDate).Select(x => x.LastReadingDate).FirstOrDefault()) ? true : false
                                                                               }).ToList();
                                            }
                                        }

                                        List<PMMeterMaster> lstValid2 = new List<PMMeterMaster>();
                                        lstValid2 = lstnew2.Where(x => x.IsGreaterReading == true && x.IsGreaterReadingDate == true).ToList();

                                        if (lstValid2 != null && lstValid2.Count > 0)
                                        {
                                            ////PMMeterService > AddPMMeterReading function
                                            for (int i = 0; i < lstValid2.Count(); i++)
                                            {
                                                if (lstValid2[i].MeterType.ToLower() == "incremental")
                                                {
                                                    if (lstValid2[i].MaxDailyIncrement > 0)
                                                    {
                                                        var timeDiff = (lstValid2[i].LastReadingDate - lstValid2[i].ReadingDate).Value.TotalDays;
                                                        var readingDiff = lstValid2[i].LastReading - lstValid2[i].CurrentReading;
                                                        var avgDailyInc = readingDiff / timeDiff;
                                                        if (Convert.ToDouble(avgDailyInc) < Convert.ToDouble(lstValid2[i].MaxDailyIncrement))
                                                        {
                                                        }
                                                        else
                                                        {
                                                            lstValid2.Where(w => w.MeterNo == lstValid2[i].MeterNo).ToList().ForEach(s => s.IsAvgDailyInc = true);

                                                            Reading obj = new Reading();
                                                            obj.MeterMasterID = lstValid2[i].MeterMasterID;
                                                            obj.CurrentReading = lstValid2[i].CurrentReading;
                                                            obj.ReadingDate = lstValid2[i].ReadingDate;
                                                            obj.CreatedBy = Convert.ToString(ProjectSession.EmployeeID);
                                                            obj.CreatedDate = DateTime.Now;

                                                            int result = PMMeterService.AddPMMeterReadingExcel(obj, lstValid2[i].MeterType, lstValid2[i].LastReading);
                                                            ////int result = 1;
                                                            if (result == 1)
                                                            {
                                                                lstValid2.Where(w => w.MeterNo == lstValid2[i].MeterNo).ToList().ForEach(s => s.IsSuccess = true);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        lstValid2.Where(w => w.MeterNo == lstValid2[i].MeterNo).ToList().ForEach(s => s.IsAvgDailyInc = true);

                                                        Reading obj = new Reading();
                                                        obj.MeterMasterID = lstValid2[i].MeterMasterID;
                                                        obj.CurrentReading = lstValid2[i].CurrentReading;
                                                        obj.ReadingDate = lstValid2[i].ReadingDate;
                                                        obj.CreatedBy = Convert.ToString(ProjectSession.EmployeeID);
                                                        obj.CreatedDate = DateTime.Now;

                                                        int result = PMMeterService.AddPMMeterReadingExcel(obj, lstValid2[i].MeterType, lstValid2[i].LastReading);
                                                        ////int result = 1;
                                                        if (result == 1)
                                                        {
                                                            lstValid2.Where(w => w.MeterNo == lstValid2[i].MeterNo).ToList().ForEach(s => s.IsSuccess = true);
                                                        }
                                                    }
                                                }
                                                else if (lstValid2[i].MeterType.ToLower() == "threshold")
                                                {
                                                    Reading obj = new Reading();
                                                    obj.MeterMasterID = lstValid2[i].MeterMasterID;
                                                    obj.CurrentReading = lstValid2[i].CurrentReading;
                                                    obj.ReadingDate = lstValid2[i].ReadingDate;
                                                    obj.CreatedBy = Convert.ToString(ProjectSession.EmployeeID);
                                                    obj.CreatedDate = DateTime.Now;

                                                    int result = PMMeterService.AddPMMeterReadingExcel(obj, lstValid2[i].MeterType, lstValid2[i].LastReading);
                                                    ////int result = 1;
                                                    if (result == 1)
                                                    {
                                                        lstValid2.Where(w => w.MeterNo == lstValid2[i].MeterNo).ToList().ForEach(s => { s.IsSuccess = true; s.IsAvgDailyInc = true; });
                                                    }
                                                }
                                            }
                                        }

                                        string a = string.Empty;
                                        lst = (from c in lst
                                               join d in lstnew2 on c.MeterNo equals d.MeterNo into gj
                                               from subpet in gj.DefaultIfEmpty()
                                               join e in lstValid2 on c.MeterNo equals e.MeterNo into gj2
                                               from subpet2 in gj2.DefaultIfEmpty()
                                               select new PMMeterMaster()
                                               {
                                                   MeterMasterID = c.MeterMasterID,
                                                   L2Code = c.L2Code,
                                                   MeterNo = c.MeterNo,
                                                   MeterType = c.MeterType,
                                                   MaxDailyIncrement = c.MaxDailyIncrement,
                                                   LastReading = c.LastReading,
                                                   ReadingDate = c.ReadingDate,
                                                   LastReadingDate = c.LastReadingDate,
                                                   CurrentReading = c.CurrentReading,
                                                   IsValidReading = c.IsValidReading,
                                                   IsValidReadingDate = c.IsValidReadingDate,
                                                   IsValidGreaterReadingDate = c.IsValidGreaterReadingDate,
                                                   IsValidMeterNo = c.IsValidMeterNo,
                                                   IsValidCityCode = c.IsValidCityCode,
                                                   IsNotDuplicateMeterNo = c.IsNotDuplicateMeterNo,
                                                   IsGreaterReading = subpet == null ? false : subpet.IsGreaterReading,
                                                   IsGreaterReadingDate = subpet == null ? false : subpet.IsGreaterReadingDate,
                                                   IsSuccess = subpet2 == null ? false : subpet2.IsSuccess,
                                                   IsAvgDailyInc = subpet2 == null ? false : subpet2.IsAvgDailyInc
                                               }).ToList();
                                        ////Where(x => !(x.IsAvgDailyInc && x.IsGreaterReading && x.IsValidCityCode && x.IsValidMeterNo && x.IsGreaterReadingDate)).ToList()
                                        lst.ForEach(x =>
                                        {
                                            string msg = string.Empty;
                                            if (x.IsAvgDailyInc && x.IsGreaterReading && x.IsValidCityCode && x.IsValidMeterNo && x.IsGreaterReadingDate && x.IsNotDuplicateMeterNo)
                                            {
                                                if (!x.IsSuccess)
                                                {
                                                    msg += ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                                                }
                                            }
                                            else
                                            {
                                                if (!x.IsValidReading || !x.IsValidReadingDate)
                                                {
                                                    if (!x.IsValidReading)
                                                    {
                                                        msg = msg + ProjectSession.Resources.message.PMReading_MsgInvalidReading;
                                                    }

                                                    if (!x.IsValidReadingDate)
                                                    {
                                                        msg = msg == string.Empty ? ProjectSession.Resources.message.PMReading_MsgInvalidReadingDate : msg + " ; " + ProjectSession.Resources.message.PMReading_MsgInvalidReadingDate;
                                                    }
                                                }
                                                else if (!x.IsValidGreaterReadingDate)
                                                {
                                                    msg = msg + ProjectSession.Resources.message.Reading_MsgReadingDateLarger;
                                                }
                                                else
                                                {
                                                    if (!x.IsValidMeterNo || !x.IsValidCityCode || !x.IsNotDuplicateMeterNo)
                                                    {
                                                        if (!x.IsNotDuplicateMeterNo)
                                                        {
                                                            msg = msg + ProjectSession.Resources.message.PMReading_MsgDuplicateMeter;
                                                        }
                                                        else
                                                        {
                                                            if (!x.IsValidMeterNo)
                                                            {
                                                                msg = msg + ProjectSession.Resources.message.PMReading_MsgInvalidMeterNo;
                                                            }

                                                            if (!x.IsValidCityCode)
                                                            {
                                                                msg = msg == string.Empty ? ProjectSession.Resources.message.PMReading_MsgInvalidCity : msg + " ; " + ProjectSession.Resources.message.PMReading_MsgInvalidCity;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!x.IsGreaterReading || !x.IsGreaterReadingDate)
                                                        {
                                                            if (!x.IsGreaterReading)
                                                            {
                                                                msg = msg + ProjectSession.Resources.message.Reading_MsgCurrReadingLesser;
                                                            }

                                                            if (!x.IsGreaterReadingDate)
                                                            {
                                                                msg = msg == string.Empty ? ProjectSession.Resources.message.Reading_MsgReadingDateLesser : msg + " ; " + ProjectSession.Resources.message.Reading_MsgReadingDateLesser;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (!x.IsAvgDailyInc)
                                                            {
                                                                msg = msg == string.Empty ? ProjectSession.Resources.message.Reading_MsgMaxDailyLimitImport : msg + " ; " + ProjectSession.Resources.message.Reading_MsgMaxDailyLimitImport;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            x.Reason = msg;
                                        });
                                    }

                                    return Json(lst, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }

                    return Json(string.Empty, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSelectFileToUpload }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}