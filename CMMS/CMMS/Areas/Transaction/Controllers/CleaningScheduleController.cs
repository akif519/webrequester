﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using Microsoft.VisualBasic;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Cleaning Schedule Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Cleaning Schedule"

        /// <summary>
        /// Pm the schedule list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningScheduleList)]
        public ActionResult CleaningScheduleList()
        {
            if (ProjectSession.PermissionAccess.Cleaning_CleaningScheduleList_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                return View(Views.CleaningSchedule);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the pm schedules.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningSchedules)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetCleaningSchedules([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PMID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            whereClause = "  and pmschedule.IsCleaningModule= 1 ";

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and pmschedule.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
                /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (Location.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (Location.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and (Location.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) OR (Location.LocationID IS NULL))";
                }

                /*(END)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                whereClause += " and pmschedule.MaintSubDeptID in (" + ProjectConfiguration.SUBDEPTIDLISTPERMISSIONWISE + " ) ";
            }

            PmSchedule objPmSchedule = new PmSchedule();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPmSchedule, SystemEnum.Pages.CleaningSchedule.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm schedule by cleaning identifier.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMScheduleByCleaningID)]
        [HttpGet]
        public ActionResult GetPMScheduleByCleaningID(int timeID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            PmSchedule objPmSchedule = new PmSchedule();
            objPmSchedule.PMID = timeID;

            if (timeID == 0)
            {
                employees obj = new employees();
                Workpriority objWorkPriority = new Workpriority();
                Worktype objWorktype = new Worktype();
                Worktrade objWorktrade = new Worktrade();

                objWorkPriority.IsDefault = true;
                objWorktype.IsDefault = true;
                objWorktrade.IsDefault = true;

                using (ServiceContext context = new ServiceContext())
                {
                    obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                    objPmSchedule.MaintDivisionID = obj.MaintDivisionID;
                    objPmSchedule.MaintDeptID = obj.MaintDeptID;
                    objPmSchedule.MaintSubDeptID = obj.MaintSubDeptID;

                    objWorkPriority = context.Search<Workpriority>(objWorkPriority).FirstOrDefault();
                    if (objWorkPriority != null)
                    {
                        objPmSchedule.Workpriorityid = objWorkPriority.WorkPriorityID;
                    }

                    objPmSchedule.WorkTypeID = 2; ////Preventive

                    objWorktrade = context.Search<Worktrade>(objWorktrade).FirstOrDefault();
                    if (objWorktrade != null)
                    {
                        objPmSchedule.Worktradeid = objWorktrade.WorkTradeID;
                    }
                }

                objPmSchedule.Frequency = 1;
                objPmSchedule.PeriodDays = 1;
                objPmSchedule.TypePM = "1";
            }
            else
            {
                objPmSchedule = PMScheduleService.GetPmScheduleByCleaningId(timeID);
                if (objPmSchedule.TypePMgenID == 1 || objPmSchedule.TypePMgenID == 2)
                {
                    objPmSchedule.Days = objPmSchedule.FreqUnits * objPmSchedule.Frequency;
                }
                else if (objPmSchedule.TypePMgenID == 3)
                {
                    ////3.Fixed Day and Week
                    objPmSchedule.Month = Convert.ToDateTime(objPmSchedule.TargetStartDate).Month;
                    objPmSchedule.Year = Convert.ToDateTime(objPmSchedule.TargetStartDate).Year;

                    objPmSchedule.Frequency_Week = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Week = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Week = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Week = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Week = objPmSchedule.NextDate;
                }
                else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                {
                    objPmSchedule.Frequency_Month = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Month = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Month = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Month = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Month = objPmSchedule.NextDate;
                }
            }

            return View(Views.CleaningScheduleDetail, objPmSchedule);
        }

        /// <summary>
        /// Checks the cig reference exist.
        /// </summary>
        /// <param name="cleaningInspectionGroupId">The cleaning inspection group identifier.</param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public ActionResult CheckCIGRefExist(int cleaningInspectionGroupId)
        {
            using (ServiceContext context = new ServiceContext())
            {
                PmSchedule obj = new PmSchedule();
                obj.CIGID = cleaningInspectionGroupId;
                obj.IsCleaningModule = true;
                int count = context.Search<PmSchedule>(obj).ToList().Count;
                return Json(count, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the pm schedule.
        /// </summary>
        /// <param name="objPmSchedule">The object pm schedule.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningSchedule)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageCleaningSchedule(PmSchedule objPmSchedule)
        {
            ////Remaining logic of last genrated date
            string validations = string.Empty;
            int newScheduleID = 0;
            try
            {
                ////1.Group ,2.location
                if (objPmSchedule.TypePM == "1")
                {
                    objPmSchedule.PhyLocationID = null;
                }
                else if (objPmSchedule.TypePM == "2")
                {
                    objPmSchedule.CIGID = null;
                }

                if (objPmSchedule.TypePMgenID == 3)
                {
                    objPmSchedule.Frequency = objPmSchedule.Frequency_Week;
                    objPmSchedule.PeriodDays = objPmSchedule.PeriodDays_Week;
                    objPmSchedule.TargetStartDate = objPmSchedule.TargetStartDate_Week;
                    objPmSchedule.TargetCompDate = objPmSchedule.TargetCompDate_Week;
                    objPmSchedule.NextDate = objPmSchedule.NextDate_Week;
                }
                else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                {
                    objPmSchedule.Frequency = objPmSchedule.Frequency_Month;
                    objPmSchedule.PeriodDays = objPmSchedule.PeriodDays_Month;
                    objPmSchedule.TargetStartDate = objPmSchedule.TargetStartDate_Month;
                    objPmSchedule.TargetCompDate = objPmSchedule.TargetCompDate_Month;
                    objPmSchedule.NextDate = objPmSchedule.NextDate_Month;
                }

                objPmSchedule.IsCleaningModule = true;
                if (objPmSchedule.PMID > 0)
                {
                    objPmSchedule.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objPmSchedule.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objPmSchedule.WorkTypeID = 2; ////Preventive
                    objPmSchedule.PMActive = 1;
                    objPmSchedule.PMCounter = 1;
                    objPmSchedule.PMMultiple = 0;
                    objPmSchedule.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objPmSchedule.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        int count = PMScheduleService.GetCleaningScheduleByScheduleslistNo(objPmSchedule.PMID, objPmSchedule.PMNo);
                        if (count > 0)
                        {
                            if (objPmSchedule.PMID > 0)
                            {
                                TempData["Message"] = ProjectSession.Resources.message.PM_PMNoExists;
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                                ////return View(Views.PMScheduleDetail, objPmSchedule);
                                return RedirectToAction(Actions.GetPMScheduleByCleaningID, Pages.Controllers.Transaction, new { timeID = objPmSchedule.PMID });
                            }
                            else
                            {
                                ViewBag.Message = ProjectSession.Resources.message.PM_PMNoExists;
                                ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                                return View(Views.CleaningScheduleDetail, objPmSchedule);
                            }
                        }
                        else
                        {
                            newScheduleID = context.Save(objPmSchedule);
                        }
                    }

                    if (newScheduleID > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                        return RedirectToAction(Actions.GetPMScheduleByCleaningID, Pages.Controllers.Transaction, new { timeID = newScheduleID });
                    }
                    else
                    {
                        if (objPmSchedule.PMID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.PM_PMNoExists;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            ////return View(Views.PMScheduleDetail, objPmSchedule);
                            return RedirectToAction(Actions.GetPMScheduleByCleaningID, Pages.Controllers.Transaction, new { timeID = objPmSchedule.PMID });
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.PM_PMNoExists;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                            return View(Views.CleaningScheduleDetail, objPmSchedule);
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }

                    return RedirectToAction(Actions.GetPMScheduleByCleaningID, Pages.Controllers.Transaction, new { timeID = objPmSchedule.PMID });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}