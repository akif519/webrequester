﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Infrastructure;
using CMMS.Model;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Asset Tree 
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Assets the tree.
        /// </summary>
        /// <param name="isAddButtonVisible">True Or False</param>
        /// <param name="extraParam">Extra parameter</param>
        /// <param name="type">The type.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.AssetTree)]
        public ActionResult AssetTree(bool isAddButtonVisible = false, string extraParam = "", int type = 0, int cityID = 0)
        {
            ViewBag.isAddButtonVisible = isAddButtonVisible;
            ViewBag.ExtraParam = extraParam;
            ViewBag.Type = type;
            ViewBag.CityID = cityID;
            return View(Views.AssetTree);
        }

        /// <summary>
        /// Gets the asset tree.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cityId">The city identifier.</param>
        /// <returns></returns>
        ////[ActionName(Actions.GetAssetTree)]
        ////public JsonResult GetAssetTree(string id = null, int cityId = 0)
        ////{
        ////    string level = string.Empty;
        ////    int levelID = 0;
        ////    List<CustomAssetTree> lstTreeClass = new List<CustomAssetTree>();
        ////    IList<L1> lstL1;
        ////    IList<L2> lstL2;
        ////    IList<L3> lstL3;
        ////    IList<L4> lstL4;
        ////    IList<L5> lstL5;
        ////    IList<Location> lstLocation;
        ////    IList<Asset> lstAsset;

        ////    if (!string.IsNullOrEmpty(id))
        ////    {
        ////        level = id.Split(',')[0];
        ////        levelID = Convert.ToInt32(id.Split(',')[1]);
        ////    }

        ////    if (id == null)
        ////    {
        ////        lstL1 = AssetService.GetL1s(cityId, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID);

        ////        lstTreeClass = lstL1.Select(x => new CustomAssetTree() { ID = "L1," + x.L1ID, Name = x.L1No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L1AltName : x.L1Name), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////        ////GetL1s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }
        ////    else if (level == "L1")
        ////    {
        ////        lstL2 = AssetService.GetL2s(levelID, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, cityId);
        ////        lstTreeClass = lstL2.Select(x => new CustomAssetTree() { ID = "L2," + x.L2ID, Name = x.L2Code + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L2Altname : x.L2name), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////        ////GetL2s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }
        ////    else if (level == "L2")
        ////    {
        ////        if (ProjectSession.IsAreaFieldsVisible)
        ////        {
        ////            lstL3 = AssetService.GetL3s(levelID);
        ////            lstTreeClass = lstL3.Select(x => new CustomAssetTree() { ID = "L3," + x.L3ID, Name = x.L3No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L3AltDesc : x.L3Desc), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////            ////GetL3s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////            if (lstL3.Count == 0)
        ////            {
        ////                lstL4 = AssetService.GetL4sByL2Id(levelID);
        ////                lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();
        ////            }
        ////        }
        ////        else
        ////        {
        ////            lstL4 = AssetService.GetL4sByL2Id(levelID);
        ////            lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();

        ////            //// GetL4sByL2Id_Callback (Remaing : If Not (IsNothing(localAsset)) Then)
        ////        }
        ////    }
        ////    else if (level == "L3")
        ////    {
        ////        lstL4 = AssetService.GetL4s(levelID);
        ////        lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();

        ////        ////GetL4s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }
        ////    else if (level == "L4")
        ////    {
        ////        lstL5 = AssetService.GetL5s(levelID);
        ////        lstTreeClass = lstL5.Select(x => new CustomAssetTree() { ID = "L5," + x.L5ID, Name = x.L5No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L5AltDescription : x.L5Description), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////        ////GetL5s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////        if (lstL5.Count == 0)
        ////        {
        ////            lstLocation = AssetService.GetLocationsByL4Id(levelID);
        ////            lstTreeClass = lstLocation.Select(x => new CustomAssetTree() { ID = "Location," + x.LocationID, Name = x.LocationNo + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.LocationAltDescription : x.LocationDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////            ////GetLocationsByL4Id_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////        }
        ////    }
        ////    else if (level == "L5")
        ////    {
        ////        lstLocation = AssetService.GetLocationsByL5Id(levelID);
        ////        lstTreeClass = lstLocation.Select(x => new CustomAssetTree() { ID = "Location," + x.LocationID, Name = x.LocationNo + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.LocationAltDescription : x.LocationDescription), HasChild = x.AssetsCount > 0 ? true : false }).ToList();

        ////        ////GetLocationsByL5Id_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }
        ////    else if (level == "Location")
        ////    {
        ////        lstAsset = AssetService.GetAssetsByLocation(levelID);
        ////        lstTreeClass = lstAsset.Select(x => new CustomAssetTree() { ID = "Asset," + x.AssetID, Name = x.AssetNumber + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.AssetAltDescription : x.AssetDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////        ////GetAssetsByLocation_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }
        ////    else if (level == "Asset")
        ////    {
        ////        lstAsset = AssetService.GetAssetsByAsset(levelID);
        ////        lstTreeClass = lstAsset.Select(x => new CustomAssetTree() { ID = "Asset," + x.AssetID, Name = x.AssetNumber + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.AssetAltDescription : x.AssetDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

        ////        ////GetAssetsByAsset_Callback (Remining :  If Not (localAsset Is Nothing) Then)
        ////    }

        ////    return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
        ////}

        [ActionName(Actions.GetAssetTree)]
        public JsonResult GetAssetTree(string id = null, int cityId = 0)
        {
            string level = string.Empty;
            int levelID = 0;
            List<CustomAssetTree> lstTreeClass = new List<CustomAssetTree>();
            IList<L1> lstL1;
            IList<Sector> lstSector;
            IList<L2> lstL2;
            IList<L3> lstL3;
            IList<L4> lstL4;
            IList<L5> lstL5;
            IList<Location> lstLocation;
            IList<Asset> lstAsset;

            if (!string.IsNullOrEmpty(id))
            {
                level = id.Split(',')[0];
                levelID = Convert.ToInt32(id.Split(',')[1]);
            }

            if (id == null)
            {
                lstL1 = AssetService.GetL1s(cityId, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID);

                lstTreeClass = lstL1.Select(x => new CustomAssetTree() { ID = "L1," + x.L1ID, Name = x.L1No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L1AltName : x.L1Name), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////GetL1s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "L1")
            {
                lstSector = AssetService.GetSector(levelID, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, cityId);
                lstTreeClass = lstSector.Select(x => new CustomAssetTree() { ID = "Sector," + x.SectorID + ",L1," + levelID, Name = x.SectorCode + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.SectorAltname : x.SectorName), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////GetL2s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "Sector")
            {
                int intl1ID = Convert.ToInt32(id.Split(',')[3]);
                lstL2 = AssetService.GetL2s(levelID, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, cityId, intl1ID);
                lstTreeClass = lstL2.Select(x => new CustomAssetTree() { ID = "L2," + x.L2ID, Name = x.L2Code + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L2Altname : x.L2name), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////GetL2s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "L2")
            {
                if (ProjectSession.IsAreaFieldsVisible)
                {
                    ////lstL3 = AssetService.GetL3s(levelID);
                    ////////lstTreeClass = lstL3.Select(x => new CustomAssetTree() { ID = "L3," + x.L3ID, Name = x.L3No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L3AltDesc : x.L3Desc), HasChild = x.HasChild > 0 ? true : false }).ToList();
                    ////lstTreeClass = lstL3.Select(x => new CustomAssetTree() { ID = "L3," + x.L3ID, Name = x.L3No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L3AltDesc : x.L3Desc), HasChild = x.HasChild > 0 && x.HasChild1 > 0 ? true : false }).ToList();

                    ////////GetL3s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
                    ////if (lstL3.Count == 0)
                    ////{
                    ////    lstL4 = AssetService.GetL4sByL2Id(levelID);
                    ////    lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();
                    ////}

                    ////Juhi Start: 02-03-2017 : If Select area in zone then : City > area > zone > ..., If does not select area in zone then : City > zone > ..,
                    lstL3 = AssetService.GetL3sAndL4sByL2Id(levelID);
                    List<L3> lstl3new = lstL3.Where(x => x.IsL3Level == true).ToList();
                    if (lstl3new != null && lstl3new.Count > 0)
                    {
                        lstTreeClass = lstl3new.Select(x => new CustomAssetTree() { ID = "L3," + x.L3ID, Name = x.L3No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L3AltDesc : x.L3Desc), HasChild = x.HasChild > 0 && x.HasChild1 > 0 ? true : false }).ToList();
                    }

                    List<CustomAssetTree> lstTreeClass1 = new List<CustomAssetTree>();
                    List<L3> lstl4new = lstL3.Where(x => x.IsL3Level == false).ToList();
                    if (lstl4new != null && lstl4new.Count > 0)
                    {
                        lstTreeClass1 = lstl4new.Select(x => new CustomAssetTree() { ID = "L4," + x.L3ID, Name = x.L3No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L3AltDesc : x.L3Desc), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();
                    }

                    lstTreeClass.AddRange(lstTreeClass1);
                    ////Juhi End: 02-03-2017
                }
                else
                {
                    lstL4 = AssetService.GetL4sByL2Id(levelID);
                    lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();

                    //// GetL4sByL2Id_Callback (Remaing : If Not (IsNothing(localAsset)) Then)
                }
            }
            else if (level == "L3")
            {
                lstL4 = AssetService.GetL4s(levelID);
                lstTreeClass = lstL4.Select(x => new CustomAssetTree() { ID = "L4," + x.L4ID, Name = x.L4No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L4AltDescription : x.L4Description), HasChild = x.HasChild > 0 || x.HasChild1 > 0 ? true : false }).ToList();

                ////GetL4s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "L4")
            {
                ////lstL5 = AssetService.GetL5s(levelID);
                ////lstTreeClass = lstL5.Select(x => new CustomAssetTree() { ID = "L5," + x.L5ID, Name = x.L5No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L5AltDescription : x.L5Description), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////////GetL5s_Callback (Remining :  If Not (localAsset Is Nothing) Then)
                ////if (lstL5.Count == 0)
                ////{
                ////    lstLocation = AssetService.GetLocationsByL4Id(levelID);
                ////    lstTreeClass = lstLocation.Select(x => new CustomAssetTree() { ID = "Location," + x.LocationID, Name = x.LocationNo + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.LocationAltDescription : x.LocationDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////    ////GetLocationsByL4Id_Callback (Remining :  If Not (localAsset Is Nothing) Then)
                ////}

                ////Juhi Start: 02-03-2017 : If Select bulding in location then : City > area > zone > bulding > location  ..., If does not select area in zone then : City > area > zone > location..,
                lstL5 = AssetService.GetL5sAndLocationByL4Id(levelID);
                List<L5> lstl5new = lstL5.Where(x => x.IsL5Level == true).ToList();
                if (lstl5new != null && lstl5new.Count > 0)
                {
                    lstTreeClass = lstl5new.Select(x => new CustomAssetTree() { ID = "L5," + x.L5ID, Name = x.L5No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L5AltDescription : x.L5Description), HasChild = x.HasChild > 0 ? true : false }).ToList();
                }

                List<CustomAssetTree> lstTreeClass2 = new List<CustomAssetTree>();
                List<L5> lstlocationnew = lstL5.Where(x => x.IsL5Level == false).ToList();
                if (lstlocationnew != null && lstlocationnew.Count > 0)
                {
                    lstTreeClass2 = lstlocationnew.Select(x => new CustomAssetTree() { ID = "Location," + x.L5ID, Name = x.L5No + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.L5AltDescription : x.L5Description), HasChild = x.HasChild > 0 ? true : false }).ToList();
                }

                lstTreeClass.AddRange(lstTreeClass2);
                ////Juhi End: 02-03-2017
            }
            else if (level == "L5")
            {
                lstLocation = AssetService.GetLocationsByL5Id(levelID);
                lstTreeClass = lstLocation.Select(x => new CustomAssetTree() { ID = "Location," + x.LocationID, Name = x.LocationNo + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.LocationAltDescription : x.LocationDescription), HasChild = x.AssetsCount > 0 ? true : false }).ToList();

                ////GetLocationsByL5Id_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "Location")
            {
                lstAsset = AssetService.GetAssetsByLocation(levelID);
                lstTreeClass = lstAsset.Select(x => new CustomAssetTree() { ID = "Asset," + x.AssetID, Name = x.AssetNumber + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.AssetAltDescription : x.AssetDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////GetAssetsByLocation_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }
            else if (level == "Asset")
            {
                lstAsset = AssetService.GetAssetsByAsset(levelID);
                lstTreeClass = lstAsset.Select(x => new CustomAssetTree() { ID = "Asset," + x.AssetID, Name = x.AssetNumber + "/" + (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? x.AssetAltDescription : x.AssetDescription), HasChild = x.HasChild > 0 ? true : false }).ToList();

                ////GetAssetsByAsset_Callback (Remining :  If Not (localAsset Is Nothing) Then)
            }

            return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
        }
    }
}