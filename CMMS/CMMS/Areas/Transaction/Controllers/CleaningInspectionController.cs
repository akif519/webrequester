﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.IO;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;
using CMMS.Service;
using CMMS.Model;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// CleaningInspection Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "CleaningInspection"

        /// <summary>
        /// load page Cleaning Inspection
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspection)]
        public ActionResult CleaningInspection()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Cleaning_CleaningInspection_Allowaccess && ProjectSession.PermissionAccess.Cleaning__ShowHideModule)
            {
                return View(Views.CleaningInspection);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Get Cleaning Inspection List.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspectionList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult CleaningInspectionList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate";
                sortDirection = "Ascending";
            }

            if (!ProjectSession.IsCentral)
            {
                int empID = ProjectSession.EmployeeID;
                strWhere = " and cleaninginspectionmaster.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Cleaninginspectionmaster objCleaninginspectionmaster = new Cleaninginspectionmaster();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCleaninginspectionmaster, SystemEnum.Pages.CleaningInspectionList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// delete Cleaning Inspection
        /// </summary>
        /// <param name="cleaningInspectionId">Cleaning Inspection identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCleaningInspection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCleaningInspection(int cleaningInspectionId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Cleaninginspectionmaster>(cleaningInspectionId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Cleaning Inspection Edit"

        /// <summary>
        /// Cleaning Inspection Edit.
        /// </summary>
        /// <param name="cleaningInspectionId">Cleaning Inspection identifier</param>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspectionEdit)]
        public ActionResult CleaningInspectionEdit(int cleaningInspectionId)
        {
            if (ProjectSession.PermissionAccess.Cleaning_CleaningInspectionGroup_Createnewrecords || ProjectSession.PermissionAccess.Cleaning_CleaningInspectionGroup_Editrecords)
            {
                ViewBag.cleaningInspectionId = cleaningInspectionId;
                Cleaninginspectionmaster objCleaninginspectionmaster = new Cleaninginspectionmaster();
                if (cleaningInspectionId > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objCleaninginspectionmaster = objContext.SearchAllByID<Cleaninginspectionmaster>(objCleaninginspectionmaster, cleaningInspectionId).FirstOrDefault();
                    }

                    employees objemp = new employees();
                    using (ServiceContext context = new ServiceContext())
                    {
                        objemp = context.SearchAll(objemp).Where(m => m.EmployeeID == objCleaninginspectionmaster.InspectionBy).FirstOrDefault();
                    }

                    objCleaninginspectionmaster.EmployeeNO = objemp.EmployeeNO;
                    objCleaninginspectionmaster.Name = objemp.Name;
                    objCleaninginspectionmaster.AltName = objemp.AltName;
                }
                else
                {
                    objCleaninginspectionmaster.DateInspection = DateTime.Now;
                }

                return View(Views.CleaningInspectionEdit, objCleaninginspectionmaster);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Insert Update Cleaning Inspection
        /// </summary>
        /// <param name="objCleaninginspectionmaster">The Cleaning Inspection master</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningInspection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCleaningInspection(Cleaninginspectionmaster objCleaninginspectionmaster)
        {
            string validations = string.Empty;
            bool isNew = true;

            if (objCleaninginspectionmaster.CleaningInspectionId > 0)
            {
                objCleaninginspectionmaster.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaninginspectionmaster.ModifiedDate = DateTime.Now;
                isNew = false;
            }
            else
            {
                objCleaninginspectionmaster.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaninginspectionmaster.CreatedDate = DateTime.Now;
            }

            if (objCleaninginspectionmaster.CleaningInspectionId == 0)
            {
                Cleaninginspectionmaster objTempCI = new Cleaninginspectionmaster();
                using (DapperContext context = new DapperContext())
                {
                    objTempCI = context.SearchAll(objTempCI).Where(m => m.ReferenceNo.ToLower() == objCleaninginspectionmaster.ReferenceNo.ToLower()).FirstOrDefault();
                }

                if (objTempCI != null)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningInspection_MsgReferenceNoAlreadyExists }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("CleaningInspectionId"))
                {
                    int id = context.Save(objCleaninginspectionmaster);
                    if (id > 0)
                    {
                        if (isNew)
                        {
                            return Json(new object[] { "insert", SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, id }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningInspection_MsgReferenceNoAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Partial view CleaningList
        /// </summary>
        /// <param name="cleaningInspectionId">cleaning InspectionId</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.CleaningList)]
        public ActionResult CleaningList(int cleaningInspectionId)
        {
            Cleaninginspectiondetail obj = new Cleaninginspectiondetail();
            obj.CleaningInspectionId = cleaningInspectionId;
            obj.Floor = 3;
            obj.Wall = 3;
            obj.Window = 3;
            obj.Ceiling = 3;
            obj.Furniture = 3;
            obj.WasteBag = 3;
            obj.Consumables = 3;
            return View(PartialViews._CleaningList, obj);
        }

        /// <summary>
        /// bind Cleaning Details List grid.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cleaningInspectionId">The cleaningInspectionId</param>
        /// <returns></returns>
        [ActionName(Actions.CleaningDetailsList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult CleaningDetailsList([DataSourceRequest]DataSourceRequest request, int cleaningInspectionId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LocationNo";
                sortDirection = "Descending";
            }

            strWhere = " and CID.CleaningInspectionId = " + cleaningInspectionId;

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Cleaninginspectiondetail objCleaninginspectiondetail = new Cleaninginspectiondetail();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCleaninginspectiondetail, SystemEnum.Pages.CleaningList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets Cleaning Group List.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningGroupList)]
        public ActionResult GetCleaningGroupList()
        {
            Cleaninggroup objCleaningGroup = new Cleaninggroup();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objCleaningGroup);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Cleaning Location List.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningLocationList)]
        public ActionResult GetCleaningLocationList()
        {
            CleaningInspectionServices objService = new CleaningInspectionServices();
            var result = objService.GetCleaningLocationList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Insert Update Cleaning Inspection
        /// </summary>
        /// <param name="objCleaninginspectiondetail">The Cleaning details list</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageCleaningList(Cleaninginspectiondetail objCleaninginspectiondetail)
        {
            string validations = string.Empty;

            if (objCleaninginspectiondetail.CleaningInspectionDetailId > 0)
            {
                objCleaninginspectiondetail.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaninginspectiondetail.ModifiedDate = DateTime.Now;
            }
            else
            {
                objCleaninginspectiondetail.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaninginspectiondetail.CreatedDate = DateTime.Now;
            }

            Location objLocation = new Location();
            using (DapperContext context = new DapperContext())
            {
                objLocation = context.SearchAll(objLocation).Where(m => m.LocationID == objCleaninginspectiondetail.LocationID).FirstOrDefault();
            }

            Cleaningclassification objCleaningclassification = new Cleaningclassification();
            using (DapperContext context = new DapperContext())
            {
                objCleaningclassification = context.SearchAll(objCleaningclassification).Where(m => m.CleaningClassificationID == objLocation.CleaningClassificationId).FirstOrDefault();
            }

            int cleaningelementid = objCleaningclassification.CleaningElementID;

            Cleaninginspectionmaster objCleaninginspectionmaster = new Cleaninginspectionmaster();
            using (DapperContext context = new DapperContext())
            {
                objCleaninginspectionmaster = context.SearchAll(objCleaninginspectionmaster).Where(m => m.CleaningInspectionId == objCleaninginspectiondetail.CleaningInspectionId).FirstOrDefault();
            }

            CleaningWeightage objCleaningWeightage = new CleaningWeightage();
            using (DapperContext context = new DapperContext())
            {
                objCleaningWeightage = context.SearchAll(objCleaningWeightage).Where(m => m.L2ID == objCleaninginspectionmaster.L2ID).FirstOrDefault();
            }

            if (objCleaningWeightage == null)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningInspection_MsgSetCleaningWeightage }, JsonRequestBehavior.AllowGet);
            }

            objCleaninginspectiondetail.TotalPercentage = CalculateTotalPercentage(objCleaningWeightage, cleaningelementid, objCleaninginspectiondetail);

            if (objCleaninginspectiondetail.TotalPercentage >= objCleaningWeightage.SatisfactionLevel)
            {
                objCleaninginspectiondetail.SatisfactionResult = "Acceptable";
            }
            else
            {
                objCleaninginspectiondetail.SatisfactionResult = "Un Acceptable";
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("CleaningInspectionDetailId"))
                {
                    if (context.Save(objCleaninginspectiondetail) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// delete Cleaning Details
        /// </summary>
        /// <param name="cleaningInspectionDetailId">Cleaning Details identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCleaningDetails)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCleaningDetails(int cleaningInspectionDetailId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Cleaninginspectiondetail>(cleaningInspectionDetailId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Calculate Total Percentage
        /// </summary>
        /// <param name="objCleaningWeightage">the object CleaningWeightage </param>
        /// <param name="cleaningelementid">the cleaning element id </param>
        /// <param name="objCleaninginspectiondetail">the object Cleaning inspection detail</param>
        /// <returns></returns>
        private float CalculateTotalPercentage(CleaningWeightage objCleaningWeightage, int cleaningelementid, Cleaninginspectiondetail objCleaninginspectiondetail)
        {
            float result = 0;
            if ((objCleaningWeightage != null) && cleaningelementid > 0)
            {
                float floor = 0;
                float wall = 0;
                float window = 0;
                float ceiling = 0;
                float furniture = 0;
                float wasteBag = 0;
                float consumables = 0;
                if (cleaningelementid == 1)
                {
                    floor = objCleaninginspectiondetail.Floor * objCleaningWeightage.PublicFloor;
                    wall = objCleaninginspectiondetail.Wall * objCleaningWeightage.PublicWall;
                    window = objCleaninginspectiondetail.Window * objCleaningWeightage.PublicWindow;
                    ceiling = objCleaninginspectiondetail.Ceiling * objCleaningWeightage.PublicCeiling;
                    furniture = objCleaninginspectiondetail.Furniture * objCleaningWeightage.PublicFurniture;
                    wasteBag = objCleaninginspectiondetail.WasteBag * objCleaningWeightage.PublicWaste;
                    consumables = objCleaninginspectiondetail.Consumables * objCleaningWeightage.PublicConsumables;
                }
                else if (cleaningelementid == 2)
                {
                    floor = objCleaninginspectiondetail.Floor * objCleaningWeightage.RoomFloor;
                    wall = objCleaninginspectiondetail.Wall * objCleaningWeightage.RoomWall;
                    window = objCleaninginspectiondetail.Window * objCleaningWeightage.RoomWindow;
                    ceiling = objCleaninginspectiondetail.Ceiling * objCleaningWeightage.RoomCeiling;
                    furniture = objCleaninginspectiondetail.Furniture * objCleaningWeightage.RoomFurniture;
                    wasteBag = objCleaninginspectiondetail.WasteBag * objCleaningWeightage.RoomWaste;
                    consumables = objCleaninginspectiondetail.Consumables * objCleaningWeightage.RoomConsumables;
                }
                else if (cleaningelementid == 3)
                {
                    floor = objCleaninginspectiondetail.Floor * objCleaningWeightage.OutsideFloor;
                    wall = objCleaninginspectiondetail.Wall * objCleaningWeightage.OutsideWall;
                    window = objCleaninginspectiondetail.Window * objCleaningWeightage.OutsideWindow;
                    ceiling = objCleaninginspectiondetail.Ceiling * objCleaningWeightage.OutsideCeiling;
                    furniture = objCleaninginspectiondetail.Furniture * objCleaningWeightage.OutsideFurniture;
                    wasteBag = objCleaninginspectiondetail.WasteBag * objCleaningWeightage.OutsideWaste;
                    consumables = objCleaninginspectiondetail.Consumables * objCleaningWeightage.OutsideConsumables;
                }

                float total = (floor + wall + window + ceiling + furniture + wasteBag + consumables) / 400;

                result = total * 100;
            }

            return result;
        }

        /// <summary>
        /// Download Attachments 
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.DownloadCIAttachment)]
        public FileResult DownloadCIAttachment()
        {
            try
            {
                string fileName = "WI-386162-NUB Inspection Form V1.0.xlsx";
                string downloadName = fileName;
                string filePath = ProjectConfiguration.UploadPath + "Downloads//" + fileName;
                string contentType = "application/.xlsx";
                if (System.IO.File.Exists(filePath))
                {
                    return File(filePath, contentType, downloadName);
                }
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        #endregion

        #region "Cleaning Inspection Group"

        /// <summary>
        /// load page Cleaning Inspection group
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspectionGroup)]
        public ActionResult CleaningInspectionGroup()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Cleaning_CleaningInspectionGroup_Allowaccess && ProjectSession.PermissionAccess.Cleaning__ShowHideModule)
            {
                return View(Views.CleaningInspectionGroup);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Get Cleaning Inspection group List.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspectionGroupList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult CleaningInspectionGroupList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CleaningInspectionGroup objCleaningInspectionGroup = new CleaningInspectionGroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCleaningInspectionGroup, SystemEnum.Pages.CleaningInspectionGroupList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// delete Cleaning Inspection Group
        /// </summary>
        /// <param name="cleaningInspectionGroupId">Cleaning Inspection group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCleaningInspectionGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCleaningInspectionGroup(int cleaningInspectionGroupId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<CleaningInspectionGroup>(cleaningInspectionGroupId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Cleaning Inspection Group Edit"

        /// <summary>
        /// Cleaning Inspection Group Edit.
        /// </summary>
        /// <param name="cleaningInspectionGroupId">Cleaning Inspection Group identifier</param>
        /// <returns></returns>
        [ActionName(Actions.CleaningInspectionGroupEdit)]
        public ActionResult CleaningInspectionGroupEdit(int cleaningInspectionGroupId)
        {
            if (ProjectSession.PermissionAccess.Cleaning_CleaningInspectionGroup_Createnewrecords || ProjectSession.PermissionAccess.Cleaning_CleaningInspectionGroup_Editrecords)
            {
                ViewBag.cleaningInspectionGroupId = cleaningInspectionGroupId;
                CleaningInspectionGroup objCleaningInspectionGroup = new CleaningInspectionGroup();
                if (cleaningInspectionGroupId > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objCleaningInspectionGroup = objContext.SearchAllByID<CleaningInspectionGroup>(objCleaningInspectionGroup, cleaningInspectionGroupId).FirstOrDefault();
                    }
                }
                else
                {
                    objCleaningInspectionGroup.Status = true;
                }

                return View(Views.CleaningInspectionGroupEdit, objCleaningInspectionGroup);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Get Cleaning Inspection group sequence List.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cleaningInspectionGroupId">list by cleaningInspectionGroupId</param>
        /// <returns></returns>
        [ActionName(Actions.CIGroupSequenceList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult CIGroupSequenceList([DataSourceRequest]DataSourceRequest request, int cleaningInspectionGroupId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SequenceNumber";
                sortDirection = "Ascending";
            }

            strWhere = " and c.CleaningInspectionGroupId = " + cleaningInspectionGroupId;

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CleaningInspectionGroupSequenceNumber objCIG = new CleaningInspectionGroupSequenceNumber();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCIG, SystemEnum.Pages.CIGSequenceList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Insert Update Cleaning Inspection Group
        /// </summary>
        /// <param name="objCleaningInspectionGroup">The Cleaning Inspection Group</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningInspectionGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCleaningInspectionGroup(CleaningInspectionGroup objCleaningInspectionGroup)
        {
            string validations = string.Empty;
            bool isNew = true;

            if (objCleaningInspectionGroup.CleaningInspectionGroupId > 0)
            {
                objCleaningInspectionGroup.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaningInspectionGroup.ModifiedDate = DateTime.Now;
                isNew = false;
            }
            else
            {
                objCleaningInspectionGroup.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaningInspectionGroup.CreatedDate = DateTime.Now;
            }

            if (isNew)
            {
                CleaningInspectionGroup tempObjCleaningInspectionGroup = new CleaningInspectionGroup();
                using (DapperContext tempcontext = new DapperContext())
                {
                    tempObjCleaningInspectionGroup = tempcontext.SearchAll(tempObjCleaningInspectionGroup).Where(m => m.GroupNumber.ToLower() == objCleaningInspectionGroup.GroupNumber.ToLower()).FirstOrDefault();
                    if (tempObjCleaningInspectionGroup != null)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningSetup_MsgCleaningGroupAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("CleaningInspectionGroupId"))
                {
                    int id = context.Save(objCleaningInspectionGroup);
                    if (id > 0)
                    {
                        if (isNew)
                        {
                            return Json(new object[] { "insert", SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, id }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Add Edit Cleaning Inspection Group Sequence
        /// </summary>
        /// <param name="objOld">the old object</param>
        /// <param name="objNew">the new object</param>
        /// <param name="kendogriddata">the grid object</param>
        /// <returns></returns>
        [ActionName(Actions.AddEditCIGroupSequence)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddEditCIGroupSequence(CleaningInspectionGroupSequenceNumber objOld, CleaningInspectionGroupSequenceNumber objNew, string kendogriddata)
        {
            List<CleaningInspectionGroupSequenceNumber> lstSequenceNumbers = new List<CleaningInspectionGroupSequenceNumber>();
            lstSequenceNumbers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CleaningInspectionGroupSequenceNumber>>(kendogriddata);
            int cleaningInspectionGroupId = objNew.CleaningInspectionGroupId;

            CleaningInspectionServices objService = new CleaningInspectionServices();

            bool isSeqUpdated = false;
            int maxSeqId = 1;
            int minSeqId = 1;
            int difference = 0;

            if (lstSequenceNumbers.Count > 0)
            {
                maxSeqId = lstSequenceNumbers.Max(x => x.SequenceNumber);
                minSeqId = lstSequenceNumbers.Min(x => x.SequenceNumber);
            }

            if (objOld.SequenceNumber > -1 && (objNew.SequenceNumber != objOld.SequenceNumber) && objNew.SequenceNumber > maxSeqId && objOld.SequenceNumber == maxSeqId)
            {
                ////Updating Max Seq ID  Item With SeqID > Max ID, Then Do not update seqid, just update content
                objNew.SequenceNumber = objOld.SequenceNumber;
            }
            else
            {
                ////Adding/Updating Item with SeqID > MaxSeqID
                if (objNew.SequenceNumber > maxSeqId + 1)
                {
                    objNew.SequenceNumber = maxSeqId + 1;
                }
            }

            if (objOld.SequenceNumber > -1 && (objNew.SequenceNumber != objOld.SequenceNumber))
            {
                ////Update Sequence
                objOld = lstSequenceNumbers.Find(x => x.SequenceNumber == objOld.SequenceNumber && x.LocationId == objOld.LocationId);
                lstSequenceNumbers.Remove(objOld);
                objNew.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNew.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOld.SequenceNumber > -1 && (objNew.SequenceNumber == objOld.SequenceNumber))
            {
                objNew.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNew.ModifiedDate = DateTime.Now;
                objOld = lstSequenceNumbers.Find(x => x.SequenceNumber == objOld.SequenceNumber && x.LocationId == objOld.LocationId);
                lstSequenceNumbers.Remove(objOld);
            }
            else if (objOld.SequenceNumber == -1)
            {
                objNew.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNew.CreatedDate = DateTime.Now;
            }

            if (isSeqUpdated)
            {
                if (objOld.SequenceNumber < objNew.SequenceNumber)
                {
                    if (objNew.SequenceNumber > maxSeqId)
                    {
                        objNew.SequenceNumber = maxSeqId;
                    }

                    lstSequenceNumbers.Where(x => x.SequenceNumber > objOld.SequenceNumber && x.SequenceNumber <= objNew.SequenceNumber).ToList().ForEach(p =>
                    {
                        p.SequenceNumber = p.SequenceNumber - 1;
                    });
                }
                else if (objOld.SequenceNumber > objNew.SequenceNumber)
                {
                    if (objNew.SequenceNumber < minSeqId)
                    {
                        if (minSeqId > 1)
                        {
                            difference = minSeqId - objNew.SequenceNumber;
                            minSeqId = 1;

                            objNew.SequenceNumber = minSeqId;
                            lstSequenceNumbers.ForEach(p =>
                            {
                                p.SequenceNumber = p.SequenceNumber - difference;
                            });
                        }
                    }
                    else
                    {
                        lstSequenceNumbers.Where(x => x.SequenceNumber >= objNew.SequenceNumber && x.SequenceNumber < objOld.SequenceNumber).ToList().ForEach(p =>
                        {
                            p.SequenceNumber = p.SequenceNumber + 1;
                        });
                    }
                }
            }
            else
            {
                if (objOld.SequenceNumber == -1 && lstSequenceNumbers.Any(x => x.SequenceNumber == objNew.SequenceNumber))
                {
                    ////Inserting New Item in the list between any two Items
                    lstSequenceNumbers.Where(x => x.SequenceNumber >= objNew.SequenceNumber).ToList().ForEach(p =>
                    {
                        p.SequenceNumber = p.SequenceNumber + 1;
                    });
                }
                else
                {
                    if (lstSequenceNumbers.Count == 0)
                    {
                        objNew.SequenceNumber = minSeqId;
                    }
                }
            }

            lstSequenceNumbers.Add(objNew);

            bool result = objService.SaveWorkOrderSafetyInstrItem(cleaningInspectionGroupId, lstSequenceNumbers);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// delete Cleaning Inspection Group Sequence Number
        /// </summary>
        /// <param name="seqId">The sequence identifier.</param>
        /// <param name="cleaningInspectionGroupId">cleaning Inspection Group no.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCIGroupSequence)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCIGroupSequence(int seqId, int cleaningInspectionGroupId, string kendogriddata)
        {
            List<CleaningInspectionGroupSequenceNumber> lstSequenceNumbers = null;
            if (!string.IsNullOrEmpty(kendogriddata))
            {
                lstSequenceNumbers = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CleaningInspectionGroupSequenceNumber>>(kendogriddata);
            }
            else
            {
                lstSequenceNumbers = new List<CleaningInspectionGroupSequenceNumber>();
            }

            CleaningInspectionServices objService = new CleaningInspectionServices();
            CleaningInspectionGroupSequenceNumber objjobTaskToBeDeleted = new CleaningInspectionGroupSequenceNumber();

            if (lstSequenceNumbers.Any<CleaningInspectionGroupSequenceNumber>(m => m.SequenceNumber == seqId))
            {
                objjobTaskToBeDeleted = lstSequenceNumbers.Find(m => m.SequenceNumber == seqId);
                lstSequenceNumbers.Remove(objjobTaskToBeDeleted);

                lstSequenceNumbers.Where(m => m.SequenceNumber > seqId).ToList().ForEach(p =>
                {
                    p.SequenceNumber = p.SequenceNumber - 1;
                    p.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
            }

            bool result = objService.SaveWorkOrderSafetyInstrItem(cleaningInspectionGroupId, lstSequenceNumbers);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets employee list.
        /// </summary>
        /// <param name="text">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeAssignPage)]
        public ActionResult GetEmployeeAssignPage(string text)
        {
            var result = CleaningInspectionServices.GetEmployeeAuto(text);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}