﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Transaction Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Section List.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CEMULPage)]
        public ActionResult CEMULPage()
        {
            ////if (ProjectSession.PermissionAccess.Locations_Buildings_Allowaccess && ProjectSession.PermissionAccess.Locations__ShowHideModule)
            ////{
                return View(Views.CEMULPage);
            ////}
            ////else
            ////{
            ////    return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            ////}
        }

        #region "View Location Diagram"

        /// <summary>
        /// _s the view location diagram.
        /// </summary>
        /// <param name="locationId">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._ViewLocationDiagram)]
        public ActionResult _ViewLocationDiagram(int locationId)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = ConvertTo.String(locationId);
            
            return PartialView(PartialViews._ViewLocationDiagram, objExt);
        }

        /// <summary>
        /// Gets the view location diagram list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="foreignKeyID">The foreign key identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetViewLocationDiagramList)]
        public ActionResult GetViewLocationDiagramList([DataSourceRequest]DataSourceRequest request, string foreignKeyID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "FileName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            DocumentService obj = new DocumentService();

            var result = new DataSourceResult()
            {
                Data = obj.GetViewLocationDiagram(foreignKeyID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion 

        #region "Reading Graph"

        /// <summary>
        /// Gets the reading graph.
        /// </summary>
        /// <param name="meterMasterID">The meter master identifier.</param>
        /// <param name="meterType">Type of the meter.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("GetReadingGraph")]
        public ActionResult GetReadingGraph(int meterMasterID, string meterType, DateTime startDate, DateTime endDate)
        {
            ReadingGraph objReadingGraph = new ReadingGraph();
            List<ReadingGraph> lstReadingGraph = new List<ReadingGraph>();
            PMMeterService objService = new PMMeterService();
            lstReadingGraph = objService.ReadingGraph(ConvertTo.Integer(meterMasterID), meterType, startDate, endDate.AddDays(+1)).ToList();
            return Json(lstReadingGraph, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}