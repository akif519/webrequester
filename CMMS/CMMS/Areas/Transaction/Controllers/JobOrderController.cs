﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Model;
using CMMS.Reports;
using CMMS.DAL;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;
using CMMS.Service.ManagementService;
using Kendo.Mvc;
using System.Drawing;
using System.IO;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Job Order Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Asset Detail - JobOrder history Tab"
        /// <summary>
        /// _s the get job order by asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="isShowChildJOs">if set to <c>true</c> [is show child jos].</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderByAssetID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobOrderByAssetID([DataSourceRequest]DataSourceRequest request, int assetID, bool isShowChildJOs)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkorderNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService obj = new JobOrderService();

            string assetIDList = string.Empty;
            if (isShowChildJOs)
            {
                assetIDList = obj.GetAssetChilds(assetID);
            }
            else
            {
                assetIDList = Convert.ToString(assetID);
            }

            var result = new DataSourceResult()
            {
                Data = obj.GetJobOrderByAssetID(assetIDList, pageNumber, sortExpression, sortDirection),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the job order detail.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="isShowChildJOs">if set to <c>true</c> [is show child jos].</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetJobOrderDetail(int assetID, bool isShowChildJOs)
        {
            try
            {
                if (assetID > 0)
                {
                    JobOrderService obj = new JobOrderService();
                    string assetIDList = string.Empty;
                    if (isShowChildJOs)
                    {
                        assetIDList = obj.GetAssetChilds(assetID);
                    }
                    else
                    {
                        assetIDList = Convert.ToString(assetID);
                    }

                    IList<WorkOrder> lst = obj.GetJobOrderDetailByAssetID(assetIDList);
                    double totalManhours = 0.00;
                    double totalLabourCost = 0.00;
                    double totalDICost = 0.00;
                    double totalItemCost = 0.00;
                    double grandTotal = 0.00;
                    for (int i = 0; i < lst.Count; i++)
                    {
                        totalManhours = totalManhours + Convert.ToDouble(lst[i].ManHours);
                        totalLabourCost = totalLabourCost + Convert.ToDouble(lst[i].LabourCost);
                        totalDICost = totalDICost + Convert.ToDouble(lst[i].DICost);
                        totalItemCost = totalItemCost + Convert.ToDouble(lst[i].ItemCost);
                    }

                    grandTotal = totalLabourCost + totalDICost + totalItemCost;

                    return Json(new object[] { string.Format("{0:0.00}", totalManhours), string.Format("{0:#,0.00}", totalLabourCost), string.Format("{0:#,0.00}", totalDICost), string.Format("{0:#,0.00}", totalItemCost), string.Format("{0:#,0.00}", grandTotal) });
                }
                else
                {
                    return Json(new object[] { string.Format("{0:0.00}", 0), string.Format("{0:#,0.00}", 0), string.Format("{0:#,0.00}", 0), string.Format("{0:#,0.00}", 0), string.Format("{0:#,0.00}", 0) });
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets the job order count.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="locationID">The Location Identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderCountByLocationAndAseetID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public int GetJOCountByAssetIDLocationID(int assetID, int locationID)
        {
            string strWhereClauseWithEmployee = string.Empty;
            int empID = ProjectSession.EmployeeID;

            if (assetID > 0)
            {
                strWhereClauseWithEmployee = " and workorders.AssetID = " + assetID + " and WorkStatusID <> 2 and WorkStatusID <> 3 ";
            }
            else if (locationID > 0)
            {
                strWhereClauseWithEmployee = " and workorders.LocationID = " + locationID + " and WorkStatusID <> 2 and WorkStatusID <> 3 ";
            }

            if (!ProjectSession.IsCentral)
            {
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.L2ID in (select L2Id from employees_L2 where empID = " + empID + " union Select L2Id from employees where employeeId = " + empID + " ) ";
            }

            if (empID > 0)
            {
                strWhereClauseWithEmployee = strWhereClauseWithEmployee + " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + empID + " union Select MaintSubDeptID from employees where employeeId = " + empID + " ) ";
            }

            JobOrderService obj = new JobOrderService();

            return obj.GetJOCountByAssetIDLocationID(assetID, locationID, strWhereClauseWithEmployee);
        }

        /// <summary>
        /// To Get WorkRequest Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="assetID">Asset ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllJOByAssetID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetAllJOByAssetID([DataSourceRequest]DataSourceRequest request, int assetID)
        {
            IList<Worequest> lstBuildings = new List<Worequest>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (sortExpression == null || sortExpression == string.Empty)
                {
                    sortExpression = "WorkorderNo";
                }

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                JobOrderService objService = new JobOrderService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetAllJOByAssetID(assetID, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Get WorkRequest Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="assetID">Asset ID</param>
        /// <param name="locationID">Location ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetJOByAssetIDLocationID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetJOByAssetIDLocationID([DataSourceRequest]DataSourceRequest request, int assetID, int locationID)
        {
            IList<Worequest> lstBuildings = new List<Worequest>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (sortExpression == null || sortExpression == string.Empty)
                {
                    sortExpression = "WorkorderNo";
                }

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                JobOrderService objService = new JobOrderService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetJOByAssetIDLocationID(assetID, locationID, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Job Order"

        #region "Basic Information"

        /// <summary>
        /// Get job Order by Order No
        /// </summary>
        /// <param name="workOrderNo">Job Order No</param>
        /// <param name="assetID">Asset ID</param>
        /// <param name="objWorkOrder">The object work Order.</param>
        /// <param name="from">From Text.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJOByJobOrderNo)]
        [HttpGet]
        public ActionResult GetJOByJobOrderNo(string workOrderNo, int assetID = 0, WorkOrder objWorkOrder = null, string from = "")
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            ViewBag.FromPage = from;
            if (objWorkOrder != null && !string.IsNullOrEmpty(objWorkOrder.WorkorderNo) && !string.IsNullOrEmpty(objWorkOrder.ProblemDescription) && objWorkOrder.L2ID > 0)
            {
                return View(Views.JobOrders, objWorkOrder);
            }

            objWorkOrder = new WorkOrder();
            Asset objAsset = new Asset();
            JobOrderService objService = new JobOrderService();
            AssetService objAssetService = new AssetService();

            if (!string.IsNullOrEmpty(workOrderNo))
            {
                objWorkOrder = objService.GetJobOrderByOrderNo(workOrderNo);
            }

            if (assetID > 0)
            {
                objAsset = objAssetService.GetAssetPageWithAllRelatedData(ProjectSession.EmployeeID, string.Empty, 1, string.Empty, string.Empty, assetID).FirstOrDefault();

                if (objAsset != null && objAsset.AssetID > 0)
                {
                    if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now)
                    {
                        HomeController obj1 = new HomeController();
                        if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpired.GetHashCode()).Data.ToString();
                            }
                        }
                        else
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.ExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.Expired.GetHashCode()).Data.ToString();
                            }
                        }
                    }

                    objWorkOrder.AssetID = objAsset.AssetID;
                    objWorkOrder.AssetNumber = objAsset.AssetNumber;
                    objWorkOrder.AuthEmployeeName = objAsset.Name;
                    objWorkOrder.AuthorisedEmployeeId = objAsset.EmployeeID;
                    objWorkOrder.AssetDescription = objAsset.AssetDescription;
                    objWorkOrder.AssetAltDescription = objAsset.AssetAltDescription;

                    objWorkOrder.Asset = objAsset;

                    objWorkOrder.L2ID = objAsset.L2ID;
                    objWorkOrder.LocationID = objAsset.LocationID;
                    objWorkOrder.NoteToTechLocation = objAsset.NoteToTech;
                    objWorkOrder.LocationNo = objAsset.LocationNo;
                    objWorkOrder.LocationDescription = objAsset.LocationDescription;
                    objWorkOrder.LocationAltDescription = objAsset.LocationAltDescription;

                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        objWorkOrder.L3ID = objAsset.L3ID;
                    }

                    objWorkOrder.L4ID = Convert.ToInt32(objAsset.L4ID);
                    objWorkOrder.L4No = objAsset.L4No;

                    objWorkOrder.L5ID = Convert.ToInt32(objAsset.L5ID);
                    objWorkOrder.L5No = objAsset.L5No;
                    objWorkOrder.L5Description = objAsset.L5Description;
                    objWorkOrder.L5AltDescription = objAsset.L5AltDescription;
                }
            }

            if (objWorkOrder.WOPMtype == "1")
            {
                PmSchedule objPM = new PmSchedule();
                if (objWorkOrder.PMID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objPM = objContext.SelectObject<PmSchedule>(Convert.ToInt32(objWorkOrder.PMID));
                    }
                }

                objWorkOrder.PMSchedule = objPM;
            }
            else
            {
                PmMeter objPM = new PmMeter();
                if (objWorkOrder.MeterID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objPM = objContext.SelectObject<PmMeter>(Convert.ToInt32(objWorkOrder.MeterID));
                    }
                }

                objWorkOrder.PMMeter = objPM;
            }

            if (TempData["MessageType"] != null && TempData["MessageType"].ToString() != string.Empty && TempData["Message"] != null && TempData["Message"].ToString() != string.Empty)
            {
                ViewBag.MessageType = TempData["MessageType"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            return View(Views.JobOrders, objWorkOrder);

            ////if (objWorkRequest != null && !string.IsNullOrEmpty(objWorkRequest.RequestNo))
            ////{
            ////    return View(Views.JobRequests, objWorkRequest);
            ////}
            ////else
            ////{
            ////    ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
            ////    ViewBag.Message = "Job Request not found with request no " + requestNo;
            ////    return View(Views.JobRequestList);
            ////}
        }

        /// <summary>
        /// Get job Order by Order No
        /// </summary>
        /// <param name="workOrderNo">Job Order No</param>
        /// <param name="assetID">Asset ID</param>
        /// <param name="objWorkOrder">The object work Order.</param>
        /// <param name="from">From Text.</param>
        /// <returns></returns>
        [ActionName(Actions.GetFeedbackJOByJONo)]
        [HttpGet]
        public ActionResult GetFeedbackJOByJONo(string workOrderNo, int assetID = 0, WorkOrder objWorkOrder = null, string from = "")
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            ViewBag.FromPage = from;
            if (objWorkOrder != null && !string.IsNullOrEmpty(objWorkOrder.WorkorderNo) && !string.IsNullOrEmpty(objWorkOrder.ProblemDescription) && objWorkOrder.L2ID > 0)
            {
                return View(Views.JobOrders, objWorkOrder);
            }

            objWorkOrder = new WorkOrder();
            Asset objAsset = new Asset();
            JobOrderService objService = new JobOrderService();
            AssetService objAssetService = new AssetService();

            if (!string.IsNullOrEmpty(workOrderNo))
            {
                objWorkOrder = objService.GetJobOrderByOrderNo(workOrderNo);
            }

            if (assetID > 0)
            {
                objAsset = objAssetService.GetAssetPageWithAllRelatedData(ProjectSession.EmployeeID, string.Empty, 1, string.Empty, string.Empty, assetID).FirstOrDefault();

                if (objAsset != null && objAsset.AssetID > 0)
                {
                    if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now)
                    {
                        HomeController obj1 = new HomeController();
                        if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpired.GetHashCode()).Data.ToString();
                            }
                        }
                        else
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.ExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.Expired.GetHashCode()).Data.ToString();
                            }
                        }
                    }

                    objWorkOrder.AssetID = objAsset.AssetID;
                    objWorkOrder.AssetNumber = objAsset.AssetNumber;
                    objWorkOrder.AuthEmployeeName = objAsset.Name;
                    objWorkOrder.AuthorisedEmployeeId = objAsset.EmployeeID;
                    objWorkOrder.AssetDescription = objAsset.AssetDescription;
                    objWorkOrder.AssetAltDescription = objAsset.AssetAltDescription;

                    objWorkOrder.Asset = objAsset;

                    objWorkOrder.L2ID = objAsset.L2ID;
                    objWorkOrder.LocationID = objAsset.LocationID;
                    objWorkOrder.NoteToTechLocation = objAsset.NoteToTech;
                    objWorkOrder.LocationNo = objAsset.LocationNo;
                    objWorkOrder.LocationDescription = objAsset.LocationDescription;
                    objWorkOrder.LocationAltDescription = objAsset.LocationAltDescription;

                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        objWorkOrder.L3ID = objAsset.L3ID;
                    }

                    objWorkOrder.L4ID = Convert.ToInt32(objAsset.L4ID);
                    objWorkOrder.L4No = objAsset.L4No;

                    objWorkOrder.L5ID = Convert.ToInt32(objAsset.L5ID);
                    objWorkOrder.L5No = objAsset.L5No;
                    objWorkOrder.L5Description = objAsset.L5Description;
                    objWorkOrder.L5AltDescription = objAsset.L5AltDescription;
                }
            }

            if (objWorkOrder.WOPMtype == "1")
            {
                PmSchedule objPM = new PmSchedule();
                if (objWorkOrder.PMID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objPM = objContext.SelectObject<PmSchedule>(Convert.ToInt32(objWorkOrder.PMID));
                    }
                }

                objWorkOrder.PMSchedule = objPM;
            }
            else
            {
                PmMeter objPM = new PmMeter();
                if (objWorkOrder.MeterID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objPM = objContext.SelectObject<PmMeter>(Convert.ToInt32(objWorkOrder.MeterID));
                    }
                }

                objWorkOrder.PMMeter = objPM;
            }

            if (TempData["MessageType"] != null && TempData["MessageType"].ToString() != string.Empty && TempData["Message"] != null && TempData["Message"].ToString() != string.Empty)
            {
                ViewBag.MessageType = TempData["MessageType"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            return View(Views.FeedBack, objWorkOrder);
        }
        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="objWO">Object Of Work Order Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobOrder)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveJobOrder(WorkOrder objWO)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            string validations = string.Empty;

            try
            {
                if (ModelState.IsValid)
                {
                    objWO.LocationID = objWO.LocationID == 0 ? null : objWO.LocationID;
                    if (objWO.DateReceived != null && Convert.ToString(objWO.TimeReceived) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.DateReceived));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.TimeReceived));

                        objWO.DateReceived = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.DateRequired != null && Convert.ToString(objWO.TimeRequired) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.DateRequired));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.TimeRequired));
                        objWO.DateRequired = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (string.IsNullOrEmpty(objWO.WorkorderNo) || objWO.WorkorderNo == "0")
                    {
                        ////New Job Order
                        objWO.WorkStatusID = 1;
                        if (objWO.WorkTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode())
                        {
                            objWO.PMTarStartDate = objWO.DateReceived;
                            objWO.PMTarCompDate = objWO.DateReceived;
                        }

                        if (objWO.CIGID > 0)
                        {
                            objWO.IsCleaningModule = true;
                        }

                        objWO.WorkorderNo = objService.NewJobOrder(objWO);

                        if (!string.IsNullOrEmpty(objWO.WorkorderNo))
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                            return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = objWO.WorkorderNo, assetID = objWO.AssetID, objWorkOrder = objWO });
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = "0", assetID = "0", objWorkOrder = objWO });
                        }
                    }
                    else
                    {
                        ////Edit Job Order

                        objWO.ModifiedBy = ProjectSession.EmployeeID.ToString();
                        if (SaveJobOrderByExtend(objWO, "BASICINFO") > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                            return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = objWO.WorkorderNo, assetID = objWO.AssetID, objWorkOrder = objWO });
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = objWO.WorkorderNo, assetID = objWO.AssetID, objWorkOrder = objWO });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " </br>";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = "0", assetID = "0", objWorkOrder = objWO });
                    }
                    else
                    {
                        TempData["Message"] = validations;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = "0", assetID = "0", objWorkOrder = objWO });
                    }
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = "0", assetID = "0", objWorkOrder = objWO });
            }
        }

        /// <summary>
        /// Saves the job order by extend.
        /// </summary>
        /// <param name="currentJO">The current job Order.</param>
        /// <param name="tabName">Name of the tab.</param>
        /// <returns></returns>
        public int SaveJobOrderByExtend(WorkOrder currentJO, string tabName)
        {
            try
            {
                JobOrderService objService = new JobOrderService();
                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(currentJO.WorkorderNo, ProjectSession.EmployeeID.ToString());

                int id = 0;

                if (tabName.ToUpper() == "BASICINFO")
                {
                    id = objService.SaveJobOrderByExtend(currentJO);
                }
                else if (tabName.ToUpper() == "JOCLOSING")
                {
                    if (currentJO.WorkStatusID == SystemEnum.WorkRequestStatus.Closed.GetHashCode())
                    {
                        currentJO.WOclosebyID = ProjectSession.EmployeeID;
                    }

                    id = objService.SaveJobOrderClosingByExtend(currentJO);
                }
                else if(tabName.ToUpper()=="FEEDBACK")
                {
                    id=objService.SaveJobOrderFeedBackByExtend(currentJO);
                }

                ////Below if condition will update TargetStartDate for Actual pmschedule if workstatus is closed and worktype = preventive
                if ((id > 0) & currentJO.WorkStatusID == 2 & currentJO.WorkTypeID == 2)
                {
                    if (currentJO.PMID != null)
                    {
                        objService.UpdateTargetStartDateForActualPM(Convert.ToInt32(currentJO.PMID), currentJO.ActDateEnd.Value);
                    }
                }

                if (id == 2)
                {
                    ////WorkTypeID = 2 means Preventive
                    if (currentJO.WorkTypeID == 2)
                    {
                        return objService.NewJOStatusAudit(currentJO.WorkorderNo, Convert.ToInt32(currentJO.WorkStatusID), Convert.ToInt32(currentJO.ModifiedBy), "PM Module");
                    }
                    else
                    {
                        return objService.NewJOStatusAudit(currentJO.WorkorderNo, Convert.ToInt32(currentJO.WorkStatusID), Convert.ToInt32(currentJO.ModifiedBy), "Job Order Module");
                    }
                }

                return id;
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Change Job Order Status.
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <param name="jobStatus">The job status.</param>
        /// <param name="oldJobStatus">The old job status.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error Text</exception>
        [ActionName(Actions.ChangeJobOrderStatusByWorkorderNo)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ChangeJobOrderStatusByWorkorderNo(string workOrderNO, int jobStatus, int oldJobStatus)
        {
            try
            {
                JobOrderService objService = new JobOrderService();

                int id = -1;

                if (objService.ChangeJobOrderStatusByWorkorderNo(workOrderNO, jobStatus, oldJobStatus, ProjectSession.EmployeeID.ToString()) == true)
                {
                    id = objService.NewJOStatusAudit(workOrderNO, jobStatus, ProjectSession.EmployeeID, "Job Order Module");
                }

                if (id == 1)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all Ratings.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="time">The time.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ValidateDateInWorkingHours)]
        [HttpGet]
        public JsonResult ValidateDateInWorkingHours(string date, string time, int cityID)
        {
            DateTime d1 = System.DateTime.MinValue;
            DateTime d2 = System.DateTime.MinValue;
            JobOrderService objService = new JobOrderService();
            int reasonId = 0;
            bool result = false;

            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    d1 = Convert.ToDateTime(Common.GetEnglishDate(DateTime.Parse(date)), new System.Globalization.CultureInfo("en-US"));
                }

                if (!string.IsNullOrEmpty(time))
                {
                    d2 = Convert.ToDateTime(Common.GetEnglishDate(DateTime.Parse(time)), new System.Globalization.CultureInfo("en-US"));
                }
                else
                {
                    d2 = DateTime.MinValue;
                }

                if (d1 != System.DateTime.MinValue)
                {
                    result = objService.IsDateInWorkDaysAndTimeInWorkingHours(cityID, d1, d2, out reasonId);
                    return Json(new object[] { result, reasonId }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { result, reasonId }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { false, reasonId }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// get Latitude and Longitude of selected location id
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCoordinateByLocationID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetCoordinateByLocationID(int locationID)
        {
            Location objLocation = new Location();
            decimal latitude = 0;
            decimal longitude = 0;

            if (locationID > 0)
            {
                try
                {
                    using (ServiceContext objService = new ServiceContext())
                    {
                        objLocation = objService.SelectObject<Location>(locationID);
                        latitude = objLocation.Latitude;
                        longitude = objLocation.Longitude;
                    }

                    return Json(new object[] { latitude, longitude }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    CMMS.Models.Common.Log(ex);
                    return Json(new object[] { latitude, longitude }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { latitude, longitude }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Assign To Tab"

        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetWOAssignTO)]
        public ActionResult _PartialWOAssignTO(string workOrderNo)
        {
            Assigntoworkorder obj = null;
            Asset objAssetWarrentyOnfo = new Asset();
            JobOrderService objService = new JobOrderService();

            obj = objService.GetSupplierInfo(workOrderNo);

            if (obj == null)
            {
                obj = new Assigntoworkorder();
            }

            objAssetWarrentyOnfo = objService.GetAssetWarrentyInfo(workOrderNo);

            if (objAssetWarrentyOnfo != null)
            {
                if (objAssetWarrentyOnfo.Warranty_ContractID == 1)
                {
                    obj.AssetWarrentyMessage = ProjectSession.Resources.message.JobOrder_MsgAssetWarrentyStatus.Replace("[AssetNumber]", objAssetWarrentyOnfo.AssetNumber).Replace("[WarrStatus]", objAssetWarrentyOnfo.Warranty_contract).Replace("[ExpiryDate]", Convert.ToString(Common.GetEnglishDateOnly(objAssetWarrentyOnfo.Warranty_ContractExpiry)));
                }

                obj.AssetAuthEmployeeID = objAssetWarrentyOnfo.EmployeeID;
                obj.LocationAuthEmployeeID = objAssetWarrentyOnfo.LocationAuthEmpID;
            }

            obj.EstStartDate = DateTime.Now;
            obj.EstStartTime = DateTime.Now;
            obj.EstEndDate = DateTime.Now;
            obj.EstEndTime = DateTime.Now;
            return View(PartialViews.WOAssignTO, obj);
        }

        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWOAssignTOList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWOAssignTOList([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "employees.Name asc, employees.EmployeeNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Assigntoworkorder objWorkRequest = new Assigntoworkorder();
            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAssignToJOByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// saves employees to assign to work order table in database
        /// </summary>
        /// <param name="objWR">Object Of Assign To Work Order Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveAssignToWorkOrders)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveAssignToWorkOrders(Assigntoworkorder objWR)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (objWR.EstStartDate != null && Convert.ToString(objWR.EstStartDate) != "undefined")
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(objWR.EstStartDate));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(objWR.EstStartTime));

                    objWR.EstStartDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (objWR.EstEndDate != null && Convert.ToString(objWR.EstEndDate) != "undefined")
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(objWR.EstEndDate));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(objWR.EstEndTime));
                    objWR.EstEndDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (objWR.EstStartDate != null && objWR.EstEndDate != null && objWR.EstStartDate != DateTime.MinValue && objWR.EstEndDate != DateTime.MinValue)
                {
                    if (objWR.EstStartDate > objWR.EstEndDate)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgEstimatedEndDateTimeGreater });
                    }
                }

                if (objWR.EmployeeID > 0)
                {
                    objWR.MaintGroupID = 0;
                    objService.NewAssignToJO(objWR);
                    objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objWR.WorkorderNo, ProjectSession.EmployeeID.ToString());
                }
                else if (objWR.MaintGroupID > 0)
                {
                    objWR.EmployeeID = 0;
                    objService.NewAssignToJO(objWR);
                    objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objWR.WorkorderNo, ProjectSession.EmployeeID.ToString());
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the Assignment Of work Order.
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssignToWorkOrders)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssignToWorkOrders(int employeeID, string workOrderNo)
        {
            JobOrderService objService = new JobOrderService();
            Assigntoworkorder objAssigntoworkorder = new Assigntoworkorder();
            objAssigntoworkorder.EmployeeID = employeeID;
            objAssigntoworkorder.WorkorderNo = workOrderNo;

            bool result = objService.DeleteAssignToJOByWorkOrderNo(objAssigntoworkorder);
            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objAssigntoworkorder.WorkorderNo, ProjectSession.EmployeeID.ToString());
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="supplierId">The supplier identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveAssignToSupplierToWorkOrder)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveAssignToSupplierToWorkOrder(int supplierId, string workOrderNo)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (!ProjectSession.PermissionAccess.JobOrder_JobOrder_AssignEmployees)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "You do not have rights to assin employee to work order." }, JsonRequestBehavior.AllowGet);
                }

                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    if (objService.UpdateSupplierIDToWorkOrder(supplierId, workOrderNo))
                    {
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());
                        return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Work Order Number cannot be blank." }, JsonRequestBehavior.AllowGet);
                }

                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="currentWorkStatusId">The current work status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateWOStatusToAssigned)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult UpdateWOStatusToAssigned(string workOrderNo, string currentWorkStatusId)
        {
            try
            {
                if (Convert.ToInt32(currentWorkStatusId) != SystemEnum.WorkRequestStatus.Open.GetHashCode())
                {
                    return Json(new object[] { true, currentWorkStatusId, string.Empty }, JsonRequestBehavior.AllowGet);
                }

                JobOrderService objService = new JobOrderService();
                string workOrderNewStatusId = string.Empty;
                string assignedToStatusId = string.Empty;
                string newStatusText = string.Empty;
                assignedToStatusId = Common.GetConfigKeyValue("JobOrderAssignedToStatus");
                workOrderNewStatusId = string.IsNullOrEmpty(assignedToStatusId) ? currentWorkStatusId : assignedToStatusId;
                objService.ChangeJobOrderStatusByWorkorderNo(workOrderNo, Convert.ToInt32(workOrderNewStatusId), Convert.ToInt32(currentWorkStatusId), ProjectSession.EmployeeID.ToString());

                if (Convert.ToInt32(workOrderNewStatusId) != Convert.ToInt32(currentWorkStatusId))
                {
                    Workstatu objWorkStatus = new Workstatu();
                    objWorkStatus.WorkStatusID = Convert.ToInt32(workOrderNewStatusId);
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objWorkStatus = objContext.SelectObject<Workstatu>(Convert.ToInt32(workOrderNewStatusId));
                        newStatusText = ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode ? objWorkStatus.AltWorkStatus : objWorkStatus.WorkStatus;
                    }
                }

                return Json(new object[] { true, workOrderNewStatusId, newStatusText }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="currentWorkStatusId">The current work status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateWOStatusToClosed)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult UpdateWOStatusToClosed(string workOrderNo, string currentWorkStatusId)
        {
            try
            {
                if (IsJOValidToClose(workOrderNo))
                {
                    JobOrderService objService = new JobOrderService();
                    int workOrderNewStatusId = SystemEnum.WorkRequestStatus.Closed.GetHashCode();
                    objService.ChangeJobOrderStatusByWorkorderNo(workOrderNo, Convert.ToInt32(workOrderNewStatusId), Convert.ToInt32(currentWorkStatusId), ProjectSession.EmployeeID.ToString());

                    return Json(new object[] { true, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgPOPRAuthorisedToCloseJO }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Determines whether [is jo valid to close] [the specified work order no].
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        private bool IsJOValidToClose(string workOrderNo)
        {
            try
            {
                bool result = false;
                JobOrderService objService = new JobOrderService();
                result = objService.IsJOValidToClose(workOrderNo);
                return result;
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }
        #endregion

        #region "JO Closing Tab"

        /// <summary>
        /// Gets all Ratings.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllRatingsForDDL)]
        [HttpGet]
        public ActionResult GetAllRatingsForDDL()
        {
            Rating costCenter = new Rating();
            IList<Rating> lstratings = new List<Rating>();

            using (ServiceContext objService = new ServiceContext())
            {
                lstratings = objService.Search<Rating>(costCenter);
            }

            return Json(lstratings, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets all Ratings.
        /// </summary>
        /// <param name="date1">The date1.</param>
        /// <param name="time1">The time1.</param>
        /// <param name="date2">The date2.</param>
        /// <param name="time2">The time2.</param>
        /// <param name="extraParam">The extra parameter.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        [ActionName(Actions.CompareDateTime)]
        [HttpGet]
        public JsonResult CompareDateTime(string date1, string time1, string date2, string time2, string extraParam = "", int type = -1)
        {
            DateCompareResult objResult = new DateCompareResult();
            objResult.Type = type;
            DateTime d1 = System.DateTime.MinValue;
            DateTime d2 = System.DateTime.MinValue;
            try
            {
                if (date1 != null && time1 != null)
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(DateTime.Parse(date1)));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(DateTime.Parse(time1)));

                    d1 = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (date2 != null && time2 != null)
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(DateTime.Parse(date2)));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(DateTime.Parse(time2)));
                    d2 = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (d1 != System.DateTime.MinValue && d2 != System.DateTime.MinValue)
                {
                    int val = DateTime.Compare(d1, d2);
                    if (val < 0)
                    {
                        objResult.Result = DateCompareResult.CompareResult.Lesser.GetHashCode();
                    }
                    else if (val > 0)
                    {
                        objResult.Result = DateCompareResult.CompareResult.Greater.GetHashCode();
                    }
                    else
                    {
                        objResult.Result = DateCompareResult.CompareResult.Equal.GetHashCode();
                    }

                    if (objResult.Result == DateCompareResult.CompareResult.Lesser.GetHashCode())
                    {
                        objResult.DifferenceInMinutes = d2.Subtract(d1).TotalMinutes;
                    }

                    if (type == 0)
                    {
                        ////Date1 = EstDateStart And Date2 = EstDateEnd
                        if (objResult.Result == DateCompareResult.CompareResult.Greater.GetHashCode())
                        {
                            objResult.Message = Server.HtmlEncode(ProjectSession.Resources.message.JobOrder_MsgEstStartDateMustBeLessThanEstEndDate);
                        }
                        else
                        {
                            objResult.DifferenceInMinutes = d2.Subtract(d1).TotalMinutes;
                        }
                    }
                    else if (type == 1)
                    {
                        ////Date1 = ActStartDate And Date2 = ActEndDate, extraParam =  jobTypeId
                        if (Convert.ToInt32(extraParam) == SystemEnum.WorkType.Preventive.GetHashCode() || Convert.ToInt32(extraParam) == SystemEnum.WorkType.ManualPreventive.GetHashCode())
                        {
                            objResult.DifferenceInMinutes = 0.00;
                        }
                        else
                        {
                            if (objResult.Result == DateCompareResult.CompareResult.Lesser.GetHashCode())
                            {
                                objResult.DifferenceInMinutes = d2.Subtract(d1).TotalMinutes;
                            }
                            else
                            {
                                objResult.Message = Server.HtmlEncode(ProjectSession.Resources.message.JobOrder_MsgActEndDateTimeGreater);
                                objResult.DifferenceInMinutes = 0.00;
                            }
                        }
                    }

                    return Json(objResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResult.ErrorMessage = "Date cannot be null to compare.";
                    return Json(objResult, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                objResult.ErrorMessage = ex.Message.ToString();
                return Json(objResult, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Validate Job Order Closing Page
        /// </summary>
        /// <param name="workTypeID">The work type identifier.</param>
        /// <param name="pmaintenenceID">The pm identifier.</param>
        /// <param name="jobStatusId">The job status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ValidateJOClosing)]
        [HttpGet]
        public JsonResult ValidateJOClosing(int workTypeID, int pmaintenenceID, int jobStatusId)
        {
            try
            {
                PmSchedule objPmSchedule = new PmSchedule();
                string erorrMessage = string.Empty;

                if (jobStatusId == SystemEnum.WorkRequestStatus.Cancelled.GetHashCode())
                {
                    if (!ProjectSession.PermissionAccess.JobOrder_JobOrder_CancelJobOrder)
                    {
                        erorrMessage = ProjectSession.Resources.message.JobOrder_MsgCancelPMJO;
                    }

                    if (workTypeID == 2 && pmaintenenceID > 0 && erorrMessage != string.Empty)
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objPmSchedule = objContext.SelectObject<PmSchedule>(pmaintenenceID);
                        }

                        if (objPmSchedule.TypePMgenID == 2)
                        {
                            erorrMessage = ProjectSession.Resources.message.JobOrder_MsgCancelPMJO;
                        }
                    }
                }
                else if (jobStatusId == SystemEnum.WorkRequestStatus.Closed.GetHashCode())
                {
                    if (!ProjectSession.PermissionAccess.JobOrder_JobOrder_CloseJobOrder)
                    {
                        erorrMessage = ProjectSession.Resources.message.JobOrder_MsgDontPermissionToCloseJO;
                    }
                }

                if (erorrMessage == string.Empty)
                {
                    return Json(new object[] { true, erorrMessage }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { false, erorrMessage }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { false, ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// saves employees to assign to work order table in database
        /// </summary>
        /// <param name="objWO">Object Of Assign To Work Order Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJOFeedBack)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveJOFeedBack(WorkOrder objWO)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (!string.IsNullOrEmpty(objWO.WorkorderNo))
                {
                    if (objService.UpdateNotesToWorkOrder(objWO.WorkorderNo,objWO.Notes))
                    {
                        objWO.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    }
                }

                    if (SaveJobOrderByExtend(objWO, "FEEDBACK") <= 0)
                    {
                        throw new Exception("Error");
                    }
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    return RedirectToAction(Actions.GetFeedbackJOByJONo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workorderNo =objWO.WorkorderNo  });
                //return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// saves employees to assign to work order table in database
        /// </summary>
        /// <param name="objWO">Object Of Assign To Work Order Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJOClosing)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJOClosing(WorkOrder objWO)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (!ProjectSession.PermissionAccess.JobOrder_JobOrder_CloseJobOrder)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgDontPermissionToCloseJO }, JsonRequestBehavior.AllowGet);
                }

                if (objWO.WorkTypeID != null && Convert.ToInt32(objWO.WorkTypeID) > 0)
                {
                    if (objWO.AstartDate != null && Convert.ToString(objWO.AstartDate) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.AstartDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.AstartTime));

                        objWO.AstartDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.AEndDate != null && Convert.ToString(objWO.AEndDate) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.AEndDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.AEndTime));
                        objWO.AEndDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.EstDateStart != null && Convert.ToString(objWO.EstDateStart) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.EstDateStart));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.EstTimeStart));
                        objWO.EstDateStart = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.EstDateEnd != null && Convert.ToString(objWO.EstDateEnd) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.EstDateEnd));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.EstTimeEnd));
                        objWO.EstDateEnd = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.ActDateStart != null && Convert.ToString(objWO.ActDateStart) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.ActDateStart));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.ActTimeStart));
                        objWO.ActDateStart = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.ActDateEnd != null && Convert.ToString(objWO.ActDateEnd) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.ActDateEnd));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.ActTimeEnd));
                        objWO.ActDateEnd = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.PMTarStartDate != null && Convert.ToString(objWO.PMTarStartDate) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.PMTarStartDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.PMTarStartTime));
                        objWO.PMTarStartDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.PMTarCompDate != null && Convert.ToString(objWO.PMTarCompDate) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.PMTarCompDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.PMTarCompTime));
                        objWO.PMTarCompDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWO.DateHandover != null && Convert.ToString(objWO.DateHandover) != "undefined")
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWO.DateHandover));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWO.TimeHandover));
                        objWO.DateHandover = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    objWO.ModifiedBy = ProjectSession.EmployeeID.ToString();

                    if (objWO.WorkStatusID == SystemEnum.WorkRequestStatus.Closed.GetHashCode())
                    {
                        if (!IsJOValidToClose(objWO.WorkorderNo))
                        {
                            return Json(new object[] { "notvalidtoclose", SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgPOPRAuthorisedToCloseJO }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (SaveJobOrderByExtend(objWO, "JOCLOSING") <= 0)
                    {
                        throw new Exception("Error");
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobType_MsgJobTypeReq }, JsonRequestBehavior.AllowGet);
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Labor Tab"

        /// <summary>
        /// Partial view Job Order labor
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJoLabor)]
        public ActionResult _PartialJoClosing(string workOrderNo)
        {
            Workorderlabor objModel = new Workorderlabor();
            objModel.HourlySalary = 0;
            objModel.OverTime1 = 0;
            objModel.OverTime2 = 0;
            objModel.OverTime3 = 0;
            objModel.TotCost = 0;
            objModel.TotHour = 0;
            objModel.WorkorderNo = workOrderNo;
            return View(PartialViews.JOLabor, objModel);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderLabours)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderLabours([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkOrderLaborID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetLaborByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// saves employees to labor table in database
        /// </summary>
        /// <param name="objWOLabor">Object Of Assign To Work Order Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveWOLabor)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveWOLabor(Workorderlabor objWOLabor)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (objWOLabor.StartDate != null && Convert.ToString(objWOLabor.StartTime) != "undefined")
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(objWOLabor.StartDate));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(objWOLabor.StartTime));

                    objWOLabor.StartDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (objWOLabor.EndDate != null && Convert.ToString(objWOLabor.EndDate) != "undefined")
                {
                    string dt1 = Convert.ToString(Common.GetEnglishDate(objWOLabor.EndDate));
                    string dt2 = Convert.ToString(Common.GetEnglishDate(objWOLabor.EndTime));
                    objWOLabor.EndDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                }

                if (objWOLabor.StartDate != null && objWOLabor.EndDate != null && objWOLabor.StartDate != DateTime.MinValue && objWOLabor.EndDate != DateTime.MinValue)
                {
                    if (objWOLabor.StartDate > objWOLabor.EndDate)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgEndDateTimeGreater });
                    }
                }

                objWOLabor.CreatedBy = ProjectSession.EmployeeID.ToString();
                objWOLabor.CreatedDate = DateTime.Now;

                int workOrderLaborId = 0;
                if (objWOLabor.EmployeeID > 0)
                {
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        workOrderLaborId = objContext.Save<Workorderlabor>(objWOLabor);
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objWOLabor.WorkorderNo, ProjectSession.EmployeeID.ToString());
                    }
                }

                if (workOrderLaborId > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    throw new Exception("Error");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the Labor To Work Order.
        /// </summary>
        /// <param name="workOrderLaborID">The work order labor identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteWorkOrderLabour)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkOrderLabour(int workOrderLaborID, string workOrderNo)
        {
            JobOrderService objService = new JobOrderService();
            Workorderlabor objAssigntoworkorder = new Workorderlabor();
            objAssigntoworkorder.WorkorderLaborID = workOrderLaborID;

            bool result = objService.DeleteLaborToJOByWorkOrderNo(workOrderLaborID, workOrderNo);
            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Items Tab"

        /// <summary>
        /// Partial view Job Order Items
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJoItems)]
        public ActionResult _PartialJoItems()
        {
            return View(PartialViews.JOItems);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderIssues)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderIssues([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "IssueID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetIssuesByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderParts)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderParts([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ReturnID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetReturnByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <param name="workTypeID">The work type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderItems)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderItems([DataSourceRequest]DataSourceRequest request, string workOrderNo, int workTypeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "StockNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();
            IList<WorkOrderItem> lstItems = new List<WorkOrderItem>();

            if (workTypeID == SystemEnum.WorkType.Preventive.GetHashCode() || workTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode())
            {
                lstItems = objService.GetWorkOrderItemsByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request);
            }

            var result = new DataSourceResult()
            {
                Data = lstItems,
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <param name="workTypeID">The work type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderTools)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderTools([DataSourceRequest]DataSourceRequest request, string workOrderNo, int workTypeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkOrderToolID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();
            IList<WorkOrderTool> lstItems = new List<WorkOrderTool>();
            WorkOrderTool objTool = new WorkOrderTool();
            int total = 0;
            /*Removed below commented conditions, change requested by client(Muhnad) on 30-Nov-2016 Meeting
            ////if (workTypeID == SystemEnum.WorkType.Preventive.GetHashCode() || workTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode())
            ////{*/

            ////objTool.WorkOrderNo = workOrderNo;
            ////using (ServiceContext objContext = new ServiceContext())
            ////{
            ////    lstItems = objContext.Search<WorkOrderTool>(objTool);
            ////    total = objContext.PagingInformation.TotalRecords;
            ////}

            lstItems = objService.GetWOToolsByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request);
            total = objService.PagingInformation.TotalRecords;

            /*}*/

            var result = new DataSourceResult()
            {
                Data = lstItems,
                Total = total
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <param name="workTypeID">The work type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderPPE)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderPPE([DataSourceRequest]DataSourceRequest request, string workOrderNo, int workTypeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkOrderPPEID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            JobOrderService objService = new JobOrderService();
            IList<WorkOrderPPE> lstItems = new List<WorkOrderPPE>();
            WorkOrderPPE objPPE = new WorkOrderPPE();
            int total = 0;
            ////if (workTypeID == SystemEnum.WorkType.Preventive.GetHashCode() || workTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode())
            ////{
            ////    objPPE.WorkOrderNo = workOrderNo;
            ////    using (ServiceContext objContext = new ServiceContext())
            ////    {
            ////        lstItems = objContext.Search<WorkOrderPPE>(objPPE);
            ////        total = objContext.PagingInformation.TotalRecords;
            ////    }
            ////}

            lstItems = objService.GetPPEByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request);
            total = objService.PagingInformation.TotalRecords;

            var result = new DataSourceResult()
            {
                Data = lstItems,
                Total = total
            };

            return Json(result);
        }

        /// <summary>
        /// Partial view Job Order Issues
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="cityID">The l2 identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJoIssues)]
        public ActionResult _PartialJoIssues(string workOrderNo, int cityID = 0)
        {
            Issue objIssue = new Issue();
            objIssue.DateIssue = DateTime.Now;
            objIssue.L2ID = cityID;
            objIssue.WONo = workOrderNo;
            return View(PartialViews.JOIssues, objIssue);
        }

        /// <summary>
        /// Gets the list of all stock codes
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="showBalance">if set to <c>true</c> [show balance].</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodes)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetStockCodes([DataSourceRequest]DataSourceRequest request, bool showBalance, int subStoreID, string strWhere)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "StockID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            if (showBalance == false)
            {
                subStoreID = 0;
            }

            var result = new DataSourceResult()
            {
                Data = objService.GetStockCodePageWithBalance(subStoreID, strWhere, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the stock level balance
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="getQtyIssued">if set to <c>true</c> [get Quantity issued].</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockLevelBalance)]
        [HttpGet]
        public JsonResult GetStockLevelBalance(int stockID, int subStoreID, bool getQtyIssued = false)
        {
            JobOrderService objService = new JobOrderService();
            string stockBalance = string.Empty;
            string qtyIssued = string.Empty;
            decimal? totalQtyIssued = 0;
            decimal? totalQtyReturned = 0;
            Issue objIssue = new Issue();
            Return objReturn = new Return();
            try
            {
                stockBalance = Convert.ToString(objService.GetStockBalance(stockID, subStoreID));

                if (getQtyIssued)
                {
                    objIssue.StockID = stockID;
                    objIssue.SubStoreID = subStoreID;

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        totalQtyIssued = objContext.Search<Issue>(objIssue).Sum(x => x.QtyIssue);
                        totalQtyIssued = totalQtyIssued == null ? 0 : totalQtyIssued;
                    }

                    objReturn.StockID = stockID;
                    objReturn.SubStoreID = subStoreID;

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        totalQtyReturned = objContext.Search<Return>(objReturn).Sum(x => x.Qty);
                        totalQtyReturned = totalQtyReturned == null ? 0 : totalQtyReturned;
                    }

                    qtyIssued = string.Format("{0:0.00}", totalQtyIssued - totalQtyReturned);
                }

                return Json(new object[] { stockBalance, qtyIssued }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { string.Empty, string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves Issue For Job Order
        /// </summary>
        /// <param name="objIssue">Object Of Issue Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveJOIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJOIssue(Issue objIssue)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                int issueID = 0;

                if (string.IsNullOrEmpty(objIssue.WONo))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Work Order Number cannot be blank." }, JsonRequestBehavior.AllowGet);
                }

                if (objIssue.DateIssue != null && objIssue.DateIssue > DateTime.MinValue && objIssue.QtyIssue > 0)
                {
                    objIssue.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objIssue.CreatedDate = DateTime.Now;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        issueID = objContext.Save<Issue>(objIssue);
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objIssue.WONo, ProjectSession.EmployeeID.ToString());
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }

                if (issueID > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    throw new Exception("Error");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Partial view Job Order Returns
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="cityID">The l2 identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJoReturns)]
        public ActionResult _PartialJoReturns(string workOrderNo, int cityID = 0)
        {
            Return objReturn = new Return();
            objReturn.Date = DateTime.Now;
            objReturn.L2ID = cityID;
            objReturn.WONo = workOrderNo;

            return View(PartialViews.JOReturns, objReturn);
        }

        /// <summary>
        /// saves return parts For Job Order
        /// </summary>
        /// <param name="objReturn">Object Of Return Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveJOReturn)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJOReturn(Return objReturn)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                int issueID = 0;
                if (objReturn.Date != null && objReturn.Date > DateTime.MinValue && objReturn.Qty > 0 && (objReturn.Qty <= objReturn.QtyIssued))
                {
                    objReturn.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objReturn.CreatedDate = DateTime.Now;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        issueID = objContext.Save<Return>(objReturn);
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objReturn.WONo, ProjectSession.EmployeeID.ToString());
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. " + Environment.NewLine + " Quantity must be greater than zero. " + Environment.NewLine + " Qty Returned cannot be greater than Qty issued. " }, JsonRequestBehavior.AllowGet);
                }

                if (issueID > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    throw new Exception("Error");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Tools Tab"

        /// <summary>
        /// Partial view Job Order Items
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJoTools)]
        public ActionResult _PartialJoTools()
        {
            return View(PartialViews.JOTools);
        }

        /// <summary>
        /// saves tools For Job Order
        /// </summary>
        /// <param name="toolID">The tool identifier.</param>
        /// <param name="toolQuantity">The tool quantity.</param>
        /// <param name="workorderNo">The work order no.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveJOTools)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveJOTools(int toolID, int toolQuantity, string workorderNo)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                int workOrderToolID = 0;
                if (toolID > 0 && toolQuantity > 0 && !string.IsNullOrEmpty(workorderNo))
                {
                    WorkOrderTool objWOTool = new WorkOrderTool();
                    Tool objTool = new Tool();
                    objTool.ToolsID = toolID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objTool = objContext.Search<Tool>(objTool).FirstOrDefault();
                    }

                    if (objTool != null && objTool.ToolsID > 0 && !string.IsNullOrEmpty(objTool.ToolsDesc))
                    {
                        objWOTool.WorkOrderNo = workorderNo;
                        objWOTool.ToolDescription = objTool.ToolsDesc;
                        objWOTool.AltToolDescription = objTool.ToolsAltDesc;
                        objWOTool.Quantity = toolQuantity;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            workOrderToolID = objContext.Save<WorkOrderTool>(objWOTool);
                            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workorderNo, ProjectSession.EmployeeID.ToString());
                        }
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "WorkOrder No cannot be blank. " + Environment.NewLine + "Tool is required field. " + Environment.NewLine + " Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }

                if (workOrderToolID > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    throw new Exception("Error");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the job tool Item of work Order.
        /// </summary>
        /// <param name="workOrderToolId">The work order tool identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteWOToolItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWOToolItem(int workOrderToolId)
        {
            int result = 0;
            JobOrderService objService = new JobOrderService();
            using (ServiceContext objContext = new ServiceContext())
            {
                WorkOrderTool objWOTool = new WorkOrderTool();
                objWOTool.WorkOrderToolID = workOrderToolId;
                objWOTool = objContext.Search<WorkOrderTool>(objWOTool).FirstOrDefault();
                result = objContext.Delete<WorkOrderTool>(workOrderToolId);
                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objWOTool.WorkOrderNo, ProjectSession.EmployeeID.ToString());
            }

            if (result == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "JoCost Tab"

        /// <summary>
        /// the partial job order Cost.
        /// </summary>
        /// <param name="workorderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJobOrderCost)]
        public ActionResult _JOCost(string workorderNo)
        {
            WorkOrder objWorkOrder = new WorkOrder();
            objWorkOrder.WorkorderNo = workorderNo;
            if (!string.IsNullOrEmpty(workorderNo))
            {
                Workorderlabor objWorkOrderlabour = new Workorderlabor();
                Return objWorkOrderReturn = new Return();
                Issue objWorkOrderIssue = new Issue();
                Direct objWorkOrderDirectIssue = new Direct();
                objWorkOrderlabour.WorkorderNo = objWorkOrder.WorkorderNo;
                objWorkOrderReturn.WONo = objWorkOrder.WorkorderNo;
                objWorkOrderIssue.WONo = objWorkOrder.WorkorderNo;
                objWorkOrderDirectIssue.WONo = objWorkOrder.WorkorderNo;

                decimal? worklabourCost = 0;
                decimal? workDICost = 0;
                decimal? workIssueCost = 0;
                decimal? workReurnCost = 0;

                using (ServiceContext objContext = new ServiceContext())
                {
                    worklabourCost = objContext.Search<Workorderlabor>(objWorkOrderlabour).Sum(x => x.TotCost);
                    worklabourCost = worklabourCost == null ? 0 : worklabourCost;

                    workReurnCost = objContext.Search<Return>(objWorkOrderReturn).Sum(x => x.Qty * x.RetPrice);
                    workReurnCost = workReurnCost == null ? 0 : workReurnCost;

                    workIssueCost = objContext.Search<Issue>(objWorkOrderIssue).Sum(x => x.QtyIssue * x.Price3);
                    workIssueCost = workIssueCost == null ? 0 : workIssueCost;

                    workDICost = objContext.Search<Direct>(objWorkOrderDirectIssue).Sum(x => x.Qty * x.Price);
                    workDICost = workDICost == null ? 0 : workDICost;

                    objWorkOrder.WOLaborCost = (float)worklabourCost;
                    objWorkOrder.WODICost = (float)workDICost;
                    objWorkOrder.WOPartCost = (float)(workIssueCost - workReurnCost);
                    objWorkOrder.WOCost = objWorkOrder.WOLaborCost + objWorkOrder.WODICost + objWorkOrder.WOPartCost;
                }
            }

            return View(PartialViews.JOCost, objWorkOrder);
        }

        #endregion

        #region "Notes Tab"

        /// <summary>
        /// the partial job order notes.
        /// </summary>
        /// <param name="objWorkOrder">The object work order.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJobOrderNotes)]
        public ActionResult _PartialJoNotes(WorkOrder objWorkOrder)
        {
            return View(PartialViews.JONotes, objWorkOrder);
        }

        /// <summary>
        /// saves work order Notes
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="notes">The notes.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJoNotes)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveJoNotes(string workOrderNo, string notes)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                if (!string.IsNullOrEmpty(workOrderNo))
                {
                    if (objService.UpdateNotesToWorkOrder(workOrderNo, notes))
                    {
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());
                        return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Job Plan Tab"

        /// <summary>
        /// the partial job order JOPlan.
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJobOrderJOPlan)]
        public ActionResult _PartialJoNotes(string workOrderNO)
        {
            Jobtask objJobTask = new Jobtask();
            objJobTask.WoNo = workOrderNO;
            return View(PartialViews.JOPlan, objJobTask);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderJOPlanItems)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderJOPlanItems([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetJobTaskItemsByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all job Plans.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderJOPlanList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderJOPlanList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            TblJobPlan objJobPlan = new TblJobPlan();
            DataSourceResult result = null;

            if (ProjectSession.IsCentral)
            {
                SearchFieldService obj = new SearchFieldService();
                result = new DataSourceResult()
                {
                    Data = obj.AdvanceSearch(objJobPlan, SystemEnum.Pages.JobPlan.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
            }
            else
            {
                result = new DataSourceResult()
                {
                    Data = objService.GetJobPlansByEmployee(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Deletes the job Plan Item of work Order.
        /// </summary>
        /// <param name="seqId">The sequence identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJoPlanItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJoPlanItem(int seqId, string workOrderNo, string kendogriddata)
        {
            List<Jobtask> lstJobPlanItems = null;
            if (!string.IsNullOrEmpty(kendogriddata))
            {
                lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Jobtask>>(kendogriddata);
            }
            else
            {
                lstJobPlanItems = new List<Jobtask>();
            }

            JobOrderService objService = new JobOrderService();
            Jobtask objjobTaskToBeDeleted = new Jobtask();

            if (lstJobPlanItems.Any<Jobtask>(m => m.SeqId == seqId))
            {
                objjobTaskToBeDeleted = lstJobPlanItems.Find(m => m.SeqId == seqId);
                lstJobPlanItems.Remove(objjobTaskToBeDeleted);

                lstJobPlanItems.Where(m => m.SeqId > seqId).ToList().ForEach(p =>
                {
                    p.SeqId = p.SeqId - 1;
                    p.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
            }

            bool result = objService.SaveWorkOrderJobPlanItem(workOrderNo, lstJobPlanItems);
            if (result)
            {
                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Copy job Plan Items To Work Order job task.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.CopyJobPlanItemsToWorkOrder)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CopyJobPlanItemsToWorkOrderNo(int jobPlanId, string workOrderNo)
        {
            JobOrderService objService = new JobOrderService();
            bool result = objService.CopyJobPlanItemsToWorkOrderNo(jobPlanId, workOrderNo);
            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the job Plan Item of work Order.
        /// </summary>
        /// <param name="objOldJobtask">The object old job task.</param>
        /// <param name="objNewJobtask">The object new job task.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagejobTask)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ManagejobTask(Jobtask objOldJobtask, Jobtask objNewJobtask, string kendogriddata)
        {
            List<Jobtask> lstJobPlanItems = new List<Jobtask>();
            lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Jobtask>>(kendogriddata);

            JobOrderService objService = new JobOrderService();

            bool isSeqUpdated = false;
            int maxSeqId = 1;
            int minSeqId = 1;
            int difference = 0;

            if (lstJobPlanItems.Count > 0)
            {
                maxSeqId = lstJobPlanItems.Max(x => x.SeqId);
                minSeqId = lstJobPlanItems.Min(x => x.SeqId);
            }

            if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId != objOldJobtask.SeqId) && objNewJobtask.SeqId > maxSeqId && objOldJobtask.SeqId == maxSeqId)
            {
                ////Updating Max Seq ID  Item With SeqID > Max ID, Then Do not update seqid, just update content
                objNewJobtask.SeqId = objOldJobtask.SeqId;
            }
            else
            {
                ////Adding/Updating Item with SeqID > MaxSeqID
                if (objNewJobtask.SeqId > maxSeqId + 1)
                {
                    objNewJobtask.SeqId = maxSeqId + 1;
                }
            }

            if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId != objOldJobtask.SeqId))
            {
                ////Update Sequence
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqId == objOldJobtask.SeqId && x.Details == objOldJobtask.Details);
                lstJobPlanItems.Remove(objOldJobtask);
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId == objOldJobtask.SeqId))
            {
                ////Updating just content, not seq
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqId == objOldJobtask.SeqId && x.Details == objOldJobtask.Details);
                lstJobPlanItems.Remove(objOldJobtask);
            }
            else if (objOldJobtask.SeqId == -1)
            {
                ////Adding New Item
                objNewJobtask.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.CreatedDate = DateTime.Now;
            }

            if (isSeqUpdated)
            {
                if (objOldJobtask.SeqId < objNewJobtask.SeqId)
                {
                    if (objNewJobtask.SeqId > maxSeqId)
                    {
                        objNewJobtask.SeqId = maxSeqId;
                    }

                    lstJobPlanItems.Where(x => x.SeqId > objOldJobtask.SeqId && x.SeqId <= objNewJobtask.SeqId).ToList().ForEach(p =>
                    {
                        p.SeqId = p.SeqId - 1;
                    });
                }
                else if (objOldJobtask.SeqId > objNewJobtask.SeqId)
                {
                    if (objNewJobtask.SeqId < minSeqId)
                    {
                        if (minSeqId > 1)
                        {
                            difference = minSeqId - objNewJobtask.SeqId;
                            minSeqId = 1;

                            objNewJobtask.SeqId = minSeqId;
                            lstJobPlanItems.ForEach(p =>
                            {
                                p.SeqId = p.SeqId - difference;
                            });
                        }
                    }
                    else
                    {
                        lstJobPlanItems.Where(x => x.SeqId >= objNewJobtask.SeqId && x.SeqId < objOldJobtask.SeqId).ToList().ForEach(p =>
                        {
                            p.SeqId = p.SeqId + 1;
                        });
                    }
                }
            }
            else
            {
                if (objOldJobtask.SeqId == -1 && lstJobPlanItems.Any(x => x.SeqId == objNewJobtask.SeqId))
                {
                    ////Inserting New Item in the list between any two Items
                    lstJobPlanItems.Where(x => x.SeqId >= objNewJobtask.SeqId).ToList().ForEach(p =>
                    {
                        p.SeqId = p.SeqId + 1;
                    });
                }
                else
                {
                    if (lstJobPlanItems.Count == 0)
                    {
                        objNewJobtask.SeqId = minSeqId;
                    }
                }
            }

            lstJobPlanItems.Add(objNewJobtask);

            bool result = objService.SaveWorkOrderJobPlanItem(objNewJobtask.WoNo, lstJobPlanItems);
            if (result)
            {
                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objNewJobtask.WoNo, ProjectSession.EmployeeID.ToString());
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Safety Instructions Tab"

        /// <summary>
        /// the partial job order Safety Instruction.
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJobOrderJOSafetyInstr)]
        public ActionResult _PartialJoSafetyInstruction(string workOrderNO)
        {
            SITask objSITask = new SITask();
            objSITask.WoNo = workOrderNO;
            return View(PartialViews.JOSafetyInstructions, objSITask);
        }

        /// <summary>
        /// Gets the list of all labors
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderJOSafetyInstrItems)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderJOSafetyInstrItems([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetSafetyInstructionByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all job Plans.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderSafetyInstrList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderSafetyInstrList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            TblSafetyinstruction objSafetyInstr = new TblSafetyinstruction();
            DataSourceResult result = null;

            if (ProjectSession.IsCentral)
            {
                SearchFieldService obj = new SearchFieldService();
                result = new DataSourceResult()
                {
                    Data = obj.AdvanceSearch(objSafetyInstr, SystemEnum.Pages.SafetyInstruction.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
            }
            else
            {
                result = new DataSourceResult()
                {
                    Data = objService.GetSafetyInstrsByEmployee(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Deletes the job Plan Item of work Order.
        /// </summary>
        /// <param name="seqId">The sequence identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletWoSafetyInstrItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletWoSafetyInstrItem(int seqId, string workOrderNo, string kendogriddata)
        {
            List<SITask> lstJobPlanItems = null;
            if (!string.IsNullOrEmpty(kendogriddata))
            {
                lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SITask>>(kendogriddata);
            }
            else
            {
                lstJobPlanItems = new List<SITask>();
            }

            JobOrderService objService = new JobOrderService();
            SITask objjobTaskToBeDeleted = new SITask();

            if (lstJobPlanItems.Any<SITask>(m => m.SeqId == seqId))
            {
                objjobTaskToBeDeleted = lstJobPlanItems.Find(m => m.SeqId == seqId);
                lstJobPlanItems.Remove(objjobTaskToBeDeleted);

                lstJobPlanItems.Where(m => m.SeqId > seqId).ToList().ForEach(p =>
                {
                    p.SeqId = p.SeqId - 1;
                    p.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    p.ModifiedDate = DateTime.Now;
                });
            }

            bool result = objService.SaveWorkOrderSafetyInstrItem(workOrderNo, lstJobPlanItems);
            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Copy job Plan Items To Work Order job task.
        /// </summary>
        /// <param name="safetyId">The job plan identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.CopySafetyInstrItemsToWorkOrder)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CopySafetyInstrItemsToWorkOrder(int safetyId, string workOrderNo)
        {
            JobOrderService objService = new JobOrderService();
            bool result = objService.CopySafetyInstrItemsToWorkOrder(safetyId, workOrderNo);

            if (result)
            {
                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(workOrderNo, ProjectSession.EmployeeID.ToString());
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the job Plan Item of work Order.
        /// </summary>
        /// <param name="objOldJobtask">The object old job task.</param>
        /// <param name="objNewJobtask">The object new job task.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageWOSafetyInstr)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ManageWOSafetyInstr(SITask objOldJobtask, SITask objNewJobtask, string kendogriddata)
        {
            List<SITask> lstJobPlanItems = new List<SITask>();
            lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SITask>>(kendogriddata);

            JobOrderService objService = new JobOrderService();

            bool isSeqUpdated = false;
            int maxSeqId = 1;
            int minSeqId = 1;
            int difference = 0;

            if (lstJobPlanItems.Count > 0)
            {
                maxSeqId = lstJobPlanItems.Max(x => x.SeqId);
                minSeqId = lstJobPlanItems.Min(x => x.SeqId);
            }

            if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId != objOldJobtask.SeqId) && objNewJobtask.SeqId > maxSeqId && objOldJobtask.SeqId == maxSeqId)
            {
                ////Updating Max Seq ID  Item With SeqID > Max ID, Then Do not update seqid, just update content
                objNewJobtask.SeqId = objOldJobtask.SeqId;
            }
            else
            {
                ////Adding/Updating Item with SeqID > MaxSeqID
                if (objNewJobtask.SeqId > maxSeqId + 1)
                {
                    objNewJobtask.SeqId = maxSeqId + 1;
                }
            }

            if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId != objOldJobtask.SeqId))
            {
                ////Update Sequence
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqId == objOldJobtask.SeqId && x.Details == objOldJobtask.Details);
                lstJobPlanItems.Remove(objOldJobtask);
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOldJobtask.SeqId > -1 && (objNewJobtask.SeqId == objOldJobtask.SeqId))
            {
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqId == objOldJobtask.SeqId && x.Details == objOldJobtask.Details);
                lstJobPlanItems.Remove(objOldJobtask);
            }
            else if (objOldJobtask.SeqId == -1)
            {
                objNewJobtask.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.CreatedDate = DateTime.Now;
            }

            if (isSeqUpdated)
            {
                if (objOldJobtask.SeqId < objNewJobtask.SeqId)
                {
                    if (objNewJobtask.SeqId > maxSeqId)
                    {
                        objNewJobtask.SeqId = maxSeqId;
                    }

                    lstJobPlanItems.Where(x => x.SeqId > objOldJobtask.SeqId && x.SeqId <= objNewJobtask.SeqId).ToList().ForEach(p =>
                    {
                        p.SeqId = p.SeqId - 1;
                    });
                }
                else if (objOldJobtask.SeqId > objNewJobtask.SeqId)
                {
                    if (objNewJobtask.SeqId < minSeqId)
                    {
                        if (minSeqId > 1)
                        {
                            difference = minSeqId - objNewJobtask.SeqId;
                            minSeqId = 1;

                            objNewJobtask.SeqId = minSeqId;
                            lstJobPlanItems.ForEach(p =>
                            {
                                p.SeqId = p.SeqId - difference;
                            });
                        }
                    }
                    else
                    {
                        lstJobPlanItems.Where(x => x.SeqId >= objNewJobtask.SeqId && x.SeqId < objOldJobtask.SeqId).ToList().ForEach(p =>
                        {
                            p.SeqId = p.SeqId + 1;
                        });
                    }
                }
            }
            else
            {
                if (objOldJobtask.SeqId == -1 && lstJobPlanItems.Any(x => x.SeqId == objNewJobtask.SeqId))
                {
                    ////Inserting New Item in the list between any two Items
                    lstJobPlanItems.Where(x => x.SeqId >= objNewJobtask.SeqId).ToList().ForEach(p =>
                    {
                        p.SeqId = p.SeqId + 1;
                    });
                }
                else
                {
                    if (lstJobPlanItems.Count == 0)
                    {
                        objNewJobtask.SeqId = minSeqId;
                    }
                }
            }

            lstJobPlanItems.Add(objNewJobtask);

            bool result = objService.SaveWorkOrderSafetyInstrItem(objNewJobtask.WoNo, lstJobPlanItems);
            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objNewJobtask.WoNo, ProjectSession.EmployeeID.ToString());

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Attachments Tab"

        /// <summary>
        /// To Get Job Order Documents
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions._GetJobOrderDocuments)]
        public ActionResult _GetJobOrderDocuments(string workOrderNO, int statusID)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = workOrderNO;
            objExt.ModuleType = SystemEnum.DocumentModuleType.J.ToString();
            objExt.IsSaveButtonEnable = false;
            objExt.IsDeleteButtonEnable = false;

            //if (ProjectSession.PermissionAccess.JobOrder_JobOrder_Editrecords == false || statusID == SystemEnum.WorkRequestStatus.WORaised.GetHashCode() || statusID == SystemEnum.WorkRequestStatus.Closed.GetHashCode() || statusID == SystemEnum.WorkRequestStatus.Cancelled.GetHashCode())
            //{
            //    objExt.IsSaveButtonEnable = false;
            //    objExt.IsDeleteButtonEnable = false;
            //}
            //else
            //{
            //    objExt.IsSaveButtonEnable = true;
            //    objExt.IsDeleteButtonEnable = true;
            //}

            return PartialView(PartialViews._DocumentList, objExt);
        }
        #endregion

        #region "PmCheckList Tab"

        /// <summary>
        /// the partial job order PMCheck List.
        /// </summary>
        /// <param name="workorderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetJobOrderPMCheckList)]
        public ActionResult _GetJobOrderPMCheckList(string workorderNo)
        {
            return View(PartialViews.JOPMCheckList);
        }

        /// <summary>
        /// Gets the list of all pm check list items
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="workOrderNo">WorkOrder Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderPmCheckListTasks)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetWorkOrderPmCheckListTasks([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetWorkOrderElementsByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Copy job Plan Pm Check List Items To Work Order elements.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="seqNo">The sequence no.</param>
        /// <param name="remark">The remark.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobOrderPmCheckList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveJobOrderPmCheckList(string workOrderNo, int seqNo, string remark)
        {
            JobOrderService objService = new JobOrderService();

            bool result = objService.UpdateWorkOrderElementRemark(workOrderNo, seqNo, remark);

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "JO Status Track Tab"

        /// <summary>
        /// _s the jo status track tab.
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._JOStatusTrackTab)]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult _JOStatusTrackTab(string workOrderNO)
        {
            JOStatusTrack objJOStatusTrack = new JOStatusTrack();
            objJOStatusTrack.WorkorderNo = workOrderNO;
            return View(PartialViews._JOStatusTrackTab, objJOStatusTrack);
        }

        /// <summary>
        /// Gets the jo status track list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJOStatusTrackList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetJOStatusTrackList([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService objService = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = objService.GetJOStatusTrackByWorkOrderNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion

        #region "Item Requisition Tab"

        /// <summary>
        /// Partial view Job Order labor
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetIRPage)]
        public ActionResult _PartialJoIR(string workOrderNo)
        {
            WorkOrder objModel = new WorkOrder();
            objModel.WorkorderNo = workOrderNo;
            return View(PartialViews.JOIR, objModel);
        }

        /// <summary>
        /// Gets the Item Requisition Items By IR ID.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workOrderNo">The Work Order identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemRequisitionItemsByWoNo)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetItemRequisitionItemsByWoNo([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartId";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            List<Material_req_detail> lstPurchaseOrderItems = new List<Material_req_detail>();
            ////lstPurchaseOrderItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendogriddata);

            StoreTransactionService objService = new StoreTransactionService();
            var result = new DataSourceResult();

            result = new DataSourceResult()
            {
                Data = objService.GetItemRequisitionByWoNo(workOrderNo, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }
        #endregion
        #endregion


        #region "Job Order Escalation"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobOrderEscalationList)]
        public ActionResult JobOrderEscalationList()
        {
            if (ProjectSession.PermissionAccess.JobOrder_JobOrderEscalation_Allowaccess)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.JobOrderEscalationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }


        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderEscalationList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetJobOrderEscalationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            whereClause = " and workorders.workstatusid <> 2 and workorders.workstatusid <> 3 ";

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and workorders.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (workorders.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (workorders.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and workorders.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }

                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                whereClause += " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
            }

            WorkOrderEscalation objWorkOrderEscalation = new WorkOrderEscalation();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objWorkOrderEscalation, SystemEnum.Pages.JobOrderEscalationList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion


        #region "Job Order List"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobOrderList)]
        public ActionResult JobOrderList()
        {
            if (ProjectSession.PermissionAccess.JobOrder_JobOrder_Allowaccess && ProjectSession.PermissionAccess.JobOrder__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.JobOrderList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetJobOrderList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            bool IsClosedFilterExist = checkFilerExist(request.Filters, "Closed");
            bool IsCancelledFilterExist = checkFilerExist(request.Filters, "Cancelled");
            if (!IsClosedFilterExist)
                whereClause = whereClause + " and workorders.workstatusid <> 2 ";

            if (!IsCancelledFilterExist)
                whereClause = whereClause + " and workorders.workstatusid <> 3 ";

            //whereClause = " and workorders.workstatusid <> 2 and workorders.workstatusid <> 3 ";
            whereClause +=" and worequest.CreatedBy= " + ProjectSession.EmployeeID;
            //if (!ProjectSession.IsCentral)
            //{
            //    whereClause += " and workorders.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

            //    /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            //    if (ProjectSession.IsAreaFieldsVisible)
            //    {
            //        whereClause += " and (workorders.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (workorders.L3ID IS NULL))";
            //    }

            //    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
            //    {
            //        whereClause += " and workorders.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
            //    }

            //    /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            //    whereClause += " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
            //}

            WorkOrder objWorkRequest = new WorkOrder();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objWorkRequest, SystemEnum.Pages.JobOrder.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        public bool checkFilerExist(IEnumerable<IFilterDescriptor> filters, string isCheckFilerName)
        {
            bool returnVal = false;
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && Convert.ToString(descriptor.ConvertedValue).Trim() == isCheckFilerName.Trim())
                    {
                        returnVal = true;
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        if (!returnVal)
                            returnVal = checkFilerExist(((CompositeFilterDescriptor)filter).FilterDescriptors, isCheckFilerName);
                    }
                }
            }
            return returnVal;
        }

        /// <summary>
        /// Gets the list of all closed job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetFeedbackJobRequests)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetFeedbackJobRequests([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            if (!request.Filters.Any())
                whereClause = " and workorders.workstatusid IN (2) ";
            whereClause = " and worequest.CreatedBy =" + ProjectSession.EmployeeID;
            //if (!ProjectSession.IsCentral)
            //{
            //    whereClause += " and workorders.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            //    whereClause += " and workorders.MainSubDeptId in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";

            //    /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            //    if (ProjectSession.IsAreaFieldsVisible)
            //    {
            //        whereClause += " and (workorders.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (worequest.L3ID IS NULL))";
            //    }

            //    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
            //    {
            //        whereClause += " and workorders.LocationID in  (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
            //    }

            //    /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            //}

            WorkOrder objWorkOrder = new WorkOrder();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objWorkOrder, SystemEnum.Pages.JobOrder.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }


        /// <summary>
        /// Jobs the order pop up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.JobOrderPopUp)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult JobOrderPopUp([DataSourceRequest]DataSourceRequest request, int? cityID = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            if (cityID > 0)
            {
                whereClause = " and workorders.L2ID =  " + cityID + " and workorders.workstatusid <> 2 and workorders.workstatusid <> 3 ";
            }

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
                whereClause += " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
            }

            JobOrderService objService = new JobOrderService();
            var result = new DataSourceResult()
            {
                Data = objService.GetJOPageWithOutLoad(whereClause, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }
        #endregion

        #region "Reports"

        /// <summary>
        /// Prints the job plan order.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.PrintJobPlanOrders)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string PrintJobPlanOrders(string workOrderNo)
        {
            return workOrderNo;
        }

        /// <summary>
        /// Prints the job plan order.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.Print)]
        public ActionResult Print(string workOrderNo)
        {
            JobOrderService objService = new JobOrderService();
            List<Jobtask> objJobTask = new List<Jobtask>();
            List<WorkOrder> objWorkOrder = new List<WorkOrder>();
            objWorkOrder = objService.PrintWorkOrderDetails(workOrderNo);
            objJobTask = objService.GetJobTaskForDataSource(workOrderNo);
            JobPlanReport report = new JobPlanReport();
            report.DataSource = objJobTask;
            report.txtJobPriority.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.txtJobStatus.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.txtJobTrade.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.txtJobType.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.txtJOPlanNo.Text = workOrderNo;
            report.xrTableCell4.DataBindings.Add("Text", objJobTask, "SeqId");
            report.xrTableCell5.DataBindings.Add("Text", objJobTask, "Details");
            report.xrTableCell6.DataBindings.Add("Text", objJobTask, "Remarks");

            return View(Views.PrintView, report);
        }

        /// <summary>
        /// Prints the safety instruction.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.PrintSafetyInstruction)]
        public ActionResult PrintSafetyInstruction(string workOrderNo)
        {
            PrintSafetyInstructionReport report = new PrintSafetyInstructionReport();
            JobOrderService objService = new JobOrderService();
            List<SITask> objSafetyInstruction = new List<SITask>();
            List<WorkOrder> objWorkOrder = new List<WorkOrder>();
            objSafetyInstruction = objService.PrintSafetyInsturctionDetails(workOrderNo);
            objWorkOrder = objService.PrintWorkOrderDetails(workOrderNo);
            report.DataSource = objSafetyInstruction;
            report.txtJobPriority.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.txtJobStatus.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.txtJobTrade.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.txtJobType.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.txtJOPlanNo.Text = workOrderNo;
            report.xrTableCell4.DataBindings.Add("Text", objSafetyInstruction, "SeqId");
            report.xrTableCell5.DataBindings.Add("Text", objSafetyInstruction, "Details");
            return View(Views.PrintView, report);
        }

        /// <summary>
        /// Jobs the order print j o1.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.JobOrderPrintJO1)]
        public ActionResult JobOrderPrintJO1(string workOrderNo, int IsPMAttachment = 0)
        {
            JobOrderPrintJO1 report = new JobOrderPrintJO1();
            JobOrderService objService = new JobOrderService();
            List<Translation> lstLabels = new List<Translation>();
            lstLabels = LanguageTranslationService.GetTranslationForReport();

            List<SITask> objSafetyInstruction = new List<SITask>();
            List<WorkOrder> objWorkOrder = new List<WorkOrder>();
            List<Workorderelement> objWorkOrderElement = new List<Workorderelement>();
            List<ChecklistInv> objChecklistInvItems = new List<ChecklistInv>();
            List<ChecklistTool> objChecklistTools = new List<ChecklistTool>();
            List<ChecklistPPE> objChecklistPPE = new List<ChecklistPPE>();

            objSafetyInstruction = objService.PrintSafetyInsturctionDetails(workOrderNo);
            objWorkOrderElement = objService.PrintWorkOrderElement(workOrderNo);

            objChecklistInvItems = objService.PrintChecklistInvItems(workOrderNo);
            objChecklistTools = objService.PrintChecklistTools(workOrderNo);
            objChecklistPPE = objService.PrintChecklistPPE(workOrderNo);            

            objWorkOrder = objService.PrintJobOrderDetails(workOrderNo);

            report.CityCode.Text = lstLabels.Where(o => o.Labelname == "MenuCity").FirstOrDefault().FromWord.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en);
            report.altCityCode.Text = lstLabels.Where(o => o.Labelname == "MenuCity").FirstOrDefault().ToWord.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar);
            report.AreaCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockAreaCode").FirstOrDefault().FromWord.Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en);
            report.altAreaCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockAreaCode").FirstOrDefault().ToWord.Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar);
            report.ZoneCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockStreetCodes").FirstOrDefault().FromWord.Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en);
            report.altZoneCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockStreetCodes").FirstOrDefault().ToWord.Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar);
            report.BuildingCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockBuildingCode").FirstOrDefault().FromWord.Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
            report.altBuildingCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockBuildingCode").FirstOrDefault().ToWord.Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);

            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
            {
                report.rptTitle.Text = lstLabels.Where(o => o.Labelname == "TextBlockJobOrderPrintReport").FirstOrDefault().ToWord;
            }
            else
            {
                report.rptTitle.Text = lstLabels.Where(o => o.Labelname == "TextBlockJobOrderPrintReport").FirstOrDefault().FromWord;
            }

            if (objSafetyInstruction.Count > 0)
            {
                report.SafetyInstructions.DataSource = objSafetyInstruction;
            }
            else
            {
                report.SafetyInstructions.Visible = false;
            }

            if (objWorkOrderElement.Count > 0)
            {
                report.CheckListReport.DataSource = objWorkOrderElement;
            }
            else
            {
                report.CheckListReport.Visible = false;
            }


            if (objChecklistInvItems.Count > 0)
            {
                report.PMChecklistReportItems.DataSource = objChecklistInvItems;
            }
            else
            {
                report.PMChecklistReportItems.Visible = false;
            }


            if (objChecklistTools.Count > 0)
            {
                report.PMChecklistReportTools.DataSource = objChecklistTools;
            }
            else
            {
                report.PMChecklistReportTools.Visible = false;
            }


            if (objChecklistPPE.Count > 0)
            {
                report.PMChecklistReportPPE.DataSource = objChecklistPPE;
            }
            else
            {
                report.PMChecklistReportPPE.Visible = false;
            }

            report.JOPlanNo.Text = workOrderNo;
            report.JobPriority.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.JobStatus.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.JobTrade.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.JobType.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.creatdBy.Text = objWorkOrder.Select(a => a.Name).FirstOrDefault();
            report.rcvDatetime.Text = Common.FormatDateTime(objWorkOrder.Select(a => a.DateReceived).FirstOrDefault(), ProjectConfiguration.DATEFORMATDDMMMYYYHHMMTT);
            report.reqDatetime.Text = Convert.ToString(objWorkOrder.Select(a => a.DateRequired).FirstOrDefault());
            report.phoneNumber.Text = objWorkOrder.Select(a => a.TelNo).FirstOrDefault();
            report.problemDescription.Text = objWorkOrder.Select(a => a.ProblemDescription).FirstOrDefault();
            report.notesAssets.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.xrLabel4.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.failCode.Text = objWorkOrder.Select(a => a.FailureCauseCode).FirstOrDefault();
            report.ctyCode.Text = objWorkOrder.Select(a => a.L2Code).FirstOrDefault();
            report.areaCod.Text = objWorkOrder.Select(a => a.L3No).FirstOrDefault();
            report.znCode.Text = objWorkOrder.Select(a => a.L4No).FirstOrDefault();
            report.buildCode.Text = objWorkOrder.Select(a => a.L5No).FirstOrDefault();
            report.txtPmNo.Text = objWorkOrder.Select(a => a.PMNo).FirstOrDefault();
            report.txtLocationNumber.Text = objWorkOrder.Select(a => a.LocationNo).FirstOrDefault();
            report.txtlocationDescription.Text = objWorkOrder.Select(a => a.LocationDescription).FirstOrDefault();
            report.txtAsset.Text = objWorkOrder.Select(a => a.AssetNumber).FirstOrDefault();
            report.txtAssetDescription.Text = objWorkOrder.Select(a => a.AssetDescription).FirstOrDefault();
            report.txtMaintDivCode.Text = objWorkOrder.Select(a => a.MaintDivisionCode).FirstOrDefault();
            report.txtMaintDeptCode.Text = objWorkOrder.Select(a => a.MaintDeptCode).FirstOrDefault();
            report.txtMaintSubDeptCode.Text = objWorkOrder.Select(a => a.MaintSubDeptCode).FirstOrDefault();
            report.txtCauseDescription.Text = objWorkOrder.Select(a => a.CauseDescription).FirstOrDefault();
            report.txtActionTaken.Text = objWorkOrder.Select(a => a.ActionTaken).FirstOrDefault();
            report.txtPrevTaken.Text = objWorkOrder.Select(a => a.PreventionTaken).FirstOrDefault();
            report.xrLabel96.Text = workOrderNo;
            report.xrLabel98.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.xrLabel100.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.xrLabel102.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.xrLabel104.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.xrLabel86.Text = objWorkOrder.Select(a => a.ChecklistNo).FirstOrDefault();
            report.xrLabel88.Text = objWorkOrder.Select(a => a.CheckListName).FirstOrDefault();
            report.seqId.DataBindings.Add("Text", objSafetyInstruction, "SeqId");
            report.xrTableCell2.DataBindings.Add("Text", objSafetyInstruction, "Details");
            report.xrTableCell3.DataBindings.Add("Text", objSafetyInstruction, "AltDetails");
            report.seqNo.DataBindings.Add("Text", objWorkOrderElement, "SeqNo");
            report.taskDesc.DataBindings.Add("Text", objWorkOrderElement, "TaskDesc");
            report.altTaskDesc.DataBindings.Add("Text", objWorkOrderElement, "AltTaskDesc");

            report.PMItemsSeqNo.DataBindings.Add("Text", objChecklistInvItems, "SeqNo");
            report.PMItemsNo.DataBindings.Add("Text", objChecklistInvItems, "PartsListNo");
            report.PMItemsDescription.DataBindings.Add("Text", objChecklistInvItems, "PartsListDescription");
            report.PMItemsAltDescription.DataBindings.Add("Text", objChecklistInvItems, "PartsListAltDescription");
            report.PMItemsQty.DataBindings.Add("Text", objChecklistInvItems, "Qty");

            report.PMChecklistReportToolsSeqNo.DataBindings.Add("Text", objChecklistTools, "Quantity");
            report.PMChecklistReportToolsDescription.DataBindings.Add("Text", objChecklistTools, "ToolDescription");
            report.PMChecklistReportToolsAltDescription.DataBindings.Add("Text", objChecklistTools, "AltToolDescription");

            report.PMChecklistReportPPESeqNo.DataBindings.Add("Text", objChecklistPPE, "SeqNo");
            report.PMChecklistReportPPEDescription.DataBindings.Add("Text", objChecklistPPE, "PPEDescription");
            report.PMChecklistReportPPEAltDescription.DataBindings.Add("Text", objChecklistPPE, "AltPPEDescription");
            
            return View(Views.PrintView, report);
        }

        /// <summary>
        /// Jobs the order print j o2.
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.JobOrderPrintJO2)]
        public ActionResult JobOrderPrintJO2(string workOrderNo)
        {
            JobOrderPrintJO2 report = new JobOrderPrintJO2();
            JobOrderService objService = new JobOrderService();
            List<Translation> lstLabels = new List<Translation>();
            lstLabels = LanguageTranslationService.GetTranslationForReport();
            List<ItemAndLabourDetails> objItemDetails = new List<ItemAndLabourDetails>();
            List<SITask> objSafetyInstruction = new List<SITask>();
            List<WorkOrder> objWorkOrder = new List<WorkOrder>();
            List<Workorderelement> objWorkOrderElement = new List<Workorderelement>();
            List<Workorderlabor> objLabourDetails = new List<Workorderlabor>();
            List<Assigntoworkorder> objAssignEmployee = new List<Assigntoworkorder>();
            objSafetyInstruction = objService.PrintSafetyInsturctionDetails(workOrderNo);
            objWorkOrderElement = objService.PrintWorkOrderElement(workOrderNo);
            objWorkOrder = objService.PrintJobOrderDetails(workOrderNo);
            objLabourDetails = objService.PrintLabourDetails(workOrderNo);
            objAssignEmployee = objService.PrintAssignEmployeeDetails(workOrderNo);
            objItemDetails = objService.PrintItemAndLabourDetails(workOrderNo);

            report.CityCode.Text = lstLabels.Where(o => o.Labelname == "MenuCity").FirstOrDefault().FromWord.Replace("[L2_en]", ProjectSession.LevelNamesTbl.L2_en);
            report.altCityCode.Text = lstLabels.Where(o => o.Labelname == "MenuCity").FirstOrDefault().ToWord.Replace("[L2_ar]", ProjectSession.LevelNamesTbl.L2_ar);
            report.AreaCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockAreaCode").FirstOrDefault().FromWord.Replace("[L3_en]", ProjectSession.LevelNamesTbl.L3_en);
            report.altAreaCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockAreaCode").FirstOrDefault().ToWord.Replace("[L3_ar]", ProjectSession.LevelNamesTbl.L3_ar);
            report.ZoneCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockStreetCodes").FirstOrDefault().FromWord.Replace("[L4_en]", ProjectSession.LevelNamesTbl.L4_en);
            report.altZoneCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockStreetCodes").FirstOrDefault().ToWord.Replace("[L4_ar]", ProjectSession.LevelNamesTbl.L4_ar);
            report.BuildingCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockBuildingCode").FirstOrDefault().FromWord.Replace("[L5_en]", ProjectSession.LevelNamesTbl.L5_en);
            report.altBuildingCode.Text = lstLabels.Where(o => o.Labelname == "TextBlockBuildingCode").FirstOrDefault().ToWord.Replace("[L5_ar]", ProjectSession.LevelNamesTbl.L5_ar);
            
            if (objSafetyInstruction.Count > 0)
            {
                report.SafetyDetailReport.DataSource = objSafetyInstruction;
            }
            else
            {
                report.SafetyDetailReport.Visible = false;
            }

            if (objWorkOrderElement.Count > 0)
            {
                report.CheckListDetailReport.DataSource = objWorkOrderElement;
            }
            else
            {
                report.CheckListDetailReport.Visible = false;
            }

            report.JOPlanNo.Text = workOrderNo;
            report.JobPriority.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.JobStatus.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.JobTrade.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.JobType.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.creatdBy.Text = objWorkOrder.Select(a => a.Name).FirstOrDefault();
            report.rcvDatetime.Text = Convert.ToString(objWorkOrder.Select(a => a.DateReceived).FirstOrDefault());
            report.reqDatetime.Text = Convert.ToString(objWorkOrder.Select(a => a.DateRequired).FirstOrDefault());
            report.phoneNumber.Text = objWorkOrder.Select(a => a.TelNo).FirstOrDefault();
            report.problemDescription.Text = objWorkOrder.Select(a => a.ProblemDescription).FirstOrDefault();
            report.notesAssets.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.xrLabel4.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.failCode.Text = objWorkOrder.Select(a => a.FailureCauseCode).FirstOrDefault();
            report.ctyCode.Text = objWorkOrder.Select(a => a.L2Code).FirstOrDefault();
            report.areaCod.Text = objWorkOrder.Select(a => a.L3No).FirstOrDefault();
            report.znCode.Text = objWorkOrder.Select(a => a.L4No).FirstOrDefault();
            report.buildCode.Text = objWorkOrder.Select(a => a.L5No).FirstOrDefault();
            report.txtPmNo.Text = objWorkOrder.Select(a => a.PMNo).FirstOrDefault();
            report.txtLocationNumber.Text = objWorkOrder.Select(a => a.LocationNo).FirstOrDefault();
            report.txtlocationDescription.Text = objWorkOrder.Select(a => a.LocationDescription).FirstOrDefault();
            report.txtAsset.Text = objWorkOrder.Select(a => a.AssetNumber).FirstOrDefault();
            report.txtAssetDescription.Text = objWorkOrder.Select(a => a.AssetDescription).FirstOrDefault();
            report.xrLabel135.Text = objWorkOrder.Select(a => a.MaintDivisionCode).FirstOrDefault();
            report.txtMaintDeptCode.Text = objWorkOrder.Select(a => a.MaintDeptCode).FirstOrDefault();
            report.txtMaintSubDeptCode.Text = objWorkOrder.Select(a => a.MaintSubDeptCode).FirstOrDefault();
            report.txtCauseDescription.Text = objWorkOrder.Select(a => a.CauseDescription).FirstOrDefault();
            report.txtActionTaken.Text = objWorkOrder.Select(a => a.ActionTaken).FirstOrDefault();
            report.txtPrevTaken.Text = objWorkOrder.Select(a => a.PreventionTaken).FirstOrDefault();
            report.xrLabel96.Text = workOrderNo;
            report.xrLabel98.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.xrLabel100.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.xrLabel102.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.xrLabel104.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.xrLabel86.Text = objWorkOrder.Select(a => a.ChecklistNo).FirstOrDefault();
            report.xrLabel88.Text = objWorkOrder.Select(a => a.CheckListName).FirstOrDefault();
            report.seqId.DataBindings.Add("Text", objSafetyInstruction, "SeqId");
            report.details.DataBindings.Add("Text", objSafetyInstruction, "Details");
            report.altDetails.DataBindings.Add("Text", objSafetyInstruction, "AltDetails");
            report.seqNo.DataBindings.Add("Text", objWorkOrderElement, "SeqNo");
            report.taskDesc.DataBindings.Add("Text", objWorkOrderElement, "TaskDesc");
            report.altTaskDesc.DataBindings.Add("Text", objWorkOrderElement, "AltTaskDesc");
            report.TableCell7.DataBindings.Add("Text", objItemDetails, "PartsNo");
            report.TableCell8.DataBindings.Add("Text", objItemDetails, "StockDesc");
            report.TableCell9.DataBindings.Add("Text", objItemDetails, "AltStockDesc");
            report.TableCell10.DataBindings.Add("Text", objItemDetails, "IssueType");
            report.TableCell11.DataBindings.Add("Text", objItemDetails, "UOM");
            report.xrTableCell24.DataBindings.Add("Text", objItemDetails, "Qty");
            report.xrTableCell9.DataBindings.Add("Text", objAssignEmployee, "Name");
            report.xrTableCell11.DataBindings.Add("Text", objAssignEmployee, "EmployeeNO");
            report.xrTableCell14.DataBindings.Add("Text", objAssignEmployee, "EstStartDate");
            report.xrTableCell16.DataBindings.Add("Text", objAssignEmployee, "EstEndDate");
            report.TableCell43.DataBindings.Add("Text", objLabourDetails, "Name");
            report.TableCell44.DataBindings.Add("Text", objLabourDetails, "EmployeeNO");
            report.TableCell45.DataBindings.Add("Text", objLabourDetails, "HourlySalary");
            report.TableCell46.DataBindings.Add("Text", objLabourDetails, "OverTime1");
            report.TableCell47.DataBindings.Add("Text", objLabourDetails, "OverTime2");
            report.TableCell48.DataBindings.Add("Text", objLabourDetails, "OverTime3");

            if (objWorkOrder.Select(a => a.ImageRaw).FirstOrDefault() != null)
            {
                report.imgUserSignature.Visible = true;
                report.xrlblSignatureOfRequester.Visible = true;
                report.imgUserSignature.Image = byteArrayToImage(objWorkOrder.Select(a => a.ImageRaw).FirstOrDefault());
            }
            else
            {
                report.imgUserSignature.Visible = false;
                report.xrlblSignatureOfRequester.Visible = false;
            }

            return View(Views.PrintView, report);
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        /// <summary>
        /// Print Job Order List Report
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobOrderListPrint)]
        public ActionResult JobOrderListPrint(string workOrderNo)
        {
            JobOrderListReport report = new JobOrderListReport();
            JobOrderService objService = new JobOrderService();
            report.DataSource = objService.GetWorkOrderListForReport(workOrderNo);
            foreach (XRControlStyle style in report.StyleSheet)
            {
                style.Font = new System.Drawing.Font("Arial", style.Font.Size);
            }

            /*objSafetyInstruction = objService.PrintSafetyInsturctionDetails(workOrderNo);
            ////objWorkOrderElement = objService.PrintWorkOrderElement(workOrderNo);
            ////objWorkOrder = objService.PrintJobOrderDetails(workOrderNo);
            ////objLabourDetails = objService.PrintLabourDetails(workOrderNo);
            ////objAssignEmployee = objService.PrintAssignEmployeeDetails(workOrderNo);
            ////objItemDetails = objService.PrintItemAndLabourDetails(workOrderNo);
            report.SafetyDetailReport.DataSource = objSafetyInstruction;
            //report.DataSource = objService.GetWorkOrderListForReport();
            //report.CheckListDetailReport.DataSource = objWorkOrderElement;
            /*report.JOPlanNo.Text = workOrderNo;
            report.JobPriority.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.JobStatus.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.JobTrade.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.JobType.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.creatdBy.Text = objWorkOrder.Select(a => a.Name).FirstOrDefault();
            report.rcvDatetime.Text = Convert.ToString(objWorkOrder.Select(a => a.DateReceived).FirstOrDefault());
            report.reqDatetime.Text = Convert.ToString(objWorkOrder.Select(a => a.DateRequired).FirstOrDefault());
            report.phoneNumber.Text = objWorkOrder.Select(a => a.TelNo).FirstOrDefault();
            report.problemDescription.Text = objWorkOrder.Select(a => a.ProblemDescription).FirstOrDefault();
            report.notesAssets.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.xrLabel4.Text = objWorkOrder.Select(a => a.NoteToTechLocation).FirstOrDefault();
            report.failCode.Text = objWorkOrder.Select(a => a.FailureCauseCode).FirstOrDefault();
            report.ctyCode.Text = objWorkOrder.Select(a => a.L2Code).FirstOrDefault();
            report.areaCod.Text = objWorkOrder.Select(a => a.L3No).FirstOrDefault();
            report.znCode.Text = objWorkOrder.Select(a => a.L4No).FirstOrDefault();
            report.buildCode.Text = objWorkOrder.Select(a => a.L5No).FirstOrDefault();
            report.txtPmNo.Text = objWorkOrder.Select(a => a.PMNo).FirstOrDefault();
            report.txtLocationNumber.Text = objWorkOrder.Select(a => a.LocationNo).FirstOrDefault();
            report.txtlocationDescription.Text = objWorkOrder.Select(a => a.LocationDescription).FirstOrDefault();
            report.txtAsset.Text = objWorkOrder.Select(a => a.AssetNumber).FirstOrDefault();
            report.txtAssetDescription.Text = objWorkOrder.Select(a => a.AssetDescription).FirstOrDefault();
            report.xrLabel135.Text = objWorkOrder.Select(a => a.MaintDivisionCode).FirstOrDefault();
            report.txtMaintDeptCode.Text = objWorkOrder.Select(a => a.MaintDeptCode).FirstOrDefault();
            report.txtMaintSubDeptCode.Text = objWorkOrder.Select(a => a.MaintSubDeptCode).FirstOrDefault();
            report.txtCauseDescription.Text = objWorkOrder.Select(a => a.CauseDescription).FirstOrDefault();
            report.txtActionTaken.Text = objWorkOrder.Select(a => a.ActionTaken).FirstOrDefault();
            report.txtPrevTaken.Text = objWorkOrder.Select(a => a.PreventionTaken).FirstOrDefault();
            /*report.xrLabel96.Text = workOrderNo;
            report.xrLabel98.Text = objWorkOrder.Select(a => a.WorkTrade).FirstOrDefault();
            report.xrLabel100.Text = objWorkOrder.Select(a => a.WorkStatus).FirstOrDefault();
            report.xrLabel102.Text = objWorkOrder.Select(a => a.WorkTypeDescription).FirstOrDefault();
            report.xrLabel104.Text = objWorkOrder.Select(a => a.WorkPriority).FirstOrDefault();
            report.xrLabel86.Text = objWorkOrder.Select(a => a.ChecklistNo).FirstOrDefault();
            report.xrLabel88.Text = objWorkOrder.Select(a => a.CheckListName).FirstOrDefault();
            report.seqId.DataBindings.Add("Text", objSafetyInstruction, "SeqId");
            report.details.DataBindings.Add("Text", objSafetyInstruction, "Details");
            report.altDetails.DataBindings.Add("Text", objSafetyInstruction, "Remarks");
            report.seqNo.DataBindings.Add("Text", objWorkOrderElement, "SeqNo");
            report.taskDesc.DataBindings.Add("Text", objWorkOrderElement, "TaskDesc");
            report.altTaskDesc.DataBindings.Add("Text", objWorkOrderElement, "AltTaskDesk");
            report.TableCell7.DataBindings.Add("Text", objItemDetails, "PartsNo");
            report.TableCell8.DataBindings.Add("Text", objItemDetails, "StockDesc");
            report.TableCell9.DataBindings.Add("Text", objItemDetails, "AltStockDesc");
            report.TableCell10.DataBindings.Add("Text", objItemDetails, "IssueType");
            report.TableCell11.DataBindings.Add("Text", objItemDetails, "UOM");
            report.xrTableCell24.DataBindings.Add("Text", objItemDetails, "Qty");
            report.xrTableCell9.DataBindings.Add("Text", objAssignEmployee, "Name");
            report.xrTableCell11.DataBindings.Add("Text", objAssignEmployee, "EmployeeNO");
            report.xrTableCell14.DataBindings.Add("Text", objAssignEmployee, "EstStartDate");
            report.xrTableCell16.DataBindings.Add("Text", objAssignEmployee, "EstEndDate");
            report.TableCell43.DataBindings.Add("Text", objLabourDetails, "Name");
            report.TableCell44.DataBindings.Add("Text", objLabourDetails, "EmployeeNO");
            report.TableCell45.DataBindings.Add("Text", objLabourDetails, "HourlySalary");
            report.TableCell46.DataBindings.Add("Text", objLabourDetails, "OverTime1");
            report.TableCell47.DataBindings.Add("Text", objLabourDetails, "OverTime2");
            report.TableCell48.DataBindings.Add("Text", objLabourDetails, "OverTime3"); */
            return View(Views.PrintView, report);
        }



        [ActionName("GetAttachment")]
        public ActionResult GetAttachment(string workOrderNO, int IsPMAttachment = 0)
        {
            ExtAssetFile objExt = new ExtAssetFile();
            List<ExtAssetFile> lstExt = new List<ExtAssetFile>();
            
            if (IsPMAttachment == 2)
            {
                objExt.FkId = workOrderNO;
                objExt.ModuleType = SystemEnum.DocumentModuleType.J.ToString();
                using (ServiceContext context = new ServiceContext())
                {
                    lstExt = context.Search<ExtAssetFile>(objExt).ToList();
                }
            }
            else if (IsPMAttachment == 1)
            {
                objExt.ModuleType = SystemEnum.DocumentModuleType.P.ToString();
                WorkOrder objwo = new WorkOrder();
                objwo.WorkorderNo = workOrderNO;
                using (ServiceContext context = new ServiceContext())
                {
                    List<WorkOrder> lst = context.Search<WorkOrder>(objwo).ToList();
                    if (lst.Count > 0)
                    {
                        objExt.FkId = Convert.ToString(lst[0].PMChecklistID);
                        lstExt = context.Search<ExtAssetFile>(objExt).ToList();
                    }
                }
                
            }

            return Json(lstExt);
        }

        #endregion
    }

    /// <summary>
    /// Result Of The Date comparison
    /// </summary>
    public class DateCompareResult
    {
        /// <summary>
        /// To Compare Result
        /// </summary>
        public enum CompareResult
        {
            Equal = 0,
            Greater = 1,
            Lesser = -1,
            DifferenceInMinutes = 0
        }

        /// <summary>
        ///  Gets or sets Values equal/greater or lesser
        /// </summary>
        public int Result { get; set; }

        /// <summary>
        ///  Gets or sets Message If you want to give based on comparison result
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        ///  Gets or sets Difference between date1 and date2 in minutes, negative if date1 is lesser
        /// </summary>
        public double DifferenceInMinutes { get; set; }

        /// <summary>
        ///  Gets or sets Error Message if any error occurred while comparing dates, like exception
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        ///  Gets or sets Type passed when calling Compare Function
        /// </summary>
        public int Type { get; set; }
    }
}