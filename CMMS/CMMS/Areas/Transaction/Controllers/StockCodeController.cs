﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Stock code Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Gets the purchase proposal high priority.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="storeID">The store identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseProposalHighPriority)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetPurchaseProposalHighPriority([DataSourceRequest]DataSourceRequest request, int storeID)
        {
            IList<L5> lstBuildings = new List<L5>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                StockCodeService objService = new StockCodeService();
                var result = new DataSourceResult()
                {
                    Data = objService.GetPurchaseProposalHighpriority(pageNumber, sortExpression, sortDirection, storeID, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the purchase proposal normal priority.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="storeID">The store identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseProposalNormalPriority)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetPurchaseProposalNormalPriority([DataSourceRequest]DataSourceRequest request, int storeID)
        {
            IList<L5> lstBuildings = new List<L5>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }                

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                StockCodeService objService = new StockCodeService();
                var result = new DataSourceResult()
                {
                    Data = objService.GetPurchaseProposalNormalpriority(pageNumber, sortExpression, sortDirection, storeID, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}