﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Collections.ObjectModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.DAL;
using CMMS.Reports;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Purchase Order Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Purchases the order list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchaseOrderList)]
        public ActionResult PurchaseOrderList()
        {
            if (ProjectSession.PermissionAccess.Purchasing_PurchaseOrder_Allowaccess && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.PurchaseOrderList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchase order.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseOrder)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseOrder([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, PurchaseOrderNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;


            bool IsClosedFilterExist = checkFilerExist(request.Filters, "Closed");
            bool IsCancelledFilterExist = checkFilerExist(request.Filters, "Cancelled");            

            if (!IsClosedFilterExist)
                whereClause = whereClause + " and purchaseorder.OrderStatus != 'Closed' ";

            if (!IsCancelledFilterExist)
                whereClause = whereClause + " and purchaseorder.OrderStatus != 'Cancelled' ";            

            //whereClause = " and purchaseorder.OrderStatus != 'Closed' and purchaseorder.OrderStatus != 'Cancelled' ";

            if (!ProjectSession.IsCentral)
            {
                whereClause = " and purchaseorder.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            PurchaseOrder objPurchaseOrder = new PurchaseOrder();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPurchaseOrder, SystemEnum.Pages.PurchaseOrder.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the purchase order page.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseOrderPage)]
        public ActionResult GetPurchaseOrderPage(int id = 0, string workOrderNo = "")
        {
            if (ProjectSession.PermissionAccess.Purchasing_PurchaseOrder_Allowaccess && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                PurchaseOrder objPurchaseOrder = new PurchaseOrder();
                objPurchaseOrder.ID = id;
                if (id > 0)
                {
                    objPurchaseOrder = PurchaseService.GetPurchaseOrderDetail(id);
                    objPurchaseOrder.IsAnyPOItemReceivedOrCancelled = PurchaseService.IsAnyPOItemReceivedOrCancelled(id);
                }
                else
                {
                    if (!string.IsNullOrEmpty(workOrderNo))
                    {
                        objPurchaseOrder.workOrderNo = workOrderNo;
                        using (ServiceContext context = new ServiceContext())
                        {
                            WorkOrder objWorkOrder = new WorkOrder();
                            objWorkOrder.WorkorderNo = workOrderNo;
                            objWorkOrder = context.Search<WorkOrder>(objWorkOrder).FirstOrDefault();

                            if (objWorkOrder != null)
                            {
                                objPurchaseOrder.L2ID = Convert.ToInt32(objWorkOrder.L2ID);
                                objPurchaseOrder.MaintDivisionID = objWorkOrder.MaintDivisionID;
                                objPurchaseOrder.MaintDeptID = objWorkOrder.MaintdeptID;
                                objPurchaseOrder.MaintsubdeptID = objWorkOrder.MaintsubdeptID;
                                objPurchaseOrder.OrderBy = ProjectSession.EmployeeID;
                                objPurchaseOrder.OrderByEmployeeNO = ProjectSession.EmployeeNo;
                                objPurchaseOrder.OrderByEmployeeName = ProjectSession.EmployeeName;
                                objPurchaseOrder.OrderByAltEmployeeName = ProjectSession.EmployeeName;
                            }
                        }
                    }
                    else
                    {
                        //if (!ProjectConfiguration.IsItemNoAutoIncrement)
                        //{
                        //    using (ServiceContext context = new ServiceContext())
                        //    {
                        //        employees obj = new employees();
                        //        obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                        //        objPurchaseOrder.L2ID = Convert.ToInt32(obj.L2ID);
                        //        objPurchaseOrder.MaintDivisionID = obj.MaintDivisionID;
                        //        objPurchaseOrder.MaintDeptID = obj.MaintDeptID;
                        //        objPurchaseOrder.MaintsubdeptID = obj.MaintSubDeptID;
                        //    }
                        //}
                        //else
                        //{
                        //    objPurchaseOrder.OrderBy = ProjectSession.EmployeeID;
                        //    objPurchaseOrder.OrderByEmployeeNO = ProjectSession.EmployeeNo;
                        //    objPurchaseOrder.OrderByEmployeeName = ProjectSession.EmployeeName;

                        //    objPurchaseOrder.InvoiceTo = ProjectSession.EmployeeID;
                        //    objPurchaseOrder.InvoiceToEmployeeNO = ProjectSession.EmployeeNo;
                        //    objPurchaseOrder.InvoiceToEmployeeName = ProjectSession.EmployeeName;
                        //}


                        using (ServiceContext context = new ServiceContext())
                        {
                            employees obj = new employees();
                            obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                            objPurchaseOrder.L2ID = Convert.ToInt32(obj.L2ID);
                            objPurchaseOrder.MaintDivisionID = obj.MaintDivisionID;
                            objPurchaseOrder.MaintDeptID = obj.MaintDeptID;
                            objPurchaseOrder.MaintsubdeptID = obj.MaintSubDeptID;
                        }

                        objPurchaseOrder.OrderBy = ProjectSession.EmployeeID;
                        objPurchaseOrder.OrderByEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseOrder.OrderByEmployeeName = ProjectSession.EmployeeName;

                        objPurchaseOrder.InvoiceTo = ProjectSession.EmployeeID;
                        objPurchaseOrder.InvoiceToEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseOrder.InvoiceToEmployeeName = ProjectSession.EmployeeName;
                    }

                    using (ServiceContext context = new ServiceContext())
                    {
                        DefaultSetup obj = new DefaultSetup();
                        List<DefaultSetup> lstDefaultSetup = context.SearchAll<DefaultSetup>(obj).ToList();

                        if (lstDefaultSetup.Count > 0)
                        {
                            DefaultSetup objGST = new DefaultSetup();
                            objGST = lstDefaultSetup.Where(x => x.SettingName == "Default GST %").FirstOrDefault();
                            if (objGST != null)
                            {
                                objPurchaseOrder.GSTPercentage = objGST.Value;
                            }

                            DefaultSetup objTax = new DefaultSetup();
                            objTax = lstDefaultSetup.Where(x => x.SettingName == "Default Tax %").FirstOrDefault();
                            if (objTax != null)
                            {
                                objPurchaseOrder.TaxPercentage = objTax.Value;
                            }
                        }
                    }
                }

                return View(Views.PurchaseOrderDetails, objPurchaseOrder);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchase order items by purchase id with out load.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="purchaseOrderID">The purchase order identifier.</param>
        /// <param name="dbcall">if set to <c>true</c> [database call].</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseOrderItemsByPRIDWithOutLoad)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseOrderItemsByPRIDWithOutLoad([DataSourceRequest]DataSourceRequest request, int purchaseOrderID, bool dbcall = false)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartNumber";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string whereClause = string.Empty;
            whereClause = " and PurchaseOrderID = " + purchaseOrderID;

            List<PurchaseOrderItem> lstPurchaseOrderItems = new List<PurchaseOrderItem>();
            ////lstPurchaseOrderItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendogriddata);

            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult();
            if (dbcall)
            {
                result = new DataSourceResult()
               {
                   Data = objService.GetPurchaseOrderItemsByPRIDWithOutLoad(whereClause, pageNumber, sortExpression, sortDirection, request: request),
                   Total = objService.PagingInformation.TotalRecords
               };
            }
            else if (TempData["GetPurchaseOrderItemdata"] != null)
            {
                lstPurchaseOrderItems = (List<PurchaseOrderItem>)TempData["GetPurchaseOrderItemdata"];
                result = new DataSourceResult()
                {
                    Data = lstPurchaseOrderItems,
                    Total = 1
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Gets the purchase request for purchase order.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRForPO)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPRForPO([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PRID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult()
            {
                Data = objService.GetPRForPO(pageNumber, sortExpression, sortDirection, cityID, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the Item Requisition for purchase order
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRForPO)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetIRForPO([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "mr_id";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult()
            {
                Data = objService.GetIRForPO(pageNumber, sortExpression, sortDirection, cityID, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the purchase order.
        /// </summary>
        /// <param name="objPurchaseOrder">The object purchase order.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchaseOrder)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePurchaseOrder(PurchaseOrder objPurchaseOrder)
        {
            if (objPurchaseOrder.DiscountPercentage == null)
            {
                objPurchaseOrder.DiscountPercentage = 0;
            }

            if (objPurchaseOrder.GSTPercentage == null)
            {
                objPurchaseOrder.GSTPercentage = 0;
            }

            if (objPurchaseOrder.FreightCharges == null)
            {
                objPurchaseOrder.FreightCharges = 0;
            }

            if (objPurchaseOrder.TaxPercentage == null)
            {
                objPurchaseOrder.TaxPercentage = 0;
            }

            objPurchaseOrder.lstPurchaseOrderItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(Convert.ToString(objPurchaseOrder.JsonlstItemModel));

            string validations = string.Empty;
            int newPurchaseOrderID = objPurchaseOrder.ID;
            try
            {
                if (objPurchaseOrder.ID > 0)
                {
                }
                else
                {
                    PurchaseOrder obj = new PurchaseOrder();
                    L2 objL2 = new L2();
                    using (DapperContext context = new DapperContext())
                    {
                        objPurchaseOrder.L2Code = context.SelectObject<L2>(objPurchaseOrder.L2ID).L2Code;

                        int year = DateTime.Today.Year;
                        int count = (from pr in context.SearchAll<PurchaseOrder>(obj) where Convert.ToDateTime(pr.CreatedDate).Year == year select pr).Count();
                        objPurchaseOrder.PurchaseOrderNo = "PO" + objPurchaseOrder.L2Code + (year % 100).ToString() + (count + 1).ToString().PadLeft(5, '0');
                    }

                    objPurchaseOrder.OrderStatus = "Not Authorised";
                    objPurchaseOrder.DeliveryStatus = "No Delivery";
                    objPurchaseOrder.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        newPurchaseOrderID = context.Save(objPurchaseOrder);

                        if (newPurchaseOrderID > 0)
                        {
                            if (objPurchaseOrder.ID > 0 && objPurchaseOrder.DeliveryStatus.ToLower() == "full delivery")
                            {
                                ////Edit mode only
                                PurchaseService.UpdatePurchaseOrder(objPurchaseOrder.ID);
                            }

                            objPurchaseOrder.ID = newPurchaseOrderID;
                            PurchaseService.NewPurchaseOrderItemDetails(objPurchaseOrder);

                            JobOrderService objService = new JobOrderService();
                            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objPurchaseOrder.workOrderNo, ProjectSession.EmployeeID.ToString());

                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Purchasing_MsgPurchaseOrderAlreadyExists;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        //// return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        ////  return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }

                if (string.IsNullOrEmpty(objPurchaseOrder.workOrderNo))
                {
                    return RedirectToAction(Actions.GetPurchaseOrderPage, Pages.Controllers.Transaction, new { id = newPurchaseOrderID });
                }
                else
                {
                    return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { workOrderNo = objPurchaseOrder.workOrderNo, from = "PO" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the purchase order.
        /// </summary>
        /// <param name="objModel">The object model.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchaseOrderItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePurchaseOrderItems(PurchaseOrderItem objModel, string kendoItemgriddata)
        {
            if (objModel != null)
            {
                decimal tempValue = new decimal();
                objModel.Quantity = objModel.Quantity == null || objModel.Quantity == 0 ? tempValue : objModel.Quantity;
                objModel.UnitPrice = objModel.UnitPrice == null || objModel.UnitPrice == 0 ? tempValue : objModel.UnitPrice;
                objModel.Discount = objModel.Discount == null || objModel.Quantity == 0 ? tempValue : objModel.Discount;
                objModel.Tax = objModel.Tax == null || objModel.Tax == 0 ? tempValue : objModel.Tax;
                objModel.LineCost = objModel.LineCost == null || objModel.LineCost == 0 ? tempValue : objModel.LineCost;
                objModel.BaseCost = objModel.BaseCost == null || objModel.BaseCost == 0 ? tempValue : objModel.BaseCost;
            }

            List<PurchaseOrderItem> lstPurchaseOrderItem = new List<PurchaseOrderItem>();
            lstPurchaseOrderItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendoItemgriddata);

            if (objModel.Guid_ItemID == new Guid())
            {
                objModel.Guid_ItemID = Guid.NewGuid();
                lstPurchaseOrderItem.Add(objModel);
            }
            else
            {
                PurchaseOrderItem editPurchaseOrderItem = lstPurchaseOrderItem.FirstOrDefault(l => l.Guid_ItemID == objModel.Guid_ItemID);
                objModel.ID = editPurchaseOrderItem.ID; // add for edit
                int editIndex = lstPurchaseOrderItem.IndexOf(editPurchaseOrderItem);

                lstPurchaseOrderItem.RemoveAt(editIndex);
                lstPurchaseOrderItem.Insert(editIndex, objModel);
            }

            TempData["GetPurchaseOrderItemdata"] = lstPurchaseOrderItem;
            return Json(lstPurchaseOrderItem);
        }

        /// <summary>
        /// Gets the purchase order item details.
        /// </summary>
        /// <param name="autoGuid">The automatic unique identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetPurchaseOrderItemDetails(string autoGuid, string kendogriddata)
        {
            List<PurchaseOrderItem> lstPurchaseOrderItem = new List<PurchaseOrderItem>();
            lstPurchaseOrderItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendogriddata);
            PurchaseOrderItem model = new PurchaseOrderItem();
            if (!string.IsNullOrEmpty(autoGuid))
            {
                Guid id = new Guid(autoGuid);
                model = lstPurchaseOrderItem.FirstOrDefault(l => l.Guid_ItemID == id);
            }

            TempData["GetPurchaseOrderItemdata"] = lstPurchaseOrderItem;
            return Json(model);
        }

        /// <summary>
        /// Deletes the purchase order items.
        /// </summary>
        /// <param name="guid_ItemID">The guid_ item identifier.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchaseOrderItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchaseOrderItems(string guid_ItemID, string kendoItemgriddata)
        {
            try
            {
                Guid guid_ItemID1 = new Guid(guid_ItemID);
                List<PurchaseOrderItem> lstPurchaseOrderItem = new List<PurchaseOrderItem>();
                lstPurchaseOrderItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendoItemgriddata);

                PurchaseOrderItem editPurchaseOrderItem = lstPurchaseOrderItem.FirstOrDefault(l => l.Guid_ItemID == guid_ItemID1);
                ////objModel.ID = editPurchaseOrderItem.ID; // add for edit
                int editIndex = lstPurchaseOrderItem.IndexOf(editPurchaseOrderItem);
                lstPurchaseOrderItem.RemoveAt(editIndex);
                TempData["GetPurchaseOrderItemdata"] = lstPurchaseOrderItem;
                //// return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                ////AssetService.CheckAssetChildRefExist(assetID);
                ////AssetService.DeleteAssets(assetID);
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Updates the delivery and order status po.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="deliveryStatus">The delivery status.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateDeliverynOrderStatusPO)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateDeliverynOrderStatusPO(int id, string deliveryStatus)
        {
            try
            {
                PurchaseService.UpdateDeliverynOrderStatusPO(id, deliveryStatus, "Cancelled", ProjectSession.EmployeeID);
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.PurchaseOrder_MsgCancelPO }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the po authentication history.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPOAuthHistory)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPOAuthHistory([DataSourceRequest]DataSourceRequest request, int id)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PO_Auth_ID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult()
            {
                Data = objService.GetPOAuthHistory(pageNumber, sortExpression, sortDirection, id, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Validates the employee.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="deliveryStatus">The delivery status.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="password">The password.</param>
        /// <param name="authorizationLimit">The authorization limit.</param>
        /// <param name="status">The status.</param>
        /// <returns></returns>
        [ActionName(Actions.ValidateEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ValidateEmployee(int id, string deliveryStatus, string userName, string password, decimal authorizationLimit, string status)
        {
            try
            {
                employees objemployees = EmployeeService.ValidateEmployee(userName, password, Convert.ToString(System.Web.HttpContext.Current.Request.UserHostAddress));
                if (objemployees != null)
                {
                    using (ServiceContext context = new ServiceContext())
                    {
                        EmployeeStatuse obj = new EmployeeStatuse();
                        string employeeStatus = context.SearchAll<EmployeeStatuse>(obj).Where(x => x.EmployeeStatusId == objemployees.EmployeeStatusId).FirstOrDefault().EmployeeStatus;
                        if (employeeStatus.ToLower() == "terminated" || employeeStatus.ToLower() == "in-active")
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgUserNotActive }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (authorizationLimit > 0)
                            {
                                if (objemployees.POAuthorisationLimit > 0)
                                {
                                    if (authorizationLimit > objemployees.POAuthorisationLimit)
                                    {
                                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchaseOrder_MsgPOExceedsLimit }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        PurchaseService.UpdateDeliverynOrderStatusPO(id, deliveryStatus, status, objemployees.EmployeeID);
                                        /*(Start)Change Work Order Status if item is Approved (By Pratik on 05-Dec-2016 for NTM-126)*/
                                        PurchaseOrderItem objPOItem = new PurchaseOrderItem();
                                        objPOItem.PurchaseOrderID = id;
                                        IList<string> lstWorkOrderNos = context.Search<PurchaseOrderItem>(objPOItem).Where(x => x.JobOrderID != null).Select(x => x.JobOrderID).Distinct().ToList();
                                        JobOrderService objService = new JobOrderService();
                                        foreach (string workOrderNo in lstWorkOrderNos)
                                        {
                                            if (objService.ChangeJobOrderStatusByWorkorderNo(workOrderNo, SystemEnum.WorkRequestStatus.WaitingforParts.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                                            {
                                                objService.NewJOStatusAudit(workOrderNo, SystemEnum.WorkRequestStatus.WaitingforParts.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                                            }
                                        }

                                        /*(End)Change Work Order Status if item is Approved (By Pratik on 05-Dec-2016 for NTM-126)*/
                                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.PurchaseOrder_MsgPOAuthorised }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchaseOrder_MsgPONotSetLimit }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchaseOrder_MsgPOTotalValidation }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgPwdNotValid }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "document Tab"

        /// <summary>
        /// _s the get purchase order documents.
        /// </summary>
        /// <param name="orderID">The order identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetPODocuments)]
        public ActionResult _GetPODocuments(int orderID)
        {
            ExtAssetFile objExtAssetFile = new ExtAssetFile();
            objExtAssetFile.FkId = Convert.ToString(orderID);
            objExtAssetFile.ModuleType = SystemEnum.DocumentModuleType.PO.ToString();

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseOrder_Deleterecords)
            {
                objExtAssetFile.IsDeleteButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseOrder_Createnewrecords)
            {
                objExtAssetFile.IsSaveButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseOrder_Editrecords)
            {
                objExtAssetFile.IsEditButtonEnable = false;
            }

            return PartialView(PartialViews._PurchaseOrderDocumentList, objExtAssetFile);
        }

        /// <summary>
        /// Gets the purchase order document list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="foreignKeyID">The foreign key identifier.</param>
        /// <param name="moduleType">Type of the module.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPODocumentList)]
        public ActionResult GetPODocumentList([DataSourceRequest]DataSourceRequest request, string foreignKeyID, string moduleType, string kendoItemgriddata)
        {
            List<PurchaseOrderItem> lstPurchaseOrderItem = new List<PurchaseOrderItem>();
            lstPurchaseOrderItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendoItemgriddata);
            string requestIDs = string.Empty;
            if (lstPurchaseOrderItem.Count > 0)
            {
                List<string> lstPurchaseRequestNos = lstPurchaseOrderItem.Select(x => x.PurchaseRequestNo).Distinct().ToList();
                if (lstPurchaseRequestNos != null)
                {
                    for (int i = 0; i < lstPurchaseRequestNos.Count; i++)
                    {
                        PurchaseRequest objPurchaseRequest = new PurchaseRequest();
                        objPurchaseRequest.PurchaseRequestID = lstPurchaseRequestNos[i];

                        if (objPurchaseRequest.PurchaseRequestID != null)
                        {
                            using (ServiceContext context = new ServiceContext())
                            {
                                objPurchaseRequest = context.Search<PurchaseRequest>(objPurchaseRequest).FirstOrDefault();
                                if (objPurchaseRequest != null)
                                {
                                    if (requestIDs == string.Empty)
                                    {
                                        requestIDs = Convert.ToString(objPurchaseRequest.Id);
                                    }
                                    else
                                    {
                                        requestIDs = requestIDs + objPurchaseRequest.Id;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "FileName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            DocumentService obj = new DocumentService();

            var result = new DataSourceResult()
            {
                Data = obj.GetPODocumentList(foreignKeyID, moduleType, requestIDs, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }
        #endregion

        #region "Print Report"

        /// <summary>
        /// Prints the purchase order.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.PrintPurchaseOrder)]
        public ActionResult PrintPurchaseOrder(int id)
        {
            List<PurchaeOrderItemTable> objDetails = new List<PurchaeOrderItemTable>();
            IList<PurchaeOrderItemTable> lstDetails = new List<PurchaeOrderItemTable>();

            PurchaeOrderTable objPurchaseOrderTable = new PurchaeOrderTable();

            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;

            string strQuery = string.Empty;

            if (id > 0)
            {
                parameters.Add(new DBParameters()
                {
                    Name = "POID",
                    Value = id,
                    DBType = DbType.Int32
                });
            }

            ////strQuery = "Select Convert(int,Row_Number() Over(Order by PurchaseOrderNo)) as RowNo,PurchaseOrderNo,CreatedDate,Name,CostCenterName,AccountCode,OrderStatus,DeliveryStatus " +
            ////          "  ,SupplierName,L2Name,Currency_TotalCost,TotalCost_BaseCurrency " +
            ////          "  From(Select PurchaseOrderNo,PO.CreatedDate,emp.Name,CostCenterName,AccountCode,OrderStatus,DeliveryStatus,SupplierName,L2Name " +
            ////          "  ,CurrencyCode + ' ' + CAST(OrderTotal as nvarchar(25)) 'Currency_TotalCost',TotalBaseCost 'TotalCost_BaseCurrency' " +
            ////          "  from PurchaseOrder PO " +
            ////          "  Left outer join accountcode AC on AC.AccCodeid = PO.AccountCodeID " +
            ////          "  Left outer join CostCenter CC on CC.CostCenterId = AC.CostCenterId " +
            ////          " Left outer join Suppliers on Suppliers.SupplierID = PO.SupplierID " +
            ////          "  Left outer join L2 on L2.L2ID = PO.L2ID " +
            ////          "  Left outer join Currency on Currency.CurrencyID = PO.CurrencyID " +
            ////          "  Left outer join employees emp on emp.EmployeeID = PO.Orderby  Where PO.ID = @POID";

            strQuery = " Select PurchaseOrder.*, " +
                            " OrderByEmp.EmployeeNO As OrderByEmployeeNO,OrderByEmp.Name As OrderByEmployeeName,OrderByEmp.AltName As OrderByAltEmployeeName, " +
                            " InvoiceToEmp.EmployeeNO As InvoiceToEmployeeNO,InvoiceToEmp.Name As InvoiceToEmployeeName,InvoiceToEmp.AltName As InvoiceToAltEmployeeName, " +
                            " Accountcode.AccountCode,Accountcode.AccountCodeDesc,Accountcode.AltAccountCodeDesc,  " +
                            " CostCenter.CostCenterNo,CostCenter.CostCenterName,CostCenter.AltCostCenterName, " +
                            " suppliers.SupplierNo,suppliers.SupplierName,suppliers.AltSupplierName, " +
                            " Currency.CurrencyCode,Currency.CurrencyDescription,Currency.AltCurrencyDescription,Currency.Rate as CurrencyRate, " +
                            " L2.L2code as L2Name, ShippedVia.ShippedViaDesc,TermofSale.TermofSaleDesc,PaymentTerms.PaymentTermsDesc " +
                            " FROM PurchaseOrder  " +
                            " INNER JOIN employees OrderByEmp on OrderByEmp.EmployeeID = PurchaseOrder.OrderBy " +
                            " INNER JOIN employees InvoiceToEmp on InvoiceToEmp.EmployeeID = PurchaseOrder.InvoiceTo " +
                            " INNER Join suppliers on suppliers.SupplierID = PurchaseOrder.SupplierID " +
                            " LEFT JOIN Accountcode on Accountcode.AccCodeid = PurchaseOrder.AccountCodeID " +
                            " LEFT JOIN CostCenter on CostCenter.CostCenterId = accountcode.CostCenterId " +
                            " LEFT JOIN Currency on Currency.CurrencyID = PurchaseOrder.CurrencyID " +
                            " LEFT JOIN ShippedVia on ShippedVia.ID = PurchaseOrder.ShippedViaID " +
                            " LEFT JOIN TermofSale on TermofSale.ID = PurchaseOrder.TermOfSaleID " +
                            " LEFT JOIN PaymentTerms on PaymentTerms.ID = PurchaseOrder.PaymentTermsID " +
                            "LEFT JOIN L2 on L2.L2ID = PurchaseOrder.L2ID " +
                            "Where PurchaseOrder.ID = @POID";

            query = "SELECT  Convert(int,Row_number() OVER (ORDER BY POI.ID)) AS RowNo, stockcode.StockNo, POI.PartDescription, POI.uom, POI.Quantity, ISNULL(POI.JoborderID, '') AS WorkorderNo, PO.ID ,ISNULL(Tax,0) as Tax,ISNULL(Discount,0) as Discount,LineCost,BaseCost FROM PurchaseOrderItems AS POI INNER JOIN PurchaseOrder AS PO ON POI.PurchaseOrderID = PO.ID LEFT OUTER JOIN stockcode ON stockcode.StockID = POI.PartNumber WHERE 1=1 and POI.PurchaseOrderID = @POID ";
            using (DapperDBContext context = new DapperDBContext())
            {
                objPurchaseOrderTable = context.ExecuteQuery<PurchaeOrderTable>(strQuery, parameters).FirstOrDefault();
                lstDetails = context.ExecuteQuery<PurchaeOrderItemTable>(query, parameters);
            }

            if (lstDetails != null && lstDetails.Count > 0)
            {
                objDetails = lstDetails.ToList();
            }

            PrintPurchaseOrderReport report = new PrintPurchaseOrderReport();
            report.DataSource = objDetails;

            report.txtCity.Text = objPurchaseOrderTable.L2Name;
            report.txtDate.Text = Convert.ToString(Common.FormatDateTime(objPurchaseOrderTable.CreatedDate, ProjectConfiguration.DATEFORMATDDMMMYYY));
            report.txtShippedVia.Text = objPurchaseOrderTable.ShippedViaDesc;
            report.txtPaymentTerms.Text = objPurchaseOrderTable.PaymentTermsDesc;
            report.txtTermofSale.Text = objPurchaseOrderTable.TermofSaleDesc;
            report.txtAccountCode.Text = objPurchaseOrderTable.AccountCodeDesc;

            report.txtRowNo.DataBindings.Add("Text", objDetails, "Rowno");
            report.txtItemNo.DataBindings.Add("Text", objDetails, "StockNo");
            report.txtDescription.DataBindings.Add("Text", objDetails, "PartDescription");
            report.txtUOM.DataBindings.Add("Text", objDetails, "uom");
            report.txtQuantity.DataBindings.Add("Text", objDetails, "Quantity");
            report.txtTax.DataBindings.Add("Text", objDetails, "Tax");
            report.txtDiscount.DataBindings.Add("Text", objDetails, "Discount");
            report.txtAmount.DataBindings.Add("Text", objDetails, "LineCost");

            report.txtName.Text = ProjectSession.EmployeeName;

            return View(Views.PrintView, report);
        }
        #endregion


        #region Asset Report

        /// <summary>
        /// List
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.POPrint)]
        public ActionResult POPrint(int id = 0)
        {
            
            PurchaseOrderPrint rpt = new PurchaseOrderPrint();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetList;

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;

            

            //#region "Colum header"
            //rpt.FindControl("lblSrNo", false).Text = ProjectSession.Resources.label.Report_TextBlockRowNo;
            //rpt.FindControl("lblAssetNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetNO;
            //if (lang)
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetDescription;
            //else
            //    rpt.FindControl("lblAssetDescription", false).Text = ProjectSession.Resources.label.Asset_TextBlockAltAssetDescription;
            //rpt.FindControl("lblModelNo", false).Text = ProjectSession.Resources.label.Asset_TextBlockModelNo;
            //rpt.FindControl("lblSerialNo", false).Text = ProjectSession.Resources.label.Employee_TextBlockSerialNo;
            //rpt.FindControl("lblAssetCatCode", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetCatCode;
            //rpt.FindControl("lblL2Code", false).Text = ProjectSession.Resources.label.Site_TextBlockSiteCodes;
            //rpt.FindControl("lblL3", false).Text = ProjectSession.Resources.label.Area_TextBlockAreaCode;
            //rpt.FindControl("lbll5", false).Text = ProjectSession.Resources.label.Building_TextBlockBuilding;
            //rpt.FindControl("lbllocation", false).Text = ProjectSession.Resources.label.Location_TextBlockLocationNo;
            //#endregion

            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = "Purchase Order";
            rpt.Parameters["l1Id"].Value = 1;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = "1";
            rpt.Parameters["poid"].Value = id;
            

            ViewData["Report"] = rpt;
            return View();
        }

        #endregion
    }
}