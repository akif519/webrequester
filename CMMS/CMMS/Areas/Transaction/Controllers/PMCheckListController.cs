﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Check List Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "For Check List Popup"
        /// <summary>
        /// Gets the pm checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklist)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetPMChecklist([DataSourceRequest]DataSourceRequest request, string strWhere)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                PMCheckListService objService = new PMCheckListService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetPMChecklistPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "PM CheckList"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMCheckList)]
        public ActionResult PMCheckList()
        {
            if (ProjectSession.PermissionAccess.Preventive_PMChecklists_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.PMCheckList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the pm checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistAdvanceSearch)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetPMChecklistAdvanceSearch([DataSourceRequest]DataSourceRequest request, string strWhere)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ChecklistNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMCheckList objPmCheckList = new PMCheckList();

            string strWhereClauseWithEmployee = string.Empty;
            int employeeID = ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID;
            if (employeeID > 0)
            {
                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                strWhereClauseWithEmployee = "  and pmchecklist.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            }

            PMCheckListService objService = new PMCheckListService();

            var result = new DataSourceResult()
            {
                Data = objService.GetPMChecklistPageAdvancedSearch(objPmCheckList, SystemEnum.Pages.PMCheckList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhereClauseWithEmployee, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
            ////Data = objService.GetPMChecklistPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection),
        }

        /// <summary>
        /// Gets the Section by identifier.
        /// </summary>
        /// <param name="checklistID">The checkList identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistByID)]
        [HttpGet]
        public ActionResult GetPMChecklistByID(int checklistID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Preventive_PMChecklists_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                PMCheckList objPMcheckList = new PMCheckList();
                string strWhere = string.Empty;
                if (checklistID > 0)
                {
                    strWhere = " and pmchecklist.ChecklistID = " + checklistID;
                    string sortExpression = string.Empty;
                    string sortDirection = string.Empty;
                    int pageNumber = 1;
                    PMCheckListService objService = new PMCheckListService();
                    objPMcheckList = objService.GetPMChecklistPage(strWhere, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection).FirstOrDefault();
                }

                return View(Views.PMCheckListDetails, objPMcheckList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Manages the PM CheckList.
        /// </summary>
        /// <param name="objCheckList">The object CheckList.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMChecklist)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePMChecklist(PMCheckList objCheckList)
        {
            string validations = string.Empty;
            int newCheckListID = objCheckList.ChecklistID;
            try
            {
                HttpPostedFileBase file1;
                if (Request.Files.Count > 0)
                {
                    file1 = Request.Files[0];
                    string fileName1 = Path.GetFileName(file1.FileName);

                    if (!("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(fileName1) + ","))
                    {
                        ////return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed }, JsonRequestBehavior.AllowGet);

                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                        if (objCheckList.ChecklistID > 0)
                        {
                            ExtAssetFile objExtAssetFile = new ExtAssetFile();
                            objExtAssetFile.FkId = Convert.ToString(objCheckList.ChecklistID);
                            objExtAssetFile.ModuleType = "P";
                            using (ServiceContext context = new ServiceContext())
                            {
                                objExtAssetFile = context.Search<ExtAssetFile>(objExtAssetFile).FirstOrDefault();
                                objCheckList.FileName = objExtAssetFile.FileName;
                            }
                        }

                        return View(Views.PMCheckListDetails, objCheckList);
                    }
                }

                if (objCheckList.EstimatedLaborHours == null || objCheckList.EstimatedLaborHours <= 0)
                {
                    objCheckList.EstimatedLaborHours = 0;
                }

                if (objCheckList.ChecklistID > 0)
                {
                    objCheckList.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objCheckList.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCheckList.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objCheckList.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        PMCheckListService objCheckListService = new PMCheckListService();
                        var lstchkli = objCheckListService.checkDuplicatedforCheckListNo(objCheckList.ChecklistID, objCheckList.ChecklistNo);
                        if (lstchkli.Count > 0)
                        {
                            if (objCheckList.ChecklistID > 0)
                            {
                                TempData["Message"] = ProjectSession.Resources.label.PMChecklist_MsgPMChecklistNoAlreadyExists;
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                                return RedirectToAction(Actions.GetPMChecklistByID, Pages.Controllers.Transaction, new { checklistID = newCheckListID });
                            }
                            else
                            {
                                ViewBag.Message = ProjectSession.Resources.label.PMChecklist_MsgPMChecklistNoAlreadyExists;
                                ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                                return View(Views.PMCheckListDetails, objCheckList);
                            }
                        }

                        newCheckListID = context.Save(objCheckList);
                        if (newCheckListID > 0)
                        {
                            //// Code Related To File(Attachment) Upload 
                            ExtAssetFile obj = new ExtAssetFile();
                            HttpPostedFileBase file;
                            if (Request.Files.Count > 0)
                            {
                                file = Request.Files[0];
                                string subPath = string.Empty;
                                string filePath = string.Empty;

                                ExtAssetFile objExtAssetFile = new ExtAssetFile();

                                //// Remove Old Attachments if already exists
                                if (objCheckList.AutoId > 0)
                                {
                                    context.Delete<ExtAssetFile>(objCheckList.AutoId);
                                }

                                obj.ModuleType = SystemEnum.DocumentModuleType.P.ToString();
                                subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathPreventive + obj.FkId;

                                bool folderExists = Directory.Exists(subPath);
                                if (!folderExists)
                                {
                                    Directory.CreateDirectory(subPath);
                                }

                                filePath = subPath + "\\" + Path.GetFileName(file.FileName);
                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                obj.FileDescription = objCheckList.FileDescription;
                                obj.CreatedBy = ProjectSession.EmployeeID;
                                obj.CreatedDate = DateTime.Now;
                                obj.ModifiedBy = ProjectSession.EmployeeID;
                                obj.ModifiedDate = DateTime.Now;
                                obj.FileLink = ProjectConfiguration.UploadPathPreventive + obj.FkId + "\\" + Path.GetFileName(file.FileName);
                                obj.FileName = Path.GetFileName(file.FileName);
                                obj.FkId = Convert.ToString(newCheckListID);
                                context.Save(obj);
                            }
                            else
                            {
                                //// for update Desc
                                if (objCheckList.AutoId > 0)
                                {
                                    obj = context.SelectObject<ExtAssetFile>(objCheckList.AutoId);
                                    obj.FileDescription = objCheckList.FileDescription;
                                    obj.ModifiedBy = ProjectSession.EmployeeID;
                                    obj.ModifiedDate = DateTime.Now;
                                    context.Save(obj);
                                }
                            }

                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                            return View(Views.PMCheckListDetails, objCheckList);
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.PMCheckListDetails, objCheckList);
                }

                return RedirectToAction(Actions.GetPMChecklistByID, Pages.Controllers.Transaction, new { checklistID = newCheckListID });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the CheckList.
        /// </summary>
        /// <param name="checklistid">The CheckList identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePMChecklist)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePMChecklist(int checklistid)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<PMCheckList>(checklistid, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Update Safety ID for the CheckList.
        /// </summary>
        /// <param name="checklistID">The checklist identifier.</param>
        /// <param name="safetyID">The safety identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateSafetyIDForPMCheckList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateSafetyIDForPMCheckList(int checklistID, int? safetyID)
        {
            try
            {
                safetyID = safetyID == 0 ? null : safetyID;
                if (checklistID > 0)
                {
                    PMCheckListService.UpdateSafetyInstruction(checklistID, safetyID);
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Check List Elements"

        /// <summary>
        /// Manages the check list elements.
        /// </summary>
        /// <param name="objOldChecklistele">The object old check list element.</param>
        /// <param name="objNewChecklistele">The object new check list element.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCheckListElements)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ManageCheckListElements(Checklistelement objOldChecklistele, Checklistelement objNewChecklistele, string kendogriddata)
        {
            List<Checklistelement> lstChecklistelement = new List<Checklistelement>();
            lstChecklistelement = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Checklistelement>>(kendogriddata);

            PMCheckListService objService = new PMCheckListService();

            bool isSeqUpdated = false;
            int maxSeqId = 1;
            int minSeqId = 1;
            int difference = 0;

            if (lstChecklistelement.Count > 0)
            {
                maxSeqId = lstChecklistelement.Max(x => x.SeqNo);
                minSeqId = lstChecklistelement.Min(x => x.SeqNo);
            }

            if (objOldChecklistele.SeqNo > -1 && (objNewChecklistele.SeqNo != objOldChecklistele.SeqNo) && objNewChecklistele.SeqNo > maxSeqId && objOldChecklistele.SeqNo == maxSeqId)
            {
                ////Updating Max Seq ID  Item With SeqID > Max ID, Then Do not update seqid, just update content
                objNewChecklistele.SeqNo = objOldChecklistele.SeqNo;
            }
            else
            {
                ////Adding/Updating Item with SeqID > MaxSeqID
                if (objNewChecklistele.SeqNo > maxSeqId + 1)
                {
                    objNewChecklistele.SeqNo = maxSeqId + 1;
                }
            }

            if (objOldChecklistele.SeqNo > -1 && (objNewChecklistele.SeqNo != objOldChecklistele.SeqNo))
            {
                ////Update Sequence
                objOldChecklistele = lstChecklistelement.Find(x => x.SeqNo == objOldChecklistele.SeqNo && x.TaskDesc == objOldChecklistele.TaskDesc);
                lstChecklistelement.Remove(objOldChecklistele);
                objNewChecklistele.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewChecklistele.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOldChecklistele.SeqNo > -1 && (objNewChecklistele.SeqNo == objOldChecklistele.SeqNo))
            {
                ////Updating just content, not seq
                objNewChecklistele.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewChecklistele.ModifiedDate = DateTime.Now;
                objOldChecklistele = lstChecklistelement.Find(x => x.SeqNo == objOldChecklistele.SeqNo && x.TaskDesc == objOldChecklistele.TaskDesc);
                lstChecklistelement.Remove(objOldChecklistele);
            }
            else if (objOldChecklistele.SeqNo == -1)
            {
                ////Adding New Item
                objNewChecklistele.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNewChecklistele.CreatedDate = DateTime.Now;
            }

            if (isSeqUpdated)
            {
                if (objOldChecklistele.SeqNo < objNewChecklistele.SeqNo)
                {
                    if (objNewChecklistele.SeqNo > maxSeqId)
                    {
                        objNewChecklistele.SeqNo = maxSeqId;
                    }

                    lstChecklistelement.Where(x => x.SeqNo > objOldChecklistele.SeqNo && x.SeqNo <= objNewChecklistele.SeqNo).ToList().ForEach(p =>
                    {
                        p.SeqNo = p.SeqNo - 1;
                    });
                }
                else if (objOldChecklistele.SeqNo > objNewChecklistele.SeqNo)
                {
                    if (objNewChecklistele.SeqNo < minSeqId)
                    {
                        if (minSeqId > 1)
                        {
                            difference = minSeqId - objNewChecklistele.SeqNo;
                            minSeqId = 1;

                            objNewChecklistele.SeqNo = minSeqId;
                            lstChecklistelement.ForEach(p =>
                            {
                                p.SeqNo = p.SeqNo - difference;
                            });
                        }
                    }
                    else
                    {
                        lstChecklistelement.Where(x => x.SeqNo >= objNewChecklistele.SeqNo && x.SeqNo < objOldChecklistele.SeqNo).ToList().ForEach(p =>
                        {
                            p.SeqNo = p.SeqNo + 1;
                        });
                    }
                }
            }
            else
            {
                if (objOldChecklistele.SeqNo == -1 && lstChecklistelement.Any(x => x.SeqNo == objNewChecklistele.SeqNo))
                {
                    ////Inserting New Item in the list between any two Items
                    lstChecklistelement.Where(x => x.SeqNo >= objNewChecklistele.SeqNo).ToList().ForEach(p =>
                    {
                        p.SeqNo = p.SeqNo + 1;
                    });
                }
                else
                {
                    if (lstChecklistelement.Count == 0)
                    {
                        objNewChecklistele.SeqNo = minSeqId;
                    }
                }
            }

            lstChecklistelement.Add(objNewChecklistele);

            bool result = objService.SaveCheckListElement(lstChecklistelement);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the CheckList Element.
        /// </summary>
        /// <param name="checklistelementId">The CheckList Element identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChecklistElement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChecklistElement(int checklistelementId, string kendogriddata)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Checklistelement>(checklistelementId, true);

                if (returnValue == 0)
                {
                    List<Checklistelement> lstChecklistelement = null;
                    Checklistelement objDelete = new Checklistelement();
                    if (!string.IsNullOrEmpty(kendogriddata))
                    {
                        lstChecklistelement = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Checklistelement>>(kendogriddata);
                    }
                    else
                    {
                        lstChecklistelement = new List<Checklistelement>();
                    }

                    objDelete = lstChecklistelement.Find(m => m.ChecklistelementId == checklistelementId);
                    lstChecklistelement.Remove(objDelete);

                    lstChecklistelement.Where(m => m.SeqNo > objDelete.SeqNo).ToList().ForEach(p =>
                    {
                        p.SeqNo = p.SeqNo - 1;
                        p.ModifiedBy = ProjectSession.EmployeeID.ToString();
                        p.ModifiedDate = DateTime.Now;
                    });

                    PMCheckListService objService = new PMCheckListService();
                    bool result = objService.SaveCheckListElement(lstChecklistelement);

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region "CheckListElements"
        /// <summary>
        /// Gets the Check List Elements DropDown
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="checklistid">The CheckList ID.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCheckListElements)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCheckListElements([DataSourceRequest]DataSourceRequest request, int checklistid)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            int pageNumber = 1;
            if (request != null && request.Sorts != null && request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;
            }

            int empID = ProjectSession.EmployeeID;

            PMCheckListService context = new PMCheckListService();
            var result = new DataSourceResult()
            {
                Data = context.GetCheckListElementsByID(checklistid, pageNumber, sortExpression, sortDirection, request: request),
                Total = context.PagingInformation.TotalRecords
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To Get Area Details
        /// </summary>
        /// <param name="checklistid">The CheckList ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetCheckListElementsDDL)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetCheckListElementsDDL(int checklistid)
        {
            try
            {
                PMCheckListService context = new PMCheckListService();
                var result = context.GetCheckListElementsByID(checklistid, 0, string.Empty, string.Empty);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Check List Items"

        /// <summary>
        /// Gets the Check List Items
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="chkLstID">The CheckList ID.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCheckListInv)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCheckListInv([DataSourceRequest]DataSourceRequest request, int chkLstID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartsListNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            PartsList objPartsList = new PartsList();

            PMCheckListService context = new PMCheckListService();

            var result = new DataSourceResult()
            {
                Data = context.GetCheckListInvByCheckListID(chkLstID, pageNumber, sortExpression, sortDirection, request: request),
                Total = context.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the PM CheckList Item.
        /// </summary>
        /// <param name="objCheckListInv">The object CheckList Items.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCheckListInv)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCheckListInv(ChecklistInv objCheckListInv)
        {
            string validations = string.Empty;
            try
            {
                if (objCheckListInv.ChecklistInvId > 0)
                {
                    objCheckListInv.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListInv.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCheckListInv.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListInv.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (ServiceContext dappercontext = new ServiceContext())
                    {
                        Checklistelement objCheckListElement = new Checklistelement();
                        PMCheckListService objCheckListService = new PMCheckListService();
                        objCheckListElement = objCheckListService.GetCheckListElementIDBySeqNo(objCheckListInv.ChecklistID, objCheckListInv.SeqNo).FirstOrDefault();
                        objCheckListInv.ChecklistelementId = objCheckListElement.ChecklistelementId;

                        ChecklistInv objdupCheckListInv = new ChecklistInv();
                        var lstchkli = objCheckListService.checkDuplicatedforListInv(objCheckListInv.ChecklistID, objCheckListElement.ChecklistelementId, objCheckListInv.PartID);
                        objdupCheckListInv = lstchkli.FirstOrDefault();
                        if (lstchkli.Count > 0 && objdupCheckListInv.ChecklistInvId != objCheckListInv.ChecklistInvId)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                        }

                        if (dappercontext.Save(objCheckListInv) > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                        else
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the CheckList Items.
        /// </summary>
        /// <param name="checkListInvID">The CheckList Item identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChecklistInv)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChecklistInv(int checkListInvID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<ChecklistInv>(checkListInvID, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region "Check List Tools"
        /// <summary>
        /// Gets the Check List Items
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="chkLstID">The CheckList ID.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCheckListTools)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCheckListTools([DataSourceRequest]DataSourceRequest request, int chkLstID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            PartsList objPartsList = new PartsList();

            PMCheckListService context = new PMCheckListService();

            var result = new DataSourceResult()
            {
                Data = context.GetCheckListToolsByCheckListID(chkLstID, pageNumber, sortExpression, sortDirection, request: request),
                Total = context.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the PM CheckList Tools.
        /// </summary>
        /// <param name="objCheckListTool">The object CheckList Tools.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCheckListTools)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCheckListTools(ChecklistTool objCheckListTool)
        {
            string validations = string.Empty;
            try
            {
                if (objCheckListTool.ChecklistToolID > 0)
                {
                    objCheckListTool.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListTool.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCheckListTool.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListTool.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (ServiceContext dappercontext = new ServiceContext())
                    {
                        if (dappercontext.Save(objCheckListTool) > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// saves tools For Job Order
        /// </summary>
        /// <param name="toolID">The tool identifier.</param>
        /// <param name="toolQuantity">The tool quantity.</param>
        /// <param name="checkListId">The check list identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SavePMCheckListTools)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SavePMCheckListTools(int toolID, int toolQuantity, int checkListId)
        {
            string result = string.Empty;
            JobOrderService objService = new JobOrderService();
            try
            {
                int checkListToolID = 0;
                if (toolID > 0 && toolQuantity > 0 && checkListId > 0)
                {
                    ChecklistTool objWOTool = new ChecklistTool();
                    Tool objTool = new Tool();
                    objTool.ToolsID = toolID;
                    using (ServiceContext objContext = new ServiceContext())
                    {
                        objTool = objContext.Search<Tool>(objTool).FirstOrDefault();
                    }

                    if (objTool != null && objTool.ToolsID > 0 && !string.IsNullOrEmpty(objTool.ToolsDesc))
                    {
                        objWOTool.ChecklistID = checkListId;
                        objWOTool.ToolDescription = objTool.ToolsDesc;
                        objWOTool.AltToolDescription = objTool.ToolsAltDesc;
                        objWOTool.Quantity = toolQuantity;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            checkListToolID = objContext.Save<ChecklistTool>(objWOTool);
                        }
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "CheckListID cannot be blank. " + Environment.NewLine + "Tool is required field. " + Environment.NewLine + " Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }

                if (checkListToolID > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    throw new Exception("Error");
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the CheckList Tools.
        /// </summary>
        /// <param name="checkListToolID">The CheckList Tools identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChecklistTools)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChecklistTools(int checkListToolID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<ChecklistTool>(checkListToolID, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Check List PPE"
        /// <summary>
        /// Gets the Check List PPE
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="chkLstID">The CheckList ID.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCheckListPPE)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCheckListPPE([DataSourceRequest]DataSourceRequest request, int chkLstID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            PartsList objPartsList = new PartsList();

            PMCheckListService context = new PMCheckListService();

            var result = new DataSourceResult()
            {
                Data = context.GetCheckListPPEByCheckListID(chkLstID, pageNumber, sortExpression, sortDirection, request: request),
                Total = context.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the PM CheckList PPE.
        /// </summary>
        /// <param name="objCheckListPPE">The object CheckList Tools.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCheckListPPE)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCheckListPPE(ChecklistPPE objCheckListPPE)
        {
            string validations = string.Empty;
            try
            {
                if (objCheckListPPE.SeqNo >= 0)
                {
                    using (ServiceContext context = new ServiceContext())
                    {
                        int count = PMCheckListService.CheckPPEDuplicate(objCheckListPPE);
                        if (count > 0)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                        }
                    }
                }

                if (objCheckListPPE.ChecklistPPEID > 0)
                {
                    objCheckListPPE.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListPPE.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCheckListPPE.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objCheckListPPE.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (ServiceContext dappercontext = new ServiceContext())
                    {
                        if (dappercontext.Save(objCheckListPPE) > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the CheckList PPE.
        /// </summary>
        /// <param name="checkListPPEID">The CheckList PPE identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChecklistPPE)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChecklistPPE(int checkListPPEID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<ChecklistPPE>(checkListPPEID, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
    }
}