﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Personal Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Personals the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PersonalList)]
        public ActionResult PersonalList()
        {
            if (ProjectSession.PermissionAccess.RequestCard_Personal_Allowaccess && ProjectSession.PermissionAccess.RequestCard__ShowHideModule)
            {
                return View(Views.PersonalList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the personal list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPersonalList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPersonalList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            string substoreId = string.Empty;
            substoreId = Common.GetConfigKeyValue("SubStorePersonal");

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CardId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            string whereClause = string.Empty;

            if (!ProjectSession.IsCentral)
            {
                whereClause = " and SubStoreIdRef in ( Select s.SubStoreId from SubStore s where s.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + " ) ) ";
            }

            whereClause += " and Status <> 3 and SubStoreIdRef = " + ConvertTo.Integer(substoreId);

            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objSubStoreCardForm, SystemEnum.Pages.PersonalList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Personals the detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.PersonalDetail)]
        public ActionResult PersonalDetail(int id)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            string substoreId = string.Empty;
            substoreId = Common.GetConfigKeyValue("SubStorePersonal");

            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();
            StockCodeService objService = new StockCodeService();
            objSubStoreCardForm.CardId = id;
            if (id > 0)
            {
                objSubStoreCardForm = objService.GetConsumablesDetailByCardID(id);
            }
            else
            {
                objSubStoreCardForm.Status = 2;
                objSubStoreCardForm.DateValue = DateTime.Now;
                objSubStoreCardForm.SubStoreIdRef = ConvertTo.Integer(substoreId);
            }

            return View(Views.PersonalDetails, objSubStoreCardForm);
        }

        #region "Store Card Transfer Popup"

        /// <summary>
        /// _s the store card transfer popup.
        /// </summary>
        /// <param name="cardId">The card identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._StoreCardTransferPopup)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _StoreCardTransferPopup(int cardId)
        {
            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();
            if (cardId > 0)
            {
                StockCodeService objService = new StockCodeService();
                objSubStoreCardForm = objService.GetConsumablesDetailByCardID(cardId);
            }

            return PartialView(Pages.PartialViews.StoreCardTransferPopup, objSubStoreCardForm);
        }

        /// <summary>
        /// Gets the employees by l2 identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesByL2ID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetEmployeesByL2ID([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();
            string cityId = string.Empty;
            cityId = Common.GetConfigKeyValue("CityStore");

            var result = new DataSourceResult()
            {
                Data = objService.GetEmployeesByL2ID(ConvertTo.Integer(cityId), pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the card no by employee identifier.
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        /// <param name="isToCard">The is to card.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCardNobyEmployeeId)]
        public ActionResult GetCardNobyEmployeeId(int employeeID, int isToCard)
        {
            var result = StockCodeService.GetCardNobyEmployeeId(employeeID, isToCard);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the store card transfer.
        /// </summary>
        /// <param name="objSubStoreCardTransferDetail">The object sub store card transfer detail.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageStoreCardTransfer)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageStoreCardTransfer(SubStoreCardTransferDetail objSubStoreCardTransferDetail)
        {
            string validations = string.Empty;

            foreach (var lst in objSubStoreCardTransferDetail.lstSubStoreCardFormDetailForTransfer)
            {
                if (lst.QtyNeeded <= 0 || lst.QtyNeeded > lst.QtyIssue)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Store_MsgQtyNeedMustBegreaterThanZero }, JsonRequestBehavior.AllowGet);
                }
            }

            StockCodeService objService = new StockCodeService();
            bool flag = false;
            flag = objService.NewSubStoreCardTransfer(objSubStoreCardTransferDetail.lstSubStoreCardFormDetailForTransfer, objSubStoreCardTransferDetail.ToCardID, objSubStoreCardTransferDetail.FromCardID, objSubStoreCardTransferDetail.ToEmployeeID, objSubStoreCardTransferDetail.FromEmployeeID);
            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Store_MsgTransferSuccessfully });
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the sub store card detail.
        /// </summary>
        /// <param name="cardIssueId">The card issue identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSubStoreCardDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSubStoreCardDetail(int cardIssueId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<SubStoreCardFormDetail>(cardIssueId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion
    }
}