﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Asset Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Add/Edit Asset"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetList)]
        public ActionResult AssetList()
        {
            if (ProjectSession.PermissionAccess.Assets_Assets_Allowaccess && ProjectSession.PermissionAccess.Assets__ShowHideModule)
            {
                return View(Views.AssetList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "asc";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Asset objAsset = new Asset();
            SearchFieldService obj = new SearchFieldService();

            /*(Start)Added By Pratik Telaviya on 24-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            if (!ProjectSession.IsCentral)
            {
                whereClause += " and location.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and Assets.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            /*(End)Added By Pratik Telaviya on 24-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objAsset, SystemEnum.Pages.AssetList.GetHashCode(), ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            ////return Json(result,JsonRequestBehavior = JsonRequestBehavior.AllowGet,max)
            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        /// <summary>
        /// Gets the asset by identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="fromPageID">From page identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetByID)]
        [HttpGet]
        public ActionResult GetAssetByID(int assetID, int fromPageID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Assets_Assets_Allowaccess && ProjectSession.PermissionAccess.Assets__ShowHideModule)
            {
                Asset objAsset = new Asset();
                employees objemployees = new employees();
                if (assetID > 0)
                {
                    objAsset = AssetService.GetAssetDetailByAssetID(assetID);
                }
                else
                {
                    using (DapperContext context = new DapperContext())
                    {
                        objemployees = context.SelectObject<employees>(ProjectSession.EmployeeID);
                        objAsset.Central = Convert.ToInt32(objemployees.Central);
                        objAsset.L2ID = Convert.ToInt32(objemployees.L2ID);
                        if (objAsset.L2ID > 0)
                        {
                            L2 objL2 = new L2();
                            objL2.L2ID = objAsset.L2ID;
                            objL2 = context.SearchAllByID<L2>(objL2, objL2.L2ID).FirstOrDefault();
                            objAsset.L1ID = objL2.L1ID;

                            if (objL2.SectorID > 0)
                            {
                                Sector objSector = new Sector();
                                objSector.SectorID = Convert.ToInt32(objL2.SectorID);
                                objSector = context.SearchAllByID<Sector>(objSector, objSector.SectorID).FirstOrDefault();
                                objAsset.SectorCode = objSector.SectorCode;
                                objAsset.SectorName = objSector.SectorName;
                                objAsset.SectorAltName = objSector.SectorAltname;
                            }
                        }

                        objAsset.Warranty_ContractExpiry = DateTime.Now;
                        objAsset.DateAcquired = DateTime.Now;
                    }
                }

                if (objAsset == null)
                {
                    objAsset = new Asset();
                }

                objAsset.fromPageID = fromPageID;
                return View(Views.AssetDetail, objAsset);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the asset detail by asset identifier.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetDetailByAssetID)]
        public JsonResult GetAssetDetailByAssetID(int assetID)
        {
            Asset objAsset = new Asset();
            objAsset = AssetService.GetAssetDetailByAssetID(assetID);
            return Json(objAsset, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset.
        /// </summary>
        /// <param name="objAsset">The object asset.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageAsset(Asset objAsset)
        {
            string validations = string.Empty;
            int newAssetID = objAsset.AssetID;
            objAsset.CriticalityID = objAsset.Id;
            try
            {
                if (objAsset.AssetID > 0)
                {
                    if (objAsset.L2ID > 0)
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            int? status = context.SelectObject<L2>(objAsset.L2ID).Status;
                            if (status != 1)
                            {
                                ViewBag.Message = ProjectSession.Resources.message.Asset_MsgSiteInactive;
                                ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                                return View(Views.AssetDetail, objAsset);
                            }
                        }
                    }

                    objAsset.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objAsset.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objAsset.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objAsset.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext("AssetNumber"))
                    {
                        newAssetID = context.Save(objAsset);
                    }

                    if (objAsset.AssetID == 0)
                    {
                        AssetsTransfer objAssetsTransfer = new AssetsTransfer();
                        objAssetsTransfer.AssetID = newAssetID;
                        objAssetsTransfer.L2ID = objAsset.L2ID;
                        objAssetsTransfer.LocationID = objAsset.LocationID;
                        objAssetsTransfer.Remarks = "New Registered Asset";
                        objAssetsTransfer.TransferDate = DateTime.Now;
                        objAssetsTransfer.CreatedDate = DateTime.Now;
                        objAssetsTransfer.CreatedBy = ProjectSession.EmployeeID.ToString();

                        using (DapperContext context = new DapperContext())
                        {
                            int newAssetsTransferID = context.Save(objAssetsTransfer);
                        }
                    }
                    else if (objAsset.AssetID > 0 && objAsset.AssetStatusID == 5)
                    {
                        ////5: Disposed
                        AssetService.UpdateAssetDetail(objAsset.AssetID);
                    }

                    if (newAssetID > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        if (objAsset.AssetID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Asset_MsgAssetNoAlreadyExists;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                            return RedirectToAction(Actions.GetAssetByID, Pages.Controllers.Transaction, new { assetID = objAsset.AssetID, fromPageID = SystemEnum.Pages.AssetList.GetHashCode() });
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.Asset_MsgAssetNoAlreadyExists;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                            return View(Views.AssetDetail, objAsset);
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.AssetDetail, objAsset);
                }

                return RedirectToAction(Actions.GetAssetByID, Pages.Controllers.Transaction, new { assetID = newAssetID, fromPageID = SystemEnum.Pages.AssetList.GetHashCode() });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the current value.
        /// </summary>
        /// <param name="purchasePrice">The purchase price.</param>
        /// <param name="estLife">The estimate life.</param>
        /// <param name="dateCommissioned">The date commissioned.</param>
        /// <returns></returns>
        public JsonResult GetCurrentValue(decimal? purchasePrice, decimal? estLife, DateTime? dateCommissioned)
        {
            decimal purchasePriceTemp = 0;
            DateTime dateCommissionedTemp = DateTime.Now;
            decimal estimatedLifeTemp = 0;
            decimal ageOfAsset = 0;
            decimal currentValue = 0;

            if (string.IsNullOrEmpty(Convert.ToString(purchasePrice)))
            {
                purchasePriceTemp = 0;
            }
            else
            {
                purchasePriceTemp = Convert.ToDecimal(purchasePrice);
            }

            if (dateCommissioned == DateTime.MinValue)
            {
                dateCommissionedTemp = DateTime.Today;
            }
            else
            {
                dateCommissionedTemp = Convert.ToDateTime(dateCommissioned);
            }

            if (string.IsNullOrEmpty(Convert.ToString(estLife)))
            {
                estimatedLifeTemp = 0;
            }
            else
            {
                estimatedLifeTemp = Convert.ToDecimal(estLife);
            }

            double daysDifference = Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Day, dateCommissionedTemp, DateTime.Today);
            ageOfAsset = Convert.ToDecimal(Math.Round(daysDifference / 365, 4));

            if (estimatedLifeTemp == 0)
            {
                currentValue = 0;
            }
            else
            {
                currentValue = purchasePriceTemp - (purchasePriceTemp * (ageOfAsset / estimatedLifeTemp));
            }

            if (currentValue <= 0)
            {
                currentValue = 1;
            }

            string strCurrentValue = string.Format("{0:0,0.00}", Math.Round(currentValue, 2));
            return Json(strCurrentValue);
        }

        /// <summary>
        /// Deletes the asset.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAsset(int assetID)
        {
            try
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                ////AssetService.CheckAssetChildRefExist(assetID);
                ////AssetService.DeleteAssets(assetID);
                ////return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// _s the get job order by location identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationid">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetsByLocationID([DataSourceRequest]DataSourceRequest request, int locationid)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetService obj = new AssetService();

            var result = new DataSourceResult()
            {
                Data = obj.GetAssetsByLocationID(locationid, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }
        #endregion

        #region "BoM List"
        /// <summary>
        /// Saves the bo m.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="partsListID">The parts list identifier.</param>
        /// <returns></returns>
        public ActionResult SaveBoM(int assetID, int? partsListID)
        {
            partsListID = partsListID == 0 ? null : partsListID;
            string validations = string.Empty;
            Asset objAsset = new Asset();
            objAsset.AssetID = assetID;
            objAsset.PartsListID = partsListID;
            if (assetID > 0)
            {
                AssetService.UpdatePartsListIDByAssetID(assetID, partsListID);
                AssetService.ChangeAssetModificationDateTimeByAssetId(assetID, ProjectSession.EmployeeID.ToString());
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
        #endregion

        #region "RelationShip"

        /// <summary>
        /// Gets the relation ship.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetRelationShip)]
        public ActionResult _GetRelationShip(int assetID)
        {
            try
            {
                Asset objAsset = new Asset();
                CustomAssetRelationship objAssetParent = new CustomAssetRelationship();
                CustomAssetRelationship obj = new CustomAssetRelationship();

                ////Get Current Asset Info
                using (ServiceContext context = new ServiceContext())
                {
                    objAsset = context.SelectObject<Asset>(assetID);
                }

                obj.AssetID = objAsset.AssetID;
                obj.AssetNumber = objAsset.AssetNumber;
                obj.AssetAltDescription = objAsset.AssetAltDescription;
                obj.AssetDescription = objAsset.AssetDescription;
                obj.LocationID = Convert.ToInt32(objAsset.LocationID);

                ////Get ParentInfo
                objAssetParent = AssetService.GetParentAssetByAssetId(assetID);
                if (objAssetParent != null)
                {
                    obj.ParentAssetID = objAssetParent.ParentAssetID;
                    obj.ParentAssetNumber = objAssetParent.ParentAssetNumber;
                    obj.ParentAssetDescription = objAssetParent.ParentAssetDescription;
                    obj.ParentAssetAltDescription = objAssetParent.ParentAssetAltDescription;
                    obj.ParentAltAssetStatusDesc = objAssetParent.ParentAltAssetStatusDesc;
                    obj.ParentAssetStatusDesc = objAssetParent.ParentAssetStatusDesc;
                }

                return PartialView(PartialViews.AssetRelationship, obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetsByLocation([DataSourceRequest]DataSourceRequest request, int locationID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string strWhere = " and ass.locationId = " + locationID + " and ass.AssetId not in (select ChildId from assetRelationship) ";
            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            var result = new DataSourceResult()
            {
                Data = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result);
        }

        /// <summary>
        /// Gets the asset childs.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetChilds)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetChilds([DataSourceRequest]DataSourceRequest request, int? assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();
            var result = new DataSourceResult()
            {
                Data = obj.GetAssetChilds(Convert.ToInt32(assetID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result);
        }

        /// <summary>
        /// Deletes the child asset.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="childID">The child identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChildAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChildAsset(int assetID, int childID)
        {
            try
            {
                AssetService.DeleteChildAsset(assetID, childID);
                AssetService.ChangeAssetModificationDateTimeByAssetId(assetID, ProjectSession.EmployeeID.ToString());
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Adds the child asset.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="childID">The child identifier.</param>
        /// <param name="parentID">The parent identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.AddChildAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddChildAsset(int assetID, int childID, int parentID)
        {
            try
            {
                if (assetID == childID)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgItemParentAlreadyExists }, JsonRequestBehavior.AllowGet);
                }
                else if (childID == parentID)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgItemParentAlreadyExists }, JsonRequestBehavior.AllowGet);
                }
                else if (AssetService.RecursiveParentChildExists(assetID, childID) == false)
                {
                    ////GetCheckChildAssetIsParent
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgCurrentAssetAlreadyParentChild }, JsonRequestBehavior.AllowGet);
                }
                else if (AssetService.CheckChildAlreadyExists(childID) == false)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgCurrentAssetAlreadyChild }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    AssetService.AddAssetChild(assetID, childID);
                    AssetService.ChangeAssetModificationDateTimeByAssetId(assetID, ProjectSession.EmployeeID.ToString());
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Job Order history"

        /// <summary>
        /// _s the get job order history.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetJobOrderHistory)]
        public ActionResult _GetJobOrderHistory(int assetID)
        {
            Asset objAsset = new Asset();
            using (ServiceContext context = new ServiceContext())
            {
                objAsset = context.SelectObject<Asset>(assetID);
            }

            return PartialView(PartialViews.JobOrderHistory, objAsset);
        }

        #endregion

        #region "PM Meter"

        /// <summary>
        /// _s the get pm meter detail.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetPmMeterDetail)]
        public ActionResult _GetPmMeterDetail(int assetID)
        {
            Asset objAsset = new Asset();
            using (ServiceContext context = new ServiceContext())
            {
                objAsset = context.SelectObject<Asset>(assetID);
            }

            return PartialView(PartialViews.PMMeter, objAsset);
        }

        #endregion

        #region "Asset Transfer"
        /// <summary>
        /// _s the get asset transfer detail.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetAssetTransferDetail)]
        public ActionResult _GetAssetTransferDetail(int assetID)
        {
            Asset objAsset = new Asset();
            objAsset = AssetService.GetAssetDetail(assetID);
            return PartialView("_AssetTransfer", objAsset);
        }

        /// <summary>
        /// Gets the assets by location and asset identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationAndAssetID)]
        public ActionResult GetAssetsByLocationAndAssetID([DataSourceRequest]DataSourceRequest request, int? locationID, int? assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            string strWhere = " and ass.locationId = " + locationID + " and ass.assetId not in (select ChildId from assetRelationShip where assetId =  " + assetID + " ) and ass.assetId != " + assetID;

            var result = new DataSourceResult()
            {
                Data = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result);
        }

        /// <summary>
        /// Gets the asset transfer list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetTransferList)]
        public ActionResult GetAssetTransferList([DataSourceRequest]DataSourceRequest request, int? assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "TransferDate";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            AssetService obj = new AssetService();

            var result = new DataSourceResult()
               {
                   Data = obj.GetAssetTransferList(Convert.ToInt32(assetID), pageNumber, sortExpression, sortDirection, request: request),
                   Total = obj.PagingInformation.TotalRecords
               };

            return Json(result);
        }

        /// <summary>
        /// Manages the asset Transfer.
        /// </summary>
        /// <param name="objAssetsTransfer">The object assets Transfer.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetTransfer)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManageAssetTransfer(AssetsTransfer objAssetsTransfer)
        {
            try
            {
                AssetService obj = new AssetService();
                string outstandingWOs = obj.checkForOutstandingWOs(Convert.ToInt32(objAssetsTransfer.AssetID));
                if (!string.IsNullOrEmpty(outstandingWOs))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgOutStandingJO1 + "<br/><br/>" + ProjectSession.Resources.message.Asset_MsgOutStandingJO2 + "<br/>" + outstandingWOs }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string schedulePM = obj.CheckForPMScheduleExists(Convert.ToInt32(objAssetsTransfer.AssetID));
                    if (!string.IsNullOrEmpty(schedulePM))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgPMNos1 + "<br/>" + ProjectSession.Resources.message.PM_MsgPMNos + "<br/>" + schedulePM }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objAssetsTransfer.CreatedBy = ProjectSession.EmployeeID.ToString();
                        objAssetsTransfer.CreatedDate = DateTime.Now;
                        ////using (DapperContext context = new DapperContext())
                        ////{
                        ////    int transferId = context.Save(objAssetsTransfer);
                        ////}

                        Asset objCurrentAsset = new Asset();
                        objCurrentAsset.LocationID = objAssetsTransfer.LocationID;
                        objCurrentAsset.AssetID = Convert.ToInt32(objAssetsTransfer.AssetID);

                        obj.SaveAssetsByLocationId(objCurrentAsset);
                        obj.NewAssetsTransferForNLevel(objAssetsTransfer);
                        obj.RecursiveLocationChangeForChildAsset(objAssetsTransfer, objCurrentAsset, true);
                        if (objAssetsTransfer.ParentAssetID > 0)
                        {
                            int result = obj.DeleteChildAssetByChildID(Convert.ToInt32(objAssetsTransfer.AssetID));
                            if (result != -1)
                            {
                                ////NewChildAssetRelationShipTransfer                         
                                AssetService.AddAssetChild(Convert.ToInt32(objAssetsTransfer.ParentAssetID), Convert.ToInt32(objAssetsTransfer.AssetID));
                            }
                        }
                        else
                        {
                            obj.DeleteChildAssetByChildID(Convert.ToInt32(objAssetsTransfer.AssetID));
                        }

                        if (objCurrentAsset.AssetID > 0)
                        {
                            AssetService.ChangeAssetModificationDateTimeByAssetId(objCurrentAsset.AssetID, ProjectSession.EmployeeID.ToString());
                        }

                        if (objAssetsTransfer.AssetID > 0)
                        {
                            AssetService.ChangeAssetModificationDateTimeByAssetId(Convert.ToInt32(objAssetsTransfer.AssetID), ProjectSession.EmployeeID.ToString());
                        }



                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Edits the asset child.
        /// </summary>
        /// <param name="parentAssetID">The parent asset identifier.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        public JsonResult EditAssetChild(int parentAssetID, int assetID)
        {
            if (AssetService.CheckChildAlreadyExists(parentAssetID) == false)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgCurrentAssetAlreadyChild }, JsonRequestBehavior.AllowGet);
            }
            else if (AssetService.RecursiveParentChildExists(assetID, parentAssetID) == false)
            {
                ////GetCheckChildAssetIsParent
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgCurrentAssetAlreadyParentChild }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), string.Empty });
            }
        }

        #endregion

        #region "Document Tab"

        /// <summary>
        /// _s the get asset documents.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetAssetDocuments)]
        public ActionResult _GetAssetDocuments(int assetID)
        {
            ExtAssetFile objExtAssetFile = new ExtAssetFile();
            Asset objAsset = new Asset();
            using (ServiceContext context = new ServiceContext())
            {
                objAsset = context.SelectObject<Asset>(assetID);
            }

            objExtAssetFile.FkId = Convert.ToString(objAsset.AssetID);
            objExtAssetFile.AssetNumber = objAsset.AssetNumber;
            objExtAssetFile.AssetDescription = objAsset.AssetDescription;
            objExtAssetFile.AssetAltDescription = objAsset.AssetAltDescription;
            objExtAssetFile.ModuleType = SystemEnum.DocumentModuleType.A.ToString();

            if (!ProjectSession.PermissionAccess.Assets_Assets_Deleterecords)
            {
                objExtAssetFile.IsDeleteButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Assets_Assets_Createnewrecords)
            {
                objExtAssetFile.IsSaveButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Assets_Assets_Editrecords)
            {
                objExtAssetFile.IsEditButtonEnable = false;
            }

            return PartialView(PartialViews._DocumentList, objExtAssetFile);
        }

        /// <summary>
        /// Gets the document list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="foreignKeyID">The foreign key identifier.</param>
        /// <param name="moduleType">Type of the module.</param>
        /// <returns></returns>
        [ActionName(Actions.GetDocumentList)]
        public ActionResult GetDocumentList([DataSourceRequest]DataSourceRequest request, string foreignKeyID, string moduleType)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "FileName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            DocumentService obj = new DocumentService();

            var result = new DataSourceResult()
            {
                Data = obj.GetDocumentList(foreignKeyID, moduleType, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the document by identifier.
        /// </summary>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetDocumentByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetDocumentByID(int autoID)
        {
            ExtAssetFile objExtAssetFile = new ExtAssetFile();
            if (autoID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objExtAssetFile = context.SelectObject<ExtAssetFile>(autoID);
                }
            }

            return Json(objExtAssetFile, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Downloads the attachment.
        /// </summary>
        /// <param name="autoID">The attachment identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">The file could not be found.</exception>
        [ActionName(Actions.DownloadAttachment)]
        public FileResult DownloadAttachment(int? autoID)
        {
            try
            {
                if (autoID > 0)
                {
                    ExtAssetFile objExtAssetFile = new ExtAssetFile();
                    using (DapperContext objContext = new DapperContext())
                    {
                        objExtAssetFile = objContext.SearchAllByID<ExtAssetFile>(objExtAssetFile, Convert.ToInt32(autoID)).FirstOrDefault();
                    }

                    string fileName = objExtAssetFile.FileName;
                    string downloadName = objExtAssetFile.FileName;
                    string filePath = ProjectConfiguration.UploadPath + objExtAssetFile.FileLink;
                    string extension = Path.GetExtension(filePath);
                    string contentType = "application/" + extension;
                    if (System.IO.File.Exists(filePath))
                    {
                        return File(filePath, contentType, downloadName);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        /// <summary>
        /// Saves the document.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.SaveDocument)]
        public JsonResult SaveDocument(ExtAssetFile obj)
        {
            HttpPostedFileBase file;
            string fileName = string.Empty;
            if (obj.AutoId == 0 && Request.Files.Count == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSelectFileToUpload }, JsonRequestBehavior.AllowGet);
            }
            else if (Request.Files.Count > 0)
            {
                file = Request.Files[0];
                fileName = Path.GetFileName(file.FileName);

                if (!("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(fileName) + ","))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed }, JsonRequestBehavior.AllowGet);
                }

                if (((file.ContentLength / 1024) / 1024) > ProjectSession.AllowedMaxFilesize)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileSizeNotAllowed + ProjectSession.AllowedMaxFilesize + "MB." }, JsonRequestBehavior.AllowGet);
                }

                string subPath = string.Empty;
                string filePath = string.Empty;

                using (DapperContext context = new DapperContext())
                {
                    ExtAssetFile objExtAssetFile = new ExtAssetFile();
                    objExtAssetFile.FkId = obj.FkId;
                    objExtAssetFile.FileName = fileName;
                    objExtAssetFile.ModuleType = obj.ModuleType;
                    List<ExtAssetFile> lst = context.Search<ExtAssetFile>(objExtAssetFile).ToList();
                    ////List<ExtAssetFile> lst = context.SearchAll(objExtAssetFile).Where(x => x.FkId == obj.FkId && x.AutoId != obj.AutoId && x.FileName == fileName && x.ModuleType == obj.ModuleType).ToList();
                    if (lst != null && lst.Count > 0)
                    {
                        lst = lst.Where(x => x.AutoId != obj.AutoId).ToList();
                        if (lst != null && lst.Count > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileExists }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                if (obj.ModuleType == SystemEnum.DocumentModuleType.A.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathAsset + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.R.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathjobRequest + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.J.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathJobOrder + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.C.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathConsumables + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.S.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathItems + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.PR.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathPurchaseRequest + obj.FkId;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.PO.ToString())
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathPurchaseOrder + obj.FkId;
                }
                ////else if....Write Other Modules cases

                bool folderExists = Directory.Exists(subPath);
                if (!folderExists)
                {
                    Directory.CreateDirectory(subPath);
                }

                filePath = subPath + "\\" + fileName;
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                file.SaveAs(filePath);

                if (obj.AutoId > 0)
                {
                    ////Delete Old file
                    string oldFileLink = obj.FileLink;
                    if (System.IO.File.Exists(ProjectConfiguration.UploadPath + oldFileLink))
                    {
                        System.IO.File.Delete(ProjectConfiguration.UploadPath + oldFileLink);
                    }

                    obj.ModifiedBy = ProjectSession.EmployeeID;
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj.CreatedBy = ProjectSession.EmployeeID;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifiedBy = ProjectSession.EmployeeID;
                    obj.ModifiedDate = DateTime.Now;
                }

                if (obj.ModuleType == SystemEnum.DocumentModuleType.A.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathAsset + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.R.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathjobRequest + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.J.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathJobOrder + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.C.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathConsumables + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.S.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathItems + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.PR.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathPurchaseRequest + obj.FkId + "\\" + fileName;
                }
                else if (obj.ModuleType == SystemEnum.DocumentModuleType.PO.ToString())
                {
                    obj.FileLink = ProjectConfiguration.UploadPathPurchaseOrder + obj.FkId + "\\" + fileName;
                }

                ////else if....Write Other Modules cases
                obj.FileName = fileName;
            }

            using (DapperContext context = new DapperContext())
            {
                int newAutoID = context.Save(obj);
                if (newAutoID > 0)
                {
                    if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.J.ToString())
                    {
                        JobOrderService objService = new JobOrderService();
                        objService.ChangeJobOrderModificationDateTimeByWorkorderNo(obj.FkId, ProjectSession.EmployeeID.ToString());
                    }
                    else if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.R.ToString())
                    {
                        JobRequestService objRService = new JobRequestService();
                        objRService.ChangeJobRequestModificationDateTimeByWorkRequestNo(obj.FkId, ProjectSession.EmployeeID.ToString());
                    }
                    else if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.A.ToString())
                    {
                        if (Convert.ToInt32(obj.FkId) > 0)
                        {
                            AssetService.ChangeAssetModificationDateTimeByAssetId(Convert.ToInt32(obj.FkId), ProjectSession.EmployeeID.ToString());
                        }
                    }
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteDocument)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteDocument(int autoID)
        {
            using (DapperContext context = new DapperContext())
            {
                ExtAssetFile obj = new ExtAssetFile();
                obj = context.SelectObject<ExtAssetFile>(autoID);

                int returnValue = context.Delete<ExtAssetFile>(autoID, true);

                if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.J.ToString())
                {
                    JobOrderService objService = new JobOrderService();
                    objService.ChangeJobOrderModificationDateTimeByWorkorderNo(obj.FkId, ProjectSession.EmployeeID.ToString());
                }
                else if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.R.ToString())
                {
                    JobRequestService objRService = new JobRequestService();
                    objRService.ChangeJobRequestModificationDateTimeByWorkRequestNo(obj.FkId, ProjectSession.EmployeeID.ToString());
                }
                else if (obj.ModuleType.Trim() == SystemEnum.DocumentModuleType.A.ToString())
                {
                    if (Convert.ToInt32(obj.FkId) > 0)
                    {
                        AssetService.ChangeAssetModificationDateTimeByAssetId(Convert.ToInt32(obj.FkId), ProjectSession.EmployeeID.ToString());
                    }
                }

                if (returnValue == 0)
                {
                    string filePath = ProjectConfiguration.UploadPath + obj.FileLink;
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgFileDelete }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region "Specification"

        /// <summary>
        /// _s the asset specification.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._AssetSpecification)]
        public ActionResult _AssetSpecification(int assetID)
        {
            Asset objAsset = new Asset();
            objAsset = AssetService.GetAssetDetail(assetID);
            return PartialView("_AssetSpecifiaction", objAsset);
        }

        /// <summary>
        /// Gets the asset specification list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetSpecificationList)]
        public ActionResult GetAssetSpecificationList([DataSourceRequest]DataSourceRequest request, int? assetID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetSpecID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            AssetService obj = new AssetService();

            var result = new DataSourceResult()
               {
                   Data = obj.GetAssetSpecificationByAssetID(Convert.ToInt32(assetID), pageNumber, sortExpression, sortDirection, request: request),
                   Total = obj.PagingInformation.TotalRecords
               };

            return Json(result);
        }

        /// <summary>
        /// Gets the asset specification by identifier.
        /// </summary>
        /// <param name="assetSpecID">The asset spec identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetSpecificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetSpecificationByID(int assetSpecID)
        {
            AssetSpecification obj = new AssetSpecification();
            if (assetSpecID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    obj = context.SelectObject<AssetSpecification>(assetSpecID);
                }
            }

            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset specification.
        /// </summary>
        /// <param name="objAssetSpecification">The object asset specification.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetSpecification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetSpecification(AssetSpecification objAssetSpecification)
        {
            string validations = string.Empty;

            if (objAssetSpecification.AssetSpecID > 0)
            {
                objAssetSpecification.ModifiedBy = ProjectSession.EmployeeID;
                objAssetSpecification.ModifiedDate = DateTime.Now;
            }
            else
            {
                objAssetSpecification.CreatedBy = ProjectSession.EmployeeID;
                objAssetSpecification.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    int count = AssetService.GeAssetSpecificationCount(Convert.ToInt32(objAssetSpecification.AssetID), Convert.ToInt32(objAssetSpecification.SpecID), Convert.ToInt32(objAssetSpecification.AssetSpecID));
                    if (count > 0)
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Specifications_MsgSpecificationAlreadyExists });
                    }
                    else
                    {
                        if (context.Save(objAssetSpecification) > 0)
                        {
                            if (Convert.ToInt32(objAssetSpecification.AssetID) > 0)
                            {
                                AssetService.ChangeAssetModificationDateTimeByAssetId(Convert.ToInt32(objAssetSpecification.AssetID), ProjectSession.EmployeeID.ToString());
                            }
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Specifications_MsgSpecificationAlreadyExists });
                        }
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset specification.
        /// </summary>
        /// <param name="assetSpecID">The asset spec identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetSpecification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetSpecification(int assetSpecID)
        {
            using (DapperContext context = new DapperContext())
            {
                AssetSpecification obj = new AssetSpecification();
                if (assetSpecID > 0)
                {
                    obj = context.SelectObject<AssetSpecification>(assetSpecID);
                }

                int returnValue = context.Delete<AssetSpecification>(assetSpecID, true);
                if (returnValue == 0)
                {
                    if (Convert.ToInt32(obj.AssetID) > 0)
                    {
                        AssetService.ChangeAssetModificationDateTimeByAssetId(Convert.ToInt32(obj.AssetID), ProjectSession.EmployeeID.ToString());
                    }
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
    }
}