﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;
using Kendo.Mvc.UI;
using Newtonsoft.Json;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Schedule By Asset Sub Type Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "PM Schedule By Asset Sub Type"

        /// <summary>
        /// Pm the type of the schedule list by asset sub.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMScheduleListByAssetSubType)]
        public ActionResult PMScheduleListByAssetSubType()
        {
            if (ProjectSession.PermissionAccess.Preventive_TimeSchedules_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                return View(Views.PMScheduleByAsset);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the pm schedules.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMSchedulesByAssetSubType)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPMSchedulesByAssetSubType([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PMGroupNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            whereClause = "  and pmschedule.IsCleaningModule=0 AND pmschedule.PMGroupNo IS NOT NULL  ";

            if (!ProjectSession.IsCentral)
            {
                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                whereClause += " and pmschedule.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and pmschedule.PhyLocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }

                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                whereClause += " and pmschedule.MaintSubDeptID in (" + ProjectConfiguration.SUBDEPTIDLISTPERMISSIONWISE + " ) ";
            }

            PmSchedule objPmSchedule = new PmSchedule();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPmSchedule, SystemEnum.Pages.PMScheduleByAsset.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm asset schedule by identifier.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMAssetScheduleByID)]
        [HttpGet]
        public ActionResult GetPMAssetScheduleByID(string groupNo)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            PmSchedule objPmSchedule = new PmSchedule();
            objPmSchedule.PMGroupNo = groupNo;

            if (string.IsNullOrEmpty(groupNo))
            {
                objPmSchedule.PMID = 0;
                employees obj = new employees();
                Workpriority objWorkPriority = new Workpriority();
                Worktype objWorktype = new Worktype();
                Worktrade objWorktrade = new Worktrade();

                objWorkPriority.IsDefault = true;
                objWorktype.IsDefault = true;
                objWorktrade.IsDefault = true;

                using (ServiceContext context = new ServiceContext())
                {
                    obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                    objPmSchedule.MaintDivisionID = obj.MaintDivisionID;
                    objPmSchedule.MaintDeptID = obj.MaintDeptID;
                    objPmSchedule.MaintSubDeptID = obj.MaintSubDeptID;

                    objWorkPriority = context.Search<Workpriority>(objWorkPriority).FirstOrDefault();
                    if (objWorkPriority != null)
                    {
                        objPmSchedule.Workpriorityid = objWorkPriority.WorkPriorityID;
                    }

                    objPmSchedule.WorkTypeID = 2; ////Preventive

                    objWorktrade = context.Search<Worktrade>(objWorktrade).FirstOrDefault();
                    if (objWorktrade != null)
                    {
                        objPmSchedule.Worktradeid = objWorktrade.WorkTradeID;
                    }
                }

                objPmSchedule.Frequency = 1;
                objPmSchedule.PeriodDays = 1;
                objPmSchedule.TypePM = "1";
            }
            else
            {
                List<PmSchedule> lst = PMScheduleService.GetPmAssetScheduleByPMGroupNO(groupNo).ToList();
                objPmSchedule = lst.FirstOrDefault();
                objPmSchedule.MultiL2 = string.Join(",", lst.Select(Item => Item.L2ID).Distinct().ToList());
                objPmSchedule.MultiAssetIDs = string.Join(",", lst.Select(Item => Item.AssetID).Distinct().ToList());
                if (objPmSchedule.TypePMgenID == 1 || objPmSchedule.TypePMgenID == 2)
                {
                    objPmSchedule.Days = objPmSchedule.FreqUnits * objPmSchedule.Frequency;
                }
                else if (objPmSchedule.TypePMgenID == 3)
                {
                    ////3.Fixed Day and Week
                    objPmSchedule.Month = Convert.ToDateTime(objPmSchedule.TargetStartDate).Month;
                    objPmSchedule.Year = Convert.ToDateTime(objPmSchedule.TargetStartDate).Year;

                    objPmSchedule.Frequency_Week = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Week = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Week = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Week = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Week = objPmSchedule.NextDate;
                }
                else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                {
                    objPmSchedule.Frequency_Month = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Month = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Month = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Month = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Month = objPmSchedule.NextDate;
                }
            }

            return View(Views.PMScheduleByAssetDetail, objPmSchedule);
        }

        /// <summary>
        /// Gets the assets for pm asset schedule.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="assetCatID">The asset cat identifier.</param>
        /// <param name="assetSubCatID">The asset sub cat identifier.</param>
        /// <param name="groupNo">The group no.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="completedDate">The completed date.</param>
        /// <param name="nextDate">The next date.</param>
        /// <param name="timeType">Type of the pm.</param>
        /// <param name="workPeriodDays">The work period days.</param>
        /// <param name="days">The days.</param>
        /// <param name="boolMonthly">if set to <c>true</c> [boolean monthly].</param>
        /// <param name="frequencyUnitDayByMonths">The frequency unit day by months.</param>
        /// <param name="dbcall">if set to <c>true</c> [database call].</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsForPMAssetSchedule)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetsForPMAssetSchedule([DataSourceRequest]DataSourceRequest request, string cityID, int? assetCatID, int? assetSubCatID, string groupNo, DateTime? startDate, DateTime? completedDate, DateTime? nextDate, int? timeType, int? workPeriodDays, int? days, bool boolMonthly, int? frequencyUnitDayByMonths, bool dbcall = false)
        {
            if (string.IsNullOrEmpty(cityID))
            {
                cityID = "0";
            }

            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = 0;
            ProjectSession.PageSize = request.PageSize;
            string strWhere = string.Empty;
            if (assetCatID != null)
            {
                strWhere += " and ass.AssetCatID = " + assetCatID;
            }
            else
            {
                strWhere += " and ass.AssetCatID = " + 0;
            }

            if (assetSubCatID != null)
            {
                strWhere += " and ass.AssetSubCatID = " + assetSubCatID;
            }

            strWhere += " and L2.L2ID IN ( " + cityID + ") ";
            ////strWhere += " and L2.L2ID IN (1,4) ";

            AssetService obj = new AssetService();
            var result = new DataSourceResult();
            List<Asset> lstAsset = new List<Asset>();

            if (dbcall)
            {
                if (string.IsNullOrEmpty(groupNo))
                {
                    lstAsset = obj.GetAssetPageForPMAsset(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, groupNo, startDate, completedDate, nextDate, request: request).ToList();
                }
                else
                {
                    lstAsset = obj.GetAssetPageForPMAssetForEdit(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, groupNo, startDate, completedDate, nextDate, request: request).ToList();
                }

                result = new DataSourceResult()
                {
                    Data = lstAsset,
                    Total = obj.PagingInformation.TotalRecords
                };

                if (!string.IsNullOrEmpty(groupNo))
                {
                    using (ServiceContext context = new ServiceContext())
                    {
                        PmSchedule objPmSchedule = new PmSchedule();
                        objPmSchedule.PMGroupNo = groupNo;
                        List<PmSchedule> lstPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList();

                        if (lstPmSchedule != null && lstPmSchedule.Count > 0 && lstAsset.Count > 0)
                        {
                            for (int i = 0; i < lstAsset.Count(); i++)
                            {
                                for (int j = 0; j < lstPmSchedule.Count(); j++)
                                {
                                    if (lstAsset[i].AssetID == lstPmSchedule[j].AssetID)
                                    {
                                        lstAsset[i].IsInclude = true;
                                    }
                                }
                            }
                        }
                    }
                }

                TempData["AssetList"] = lstAsset;
            }
            else if (TempData["AssetList"] != null)
            {
                lstAsset = (List<Asset>)TempData["AssetList"];
                if (timeType == 1)
                {
                    for (int i = 0; i < lstAsset.Count(); i++)
                    {
                        lstAsset[i].TargetCompDate = Convert.ToDateTime(lstAsset[i].TargetStartDate).AddDays(Convert.ToDouble(workPeriodDays) - 1);
                        lstAsset[i].NextDate = Convert.ToDateTime(lstAsset[i].TargetStartDate).AddDays(Convert.ToDouble(days));
                    }
                }
                else if (timeType == 3)
                {
                    for (int i = 0; i < lstAsset.Count(); i++)
                    {
                        if (boolMonthly == true)
                        {
                            lstAsset[i].TargetCompDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", 1, lstAsset[i].TargetStartDate).AddDays(-1);
                        }
                        else
                        {
                            lstAsset[i].TargetCompDate = Convert.ToDateTime(lstAsset[i].TargetStartDate).AddDays(Convert.ToDouble(workPeriodDays) - 1);
                        }

                        lstAsset[i].NextDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", Convert.ToDouble(frequencyUnitDayByMonths), lstAsset[i].TargetStartDate);
                    }
                }

                result = new DataSourceResult()
                {
                    Data = lstAsset,
                    Total = 1
                };

                TempData["AssetList"] = lstAsset;
            }

            return Json(result);
        }

        /// <summary>
        /// Manages the pm schedule.
        /// </summary>
        /// <param name="objectCurrentPMSchedule">The object pm schedule.</param>        
        /// <returns></returns>
        [ActionName(Actions.ManagePMScheduleByAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePMScheduleByAsset(PmSchedule objectCurrentPMSchedule)
        {
            ////Remaining logic of last genrated date
            string validations = string.Empty;
            int newScheduleID = 0;
            List<PmSchedule> lstPmSchedule = new List<PmSchedule>();
            List<Asset> lstAssets = new List<Asset>();
            AssetService objAssetService = new AssetService();
            try
            {
                if (ModelState.IsValid)
                {
                    ////Get PMSchdule info
                    PmSchedule objSearch = new PmSchedule();
                    objSearch.PMGroupNo = objectCurrentPMSchedule.PMGroupNo;
                    using (DapperContext context = new DapperContext())
                    {
                        lstPmSchedule = context.Search<PmSchedule>(objSearch).ToList();
                    }

                    if (objectCurrentPMSchedule.PMID == 0 && lstPmSchedule.Count > 0)
                    {
                        ////TempData["Message"] = ProjectSession.Resources.message.PM_MsgPMNoAlreadyExists;
                        ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                        ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = string.Empty });

                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgPMNoAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }

                    ////Get Asset Info
                    List<Asset> lstAllAssets = new List<Asset>();
                    var microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    };

                    lstAllAssets = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Asset>>(Convert.ToString(objectCurrentPMSchedule.JsonlstItemModel), microsoftDateFormatSettings);
                    if (lstAllAssets != null && lstAllAssets.Count > 0)
                    {
                        lstAssets = lstAllAssets.Where(x => x.IsInclude == true).ToList();
                        ////lstAssets = objAssetService.GetAssetsInfo(objectCurrentPMSchedule.MultiAssetIDs).ToList();
                        if (lstAssets.Count > 0)
                        {
                            objectCurrentPMSchedule.MultiL2 = string.Join(",", lstAssets.Select(Item => Item.L2Code).Distinct().ToList());

                            for (int i = 0; i < lstAssets.Count; i++)
                            {
                                PMGenDate obj = new PMGenDate();
                                DateTime lastGenDate = DateTime.MinValue;

                                using (ServiceContext context = new ServiceContext())
                                {
                                    obj = context.SearchAll(obj).Where(x => x.L2ID == lstAssets[i].L2ID && x.IsCleaningModule == false && (x.MaintdivId == objectCurrentPMSchedule.MaintDivisionID || x.MaintdivId == 0) && (x.MaintdeptId == objectCurrentPMSchedule.MaintDeptID || x.MaintdeptId == 0) && (x.MainSubDeptId == objectCurrentPMSchedule.MaintSubDeptID || x.MainSubDeptId == 0)).OrderByDescending(x => x.ToDate).FirstOrDefault();
                                }

                                if (obj != null)
                                {
                                    lastGenDate = Convert.ToDateTime(obj.DateGenerate);
                                }

                                if (lstAssets[i].TargetStartDate <= lastGenDate)
                                {
                                    ////TempData["Message"] = ProjectSession.Resources.message.PM_MsgTargetStartDateGreater + " For Asset : " + lstAssets[i].AssetNumber + ";";
                                    ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                                    ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = objectCurrentPMSchedule.PMGroupNo });

                                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgTargetStartDateGreater + " For Asset : " + lstAssets[i].AssetNumber + ";" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                        else
                        {
                            ////TempData["Message"] = ProjectSession.Resources.message.PMSchedule_MsgSelectAssetReq;
                            ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                            ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = objectCurrentPMSchedule.PMGroupNo });

                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PMSchedule_MsgSelectAssetReq }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    if (lstPmSchedule.Count == 0)
                    {
                        ////Add Mode

                        ////1.Asset,2.location              
                        objectCurrentPMSchedule.TypePM = "1";

                        if (objectCurrentPMSchedule.TypePMgenID == 3)
                        {
                            objectCurrentPMSchedule.Frequency = objectCurrentPMSchedule.Frequency_Week;
                            objectCurrentPMSchedule.PeriodDays = objectCurrentPMSchedule.PeriodDays_Week;
                            ////objectCurrentPMSchedule.TargetStartDate = objectCurrentPMSchedule.TargetStartDate_Week;
                            ////objectCurrentPMSchedule.TargetCompDate = objectCurrentPMSchedule.TargetCompDate_Week;
                            ////objectCurrentPMSchedule.NextDate = objectCurrentPMSchedule.NextDate_Week;
                        }
                        else if (objectCurrentPMSchedule.TypePMgenID == 4 || objectCurrentPMSchedule.TypePMgenID == 5)
                        {
                            objectCurrentPMSchedule.Frequency = objectCurrentPMSchedule.Frequency_Month;
                            objectCurrentPMSchedule.PeriodDays = objectCurrentPMSchedule.PeriodDays_Month;
                            ////objectCurrentPMSchedule.TargetStartDate = objectCurrentPMSchedule.TargetStartDate_Month;
                            ////objectCurrentPMSchedule.TargetCompDate = objectCurrentPMSchedule.TargetCompDate_Month;
                            ////objectCurrentPMSchedule.NextDate = objectCurrentPMSchedule.NextDate_Month;
                        }

                        objectCurrentPMSchedule.PMID = 0;
                        objectCurrentPMSchedule.WorkTypeID = 2; ////Preventive
                        objectCurrentPMSchedule.PMActive = 1;
                        objectCurrentPMSchedule.PMCounter = 1;
                        objectCurrentPMSchedule.PMMultiple = 0;
                        objectCurrentPMSchedule.IsCleaningModule = false;
                        objectCurrentPMSchedule.CreatedBy = ProjectSession.EmployeeID.ToString();
                        objectCurrentPMSchedule.CreatedDate = DateTime.Now;
                    }
                    else
                    {
                        ////Edit Mode
                        objectCurrentPMSchedule.PMID = 0;
                        objectCurrentPMSchedule.AssetCategoryID = lstPmSchedule[0].AssetCategoryID;
                        objectCurrentPMSchedule.AssetSubCategoryID = lstPmSchedule[0].AssetSubCategoryID;
                        objectCurrentPMSchedule.WorkTypeID = lstPmSchedule[0].WorkTypeID;
                        ////objectCurrentPMSchedule.TargetStartDate = lstPmSchedule[0].TargetStartDate;
                        ////objectCurrentPMSchedule.TargetCompDate = lstPmSchedule[0].TargetCompDate;
                        ////objectCurrentPMSchedule.NextDate = lstPmSchedule[0].NextDate;
                        objectCurrentPMSchedule.ActualCompDate = lstPmSchedule[0].ActualCompDate;
                        objectCurrentPMSchedule.PMCounter = lstPmSchedule[0].PMCounter;
                        objectCurrentPMSchedule.IsCleaningModule = lstPmSchedule[0].IsCleaningModule;
                        objectCurrentPMSchedule.CreatedBy = lstPmSchedule[0].CreatedBy;
                        objectCurrentPMSchedule.CreatedDate = lstPmSchedule[0].CreatedDate;
                    }

                    ////Set Asset info to PMSchdule
                    if (lstAssets.Count > 0)
                    {
                        for (int i = 0; i < lstAssets.Count; i++)
                        {
                            objectCurrentPMSchedule.PMNo = lstAssets[i].AssetNumber;
                            objectCurrentPMSchedule.PMName = lstAssets[i].AssetNumber;
                            objectCurrentPMSchedule.AltPMName = lstAssets[i].AssetNumber;
                            objectCurrentPMSchedule.L2ID = lstAssets[i].L2ID;
                            objectCurrentPMSchedule.AssetID = lstAssets[i].AssetID;
                            objectCurrentPMSchedule.PhyLocationID = lstAssets[i].LocationID;
                            objectCurrentPMSchedule.TargetStartDate = lstAssets[i].TargetStartDate;
                            objectCurrentPMSchedule.TargetCompDate = lstAssets[i].TargetCompDate;
                            objectCurrentPMSchedule.NextDate = lstAssets[i].NextDate;

                            if (lstPmSchedule.Count > 0 && lstPmSchedule.Any(x => x.AssetID == lstAssets[i].AssetID))
                            {
                                PmSchedule objectTempPMSchedule = lstPmSchedule.Where(x => x.AssetID == lstAssets[i].AssetID).FirstOrDefault();
                                objectCurrentPMSchedule.PMID = objectTempPMSchedule.PMID;
                                objectCurrentPMSchedule.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                objectCurrentPMSchedule.ModifiedDate = DateTime.Now;
                            }
                            else
                            {
                                objectCurrentPMSchedule.PMID = 0;
                                objectCurrentPMSchedule.ModifiedBy = string.Empty;
                                objectCurrentPMSchedule.ModifiedDate = null;
                            }

                            using (DapperContext context = new DapperContext())
                            {
                                newScheduleID = context.Save(objectCurrentPMSchedule);
                            }

                            if (objectCurrentPMSchedule.PMID == 0)
                            {
                                if (lstPmSchedule.Count() > 0)
                                {
                                    int oldPMID = lstPmSchedule.FirstOrDefault().PMID;

                                    using (DapperContext context = new DapperContext())
                                    {
                                        ////Entry in MultiplePM for New Assets                                    
                                        if (PMScheduleService.GetCountOfMultiplePMByPMId(oldPMID) > 0)
                                        {
                                            PMScheduleService.ManageMultiplePMForAsset(oldPMID, newScheduleID);
                                        }

                                        ////Entry in PmScheduleAssignto for New Assets                                   
                                        if (PMScheduleService.GetCountOfPmScheduleAssigntoByPMId(oldPMID) > 0)
                                        {
                                            PMScheduleService.ManagePmScheduleAssigntoForAsset(oldPMID, newScheduleID);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    ////Delete Extra PMS
                    List<int> lstAssetIDs = lstAssets.Select(x => x.AssetID).ToList();
                    List<int> lstPMAssetIDs = lstPmSchedule.Select(x => x.AssetID.Value).ToList();
                    List<int> deletePMAssetIDs = lstPMAssetIDs.Except(lstAssetIDs).ToList();

                    if (deletePMAssetIDs.Count > 0)
                    {
                        for (int i = 0; i < deletePMAssetIDs.Count; i++)
                        {
                            PmSchedule objDeletePMS = lstPmSchedule.Where(x => x.AssetID == deletePMAssetIDs[i]).FirstOrDefault();
                            bool result = PMScheduleService.DeleteMultiplePM(objDeletePMS.PMID);
                            bool result1 = PMScheduleService.DeleteAllPmScheduleAssignto(objDeletePMS.PMID);

                            using (DapperContext context = new DapperContext())
                            {
                                context.Delete<PmSchedule>(objDeletePMS.PMID);
                            }
                        }
                    }

                    if (newScheduleID > 0)
                    {
                        ////TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        ////TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                        ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = objectCurrentPMSchedule.PMGroupNo });

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, objectCurrentPMSchedule.PMGroupNo }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ////TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                        ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = string.Empty });
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ////TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ////TempData["Message"] = validations;
                        ////TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') }, JsonRequestBehavior.AllowGet);
                    }

                    ////return View(Views.PMScheduleDetail, objPmSchedule);
                    ////return RedirectToAction(Actions.GetPMAssetScheduleByID, Pages.Controllers.Transaction, new { groupNo = string.Empty });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the temporary asset detail.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTempAssetDetail(int assetID, string kendogriddata)
        {
            List<Asset> lstAsset = new List<Asset>();
            lstAsset = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Asset>>(kendogriddata);
            Asset model = new Asset();
            if (assetID > 0)
            {
                model = lstAsset.FirstOrDefault(l => l.AssetID == assetID);
            }

            TempData["AssetList"] = lstAsset;
            return Json(model);
        }

        /// <summary>
        /// Manages the temporary asset detail.
        /// </summary>
        /// <param name="objModel">The object model.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageTempAssetDetail(Asset objModel, string kendoItemgriddata)
        {
            List<Asset> lstAsset = new List<Asset>();
            var microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    };
            lstAsset = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Asset>>(kendoItemgriddata, microsoftDateFormatSettings);

            Asset editAsset = lstAsset.FirstOrDefault(l => l.AssetID == objModel.AssetID);
            ////objModel.AssetID = editAsset.AssetID; // add for edit
            int editIndex = lstAsset.IndexOf(editAsset);

            editAsset.TargetStartDate = objModel.TargetStartDate;
            editAsset.TargetCompDate = objModel.TargetCompDate;
            editAsset.NextDate = objModel.NextDate;

            lstAsset.RemoveAt(editIndex);
            lstAsset.Insert(editIndex, editAsset);

            TempData["AssetList"] = lstAsset;
            return Json(lstAsset);
        }

        /// <summary>
        /// Updates the date.
        /// </summary>
        /// <param name="startMonth">The start month.</param>
        /// <param name="year">The year.</param>
        /// <param name="weekOfMonth">The week of month.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <param name="frequencyUnitsMonths">The frequency units months.</param>
        /// <param name="workPeriodDaysMonths">The work period days months.</param>
        /// <returns></returns>
        public JsonResult UpdateDateForAssetPage(int startMonth, int year, int weekOfMonth, int dayOfWeek, int frequencyUnitsMonths, int workPeriodDaysMonths)
        {
            try
            {
                zMonth = startMonth;
                zYear = year;
                int i = 1;
                DateTime textBoxTargetStartDateMonthFixedDay = DateTime.MinValue;
                DateTime textBoxNextStartDateFixedDay = DateTime.MinValue;
                DateTime textBoxTargetCompleteDateFixedDay = DateTime.MinValue;
                while (i < 3)
                {
                    switch (weekOfMonth)
                    {
                        case 1:
                            firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                            tempDay = firstDay;
                            GetPMDayStart(Microsoft.VisualBasic.DateAndTime.Weekday(firstDay), dayOfWeek);
                            break;

                        case 2:
                        case 3:
                        case 4:
                            firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                            tempDay = Microsoft.VisualBasic.DateAndTime.DateAdd("ww", weekOfMonth - 1, firstDay);
                            GetDayofPM(Microsoft.VisualBasic.DateAndTime.Weekday(tempDay), dayOfWeek);
                            break;

                        case 5:
                            lastDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth + 1, 0);
                            tempDay = lastDay;
                            GetPMDayLast(Microsoft.VisualBasic.DateAndTime.Weekday(lastDay), dayOfWeek);
                            break;
                    }

                    zMonth = zMonth + frequencyUnitsMonths;
                    if (zMonth > 12)
                    {
                        zYear = zYear + 1;
                        zMonth = zMonth - 12;
                    }

                    if (i == 1)
                    {
                        textBoxTargetStartDateMonthFixedDay = PMDate;
                    }
                    else
                    {
                        textBoxNextStartDateFixedDay = PMDate;
                    }

                    i = i + 1;
                }

                if (textBoxTargetStartDateMonthFixedDay != DateTime.MinValue)
                {
                    textBoxTargetCompleteDateFixedDay = textBoxTargetStartDateMonthFixedDay.AddDays(workPeriodDaysMonths - 1);
                }

                DateTime? textBoxTargetStartDateMonthFixedDayTemp;
                if (textBoxTargetStartDateMonthFixedDay == DateTime.MinValue)
                {
                    textBoxTargetStartDateMonthFixedDayTemp = null;
                }
                else
                {
                    textBoxTargetStartDateMonthFixedDayTemp = textBoxTargetStartDateMonthFixedDay;
                }

                DateTime? textBoxNextStartDateFixedDayTemp;
                if (textBoxNextStartDateFixedDay == DateTime.MinValue)
                {
                    textBoxNextStartDateFixedDayTemp = null;
                }
                else
                {
                    textBoxNextStartDateFixedDayTemp = textBoxNextStartDateFixedDay;
                }

                DateTime? textBoxTargetCompleteDateFixedDayTemp = DateTime.MinValue;
                if (textBoxTargetCompleteDateFixedDay == DateTime.MinValue)
                {
                    textBoxTargetCompleteDateFixedDayTemp = null;
                }
                else
                {
                    textBoxTargetCompleteDateFixedDayTemp = textBoxTargetCompleteDateFixedDay;
                }

                return Json(new object[] { "TextBoxTargetStartDateMonthFixedDay", textBoxTargetStartDateMonthFixedDayTemp, "TextBoxNextStartDateFixedDay", textBoxNextStartDateFixedDayTemp, "TextBoxTargetCompleteDateFixedDay", textBoxTargetCompleteDateFixedDayTemp });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Multiple checklist"

        /// <summary>
        /// _s the get multiple check list.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetMultipleCheckListForAsset)]
        public ActionResult _GetMultipleCheckListForAsset(string groupNo)
        {
            try
            {
                PmSchedule objPmSchedule = new PmSchedule();
                objPmSchedule.PMGroupNo = groupNo;
                MultiplePM objMultiplePM = new MultiplePM();

                using (ServiceContext context = new ServiceContext())
                {
                    objPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList().FirstOrDefault();
                    objMultiplePM.PMID = objPmSchedule.PMID;
                    objMultiplePM.PMGroupNo = objPmSchedule.PMGroupNo;
                    objMultiplePM.PMGroupName = objPmSchedule.PMGroupName;
                    objMultiplePM.AltPMGroupName = objPmSchedule.AltPMGroupName;
                    objMultiplePM.TypePMgenID = Convert.ToInt32(objPmSchedule.TypePMgenID);
                }

                return PartialView(PartialViews._PMByAssetMultipleCheckList, objMultiplePM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the multiple pm.
        /// </summary>
        /// <param name="objMultiplePM">The object multiple pm.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMultiplePMForAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageMultiplePMForAsset(MultiplePM objMultiplePM)
        {
            bool result = false;
            PMScheduleService objService = new PMScheduleService();
            List<MultiplePM> lstMultiplePM = new List<MultiplePM>();
            string validations = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    lstMultiplePM = objService.GetMultiPMList(objMultiplePM.PMID, 1, "ChecklistNo", "Ascending").ToList();
                    if (lstMultiplePM.Count > 0)
                    {
                        int max = lstMultiplePM.Select(x => x.MSeq).Max();
                        if (objMultiplePM.MSeq <= max)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgSeqNoGreaterthanLastSeqNo });
                        }
                    }

                    objMultiplePM.MPMCounter = 1;
                    objMultiplePM.MTaskSeq = lstMultiplePM.Count() + 2;
                    objMultiplePM.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objMultiplePM.CreatedDate = DateTime.Now;

                    PmSchedule obj = new PmSchedule();
                    obj.PMGroupNo = objMultiplePM.PMGroupNo;
                    List<PmSchedule> lstPmSchedule = new List<PmSchedule>();
                    using (DapperContext context = new DapperContext())
                    {
                        lstPmSchedule = context.Search<PmSchedule>(obj).ToList();
                    }

                    for (int i = 0; i < lstPmSchedule.Count; i++)
                    {
                        objMultiplePM.PMID = lstPmSchedule[i].PMID;
                        result = PMScheduleService.ManageMultiplePM(objMultiplePM);
                    }

                    if (result)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the multiple pm.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMultiplePMForAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMultiplePMForAsset(string groupNo)
        {
            bool result = false;
            List<PmSchedule> lstPmSchedule = PMScheduleService.GetPmAssetScheduleByPMGroupNO(groupNo).ToList();
            List<int> lstPMIDs = lstPmSchedule.Select(x => x.PMID).ToList();
            for (int i = 0; i < lstPMIDs.Count; i++)
            {
                result = PMScheduleService.DeleteMultiplePM(lstPMIDs[i]);
            }

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Assign To"

        /// <summary>
        /// _s the pm assign to.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._PMByAssetAssignTo)]
        public ActionResult _PMByAssetAssignTo(string groupNo)
        {
            try
            {
                PmSchedule objPmSchedule = new PmSchedule();
                objPmSchedule.PMGroupNo = groupNo;
                PmSchedule_AssignTo objPMAssignTo = new PmSchedule_AssignTo();

                using (ServiceContext context = new ServiceContext())
                {
                    objPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList().FirstOrDefault();
                    objPMAssignTo.PMID = objPmSchedule.PMID;
                    objPMAssignTo.PMGroupNo = objPmSchedule.PMGroupNo;
                    objPMAssignTo.PMGroupName = objPmSchedule.PMGroupName;
                    objPMAssignTo.AltPMGroupName = objPmSchedule.AltPMGroupName;
                }

                return PartialView(PartialViews._PMByAssetAssignTo, objPMAssignTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the pm assign to employee group.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <param name="groupID">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMAssignToEmployeeGroupForAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMAssignToEmployeeGroupForAsset(string groupNo, int groupID)
        {
            try
            {
                PmSchedule objPmSchedule = new PmSchedule();
                objPmSchedule.PMGroupNo = groupNo;
                List<PmSchedule> lstPmSchedule = new List<PmSchedule>();
                using (ServiceContext context = new ServiceContext())
                {
                    lstPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList();
                }

                for (int i = 0; i < lstPmSchedule.Count; i++)
                {
                    PMScheduleService.ManagePMAssignToEmployeeGroup(lstPmSchedule[i].PMID, groupID);
                }

                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the pm assign to employee.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <param name="groupNo">The group no.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMAssignToEmployeeForAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMAssignToEmployeeForAsset(int timeID, int employeeID, string groupNo)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    PmSchedule_AssignTo obj = new PmSchedule_AssignTo();
                    obj.PMID = timeID;
                    obj.EmployeeID = employeeID;
                    int count = context.Search<PmSchedule_AssignTo>(obj).ToList().Count();
                    if (count > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgEmployeeAlreadyExists });
                    }
                    else
                    {
                        PmSchedule objPmSchedule = new PmSchedule();
                        objPmSchedule.PMGroupNo = groupNo;
                        List<PmSchedule> lstPmSchedule = new List<PmSchedule>();
                        lstPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList();

                        for (int i = 0; i < lstPmSchedule.Count; i++)
                        {
                            PMScheduleService.ManagePMAssignToEmployee(lstPmSchedule[i].PMID, employeeID);
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the pm schedule assign to.
        /// </summary>
        /// <param name="groupNo">The group no.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePmScheduleAssigntoForAsset)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePmScheduleAssigntoForAsset(string groupNo, int employeeID)
        {
            bool result = false;
            PmSchedule objPmSchedule = new PmSchedule();
            objPmSchedule.PMGroupNo = groupNo;
            List<PmSchedule> lstPmSchedule = new List<PmSchedule>();
            using (ServiceContext context = new ServiceContext())
            {
                lstPmSchedule = context.Search<PmSchedule>(objPmSchedule).ToList();
            }

            for (int i = 0; i < lstPmSchedule.Count; i++)
            {
                result = PMScheduleService.DeletePmScheduleAssignto(lstPmSchedule[i].PMID, employeeID);
            }

            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}