﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Direct Issue Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        ///  Partial view Direct Issue
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetDirectIssue)]
        public ActionResult _PartialDirectIssue()
        {
            return View(PartialViews.DirectIssueList);
        }

        /// <summary>
        /// Gets the Direct Issue list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="workOrderNo">get list by workOrderNo.</param>
        /// <returns></returns>
        [ActionName(Actions.GetDirectIssueList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetDirectIssueList([DataSourceRequest]DataSourceRequest request, string workOrderNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "DirectID";
                sortDirection = "Ascending";
            }

            if (!string.IsNullOrEmpty(workOrderNo))
            {
                strWhere = "and direct.WONo = '" + Common.setQuote(workOrderNo) + "' ";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Direct directIssue = new Direct();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(directIssue, SystemEnum.Pages.DirectIssueList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Delete the DirectIssue.
        /// </summary>
        /// <param name="directID">The Direct Issue identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteDirectIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteDirectIssue(int directID)
        {

            Direct objDirect = new Direct();
            objDirect.DirectID = directID;
            using (ServiceContext objService = new ServiceContext())
            {
                objDirect = objService.Search<Direct>(objDirect).FirstOrDefault();
            }

            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Direct>(directID, true);
                if (returnValue == 0)
                {
                    JobOrderService objService = new JobOrderService();
                    objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objDirect.WONo, ProjectSession.EmployeeID.ToString());
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        ///  edit direct Issue.
        /// </summary>
        /// <param name="directId">get directId</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions.AddEditDirectIssue)]
        public PartialViewResult AddEditDirectIssue(int directId)
        {
            Direct objDirectIssue = new Direct();
            objDirectIssue.Qty = 0;
            objDirectIssue.Price = 0;
            return PartialView(Pages.PartialViews.AddEditDirectIssue, objDirectIssue);
        }

        /// <summary>
        /// Add new Direct Issue
        /// </summary>
        /// <param name="objDirect">add new direct issue</param>
        /// <returns></returns>
        [ActionName(Actions.AddNewDirectIssue)]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddNewDirectIssue(Direct objDirect)
        {
            objDirect.CreatedBy = ProjectSession.EmployeeID.ToString();
            objDirect.CreatedDate = DateTime.Now;

            try
            {
                if (objDirect.Qty <= 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.label.PMChecklist_MsgQuantityGreaterthanZero }, JsonRequestBehavior.AllowGet);
                }
                else if (objDirect.Price <= 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobOrder_MsgUnitPriceGreaterthanZero }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (objDirect.TotalPrice > 0)
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            context.Save(objDirect);
                            JobOrderService objService = new JobOrderService();
                            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objDirect.WONo, ProjectSession.EmployeeID.ToString());

                        }

                        return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else 
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), "Total price must be greater than zero." }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}