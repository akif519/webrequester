﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Section Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Section List.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SectionList)]
        public ActionResult SectionList()
        {
            if (ProjectSession.PermissionAccess.Locations_Locations_Allowaccess && ProjectSession.PermissionAccess.Locations__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                return View(Views.SectionList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Section list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectionList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSectionList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LocationNo";
                sortDirection = "asc";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Location objLocation = new Location();
            SearchFieldService obj = new SearchFieldService();

            /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and Location.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (Location.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + " )  OR (Location.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and Location.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objLocation, SystemEnum.Pages.Section.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        /// <summary>
        /// Gets the Section by identifier.
        /// </summary>
        /// <param name="locationid">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectionByID)]
        [HttpGet]
        public ActionResult GetSectionByID(int locationid)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Locations_Locations_Allowaccess && ProjectSession.PermissionAccess.Locations__ShowHideModule)
            {
                Location objLocation = new Location();
                bool returnValue = false;
                string strWhere = string.Empty;
                if (locationid > 0)
                {
                    strWhere = " and location.locationid = " + locationid;
                    string sortExpression = string.Empty;
                    string sortDirection = string.Empty;

                    LocationService objService = new LocationService();
                    objLocation = objService.GetLocationPage(strWhere, ProjectSession.EmployeeID, ProjectSession.IsCentral, 1, sortExpression, sortDirection).FirstOrDefault();

                    if (objLocation != null)
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            returnValue = objContext.HasReferencesRecords<Location>(objLocation, locationid);
                            objLocation.LocationHasReferences = returnValue;
                        }
                    }
                }
                else
                {
                    objLocation.LocationHasReferences = false;
                }

                if (objLocation != null)
                {
                    return View(Views.SectionDetail, objLocation);
                }
                else
                {
                    ProjectSession.ErrorMessage = ProjectSession.Resources.message.Location_MsgNoRightToLocation;
                    return RedirectToAction(Actions.DisplayError, Pages.Controllers.Account, new { area = string.Empty });
                }
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Manages the Location.
        /// </summary>
        /// <param name="objLocation">The object Location.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageSection(Location objLocation)
        {
            string validations = string.Empty;
            int newlocationID = objLocation.LocationID;
            bool isAddMode = false;

            try
            {
                if (objLocation.LocationID > 0)
                {
                    objLocation.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objLocation.ModifiedDate = DateTime.Now;
                }
                else
                {
                    isAddMode = true;
                    objLocation.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objLocation.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext("LocationNo"))
                    {
                        newlocationID = context.Save(objLocation);
                        if (newlocationID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.Location_MsgLocationNoAlreadyExists;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                            return View(Views.SectionDetail, objLocation);
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.SectionDetail, objLocation);
                }

                if (isAddMode && ProjectSession.IsCentral == false && ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Location_MsgNoRightToLocation;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    return RedirectToAction(Actions.SectionList, Pages.Controllers.Transaction, new { });
                }
                else
                {
                    return RedirectToAction(Actions.GetSectionByID, Pages.Controllers.Transaction, new { locationid = newlocationID });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the Location.
        /// </summary>
        /// <param name="locationid">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSection(int locationid)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Location>(locationid, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// The Move To JobOrderController Once it Unlocked By Pratik - PK
        #region "Job Order History Tab"
        /// <summary>
        /// _s the get job order by location identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationid">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderByLocationID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobOrderByLocationID([DataSourceRequest]DataSourceRequest request, int locationid)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkorderNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobOrderService obj = new JobOrderService();

            var result = new DataSourceResult()
            {
                Data = obj.GetJobOrderByLocationID(locationid, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion

        #region "Attachment Tab"

        /// <summary>
        /// _s the get section documents.
        /// </summary>
        /// <param name="locationId">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._GetSectionDocuments)]
        public ActionResult _GetSectionDocuments(int locationId)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = ConvertTo.String(locationId);
            objExt.ModuleType = SystemEnum.DocumentModuleType.R.ToString();

            if (ProjectSession.PermissionAccess.Locations_Locations_Editrecords == false)
            {
                objExt.IsSaveButtonEnable = false;
                objExt.IsDeleteButtonEnable = false;
            }
            else
            {
                objExt.IsSaveButtonEnable = true;
                objExt.IsDeleteButtonEnable = true;
            }

            return PartialView(PartialViews._LocationDocumentList, objExt);
        }

        /// <summary>
        /// Gets the section document list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="foreignKeyID">The foreign key identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectionDocumentList)]
        public ActionResult GetSectionDocumentList([DataSourceRequest]DataSourceRequest request, string foreignKeyID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "FileName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            DocumentService obj = new DocumentService();

            var result = new DataSourceResult()
            {
                Data = obj.GetSectionDocumentList(foreignKeyID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the section document by identifier.
        /// </summary>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectionDocumentByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSectionDocumentByID(int autoID)
        {
            ExtAssetFile objExtAssetFile = new ExtAssetFile();
            if (autoID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objExtAssetFile = context.SelectObject<ExtAssetFile>(autoID);
                }
            }

            return Json(objExtAssetFile, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the section document.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        [ActionName(Actions.SaveSectionDocument)]
        public JsonResult SaveSectionDocument(ExtAssetFile obj)
        {
            HttpPostedFileBase file;
            string fileName = string.Empty;
            if (obj.AutoId == 0 && Request.Files.Count == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSelectFileToUpload }, JsonRequestBehavior.AllowGet);
            }
            else if (Request.Files.Count > 0)
            {
                file = Request.Files[0];
                fileName = Path.GetFileName(file.FileName);

                if (!("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(fileName) + ","))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed }, JsonRequestBehavior.AllowGet);
                }

                if (((file.ContentLength / 1024) / 1024) > ProjectSession.AllowedMaxFilesize)                                        
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileSizeNotAllowed +  ProjectSession.AllowedMaxFilesize + "MB." }, JsonRequestBehavior.AllowGet);
                }

                string subPath = string.Empty;
                string filePath = string.Empty;

                if (obj.IsLocationDiagram)
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathLocationDiagram + obj.FkId;
                    obj.ModuleType = SystemEnum.DocumentModuleType.D.ToString();
                }
                else
                {
                    subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathLocation + obj.FkId;
                    obj.ModuleType = SystemEnum.DocumentModuleType.L.ToString();
                }
                ////else if....Write Other Modules cases

                using (DapperContext context = new DapperContext())
                {
                    ExtAssetFile objExtAssetFile = new ExtAssetFile();
                    objExtAssetFile.FkId = obj.FkId;
                    objExtAssetFile.FileName = fileName;
                    objExtAssetFile.ModuleType = obj.ModuleType;
                    List<ExtAssetFile> lst = context.Search<ExtAssetFile>(objExtAssetFile).ToList();
                    ////List<ExtAssetFile> lst = context.SearchAll(objExtAssetFile).Where(x => x.FkId == obj.FkId && x.AutoId != obj.AutoId && x.FileName == fileName && x.ModuleType == obj.ModuleType).ToList();
                    if (lst != null && lst.Count > 0)
                    {
                        lst = lst.Where(x => x.AutoId != obj.AutoId).ToList();
                        if (lst != null && lst.Count > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileExists }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                bool folderExists = Directory.Exists(subPath);
                if (!folderExists)
                {
                    Directory.CreateDirectory(subPath);
                }

                filePath = subPath + "\\" + fileName;
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }

                file.SaveAs(filePath);

                if (obj.AutoId > 0)
                {
                    ////Delete Old file
                    string oldFileLink = obj.FileLink;
                    if (System.IO.File.Exists(ProjectConfiguration.UploadPath + oldFileLink))
                    {
                        System.IO.File.Delete(ProjectConfiguration.UploadPath + oldFileLink);
                    }

                    obj.ModifiedBy = ProjectSession.EmployeeID;
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj.CreatedBy = ProjectSession.EmployeeID;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifiedBy = ProjectSession.EmployeeID;
                    obj.ModifiedDate = DateTime.Now;
                }

                if (obj.IsLocationDiagram)
                {
                    obj.FileLink = ProjectConfiguration.UploadPathLocationDiagram + obj.FkId + "\\" + fileName;
                }
                else
                {
                    obj.FileLink = ProjectConfiguration.UploadPathLocation + obj.FkId + "\\" + fileName;
                }

                obj.FileName = fileName;
            }

            using (DapperContext context = new DapperContext())
            {
                int newAutoID = context.Save(obj);
                if (newAutoID > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Deletes the section document.
        /// </summary>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSectionDocument)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSectionDocument(int autoID)
        {
            using (DapperContext context = new DapperContext())
            {
                ExtAssetFile obj = new ExtAssetFile();
                obj = context.SelectObject<ExtAssetFile>(autoID);

                int returnValue = context.Delete<ExtAssetFile>(autoID, true);
                if (returnValue == 0)
                {
                    string filePath = ProjectConfiguration.UploadPath + obj.FileLink;
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Linked Locations Tab"

        /// <summary>
        /// Partial view Linked Locations
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetLinkedLocationsPage)]
        public ActionResult _PartialLinkedlocations(int locationID)
        {
            Location objModel = new Location();
            objModel.LocationID = locationID;

            return View(PartialViews.LinkedLocations, objModel);
        }

        /// <summary>
        /// Gets the Linked locations list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLinkedLocationsList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLinkedLocationsList([DataSourceRequest]DataSourceRequest request, int locationID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LocationNo";
                sortDirection = "asc";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            LocationService objService = new LocationService();

            var result = new DataSourceResult()
            {
                Data = objService.GetLinkedLocations(locationID, pageNumber, sortExpression, sortDirection, request),
                Total = objService.PagingInformation.TotalRecords
            };

            return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet, MaxJsonLength = int.MaxValue };
        }

        /// <summary>
        /// Gets the linked location count.
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        public JsonResult GetLinkedLocationCount(int locationID)
        {
            int count = LocationService.GetLinkedLocationsCount(locationID);
            return Json(count);
        }

        #endregion

        #region "PM Meter"

        /// <summary>
        /// _s the get pm meter detail.
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._PmMeterByLocation)]
        public ActionResult _PmMeterByLocation(int locationID)
        {
            Location objLocation = new Location();
            using (ServiceContext context = new ServiceContext())
            {
                objLocation = context.SelectObject<Location>(locationID);
            }

            return PartialView(PartialViews.PMMeterLocation, objLocation);
        }

        #endregion
    }
}