﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Purchase order request controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Job Order Purchase Request/Order"

        /// <summary>
        /// Partial view Purchase order request
        /// </summary>
        /// <param name="workOrderNO">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetPurchaseOrderRequest)]
        public ActionResult _PartialPurchaseOrderRequest(string workOrderNO)
        {
            PurchaseRequestItem model = new PurchaseRequestItem();
            model.JoborderID = workOrderNO;
            return View(PartialViews.PurchaseOrderRequest, model);
        }

        /// <summary>
        /// Gets the Purchase Request list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="jobOrderID">Job Order ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseRequestList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchaseRequestList([DataSourceRequest]DataSourceRequest request, string jobOrderID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PurchaseRequestNo";
                sortDirection = "Ascending";
            }

            if (!string.IsNullOrEmpty(jobOrderID))
            {
                strWhere = "and PurchaserequestItems.JobOrderID = '" + Common.setQuote(jobOrderID) + "' ";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseRequestItem purchaseRequestItem = new PurchaseRequestItem();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(purchaseRequestItem, SystemEnum.Pages.PurchaseRequestItem.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Purchase order list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="jobOrderID">job Order ID.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseOrderList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchaseOrderList([DataSourceRequest]DataSourceRequest request, string jobOrderID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PurchaseOrderNo";
                sortDirection = "Ascending";
            }

            if (!string.IsNullOrEmpty(jobOrderID))
            {
                strWhere = " and PurchaseOrderItems.JobOrderID = '" + Common.setQuote(jobOrderID) + "' ";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(purchaseOrderItem, SystemEnum.Pages.PurchaseOrderItem.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }
        #endregion

        #region "Purchase Request"
        #endregion
    }
}