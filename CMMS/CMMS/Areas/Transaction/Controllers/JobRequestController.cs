﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Job Request Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {

        #region "Job Request Escalation"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobRequestEscalationList)]
        public ActionResult JobRequestEscalationList()
        {
            if (ProjectSession.PermissionAccess.JobRequest_JobRequestEscalation_Allowaccess)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.JobRequestEscalationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }


        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobEscalationRequests)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetJobEscalationRequests([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WoRequest.workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            whereClause = " and worequest.requeststatusid IN (1, 8) ";

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and worequest.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
                whereClause += " and worequest.MainSubDeptId in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (worequest.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (worequest.L3ID IS NULL))";
                }

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and worequest.LocationID in  (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            WorkRequestEscalation objWorkRequest = new WorkRequestEscalation();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objWorkRequest, SystemEnum.Pages.JobRequestEscalationList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }



        #endregion

        #region "Job Request"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobRequestList)]
        public ActionResult JobRequestList()
        {
            if (ProjectSession.PermissionAccess.JobRequest_JobRequest_Allowaccess && ProjectSession.PermissionAccess.JobRequest__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.JobRequestList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Add/Update Job Request
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobRequests)]
        public ActionResult JobRequests()
        {
            return View(Views.JobRequests);
        }

        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobRequests)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetJobRequests([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, workorderno";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            if (!request.Filters.Any())
                whereClause = " and worequest.requeststatusid IN (1, 8) ";
            whereClause += " and worequest.CreatedBy= " + ProjectSession.EmployeeID;
            //if (!ProjectSession.IsCentral)
            //{
            //    whereClause += " and worequest.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            //    whereClause += " and worequest.MainSubDeptId in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";

            //    /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            //    if (ProjectSession.IsAreaFieldsVisible)
            //    {
            //        whereClause += " and (worequest.L3ID in (" + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (worequest.L3ID IS NULL))";
            //    }

            //    if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
            //    {
            //        whereClause += " and worequest.LocationID in  (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
            //    }

            //    /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            //}

            Worequest objWorkRequest = new Worequest();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objWorkRequest, SystemEnum.Pages.JobRequest.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Indexes jobrequest feedback.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.FeedbackJobRequestList)]
        public ActionResult FeedbackJobRequestList()
        {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.FeedbackJobRequest);
        }

       

 
        /// <summary>
        /// Gets the list of all job requests
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="requesterID">Requester ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllWRByRequesterID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetAllWRByRequesterID([DataSourceRequest]DataSourceRequest request, int requesterID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobRequestService objService = new JobRequestService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAllWRByRequesterID(requesterID, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Get Request by Id
        /// </summary>
        /// <param name="requestNo">Job Request No</param>
        /// <param name="assetID">Asset ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobRequestById)]
        [HttpGet]
        public ActionResult GetJobRequestById(string requestNo, int assetID = 0)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            Worequest objWorkRequest = new Worequest();
            Asset objAsset = new Asset();
            JobRequestService objService = new JobRequestService();
            AssetService objAssetService = new AssetService();

            if (!string.IsNullOrEmpty(requestNo))
            {
                objWorkRequest = objService.GetJobRequestByRequestNo(requestNo);
            }

            if (assetID > 0)
            {
                objAsset = objAssetService.GetAssetPageWithAllRelatedData(ProjectSession.EmployeeID, string.Empty, 1, string.Empty, string.Empty, assetID).FirstOrDefault();

                if (objAsset != null && objAsset.AssetID > 0)
                {
                    if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now)
                    {
                        HomeController obj1 = new HomeController();
                        if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpired.GetHashCode()).Data.ToString();
                            }
                        }
                        else
                        {
                            if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.ExpiredArabic.GetHashCode()).Data.ToString();
                            }
                            else
                            {
                                objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.Expired.GetHashCode()).Data.ToString();
                            }
                        }
                    }

                    objWorkRequest.AssetID = objAsset.AssetID;
                    objWorkRequest.AssetNumber = objAsset.AssetNumber;
                    objWorkRequest.AuthEmployeeName = objAsset.Name;
                    objWorkRequest.AuthorisedEmployeeId = objAsset.EmployeeID;
                    objWorkRequest.AssetDescription = objAsset.AssetDescription;
                    objWorkRequest.AssetAltDescription = objAsset.AssetAltDescription;

                    objWorkRequest.Asset = objAsset;

                    objWorkRequest.L1ID = objAsset.L1ID;
                    objWorkRequest.L2ID = objAsset.L2ID;
                    objWorkRequest.LocationID = objAsset.LocationID;
                    objWorkRequest.NoteToTechLocation = objAsset.NoteToTech;
                    objWorkRequest.LocationNo = objAsset.LocationNo;
                    objWorkRequest.LocationDescription = objAsset.LocationDescription;
                    objWorkRequest.LocationAltDescription = objAsset.LocationAltDescription;

                    if (ProjectSession.IsAreaFieldsVisible)
                    {
                        objWorkRequest.L3ID = objAsset.L3ID;
                    }

                    objWorkRequest.L4ID = Convert.ToInt32(objAsset.L4ID);
                    objWorkRequest.L4No = objAsset.L4No;

                    objWorkRequest.L5ID = Convert.ToInt32(objAsset.L5ID);
                    objWorkRequest.L5No = objAsset.L5No;
                    objWorkRequest.L5Description = objAsset.L5Description;
                    objWorkRequest.L5AltDescription = objAsset.L5AltDescription;
                }
            }

            return View(Views.JobRequests, objWorkRequest);

            ////if (objWorkRequest != null && !string.IsNullOrEmpty(objWorkRequest.RequestNo))
            ////{
            ////    return View(Views.JobRequests, objWorkRequest);
            ////}
            ////else
            ////{
            ////    ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
            ////    ViewBag.Message = "Job Request not found with request no " + requestNo;
            ////    return View(Views.JobRequestList);
            ////}
        }

        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="projectNumber">Project Number</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintDivByEmpIdOrProjectNumber)]
        [HttpGet]
        public JsonResult GetMaintDivByEmpIdOrProjectNumber(string projectNumber)
        {
            IList<MainenanceDivision> lstMaintDiv = new List<MainenanceDivision>();
            try
            {
                JobRequestService objService = new JobRequestService();

                lstMaintDiv = objService.GetMaintDivisionByEmployeeIDOrProjectNumber(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, projectNumber);
                return Json(lstMaintDiv, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="cityID">City ID</param>
        /// <param name="areaID">Area ID</param>
        /// <param name="zoneID">Zone ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuildingDetails)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetBuildingDetails([DataSourceRequest]DataSourceRequest request, int cityID, int areaID, int zoneID)
        {
            IList<L5> lstBuildings = new List<L5>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                BuildingService objService = new BuildingService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetBuildingDetails(cityID, areaID, zoneID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationForJobRequest)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetsByLocationForJobRequest([DataSourceRequest]DataSourceRequest request, int locationID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string strWhere = " and l.L2ID = " + locationID;
            if (!ProjectSession.IsCentral)
            {
                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    strWhere += " and ass.LocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }
            }

            AssetService obj = new AssetService();
            var result = new DataSourceResult();

            result = new DataSourceResult()
            {
                Data = obj.GetAssetPageWithAllRelatedData(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            foreach (Asset objAsset in result.Data)
            {
                if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now)
                {
                    HomeController obj1 = new HomeController();
                    if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                    {
                        if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpiredArabic.GetHashCode()).Data.ToString();
                        }
                        else
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpired.GetHashCode()).Data.ToString();
                        }
                    }
                    else
                    {
                        if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.ExpiredArabic.GetHashCode()).Data.ToString();
                        }
                        else
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.Expired.GetHashCode()).Data.ToString();
                        }
                    }
                }
            }

            return Json(result);
        }

        /// <summary>
        /// To Get WorkRequest Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="assetID">Asset ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllWRByAssetID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetAllWRByAssetID([DataSourceRequest]DataSourceRequest request, int assetID)
        {
            IList<Worequest> lstBuildings = new List<Worequest>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                JobRequestService objService = new JobRequestService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetAllWRByAssetID(assetID, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves employee data in database
        /// </summary>
        /// <param name="objWR">Object Of Employee Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveJobRequest)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveJobRequest(Worequest objWR)
        {
            string result = string.Empty;
            string projectNumber = string.Empty;
            string validations = string.Empty;
            /*Assign Project Number to ProjectNumber if it is visible*/

            try
            {
                if (ModelState.IsValid)
                {
                    if (objWR.ReceivedDate != null)
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWR.ReceivedDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWR.ReceivedTime));

                        objWR.ReceivedDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if (objWR.RequiredDate != null)
                    {
                        string dt1 = Convert.ToString(Common.GetEnglishDate(objWR.RequiredDate));
                        string dt2 = Convert.ToString(Common.GetEnglishDate(objWR.RequiredTime));
                        objWR.RequiredDate = DateTime.Parse(Common.ParseDateInEnglishFormate(dt1).ToString("d") + " " + Common.ParseDateInEnglishFormate(dt2).ToString("HH:mm:ss"));
                    }

                    if ((objWR.RequestNo == null || objWR.RequestNo == string.Empty || objWR.RequestNo == "0") && Convert.ToInt32(objWR.RequestStatusID) == SystemEnum.WorkRequestStatus.Open.GetHashCode())
                    {
                        ////New Request
                        if (!ProjectSession.PermissionAccess.JobRequest_JobRequest_CreatenewJobRequest)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                        }

                        //Remove below code because records is not listed if worktypeid is null on job reqest list. 
                        //if (objWR.WorkTypeID == SystemEnum.WorkType.Preventive.GetHashCode() || objWR.WorkTypeID == SystemEnum.WorkType.ManualPreventive.GetHashCode())
                        //{
                        //    objWR.WorkTypeID = null;
                        //}

                        long minRequestNo = Convert.ToInt64(DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));
                        long minRequestNo1 = 0;
                        Worequest objnewWr = new Worequest();
                        objnewWr.L2ID = objWR.L2ID;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            minRequestNo1 = Convert.ToInt64(objContext.Search<Worequest>(objnewWr).Select(x => x.RequestNo.Substring(x.RequestNo.IndexOf("R") + 1, x.RequestNo.Length - (x.RequestNo.IndexOf("R") + 1))).Max());
                        }

                        if (minRequestNo1 > minRequestNo)
                        {
                            minRequestNo = minRequestNo1;
                        }
                        employees objEmp = new employees();     // Send maintDept which is hidden in view By: Akif519
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            objEmp = objContext.SearchAllByID<employees>(objEmp, ProjectSession.EmployeeID).FirstOrDefault();
                        }

                        objWR.RequestNo = Convert.ToString(objWR.L2ID).PadLeft(3, '0') + "R" + Convert.ToString(minRequestNo + 1).PadLeft(8, '0');

                        objWR.CreatedBy = ProjectSession.EmployeeID.ToString();
                        objWR.CreatedByID = ProjectSession.EmployeeID;
                        objWR.CreatedDate = DateTime.Now;
                        objWR.MaintdivId = objEmp.MaintDivisionID;
                        objWR.MaintdeptId = objEmp.MaintDeptID;
                        objWR.MainSubDeptId = objEmp.MaintSubDeptID;
                        objWR.RequesterID = ProjectSession.EmployeeID;

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.SaveWithReturnString<Worequest>(objWR);
                            JobRequestService objRService = new JobRequestService();
                            objRService.ChangeJobRequestModificationDateTimeByWorkRequestNo(objWR.RequestNo, ProjectSession.EmployeeID.ToString());
                        }

                        //if (result != string.Empty && result != "-1")
                        if (result != "-1")
                        {
                            EmailConfigurationService objService = new EmailConfigurationService();
                            objService.CheckEmailNotificationRule(SystemEnum.ModuleCode.WorkRequest, SystemEnum.ModuleEvent.JobRequestCreation, objWR.RequestNo);

                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                            return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, requestNo = objWR.RequestNo });
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgEntityHasRelationShip;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, requestNo = "0" });
                        }
                    }
                    else
                    {
                        ////Update Request
                        JobRequestService objJRService = new JobRequestService();

                        objWR.ModifiedBy = ProjectSession.EmployeeID.ToString();
                        objWR.ModifiedDate = DateTime.Now;

                        if (Convert.ToInt32(objWR.RequestStatusID) == SystemEnum.WorkRequestStatus.Cancelled.GetHashCode() && ProjectSession.PermissionAccess.JobRequest_JobRequest_CancelJobRequest)
                        {
                            objWR.CancelledByID = ProjectSession.EmployeeID;
                        }

                        if (!ProjectSession.PermissionAccess.JobRequest_JobRequest_EditJobRequest)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                        }

                        if (Convert.ToInt32(objWR.RequestStatusID) == SystemEnum.WorkRequestStatus.WORaised.GetHashCode())
                        {
                            if (!ProjectSession.PermissionAccess.JobOrder_JobOrder_Createnewrecords)
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                            }

                            if (projectNumber != string.Empty)
                            {
                                objWR.ProjectNumber = projectNumber;
                            }
                            ////Create New Job Order
                            JobOrderService objJOService = new JobOrderService();
                            string workOrderNo = objJOService.NewJobOrderByWorkRequest(objWR);

                            if (workOrderNo != string.Empty)
                            {
                                objWR.WorkorderNo = workOrderNo;
                                if (objJRService.SaveWorkRequestByExtend(objWR))
                                {
                                    return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, workOrderNo = objWR.WorkorderNo });
                                }
                                else
                                {
                                    throw new Exception("Some Error While updating the record.");
                                }
                            }
                        }
                        else
                        {
                            if (objJRService.SaveWorkRequestByExtend(objWR))
                            {
                                if (Convert.ToInt32(objWR.CancelledByID) > 0)
                                {
                                    TempData["Message"] = ProjectSession.Resources.message.JobRequest_MsgCancelWorkRequestSuccess;
                                }
                                else
                                {
                                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                                }

                                TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                                return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, requestNo = objWR.RequestNo });
                                ////return RedirectToAction(Actions.JobRequestList, Pages.Controllers.Transaction);
                            }
                            else
                            {
                                throw new Exception("Some Error While updating the record.");
                            }
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { requestNo = string.IsNullOrEmpty(objWR.RequestNo) ? "0" : objWR.RequestNo });
                    }
                    else
                    {
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { requestNo = string.IsNullOrEmpty(objWR.RequestNo) ? "0" : objWR.RequestNo });
                    }
                }

                return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { requestNo = string.IsNullOrEmpty(objWR.RequestNo) ? "0" : objWR.RequestNo });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                if (Convert.ToString(TempData["Message"]) == string.Empty)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                }

                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                return RedirectToAction(Actions.GetJobRequestById, Pages.Controllers.Transaction, new { requestNo = string.IsNullOrEmpty(objWR.RequestNo) ? "0" : objWR.RequestNo });
            }
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetApprovedProjectListPage)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetApprovedProjectListPage([DataSourceRequest]DataSourceRequest request, string cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ProjectNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JobRequestService obj = new JobRequestService();
            var result = new DataSourceResult();

            result = new DataSourceResult()
            {
                Data = obj.GetApprovedProjectListPage(cityID, pageNumber, string.Empty, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        #endregion

        #region "Document Tab"

        /// <summary>
        /// To Get Job Request Documents
        /// </summary>
        /// <param name="requestNo">The JobRequest identifier.</param>
        /// <param name="requestStatusId">Request Status ID</param>
        /// <returns></returns>
        [ActionName(Actions._GetJobRequestDocuments)]
        public ActionResult _GetJobRequestDocuments(string requestNo, int requestStatusId)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = requestNo;
            objExt.ModuleType = SystemEnum.DocumentModuleType.R.ToString();

            if (requestStatusId != SystemEnum.WorkRequestStatus.Open.GetHashCode() || ProjectSession.PermissionAccess.JobRequest_JobRequest_EditJobRequest == false)
            {
                objExt.IsSaveButtonEnable = false;
                objExt.IsDeleteButtonEnable = false;
            }
            else
            {
                objExt.IsSaveButtonEnable = true;
                objExt.IsDeleteButtonEnable = true;
            }

            return PartialView(PartialViews._DocumentList, objExt);
        }
        #endregion
    }
}