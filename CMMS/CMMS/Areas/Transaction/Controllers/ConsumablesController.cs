﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;
using CMMS.Reports;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Consumables Controller
    /// </summary>
    public partial class TransactionController : BaseController 
    {
        /// <summary>
        /// Consumables the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ConsumablesList)]
        public ActionResult ConsumablesList()
        {
            if (ProjectSession.PermissionAccess.RequestCard_Consumables_Allowaccess && ProjectSession.PermissionAccess.RequestCard__ShowHideModule)
            {
                return View(Views.ConsumablesList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the consumables list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetConsumablesList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetConsumablesList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;
            string substoreId = string.Empty;
            substoreId = Common.GetConfigKeyValue("SubStoreConsume");

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CardId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            string whereClause = string.Empty;

            if (!ProjectSession.IsCentral)
            {
                whereClause = " and SubStoreIdRef in ( Select s.SubStoreId from SubStore s where s.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ) ";
            }

            whereClause += " and Status <> 3 and SubStoreIdRef =  " + ConvertTo.Integer(substoreId);

            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objSubStoreCardForm, SystemEnum.Pages.ConsumableList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Consumables the detail.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ConsumablesDetail)]
        public ActionResult ConsumablesDetail(int id)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            string substoreId = string.Empty;
            substoreId = Common.GetConfigKeyValue("SubStoreConsume");

            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();
            StockCodeService objService = new StockCodeService();
            objSubStoreCardForm.CardId = id;
            if (id > 0)
            {
                objSubStoreCardForm = objService.GetConsumablesDetailByCardID(id);
            }
            else
            {
                objSubStoreCardForm.Status = 2;
                objSubStoreCardForm.DateValue = DateTime.Now;
                objSubStoreCardForm.SubStoreIdRef = ConvertTo.Integer(substoreId);
            }

            return View(Views.ConsumablesDetail, objSubStoreCardForm);
        }

        /// <summary>
        /// _s the get consumables documents.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._GetConsumablesDocuments)]
        public ActionResult _GetConsumablesDocuments(int id)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = id.ToString();
            objExt.ModuleType = SystemEnum.DocumentModuleType.C.ToString();

            if (ProjectSession.PermissionAccess.JobOrder_JobOrder_Editrecords == false)
            {
                objExt.IsSaveButtonEnable = false;
                objExt.IsDeleteButtonEnable = false;
            }
            else
            {
                objExt.IsSaveButtonEnable = true;
                objExt.IsDeleteButtonEnable = true;
            }

            return PartialView(PartialViews._DocumentList, objExt);
        }

        /// <summary>
        /// Gets the sub store status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetSubStoreStatus)]
        public ActionResult GetSubStoreStatus()
        {
            SubStoreStatu objSubStoreStatu = new SubStoreStatu();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objSubStoreStatu);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the sub store card form detail by card identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="returnStore">The return store.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSubStoreCardFormDetailByCardID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetSubStoreCardFormDetailByCardID([DataSourceRequest]DataSourceRequest request, int cardId, int returnStore)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CardIssueId";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string whereClause = string.Empty;
            whereClause = " and s.CardId = " + cardId;

            StockCodeService objService = new StockCodeService();
            var result = new DataSourceResult()
                {
                    Data = objService.GetSubStoreCardFormDetailByCardID(whereClause, pageNumber, sortExpression, sortDirection, returnStore, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            return Json(result);
        }

        /// <summary>
        /// Manages the sub store card form detail.
        /// </summary>
        /// <param name="objSubStoreCardFormDetail">The object sub store card form detail.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSubStoreCardFormDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSubStoreCardFormDetail(SubStoreCardFormDetail objSubStoreCardFormDetail)
        {
            string validations = string.Empty;

            if (objSubStoreCardFormDetail.CardIssueId > 0)
            {
                objSubStoreCardFormDetail.ModifiedBy = ProjectSession.EmployeeID;
                objSubStoreCardFormDetail.ModfiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.RequestCard_Consumables_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSubStoreCardFormDetail.CreatedBy = ProjectSession.EmployeeID;
                objSubStoreCardFormDetail.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.RequestCard_Consumables_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    if (context.Save(objSubStoreCardFormDetail) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.MaintenanceSubDept_MsgMaintSubDeptCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Gets the employees by sub department identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="subDeptID">The sub department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesBySubDeptID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetEmployeesBySubDeptID([DataSourceRequest]DataSourceRequest request, int subDeptID)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetEmployeesBySubDeptID(subDeptID, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the consumables.
        /// </summary>
        /// <param name="objSubStoreCardForm">The object sub store card form.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageConsumables)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageConsumables(SubStoreCardForm objSubStoreCardForm)
        {
            string validations = string.Empty;

            if (objSubStoreCardForm.CardId > 0)
            {
                objSubStoreCardForm.ModfiedBy = ProjectSession.EmployeeID;
                objSubStoreCardForm.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.RequestCard_Consumables_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSubStoreCardForm.CreatedBy = ProjectSession.EmployeeID;
                objSubStoreCardForm.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.RequestCard_Consumables_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("CardNo"))
                {
                    int cardId = context.Save(objSubStoreCardForm);
                    if (cardId > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, cardId });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Store_MsgCardNoAlreadyExit });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Manages the lock issue.
        /// </summary>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="statusId">The status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageLockIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageLockIssue(int cardId, int statusId)
        {
            string validations = string.Empty;
            StockCodeService objService = new StockCodeService();
            bool flag = false;

            string cityId = string.Empty;
            cityId = Common.GetConfigKeyValue("CityStore");

            if (cardId > 0 || statusId > 0)
            {
                flag = objService.LockIssue(cardId, statusId, ConvertTo.Integer(cityId));
                if (flag)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, cardId });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Prints the specified card identifier.
        /// </summary>
        /// <param name="cardId">The card identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.PrintConsumables)]
        public ActionResult PrintConsumables(int cardId)
        {
            StockCodeService objService = new StockCodeService();
            SubStoreCardForm objSubStoreCardForm = new SubStoreCardForm();
            List<PrintConsumableTable> objStockCode = new List<PrintConsumableTable>();
            objStockCode = objService.PrintConsumableTableDetails(cardId);
            objSubStoreCardForm = objService.GetConsumablesDetailByCardID(cardId);
            SubStoreItemReport report = new SubStoreItemReport();
            report.DataSource = objStockCode;
            report.txtCardNo.Text = objSubStoreCardForm.CardNo;
            report.txtDateValue.Text = Common.FormatDateTime(objSubStoreCardForm.DateValue, ProjectConfiguration.DATEFORMATDDMMMYYYHHMMTT);
            report.txtAttachedValue.Text = objSubStoreCardForm.Attached;
            report.txtNameValue.Text = objSubStoreCardForm.Name;
            report.txtEmpNo.Text = objSubStoreCardForm.EmployeeNO;
            report.txtPosition.Text = objSubStoreCardForm.Positions;
            report.txtUserId.Text = ProjectSession.EmployeeName;
            report.txtRemarks.DataBindings.Add("Text", objStockCode, "Remarks");
            report.txtQty.DataBindings.Add("Text", objStockCode, "QtyIssue");
            report.txtRowNo.DataBindings.Add("Text", objStockCode, "RowNo");
            report.txtUom.DataBindings.Add("Text", objStockCode, "uom");
            report.txtAltItem.DataBindings.Add("Text", objStockCode, "AltStockDescription");
            report.txtItem.DataBindings.Add("Text", objStockCode, "StockDescription");
            report.txtStockNo.DataBindings.Add("Text", objStockCode, "StockNo");
            return View(Views.PrintView, report);
        }

        #region "Store Card Return Popup"

        /// <summary>
        /// _s the store card return popup.
        /// </summary>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="isType">Type of the is.</param>
        /// <returns></returns>
        [ActionName(Actions._StoreCardReturnPopup)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _StoreCardReturnPopup(int cardId, int isType)
        {
            SubStoreFormRetunDetail objSubStoreFormRetunDetail = new SubStoreFormRetunDetail();
            if (cardId > 0)
            {
                objSubStoreFormRetunDetail.CardIdRef = cardId;
                objSubStoreFormRetunDetail.IsType = isType;
            }

            return PartialView(Pages.PartialViews.StoreCardReturnPopup, objSubStoreFormRetunDetail);
        }

        /// <summary>
        /// Manages the store card return.
        /// </summary>
        /// <param name="objSubStoreCardFormDetail">The object sub store card form detail.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageStoreCardReturn)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageStoreCardReturn(SubStoreCardFormDetail objSubStoreCardFormDetail)
        {
            string validations = string.Empty;

            foreach (var lst in objSubStoreCardFormDetail.lstSubStoreCardFormDetail)
            {
                if (lst.QtyNeeded <= 0 || lst.QtyNeeded > lst.QtyIssue)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Store_MsgQtyNeedMustBegreaterThanZero }, JsonRequestBehavior.AllowGet);
                }
            }

            StockCodeService objService = new StockCodeService();
            bool flag = false;
            flag = objService.NewSubStoreCardReturOrDispose(objSubStoreCardFormDetail.lstSubStoreCardFormDetail, objSubStoreCardFormDetail.isType);
            if (flag)
            {
                if (objSubStoreCardFormDetail.isType == 1)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Store_MsgReturnSuccessfully });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Store_MsgDisposedSuccessfully });
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}