﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Controllers;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Reports;
using DevExpress.XtraReports.UI;
using CMMS.DAL;
using DevExpress.DataAccess.ObjectBinding;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Jo Generation Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Pm the jo generation.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMJoGeneration)]
        public ActionResult PMJoGeneration()
        {
            if (ProjectSession.PermissionAccess.Preventive_TimeSchedules_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                PmSchedule objPmSchedule = new PmSchedule();
                using (ServiceContext context = new ServiceContext())
                {
                    employees obj = new employees();
                    obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                    objPmSchedule.MaintDivisionID = obj.MaintDivisionID;
                    objPmSchedule.MaintDeptID = obj.MaintDeptID;
                    objPmSchedule.MaintSubDeptID = obj.MaintSubDeptID;

                    if (ProjectSession.DateCulture == ProjectConfiguration.GregorianDate)
                    {
                        objPmSchedule.DefaultDate = Convert.ToDateTime("1-Jan-1990");
                    }
                    else
                    {
                        objPmSchedule.DefaultDate = Convert.ToDateTime("04-جمادى الثانية-1410");
                    }
                }

                return View(Views.PMJoGeneration, objPmSchedule);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the list pm gen dates.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance division.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.GetListPMGenDates)]
        public JsonResult GetListPMGenDates(string cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            DateTime? lastGenerateDate = null;
            DateTime? generatedUntil = null;

            List<PMGenDate> lstPMGenDate = PMJoGenerationService.GetListPMGenDates(cityID, maintenanceDiv, maintenanceDept, maintenanceSubDept);
            if (lstPMGenDate.Count > 0)
            {
                lastGenerateDate = lstPMGenDate.Max(x => x.DateGenerate);
                generatedUntil = lstPMGenDate.Max(x => x.ToDate);
            }

            return Json(new object[] { lastGenerateDate, generatedUntil });
        }

        /// <summary>
        /// Views the pm.
        /// </summary>
        /// <param name="generateUntil">The generate until.</param>
        /// <param name="defaultDate">The default date.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance division.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.ViewPM)]
        public JsonResult ViewPM(DateTime generateUntil, DateTime defaultDate, string cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            try
            {
                PMJoGenerationService objService = new PMJoGenerationService();
                objService.clearPMLockIfStuck(ProjectSession.EmployeeID);
                objService.SaveIntermediateDataByListL2ID(generateUntil, defaultDate, ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                return Json("success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the intermediate data by user identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="basedOnDate">The based on date.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIntermediateDataByUserId)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetIntermediateDataByUserId([DataSourceRequest]DataSourceRequest request, int basedOnDate)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "TargetStartDate";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMJoGenerationService obj = new PMJoGenerationService();
            var result = new DataSourceResult();
            if (basedOnDate == 1)
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetIntermediateDataByUserId_ScheduleDate(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
            }
            else if (basedOnDate == 2)
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetIntermediateDataByUserId_CompletedDate(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Opens the pm jo.
        /// </summary>
        /// <param name="generateUntil">The generate until.</param>
        /// <param name="defaultDate">The default date.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance division.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.OpenPMJo)]
        public JsonResult OpenPMJo(DateTime generateUntil, DateTime defaultDate, string cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            try
            {
                PMJoGenerationService objService = new PMJoGenerationService();
                bool result = objService.SetPMLock(ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                if (result == false)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PMJOGeneration_MsgPMJOGenInProgress }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objService.SaveIntermediateDataByListL2ID(generateUntil, defaultDate, ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                    objService.GeneratePMJO(generateUntil, defaultDate, ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                    return Json("success");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Prints the pm jo.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.PrintPMJO)]
        public ActionResult PrintPMJO(string employeeId)
        {
            int nonCentral;
            if (ProjectSession.IsCentral == true)
            {
                nonCentral = 0;
            }
            else
            {
                nonCentral = 1;
            }

            JobOrderService objService = new JobOrderService();
            List<WorkOrder> objWO = new List<WorkOrder>();
            List<SITask> objSafetyInstruction = new List<SITask>();
            List<Workorderelement> objCheckList = new List<Workorderelement>();
            PMJobOrderReport report = new PMJobOrderReport();
            if (!string.IsNullOrEmpty(employeeId))
            {
                objWO = objService.GetJobOrderDetailsForDataSource(employeeId, nonCentral).ToList();
                objSafetyInstruction = objService.GetSafetyInsturctionDetailsForDataSource(employeeId, nonCentral).ToList();
                objCheckList = objService.GetJobOrderPMCheckListForDataSource(employeeId, nonCentral).ToList();
            }

            report.DataSource = objWO;
            report.SafetyInsturction.DataSource = objSafetyInstruction;
            report.checkList.DataSource = objCheckList;

            ////labels
            report.MainHeader.Text = ProjectSession.Resources.label.Reports_TextBlockPMJobOrderReportHeader + " :";
            report.jobOrderNo.Text = ProjectSession.Resources.label.JobRequest_TextBlockJobOrderNo + " :";
            report.label1.Text = ProjectSession.Resources.label.JobTrade_TextBlockJobTrade + " :";
            report.label2.Text = ProjectSession.Resources.label.JobStatus_TextBlockJobStatus + " :";
            report.xrLabel2.Text = ProjectSession.Resources.label.JobType_TextBlockJobType + " :";
            report.xrLabel3.Text = ProjectSession.Resources.label.JobPriority_TextBlockJobPriority + " :";
            report.CreatedBy.Text = ProjectSession.Resources.label.JobRequest_TextBlockCreatedBy + " :";
            report.xrLabel5.Text = ProjectSession.Resources.label.Reports_TextBlockReceivedDateTime + " :";
            report.xrLabel6.Text = ProjectSession.Resources.label.Reports_TextBlockRequiredDateTime + " :";
            report.xrLabel4.Text = ProjectSession.Resources.label.Reports_TextBlockAssignedToSupplier + " :";
            report.xrLabel7.Text = ProjectSession.Resources.label.JobOrder_TextBlockRequesterName + " :";
            report.xrLabel8.Text = ProjectSession.Resources.label.Supplier_TextBlockPhoneNumber + " :";
            report.ProblemDesc.Text = ProjectSession.Resources.message.JobOrder_TextBlockProbDesc + " :";
            report.NotestoAssets.Text = ProjectSession.Resources.label.Reports_TextBlockNotesToTechAsset + " :";
            report.NotesToLocation.Text = ProjectSession.Resources.label.Reports_TextBlockNotesToTechLocation + " :";
            report.FailureCode.Text = ProjectSession.Resources.label.FailureCodes_TextBlockFailureCode + " :";
            report.CityCode.Text = ProjectSession.Resources.label.JobRequest_TextBlockCityCode + " :";
            report.AreaCode.Text = ProjectSession.Resources.label.JobRequest_TextBlockAreaCode + " :";
            report.ZoneCode.Text = ProjectSession.Resources.label.JobRequest_TextBlockStreetCode + " :";
            report.BuildingCode.Text = ProjectSession.Resources.audit.AuditTrail_assets_l5no + " :";
            report.lablPmNo.Text = ProjectSession.Resources.label.PM_TextBlockPMNo + " :";
            report.locationNumber.Text = ProjectSession.Resources.label.Employee_TextBlockLoacationNo + " :";
            report.LocationDescription.Text = ProjectSession.Resources.label.Location_TextBlockLocationDesc + " :";
            report.Asset.Text = ProjectSession.Resources.menu.Reports_MenuAsset + " :";
            report.AssetDescription.Text = ProjectSession.Resources.label.JobRequest_TextBlockAssetDescription + " :";
            report.MaintDivCode.Text = ProjectSession.Resources.label.MaintDivision_TextBlockMaintDivCode + " :";
            report.MaintDeptCode.Text = ProjectSession.Resources.label.MaintenanceDept_TextBlockMaintDeptCode + " :";
            report.MaintSubDeptCode.Text = ProjectSession.Resources.label.MaintenanceSubDept_TextBlockMaintenanceSubDeptCode + " :";
            report.ItemDetailLabl.Text = ProjectSession.Resources.label.Reports_TextBlockItemsLabourDetails + " :";
            report.TableCell1.Text = ProjectSession.Resources.label.ItemList_TextBlockPartNO;
            report.TableCell2.Text = ProjectSession.Resources.label.ItemList_TextBlockPartDescription;
            report.TableCell3.Text = ProjectSession.Resources.label.Reports_TextBlockIssueType;
            report.TableCell4.Text = ProjectSession.Resources.label.Item_TextBlockUOM;
            report.TableCell5.Text = ProjectSession.Resources.label.Reports_TextBlockQtyUsed;
            report.TableCell6.Text = ProjectSession.Resources.label.Reports_TextBlockQtyReturned;
            report.EmployeeName.Text = ProjectSession.Resources.label.Employee_TextBlockEmployeeName;
            report.EmployeeNo.Text = ProjectSession.Resources.label.Employee_TextBlockEmployeeNO;
            report.Normal.Text = ProjectSession.Resources.label.Reports_TextBlockNormal;
            report.OT1.Text = ProjectSession.Resources.label.Employee_TextBlockOT1;
            report.OT2.Text = ProjectSession.Resources.label.Employee_TextBlockOT2;
            report.OT3.Text = ProjectSession.Resources.label.Employee_TextBlockOT3;
            report.xrLabel15.Text = ProjectSession.Resources.label.Reports_TextBlockCertificateOfWorkCompletion;
            report.xrLabel10.Text = ProjectSession.Resources.label.JobOrder_TextBlockCauseDescription;
            report.xrLabel9.Text = ProjectSession.Resources.label.JobOrder_TextBlockActionTaken;
            report.xrLabel1.Text = ProjectSession.Resources.label.JobOrder_TextBlockPreventionTaken;
            report.DateTimeStarted.Text = ProjectSession.Resources.label.Reports_TextBlockDateTimeWorkStarted;
            report.DateTimeCopleted.Text = ProjectSession.Resources.label.Reports_TextBlockDateTimeWorkCompleted;
            report.xrLabel18.Text = ProjectSession.Resources.label.Reports_TextBlockNameSignOfTechnician;
            report.xrLabel20.Text = ProjectSession.Resources.label.Reports_TextBlockNameSignOfRequester;
            report.xrLabel16.Text = ProjectSession.Resources.label.Reports_TextBlockRateServices;
            report.xrLabel33.Text = ProjectSession.Resources.label.SafetyInstruction_TextBlockSafetyInstructions;
            report.xrLabel28.Text = ProjectSession.Resources.label.Reports_TextBlockReferToAttachedPM;
            report.xrLabel29.Text = ProjectSession.Resources.label.PMChecklist_TextBlockPMChecklistNo + " :";
            report.xrLabel31.Text = ProjectSession.Resources.label.PMChecklist_TextBlockPMChecklistDesc + " :";
            report.xrLabel17.Text = ProjectSession.Resources.label.Reports_TextBlockRatings;
            report.DI.Text = ProjectSession.Resources.label.Reports_TextBlockDINote;
            report.Date.Text = ProjectSession.Resources.label.AssetTransfer_TextBlockDate + " :";
            report.Time.Text = ProjectSession.Resources.label.Reports_TextBlockTime + " :";
            report.DateComplete.Text = ProjectSession.Resources.label.AssetTransfer_TextBlockDate + " :";
            report.TimeCompleted.Text = ProjectSession.Resources.label.Reports_TextBlockTime + " :";
            report.xrLabel19.Text = ProjectSession.Resources.label.ItemRequisition_TextBlockName;
            report.xrLabel24.Text = ProjectSession.Resources.label.Reports_TextBlockSignature;
            report.xrLabel21.Text = ProjectSession.Resources.label.AssetTransfer_TextBlockDate + " :";
            report.xrLabel25.Text = ProjectSession.Resources.label.Reports_TextBlockTime + " :";
            report.xrLabel22.Text = ProjectSession.Resources.label.ItemRequisition_TextBlockName;
            report.xrLabel26.Text = ProjectSession.Resources.label.Reports_TextBlockSignature;
            report.xrLabel23.Text = ProjectSession.Resources.label.AssetTransfer_TextBlockDate + " :";
            report.xrLabel27.Text = ProjectSession.Resources.label.Reports_TextBlockTime + " :";

            report.JOPlanNo.DataBindings.Add("Text", objWO, "WorkOrderNo");
            report.creatdBy.DataBindings.Add("Text", objWO, "Name");
            report.rcvDatetime.DataBindings.Add("Text", objWO, "DateReceived");
            report.reqDatetime.DataBindings.Add("Text", objWO, "DateRequired");
            report.problemDescription.DataBindings.Add("Text", objWO, "ProblemDescription");
            report.notesAssets.DataBindings.Add("Text", objWO, "NotesToTechLocation");
            report.xrLabel14.DataBindings.Add("Text", objWO, "NotesToTechLocation");
            report.txtLocationNumber.DataBindings.Add("Text", objWO, "LocationNo");
            report.txtlocationDescription.DataBindings.Add("Text", objWO, "LocationDescription");
            report.phoneNumber.DataBindings.Add("Text", objWO, "WorkPhone");
            report.JobTrade.DataBindings.Add("Text", objWO, "WorkTrade");
            report.JobStatus.DataBindings.Add("Text", objWO, "WorkStatus");
            report.JobType.DataBindings.Add("Text", objWO, "WorkTypeDescription");
            report.JobPriority.DataBindings.Add("Text", objWO, "WorkPriority");
            report.assignedSupplier.DataBindings.Add("Text", objWO, "SupplierName");
            report.txtMaintDivCode.DataBindings.Add("Text", objWO, "MaintDivisionCode");
            report.txtMaintDeptCode.DataBindings.Add("Text", objWO, "MaintDeptCode");
            report.txtMaintSubDeptCode.DataBindings.Add("Text", objWO, "MaintSubDeptCode");
            report.failCode.DataBindings.Add("Text", objWO, "FailureCauseCode");
            report.ctyCode.DataBindings.Add("Text", objWO, "L2Code");
            report.areaCod.DataBindings.Add("Text", objWO, "L3No");
            report.znCode.DataBindings.Add("Text", objWO, "L4No");
            report.buildCode.DataBindings.Add("Text", objWO, "L5No");
            report.txtPmNo.DataBindings.Add("Text", objWO, "PMNo");
            report.txtAsset.DataBindings.Add("Text", objWO, "AssetNumber");
            report.txtAssetDescription.DataBindings.Add("Text", objWO, "AssetDescription");
            report.txtCauseDesc.DataBindings.Add("Text", objWO, "CauseDescription");
            report.txtActionTaken.DataBindings.Add("Text", objWO, "ActionTaken");
            report.txtPrevTaken.DataBindings.Add("Text", objWO, "PreventionTaken");
            report.seqId.DataBindings.Add("Text", objSafetyInstruction, "SeqId");
            report.xrTableCell2.DataBindings.Add("Text", objSafetyInstruction, "Details");
            report.xrTableCell3.DataBindings.Add("Text", objSafetyInstruction, "Remarks");
            report.xrLabel30.DataBindings.Add("Text", objWO, "ChecklistNo");
            report.xrLabel32.DataBindings.Add("Text", objWO, "CheckListName");
            report.seqNo.DataBindings.Add("Text", objCheckList, "SeqNo");
            report.taskDesc.DataBindings.Add("Text", objCheckList, "TaskDesc");
            report.altTaskDesc.DataBindings.Add("Text", objCheckList, "AltTaskDesc");
            return View(Views.PrintView, report);
        }
    }
}