﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using Microsoft.VisualBasic;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Schedule Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Declare variables"
        /// <summary>
        /// The first day
        /// </summary>
        private DateTime firstDay, tempDay, lastDay, PMDate;

        /// <summary>
        /// The z month
        /// </summary>
        private int zMonth, zYear, Dayno, Tskno, msdseq, jbseq;

        /// <summary>
        /// The task_no
        /// </summary>
        private string taskno;

        /// <summary>
        /// The PMFT
        /// </summary>
        private List<PmForecastTemp> pmft = new List<PmForecastTemp>();

        /// <summary>
        /// The PMFT
        /// </summary>
        private List<PmForecastTemp> newpmft = new List<PmForecastTemp>();

        #endregion

        #region "PM Schedule"

        /// <summary>
        /// Pm the schedule list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMScheduleList)]
        public ActionResult PMScheduleList()
        {
            if (ProjectSession.PermissionAccess.Preventive_TimeSchedules_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                return View(Views.PMSchedule);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the pm schedules.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMSchedules)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPMSchedules([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "pmschedule.PMNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            whereClause = "  and pmschedule.IsCleaningModule=0  AND pmschedule.PMGroupNo IS NULL  ";

            if (!ProjectSession.IsCentral)
            {
                /*(Start)Added By Pratik Telaviya on 24-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                whereClause += " and pmschedule.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";

                if (ProjectSession.PermissionAccess.Locations_Locations_LimitAccesstoLocation)
                {
                    whereClause += " and pmschedule.PhyLocationID in (" + ProjectConfiguration.LOCATIONIDLISTPERMISSIONWISE + " ) ";
                }

                /*(End)Added By Pratik Telaviya on 24-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                whereClause += " and pmschedule.MaintSubDeptID in (" + ProjectConfiguration.SUBDEPTIDLISTPERMISSIONWISE + ")";
            }

            PmSchedule objPmSchedule = new PmSchedule();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPmSchedule, SystemEnum.Pages.PMSchedule.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm schedule by identifier.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMScheduleByID)]
        [HttpGet]
        public ActionResult GetPMScheduleByID(int timeID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            PmSchedule objPmSchedule = new PmSchedule();
            objPmSchedule.PMID = timeID;

            if (timeID == 0)
            {
                employees obj = new employees();
                Workpriority objWorkPriority = new Workpriority();
                Worktype objWorktype = new Worktype();
                Worktrade objWorktrade = new Worktrade();

                objWorkPriority.IsDefault = true;
                objWorktype.IsDefault = true;
                objWorktrade.IsDefault = true;

                using (ServiceContext context = new ServiceContext())
                {
                    obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                    objPmSchedule.MaintDivisionID = obj.MaintDivisionID;
                    objPmSchedule.MaintDeptID = obj.MaintDeptID;
                    objPmSchedule.MaintSubDeptID = obj.MaintSubDeptID;

                    objWorkPriority = context.Search<Workpriority>(objWorkPriority).FirstOrDefault();
                    if (objWorkPriority != null)
                    {
                        objPmSchedule.Workpriorityid = objWorkPriority.WorkPriorityID;
                    }

                    objPmSchedule.WorkTypeID = 2; ////Preventive

                    objWorktrade = context.Search<Worktrade>(objWorktrade).FirstOrDefault();
                    if (objWorktrade != null)
                    {
                        objPmSchedule.Worktradeid = objWorktrade.WorkTradeID;
                    }
                }

                objPmSchedule.Frequency = 1;
                objPmSchedule.PeriodDays = 1;
                objPmSchedule.TypePM = "1";
            }
            else
            {
                objPmSchedule = PMScheduleService.GetPmScheduleById(timeID);
                if (objPmSchedule.TypePMgenID == 1 || objPmSchedule.TypePMgenID == 2)
                {
                    objPmSchedule.Days = objPmSchedule.FreqUnits * objPmSchedule.Frequency;
                }
                else if (objPmSchedule.TypePMgenID == 3)
                {
                    ////3.Fixed Day and Week
                    objPmSchedule.Month = Convert.ToDateTime(objPmSchedule.TargetStartDate).Month;
                    objPmSchedule.Year = Convert.ToDateTime(objPmSchedule.TargetStartDate).Year;

                    objPmSchedule.Frequency_Week = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Week = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Week = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Week = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Week = objPmSchedule.NextDate;
                }
                else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                {
                    objPmSchedule.Frequency_Month = objPmSchedule.Frequency;
                    objPmSchedule.PeriodDays_Month = objPmSchedule.PeriodDays;
                    objPmSchedule.TargetStartDate_Month = objPmSchedule.TargetStartDate;
                    objPmSchedule.TargetCompDate_Month = objPmSchedule.TargetCompDate;
                    objPmSchedule.NextDate_Month = objPmSchedule.NextDate;
                }
            }

            return View(Views.PMScheduleDetail, objPmSchedule);
        }

        /// <summary>
        /// Manages the pm schedule.
        /// </summary>
        /// <param name="objPmSchedule">The object pm schedule.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMSchedule)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePMSchedule(PmSchedule objPmSchedule)
        {
            ////Remaining logic of last genrated date
            string validations = string.Empty;
            int newScheduleID = 0;
            try
            {
                ////1.Asset,2.location
                if (objPmSchedule.TypePM == "1")
                {
                    objPmSchedule.PhyLocationID = objPmSchedule.LocationIDAsset;
                }

                if (objPmSchedule.TypePMgenID == 3)
                {
                    objPmSchedule.Frequency = objPmSchedule.Frequency_Week;
                    objPmSchedule.PeriodDays = objPmSchedule.PeriodDays_Week;
                    objPmSchedule.TargetStartDate = objPmSchedule.TargetStartDate_Week;
                    objPmSchedule.TargetCompDate = objPmSchedule.TargetCompDate_Week;
                    objPmSchedule.NextDate = objPmSchedule.NextDate_Week;
                }
                else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                {
                    objPmSchedule.Frequency = objPmSchedule.Frequency_Month;
                    objPmSchedule.PeriodDays = objPmSchedule.PeriodDays_Month;
                    objPmSchedule.TargetStartDate = objPmSchedule.TargetStartDate_Month;
                    objPmSchedule.TargetCompDate = objPmSchedule.TargetCompDate_Month;
                    objPmSchedule.NextDate = objPmSchedule.NextDate_Month;
                }

                if (objPmSchedule.PMID > 0)
                {
                    objPmSchedule.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objPmSchedule.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objPmSchedule.WorkTypeID = 2; ////Preventive
                    objPmSchedule.PMActive = 1;
                    objPmSchedule.PMCounter = 1;
                    objPmSchedule.PMMultiple = 0;
                    objPmSchedule.IsCleaningModule = false;
                    objPmSchedule.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objPmSchedule.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext("PMNo"))
                    {
                        newScheduleID = context.Save(objPmSchedule);
                    }

                    if (newScheduleID > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();

                        return RedirectToAction(Actions.GetPMScheduleByID, Pages.Controllers.Transaction, new { timeID = newScheduleID });
                    }
                    else
                    {
                        if (objPmSchedule.PMID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.PM_PMNoExists;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                            return RedirectToAction(Actions.GetPMScheduleByID, Pages.Controllers.Transaction, new { timeID = objPmSchedule.PMID });
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.PM_PMNoExists;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                            return View(Views.PMScheduleDetail, objPmSchedule);
                            ////return RedirectToAction(Actions.GetPMScheduleByID, Pages.Controllers.Transaction, new { timeID = newScheduleID });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.PMScheduleDetail, objPmSchedule);
                    //// return RedirectToAction(Actions.GetPMScheduleByID, Pages.Controllers.Transaction, new { timeID = newScheduleID });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the assets by l2 identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByL2ID)]
        public ActionResult GetAssetsByL2ID([DataSourceRequest]DataSourceRequest request, int? cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetNumber";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            string strWhere = "and l.L2ID = " + cityID;

            var result = new DataSourceResult()
            {
                Data = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result);
        }

        /// <summary>
        /// Checks the asset reference exist.
        /// </summary>
        /// <param name="assetID">The asset identifier.</param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        public ActionResult CheckAssetRefExist(int assetID)
        {
            using (ServiceContext context = new ServiceContext())
            {
                PmSchedule obj = new PmSchedule();
                obj.AssetID = assetID;
                int count = context.Search<PmSchedule>(obj).ToList().Count;
                return Json(count, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Updates the date.
        /// </summary>
        /// <param name="startMonth">The start month.</param>
        /// <param name="year">The year.</param>
        /// <param name="weekOfMonth">The week of month.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <param name="frequencyUnitsMonths">The frequency units months.</param>
        /// <param name="workPeriodDaysMonths">The work period days months.</param>
        /// <param name="lastGenDate">The last gen date.</param>
        /// <returns></returns>
        public JsonResult UpdateDate(int startMonth, int year, int weekOfMonth, int dayOfWeek, int frequencyUnitsMonths, int workPeriodDaysMonths, DateTime? lastGenDate)
        {
            try
            {
                zMonth = startMonth;
                zYear = year;
                int i = 1;
                DateTime textBoxTargetStartDateMonthFixedDay = DateTime.MinValue;
                DateTime textBoxNextStartDateFixedDay = DateTime.MinValue;
                DateTime textBoxTargetCompleteDateFixedDay = DateTime.MinValue;
                while (i < 3)
                {
                    switch (weekOfMonth)
                    {
                        case 1:
                            firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                            tempDay = firstDay;
                            GetPMDayStart(Microsoft.VisualBasic.DateAndTime.Weekday(firstDay), dayOfWeek);
                            break;

                        case 2:
                        case 3:
                        case 4:
                            firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                            tempDay = Microsoft.VisualBasic.DateAndTime.DateAdd("ww", weekOfMonth - 1, firstDay);
                            GetDayofPM(Microsoft.VisualBasic.DateAndTime.Weekday(tempDay), dayOfWeek);
                            break;

                        case 5:
                            lastDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth + 1, 0);
                            tempDay = lastDay;
                            GetPMDayLast(Microsoft.VisualBasic.DateAndTime.Weekday(lastDay), dayOfWeek);
                            break;
                    }

                    zMonth = zMonth + frequencyUnitsMonths;
                    if (zMonth > 12)
                    {
                        zYear = zYear + 1;
                        zMonth = zMonth - 12;
                    }

                    if (i == 1)
                    {
                        if (PMDate <= lastGenDate)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgTargetStartDateGreater });
                        }
                        else
                        {
                            textBoxTargetStartDateMonthFixedDay = PMDate;
                        }
                    }
                    else
                    {
                        textBoxNextStartDateFixedDay = PMDate;
                    }

                    i = i + 1;
                }

                if (textBoxTargetStartDateMonthFixedDay != DateTime.MinValue)
                {
                    textBoxTargetCompleteDateFixedDay = textBoxTargetStartDateMonthFixedDay.AddDays(workPeriodDaysMonths - 1);
                }

                DateTime? textBoxTargetStartDateMonthFixedDayTemp;
                if (textBoxTargetStartDateMonthFixedDay == DateTime.MinValue)
                {
                    textBoxTargetStartDateMonthFixedDayTemp = null;
                }
                else
                {
                    textBoxTargetStartDateMonthFixedDayTemp = textBoxTargetStartDateMonthFixedDay;
                }

                DateTime? textBoxNextStartDateFixedDayTemp;
                if (textBoxNextStartDateFixedDay == DateTime.MinValue)
                {
                    textBoxNextStartDateFixedDayTemp = null;
                }
                else
                {
                    textBoxNextStartDateFixedDayTemp = textBoxNextStartDateFixedDay;
                }

                DateTime? textBoxTargetCompleteDateFixedDayTemp = DateTime.MinValue;
                if (textBoxTargetCompleteDateFixedDay == DateTime.MinValue)
                {
                    textBoxTargetCompleteDateFixedDayTemp = null;
                }
                else
                {
                    textBoxTargetCompleteDateFixedDayTemp = textBoxTargetCompleteDateFixedDay;
                }

                return Json(new object[] { "TextBoxTargetStartDateMonthFixedDay", textBoxTargetStartDateMonthFixedDayTemp, "TextBoxNextStartDateFixedDay", textBoxNextStartDateFixedDayTemp, "TextBoxTargetCompleteDateFixedDay", textBoxTargetCompleteDateFixedDayTemp });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the pm day start.
        /// </summary>
        /// <param name="weeksDay">The weeks day.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns></returns>
        public EmptyResult GetPMDayStart(int weeksDay, int dayOfWeek)
        {
            if (weeksDay != dayOfWeek)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(dayOfWeek + (weeksDay < dayOfWeek ? 0 : 7));
            }
            else
            {
                PMDate = tempDay;
            }

            return new EmptyResult();
        }

        /// <summary>
        /// Gets the day of pm.
        /// </summary>
        /// <param name="weeksDay">The weeks day.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns></returns>
        public EmptyResult GetDayofPM(int weeksDay, int dayOfWeek)
        {
            if (weeksDay > dayOfWeek)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(dayOfWeek - (weeksDay > dayOfWeek ? 0 : 7));
            }
            else
            {
                if (weeksDay < dayOfWeek)
                {
                    PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(dayOfWeek - (weeksDay < dayOfWeek ? 0 : 7));
                }
                else
                {
                    PMDate = tempDay;
                }
            }

            return new EmptyResult();
        }

        /// <summary>
        /// Gets the pm day last.
        /// </summary>
        /// <param name="weeksDay">The weeks day.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns></returns>
        public EmptyResult GetPMDayLast(int weeksDay, int dayOfWeek)
        {
            if (weeksDay != dayOfWeek)
            {
                PMDate = tempDay.AddDays(weeksDay * (-1)).AddDays(dayOfWeek - (weeksDay > dayOfWeek ? 0 : 7));
            }
            else
            {
                PMDate = tempDay;
            }

            return new EmptyResult();
        }

        /// <summary>
        /// Updates the date type3.
        /// </summary>
        /// <param name="frequencyUnitDayByMonths">The frequency unit day by months.</param>
        /// <param name="targetStartDateByMonth">The target start date by month.</param>
        /// <returns></returns>
        public JsonResult UpdateDateType3(int frequencyUnitDayByMonths, DateTime? targetStartDateByMonth)
        {
            if (targetStartDateByMonth != null)
            {
                PMDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", frequencyUnitDayByMonths, targetStartDateByMonth);
                return Json(PMDate);
            }
            else
            {
                return Json(string.Empty);
            }
        }

        /// <summary>
        /// Updates the tar comp date.
        /// </summary>
        /// <param name="chkMonthly">if set to <c>true</c> [CHK monthly].</param>
        /// <param name="targetStartDateByMonth">The target start date by month.</param>
        /// <param name="workPeriodDaysByMonths">The work period days by months.</param>
        /// <returns></returns>
        public JsonResult UpdateTarCompDate(bool chkMonthly, DateTime? targetStartDateByMonth, int? workPeriodDaysByMonths)
        {
            if (targetStartDateByMonth != null)
            {
                DateTime newtargetStartDateByMonth = Convert.ToDateTime(targetStartDateByMonth);
                if (chkMonthly)
                {
                    PMDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", 1, newtargetStartDateByMonth).AddDays(-1);
                }
                else
                {
                    PMDate = newtargetStartDateByMonth.AddDays(Convert.ToDouble(workPeriodDaysByMonths) - 1);
                }

                return Json(PMDate);
            }
            else
            {
                return Json(string.Empty);
            }
        }

        /// <summary>
        /// Loads the generation date.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance division.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        public JsonResult LoadGenerationDate(int? cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            DateTime? lastGenDate;
            using (ServiceContext context = new ServiceContext())
            {
                PMGenDate obj = new PMGenDate();
                obj = context.SearchAll(obj).Where(x => x.L2ID == cityID && x.IsCleaningModule == false && (x.MaintdivId == maintenanceDiv || x.MaintdivId == 0) && (x.MaintdeptId == maintenanceDept || x.MaintdeptId == 0) && (x.MainSubDeptId == maintenanceSubDept || x.MainSubDeptId == 0)).OrderByDescending(x => x.ToDate).FirstOrDefault();
                if (obj != null)
                {
                    lastGenDate = Convert.ToDateTime(obj.DateGenerate);
                }
                else
                {
                    lastGenDate = null;
                }
            }

            return Json(lastGenDate == null ? string.Empty : Convert.ToString(lastGenDate), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Updates the start date.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="multiSeqNumber">The multiple sequence number.</param>
        /// <returns></returns>
        public JsonResult UpdateStartDate(int timeID, int multiSeqNumber)
        {
            DateTime? startAtDate = null;
            int seqNo = 0;
            PmSchedule objPmSchedule = new PmSchedule();
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    objPmSchedule = context.SelectObject<PmSchedule>(timeID);
                }

                if (objPmSchedule != null)
                {
                    if (objPmSchedule.TypePMgenID == 1 || objPmSchedule.TypePMgenID == 2)
                    {
                        if (objPmSchedule.TypePMgenID == 1)
                        {
                            ////Fixed Time - Scheduled
                            startAtDate = Convert.ToDateTime(objPmSchedule.TargetStartDate).AddDays(Convert.ToInt32(objPmSchedule.FreqUnits) * Convert.ToInt32(objPmSchedule.Frequency) * (multiSeqNumber - 1));
                        }
                        else if (objPmSchedule.TypePMgenID == 2)
                        {
                            ////Fixed Time - Actual  //Remaining Logic
                            startAtDate = Convert.ToDateTime(objPmSchedule.TargetStartDate).AddDays(Convert.ToInt32(objPmSchedule.FreqUnits) * Convert.ToInt32(objPmSchedule.Frequency) * (multiSeqNumber - 1));
                            //// StartAtDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", (Convert.ToInt32(objPmSchedule.FreqUnits) * Convert.ToInt32(objPmSchedule.Frequency) * (multiSeqNumber - 1)),objPmSchedule.NextDate);
                        }
                    }
                    else if (objPmSchedule.TypePMgenID == 3)
                    {
                        ////Fixed Day and Week
                        zMonth = Microsoft.VisualBasic.DateAndTime.Month(Convert.ToDateTime(objPmSchedule.TargetStartDate)) + (Convert.ToInt32(objPmSchedule.Frequency) * multiSeqNumber) - 1;
                        zYear = Microsoft.VisualBasic.DateAndTime.Year(Convert.ToDateTime(objPmSchedule.TargetStartDate));

                        if (zMonth > 12)
                        {
                            zYear = zYear + 1;
                            zMonth = zMonth - 12;
                        }

                        switch (objPmSchedule.WeekofMonth)
                        {
                            case 1:
                                firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                                tempDay = firstDay;
                                GetPMDayStart(Microsoft.VisualBasic.DateAndTime.Weekday(firstDay), Convert.ToInt32(objPmSchedule.DayofWeek));
                                break;

                            case 2:
                            case 3:
                            case 4:
                                firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                                tempDay = Microsoft.VisualBasic.DateAndTime.DateAdd("ww", Convert.ToInt32(objPmSchedule.WeekofMonth), firstDay);
                                GetDayofPM(Microsoft.VisualBasic.DateAndTime.Weekday(tempDay), Convert.ToInt32(objPmSchedule.DayofWeek));
                                break;

                            case 5:
                                lastDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth + 1, 0);
                                tempDay = lastDay;
                                GetPMDayLast(Microsoft.VisualBasic.DateAndTime.Weekday(lastDay), Convert.ToInt32(objPmSchedule.DayofWeek));
                                break;
                        }

                        startAtDate = PMDate;
                    }
                    else if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                    {
                        startAtDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", Convert.ToInt32(objPmSchedule.Frequency) * (multiSeqNumber - 1), objPmSchedule.TargetStartDate);
                    }

                    if (objPmSchedule.PMCounter == 1)
                    {
                        seqNo = multiSeqNumber;
                    }
                    else
                    {
                        seqNo = Convert.ToInt32(objPmSchedule.PMCounter) + (multiSeqNumber - 1);
                    }
                }

                return Json(new object[] { startAtDate, seqNo });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Sets the next start date.
        /// </summary>
        /// <param name="dateTimeTargetStartDate">The date time target start date.</param>
        /// <param name="days">The days.</param>
        /// <returns></returns>
        public JsonResult SetNextStartDate(DateTime? dateTimeTargetStartDate, int? days)
        {
            if (dateTimeTargetStartDate != null)
            {
                DateTime newdateTimeTargetStartDate = Convert.ToDateTime(dateTimeTargetStartDate);
                DateTime result = newdateTimeTargetStartDate.AddDays(Convert.ToDouble(days));

                return Json(result);
            }
            else
            {
                return Json(string.Empty);
            }
        }

        /// <summary>
        /// Sets the target complete date.
        /// </summary>
        /// <param name="dateTimeTargetStartDate">The date time target start date.</param>
        /// <param name="workPeriodDays">The work period days.</param>
        /// <returns></returns>
        public JsonResult SetTargetCompleteDate(DateTime? dateTimeTargetStartDate, int? days)
        {
            if (dateTimeTargetStartDate != null)
            {
                DateTime newdateTimeTargetStartDate = Convert.ToDateTime(dateTimeTargetStartDate);
                DateTime result = newdateTimeTargetStartDate.AddDays(Convert.ToDouble(days - 1));

                return Json(result);
            }
            else
            {
                return Json(string.Empty);
            }
        }

        /// <summary>
        /// Gets the frequency unit.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetFrequencyUnit()
        {
            List<SelectListItem> lst = Common.GetFrequencyUnit();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the week of month.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetWeekOfMonth()
        {
            List<SelectListItem> lst = Common.GetWeekOfMonth();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the day of week.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDayOfWeek()
        {
            List<SelectListItem> lst = Common.GetDayOfWeek();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the start months.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetStartMonths()
        {
            List<SelectListItem> lst = Common.GetStartMonths();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the year drop down.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetYearDropDown()
        {
            List<SelectListItem> lst = Common.GetYearDropDown();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Multiple checklist"

        /// <summary>
        /// _s the get multiple check list.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetMultipleCheckList)]
        public ActionResult _GetMultipleCheckList(int timeID)
        {
            try
            {
                MultiplePM objMultiplePM = new MultiplePM();
                objMultiplePM.PMID = timeID;

                using (ServiceContext context = new ServiceContext())
                {
                    PmSchedule objPmSchedule = context.SelectObject<PmSchedule>(timeID);
                    objMultiplePM.PMNo = objPmSchedule.PMNo;
                    objMultiplePM.PMName = objPmSchedule.PMName;
                    objMultiplePM.AltPMName = objPmSchedule.AltPMName;
                    objMultiplePM.TypePMgenID = Convert.ToInt32(objPmSchedule.TypePMgenID);
                }

                return PartialView(PartialViews._MultipleCheckList, objMultiplePM);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the multiple pm list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="parameter">The parameter.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMultiplePMList)]
        [HttpPost]
        public ActionResult GetMultiplePMList([DataSourceRequest]DataSourceRequest request, int parameter)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ChecklistNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ////string strWhere = " and ass.locationId = " + locationID + " and ass.AssetId not in (select ChildId from assetRelationship) ";
            PartsList objPartsList = new PartsList();
            PMScheduleService obj = new PMScheduleService();

            var result = new DataSourceResult()
            {
                Data = obj.GetMultiPMList(parameter, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm plan list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="checklistNo">The checklist no.</param>
        /// <param name="multipleSeqNo">The multiple sequence no.</param>
        /// <param name="seqNo">The sequence no.</param>
        /// <param name="nextTask">The next task.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMPlanList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPMPlanList([DataSourceRequest]DataSourceRequest request, int timeID, string checklistNo, int multipleSeqNo, int seqNo, string nextTask)
        {
            string pmchecklistNo = string.Empty;
            PmSchedule objPmSchedule = new PmSchedule();
            PMScheduleService objService = new PMScheduleService();

            using (ServiceContext context = new ServiceContext())
            {
                objPmSchedule = context.SelectObject<PmSchedule>(timeID);

                if (objPmSchedule != null)
                {
                    if (Convert.ToInt32(objPmSchedule.ChecklistID) > 0)
                    {
                        pmchecklistNo = context.SelectObject<PMCheckList>(Convert.ToInt32(objPmSchedule.ChecklistID)).ChecklistNo;
                    }
                }
            }

            int count1, count3;
            DateTime date0, date1;

            ////TextBoxYearForViewPM.Text                      
            int intYear = 1;

            count1 = 1;
            count3 = Convert.ToInt32(objPmSchedule.PMCounter);

            date0 = Convert.ToDateTime(objPmSchedule.TargetStartDate);
            date1 = Convert.ToDateTime(objPmSchedule.TargetStartDate);

            pmft = new List<PmForecastTemp>();
            for (count1 = 1; count1 <= (365 * intYear) + 1; count1++)
            {
                PmForecastTemp objpmft = new PmForecastTemp();
                objpmft.PMStartDate = date0;
                date0 = Microsoft.VisualBasic.DateAndTime.DateAdd("d", 1, date0);
                if (objpmft.PMStartDate == date1)
                {
                    objpmft.PMCount = count3;
                    objpmft.PmTarDate1 = date1;
                    if (objPmSchedule.TypePMgenID == 3)
                    {
                        zMonth = Microsoft.VisualBasic.DateAndTime.Month(date1) + Convert.ToInt32(objPmSchedule.Frequency);
                        zYear = Microsoft.VisualBasic.DateAndTime.Year(date1);
                        Dayno = Convert.ToInt32(objPmSchedule.DayofWeek);
                        GetPmDate(Convert.ToInt32(objPmSchedule.WeekofMonth) - 1, Convert.ToInt32(objPmSchedule.DayofWeek));
                        date1 = PMDate;
                    }
                    else
                    {
                        if (objPmSchedule.TypePMgenID == 4 || objPmSchedule.TypePMgenID == 5)
                        {
                            PMDate = Microsoft.VisualBasic.DateAndTime.DateAdd("m", Convert.ToInt32(objPmSchedule.Frequency), date1);
                            date1 = PMDate;
                        }
                        else
                        {
                            date1 = Microsoft.VisualBasic.DateAndTime.DateAdd("d", Convert.ToInt32(objPmSchedule.FreqUnits) * Convert.ToInt32(objPmSchedule.Frequency), objpmft.PmTarDate1);
                        }
                    }

                    objpmft.PmTask1 = pmchecklistNo;
                    count3 = count3 + 1;
                }

                pmft.Add(objpmft);
            }

            List<MultiplePM> lstMultiplePM = objService.GetMultiPMList(timeID, 1, "ChecklistNo", "Ascending").ToList();
            if (lstMultiplePM.Count > 0)
            {
                for (int i = 0; i < lstMultiplePM.Count(); i++)
                {
                    Tskno = Convert.ToInt32(lstMultiplePM[i].MTaskSeq);
                    taskno = lstMultiplePM[i].CheckListNo;

                    if (objPmSchedule.PMCounter == 1 || lstMultiplePM[i].MScount == 0)
                    {
                        msdseq = Convert.ToInt32(lstMultiplePM[i].MStartDateCount);
                    }
                    else
                    {
                        msdseq = Convert.ToInt32(lstMultiplePM[i].MScount);
                    }

                    jbseq = lstMultiplePM[i].MSeq;
                    StoreFore();
                }

                if (!string.IsNullOrEmpty(nextTask))
                {
                    Tskno = lstMultiplePM.Count + 2;
                    taskno = nextTask;
                    jbseq = multipleSeqNo;
                    msdseq = seqNo;
                    StoreFore();
                }
            }

            newpmft = new List<PmForecastTemp>();
            for (count1 = 0; count1 <= 365 * 1; count1++)
            {
                PmForecastTemp itmx = new PmForecastTemp();
                itmx.PMStartDate = pmft[count1].PMStartDate;

                if (pmft[count1].PMCount != 0)
                {
                    itmx.PMCount = pmft[count1].PMCount;
                }

                if (pmft[count1].PmTask1 != null)
                {
                    itmx.PmTask1 = Convert.ToString(pmft[count1].PmTask1);
                }

                if (pmft[count1].PmTask2 != null)
                {
                    itmx.PmTask2 = Convert.ToString(pmft[count1].PmTask2);
                }

                if (pmft[count1].PmTask3 != null)
                {
                    itmx.PmTask3 = Convert.ToString(pmft[count1].PmTask3);
                }

                if (pmft[count1].PmTask4 != null)
                {
                    itmx.PmTask4 = Convert.ToString(pmft[count1].PmTask4);
                }

                if (pmft[count1].PmTask5 != null)
                {
                    itmx.PmTask5 = Convert.ToString(pmft[count1].PmTask5);
                }

                if (pmft[count1].PmTask6 != null)
                {
                    itmx.PmTask6 = Convert.ToString(pmft[count1].PmTask6);
                }

                if (pmft[count1].PmTask7 != null)
                {
                    itmx.PmTask7 = Convert.ToString(pmft[count1].PmTask7);
                }

                newpmft.Add(itmx);
            }

            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PMStartDate";
                sortDirection = "Ascending";
            }

            if (sortDirection == System.ComponentModel.ListSortDirection.Ascending.ToString())
            {
                if (sortExpression == "PMStartDate")
                {
                    newpmft = newpmft.OrderBy(x => x.PMStartDate).ToList();
                }
                else if (sortExpression == "PMCount")
                {
                    newpmft = newpmft.OrderBy(x => x.PMCount).ToList();
                }
                else if (sortExpression == "PmTask1")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask1).ToList();
                }
                else if (sortExpression == "PmTask2")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask2).ToList();
                }
                else if (sortExpression == "PmTask3")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask3).ToList();
                }
                else if (sortExpression == "PmTask4")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask4).ToList();
                }
                else if (sortExpression == "PmTask5")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask5).ToList();
                }
                else if (sortExpression == "PmTask6")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask6).ToList();
                }
                else if (sortExpression == "PmTask7")
                {
                    newpmft = newpmft.OrderBy(x => x.PmTask7).ToList();
                }
            }
            else
            {
                if (sortExpression == "PMStartDate")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PMStartDate).ToList();
                }
                else if (sortExpression == "PMCount")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PMCount).ToList();
                }
                else if (sortExpression == "PmTask1")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask1).ToList();
                }
                else if (sortExpression == "PmTask2")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask2).ToList();
                }
                else if (sortExpression == "PmTask3")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask3).ToList();
                }
                else if (sortExpression == "PmTask4")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask4).ToList();
                }
                else if (sortExpression == "PmTask5")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask5).ToList();
                }
                else if (sortExpression == "PmTask6")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask6).ToList();
                }
                else if (sortExpression == "PmTask7")
                {
                    newpmft = newpmft.OrderByDescending(x => x.PmTask7).ToList();
                }
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            List<PmForecastTemp> lst = objService.ApplyPagging(newpmft, pageNumber).ToList();

            var result = new DataSourceResult()
            {
                Data = lst,
                Total = newpmft.Count()
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the pm date.
        /// </summary>
        /// <param name="cboWeeks">The weeks.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns></returns>
        public EmptyResult GetPmDate(int cboWeeks, int dayOfWeek)
        {
            try
            {
                if (zMonth > 12)
                {
                    zYear = zYear + 1;
                    zMonth = zMonth - 12;
                }

                switch (cboWeeks)
                {
                    case 0:
                        firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                        tempDay = firstDay;
                        GetPMDayStart(Microsoft.VisualBasic.DateAndTime.Weekday(firstDay), dayOfWeek);
                        break;
                    case 1:
                    case 2:
                    case 3:
                        firstDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth, 1);
                        tempDay = Microsoft.VisualBasic.DateAndTime.DateAdd("ww", cboWeeks, firstDay);
                        GetDayofPM(Microsoft.VisualBasic.DateAndTime.Weekday(tempDay), dayOfWeek);
                        break;
                    case 4:
                        lastDay = Microsoft.VisualBasic.DateAndTime.DateSerial(zYear, zMonth + 1, 0);
                        tempDay = lastDay;
                        GetPMDayLast(Microsoft.VisualBasic.DateAndTime.Weekday(lastDay), dayOfWeek);
                        break;
                }

                return new EmptyResult();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Stores the fore.
        /// </summary>
        /// <returns></returns>
        public EmptyResult StoreFore()
        {
            for (int i = 0; i <= 365 * 1; i++)
            {
                if (pmft[i].PMCount == msdseq)
                {
                    if (Tskno == 2)
                    {
                        pmft[i].PmTask2 = taskno;
                    }
                    else if (Tskno == 3)
                    {
                        pmft[i].PmTask3 = taskno;
                    }
                    else if (Tskno == 4)
                    {
                        pmft[i].PmTask4 = taskno;
                    }
                    else if (Tskno == 5)
                    {
                        pmft[i].PmTask5 = taskno;
                    }
                    else if (Tskno == 6)
                    {
                        pmft[i].PmTask6 = taskno;
                    }
                    else if (Tskno == 7)
                    {
                        pmft[i].PmTask7 = taskno;
                    }

                    msdseq = msdseq + jbseq;
                }
            }

            return new EmptyResult();
        }

        /// <summary>
        /// Manages the multiple pm.
        /// </summary>
        /// <param name="objMultiplePM">The object multiple pm.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMultiplePM)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageMultiplePM(MultiplePM objMultiplePM)
        {
            PMScheduleService objService = new PMScheduleService();
            List<MultiplePM> lstMultiplePM = new List<MultiplePM>();
            string validations = string.Empty;
            try
            {
                if (ModelState.IsValid)
                {
                    lstMultiplePM = objService.GetMultiPMList(objMultiplePM.PMID, 1, "ChecklistNo", "Ascending").ToList();
                    if (lstMultiplePM.Count > 0)
                    {
                        int max = lstMultiplePM.Select(x => x.MSeq).Max();
                        if (objMultiplePM.MSeq <= max)
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgSeqNoGreaterthanLastSeqNo });
                        }
                    }

                    objMultiplePM.MPMCounter = 1;
                    objMultiplePM.MTaskSeq = lstMultiplePM.Count() + 2;
                    objMultiplePM.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objMultiplePM.CreatedDate = DateTime.Now;

                    bool result = PMScheduleService.ManageMultiplePM(objMultiplePM);

                    if (result)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the multiple pm.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMultiplePM)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMultiplePM(int timeID)
        {
            bool result = PMScheduleService.DeleteMultiplePM(timeID);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Assign To"
        /// <summary>
        /// _s the pm assign to.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._PMAssignTo)]
        public ActionResult _PMAssignTo(int timeID)
        {
            try
            {
                PmSchedule_AssignTo objPMAssignTo = new PmSchedule_AssignTo();
                objPMAssignTo.PMID = timeID;

                using (ServiceContext context = new ServiceContext())
                {
                    PmSchedule objPmSchedule = context.SelectObject<PmSchedule>(timeID);
                    objPMAssignTo.PMNo = objPmSchedule.PMNo;
                    objPMAssignTo.PMName = objPmSchedule.PMName;
                    objPMAssignTo.AltPMName = objPmSchedule.AltPMName;
                    objPMAssignTo.IsCleaning = objPmSchedule.IsCleaningModule;
                }

                return PartialView(PartialViews._PMAssignTo, objPMAssignTo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the pm assign to list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="timeID">The time identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMAssignToList)]
        [HttpPost]
        public ActionResult GetPMAssignToList([DataSourceRequest]DataSourceRequest request, int timeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PMID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMScheduleService obj = new PMScheduleService();

            var result = new DataSourceResult()
            {
                Data = obj.GetPMAssignList(timeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the pm assign to employee group.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="groupID">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMAssignToEmployeeGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMAssignToEmployeeGroup(int timeID, int groupID)
        {
            try
            {
                PMScheduleService.ManagePMAssignToEmployeeGroup(timeID, groupID);
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the pm assign to employee.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMAssignToEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePMAssignToEmployee(int timeID, int employeeID)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    PmSchedule_AssignTo obj = new PmSchedule_AssignTo();
                    obj.PMID = timeID;
                    obj.EmployeeID = employeeID;
                    int count = context.Search<PmSchedule_AssignTo>(obj).ToList().Count();
                    if (count > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PM_MsgEmployeeAlreadyExists });
                    }
                    else
                    {
                        PMScheduleService.ManagePMAssignToEmployee(timeID, employeeID);
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the pm schedule assign to.
        /// </summary>
        /// <param name="timeID">The time identifier.</param>
        /// <param name="employeeID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePmScheduleAssignto)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePmScheduleAssignto(int timeID, int employeeID)
        {
            bool result = PMScheduleService.DeletePmScheduleAssignto(timeID, employeeID);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}