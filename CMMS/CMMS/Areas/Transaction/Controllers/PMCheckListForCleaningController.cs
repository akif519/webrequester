﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// PM Check List For Cleaning sController
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "PM CheckList"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMCheckListForCleaning)]
        public ActionResult PMCheckListForCleaning()
        {
            if (ProjectSession.PermissionAccess.Cleaning_CleaningChecklist_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.PMCheckListForCleaning);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the pm checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistForCleaningPage)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetPMChecklistForCleaningPage([DataSourceRequest]DataSourceRequest request, string strWhere)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ChecklistNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PMCheckList objPmCheckList = new PMCheckList();

            string strWhereClauseWithEmployee = string.Empty;

            int employeeID = ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID;
            if (employeeID > 0)
            {
                strWhereClauseWithEmployee = "  and ( pmchecklist.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) OR pmchecklist.L2ID IS NULL) ";
            }

            PMCheckListForCleaningService objService = new PMCheckListForCleaningService();

            var result = new DataSourceResult()
            {
                Data = objService.GetPMChecklistPageAdvancedSearch(objPmCheckList, SystemEnum.Pages.PMCheckListForCleaning.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhereClauseWithEmployee, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Deletes the pm checklist for cleaning.
        /// </summary>
        /// <param name="checklistid">The checklist id.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePMChecklistForCleaning)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePMChecklistForCleaning(int checklistid)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<PMCheckList>(checklistid, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the pm checklist detail for cleaning.
        /// </summary>
        /// <param name="checklistID">The checklist identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistDetailForCleaning)]
        [HttpGet]
        public ActionResult GetPMChecklistDetailForCleaning(int checklistID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.Cleaning_CleaningChecklist_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                PMCheckList objPMcheckList = new PMCheckList();
                if (checklistID > 0)
                {
                    objPMcheckList = PMCheckListForCleaningService.GetPMChecklistForCleaningByID(checklistID);
                }

                return View(Views.PMCheckListForCleaningDetails, objPMcheckList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Manages the pm checklist for cleaning.
        /// </summary>
        /// <param name="objCheckList">The object check list.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePMChecklistForCleaning)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePMChecklistForCleaning(PMCheckList objCheckList)
        {
            string validations = string.Empty;
            int newCheckListID = objCheckList.ChecklistID;

            if (objCheckList.EstimatedLaborHours == null || objCheckList.EstimatedLaborHours <= 0)
            {
                objCheckList.EstimatedLaborHours = 0;
            }

            try
            {
                HttpPostedFileBase file1;
                if (Request.Files.Count > 0)
                {
                    file1 = Request.Files[0];
                    string fileName1 = Path.GetFileName(file1.FileName);

                    if (!("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(fileName1) + ","))
                    {
                        ////return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed }, JsonRequestBehavior.AllowGet);

                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                        if (objCheckList.ChecklistID > 0)
                        {
                            ExtAssetFile objExtAssetFile = new ExtAssetFile();
                            objExtAssetFile.FkId = Convert.ToString(objCheckList.ChecklistID);
                            objExtAssetFile.ModuleType = "P";
                            using (ServiceContext context = new ServiceContext())
                            {
                                objExtAssetFile = context.Search<ExtAssetFile>(objExtAssetFile).FirstOrDefault();
                                objCheckList.FileName = objExtAssetFile.FileName;
                            }
                        }

                        return View(Views.PMCheckListForCleaningDetails, objCheckList);
                    }
                }

                objCheckList.IsCleaningModule = true;
                if (objCheckList.ChecklistID > 0)
                {
                    objCheckList.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objCheckList.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCheckList.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objCheckList.CreatedDate = DateTime.Now;
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        PMCheckListService objCheckListService = new PMCheckListService();
                        var lstchkli = objCheckListService.checkDuplicatedforCheckListNo(objCheckList.ChecklistID, objCheckList.ChecklistNo);
                        if (lstchkli.Count > 0)
                        {
                            if (objCheckList.ChecklistID > 0)
                            {
                                TempData["Message"] = ProjectSession.Resources.label.PMChecklist_MsgPMChecklistNoAlreadyExists;
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                                return RedirectToAction(Actions.GetPMChecklistDetailForCleaning, Pages.Controllers.Transaction, new { checklistID = newCheckListID });
                            }
                            else
                            {
                                ViewBag.Message = ProjectSession.Resources.label.PMChecklist_MsgPMChecklistNoAlreadyExists;
                                ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                                return View(Views.PMCheckListForCleaningDetails, objCheckList);
                            }
                        }

                        newCheckListID = context.Save(objCheckList);
                        if (newCheckListID > 0)
                        {
                            //// Code Related To File(Attachment) Upload 
                            ExtAssetFile obj = new ExtAssetFile();
                            HttpPostedFileBase file;
                            if (Request.Files.Count > 0)
                            {
                                file = Request.Files[0];
                                string subPath = string.Empty;
                                string filePath = string.Empty;

                                ExtAssetFile objExtAssetFile = new ExtAssetFile();
                                ////List<ExtAssetFile> lst = context.SearchAll(objExtAssetFile).Where(x => Convert.ToInt32(x.FkId) == objCheckList.ChecklistID && x.AutoId != objCheckList.AutoId && x.FileName == file.FileName).ToList();
                                ////if (lst.Count > 0)
                                ////{
                                ////    TempData["Message"] = ProjectSession.Resources.message.Common_MsgFileExists;
                                ////    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                                ////    return RedirectToAction(Actions.GetPMChecklistDetailForCleaning, Pages.Controllers.Transaction, new { checklistID = newCheckListID });
                                ////}

                                //// Remove Old Attachments if already exists
                                if (objCheckList.AutoId > 0)
                                {
                                    int result = context.Delete<ExtAssetFile>(objCheckList.AutoId);
                                }

                                obj.ModuleType = SystemEnum.DocumentModuleType.P.ToString();
                                subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathPreventive + obj.FkId;

                                bool folderExists = Directory.Exists(subPath);
                                if (!folderExists)
                                {
                                    Directory.CreateDirectory(subPath);
                                }

                                filePath = subPath + "\\" + Path.GetFileName(file.FileName);
                                if (System.IO.File.Exists(filePath))
                                {
                                    System.IO.File.Delete(filePath);
                                }

                                file.SaveAs(filePath);

                                obj.FileDescription = objCheckList.FileDescription;
                                obj.CreatedBy = ProjectSession.EmployeeID;
                                obj.CreatedDate = DateTime.Now;
                                obj.ModifiedBy = ProjectSession.EmployeeID;
                                obj.ModifiedDate = DateTime.Now;
                                obj.FileLink = ProjectConfiguration.UploadPathPreventive + obj.FkId + "\\" + Path.GetFileName(file.FileName);
                                obj.FileName = Path.GetFileName(file.FileName);
                                obj.FkId = Convert.ToString(newCheckListID);
                                context.Save(obj);
                            }
                            else
                            {
                                //// for update Desc
                                if (objCheckList.AutoId > 0)
                                {
                                    obj = context.SelectObject<ExtAssetFile>(objCheckList.AutoId);
                                    obj.FileDescription = objCheckList.FileDescription;
                                    obj.ModifiedBy = ProjectSession.EmployeeID;
                                    obj.ModifiedDate = DateTime.Now;
                                    context.Save(obj);
                                }
                            }

                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();

                            return View(Views.PMCheckListForCleaningDetails, objCheckList);
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        ViewBag.Message = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        ViewBag.Message = validations.TrimEnd(',');
                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                    }

                    return View(Views.PMCheckListForCleaningDetails, objCheckList);
                }

                return RedirectToAction(Actions.GetPMChecklistDetailForCleaning, Pages.Controllers.Transaction, new { checklistID = newCheckListID });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "For Check List Popup"
        /// <summary>
        /// Gets the pm checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningCheckList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetCleaningCheckList([DataSourceRequest]DataSourceRequest request, string strWhere)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                PMCheckListForCleaningService objService = new PMCheckListForCleaningService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetCleaningChecklistPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}