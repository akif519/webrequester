﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.ObjectModel;
using System.Data;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.DAL;
using CMMS.Reports;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Purchase Request Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Purchases the request list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchaseRequestList)]
        public ActionResult PurchaseRequestList()
        {
            if (ProjectSession.PermissionAccess.Purchasing_PurchaseRequest_Allowaccess && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.PurchaseRequestList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchase order.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseRequest)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseRequest([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, PurchaseRequestID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            bool IsClosedFilterExist = checkFilerExist(request.Filters, "Closed");
            bool IsCancelledFilterExist = checkFilerExist(request.Filters, "Cancelled");
            bool IsFullyOrderedFilterExist = checkFilerExist(request.Filters, "Fully Ordered");
            
            if (!IsClosedFilterExist)
                whereClause = whereClause + " and purchaserequest.status_id <> 2 ";

            if (!IsCancelledFilterExist)
                whereClause = whereClause + " and purchaserequest.status_id <> 3 ";

            if (!IsFullyOrderedFilterExist)
                whereClause = whereClause + " and purchaserequest.status_id <> 5 ";
                        
            //whereClause = " and ((purchaserequest.status_id Is NULL Or purchaserequest.status_id <> 2) AND (purchaserequest.status_id <> 3 and purchaserequest.status_id <> 5))  ";

            if (!ProjectSession.IsCentral)
            {
                whereClause = " and purchaserequest.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            PurchaseRequest objPurchaseRequest = new PurchaseRequest();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPurchaseRequest, SystemEnum.Pages.PurchaseRequest.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Get Purchase Request
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetPurchaseRequestPage)]
        public ActionResult GetPurchaseRequestPage(int id = 0, string workOrderNo = "")
        {
            if (ProjectSession.PermissionAccess.Purchasing_PurchaseRequest_Allowaccess && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                PurchaseRequest objPurchaseRequest = new PurchaseRequest();
                objPurchaseRequest.Id = id;
                if (id > 0)
                {
                    objPurchaseRequest = PurchaseService.GetPurchaseRequestDetail(id);
                    objPurchaseRequest.IsAnyPRItemRaisedToPO = PurchaseService.IsAnyPRItemRaisedToPO(id);
                }
                else
                {
                    if (!string.IsNullOrEmpty(workOrderNo))
                    {
                        objPurchaseRequest.workOrderNo = workOrderNo;
                        using (ServiceContext context = new ServiceContext())
                        {
                            WorkOrder objWorkOrder = new WorkOrder();
                            objWorkOrder.WorkorderNo = workOrderNo;
                            objWorkOrder = context.Search<WorkOrder>(objWorkOrder).FirstOrDefault();

                            if (objWorkOrder != null)
                            {
                                objPurchaseRequest.L2ID = Convert.ToInt32(objWorkOrder.L2ID);
                                objPurchaseRequest.MaintdivId = objWorkOrder.MaintDivisionID;
                                objPurchaseRequest.MaintdeptId = objWorkOrder.MaintdeptID;
                                objPurchaseRequest.MainSubDeptId = objWorkOrder.MaintsubdeptID;
                                objPurchaseRequest.CreatedBy = ProjectSession.EmployeeID;
                                objPurchaseRequest.CreatedByEmployeeNO = ProjectSession.EmployeeNo;
                                objPurchaseRequest.CreatedByEmployeeName = ProjectSession.EmployeeName;
                                objPurchaseRequest.CreatedByAltEmployeeName = ProjectSession.EmployeeName;

                                if (Convert.ToInt32(objWorkOrder.RequestorID) > 0)
                                {
                                    employees objReq = context.SelectObject<employees>(Convert.ToInt32(objWorkOrder.RequestorID));
                                    objPurchaseRequest.RequestedBy = objReq.EmployeeID;
                                    objPurchaseRequest.RequestedByEmployeeNO = objReq.EmployeeNO;
                                    objPurchaseRequest.RequestedByEmployeeName = objReq.Name;
                                    objPurchaseRequest.RequestedByAltEmployeeName = objReq.AltName;
                                }
                            }
                        }
                    }
                    else
                    {
                        employees obj = new employees();
                        using (ServiceContext context = new ServiceContext())
                        {
                            obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                        }

                        objPurchaseRequest.L2ID = Convert.ToInt32(obj.L2ID);
                        objPurchaseRequest.MaintdivId = obj.MaintDivisionID;
                        objPurchaseRequest.MaintdeptId = obj.MaintDeptID;
                        objPurchaseRequest.MainSubDeptId = obj.MaintSubDeptID;

                        objPurchaseRequest.CreatedBy = ProjectSession.EmployeeID;
                        objPurchaseRequest.CreatedByEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseRequest.CreatedByEmployeeName = ProjectSession.EmployeeName;
                        objPurchaseRequest.CreatedByAltEmployeeName = ProjectSession.EmployeeName;

                        objPurchaseRequest.RequestedBy = ProjectSession.EmployeeID;
                        objPurchaseRequest.RequestedByEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseRequest.RequestedByEmployeeName = ProjectSession.EmployeeName;
                        objPurchaseRequest.RequestedByAltEmployeeName = ProjectSession.EmployeeName;
                    }
                }

                return View(Views.PurchaseRequestDetails, objPurchaseRequest);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchase request items by Purchase Request id.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="purchaseRequestID">The purchase request identifier.</param>
        /// <param name="dbcall">if set to <c>true</c> [database call].</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseRequestItemsByPRID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseRequestItemsByPRID([DataSourceRequest]DataSourceRequest request, int purchaseRequestID, bool dbcall = false)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "id";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string whereClause = string.Empty;
            whereClause = " and PurchaseRequestID = " + purchaseRequestID;

            List<PurchaseRequestItem> lstPurchaseRequestItems = new List<PurchaseRequestItem>();
            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult();
            if (dbcall)
            {
                result = new DataSourceResult()
                {
                    Data = objService.GetPurchaseRequestItemsByPRID(whereClause, pageNumber, sortExpression, sortDirection),
                    Total = objService.PagingInformation.TotalRecords
                };
            }
            else if (TempData["GetPurchaseRequestItemdata"] != null)
            {
                lstPurchaseRequestItems = (List<PurchaseRequestItem>)TempData["GetPurchaseRequestItemdata"];
                result = new DataSourceResult()
                {
                    Data = lstPurchaseRequestItems,
                    Total = 1
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Manages the purchase request.
        /// </summary>
        /// <param name="objPurchaseRequest">The object purchase request.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchaseRequest)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePurchaseRequest(PurchaseRequest objPurchaseRequest)
        {
            objPurchaseRequest.lstPurchaseRequestItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestItem>>(Convert.ToString(objPurchaseRequest.JsonlstItemModel));

            string validations = string.Empty;
            int newPurchaseRequestID = objPurchaseRequest.Id;
            try
            {
                if (objPurchaseRequest.Id > 0)
                {
                    objPurchaseRequest.CancelNotes = string.Empty;
                }
                else
                {
                    ////Add Mode
                    PurchaseRequest obj = new PurchaseRequest();
                    L2 objL2 = new L2();
                    using (DapperContext context = new DapperContext())
                    {
                        objPurchaseRequest.L2Code = context.SelectObject<L2>(Convert.ToInt32(objPurchaseRequest.L2ID)).L2Code;

                        int year = DateTime.Today.Year;
                        int count = (from pr in context.SearchAll<PurchaseRequest>(obj) where Convert.ToDateTime(pr.CreatedDate).Year == year select pr).Count();
                        objPurchaseRequest.PurchaseRequestID = "PR" + objPurchaseRequest.L2Code + (year % 100).ToString() + (count + 1).ToString().PadLeft(5, '0');
                    }

                    objPurchaseRequest.OrderStatus = "None";
                    objPurchaseRequest.CreatedDate = DateTime.Now;
                    objPurchaseRequest.Status_id = 1; ////Open
                    objPurchaseRequest.Auth_status = 4; ////New
                }

                if (ModelState.IsValid)
                {
                    using (DapperContext context = new DapperContext())
                    {
                        newPurchaseRequestID = context.Save(objPurchaseRequest);                        
                        if (newPurchaseRequestID > 0)
                        {
                            JobOrderService objService = new JobOrderService();
                            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objPurchaseRequest.workOrderNo, ProjectSession.EmployeeID.ToString());

                            objPurchaseRequest.Id = newPurchaseRequestID;
                            PurchaseService.NewPurchaseRequestItemDetails(objPurchaseRequest);

                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Purchasing_MsgPurchaseRequestAlreadyExists;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                    else
                    {
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                }

                if (string.IsNullOrEmpty(objPurchaseRequest.workOrderNo))
                {
                    return RedirectToAction(Actions.GetPurchaseRequestPage, Pages.Controllers.Transaction, new { id = newPurchaseRequestID });
                }
                else
                {
                    return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { workOrderNo = objPurchaseRequest.workOrderNo, from = "PR" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Manages the purchase request items.
        /// </summary>
        /// <param name="objModel">The object model.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchaseRequestItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManagePurchaseRequestItems(PurchaseRequestItem objModel, string kendoItemgriddata)
        {
            if (objModel != null)
            {
                decimal tempValue = new decimal();
                objModel.Quantity = objModel.Quantity == null || objModel.Quantity == 0 ? tempValue : objModel.Quantity;
            }

            List<PurchaseRequestItem> lstPurchaseRequestItem = new List<PurchaseRequestItem>();
            lstPurchaseRequestItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestItem>>(kendoItemgriddata);

            if (objModel.Guid_ItemID == new Guid())
            {
                objModel.Guid_ItemID = Guid.NewGuid();
                lstPurchaseRequestItem.Add(objModel);
            }
            else
            {
                PurchaseRequestItem editPurchaseRequestItem = lstPurchaseRequestItem.FirstOrDefault(l => l.Guid_ItemID == objModel.Guid_ItemID);
                objModel.ID = editPurchaseRequestItem.ID; // add for edit
                int editIndex = lstPurchaseRequestItem.IndexOf(editPurchaseRequestItem);

                lstPurchaseRequestItem.RemoveAt(editIndex);
                lstPurchaseRequestItem.Insert(editIndex, objModel);
            }

            TempData["GetPurchaseRequestItemdata"] = lstPurchaseRequestItem;
            return Json(lstPurchaseRequestItem);
        }

        /// <summary>
        /// Gets the purchase request item details.
        /// </summary>
        /// <param name="autoGuid">The automatic unique identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetPurchaseRequestItemDetails(string autoGuid, string kendogriddata)
        {
            List<PurchaseRequestItem> lstPurchaseRequestItem = new List<PurchaseRequestItem>();
            lstPurchaseRequestItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestItem>>(kendogriddata);
            PurchaseRequestItem model = new PurchaseRequestItem();
            if (!string.IsNullOrEmpty(autoGuid))
            {
                Guid id = new Guid(autoGuid);
                model = lstPurchaseRequestItem.FirstOrDefault(l => l.Guid_ItemID == id);
            }

            TempData["GetPurchaseRequestItemdata"] = lstPurchaseRequestItem;
            return Json(model);
        }

        /// <summary>
        /// Deletes the purchase request items.
        /// </summary>
        /// <param name="guid_ItemID">The guid_ item identifier.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchaseRequestItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchaseRequestItems(string guid_ItemID, string kendoItemgriddata)
        {
            try
            {
                Guid guid_ItemID1 = new Guid(guid_ItemID);
                List<PurchaseRequestItem> lstPurchaseRequestItem = new List<PurchaseRequestItem>();
                lstPurchaseRequestItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseRequestItem>>(kendoItemgriddata);

                PurchaseRequestItem editPurchaseRequestItem = lstPurchaseRequestItem.FirstOrDefault(l => l.Guid_ItemID == guid_ItemID1);

                int editIndex = lstPurchaseRequestItem.IndexOf(editPurchaseRequestItem);
                lstPurchaseRequestItem.RemoveAt(editIndex);
                TempData["GetPurchaseRequestItemdata"] = lstPurchaseRequestItem;

                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the approve identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetApprovePRByprID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetApprovePRByprID([DataSourceRequest]DataSourceRequest request, int id)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PRAuthId";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PurchaseService objService = new PurchaseService();
            var result = new DataSourceResult()
            {
                Data = objService.GetApprovePRBypr_ID(pageNumber, sortExpression, sortDirection, id, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the purchase request approval levels by l2 i purchase request identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRApprovalLevelsByL2IDPRID)]
        public ActionResult GetPRApprovalLevelsByL2IDPRID(int cityId, int id)
        {
            var result = PurchaseService.GetPRApprovalLevelsByL2ID_pr_ID(cityId, id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the employees by purchase request  approval level identifier.
        /// </summary>
        /// <param name="cityId">The city identifier.</param>
        /// <param name="approvalLevelId">The approval level identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesByPRApprovalLevelId)]
        public ActionResult GetEmployeesByPRApprovalLevelId(int cityId, int approvalLevelId)
        {
            using (ServiceContext context = new ServiceContext())
            {
                approvalLevelId = context.SelectObject<PRApprovalLevelMapping>(approvalLevelId).PRApprovalLevelId;
            }

            var result = PurchaseService.GetEmployeesByPRApprovalLevelId_L2ID(cityId, approvalLevelId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// News the approve purchase request.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        [ActionName(Actions.NewApprovePR)]
        public JsonResult NewApprovePR(Purchase_req_auth obj)
        {
            string auth_status_desc = string.Empty;
            int auth_status_id = 0;
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    employees objemployees = new employees();
                    objemployees.EmployeeID = Convert.ToInt32(obj.EmployeeID);
                    objemployees.Password = obj.Password;
                    objemployees = context.Search<employees>(objemployees).FirstOrDefault();

                    if (objemployees != null)
                    {
                        obj.Auth_date = DateTime.Now;
                        int result = context.Save<Purchase_req_auth>(obj);

                        if (result > 0)
                        {
                            auth_status_id = context.SelectObject<PRApprovalLevelMapping>(obj.PRApprovalLevelMappingId).Auth_status_id;

                            PurchaseRequest objPurchaseRequest = context.SelectObject<PurchaseRequest>(Convert.ToInt32(obj.PurchaseRequestID));
                            objPurchaseRequest.Auth_status = auth_status_id;
                            int result1 = context.Save<PurchaseRequest>(objPurchaseRequest);

                            auth_status_desc = context.SelectObject<Pr_authorisation_statu>(auth_status_id).Auth_status_desc;
                            /*(Start)Change Work Order Status if item is Approved (By Pratik on 05-Dec-2016 for NTM-126)*/
                            if (auth_status_id == 3)
                            {
                                PurchaseRequestItem objPRItem = new PurchaseRequestItem();
                                objPRItem.PurchaseRequestID = obj.PurchaseRequestID == null ? 0 : Convert.ToInt32(obj.PurchaseRequestID);
                                IList<string> lstWorkOrderNos = context.Search<PurchaseRequestItem>(objPRItem).Where(x => x.JoborderID != null).Select(x => x.JoborderID).Distinct().ToList();
                                JobOrderService objService = new JobOrderService();
                                foreach (string workOrderNo in lstWorkOrderNos)
                                {
                                    if (objService.ChangeJobOrderStatusByWorkorderNo(workOrderNo, SystemEnum.WorkRequestStatus.WaitingforParts.GetHashCode(), -1, ProjectSession.EmployeeID.ToString()) == true)
                                    {
                                        objService.NewJOStatusAudit(workOrderNo, SystemEnum.WorkRequestStatus.WaitingforParts.GetHashCode(), ProjectSession.EmployeeID, "Purchasing Module");
                                    }
                                }
                            }

                            /*(End)Change Work Order Status if item is Approved (By Pratik on 05-Dec-2016 for NTM-126)*/
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), auth_status_desc, auth_status_id }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgValidPassword }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cancels the purchase request.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="cancelNote">The cancel note.</param>
        /// <returns></returns>
        [ActionName(Actions.CancelPurchaseRequest)]
        public JsonResult CancelPurchaseRequest(int id, string cancelNote)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    DateTime now = DateTime.Now;
                    PurchaseRequest objPurchaseRequest = context.SelectObject<PurchaseRequest>(id);
                    objPurchaseRequest.CancelNotes = cancelNote;
                    objPurchaseRequest.Cancel_by = ProjectSession.EmployeeID;
                    objPurchaseRequest.Cancel_date = now;
                    objPurchaseRequest.Status_id = 3; ////cancelled
                    int result1 = context.Save<PurchaseRequest>(objPurchaseRequest);

                    string status_desc = context.SelectObject<Purchase_req_statu>(3).Status_desc;
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), status_desc, ProjectSession.EmployeeName, now }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Res the open purchase request.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ReOpenPurchaseRequest)]
        public JsonResult ReOpenPurchaseRequest(int id)
        {
            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    PurchaseRequest objPurchaseRequest = context.SelectObject<PurchaseRequest>(id);
                    objPurchaseRequest.Status_id = 4; ////(Now reopened)
                    int result1 = context.Save<PurchaseRequest>(objPurchaseRequest);

                    string status_desc = context.SelectObject<Purchase_req_statu>(4).Status_desc;
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), status_desc }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region "document Tab"

        /// <summary>
        /// _s the get purchase request documents.
        /// </summary>
        /// <param name="requestID">The request identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._GetPRDocuments)]
        public ActionResult _GetPRDocuments(int requestID)
        {
            ExtAssetFile objExtAssetFile = new ExtAssetFile();
            ////Asset objAsset = new Asset();
            ////using (ServiceContext context = new ServiceContext())
            ////{
            ////    objAsset = context.SelectObject<Asset>(assetID);
            ////}

            objExtAssetFile.FkId = Convert.ToString(requestID);
            ////objExtAssetFile.AssetNumber = objAsset.AssetNumber;
            ////objExtAssetFile.AssetDescription = objAsset.AssetDescription;
            ////objExtAssetFile.AssetAltDescription = objAsset.AssetAltDescription;
            objExtAssetFile.ModuleType = SystemEnum.DocumentModuleType.PR.ToString();

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseRequest_Deleterecords)
            {
                objExtAssetFile.IsDeleteButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseRequest_Createnewrecords)
            {
                objExtAssetFile.IsSaveButtonEnable = false;
            }

            if (!ProjectSession.PermissionAccess.Purchasing_PurchaseRequest_Editrecords)
            {
                objExtAssetFile.IsEditButtonEnable = false;
            }

            return PartialView(PartialViews._DocumentList, objExtAssetFile);
        }
        #endregion


        #region "Print Report"

        /// <summary>
        /// Prints the purchase request.
        /// </summary>
        /// <param name="prid">The purchase request identifier.</param> 
        /// <returns></returns>
        [ActionName(Actions.PrintPurchaseRequest)]
        public ActionResult PrintPurchaseRequest(int prid)
        {
            PurchaseRequestPrint rpt = new PurchaseRequestPrint();

            //rpt.FindControl("lblReportHeader", false).Text = ProjectSession.Resources.label.Asset_TextBlockAssetList;

            bool lang = ProjectSession.Culture != ProjectConfiguration.ArabicCultureCode;
            if (!lang)
            {
                rpt.RightToLeft = DevExpress.XtraReports.UI.RightToLeft.Yes;
                rpt.RightToLeftLayout = DevExpress.XtraReports.UI.RightToLeftLayout.Yes;
            }
            rpt.Parameters["header"].Value = "Purchase Request";
            rpt.Parameters["l1Id"].Value = 1;
            rpt.Parameters["isCentral"].Value = ProjectSession.IsCentral;
            rpt.Parameters["lang"].Value = lang;
            rpt.Parameters["employeeId"].Value = ProjectSession.EmployeeID;
            rpt.Parameters["accountId"].Value = ProjectSession.AccountID;
            rpt.Parameters["l2Id"].Value = "1";
            rpt.Parameters["poid"].Value = prid;


            ViewData["Report"] = rpt;
            return View(Views.PrintView, rpt);

            //List<PrintPurchaseRequisitions> lstDetails = new List<PrintPurchaseRequisitions>();

            //lstDetails = PurchaseService.GetPurchaseRequestItemsByPRIDForReport(prid);

            //PurchaseRequisitionsReport report = new PurchaseRequisitionsReport();
            //report.DataSource = lstDetails;
            //DateTime date = new DateTime();

            //date = lstDetails.Select(a => a.CreatedDate).FirstOrDefault();

            //report.txtDate.Text = Common.GetEnglishDateOnly(date).ToString();
            //report.txtAltDate.Text = Common.GetArabicDateOnly(date).ToString();

            //report.txtProject.DataBindings.Add("Text", lstDetails, "L2Code");
            //report.txtCityPRNo.DataBindings.Add("Text", lstDetails, "PurchaseRequestID");
            //report.txtManitDivision.DataBindings.Add("Text", lstDetails, "MaintDivisionName");
            //report.txtAltManitDivision.DataBindings.Add("Text", lstDetails, "MaintDivisionAltName");
            //report.txtMaintDepart.DataBindings.Add("Text", lstDetails, "MaintDeptdesc");
            //report.txtAltMaintDepart.DataBindings.Add("Text", lstDetails, "MaintDeptAltdesc");

            //report.txtRowNo.DataBindings.Add("Text", lstDetails, "Rowno");
            //report.txtJobOrderNo.DataBindings.Add("Text", lstDetails, "WorkorderNo");
            //report.txtQty.DataBindings.Add("Text", lstDetails, "Quantity");
            //report.txtDiscription.DataBindings.Add("Text", lstDetails, "PartDescription");
            //report.txtItemNo.DataBindings.Add("Text", lstDetails, "StockNo");
            //report.txtUOM.DataBindings.Add("Text", lstDetails, "UoM");

            //report.txtprparedby.Text = ProjectSession.EmployeeName;

            //return View(Views.PrintView, report);
        }

        #endregion
		
    }
}