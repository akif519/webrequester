﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Model;
using Newtonsoft.Json;
using CMMS.Reports;
using CMMS.DAL;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Transaction Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        #region "Items Menu"
        #region "Items List"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.StoreItemsList)]
        public ActionResult StoreItemsList()
        {
            if (ProjectSession.PermissionAccess.Stores_ItemList_Allowaccess && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.ItemsList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Items List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="excludeStockId">The exclude stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStoreItemsList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetStoreItemsList([DataSourceRequest]DataSourceRequest request, int cityID, int subStoreID, int excludeStockId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "StockID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            if (cityID > 0 && subStoreID == 0)
            {
                whereClause = " and StockId in (Select Stock_Id From stockcode_levels l inner join substore s on l.sub_store_id = s.SubStoreID where s.L2Id=" + cityID.ToString() + ") ";
            }

            if (subStoreID > 0)
            {
                whereClause = " and StockId in (Select Stock_Id From stockcode_levels l where  l.sub_store_id = " + subStoreID.ToString() + ") ";
            }

            if (cityID == 0 && subStoreID == 0)
            {
                whereClause = string.Empty;
            }

            if (excludeStockId > 0)
            {
                whereClause += " and StockID Not in (" + excludeStockId.ToString() + ") ";
            }

            StockCode objStockCode = new StockCode();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objStockCode, SystemEnum.Pages.StockCodesList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Delete the item by id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteItemById)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteItemById(int id)
        {
            try
            {
                if (!ProjectSession.PermissionAccess.Stores_ItemList_Deleterecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDontPermissionToDelete }, JsonRequestBehavior.AllowGet);
                }

                int result = 0;
                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objContext.Delete<StockCode>(id, true);
                }

                if (result == 0)
                {
                    result = -1;
                    StockCodeService objService = new StockCodeService();
                    bool blnSuccess = objService.UpdateStockCodeCDCTableOnDeleteItem(id);

                    if (blnSuccess)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (result == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }

                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Items- Basic information tab"
        /// <summary>
        /// Get Stock Item Information by Stock Id
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemByStockID)]
        [HttpGet]
        public ActionResult GetItemByStockID(int id)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            StockCode objStockCode = new StockCode();
            Supplier objSupplier = new Supplier();
            objStockCode.StockID = id;
            using (ServiceContext objContext = new ServiceContext())
            {
                objStockCode = objContext.SelectObject<StockCode>(id);
                if (objStockCode.Default_supplier_id > 0)
                {
                    objSupplier = objContext.SelectObject<Supplier>(Convert.ToInt32(objStockCode.Default_supplier_id));

                    if (objSupplier != null && !string.IsNullOrEmpty(objSupplier.SupplierNo))
                    {
                        objStockCode.SupplierNo = objSupplier.SupplierNo;
                        objStockCode.SupplierName = objSupplier.SupplierName;
                        objStockCode.AltSupplierName = objSupplier.AltSupplierName;
                    }
                }
            }

            if (TempData["MessageType"] != null && TempData["MessageType"].ToString() != string.Empty && TempData["Message"] != null && TempData["Message"].ToString() != string.Empty)
            {
                ViewBag.MessageType = TempData["MessageType"].ToString();
                ViewBag.Message = TempData["Message"].ToString();
            }

            objStockCode.OldStockNo = objStockCode.StockNo;
            return View(Views.ItemDetail, objStockCode);
        }

        /// <summary>
        /// saves store item in database
        /// </summary>
        /// <param name="objStockCode">The object stock code.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveItem(StockCode objStockCode)
        {
            int result = 0;
            StockCode obj = new StockCode();
            bool blnValid = true;
            string validations = string.Empty;
            StockCodeService objservice = new StockCodeService();

            try
            {
                if (ModelState.IsValid)
                {
                    if (objservice.isStockNoDuplicate(objStockCode.StockNo, objStockCode.StockID))
                    {
                        blnValid = false;
                        return Json(new object[] { -1, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemNoAlreadyExists }, JsonRequestBehavior.AllowGet);
                    }

                    if (objStockCode.StockID > 0)
                    {
                        /*Edit Case*/
                        objStockCode.ModifiedBy = ProjectSession.EmployeeID.ToString();
                        objStockCode.ModifiedByName = ProjectSession.EmployeeName;
                        objStockCode.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        objStockCode.CreatedBy = ProjectSession.EmployeeID.ToString();
                        objStockCode.CreatedByName = ProjectSession.EmployeeName;
                        objStockCode.CreatedDate = DateTime.Now;
                        objStockCode.AvgPrice = objStockCode.Price2;
                    }

                    if (blnValid)
                    {
                        if (!ProjectSession.PermissionAccess.Stores_ItemList_ChangeMastersCodes && objStockCode.StockID > 0)
                        {
                            objStockCode.StockNo = null;

                            /*We Do not update null values to database that's why stockno is assigned null*/
                        }

                        using (DapperContext objContext = new DapperContext())
                        {
                            if (objStockCode.StockID > 0)
                            {
                                if (!ProjectSession.PermissionAccess.Stores_ItemList_Editrecords)
                                {
                                    return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                if (!ProjectSession.PermissionAccess.Stores_ItemList_Createnewrecords)
                                {
                                    return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            result = objContext.Save<StockCode>(objStockCode);
                            try
                            {
                                if (objStockCode.OldImagePath.Length > 0 && !string.IsNullOrEmpty(objStockCode.OldImagePath))
                                {
                                    IOManager.DeleteFile(ProjectConfiguration.UploadPath + "ItemImages\\" + Convert.ToString(objStockCode.OldImagePath).Trim());
                                }
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }

                if (result > 0)
                {
                    return Json(new object[] { result, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { objStockCode.StockID, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Last Stock Code No
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetLastStocCodeStoreNo)]
        [HttpGet]
        public JsonResult GetLastStocCodeStoreNo()
        {
            StockCodeService objService = new StockCodeService();

            try
            {
                List<string> lstNo = objService.GetLastStocCodeStoreNo();

                if (lstNo.Count > 1)
                {
                    return Json(new object[] { lstNo[0], lstNo[1] }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { string.Empty, string.Empty }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        ///  Changes the Item photo.
        /// </summary>
        /// <param name="cropPointX">The crop point x.</param>
        /// <param name="cropPointY">The crop point y.</param>
        /// <param name="imageCropWidth">Width of the image crop.</param>
        /// <param name="imageCropHeight">Height of the image crop.</param>
        /// <param name="oldFileName">Old File Name</param>
        /// <returns></returns>
        [ActionName(Actions.ChangeItemPhoto)]
        [HttpPost]
        public ActionResult ChangeItemPhoto(int? cropPointX, int? cropPointY, int? imageCropWidth, int? imageCropHeight, string oldFileName)
        {
            HttpPostedFileBase employeePhoto = Request.Files["UploadedFile"];
            int allowedMaxSizeinMB = 1;
            string[] allowedExt = new string[] { ".jpeg", ".jpg", ".gif", ".png", ".bmp", ".psd" };
            string[] allowedExtFromMaster = ProjectSession.AllowedFileTypes.ToLower().Split(',');
            int allowedMaxSizeinMBMaster = ProjectSession.AllowedMaxFilesize;
            if (employeePhoto.ContentLength > 0 && !allowedExt.Contains(employeePhoto.FileName.ToLower().Substring(employeePhoto.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }
            else if (!allowedExtFromMaster.Contains(employeePhoto.FileName.ToLower().Substring(employeePhoto.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }

            if ((employeePhoto.ContentLength / 1024) / 1024 > allowedMaxSizeinMB)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadSizeImage, string.Empty });
            }

            string photoName = employeePhoto.FileName;
            oldFileName = Path.GetFileName(oldFileName);
            string filename = string.Empty;
            byte[] imageBytes = ConvertTo.Bytes(employeePhoto);
            byte[] croppedImage = IOManager.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            try
            {
                if (photoName.Length > 0)
                {
                    filename = System.Guid.NewGuid().ToString() + Path.GetExtension(photoName);
                    filename = IOManager.SaveFile(croppedImage, ProjectConfiguration.UploadPath + "ItemImages\\", filename);
                }
            }
            catch
            {
            }

            return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Photo Uploaded Successfully.", filename });
        }

        #endregion

        #region "Items- Alternative Items tab"
        /// <summary>
        /// Partial view Alternative Items
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetAlternativeItems)]
        public ActionResult _PartialAlternativeItems(int id)
        {
            Stock_alternative obj = new Stock_alternative();
            obj.StockID = id;

            return View(PartialViews.AlterNativeItems, obj);
        }

        /// <summary>
        /// Deletes the Alternative Item by stock id.
        /// </summary>
        /// <param name="stockId">The stock identifier.</param>
        /// <param name="partId">The part identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAlternativeItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAlternativeItem(int stockId, int partId)
        {
            StockCodeService objService = new StockCodeService();
            Stock_alternative objAssigntoworkorder = new Stock_alternative();

            bool result = objService.DeleteAlternativeItembyStockId(stockId, partId);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the list of all alternative items by stock id.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="stockId">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAlternativeItemList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetAlternativeItemList([DataSourceRequest]DataSourceRequest request, int stockId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "sa.AlternativePart";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAlternativeItembyStockID(stockId, pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="stockCodeId">The stock code identifier.</param>
        /// <param name="altPartId">The alternative part identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveAlternativeItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveAlternativeItem(int stockCodeId, int altPartId)
        {
            string result = string.Empty;
            StockCodeService objService = new StockCodeService();
            try
            {
                if (stockCodeId > 0 && altPartId > 0)
                {
                    if (objService.SaveAlternativeItem(stockCodeId, altPartId))
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "stockCodeId or altPartId cannot be blank." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Items- Alternative Suppliers tab"
        /// <summary>
        /// Partial view Alternative Suppliers
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetAlternativeSuppliers)]
        public ActionResult _PartialAlternativeSuppliers(int id)
        {
            Stock_code_supplier obj = new Stock_code_supplier();
            obj.Stock_id = id;

            return View(PartialViews.AlterNativeSuppliers, obj);
        }

        /// <summary>
        /// Gets the list of all alternative suppliers by stock id.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="stockId">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAlternativeSuppliersList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetAlternativeSuppliersList([DataSourceRequest]DataSourceRequest request, int stockId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "s.SupplierNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAlternativeSuppliersByStockID(stockId, pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// saves work order
        /// </summary>
        /// <param name="stockCodeId">The stock code identifier.</param>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveAlternativeSupplier)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveAlternativeSupplier(int stockCodeId, int supplierID)
        {
            string result = string.Empty;
            StockCodeService objService = new StockCodeService();
            try
            {
                if (stockCodeId > 0 && supplierID > 0)
                {
                    if (objService.SaveAlternativeSupplier(stockCodeId, supplierID))
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "stockCodeId or supplierid cannot be blank." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the Alternative Item by stock id.
        /// </summary>
        /// <param name="stockId">The stock identifier.</param>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAlternativeSupplier)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAlternativeSupplier(int stockId, int supplierID)
        {
            StockCodeService objService = new StockCodeService();
            Stock_code_supplier objAssigntoworkorder = new Stock_code_supplier();

            bool result = objService.DeleteAlternativeSupplierbyStockId(stockId, supplierID);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Items - Attachment tab"

        /// <summary>
        /// To Get Items Documents
        /// </summary>
        /// <param name="id">The Stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._GetItemsDocuments)]
        public ActionResult _GetItemsDocuments(string id)
        {
            ExtAssetFile objExt = new ExtAssetFile();

            objExt.FkId = id;
            objExt.ModuleType = SystemEnum.DocumentModuleType.S.ToString();

            if (ProjectSession.PermissionAccess.Stores_ItemList_Editrecords == false)
            {
                objExt.IsSaveButtonEnable = false;
                objExt.IsDeleteButtonEnable = false;
            }
            else
            {
                objExt.IsSaveButtonEnable = true;
                objExt.IsDeleteButtonEnable = true;
            }

            return PartialView(PartialViews._DocumentList, objExt);
        }

        #endregion

        #region "Items - Stock Code Level"

        /// <summary>
        /// Gets the list of all alternative suppliers by stock id.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="stockId">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodeLevelByStockId)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetStockCodeLevelByStockId([DataSourceRequest]DataSourceRequest request, int stockId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "sl.createdDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetStockCodeLevelByStockId(stockId, pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Delete the stock code level by id
        /// </summary>
        /// <param name="stockId">The stock identifier.</param>
        /// <param name="subStoreId">The sub store identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteStockCodeLevel)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteStockCodeLevel(int stockId, int subStoreId)
        {
            try
            {
                if (!ProjectSession.PermissionAccess.Stores_ItemList_Deleterecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDontPermissionToDelete }, JsonRequestBehavior.AllowGet);
                }

                bool result = false;
                StockCodeService objService = new StockCodeService();
                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objService.DeleteStockCodeLevel(stockId, subStoreId);
                }

                if (result)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }

                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all Account Codes.
        /// </summary>
        /// <param name="subStoreId">The sub store identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPartLocationsBySubStore)]
        [HttpGet]
        public ActionResult GetPartLocationsBySubStore(int subStoreId)
        {
            PartLocations partLocation = new PartLocations();
            IList<PartLocations> lstPartLocations = new List<PartLocations>();

            if (subStoreId > 0)
            {
                partLocation.SubStoreID = subStoreId;
            }

            using (ServiceContext objService = new ServiceContext())
            {
                lstPartLocations = objService.Search<PartLocations>(partLocation);
            }

            return Json(lstPartLocations, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// saves Stock Code Level
        /// </summary>
        /// <param name="stockCodeLevel">The stock code level.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveStockCodeLevel)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveStockCodeLevel(Stockcode_level stockCodeLevel)
        {
            StockCodeService objService = new StockCodeService();
            Stockcode_level obj = new Stockcode_level();
            bool result = false;

            try
            {
                if (stockCodeLevel.Stock_id > 0 && stockCodeLevel.Sub_store_id > 0)
                {
                    obj.Stock_id = stockCodeLevel.Stock_id;
                    obj.Sub_store_id = stockCodeLevel.Sub_store_id;

                    using (ServiceContext objContext = new ServiceContext())
                    {
                        obj = objContext.Search<Stockcode_level>(obj).FirstOrDefault();
                    }

                    if (obj != null && obj.Stock_id > 0 && obj.Sub_store_id > 0 && obj.Max_level > 0 && obj.Re_order_level > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Store_MsgStoreAlreadyExists });
                    }
                    else
                    {
                        string[] arrValues = new string[5];
                        arrValues = GetStockBalBySubStore(Convert.ToInt32(stockCodeLevel.Stock_id), Convert.ToInt32(stockCodeLevel.Sub_store_id));
                        string stockBalance = string.Empty;

                        if (arrValues.Length > 0)
                        {
                            stockBalance = arrValues[0];
                            stockCodeLevel.Balance = Convert.ToDecimal(stockBalance == null ? "0" : stockBalance);
                        }

                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objService.SaveStockCodeLevel(stockCodeLevel);
                        }
                    }

                    if (result)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Item_MsgItemAlreadyExists });
                    }
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), "stockCode and substore must be greater than 0." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the list of all alternative suppliers by stock id.
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="stockId">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodeLevelCityByStockId)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetStockCodeLevelCityByStockId([DataSourceRequest]DataSourceRequest request, int stockId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "sl.createdDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCodeService objService = new StockCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetStockCodeLevelCityByStockId(stockId, pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }
        #endregion

        #endregion

        #region "Transaction Menu"

        #region "Transaction List"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetTransactionListPage)]
        public ActionResult GetTransactionListPage()
        {
            if (ProjectSession.PermissionAccess.Stores_Transaction_ViewTransactions && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.TransactionList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Transaction List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetTransactionList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetTransactionList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            Transactions objTransaction = new Transactions();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objTransaction, SystemEnum.Pages.TransactionList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the Transaction
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetTransactionByIdPage)]
        [HttpGet]
        public ActionResult GetTransactionById(int id)
        {
            IList<Transactions> lstTransactions = new List<Transactions>();
            Transactions objTransaction = new Transactions();
            StoreTransactionService objService = new StoreTransactionService();

            lstTransactions = objService.GetTransactionbyTransactionID(id);

            if (lstTransactions != null && lstTransactions.Count > 0)
            {
                objTransaction = lstTransactions.FirstOrDefault();
            }

            return View(Views.TransactionDetail, objTransaction);
        }

        #endregion

        #region "Transaction - Issue"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionIssuePage)]
        public ActionResult GetTransactionIssuePage()
        {
            Issue obj = new Issue();
            obj.QtyIssue = 1;
            return View(Views.TransactionIssue, obj);
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="tranId">The tran identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIssueListByItemCityStore)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetIssueByStockCodeandSubStore([DataSourceRequest]DataSourceRequest request, int stockID, int subStoreID, int tranId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "i.IssueID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Issue>(),
                Total = 0
            };

            if ((stockID > 0 && tranId == 0) || (stockID == 0 && tranId > 0))
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetIssueByStockCodeandSubStore(stockID, subStoreID, tranId, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// saves Issue To Temp Grid
        /// </summary>
        /// <param name="objIssue">Object Of Issue Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTempIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTempIssue(Issue objIssue)
        {
            List<Issue> lstissues = new List<Issue>();
            List<ViewModelIssue> lstViewModelIssue = new List<ViewModelIssue>();            
            try
            {
                if (string.IsNullOrEmpty(objIssue.HDNWoNo) && !(objIssue.LocationID > 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please select JobOrder No or Location No." }, JsonRequestBehavior.AllowGet);
                }

                if (objIssue.DateIssue != null && objIssue.DateIssue > DateTime.MinValue && objIssue.QtyIssue >= 0)
                {
                    objIssue.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objIssue.CreatedDate = DateTime.Now;
                    objIssue.WONo = objIssue.HDNWoNo;
                    objIssue.L2ID = objIssue.HdnL2ID;
                    objIssue.SubStoreID = objIssue.HdnSubStoreID;
                    objIssue.GUID = System.Guid.NewGuid().ToString();
                    objIssue.TotalPrice = objIssue.QtyIssue * objIssue.Price3;

                    if (!string.IsNullOrEmpty(objIssue.lstIssues))
                    {
                        lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Issue>>(objIssue.lstIssues);
                    }

                    lstissues.Add(objIssue);

                    lstViewModelIssue = lstissues
                   .Select(t => new ViewModelIssue
                   {
                       StockID = t.StockID,
                       SubStoreID = t.SubStoreID,
                       AltStockDescription=t.AltStockDescription,
                       DateIssue=t.DateIssue,
                       GUID=t.GUID,
                       Price3=t.Price3,
                       QtyIssue=t.QtyIssue,
                       StockDescription=t.StockDescription,
                       StockNo=t.StockNo,
                       SubStoreCode=t.SubStoreCode,
                       TotalPrice=t.TotalPrice,
                       UoM=t.UoM,
                       LocationID=t.LocationID,
                       L2ID=t.L2ID,
                       ToSubStoreID=t.ToSubStoreID,
                       ToL2ID=t.ToL2ID,
                       WONo=t.WONo,
                       Description=t.Description,
                       Warehouse=t.Warehouse,
                       IWTNo = t.IWTNo,
                       BatchNo = t.BatchNo,
                       IsTransfer = t.IsTransfer
                   }).ToList();

                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), lstViewModelIssue });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves Issue For Transaction
        /// </summary>
        /// <param name="kendogridData">The kendo grid data.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTransactionIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTransactionIssue(string kendogridData)
        {
            List<Issue> lstissues = new List<Issue>();
            List<ViewModelIssue> lstViewModelIssue = new List<ViewModelIssue>();
            Transactions objTransaction = null;
            StoreTransactionService objService = new StoreTransactionService();
            string[] arrValues = new string[5];
            string stockBalance = string.Empty;
            int result = -1;
            try
            {
                if (!string.IsNullOrEmpty(kendogridData))
                {
                    var microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    };

                    lstViewModelIssue = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ViewModelIssue>>(kendogridData, microsoftDateFormatSettings);

                    lstissues = lstViewModelIssue.Select(t => new Issue
                    {
                      StockID = t.StockID,
                      SubStoreID = t.SubStoreID,
                      AltStockDescription = t.AltStockDescription,
                      DateIssue = t.DateIssue,
                      GUID = t.GUID,
                      Price3 = t.Price3,
                      QtyIssue = t.QtyIssue,
                      StockDescription = t.StockDescription,
                      StockNo = t.StockNo,
                      SubStoreCode = t.SubStoreCode,
                      TotalPrice = t.TotalPrice,
                      UoM = t.UoM,
                      LocationID = t.LocationID,
                      L2ID = t.L2ID,
                      ToSubStoreID = t.ToSubStoreID,
                      ToL2ID = t.ToL2ID,
                      WONo=t.WONo,
                      Description = t.Description,
                      Warehouse = t.Warehouse,
                      IWTNo=t.IWTNo,
                      BatchNo=t.BatchNo,
                      IsTransfer =t.IsTransfer,
                      CreatedBy = ProjectSession.EmployeeID.ToString(),
                      CreatedDate = DateTime.Now
                    }).ToList();
                }

                if (lstissues != null && lstissues.Count > 0)
                {
                    string lastTranNo = string.Empty;
                    objTransaction = new Transactions();
                    objTransaction.CreatedBy = ProjectSession.EmployeeID;
                    objTransaction.CreatedDate = DateTime.Now;
                    objTransaction.Type = SystemEnum.TransactionType.Issue.GetHashCode();
                    objTransaction.TransactionNO = objService.GenerateNewTransactionNo(objTransaction.Type);
                    objTransaction.Status = SystemEnum.TransactionStatus.Active.GetHashCode();

                    if (string.IsNullOrEmpty(objTransaction.TransactionNO))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Transaction No cannot be blank." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.Save<Transactions>(objTransaction);

                            if (result > 0)
                            {
                                foreach (Issue objIssue in lstissues)
                                {
                                    objIssue.TransactionID = result;
                                    arrValues = GetStockBalBySubStore(Convert.ToInt32(objIssue.StockID), Convert.ToInt32(objIssue.SubStoreID));

                                    if (arrValues.Length > 0)
                                    {
                                        stockBalance = arrValues[0];
                                    }

                                    if (objContext.Save<Issue>(objIssue) > 0)
                                    {
                                        objService.UpdateBalance(Convert.ToInt32(objIssue.StockID), Convert.ToInt32(objIssue.SubStoreID), (Convert.ToDecimal(stockBalance) - objIssue.QtyIssue).ToString());
                                    }
                                }
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Transaction - Receive"

        /// <summary>
        /// Receive Page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionReceivePage)]
        public ActionResult GetTransactionReceivePage()
        {
            Receive obj = new Receive();
            obj.QtyRec = 1;
            obj.Price = 0;
            return View(Views.TransactionReceive, obj);
        }

        /// <summary>
        /// saves Receive To Temp Grid
        /// </summary>
        /// <param name="objReceive">Object Of Receive Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTempReceive)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTempReceive(Receive objReceive)
        {
            List<Receive> lstissues = new List<Receive>();
            List<ViewModelReceive> lstViewModelReceiveissues = new List<ViewModelReceive>();
            try
            {
                if (!(objReceive.HdnL2ID > 0) || !(objReceive.HdnSubStoreID > 0) || !(objReceive.SupplierID > 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Mandatory fields cannot be blank." }, JsonRequestBehavior.AllowGet);
                }
                else if ((objReceive.QtyRec < 0) || (objReceive.Price < 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Quantity and Price must be greater than or equal to zero." }, JsonRequestBehavior.AllowGet);
                }

                DateTime temp;
                if (!DateTime.TryParse(objReceive.Date.ToString(), out temp))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please enter valid date." }, JsonRequestBehavior.AllowGet);
                }

                if (objReceive.Date != null && objReceive.Date > DateTime.MinValue && objReceive.QtyRec > 0)
                {
                    objReceive.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objReceive.CreatedDate = DateTime.Now;
                    objReceive.L2ID = objReceive.HdnL2ID;
                    objReceive.SubStoreID = objReceive.HdnSubStoreID;
                    objReceive.GUID = System.Guid.NewGuid().ToString();
                    objReceive.TotalPrice = objReceive.QtyRec * objReceive.Price;

                    if (!string.IsNullOrEmpty(objReceive.lstReceives))
                    {
                        lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Receive>>(objReceive.lstReceives);
                    }

                    lstissues.Add(objReceive);


                    lstViewModelReceiveissues = lstissues
                    .Select(t => new ViewModelReceive
                    {
                        StockID = t.StockID,
                        L2ID = t.L2ID,
                        SubStoreID = t.SubStoreID,
                        SubStoreCode = t.SubStoreCode,
                        Date = t.Date,
                        MRNNo = t.MRNNo,
                        PONo = t.PONo,
                        SupplierID = t.SupplierID,
                        QtyRec = t.QtyRec,
                        Price = t.Price,
                        TotalPrice = t.TotalPrice,
                        HdnAvgPrice = t.HdnAvgPrice,
                        Comments = t.Comments,
                        StockNo = t.StockNo,
                        StockDescription = t.StockDescription,
                        AltStockDescription = t.AltStockDescription,
                        GUID = t.GUID
                    }).ToList();

                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), lstViewModelReceiveissues });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="tranId">The transaction identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetReceiveListByItemCityStore)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetReceiveListByItemCityStore([DataSourceRequest]DataSourceRequest request, int stockID, int subStoreID, int tranId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "r.ReceiveID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Receive>(),
                Total = 0
            };

            if ((stockID > 0 && tranId == 0) || (stockID == 0 && tranId > 0))
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetReceiveListByItemCityStore(stockID, subStoreID, tranId, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// saves Issue For Transaction
        /// </summary>
        /// <param name="kendogridData">The kendo grid data.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTransactionReceive)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTransactionReceive(string kendogridData)
        {
            List<Receive> lstReceives = new List<Receive>();
            List<ViewModelReceive> lstViewModelReceives = new List<ViewModelReceive>();
            Transactions objTransaction = null;
            StoreTransactionService objService = new StoreTransactionService();
            string[] arrValues = new string[5];
            string stockBalance = string.Empty;
            int result = -1;
            float total = 0;

            try
            {
                if (!string.IsNullOrEmpty(kendogridData))
                {
                    var microsoftDateFormatSettings = new JsonSerializerSettings
                     {
                         DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                         DateTimeZoneHandling = DateTimeZoneHandling.Local
                     };
                    lstViewModelReceives = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ViewModelReceive>>(kendogridData, microsoftDateFormatSettings);

                    lstReceives = lstViewModelReceives
                    .Select(t => new Receive
                    {
                        StockID = t.StockID,
                        L2ID = t.L2ID,
                        SubStoreID = t.SubStoreID,
                        SubStoreCode = t.SubStoreCode,
                        Date = t.Date,
                        MRNNo = t.MRNNo,
                        PONo = t.PONo,
                        SupplierID=t.SupplierID,
                        QtyRec = t.QtyRec,
                        Price = t.Price,
                        TotalPrice = t.TotalPrice,                        
                        HdnAvgPrice = t.HdnAvgPrice,                       
                        Comments=t.Comments,
                        StockNo = t.StockNo,
                        StockDescription = t.StockDescription,
                        AltStockDescription = t.AltStockDescription,
                        GUID = t.GUID
                    }).ToList();

                }

                if (lstReceives != null && lstReceives.Count > 0)
                {
                    string lastTranNo = string.Empty;
                    objTransaction = new Transactions();
                    objTransaction.CreatedBy = ProjectSession.EmployeeID;
                    objTransaction.CreatedDate = DateTime.Now;

                    if (lstReceives[0].Transfer == 1)
                    {
                        objTransaction.Type = SystemEnum.TransactionType.Transfer.GetHashCode();
                    }
                    else
                    {
                        objTransaction.Type = SystemEnum.TransactionType.Receive.GetHashCode();
                    }

                    objTransaction.TransactionNO = objService.GenerateNewTransactionNo(objTransaction.Type);
                    objTransaction.Status = SystemEnum.TransactionStatus.Active.GetHashCode();

                    if (string.IsNullOrEmpty(objTransaction.TransactionNO))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Transaction No cannot be blank." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.Save<Transactions>(objTransaction);

                            if (result > 0)
                            {
                                foreach (Receive objReceive in lstReceives)
                                {
                                    objReceive.TransactionID = result;
                                    arrValues = GetStockBalBySubStore(Convert.ToInt32(objReceive.StockID), Convert.ToInt32(objReceive.SubStoreID));

                                    if (arrValues.Length > 0)
                                    {
                                        stockBalance = arrValues[0];
                                    }

                                    if (objContext.Save<Receive>(objReceive) > 0)
                                    {
                                        if (objTransaction.Type == SystemEnum.TransactionType.Transfer.GetHashCode())
                                        {
                                            Issue objTransferIssue = new Issue();
                                            objTransferIssue.StockID = objReceive.StockID;
                                            objTransferIssue.QtyIssue = objReceive.QtyRec;
                                            objTransferIssue.L2ID = objReceive.FromL2ID;
                                            objTransferIssue.ToL2ID = objReceive.L2ID;
                                            objTransferIssue.SubStoreID = objReceive.FromSubStoreID;
                                            objTransferIssue.ToSubStoreID = objReceive.SubStoreID;
                                            objTransferIssue.Description = objReceive.Comments;
                                            objTransferIssue.DateIssue = objReceive.Date;
                                            objTransferIssue.Price3 = objReceive.Price;
                                            objTransferIssue.IsTransfer = 1;

                                            if (objContext.Save<Issue>(objTransferIssue) > 0)
                                            {
                                                total = ((Convert.ToSingle(stockBalance) * Convert.ToSingle(objReceive.Price)) + (Convert.ToSingle(objTransferIssue.QtyIssue) * Convert.ToSingle(objTransferIssue.Price3))) / (Convert.ToSingle(stockBalance) + Convert.ToSingle(objTransferIssue.QtyIssue));
                                                if (UpdateAvgPrice(Convert.ToInt32(objReceive.StockID), Convert.ToDecimal(total)))
                                                {
                                                    arrValues = GetStockBalBySubStore(Convert.ToInt32(objReceive.StockID), Convert.ToInt32(objReceive.SubStoreID));

                                                    if (arrValues.Length > 0)
                                                    {
                                                        stockBalance = arrValues[0];
                                                    }

                                                    objService.UpdateBalance(Convert.ToInt32(objReceive.StockID), Convert.ToInt32(objReceive.SubStoreID), (Convert.ToDecimal(stockBalance) + objReceive.QtyRec).ToString());
                                                }
                                            }
                                        }
                                        else
                                        {
                                            total = ((Convert.ToSingle(stockBalance) * Convert.ToSingle(objReceive.HdnAvgPrice)) + (Convert.ToSingle(objReceive.QtyRec) * Convert.ToSingle(objReceive.Price))) / (Convert.ToSingle(stockBalance) + Convert.ToSingle(objReceive.QtyRec));
                                            if (UpdateAvgPrice(Convert.ToInt32(objReceive.StockID), Convert.ToDecimal(total)))
                                            {
                                                objService.UpdateBalance(Convert.ToInt32(objReceive.StockID), Convert.ToInt32(objReceive.SubStoreID), (Convert.ToDecimal(stockBalance) + objReceive.QtyRec).ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Transaction - Return"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionReturnPage)]
        public ActionResult GetTransactionReturnPage()
        {
            Return obj = new Return();
            obj.Qty = 1;
            return View(Views.TransactionReturn, obj);
        }

        /// <summary>
        /// To Get Return Balance.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="storeID">The store identifier.</param>
        /// <param name="workOrderNO">The work order no.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns>
        /// Return Balance
        /// </returns>
        [HttpGet]
        [ActionName(Actions.GetReturnBalance)]
        public JsonResult GetReturnBalance(int stockID, int storeID, string workOrderNO, int locationID)
        {
            try
            {
                StoreTransactionService objService = new StoreTransactionService();
                string balance = objService.GetReturnBalance(stockID, storeID, workOrderNO, locationID).ToString();
                return Json(balance, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all average price list.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="storeID">The store identifier.</param>
        /// <param name="workOrderNO">The work order no.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIssueUnitPriceByStockStoreWOandLOC)]
        [HttpGet]
        public ActionResult GetIssueUnitPriceByStockStoreWOandLOC(int stockID, int storeID, string workOrderNO, int locationID)
        {
            try
            {
                StoreTransactionService objService = new StoreTransactionService();
                IList<Issue> result = new List<Issue>();
                result = objService.GetIssueUnitPriceByStockStoreWOandLOC(stockID, storeID, workOrderNO, locationID);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves Issue To Temp Grid
        /// </summary>
        /// <param name="objReturn">Object Of Issue Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTempReturn)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTempReturn(Return objReturn)
        {
            List<Return> lstissues = new List<Return>();
            try
            {
                if (string.IsNullOrEmpty(objReturn.HDNWoNo) && !(objReturn.LocationID > 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please select JobOrder No or Location No." }, JsonRequestBehavior.AllowGet);
                }

                if (!(objReturn.HdnL2ID > 0) || !(objReturn.HdnSubStoreID > 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Mandatory fields cannot be blank." }, JsonRequestBehavior.AllowGet);
                }
                else if ((objReturn.Qty < 0) || (objReturn.RetPrice < 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Quantity and Price must be greater than or equals to zero." }, JsonRequestBehavior.AllowGet);
                }

                DateTime temp;
                if (!DateTime.TryParse(objReturn.Date.ToString(), out temp))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please enter valid date." }, JsonRequestBehavior.AllowGet);
                }

                if (objReturn.Date != null && objReturn.Date > DateTime.MinValue && objReturn.Qty > 0)
                {
                    objReturn.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objReturn.CreatedDate = DateTime.Now;
                    objReturn.WONo = objReturn.HDNWoNo;
                    objReturn.L2ID = objReturn.HdnL2ID;
                    objReturn.SubStoreID = objReturn.HdnSubStoreID;
                    objReturn.GUID = System.Guid.NewGuid().ToString();
                    objReturn.TotalPrice = objReturn.Qty * objReturn.RetPrice;

                    if (!string.IsNullOrEmpty(objReturn.lstReturn))
                    {
                        lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Return>>(objReturn.lstReturn);
                    }

                    lstissues.Add(objReturn);

                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), lstissues });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="tranId">The tran identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetReturnByStockCodeandSubStore)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetReturnByStockCodeandSubStore([DataSourceRequest]DataSourceRequest request, int stockID, int subStoreID, int tranId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "r.ReturnID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Return>(),
                Total = 0
            };

            if ((stockID > 0 && tranId == 0) || (stockID == 0 && tranId > 0))
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetReturnByStockCodeandSubStore(stockID, subStoreID, tranId, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// saves Issue For Transaction
        /// </summary>
        /// <param name="kendogridData">The kendo grid data.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTransactionReturn)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTransactionReturn(string kendogridData)
        {
            List<Return> lstissues = new List<Return>();
            Transactions objTransaction = null;
            StoreTransactionService objService = new StoreTransactionService();
            int result = -1;
            try
            {
                if (!string.IsNullOrEmpty(kendogridData))
                {
                    var microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    };
                    lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Return>>(kendogridData, microsoftDateFormatSettings);
                }

                if (lstissues != null && lstissues.Count > 0)
                {
                    string lastTranNo = string.Empty;
                    objTransaction = new Transactions();
                    objTransaction.CreatedBy = ProjectSession.EmployeeID;
                    objTransaction.CreatedDate = DateTime.Now;
                    objTransaction.Type = SystemEnum.TransactionType.Return.GetHashCode();
                    objTransaction.TransactionNO = objService.GenerateNewTransactionNo(objTransaction.Type);
                    objTransaction.Status = SystemEnum.TransactionStatus.Active.GetHashCode();

                    if (string.IsNullOrEmpty(objTransaction.TransactionNO))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Transaction No cannot be blank." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.Save<Transactions>(objTransaction);

                            if (result > 0)
                            {
                                foreach (Return objIssue in lstissues)
                                {
                                    objIssue.TransactionID = result;
                                    objContext.Save<Return>(objIssue);
                                }
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Transaction - Adjustment"

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionAdjustmentPage)]
        public ActionResult GetTransactionAdjustmentPage()
        {
            Adjustment obj = new Adjustment();
            obj.QtyAdj = 1;
            return View(Views.TransactionAdjustment, obj);
        }

        /// <summary>
        /// saves Adjustment To Temp Grid
        /// </summary>
        /// <param name="objAdj">Object Of Adjustment Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTempAdjustment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTempAdjustment(Adjustment objAdj)
        {
            List<Adjustment> lstissues = new List<Adjustment>();
            try
            {
                DateTime temp;
                if (!DateTime.TryParse(objAdj.Date.ToString(), out temp))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please enter valid date." }, JsonRequestBehavior.AllowGet);
                }

                //if (objAdj.Date != null && objAdj.Date > DateTime.MinValue && objAdj.QtyAdj > 0)
                //if (objAdj.Date != null && objAdj.Date > DateTime.MinValue && (objAdj.QtyAdj > 0 || objAdj.QtyAdj < 0))
                if (objAdj.Date != null && objAdj.Date > DateTime.MinValue && !(objAdj.QtyAdj < 0))
                {
                    objAdj.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objAdj.CreatedDate = DateTime.Now;
                    objAdj.L2ID = objAdj.HdnL2ID;
                    objAdj.SubStoreID = objAdj.HdnSubStoreID;
                    objAdj.GUID = System.Guid.NewGuid().ToString();
                    objAdj.TotalPrice = objAdj.QtyAdj * objAdj.Price4;

                    if (!string.IsNullOrEmpty(objAdj.lstAdj))
                    {
                        lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Adjustment>>(objAdj.lstAdj);
                    }

                    lstissues.Add(objAdj);

                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), lstissues });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity can not be zero." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// saves Adjustment For Transaction
        /// </summary>
        /// <param name="kendogridData">The kendo grid data.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTransactionAdjustment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTransactionAdjustment(string kendogridData)
        {
            List<Adjustment> lstissues = new List<Adjustment>();
            Transactions objTransaction = null;
            StoreTransactionService objService = new StoreTransactionService();
            string[] arrValues = new string[5];
            string stockBalance = string.Empty;
            int result = -1;
            try
            {
                if (!string.IsNullOrEmpty(kendogridData))
                {
                    var microsoftDateFormatSettings = new JsonSerializerSettings
                    {
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    };

                    lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Adjustment>>(kendogridData, microsoftDateFormatSettings);
                }

                if (lstissues != null && lstissues.Count > 0)
                {
                    string lastTranNo = string.Empty;
                    objTransaction = new Transactions();
                    objTransaction.CreatedBy = ProjectSession.EmployeeID;
                    objTransaction.CreatedDate = DateTime.Now;
                    objTransaction.Type = SystemEnum.TransactionType.Adjustment.GetHashCode();
                    objTransaction.TransactionNO = objService.GenerateNewTransactionNo(objTransaction.Type);
                    objTransaction.Status = SystemEnum.TransactionStatus.Active.GetHashCode();

                    if (string.IsNullOrEmpty(objTransaction.TransactionNO))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Transaction No cannot be blank." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (ServiceContext objContext = new ServiceContext())
                        {
                            result = objContext.Save<Transactions>(objTransaction);

                            if (result > 0)
                            {
                                foreach (Adjustment objIssue in lstissues)
                                {
                                    objIssue.TransactionID = result;
                                    arrValues = GetStockBalBySubStore(Convert.ToInt32(objIssue.StockID), Convert.ToInt32(objIssue.SubStoreID));

                                    if (arrValues.Length > 0)
                                    {
                                        stockBalance = arrValues[0];
                                    }

                                    if (objContext.Save<Adjustment>(objIssue) > 0)
                                    {
                                        objService.UpdateBalance(Convert.ToInt32(objIssue.StockID), Convert.ToInt32(objIssue.SubStoreID), (Convert.ToDecimal(stockBalance) + objIssue.QtyAdj).ToString());
                                    }
                                }
                            }
                            else
                            {
                                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="tranId">The tran identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAdjustmentByStockCodeandSubStore)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetAdjustmentByStockCodeandSubStore([DataSourceRequest]DataSourceRequest request, int stockID, int subStoreID, int tranId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "a.AdjustmentID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Adjustment>(),
                Total = 0
            };

            if ((stockID > 0 && tranId == 0) || (stockID == 0 && tranId > 0))
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetAdjustmentByStockCodeandSubStore(stockID, subStoreID, tranId, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }
        #endregion

        #region "Transaction - Transfer"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionTransferPage)]
        public ActionResult GetTransactionTransferPage()
        {
            Receive obj = new Receive();
            obj.QtyRec = 1;
            obj.Price = 0;
            return View(Views.TransactionTransfer, obj);
        }

        /// <summary>
        /// saves Receive To Temp Grid
        /// </summary>
        /// <param name="objTransfer">Object Of Receive Model</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error thrown</exception>
        [ActionName(Actions.SaveTempTransfer)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveTempTransfer(Receive objTransfer)
        {
            List<Receive> lstissues = new List<Receive>();
            try
            {
                if (!(objTransfer.HdnL2ID > 0) || !(objTransfer.HdnSubStoreID > 0) || !(objTransfer.L2ID > 0) || !(objTransfer.SubStoreID > 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Mandatory fields cannot be blank." }, JsonRequestBehavior.AllowGet);
                }
                else if ((objTransfer.QtyRec < 0) || (objTransfer.Price < 0))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Quantity and Price must be greater than or equals to zero." }, JsonRequestBehavior.AllowGet);
                }

                DateTime temp;
                if (!DateTime.TryParse(objTransfer.Date.ToString(), out temp))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Please enter valid date." }, JsonRequestBehavior.AllowGet);
                }

                if (objTransfer.Date != null && objTransfer.Date > DateTime.MinValue && objTransfer.QtyRec > 0)
                {
                    objTransfer.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objTransfer.CreatedDate = DateTime.Now;
                    objTransfer.FromL2ID = objTransfer.HdnL2ID;
                    objTransfer.FromSubStoreID = objTransfer.HdnSubStoreID;
                    objTransfer.GUID = System.Guid.NewGuid().ToString();
                    objTransfer.TotalPrice = objTransfer.QtyRec * objTransfer.Price;

                    if (!string.IsNullOrEmpty(objTransfer.lstReceives))
                    {
                        lstissues = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Receive>>(objTransfer.lstReceives);
                    }

                    lstissues.Add(objTransfer);

                    return Json(new object[] { SystemEnum.MessageType.success.GetHashCode().ToString(), SystemEnum.MessageType.success.ToString(), lstissues });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), "Date is required field. \n Quantity must be greater than zero." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode().ToString(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Transaction - Purchase Proposal"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetTransactionPurchaseProposalPage)]
        public ActionResult GetTransactionPurchaseProposalPage()
        {
            return View(Views.TransactionPurchaseProposal);
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="storeID">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseProposalNormalpriority)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseProposalNormalpriority([DataSourceRequest]DataSourceRequest request, int storeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "vw.RowNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Stockcode_level>(),
                Total = 0
            };

            if (storeID > 0)
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetPurchaseProposalNormalpriority(storeID, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Gets the Issue List
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="storeID">The stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchaseProposalHighpriority)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPurchaseProposalHighpriority([DataSourceRequest]DataSourceRequest request, int storeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "vw.RowNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService obj = new StoreTransactionService();

            DataSourceResult result = new DataSourceResult
            {
                Data = new List<Stockcode_level>(),
                Total = 0
            };

            if (storeID > 0)
            {
                result = new DataSourceResult()
                {
                    Data = obj.GetPurchaseProposalHighpriority(storeID, pageNumber, sortExpression, sortDirection),
                    Total = obj.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Gets the stock balance
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockBalanceByStore)]
        [HttpGet]
        public JsonResult GetStockBalanceByStore(int stockID, int subStoreID)
        {
            string[] arrValues = new string[5];
            string stockBalance = string.Empty;
            string qtyIssued = string.Empty;
            string totalQtyIssued = "0";
            string totalQtyReturned = "0";
            string totalQtyReceive = "0";
            string totalQtyAdjustment = "0";
            try
            {
                stockBalance = "0";

                arrValues = GetStockBalBySubStore(stockID, subStoreID);

                if (arrValues.Length > 4)
                {
                    stockBalance = arrValues[0];
                    totalQtyIssued = arrValues[1];
                    totalQtyReturned = arrValues[2];
                    totalQtyAdjustment = arrValues[3];
                    totalQtyReceive = arrValues[4];
                }

                return Json(new object[] { stockBalance, totalQtyIssued, totalQtyReturned, totalQtyAdjustment, totalQtyReceive }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { string.Empty, string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the stock balance by sub store.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <returns></returns>
        public string[] GetStockBalBySubStore(int stockID, int subStoreID)
        {
            JobOrderService objService = new JobOrderService();
            string stockBalance = string.Empty;
            string qtyIssued = string.Empty;
            decimal? totalQtyIssued = 0;
            decimal? totalQtyReturned = 0;
            decimal? totalQtyReceive = 0;
            decimal? totalQtyAdjustment = 0;
            Issue objIssue = new Issue();
            Return objReturn = new Return();
            Receive objReceive = new Receive();
            Adjustment objAdjustment = new Adjustment();
            string[] arrValues = new string[5];

            try
            {
                stockBalance = "0";

                objIssue.StockID = stockID;
                objIssue.SubStoreID = subStoreID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    totalQtyIssued = objContext.Search<Issue>(objIssue).Sum(x => x.QtyIssue);
                    totalQtyIssued = totalQtyIssued == null ? 0 : totalQtyIssued;
                }

                objReturn.StockID = stockID;
                objReturn.SubStoreID = subStoreID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    totalQtyReturned = objContext.Search<Return>(objReturn).Sum(x => x.Qty);
                    totalQtyReturned = totalQtyReturned == null ? 0 : totalQtyReturned;
                }

                objAdjustment.StockID = stockID;
                objAdjustment.SubStoreID = subStoreID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    totalQtyAdjustment = objContext.Search<Adjustment>(objAdjustment).Sum(x => x.QtyAdj);
                    totalQtyAdjustment = totalQtyAdjustment == null ? 0 : totalQtyAdjustment;
                }

                objReceive.StockID = stockID;
                objReceive.SubStoreID = subStoreID;

                using (ServiceContext objContext = new ServiceContext())
                {
                    totalQtyReceive = objContext.Search<Receive>(objReceive).Sum(x => x.QtyRec);
                    totalQtyReceive = totalQtyReceive == null ? 0 : totalQtyReceive;
                }

                stockBalance = string.Format("{0:0.00}", totalQtyReceive - totalQtyIssued + totalQtyReturned + totalQtyAdjustment);

                arrValues[0] = stockBalance;
                arrValues[1] = totalQtyReceive.ToString();
                arrValues[2] = totalQtyIssued.ToString();
                arrValues[3] = totalQtyReturned.ToString();
                arrValues[4] = totalQtyAdjustment.ToString();

                return arrValues;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion

        #region "Item Requisition Menu"

        /// <summary>
        /// load page Item Requisition
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ItemRequisition)]
        public ActionResult ItemRequisition()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.ItemRequisitions_ItemRequisitions_Allowaccess && ProjectSession.PermissionAccess.ItemRequisitions__ShowHideModule)
            {
                return View(Views.ItemRequisitionList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Prints the IR.
        /// </summary>
        /// <param name="mr_no">IR identifier</param>
        /// <returns></returns>
        [ActionName(Actions.PrintItemReq)]
        public ActionResult PrintItemReq(string mr_no)
        {
            List<PrintItemRequisitions> objDetails = new List<PrintItemRequisitions>();
            IList<PrintItemRequisitions> lstDetails = new List<PrintItemRequisitions>();
            PrintItemRequisitions objIRDetail = new PrintItemRequisitions();
            Collection<DBParameters> parameters = new Collection<DBParameters>();
            var strWhere = string.Empty;
            string query = string.Empty;
            if (!string.IsNullOrEmpty(mr_no))
            {
                parameters.Add(new DBParameters()
                {
                    Name = "mr_no",
                    Value = mr_no,
                    DBType = DbType.Int32
                });
            }

            string strQuery = " select mr_no,mr_date,order_date,L2.L2ID,l2.l2code, " +
                              " MaintenanceDepartment.MaintdeptId,MaintenanceDepartment.MaintDeptCode,MaintenanceDepartment.MaintDeptdesc,MaintenanceDepartment.MaintDeptAltdesc, " +
                              " MainenanceDivision.MaintDivisionID,MainenanceDivision.MaintDivisionName,MainenanceDivision.MaintDivisionCode,MainenanceDivision.MaintDivisionAltName, " +
                              " substore.SubStoreId,substore.SubStoreCode,mr.SectionSupId,employees.Name,createdemployees.Name AS CreatedBy, " +
                              " reqemployees.Name As RequestedBy " +
                              " from material_req_master mr " +
                              " LEFT JOIN L2 ON L2.L2ID = mr.L2ID  " +
                              " LEFT JOIN MaintenanceDepartment ON MaintenanceDepartment.maintDeptID = mr.MaintdeptId " +
                              " LEFT JOIN MainenanceDivision ON MainenanceDivision.MaintDivisionID = mr.MaintdivId " +
                              " LEFT JOIN substore ON substore.SubStoreID = mr.SubStoreId " +
                              " LEFT JOIN employees ON employees.EmployeeID = mr.SectionSupId " +
                              " LEFT JOIN employees createdemployees ON createdemployees.EmployeeID = mr.CreatedBy " +
                              " LEFT JOIN employees reqemployees ON reqemployees.EmployeeID = mr.req_by " +
                              " where mr_no = @mr_no";

            query = "SELECT  Row_number() over (order by mr_id) as Rowno,(CASE WHEN mrd.Qty = (mrd.ReceivedQty + mrd.CanceledQty) THEN 'Fully Issued' ELSE 'Not Fully Issued' END) AS IssuanceStatus, ISNULL(mrd.WorkorderNo, '') AS WorkorderNo, assets.AssetNumber, mrd.Qty, mrd.uom, mrd.part_desc, mrd.Altpart_desc, stockcode.StockNo,mrd.mr_no FROM material_req_details mrd INNER JOIN material_req_master mr ON mrd.mr_no = mr.mr_no LEFT OUTER JOIN assets ON assets.AssetID = mrd.AssetId LEFT OUTER JOIN stockcode ON stockcode.StockID = mrd.partid where mrd.mr_no= @mr_no";
            using (DapperDBContext context = new DapperDBContext())
            {
                objIRDetail = context.ExecuteQuery<PrintItemRequisitions>(strQuery, parameters).FirstOrDefault();
                lstDetails = context.ExecuteQuery<PrintItemRequisitions>(query, parameters);
            }

            if (lstDetails != null && lstDetails.Count > 0)
            {
                objDetails = lstDetails.ToList();
            }

            ItemRequisitionsReport report = new ItemRequisitionsReport();
            report.DataSource = objDetails;
            if (!string.IsNullOrWhiteSpace(objIRDetail.RequestedBy))
                report.lblRequestedBy.Text = "Requested By:" + objIRDetail.RequestedBy; 

            report.txtProject.Text = objIRDetail.l2code;
            report.txtStore.Text = objIRDetail.SubStoreCode;
            report.txtStoreIRNo.Text = objIRDetail.mr_no;
            report.txtArabicDate.Text = Common.GetArabicDateOnly(objIRDetail.mr_date).ToString();
            report.txtDate.Text = Common.GetEnglishDateOnly(objIRDetail.mr_date).ToString();
            report.txtMaintDepart.Text = objIRDetail.MaintDeptdesc;
            report.txtAltMaintDepart.Text = objIRDetail.MaintDeptAltdesc;
            report.txtAltManitDivision.Text = objIRDetail.MaintDivisionAltName;
            report.txtManitDivision.Text = objIRDetail.MaintDivisionName;
            report.txtSectionSuperitendent.Text = objIRDetail.Name;
            report.txtPreparedBy.Text = objIRDetail.CreatedBy;
            report.txtApprovedBy.Text = ProjectSession.EmployeeName;
            report.xrLabel4.Text = ProjectSession.HeaderNameEn;
            report.txtRowNo.DataBindings.Add("Text", objDetails, "Rowno");
            report.txtItemNo.DataBindings.Add("Text", objDetails, "StockNo");
            report.txtDiscription.DataBindings.Add("Text", objDetails, "part_desc");
            report.txtAltDiscription.DataBindings.Add("Text", objDetails, "Altpart_desc");
            report.txtUOM.DataBindings.Add("Text", objDetails, "Uom");
            report.txtQty.DataBindings.Add("Text", objDetails, "Qty");
            report.txtAssetNo.DataBindings.Add("Text", objDetails, "AssetNumber");
            report.txtJobOrderNo.DataBindings.Add("Text", objDetails, "WorkorderNo");
            report.txtIssuanceStatus.DataBindings.Add("Text", objDetails, "IssuanceStatus");

            return View(Views.PrintView, report);
        }

        /// <summary>
        /// Get Item Requisition List.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="showAll">The showAll.</param>
        /// <returns></returns>
        [ActionName(Actions.ItemRequisitionList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult ItemRequisitionList([DataSourceRequest]DataSourceRequest request, int showAll = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;
            int employeeID = ProjectSession.EmployeeID;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "mr_date";
                sortDirection = "Descending";
            }

            if (ProjectSession.IsCentral)
            {
                employeeID = 0;
            }

            if (showAll == 0)
            {
                strWhere = strWhere + " and material_req_master.status_id = 1 ";
            }

            if (!ProjectSession.IsCentral)
            {
                /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                strWhere += " and material_req_master.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
                /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Material_req_master objMaterialreqmaster = new Material_req_master();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objMaterialreqmaster, SystemEnum.Pages.ItemRequisitionList.GetHashCode(), employeeID, pageNumber, sortExpression, sortDirection, true, strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// load page Item Requisition Details
        /// </summary>
        /// <param name="mrno">IR Identifier</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.ItemRequisitionDetails)]
        public ActionResult ItemRequisitionDetails(string mrno, string workOrderNo = "")
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            if (ProjectSession.PermissionAccess.ItemRequisitions_ItemRequisitions_Allowaccess && ProjectSession.PermissionAccess.ItemRequisitions__ShowHideModule)
            {
                Material_req_master objMaterial_req_master = new Material_req_master();
                if (!string.IsNullOrEmpty(mrno) && mrno != "0")
                {
                    objMaterial_req_master = StoreTransactionService.GetItemRequisitionDetail(mrno);
                }
                else
                {
                    if (!string.IsNullOrEmpty(workOrderNo))
                    {
                        objMaterial_req_master.workOrderNo = workOrderNo;
                        using (ServiceContext context = new ServiceContext())
                        {
                            WorkOrder objWorkOrder = new WorkOrder();
                            objWorkOrder.WorkorderNo = workOrderNo;
                            objWorkOrder = context.Search<WorkOrder>(objWorkOrder).FirstOrDefault();

                            if (objWorkOrder != null)
                            {
                                objMaterial_req_master.L2ID = Convert.ToInt32(objWorkOrder.L2ID);
                                objMaterial_req_master.MaintdivId = objWorkOrder.MaintDivisionID;
                                objMaterial_req_master.MaintdeptId = objWorkOrder.MaintdeptID;
                                objMaterial_req_master.MainSubDeptId = objWorkOrder.MaintsubdeptID;
                                objMaterial_req_master.CreatedBy = ProjectSession.EmployeeID;
                                objMaterial_req_master.CreatedByName = ProjectSession.EmployeeName;

                                if (Convert.ToInt32(objWorkOrder.RequestorID) > 0)
                                {
                                    employees objReq = context.SelectObject<employees>(Convert.ToInt32(objWorkOrder.RequestorID));
                                    objMaterial_req_master.Req_by = objReq.EmployeeID;
                                    objMaterial_req_master.RequestedByNo = objReq.EmployeeNO;
                                    objMaterial_req_master.RequestedByName = objReq.Name;
                                }
                            }
                        }
                    }
                    else
                    {
                        employees objemp = new employees();
                        using (ServiceContext context = new ServiceContext())
                        {
                            objemp = context.SearchAll(objemp).Where(m => m.EmployeeID == ProjectSession.EmployeeID).FirstOrDefault();
                        }

                        objMaterial_req_master.L2ID = objemp.L2ID;
                        objMaterial_req_master.MaintdivId = objemp.MaintDivisionID;
                        objMaterial_req_master.MaintdeptId = objemp.MaintDeptID;
                        objMaterial_req_master.MainSubDeptId = objemp.MaintSubDeptID;

                    }

                    objMaterial_req_master.Order_date = DateTime.Now;
                    objMaterial_req_master.Requiredby_date = DateTime.Now;
                }

                if (objMaterial_req_master == null)
                {
                    objMaterial_req_master = new Material_req_master();
                    employees objemp = new employees();
                    using (ServiceContext context = new ServiceContext())
                    {
                        objemp = context.SearchAll(objemp).Where(m => m.EmployeeID == ProjectSession.EmployeeID).FirstOrDefault();
                    }

                    objMaterial_req_master.L2ID = objemp.L2ID;
                    objMaterial_req_master.MaintdivId = objemp.MaintDivisionID;
                    objMaterial_req_master.MaintdeptId = objemp.MaintDeptID;
                    objMaterial_req_master.MainSubDeptId = objemp.MaintSubDeptID;
                    objMaterial_req_master.Order_date = DateTime.Now;
                    objMaterial_req_master.Requiredby_date = DateTime.Now;
                }

                return View(Views.ItemRequisitionDetails, objMaterial_req_master);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Item Requisition Items By IR ID.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="mrno">The Item Requisition identifier.</param>
        /// <param name="dbcall">if set to <c>true</c> [database call].</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemRequisitionItemsByIRID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetItemRequisitionItemsByIRID([DataSourceRequest]DataSourceRequest request, string mrno, bool dbcall = false)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartId";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            List<Material_req_detail> lstPurchaseOrderItems = new List<Material_req_detail>();
            ////lstPurchaseOrderItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PurchaseOrderItem>>(kendogriddata);

            StoreTransactionService objService = new StoreTransactionService();
            var result = new DataSourceResult();
            if (dbcall)
            {
                result = new DataSourceResult()
                {
                    Data = objService.GetItemRequisitionByIRID(mrno, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            }
            else if (TempData["GetPurchaseOrderItemdata"] != null)
            {
                lstPurchaseOrderItems = (List<Material_req_detail>)TempData["GetPurchaseOrderItemdata"];
                result = new DataSourceResult()
                {
                    Data = lstPurchaseOrderItems,
                    Total = 1
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Gets the item requisition item details.
        /// </summary>
        /// <param name="autoGuid">The automatic unique identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetIRItemDetails(string autoGuid, string kendogriddata)
        {
            List<Material_req_detail> lstIRItem = new List<Material_req_detail>();
            lstIRItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Material_req_detail>>(kendogriddata);
            Material_req_detail model = new Material_req_detail();
            if (!string.IsNullOrEmpty(autoGuid))
            {
                Guid id = new Guid(autoGuid);
                model = lstIRItem.FirstOrDefault(l => l.Guid_ItemID == id);
            }

            TempData["GetPurchaseOrderItemdata"] = lstIRItem;
            return Json(model);
        }

        /// <summary>
        /// Manages the purchase order.
        /// </summary>
        /// <param name="objModel">The object model.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageItemRequisitionItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageItemRequisitionItems(Material_req_detail objModel, string kendoItemgriddata)
        {
            if (objModel != null)
            {
                decimal tempValue = new decimal();
                objModel.Qty = objModel.Qty == null || objModel.Qty == 0 ? tempValue : objModel.Qty;
            }

            List<Material_req_detail> lstIRItem = new List<Material_req_detail>();
            lstIRItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Material_req_detail>>(kendoItemgriddata);

            if (objModel.Guid_ItemID == new Guid())
            {
                objModel.Guid_ItemID = Guid.NewGuid();
                lstIRItem.Add(objModel);
            }
            else
            {
                Material_req_detail editIRItem = lstIRItem.FirstOrDefault(l => l.Guid_ItemID == objModel.Guid_ItemID);
                objModel.Mr_no = editIRItem.Mr_no; // add for edit
                int editIndex = lstIRItem.IndexOf(editIRItem);

                lstIRItem.RemoveAt(editIndex);
                lstIRItem.Insert(editIndex, objModel);
            }

            TempData["GetPurchaseOrderItemdata"] = lstIRItem;
            return Json(lstIRItem);
        }

        /// <summary>
        /// Delete Item Requisition Items
        /// </summary>
        /// <param name="guid_ItemID">The guid_ item identifier.</param>
        /// <param name="kendoItemgriddata">The kendo item grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteItemRequisitionItems)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteItemRequisitionItems(string guid_ItemID, string kendoItemgriddata)
        {
            try
            {
                Guid guid_ItemID1 = new Guid(guid_ItemID);
                List<Material_req_detail> lstIRItem = new List<Material_req_detail>();
                lstIRItem = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Material_req_detail>>(kendoItemgriddata);

                Material_req_detail editIRItem = lstIRItem.FirstOrDefault(l => l.Guid_ItemID == guid_ItemID1);
                ////objModel.ID = editPurchaseOrderItem.ID; // add for edit
                int editIndex = lstIRItem.IndexOf(editIRItem);
                lstIRItem.RemoveAt(editIndex);
                TempData["GetPurchaseOrderItemdata"] = lstIRItem;
                //// return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                ////AssetService.CheckAssetChildRefExist(assetID);
                ////AssetService.DeleteAssets(assetID);
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the Item  Requisition.
        /// </summary>
        /// <param name="objMaterialreqmaster">The object Material Requisition master.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageItemRequisition)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageItemRequisition(Material_req_master objMaterialreqmaster)
        {
            objMaterialreqmaster.lstMaterialreqdetail = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Material_req_detail>>(Convert.ToString(objMaterialreqmaster.JsonlstItemModel));

            string validations = string.Empty;
            string newPurchaseOrderID = objMaterialreqmaster.Mr_no;
            try
            {
                if (objMaterialreqmaster.Mr_no != "0" && !string.IsNullOrEmpty(objMaterialreqmaster.Mr_no))
                {
                    objMaterialreqmaster.ModifiedDate = DateTime.Now;
                    objMaterialreqmaster.ModifiedBy = ProjectSession.EmployeeID;
                }
                else
                {
                    long currentMinNo = Convert.ToInt64(objMaterialreqmaster.L2ID.ToString().PadLeft(3, '0') + DateTime.Now.Year.ToString().Remove(0, 2).Insert(2, "000000"));

                    IList<Material_req_master> lstTemp = new List<Material_req_master>();
                    Material_req_master objTemp = new Material_req_master();
                    using (ServiceContext context = new ServiceContext())
                    {
                        lstTemp = context.SearchAll(objTemp).Where(m => m.L2ID == objMaterialreqmaster.L2ID).ToList();
                    }

                    if (lstTemp.Count > 0)
                    {
                        currentMinNo = Convert.ToInt64(lstTemp.Max(p => p.Mr_no));
                    }

                    objMaterialreqmaster.Mr_no = Convert.ToString(currentMinNo + 1).PadLeft(11, '0');
                    objMaterialreqmaster.Mr_date = DateTime.Now.Date;
                    objMaterialreqmaster.Status_id = 1;
                    objMaterialreqmaster.Auth_status = 4;
                    objMaterialreqmaster.CreatedBy = ProjectSession.EmployeeID;
                }

                if (ModelState.IsValid && !string.IsNullOrEmpty(objMaterialreqmaster.Mr_no))
                {
                    if (newPurchaseOrderID != "0" && !string.IsNullOrEmpty(newPurchaseOrderID))
                    {
                        ////edit mode
                        using (DapperContext context = new DapperContext())
                        {
                            ////We don't want to update Mr_date thats why it is made null here

                            objMaterialreqmaster.Mr_date = null;
                            string success = context.SaveWithReturnString(objMaterialreqmaster, true);

                            if (!string.IsNullOrEmpty(success) && success != "-1")
                            {
                                newPurchaseOrderID = objMaterialreqmaster.Mr_no;

                                StoreTransactionService.NewItemRequisitionItemDetails(objMaterialreqmaster);

                                JobOrderService objService = new JobOrderService();
                                objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objMaterialreqmaster.workOrderNo, ProjectSession.EmployeeID.ToString());

                                TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                                TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                            }
                            else
                            {
                                TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            }
                        }
                    }
                    else
                    {
                        if (StoreTransactionService.InsertMaterial_req(objMaterialreqmaster))
                        {
                            StoreTransactionService.NewItemRequisitionItemDetails(objMaterialreqmaster);

                            JobOrderService objService = new JobOrderService();
                            objService.ChangeJobOrderModificationDateTimeByWorkorderNo(objMaterialreqmaster.workOrderNo, ProjectSession.EmployeeID.ToString());

                            newPurchaseOrderID = objMaterialreqmaster.Mr_no;
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                            TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                        }
                        else
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        //// return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        ////  return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }

                if (string.IsNullOrEmpty(objMaterialreqmaster.workOrderNo))
                {
                    return RedirectToAction(Actions.ItemRequisitionDetails, Pages.Controllers.Transaction, new { mrno = newPurchaseOrderID });
                }
                else
                {
                    JobOrderService objService = new JobOrderService();
                    WorkOrder objWorkOrder = new WorkOrder();
                    objWorkOrder = objService.GetJobOrderByOrderNo(objMaterialreqmaster.workOrderNo);


                    bool isChanged = objService.ChangeJobOrderStatusByWorkorderNo(objMaterialreqmaster.workOrderNo, 10, Convert.ToInt32(objWorkOrder.WorkStatusID), ProjectSession.EmployeeID.ToString());

                    if (isChanged)
                    {
                        return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { workOrderNo = objMaterialreqmaster.workOrderNo, from = "IR" });
                    }
                    else
                    {
                        return RedirectToAction(Actions.GetJOByJobOrderNo, Pages.Controllers.Transaction, new { workOrderNo = objMaterialreqmaster.workOrderNo, from = "IR" });
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                CMMS.Models.Common.Log(ex);
                return RedirectToAction(Actions.ItemRequisitionDetails, Pages.Controllers.Transaction, new { mrno = newPurchaseOrderID });
            }
        }

        /// <summary>
        /// Cancel Item Requisition.
        /// </summary>
        /// <param name="mrno">The identifier.</param>
        /// <param name="deliveryStatus">The delivery status.</param>
        /// <param name="cancel_note">The cancel note.</param>
        /// <returns></returns>
        [ActionName(Actions.CancelItemRequisition)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CancelItemRequisition(string mrno, int deliveryStatus, string cancel_note)
        {
            try
            {
                StoreTransactionService.CancelIR(mrno, deliveryStatus, cancel_note);

                using (ServiceContext context = new ServiceContext())
                {
                    string status_desc = context.SelectObject<Material_req_statu>(3).Status_desc;
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), status_desc }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Re Open Item Requisition.
        /// </summary>
        /// <param name="mrno">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.ReOpenItemRequisition)]
        public JsonResult ReOpenItemRequisition(string mrno)
        {
            try
            {
                StoreTransactionService.ReopenIR(mrno);

                using (ServiceContext context = new ServiceContext())
                {
                    string status_desc = context.SelectObject<Material_req_statu>(1).Status_desc;
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), status_desc }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the approve identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="mr_no">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetApproveIRByIRID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetApproveIRByIRID([DataSourceRequest]DataSourceRequest request, string mr_no)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "IRAuthId";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StoreTransactionService objService = new StoreTransactionService();
            var result = new DataSourceResult()
            {
                Data = objService.GetApproveIRBymr_no(pageNumber, sortExpression, sortDirection, mr_no, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the IR approval levels by sub Store id and item requisition identifier.
        /// </summary>
        /// <param name="subStoreid">The store identifier.</param>
        /// <param name="mrno">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRApprovalLevelsByStoreIDIRID)]
        public ActionResult GetPRApprovalLevelsByStoreIDIRID(int subStoreid, string mrno)
        {
            var result = StoreTransactionService.GetPRApprovalLevelsBySubStore_IR_ID(subStoreid, mrno);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the employees by IR  approval level identifier.
        /// </summary>
        /// <param name="subStoreid">The store identifier.</param>
        /// <param name="approvalLevelId">The approval level identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesByIRApprovalLevelId)]
        public ActionResult GetEmployeesByIRApprovalLevelId(int subStoreid, int approvalLevelId)
        {
            using (ServiceContext context = new ServiceContext())
            {
                approvalLevelId = context.SelectObject<IRApprovalLevelMapping>(approvalLevelId).IRApprovalLevelId;
            }

            var result = StoreTransactionService.GetEmployeesByPRApprovalLevelId_L2ID(subStoreid, approvalLevelId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// News the approve purchase request.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        [ActionName(Actions.NewApproveIR)]
        public JsonResult NewApproveIR(Material_req_auth obj)
        {
            string auth_status_desc = string.Empty;
            int auth_status_id = 0;

            try
            {
                using (ServiceContext context = new ServiceContext())
                {
                    employees objemployees = new employees();
                    int employeeID = Convert.ToInt32(obj.EmployeeID);

                    objemployees = context.Search<employees>(objemployees).FirstOrDefault();

                    objemployees = context.SearchAll(objemployees).Where(m => m.EmployeeID == employeeID).FirstOrDefault();

                    if (objemployees.Password == obj.Password)
                    {
                        obj.Auth_date = DateTime.Now;
                        int result = context.Save<Material_req_auth>(obj);

                        if (result > 0)
                        {
                            auth_status_id = context.SelectObject<IRApprovalLevelMapping>(obj.IRApprovalLevelMappingId).Auth_status_id;

                            auth_status_desc = context.SelectObject<Mr_authorisation_statu>(auth_status_id).Auth_status_desc;
                            StoreTransactionService.UpdateIRAuthStatus(obj.Mr_no, auth_status_id);

                            /*(Start)Change Work Order Status if item is Approved (By Pratik on 19-Jan-2017 for TA270)*/
                            if (auth_status_id == 3)
                            {
                                Material_req_detail objPRItem = new Material_req_detail();
                                objPRItem.Mr_no = obj.Mr_no == null ? string.Empty : obj.Mr_no;
                                IList<string> lstWorkOrderNos = context.Search<Material_req_detail>(objPRItem).Where(x => x.WorkorderNo != null).Select(x => x.WorkorderNo).Distinct().ToList();

                                foreach (string workOrderNo in lstWorkOrderNos)
                                {
                                    UpdateWOStatusToMaterialRequested(workOrderNo, "-1");
                                }
                            }

                            /*(End)Change Work Order Status if item is Approved (By Pratik on 19-Jan-2017 for TA270)*/
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), auth_status_desc, auth_status_id }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgValidPassword }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Changes the work order's status to material requested
        /// </summary>
        /// <param name="workOrderNo">The work order no.</param>
        /// <param name="currentWorkStatusId">The current work status identifier.</param>
        /// <returns>Returns the Material Request Status ID</returns>
        public int UpdateWOStatusToMaterialRequested(string workOrderNo, string currentWorkStatusId)
        {
            try
            {
                JobOrderService objService = new JobOrderService();
                string workOrderNewStatusId = string.Empty;
                string materialRequestedStatusId = string.Empty;
                string newStatusText = string.Empty;
                materialRequestedStatusId = Common.GetConfigKeyValue("JobOrderMaterialRequestedStatus");
                workOrderNewStatusId = string.IsNullOrEmpty(materialRequestedStatusId) ? currentWorkStatusId : materialRequestedStatusId;
                bool result = false;
                if (Convert.ToInt32(workOrderNewStatusId) != Convert.ToInt32(currentWorkStatusId))
                {
                    result = objService.ChangeJobOrderStatusByWorkorderNo(workOrderNo, Convert.ToInt32(workOrderNewStatusId), Convert.ToInt32(currentWorkStatusId), ProjectSession.EmployeeID.ToString(), true, "Job Order Module");
                }

                return Convert.ToInt32(workOrderNewStatusId);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <param name="mr_id">The identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._IssueItemFromIR)]
        public ActionResult GetIssueItemPopUp(int mr_id)
        {
            Material_req_detail objReqDetail = new Material_req_detail();

            try
            {
                if (ProjectSession.PermissionAccess.ItemRequisitions_ItemRequisitions_CloseRequisitions)
                {
                    objReqDetail = StoreTransactionService.GetIRIssueDetail(mr_id);
                    if (objReqDetail != null)
                    {
                        return View(PartialViews.IssueItemFromIR, objReqDetail);
                    }
                    else
                    {
                        ProjectSession.ErrorMessage = "Item Requisition Detail not found.";
                        return RedirectToAction(Actions.DisplayError, Pages.Controllers.Account, new { area = string.Empty });
                    }
                }
                else
                {
                    return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                objReqDetail = new Material_req_detail();
                return View(PartialViews.IssueItemFromIR, objReqDetail);
            }
        }

        /// <summary>
        /// Save Item Requisition.
        /// </summary>
        /// <param name="objMaterial_req_detail">The object material request detail.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveItemIssue)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveItemIssue(Material_req_detail objMaterial_req_detail)
        {
            try
            {
                if (ProjectSession.PermissionAccess.ItemRequisitions_ItemRequisitions_Editrecords)
                {
                    if (ProjectSession.PermissionAccess.ItemRequisitions_ItemRequisitions_CloseRequisitions)
                    {
                        StoreTransactionService.UpdateMaterialReqDetailIssue(objMaterial_req_detail);
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        TempData["Message"] = ProjectSession.Resources.message.ItemRequisition_MsgDontPermissionToIssueItem;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions.ItemRequisitionDetails, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, mrno = objMaterial_req_detail.Mr_no, workOrderNo = objMaterial_req_detail.WorkorderNo });
                    }
                }

                return RedirectToAction(Actions.ItemRequisitionDetails, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, mrno = objMaterial_req_detail.Mr_no, workOrderNo = objMaterial_req_detail.WorkorderNo });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                return RedirectToAction(Actions.ItemRequisitionDetails, Pages.Controllers.Transaction, new { area = Pages.Areas.Transaction, mrno = objMaterial_req_detail.Mr_no, workOrderNo = objMaterial_req_detail.WorkorderNo });
            }
        }
        #endregion

        #region "Common"

        /// <summary>
        /// Updates the average price.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="avgPrice">The average price.</param>
        /// <returns></returns>
        public bool UpdateAvgPrice(int stockID, decimal avgPrice)
        {
            try
            {
                StoreTransactionService objService = new StoreTransactionService();
                return objService.UpdateItemAvgPrice(stockID, avgPrice);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return false;
            }
        }

        #endregion
    }
}