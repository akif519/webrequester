﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.Service.TransactionService;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Good Receipt Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Goods the receipt list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GoodReceiptList)]
        public ActionResult GoodReceiptList()
        {
            if (ProjectSession.PermissionAccess.Purchasing_GoodsReceipt_Allowaccess  && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.GoodReceiptList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the good receipt.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetGoodReceipt)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetGoodReceipt([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CreatedDate desc, PurchaseOrderNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            whereClause = " and PurchaseOrder.OrderStatus = 'Authorised' ";
            whereClause += " and (DeliveryStatus = 'No Delivery' OR DeliveryStatus = 'Partial Delivery') ";

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and PurchaseOrder.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            PurchaseOrder objPurchaseOrder = new PurchaseOrder();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPurchaseOrder, SystemEnum.Pages.GoodReceipt.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request), 
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the purchase order page.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="workOrderNo">The work order no.</param>
        /// <returns></returns>
        [ActionName(Actions.GoodReceiptDetail)]
        public ActionResult GoodReceiptDetail(int id = 0, string workOrderNo = "")
        {
            if (ProjectSession.PermissionAccess.Purchasing_GoodsReceipt_Allowaccess && ProjectSession.PermissionAccess.Purchasing__ShowHideModule)
            {
                PurchaseOrder objPurchaseOrder = new PurchaseOrder();
                objPurchaseOrder.ID = id;
                if (id > 0)
                {
                    objPurchaseOrder = GoodReceiptService.GetGoodReceiptDetail(id);
                    objPurchaseOrder.IsAnyPOItemReceivedOrCancelled = PurchaseService.IsAnyPOItemReceivedOrCancelled(id);
                }
                else
                {
                    if (!ProjectSession.IsItemNoAutoIncrement)
                    {
                        using (ServiceContext context = new ServiceContext())
                        {
                            employees obj = new employees();
                            obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                            objPurchaseOrder.L2ID = Convert.ToInt32(obj.L2ID);
                            objPurchaseOrder.MaintDivisionID = obj.MaintDivisionID;
                            objPurchaseOrder.MaintDeptID = obj.MaintDeptID;
                            objPurchaseOrder.MaintsubdeptID = obj.MaintSubDeptID;
                        }
                    }
                    else
                    {
                        objPurchaseOrder.OrderBy = ProjectSession.EmployeeID;
                        objPurchaseOrder.OrderByEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseOrder.OrderByEmployeeName = ProjectSession.EmployeeName;

                        objPurchaseOrder.InvoiceTo = ProjectSession.EmployeeID;
                        objPurchaseOrder.InvoiceToEmployeeNO = ProjectSession.EmployeeNo;
                        objPurchaseOrder.InvoiceToEmployeeName = ProjectSession.EmployeeName;
                    }
                }

                return View(Views.GoodReceiptDetail, objPurchaseOrder);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        #region "PO Transaction List"

        /// <summary>
        /// Gets the po items by po id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPOItemsByPOID)]
        public ActionResult GetPOItemsByPOID(int id)
        {
            PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objPurchaseOrderItem).Where(o => o.PurchaseOrderID == id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the po item list by po item identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPOItemListByPOItemID)]
        public ActionResult GetPOItemListByPOItemID(int id)
        {
            PoItemDetailModal objPoItemDetailModal = new PoItemDetailModal();
            if (id > 0)
            {
                GoodReceiptService objService = new GoodReceiptService();
                objPoItemDetailModal = objService.GetTranDetailByPOItemID(id);
            }

            return Json(objPoItemDetailModal, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Calculates all items summary.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.CalculateAllItemsSummary)]
        public ActionResult CalculateAllItemsSummary(int id)
        {
            PoItemDetailModal objPoItemDetailModal = new PoItemDetailModal();
            if (id > 0)
            {
                bool flag = false;
                GoodReceiptService objService = new GoodReceiptService();
                objPoItemDetailModal = objService.CalculateAllItemsSummary(id);
                PurchaseOrder objPurchaseOrder = new PurchaseOrder();
                using (DapperContext context = new DapperContext())
                {
                    objPurchaseOrder = context.SelectObject<PurchaseOrder>(id);
                }

                if (objPoItemDetailModal.Balance == 0)
                {
                    if (objPurchaseOrder.DeliveryStatus != "Full Delivery")
                    {
                        objPurchaseOrder.DeliveryStatus = "Full Delivery";
                        flag = objService.UpdatePurchaseOrder(objPurchaseOrder);
                    }
                }
                else if (objPoItemDetailModal.Balance > 0 && objPoItemDetailModal.Balance != objPoItemDetailModal.TotalOrdered)
                {
                    if (objPurchaseOrder.DeliveryStatus != "Partial Delivery")
                    {
                        objPurchaseOrder.DeliveryStatus = "Partial Delivery";
                        flag = objService.UpdatePurchaseOrder(objPurchaseOrder);
                    }
                }

                objPoItemDetailModal.OrderStatus = objPurchaseOrder.OrderStatus;
                objPoItemDetailModal.Status = objPurchaseOrder.DeliveryStatus;
            }

            return Json(objPoItemDetailModal, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the po item transactions.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="allItemdone">All item done.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPOItemTransactions)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetPOItemTransactions([DataSourceRequest]DataSourceRequest request, int id, string allItemdone)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "TransactionID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            GoodReceiptService objService = new GoodReceiptService();
            var result = new DataSourceResult()
            {
                Data = objService.GetPOItemTransactions(pageNumber, sortExpression, sortDirection, id, allItemdone, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the po store by po id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPOStoreByPOID)]
        public ActionResult GetPOStoreByPOID(int id)
        {
            IList<Substore> result = new List<Substore>();
            if (id > 0)
            {
                GoodReceiptService objService = new GoodReceiptService();
                result = objService.GetPOStoreByPOID(id);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Save PO Item"

        /// <summary>
        /// Manages the ra receive all.
        /// </summary>
        /// <param name="objReceive">The object receive.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageRAReceiveAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageRAReceiveAll(Receive objReceive)
        {
            string validations = string.Empty;

            objReceive.CreatedDate = DateTime.Now;
            objReceive.CreatedBy = ProjectSession.EmployeeID.ToString();
            objReceive.Date = objReceive.Date;
            objReceive.SubStoreID = objReceive.SubStoreID;
            objReceive.MRNNo = objReceive.MRNNo;
            objReceive.InvoiceDate = objReceive.InvoiceDate;
            objReceive.DeliveryOrderNo = objReceive.DeliveryOrderNo;
            objReceive.ID = objReceive.ID;
            GoodReceiptService objService = new GoodReceiptService();
            bool flag = objService.ReceiveAllPOItems(objReceive);
            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.GoodsReceipt_MsgAllItemreceivedsuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Cancels the po item.
        /// </summary>
        /// <param name="objTran">The object tran.</param>
        /// <returns></returns>
        [ActionName(Actions.CancelPOItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult CancelPOItem(Tran objTran)
        {
            PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();
            Tran objNewTran = new Tran();
            using (DapperContext context = new DapperContext())
            {
                objPurchaseOrderItem = context.SelectObject<PurchaseOrderItem>(ConvertTo.Integer(objTran.PurchaseOrderItemsId));
                decimal? totalQty = context.SearchAll(objNewTran).Where(o => o.PurchaseOrderItemsId == ConvertTo.Integer(objTran.PurchaseOrderItemsId)).Sum(o => o.Quantity);
                if (objTran.Quantity > objPurchaseOrderItem.Quantity - totalQty)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.GoodsReceipt_MsgQuantityCheck });
                }
            }

            GoodReceiptService objService = new GoodReceiptService();
            bool flag = objService.CancelPOItem(objTran);

            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.GoodsReceipt_MsgItemcancellsuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Manages the new receive item.
        /// </summary>
        /// <param name="objReceive">The object receive.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageNewReceiveItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageNewReceiveItem(Receive objReceive)
        {
            PurchaseOrderItem objPurchaseOrderItem = new PurchaseOrderItem();
            PurchaseOrder objPurchaseOrder = new PurchaseOrder();
            Tran objNewTran = new Tran();
            using (DapperContext context = new DapperContext())
            {
                objPurchaseOrderItem = context.SelectObject<PurchaseOrderItem>(ConvertTo.Integer(objReceive.POItemID));
                objPurchaseOrder = context.SelectObject<PurchaseOrder>(ConvertTo.Integer(objReceive.ID));
                decimal? totalQty = context.SearchAll(objNewTran).Where(o => o.PurchaseOrderItemsId == ConvertTo.Integer(objReceive.POItemID)).Sum(o => o.Quantity);
                if (objReceive.QtyRec > objPurchaseOrderItem.Quantity - totalQty)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.GoodsReceipt_MsgQuantityCheck });
                }
            }

            objReceive.POItemID = objReceive.POItemID;
            objReceive.CreatedBy = ProjectSession.EmployeeID.ToString();
            objReceive.CreatedDate = DateTime.Now;
            objReceive.Date = objReceive.Date;
            objReceive.StockDesc = objPurchaseOrderItem.PartDescription;
            objReceive.PRNo = objPurchaseOrderItem.PurchaseRequestNo;
            objReceive.WONo = objPurchaseOrderItem.JobOrderID;
            objReceive.L2ID = objPurchaseOrder.L2ID;
            objReceive.SubStoreID = objReceive.SubStoreID;
            objReceive.StockID = objPurchaseOrderItem.PartNumber;
            objReceive.MRNNo = objReceive.MRNNo;
            objReceive.PONo = objPurchaseOrder.PurchaseOrderNo;
            objReceive.InvoiceDate = objReceive.InvoiceDate;
            objReceive.DeliveryOrderNo = objReceive.DeliveryOrderNo;
            objReceive.Price = objPurchaseOrderItem.UnitPrice * objReceive.QtyRec;
            objReceive.QtyRec = objReceive.QtyRec;

            objReceive.SupplierID = objPurchaseOrder.SupplierID;

            GoodReceiptService objService = new GoodReceiptService();
            bool flag = objService.NewReceiveItem(objReceive);

            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.GoodsReceipt_MsgItemreceivedsuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        #endregion
    }
}