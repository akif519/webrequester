﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Transaction.Controllers
{
    /// <summary>
    /// Meter Jo Generation Controller
    /// </summary>
    public partial class TransactionController : BaseController
    {
        /// <summary>
        /// Pm meter the jo generation.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PMMeterJoGeneration)]
        public ActionResult PMMeterJoGeneration()
        {
            if (ProjectSession.PermissionAccess.Preventive_TimeSchedules_Allowaccess && ProjectSession.PermissionAccess.Preventive__ShowHideModule)
            {
                PmSchedule objPmSchedule = new PmSchedule();
                using (ServiceContext context = new ServiceContext())
                {
                    employees obj = new employees();
                    obj = context.SelectObject<employees>(ProjectSession.EmployeeID);
                    objPmSchedule.L2ID = obj.L2ID;
                    objPmSchedule.MaintDivisionID = obj.MaintDivisionID;
                    objPmSchedule.MaintDeptID = obj.MaintDeptID;
                    objPmSchedule.MaintSubDeptID = obj.MaintSubDeptID;
                }

                return View(Views.PMMeterJoGeneration, objPmSchedule);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// bind threshold pm meter jo
        /// </summary>
        /// <param name="request">the request</param>
        /// <param name="meterType">Type of the meter.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMeterIntermediateDataByUserId)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetMeterIntermediateDataByUserId([DataSourceRequest]DataSourceRequest request, string meterType)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MeterNo";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;
            if (meterType.ToLower() == "threshold")
            {
                whereClause = " and intermediate_pmmetersgenerate.Userid = " + ProjectSession.EmployeeID + " AND MeterType = 'Threshold' ";
            }
            else if (meterType.ToLower() == "incremental")
            {
                whereClause = "  and intermediate_pmmetersgenerate.Userid = " + ProjectSession.EmployeeID + " AND MeterType = 'Incremental' ";
            }
            else
            {
                whereClause = "  and intermediate_pmmetersgenerate.Userid = " + ProjectSession.EmployeeID + " AND MeterType = '' ";
            }

            Intermediate_pmmetersgenerate objIntermediate_pmmetersgenerate = new Intermediate_pmmetersgenerate();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objIntermediate_pmmetersgenerate, SystemEnum.Pages.PMMetersJOGenerate.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Views the pm meter.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance div.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.ViewPMMeter)]
        public JsonResult ViewPMMeter(int cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            try
            {
                PMMeterJoGenerationService objService = new PMMeterJoGenerationService();
                objService.ClearPMLockMetersIfStuck(ProjectSession.EmployeeID);
                objService.SaveMeterIntermediateData(ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                return Json("success");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Opens the pm meter jo.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="maintenanceDiv">The maintenance div.</param>
        /// <param name="maintenanceDept">The maintenance department.</param>
        /// <param name="maintenanceSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.OpenPMMeterJo)]
        public JsonResult OpenPMMeterJo(int cityID, int? maintenanceDiv, int? maintenanceDept, int? maintenanceSubDept)
        {
            try
            {
                PMMeterJoGenerationService objService = new PMMeterJoGenerationService();
                bool result = objService.SetPMLockMeters(ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                if (result == false)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PMJOGeneration_MsgPMMeterJOGenInProgress }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objService.SaveMeterIntermediateData(ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                    objService.GeneratePMMeterJO(ProjectSession.EmployeeID, cityID, Convert.ToInt32(maintenanceDiv), Convert.ToInt32(maintenanceDept), Convert.ToInt32(maintenanceSubDept));
                    return Json("success");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}