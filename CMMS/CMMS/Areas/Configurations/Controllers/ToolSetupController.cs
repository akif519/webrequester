﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Tool Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Tool Category"

        /// <summary>
        /// Tools the category.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ToolCategory)]
        public ActionResult ToolCategory()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_ToolCategory_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.ToolCategory);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the tool category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetToolCategory([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ToolCategoryCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ToolCategory objToolCategory = new ToolCategory();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objToolCategory, SystemEnum.Pages.ToolCategory.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the tool category by identifier.
        /// </summary>
        /// <param name="toolCategoryId">The tool category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolCategoryByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetToolCategoryByID(int toolCategoryId)
        {
            ToolCategory objToolCategory = new ToolCategory();
            if (toolCategoryId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objToolCategory = objDapperContext.SelectObject<ToolCategory>(toolCategoryId);
                }
            }

            return Json(objToolCategory, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the tool category.
        /// </summary>
        /// <param name="objToolCategory">The object tool category.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageToolCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageToolCategory(ToolCategory objToolCategory)
        {
            string validations = string.Empty;

            if (objToolCategory.ToolCategoryID > 0)
            {
                objToolCategory.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objToolCategory.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolCategory_ChangeMastersCodes)
                {
                    objToolCategory.ToolCategoryCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolCategory_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objToolCategory.CreatedBy = ProjectSession.EmployeeID.ToString();
                objToolCategory.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolCategory_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("ToolCategoryCode"))
                {
                    if (objDapperContext.Save(objToolCategory) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ToolSetups_MsgCategoryCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the tool category.
        /// </summary>
        /// <param name="toolCategoryId">The tool category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteToolCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteToolCategory(int toolCategoryId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<ToolCategory>(toolCategoryId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the tool categories.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetToolCategories)]
        public ActionResult GetToolCategories()
        {
            ToolCategory objToolCategory = new ToolCategory();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objToolCategory);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Tool Sub Category"

        /// <summary>
        /// Tools the sub category.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ToolSubCategory)]
        public ActionResult ToolSubCategory()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_ToolSubCategory_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.ToolSubCategory);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the tool sub category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolSubCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetToolSubCategory([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ToolSubCategoryCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ToolSubCategory objToolSubCategory = new ToolSubCategory();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objToolSubCategory, SystemEnum.Pages.ToolSubCategory.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the tool sub category by identifier.
        /// </summary>
        /// <param name="toolSubCategoryId">The tool sub category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolSubCategoryByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetToolSubCategoryByID(int toolSubCategoryId)
        {
            ToolSubCategory objToolSubCategory = new ToolSubCategory();
            if (toolSubCategoryId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objToolSubCategory = objDapperContext.SelectObject<ToolSubCategory>(toolSubCategoryId);
                }
            }

            return Json(objToolSubCategory, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the tool sub category.
        /// </summary>
        /// <param name="objToolSubCategory">The object tool sub category.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageToolSubCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageToolSubCategory(ToolSubCategory objToolSubCategory)
        {
            string validations = string.Empty;

            if (objToolSubCategory.ToolSubCategoryID > 0)
            {
                objToolSubCategory.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objToolSubCategory.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolSubCategory_ChangeMastersCodes)
                {
                    objToolSubCategory.ToolSubCategoryCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolSubCategory_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objToolSubCategory.CreatedBy = ProjectSession.EmployeeID.ToString();
                objToolSubCategory.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_ToolSubCategory_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("ToolSubCategoryCode", "ToolCategoryID", true))
                {
                    if (objDapperContext.Save(objToolSubCategory) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ToolSetups_MsgSubCategoryCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the tool sub category.
        /// </summary>
        /// <param name="toolSubCategoryId">The tool sub category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteToolSubCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteToolSubCategory(int toolSubCategoryId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<ToolSubCategory>(toolSubCategoryId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the tool sub categories.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetToolSubCategories)]
        public ActionResult GetToolSubCategories()
        {
            ToolSubCategory objToolSubCategory = new ToolSubCategory();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objToolSubCategory);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the tool sub category by category identifier.
        /// </summary>
        /// <param name="toolCategoryId">The tool category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolSubCategoryByCategoryID)]
        public ActionResult GetToolSubCategoryByCategoryID(int toolCategoryId)
        {
            ToolSubCategory objToolSubCategory = new ToolSubCategory();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objToolSubCategory).Where(o => o.ToolCategoryID == toolCategoryId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Tool"

        /// <summary>
        /// Tools this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Tool)]
        public ActionResult Tool()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_Tools_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.Tool);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the tool.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetTool)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetTool([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ToolsCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Tool objTool = new Tool();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objTool, SystemEnum.Pages.Tool.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the tool by sub category and category identifier.
        /// </summary>
        /// <param name="toolSubCategoryId">The tool sub category identifier.</param>
        /// <param name="toolCategoryId">The tool category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolsByCatAndSubCat)]
        public ActionResult GetToolsByCatAndSubCat(int toolSubCategoryId, int toolCategoryId)
        {
            Tool objTool = new Tool();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objTool).Where(o => o.ToolCategoryID == toolCategoryId && o.ToolSubCategoryID == toolSubCategoryId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the tool by identifier.
        /// </summary>
        /// <param name="toolId">The tool identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetToolByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetToolByID(int toolId)
        {
            Tool objTool = new Tool();
            if (toolId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objTool = objDapperContext.SelectObject<Tool>(toolId);
                }
            }

            return Json(objTool, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the tool.
        /// </summary>
        /// <param name="objTool">The object tool.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageTool)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageTool(Tool objTool)
        {
            string validations = string.Empty;

            if (objTool.ToolsID > 0)
            {
                objTool.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objTool.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_Tools_ChangeMastersCodes)
                {
                    objTool.ToolsCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Tools_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objTool.CreatedBy = ProjectSession.EmployeeID.ToString();
                objTool.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Tools_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("ToolsCode", "ToolSubCategoryID", true))
                {
                    if (objDapperContext.Save(objTool) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ToolSetups_MsgToolCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the tool.
        /// </summary>
        /// <param name="toolId">The tool identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteTool)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteTool(int toolId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Tool>(toolId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion
    }
}