﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Sites the class.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SiteClass)]
        public ActionResult SiteClass()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_CityClass_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.SiteClass);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the site class.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSiteClass)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSiteClass([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L2ClassCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L2Classe siteClassSearch = new L2Classe();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(siteClassSearch, SystemEnum.Pages.SiteClass.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the site class by identifier.
        /// </summary>
        /// <param name="siteClassId">The site class identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSiteClassByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSiteClassByID(int siteClassId)
        {
            L2Classe siteClass = new L2Classe();
            if (siteClassId > 0)
            {
                using (ServiceContext siteClassService = new ServiceContext())
                {
                    siteClass = siteClassService.SelectObject<L2Classe>(siteClassId);
                }
            }

            return Json(siteClass, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the site class.
        /// </summary>
        /// <param name="siteClass">The site class.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSiteClass)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSiteClass(L2Classe siteClass)
        {
            string validations = string.Empty;

            if (siteClass.L2ClassID > 0)
            {
                siteClass.ModifiedBy = ProjectSession.EmployeeID.ToString();
                siteClass.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CityClass_ChangeMastersCodes)
                {
                    siteClass.L2ClassCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CityClass_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                siteClass.CreatedBy = ProjectSession.EmployeeID.ToString();
                siteClass.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CityClass_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext siteClassService = new ServiceContext("L2ClassCode"))
                {
                    if (siteClassService.Save(siteClass) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityClassAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the site class.
        /// </summary>
        /// <param name="siteClassId">The site class identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSiteClass)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSiteClass(int siteClassId)
        {
            using (ServiceContext siteClassService = new ServiceContext())
            {
                int returnValue = siteClassService.Delete<L2Classe>(siteClassId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the site classes.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetSiteClasses)]
        public ActionResult GetSiteClasses()
        {
            L2Classe siteClass = new L2Classe();
            using (ServiceContext siteClassService = new ServiceContext())
            {
                var result = siteClassService.Search(siteClass, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}