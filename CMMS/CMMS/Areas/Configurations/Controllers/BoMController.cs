﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Service;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// BOM Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// BoM list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.BoMList)]
        public ActionResult BoMList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_BoMList_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.BoMList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the bo m list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBoMList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBoMList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartsListNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartsList objPartsList = new PartsList();
            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPartsList, SystemEnum.Pages.BoMList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the items list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="partsListId">The parts list identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBoMItemsList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBoMIteamsList([DataSourceRequest]DataSourceRequest request, int? partsListId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "StockNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartsList objPartsList = new PartsList();
            BoMService obj = new BoMService();
            var result = new DataSourceResult()
            {
                Data = obj.GetBoMDetailsByBoMId(Convert.ToInt32(partsListId), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result);
        }

        /// <summary>
        /// _s the edit bo m list.
        /// </summary>
        /// <param name="partsListId">The parts list identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._EditBoMList)]
        public PartialViewResult _EditBoMList(int partsListId)
        {
            PartsList objPartsList = new PartsList();
            if (partsListId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objPartsList = context.SelectObject<PartsList>(partsListId);
                }
            }

            return PartialView(Pages.PartialViews.EditBoMList, objPartsList);
        }

        /// <summary>
        /// Gets the stock code item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodeItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetStockCodeItem([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "StockNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            StockCode objStockCode = new StockCode();

            BoMService obj = new BoMService();
            var result = new DataSourceResult()
            {
                Data = obj.GetAllItemDetail(pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the bo m list item.
        /// </summary>
        /// <param name="objPartsListDetail">The object parts list detail.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageBoMListItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageBoMListItem(PartsListDetail objPartsListDetail)
        {
            string validations = string.Empty;

            if (objPartsListDetail.hdnPartsListDetailID > 0)
            {
                objPartsListDetail.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objPartsListDetail.ModifiedDate = DateTime.Now;
            }
            else
            {
                objPartsListDetail.CreatedBy = ProjectSession.EmployeeID.ToString();
                objPartsListDetail.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                BoMService context = new BoMService();

                if (context.InsertUpdatePartListItem(objPartsListDetail))
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Manages the bo m list.
        /// </summary>
        /// <param name="objPartsList">The object parts list.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageBoMList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageBoMList(PartsList objPartsList)
        {
            string validations = string.Empty;

            if (objPartsList.PartsListID > 0)
            {
                objPartsList.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objPartsList.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_BoMList_ChangeMastersCodes)
                {
                    objPartsList.PartsListNo = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_BoMList_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objPartsList.CreatedBy = ProjectSession.EmployeeID.ToString();
                objPartsList.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_BoMList_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext siteClassService = new ServiceContext("PartsListNo"))
                {
                    if (siteClassService.Save(objPartsList) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.BoM_MsgBOMNoAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the bo m list.
        /// </summary>
        /// <param name="partsListID">The parts list identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteBoMList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteBoMList(int partsListID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<PartsList>(partsListID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Deletes the bo m list item.
        /// </summary>
        /// <param name="stockID">The stock identifier.</param>
        /// <param name="partsListID">The parts list identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteBoMListItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteBoMListItem(int stockID, int partsListID)
        {
            BoMService context = new BoMService();

            if (context.DeletePartListItem(stockID, partsListID))
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}