﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Sites this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Site)]
        public ActionResult Site()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_City_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                IEnumerable<SelectListItem> status = Enum.GetValues(typeof(SystemEnum.Status)).Cast<SystemEnum.Status>().Select(x => new SelectListItem()
                {
                    Text = SystemEnum.GetEnumName(typeof(SystemEnum.Status), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();
                ViewBag.Status = status;

                IEnumerable<SelectListItem> defaultCalendar = Enum.GetValues(typeof(SystemEnum.DefaultCalendar)).Cast<SystemEnum.DefaultCalendar>().Select(x => new SelectListItem()
                {
                    Text = SystemEnum.GetEnumName(typeof(SystemEnum.DefaultCalendar), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();
                ViewBag.DefaultCalendar = defaultCalendar;

                IEnumerable<SelectListItem> timeZone = Enum.GetValues(typeof(SystemEnum.TimeZone)).Cast<SystemEnum.TimeZone>().Select(x => new SelectListItem()
                {
                    Text = SystemEnum.GetEnumName(typeof(SystemEnum.TimeZone), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();
                ViewBag.TimeZone = timeZone;

                return View(Views.Site);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the site.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSite)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSite([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L2Code";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L2 siteSearch = new L2();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L2.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(siteSearch, SystemEnum.Pages.Site.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the site.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="orgID">The organization identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSiteByPermissionAndL1ID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSiteByPermissionAndL1ID([DataSourceRequest]DataSourceRequest request, int empID, int orgID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L2Code";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L2Service objService = new L2Service();
            var result = new DataSourceResult()
            {
                Data = objService.GetCityByPermissionAndL1ID(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, orgID, pageNumber, sortExpression, sortDirection, request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the site by identifier.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSiteByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSiteByID(int siteId)
        {
            L2 site = new L2();
            if (siteId > 0)
            {
                using (ServiceContext siteService = new ServiceContext())
                {
                    site = siteService.SelectObject<L2>(siteId);
                }
            }

            return Json(site, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the site.
        /// </summary>
        /// <param name="objSite">The object site.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSite)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSite(L2 objSite)
        {
            string validations = string.Empty;
            bool isEditModeAndEmpNotCentral = false;

            if (objSite.L2ID > 0)
            {
                objSite.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objSite.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_City_ChangeMastersCodes)
                {
                    objSite.L2Code = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_City_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSite.CreatedBy = ProjectSession.EmployeeID.ToString();
                objSite.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_City_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext siteService = new ServiceContext("L2Code"))
                {
                    if (objSite.L2ID == 0 && ProjectSession.IsCentral == false)
                    {
                        isEditModeAndEmpNotCentral = true;
                    }

                    if (siteService.Save(objSite) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, isEditModeAndEmpNotCentral });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the site.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSite)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSite(int siteId)
        {
            using (ServiceContext siteService = new ServiceContext())
            {
                int returnValue = siteService.Delete<L2>(siteId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the cities by employee.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="orgID">The city by L1.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCitiesByEmployee)]
        public ActionResult GetCitiesByEmployee(int? cityID, int? orgID = 0)
        {
            var result = L2Service.GetCitiesByEmployee(ProjectSession.EmployeeID, cityID, orgID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets all cities.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="orgID">The city by L1.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllCities)]
        public ActionResult GetAllCities(int? cityID, int? orgID = 0)
        {
            L2 objL2 = new L2();
            int? status = 0;
            var result = new List<L2>();
            using (DapperContext objContext = new DapperContext())
            {
                if (cityID > 0)
                {
                    L2 objL2Check = new L2();
                    status = objContext.SelectObject<L2>(Convert.ToInt32(cityID)).Status;
                    if (status == 1)
                    {
                        if (orgID > 0)
                        {
                            result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1 && x.L1ID == orgID).ToList();
                        }
                        else
                        {
                            result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1).ToList();
                        }
                    }
                    else
                    {
                        if (orgID > 0)
                        {
                            result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1 && x.L1ID == orgID).Union(objContext.SearchAll<L2>(objL2).Where(x => x.L2ID == cityID)).ToList();
                        }
                        else
                        {
                            result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1).Union(objContext.SearchAll<L2>(objL2).Where(x => x.L2ID == cityID)).ToList();
                        }
                    }
                }
                else
                {
                    if (orgID > 0)
                    {
                        result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1 && x.L1ID == orgID).ToList();
                    }
                    else
                    {
                        result = objContext.SearchAll<L2>(objL2).Where(x => x.Status == 1).ToList();
                    }
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the l2 information by identifier.
        /// </summary>
        /// <param name="siteID">The l2 identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetL2InformationById)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetL2InformationById(int siteID)
        {
            L2 objL2 = new L2();
            Sector objSector = new Sector();
            using (DapperContext context = new DapperContext())
            {
                objL2.L2ID = siteID;
                objL2 = context.SearchAllByID<L2>(objL2, objL2.L2ID).FirstOrDefault();

                if (objL2 != null && objL2.SectorID > 0)
                {
                    objSector.SectorID = Convert.ToInt32(objL2.SectorID);
                    objSector = context.SearchAllByID<Sector>(objSector, objSector.SectorID).FirstOrDefault();
                }
                else
                {
                    objSector = new Sector();
                }
            }

            return Json(objSector, JsonRequestBehavior.AllowGet);
        }
    }
}