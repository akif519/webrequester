﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Model;
using CMMS.Services;
using CMMS.Infrastructure;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Failure Code Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.FailureCauseList)]
        public ActionResult FailureCauseList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_FailureCodes_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.FailureCauseList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetFailureCauseList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetFailureCauseList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "FailureCauseCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Failurecause areaSearch = new Failurecause();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.FailureCause.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the failure cause list by identifier.
        /// </summary>
        /// <param name="failureCauseID">The failure cause identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetFailureCauseListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetFailureCauseListByID(int failureCauseID)
        {
            Failurecause objFailurecause = new Failurecause();
            if (failureCauseID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objFailurecause = context.SelectObject<Failurecause>(failureCauseID);
                }
            }

            return Json(objFailurecause, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllFailureCauseListForDDL)]
        [HttpGet]
        public ActionResult GetAllFailureCauseListForDDL()
        {
            Failurecause objFailurecause = new Failurecause();
            IList<Failurecause> lstFailureCause = new List<Failurecause>();
            using (DapperContext context = new DapperContext())
            {
                lstFailureCause = context.Search<Failurecause>(objFailurecause);
            }

            return Json(lstFailureCause, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the failure cause.
        /// </summary>
        /// <param name="objFailureCause">The object failure cause.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageFailureCause)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageFailureCause(Failurecause objFailureCause)
        {
            string validations = string.Empty;

            if (objFailureCause.FailureCauseID > 0)
            {
                objFailureCause.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objFailureCause.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_FailureCodes_ChangeMastersCodes)
                {
                    objFailureCause.FailureCauseCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_FailureCodes_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objFailureCause.CreatedBy = ProjectSession.EmployeeID.ToString();
                objFailureCause.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_FailureCodes_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("FailureCauseCode"))
                {
                    if (context.Save(objFailureCause) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.FailureCodes_MsgFailureCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the failure cause.
        /// </summary>
        /// <param name="failureCauseID">The failure cause identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteFailureCause)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteFailureCause(int failureCauseID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Failurecause>(failureCauseID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}