﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Job Management Setup Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Job Priority"

        /// <summary>
        /// Jobs the priority.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobPriority)]
        public ActionResult JobPriority()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_JobPriority_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JobPriority);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the job priority.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobPriority)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobPriority([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkPriority";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Workpriority jobPrioritySearch = new Workpriority();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(jobPrioritySearch, SystemEnum.Pages.JobPriority.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the job priority by identifier.
        /// </summary>
        /// <param name="jobPriorityId">The job priority identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobPriorityByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobPriorityByID(int jobPriorityId)
        {
            Workpriority objJobPriority = new Workpriority();
            IList<Workpriority> lstWorkPriority = new List<Workpriority>();

            if (jobPriorityId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobPriority = context.SelectObject<Workpriority>(jobPriorityId);
                }

                return Json(objJobPriority, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobPriority = new Workpriority();
                    lstWorkPriority = context.SearchAll<Workpriority>(objJobPriority);
                }

                return Json(lstWorkPriority, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the job priority.
        /// </summary>
        /// <param name="objJobPriority">The object job priority.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobPriority)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobPriority(Workpriority objJobPriority)
        {
            string validations = string.Empty;

            if (objJobPriority.WorkPriorityID > 0)
            {
                objJobPriority.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objJobPriority.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobPriority_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJobPriority.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJobPriority.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobPriority_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("WorkPriority"))
                {
                    objJobPriority.WorkPriorityID = context.Save(objJobPriority);
                    if (objJobPriority.WorkPriorityID > 0)
                    {
                        if (Convert.ToBoolean(objJobPriority.IsDefault))
                        {
                            Workpriority objOldJobPriority = new Workpriority();
                            var oldResult = context.SearchAll(objOldJobPriority).Where(c => c.IsDefault == true && c.WorkPriorityID != objJobPriority.WorkPriorityID).FirstOrDefault();
                            if (oldResult != null)
                            {
                                oldResult.IsDefault = false;
                                oldResult.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                oldResult.ModifiedDate = DateTime.Now;
                                context.Save(oldResult);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobPriority_MsgJobPriorityAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the job priority.
        /// </summary>
        /// <param name="jobPriorityId">The job priority identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobPriority)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobPriority(int jobPriorityId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Workpriority>(jobPriorityId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Job Status"

        /// <summary>
        /// Jobs the status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobStatus)]
        public ActionResult JobStatus()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_JobStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JobStatus);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the job status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobStatus([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkStatus";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Workstatu jobStatusSearch = new Workstatu();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(jobStatusSearch, SystemEnum.Pages.JobStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// To Get All Job Status
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllJobStatusForDDL)]
        [HttpGet]
        public JsonResult GetAllJobStatusForDDL()
        {
            try
            {
                JobRequestService objService = new JobRequestService();
                Workstatu objWS = new Workstatu();
                IList<Workstatu> lstWorkStatus = new List<Workstatu>();

                using (DapperContext context = new DapperContext())
                {
                    lstWorkStatus = context.SearchAll<Workstatu>(objWS);
                }

                return Json(lstWorkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.GetAllJobStatusForDDLForJO)]
        [HttpGet]
        public JsonResult GetAllJobStatusForDDLForJO()
        {
            try
            {
                JobRequestService objService = new JobRequestService();
                Workstatu objWS = new Workstatu();
                IList<Workstatu> lstWorkStatus = new List<Workstatu>();

                using (DapperContext context = new DapperContext())
                {
                    lstWorkStatus = context.SearchAll<Workstatu>(objWS);
                }
                if (lstWorkStatus.Count > 0)
                {
                    lstWorkStatus = lstWorkStatus.Where(x => x.WorkStatus.ToLower() != "closed" && x.WorkStatus.ToLower() != "cancelled").ToList();
                }

                return Json(lstWorkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName(Actions.GetAllJobStatusForDDLForJR)]
        [HttpGet]
        public JsonResult GetAllJobStatusForDDLForJR()
        {
            try
            {
                JobRequestService objService = new JobRequestService();
                Workstatu objWS = new Workstatu();
                IList<Workstatu> lstWorkStatus = new List<Workstatu>();

                using (DapperContext context = new DapperContext())
                {
                    lstWorkStatus = context.SearchAll<Workstatu>(objWS);
                }
                //if (lstWorkStatus.Count > 0)
                //{
                //    lstWorkStatus = lstWorkStatus.Where(x => x.WorkStatus.ToLower() == "open").ToList();
                //}
                return Json(lstWorkStatus, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the job status by identifier.
        /// </summary>
        /// <param name="jobStatusId">The job status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobStatusByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobStatusByID(int jobStatusId)
        {
            Workstatu objJobStatus = new Workstatu();
            IList<Workstatu> lstJobStatus = new List<Workstatu>();
            if (jobStatusId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobStatus = context.SelectObject<Workstatu>(jobStatusId);
                }

                return Json(objJobStatus, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    lstJobStatus = context.SearchAll<Workstatu>(objJobStatus);
                }

                return Json(lstJobStatus, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the job status.
        /// </summary>
        /// <param name="objJobStatus">The object job status.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobStatus(Workstatu objJobStatus)
        {
            string validations = string.Empty;

            if (objJobStatus.WorkStatusID > 0)
            {
                objJobStatus.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objJobStatus.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJobStatus.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJobStatus.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("WorkStatus"))
                {
                    objJobStatus.WorkStatusID = context.Save(objJobStatus);
                    if (objJobStatus.WorkStatusID > 0)
                    {
                        if (Convert.ToBoolean(objJobStatus.IsDefault))
                        {
                            Workstatu objOldJobStatus = new Workstatu();
                            var oldResult = context.SearchAll(objOldJobStatus).Where(c => c.IsDefault == true && c.WorkStatusID != objJobStatus.WorkStatusID).FirstOrDefault();
                            if (oldResult != null)
                            {
                                oldResult.IsDefault = false;
                                oldResult.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                oldResult.ModifiedDate = DateTime.Now;
                                context.Save(oldResult);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobStatus_MsgJobStatusReq });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the job status.
        /// </summary>
        /// <param name="jobStatusId">The job status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobStatus(int jobStatusId)
        {
            using (DapperContext context = new DapperContext())
            {
                if (jobStatusId >= 1 && jobStatusId <= 7)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSystemReservedDeleteMsg }, JsonRequestBehavior.AllowGet);
                }

                int returnValue = context.Delete<Workstatu>(jobStatusId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Job Type"

        /// <summary>
        /// Jobs the type.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobType)]
        public ActionResult JobType()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_JobType_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JobType);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the type of the job.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobType([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkTypeDescription";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Worktype jobTypeSearch = new Worktype();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(jobTypeSearch, SystemEnum.Pages.JobType.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the job type by identifier.
        /// </summary>
        /// <param name="jobTypeId">The job type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobTypeByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobTypeByID(int jobTypeId)
        {
            Worktype objJobType = new Worktype();
            IList<Worktype> lstWorktype = new List<Worktype>();
            if (jobTypeId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobType = context.SelectObject<Worktype>(jobTypeId);
                }

                return Json(objJobType, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    lstWorktype = context.SearchAll<Worktype>(objJobType);
                }

                return Json(lstWorktype, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the job Type.
        /// </summary>
        /// <param name="objJobType">Type of the object job.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobType(Worktype objJobType)
        {
            string validations = string.Empty;

            if (objJobType.WorkTypeID > 0)
            {
                objJobType.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objJobType.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobType_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJobType.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJobType.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobType_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("WorkTypeDescription"))
                {
                    objJobType.WorkTypeID = context.Save(objJobType);
                    if (objJobType.WorkTypeID > 0)
                    {
                        if (Convert.ToBoolean(objJobType.IsDefault))
                        {
                            Worktype objOldJobType = new Worktype();
                            var oldResult = context.SearchAll(objOldJobType).Where(c => c.IsDefault == true && c.WorkTypeID != objJobType.WorkTypeID).FirstOrDefault();
                            if (oldResult != null)
                            {
                                oldResult.IsDefault = false;
                                oldResult.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                oldResult.ModifiedDate = DateTime.Now;
                                context.Save(oldResult);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobType_MsgJobTypeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the job type.
        /// </summary>
        /// <param name="jobTypeId">The job type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobType(int jobTypeId)
        {
            using (DapperContext context = new DapperContext())
            {
                if (jobTypeId >= 1 && jobTypeId <= 8)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSystemReservedDeleteMsg }, JsonRequestBehavior.AllowGet);
                }

                int returnValue = context.Delete<Worktype>(jobTypeId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Job Trade"

        /// <summary>
        /// Jobs the trade.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobTrade)]
        public ActionResult JobTrade()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_JobTrade_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JobTrade);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the job trade.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobTrade)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobTrade([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkTrade";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Worktrade jobTradeSearch = new Worktrade();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(jobTradeSearch, SystemEnum.Pages.JobTrade.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the job trade by identifier.
        /// </summary>
        /// <param name="jobTradeId">The job trade identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobTradeByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobTradeByID(int jobTradeId)
        {
            Worktrade objJobTrade = new Worktrade();
            IList<Worktrade> lstWorkTrade = new List<Worktrade>();

            if (jobTradeId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobTrade = context.SelectObject<Worktrade>(jobTradeId);
                }

                return Json(objJobTrade, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobTrade = new Worktrade();
                    lstWorkTrade = context.SearchAll<Worktrade>(objJobTrade);
                }

                return Json(lstWorkTrade, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the job trade.
        /// </summary>
        /// <param name="objJobTrade">The object job trade.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobTrade)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobTrade(Worktrade objJobTrade)
        {
            string validations = string.Empty;

            if (objJobTrade.WorkTradeID > 0)
            {
                objJobTrade.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objJobTrade.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobTrade_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJobTrade.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJobTrade.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobTrade_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("WorkTrade"))
                {
                    objJobTrade.WorkTradeID = context.Save(objJobTrade);
                    if (objJobTrade.WorkTradeID > 0)
                    {
                        if (Convert.ToBoolean(objJobTrade.IsDefault))
                        {
                            Worktrade objOldJobTrade = new Worktrade();
                            var oldResult = context.SearchAll(objOldJobTrade).Where(c => c.IsDefault == true && c.WorkTradeID != objJobTrade.WorkTradeID).FirstOrDefault();
                            if (oldResult != null)
                            {
                                oldResult.IsDefault = false;
                                oldResult.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                oldResult.ModifiedDate = DateTime.Now;
                                context.Save(oldResult);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobRequest_MsgJobTradeReq });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the job trade.
        /// </summary>
        /// <param name="jobTradeId">The job trade identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobTrade)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobTrade(int jobTradeId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Worktrade>(jobTradeId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// To get All Job Trades
        /// </summary>
        /// <returns>List Of Job Trades without any filtering</returns>
        [ActionName(Actions.GetJobTrades)]
        public ActionResult GetJobTrades()
        {
            Worktrade objWOrkTrade = new Worktrade();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll(objWOrkTrade);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// To get All Problem description type for WebReq By: Akif519
        /// </summary>
        /// <returns>List Of Problem description type</returns>
        [ActionName(Actions.GetProblemDescType)]
        public ActionResult GetProblemDescType()
        {
            WR_ProblemType objWOProblemType = new WR_ProblemType();
            using (DapperContext objcontext = new DapperContext())
            {
                var result = objcontext.SearchAll(objWOProblemType);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To get All Problem description for WebReq By: Akif519
        /// </summary>
        /// <returns>List Of Problem description</returns>
        [ActionName(Actions.GetProblemDesc)]
        public ActionResult GetProblemDesc(int problemTypeIdRef)
        {
            WR_ProblemDesc objWOProblemDesc = new WR_ProblemDesc();
            using (DapperContext objcontext = new DapperContext())
            {
                var result = objcontext.SearchAll(objWOProblemDesc).Where(o => o.ProblemTypeIdRef == problemTypeIdRef);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}