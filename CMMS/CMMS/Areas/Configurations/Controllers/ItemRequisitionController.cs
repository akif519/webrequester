﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Item Requisition Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "IR Req Status"

        /// <summary>
        /// item requisition the request status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.IRReqStatus)]
        public ActionResult IRReqStatus()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.IRReqStatus);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the item requisition status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRReqStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRReqStatus([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Status_desc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Material_req_statu objStatusSearch = new Material_req_statu();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objStatusSearch, SystemEnum.Pages.IRReqStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item requisition status by identifier.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRReqStatusByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRReqStatusByID(int statusId)
        {
            Material_req_statu objIRReqStatus = new Material_req_statu();
            if (statusId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objIRReqStatus = context.SelectObject<Material_req_statu>(statusId);
                }
            }

            return Json(objIRReqStatus, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the item requisition status.
        /// </summary>
        /// <param name="objIRReqStatus">The object item requisition status.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageIRReqStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageIRReqStatus(Material_req_statu objIRReqStatus)
        {
            string validations = string.Empty;

            if (objIRReqStatus.Mr_status_id > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("Status_desc"))
                {
                    if (context.Save(objIRReqStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRAuthoriseStatus_MsgIRStatusDescAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the item requisition request status.
        /// </summary>
        /// <param name="statusId">The status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteIRReqStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteIRReqStatus(int statusId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Material_req_statu>(statusId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "IR Status"

        /// <summary>
        /// item requisition the status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.IRStatus)]
        public ActionResult IRStatus()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.IRStatus);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the item requisition status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRStatus([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Auth_status_desc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Mr_authorisation_statu objStatusSearch = new Mr_authorisation_statu();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objStatusSearch, SystemEnum.Pages.IRStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item requisition status by identifier.
        /// </summary>
        /// <param name="itemRequisitionStatusId">The item requisition status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRStatusByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRStatusByID(int itemRequisitionStatusId)
        {
            Mr_authorisation_statu objIRStatus = new Mr_authorisation_statu();
            if (itemRequisitionStatusId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objIRStatus = context.SelectObject<Mr_authorisation_statu>(itemRequisitionStatusId);
                }
            }

            return Json(objIRStatus, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the item requisition status.
        /// </summary>
        /// <param name="objIRStatus">The object item requisition status.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageIRStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageIRStatus(Mr_authorisation_statu objIRStatus)
        {
            string validations = string.Empty;

            if (objIRStatus.Auth_status_id > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRAuthoriseStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("Auth_status_desc"))
                {
                    if (context.Save(objIRStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRAuthoriseStatus_MsgIRStatusDescReq });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the item requisition status.
        /// </summary>
        /// <param name="itemRequisitionStatusId">The item requisition status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteIRStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteIRStatus(int itemRequisitionStatusId)
        {
            using (DapperContext context = new DapperContext())
            {
                if (itemRequisitionStatusId == 3 || itemRequisitionStatusId == 4)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSystemReservedDeleteMsg }, JsonRequestBehavior.AllowGet);
                }

                int returnValue = context.Delete<Mr_authorisation_statu>(itemRequisitionStatusId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the item requisition all status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetIRAllStatus)]
        public ActionResult GetIRAllStatus()
        {
            Mr_authorisation_statu objIRStatus = new Mr_authorisation_statu();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objIRStatus);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "IR Approval Levels"

        /// <summary>
        /// item requisition the approval levels.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.IRApprovalLevels)]
        public ActionResult IRApprovalLevels()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevels_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.IRApprovalLevels);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the item requisition approval levels.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRApprovalLevels)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRApprovalLevels([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LevelNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            IRApprovalLevel objIRApprovalLevelSearch = new IRApprovalLevel();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objIRApprovalLevelSearch, SystemEnum.Pages.IRApprovalLevels.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item requisition status by identifier.
        /// </summary>
        /// <param name="approvalLevelId">The approval level identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRApprovalLevelsByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRApprovalLevelsByID(int approvalLevelId)
        {
            IRApprovalLevel objIRApprovalLevel = new IRApprovalLevel();
            if (approvalLevelId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objIRApprovalLevel = context.SelectObject<IRApprovalLevel>(approvalLevelId);
                }
            }

            return Json(objIRApprovalLevel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the item requisition approval levels.
        /// </summary>
        /// <param name="objIRApprovalLevel">The object item requisition approval level.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageIRApprovalLevels)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageIRApprovalLevels(IRApprovalLevel objIRApprovalLevel)
        {
            string validations = string.Empty;

            if (objIRApprovalLevel.IRApprovalLevelId > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevels_ChangeMastersCodes)
                {
                    objIRApprovalLevel.LevelNo = objIRApprovalLevel.LevelNo;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevels_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevels_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            objIRApprovalLevel.CreatedBy = ProjectSession.EmployeeID;
            objIRApprovalLevel.Createddate = DateTime.Now;
            objIRApprovalLevel.ModifiedBy = ProjectSession.EmployeeID;
            objIRApprovalLevel.Modifieddate = DateTime.Now;

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("LevelNo"))
                {
                    IRApprovalLevel oldObjIRApprovalLevelResult = new IRApprovalLevel();
                    var oldResult = context.SearchAll(oldObjIRApprovalLevelResult);

                    ////Juhi : 3-3-2017 : Atleast one level must be final
                    if (!objIRApprovalLevel.IsFinalLevel && objIRApprovalLevel.IRApprovalLevelId == 0)
                    {
                        int finalcount = oldResult.Where(x => x.IsFinalLevel == true).Count();
                        if (finalcount < 1)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Atleast one level should be final." });
                        }
                    }
                    else if (!objIRApprovalLevel.IsFinalLevel && objIRApprovalLevel.IRApprovalLevelId > 0)
                    {
                        int finalcount = oldResult.Where(x => x.IsFinalLevel == true && x.IRApprovalLevelId != objIRApprovalLevel.IRApprovalLevelId).Count();
                        if (finalcount < 1)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Atleast one level should be final." });
                        }
                    }
                    ////End Juhi : 3-3-2017 

                    if (objIRApprovalLevel.IRApprovalLevelId == 0)
                    {
                        if (objIRApprovalLevel.IsFinalLevel)
                        {
                            if (oldResult.Count > 0)
                            {
                                int objcount = oldResult.Max(c => c.LevelNo);
                                objIRApprovalLevel.LevelNo = objcount + 1;
                            }
                        }
                    }
                    else
                    {
                        int objMaxcount = oldResult.Max(c => c.LevelNo);
                        if (objIRApprovalLevel.IsFinalLevel)
                        {
                            objIRApprovalLevel.LevelNo = objMaxcount;
                        }
                        else if (objIRApprovalLevel.LevelNo > objMaxcount)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgEnterValidLevelNumber });
                        }
                        else
                        {
                            objIRApprovalLevel.LevelNo = objIRApprovalLevel.LevelNo;
                        }
                    }

                    CustomIRWorkFlow obj = new CustomIRWorkFlow();

                    if (obj.CheckLevelName(objIRApprovalLevel))
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgLevelNameAlreadyExists });
                    }

                    if (context.Save(objIRApprovalLevel) > 0)
                    {
                        if (objIRApprovalLevel.IsFinalLevel)
                        {
                            var oldIsFinal = oldResult.Where(c => c.IsFinalLevel == true && c.IRApprovalLevelId != objIRApprovalLevel.IRApprovalLevelId).FirstOrDefault();
                            if (oldIsFinal != null)
                            {
                                oldIsFinal.IsFinalLevel = false;
                                oldIsFinal.ModifiedBy = ProjectSession.EmployeeID;
                                oldIsFinal.Createddate = DateTime.Now;
                                oldIsFinal.Modifieddate = DateTime.Now;
                                context.Save(oldIsFinal);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgLevelNoAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the item requisition status.
        /// </summary>
        /// <param name="approvalLevelId">The approval level identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteIRApprovalLevels)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteIRApprovalLevels(int approvalLevelId)
        {
            using (DapperContext context = new DapperContext())
            {
                IRApprovalLevel objIRApprovalLevel = new IRApprovalLevel();
                objIRApprovalLevel = context.SelectObject<IRApprovalLevel>(approvalLevelId);
                if (objIRApprovalLevel.IsFinalLevel)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRWorkFlow_MsgIsFinalLevel }, JsonRequestBehavior.AllowGet);
                }

                int returnValue = context.Delete<IRApprovalLevel>(approvalLevelId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the item requisition approval all levels.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetIRApprovalAllLevels)]
        public ActionResult GetIRApprovalAllLevels()
        {
            IRApprovalLevel objIRApprovalLevel = new IRApprovalLevel();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objIRApprovalLevel);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "IR WorkFlow"

        /// <summary>
        /// item requisition the work flow.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.IRWorkFlow)]
        public ActionResult IRWorkFlow()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevelMappings_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.IRWorkFlow);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the item requisition work flow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRWorkFlow([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AutoId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            IRApprovalLevelMapping objIRApprovalLevelMapping = new IRApprovalLevelMapping();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L2.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objIRApprovalLevelMapping, SystemEnum.Pages.IRWorkFlow.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item requisition work flow by identifier.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetIRWorkFlowByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetIRWorkFlowByID(int workFlowId)
        {
            IRApprovalLevelMapping objIRApprovalLevelMapping = new IRApprovalLevelMapping();
            Substore objSubStore = new Substore();
            if (workFlowId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objIRApprovalLevelMapping = context.SelectObject<IRApprovalLevelMapping>(workFlowId);
                    objSubStore = context.SelectObject<Substore>(Convert.ToInt32(objIRApprovalLevelMapping.SubStoreId));
                    objIRApprovalLevelMapping.L2ID = Convert.ToInt32(objSubStore.L2ID);
                }
            }

            return Json(objIRApprovalLevelMapping, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// _s the edit item requisition work flow.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditIRWorkFlow)]
        public PartialViewResult _EditIRWorkFlow(int workFlowId)
        {
            IRApprovalLevelMapping objIRApprovalLevelMapping = new IRApprovalLevelMapping();
            Substore objSubStore = new Substore();
            if (workFlowId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objIRApprovalLevelMapping = context.SelectObject<IRApprovalLevelMapping>(workFlowId);
                    objSubStore = context.SelectObject<Substore>(Convert.ToInt32(objIRApprovalLevelMapping.SubStoreId));
                    objIRApprovalLevelMapping.L2ID = Convert.ToInt32(objSubStore.L2ID);

                    L2 objL2 = new L2();
                    objL2 = context.SelectObject<L2>(Convert.ToInt32(objSubStore.L2ID));
                    objIRApprovalLevelMapping.L1ID = Convert.ToInt32(objL2.L1ID);

                    Mr_authorisation_statu objauthStatus = new Mr_authorisation_statu();
                    objauthStatus = context.SelectObject<Mr_authorisation_statu>(Convert.ToInt32(objIRApprovalLevelMapping.Auth_status_id));
                    objIRApprovalLevelMapping.auth_status_desc = objauthStatus.Auth_status_desc;
                }
            }

            return PartialView(Pages.PartialViews.EditIRWorkFlow, objIRApprovalLevelMapping);
        }

        /// <summary>
        /// Gets the employee item requisition work flow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeIRWorkFlow([DataSourceRequest]DataSourceRequest request, int? autoID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            if (autoID == 0)
            {
                autoID = null;
            }

            CustomIRWorkFlow objCustomIRWorkFlow = new CustomIRWorkFlow();
            var result = new DataSourceResult()
            {
                Data = objCustomIRWorkFlow.GetIRApprovalLevelMappingsEmployeesInfoSearch(autoID, pageNumber, sortExpression, sortDirection).Distinct(),
                Total = objCustomIRWorkFlow.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the item requisition work flow.
        /// </summary>
        /// <param name="objIRApprovalLevelMapping">The object item requisition approval level mapping.</param>
        /// <param name="assignedEmployee">The assigned employee.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageIRWorkFlow(IRApprovalLevelMapping objIRApprovalLevelMapping, string assignedEmployee)
        {
            try
            {
                string validations = string.Empty;

                L2 objL2 = new L2();
                using (DapperContext context = new DapperContext())
                {
                    objL2 = objIRApprovalLevelMapping.L2ID > 0 ? context.SelectObject<L2>(Convert.ToInt32(objIRApprovalLevelMapping.L2ID)) : new L2();
                }

                if (objL2.Status == 0)
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
                }

                if (objIRApprovalLevelMapping.AutoId > 0)
                {
                    if (!ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevelMappings_Editrecords)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!ProjectSession.PermissionAccess.SetupConfiguration_IRApprovalLevelMappings_Createnewrecords)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                    }
                }

                IRApprovalLevel objApprovalLevel = new IRApprovalLevel();
                Mr_authorisation_statu objauthStatus = new Mr_authorisation_statu();

                if (ModelState.IsValid)
                {
                    CustomIRWorkFlow objCustomIRWorkFlow = new CustomIRWorkFlow();
                    if (objIRApprovalLevelMapping.AutoId > 0)
                    {
                        int returnvalue = objCustomIRWorkFlow.CheckForDuplicatesIRReq(objIRApprovalLevelMapping.SubStoreId, objIRApprovalLevelMapping.IRApprovalLevelId, objIRApprovalLevelMapping.AutoId);
                        if (returnvalue == 0)
                        {
                            using (DapperContext context = new DapperContext())
                            {
                                int autoId = context.Save(objIRApprovalLevelMapping);
                                if (autoId > 0)
                                {
                                    if (assignedEmployee.Count() > 0 && assignedEmployee != string.Empty)
                                    {
                                        bool flag = false;
                                        flag = objCustomIRWorkFlow.InsertIRApprovalLevelMappingsEmployeeBulk(autoId, assignedEmployee);
                                    }
                                }

                                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                            }
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
                        }
                    }
                    else
                    {
                        // For Final level Approval status must always be Approved
                        using (DapperContext context = new DapperContext())
                        {
                            objApprovalLevel = context.SelectObject<IRApprovalLevel>(Convert.ToInt32(objIRApprovalLevelMapping.IRApprovalLevelId));
                            if (objApprovalLevel.IsFinalLevel)
                            {
                                objauthStatus = context.SelectObject<Mr_authorisation_statu>(Convert.ToInt32(objIRApprovalLevelMapping.Auth_status_id));
                                if (objauthStatus.Auth_status_desc.ToLower() != "approved")
                                {
                                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgMustbeApproved });
                                }
                            }
                            else
                            {
                                objauthStatus = context.SelectObject<Mr_authorisation_statu>(Convert.ToInt32(objIRApprovalLevelMapping.Auth_status_id));
                                if (objauthStatus.Auth_status_desc.ToLower() == "approved")
                                {
                                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgCannotbeApproved });
                                }
                            }
                        }

                        int returnvalue = objCustomIRWorkFlow.SaveIRWorkFlowMapping(Convert.ToInt32(objIRApprovalLevelMapping.SubStoreId), objIRApprovalLevelMapping.IRApprovalLevelId);
                        if (returnvalue == 1)
                        {
                            using (DapperContext context = new DapperContext())
                            {
                                int autoId = context.Save(objIRApprovalLevelMapping);
                                objCustomIRWorkFlow.NewIRApprovalLevelMappingInsertFinalLevel(Convert.ToInt32(objIRApprovalLevelMapping.SubStoreId));

                                if (autoId > 0)
                                {
                                    if (assignedEmployee.Count() > 0 && assignedEmployee != string.Empty)
                                    {
                                        bool flag = false;
                                        flag = objCustomIRWorkFlow.InsertIRApprovalLevelMappingsEmployeeBulk(autoId, assignedEmployee);
                                    }
                                }

                                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                            }
                        }
                        else if (returnvalue == -3)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgFollowApprovalSeq });
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Deletes the item requisition work flow.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <param name="intSubStoreId">The Sub Store id.</param>
        /// <param name="approvalLevelId">The approval level id.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteIRWorkFlow(int workFlowId, int intSubStoreId, int approvalLevelId)
        {
            CustomIRWorkFlow context = new CustomIRWorkFlow();
            int returnValue = context.DeleteIRWorkFlowMapping(workFlowId, intSubStoreId, approvalLevelId);

            if (returnValue == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -2)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -3)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDontPermissionToDelete }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -4)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgFollowApprovalSeq }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the employee item requisition work flow.
        /// </summary>
        /// <param name="objEmployeesIRWorkFlow">The object employees item requisition work flow.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmployeeIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmployeeIRWorkFlow(IRApprovalLevelMappings_employee objEmployeesIRWorkFlow)
        {
            string validations = string.Empty;

            if (ModelState.IsValid)
            {
                CustomIRWorkFlow context = new CustomIRWorkFlow();

                if (context.InsertUpdateEmployeeIRApprovalLevel(objEmployeesIRWorkFlow))
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the employee item requisition work flow.
        /// </summary>
        /// <param name="autoId">The automatic identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmployeeIRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeIRWorkFlow(int autoId, int employeeId)
        {
            CustomIRWorkFlow context = new CustomIRWorkFlow();

            if (context.DeleteEmployeeIRApprovalLevel(autoId, employeeId))
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        #endregion
    }
}