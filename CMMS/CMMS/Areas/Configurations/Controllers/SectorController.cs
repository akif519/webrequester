﻿using System.Collections.Generic;
using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using Newtonsoft.Json;
using Kendo.Mvc;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Sectors the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SectorList)]
        public ActionResult SectorList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_Sector_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.SectorList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectorList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSectorList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SectorCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Sector sectorSearch = new Sector();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(sectorSearch, SystemEnum.Pages.Sector.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectorList)]
        [ValidateAntiForgeryToken]
        public ActionResult GetSectorList(string tableName)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            Sector sectorSearch = new Sector();
            SearchFieldService obj = new SearchFieldService();

            using (ServiceContext objContext = new ServiceContext())
            {
                Sector objCofig = new Sector();
                var result = objContext.SearchAll(objCofig);
                return Json(result);
            }
        }

        /// <summary>
        /// Gets the sector list by identifier.
        /// </summary>
        /// <param name="sectorId">The sector identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectorListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSectorListByID(int sectorId)
        {
            Sector sector = new Sector();
            if (sectorId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    sector = objDapperContext.SelectObject<Sector>(sectorId);
                }
            }

            return Json(sector, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the sector.
        /// </summary>
        /// <param name="sector">The sector.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSector)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSector(Sector sector)
        {
            string validations = string.Empty;
            SectorService objService = new SectorService();

            if (sector.SectorID > 0)
            {
                sector.ModifiedBy = ProjectSession.EmployeeID.ToString();
                sector.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_Sector_ChangeMastersCodes)
                {
                    sector.SectorCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Sector_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                sector.CreatedBy = ProjectSession.EmployeeID.ToString();
                sector.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Sector_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("SectorCode"))
                {
                    if (objDapperContext.Save(sector) > 0)
                    {
                        objService.UpdateL1InL2BySectorID(sector.SectorID, Convert.ToInt32(sector.L1ID));
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Setups_Configurations_MsgSectorCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the sector.
        /// </summary>
        /// <param name="sectorId">The sector identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSector)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSector(int sectorId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Sector>(sectorId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the sectors.
        /// </summary>
        /// <param name="orgID">Get Sector By L1 ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetSectors)]
        public ActionResult GetSectors(int? orgID = 0)
        {
            Sector sector = new Sector();
            using (DapperContext objDapperContext = new DapperContext())
            {
                if (orgID > 0)
                {
                    sector.L1ID = orgID;
                    var result = objDapperContext.Search<Sector>(sector);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = objDapperContext.SearchAll(sector);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}