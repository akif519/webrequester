﻿using CMMS.Controllers;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Pages;
using CMMS.Service;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CMMS.Areas.Configurations.Controllers
{
    public partial class ConfigurationsController : BaseController
    {
        #region AssetCategoryNotification

        [ActionName(Actions.AssetCategoryNotificationList)]
        public ActionResult AssetCategoryNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_AssetCategoryNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.AssetCategoryNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetAssetCategoryNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetCategoryNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetCategoryNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetCategoryNotification areaSearch = new AssetCategoryNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetCategoryNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetAssetCategoryNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetCategoryNotificationByID(int AssetCategoryNotificationID)
        {
            AssetCategoryNotification objAssetCategoryNotification = new AssetCategoryNotification();
            if (AssetCategoryNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetCategoryNotification = context.SelectObject<AssetCategoryNotification>(AssetCategoryNotificationID);
                }
            }

            return Json(objAssetCategoryNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageAssetCategoryNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetCategoryNotification(AssetCategoryNotification objAssetCategoryNotification)
        {
            string validations = string.Empty;

            if (objAssetCategoryNotification.AssetCategoryNotificationID > 0)
            {
                objAssetCategoryNotification.ModifiedBy = ProjectSession.EmployeeID;
                objAssetCategoryNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_AssetCategoryNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetCategoryNotification.CreatedBy = ProjectSession.EmployeeID;
                objAssetCategoryNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_AssetCategoryNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "AssetCatID", true))
                {
                    if (context.Save(objAssetCategoryNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgAssetCategoryNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteAssetCategoryNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetCategoryNotification(int AssetCategoryNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<AssetCategoryNotification>(AssetCategoryNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region CriticalityNotification
        [ActionName(Actions.CriticalityNotificationList)]
        public ActionResult CriticalityNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_CriticalityNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.CriticalityNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetCriticalityNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCriticalityNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CriticalityNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CriticalityNotification areaSearch = new CriticalityNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.CriticalityNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetCriticalityNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCriticalityNotificationByID(int CriticalityNotificationID)
        {
            CriticalityNotification objCriticalityNotification = new CriticalityNotification();
            if (CriticalityNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objCriticalityNotification = context.SelectObject<CriticalityNotification>(CriticalityNotificationID);
                }
            }

            return Json(objCriticalityNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageCriticalityNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCriticalityNotification(CriticalityNotification objCriticalityNotification)
        {
            string validations = string.Empty;

            if (objCriticalityNotification.CriticalityNotificationID > 0)
            {
                objCriticalityNotification.ModifiedBy = ProjectSession.EmployeeID;
                objCriticalityNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_CriticalityNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCriticalityNotification.CreatedBy = ProjectSession.EmployeeID;
                objCriticalityNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_CriticalityNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "CriticalityID", true))
                {
                    if (context.Save(objCriticalityNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgCriticalityNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteCriticalityNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCriticalityNotification(int CriticalityNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<CriticalityNotification>(CriticalityNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region JOStatusNotification
        [ActionName(Actions.JOStatusNotificationList)]
        public ActionResult JOStatusNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_JOStatusNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.JOStatusNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetJOStatusNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJOStatusNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "JOStatusNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JOStatusNotification areaSearch = new JOStatusNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.JOStatusNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetJOStatusNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJOStatusNotificationByID(int JOStatusNotificationID)
        {
            JOStatusNotification objJOStatusNotification = new JOStatusNotification();
            if (JOStatusNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJOStatusNotification = context.SelectObject<JOStatusNotification>(JOStatusNotificationID);
                }
            }

            return Json(objJOStatusNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageJOStatusNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJOStatusNotification(JOStatusNotification objJOStatusNotification)
        {
            string validations = string.Empty;

            if (objJOStatusNotification.JOStatusNotificationID > 0)
            {
                objJOStatusNotification.ModifiedBy = ProjectSession.EmployeeID;
                objJOStatusNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_JOStatusNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJOStatusNotification.CreatedBy = ProjectSession.EmployeeID;
                objJOStatusNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_JOStatusNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "WorkStatusID", true))
                {
                    if (context.Save(objJOStatusNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgJOStatusNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteJOStatusNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJOStatusNotification(int JOStatusNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<JOStatusNotification>(JOStatusNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region JRStatusNotification

        [ActionName(Actions.JRStatusNotificationList)]
        public ActionResult JRStatusNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_JRStatusNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JRStatusNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetJRStatusNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJRStatusNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "JRStatusNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            JRStatusNotification areaSearch = new JRStatusNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.JRStatusNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetJRStatusNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJRStatusNotificationByID(int JRStatusNotificationID)
        {
            JRStatusNotification objJRStatusNotification = new JRStatusNotification();
            if (JRStatusNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJRStatusNotification = context.SelectObject<JRStatusNotification>(JRStatusNotificationID);
                }
            }

            return Json(objJRStatusNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageJRStatusNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJRStatusNotification(JRStatusNotification objJRStatusNotification)
        {
            string validations = string.Empty;

            if (objJRStatusNotification.JRStatusNotificationID > 0)
            {
                objJRStatusNotification.ModifiedBy = ProjectSession.EmployeeID;
                objJRStatusNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_JRStatusNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJRStatusNotification.CreatedBy = ProjectSession.EmployeeID;
                objJRStatusNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_JRStatusNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "WorkStatusID", true))
                {
                    if (context.Save(objJRStatusNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgJRStatusNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteJRStatusNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJRStatusNotification(int JRStatusNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<JRStatusNotification>(JRStatusNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        #endregion

        #region LocationTypeNotification

        [ActionName(Actions.LocationTypeNotificationList)]
        public ActionResult LocationTypeNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_LocationTypeNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.LocationTypeNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetLocationTypeNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationTypeNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LocationTypeNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            LocationTypeNotification areaSearch = new LocationTypeNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.LocationTypeNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetLocationTypeNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationTypeNotificationByID(int LocationTypeNotificationID)
        {
            LocationTypeNotification objLocationTypeNotification = new LocationTypeNotification();
            if (LocationTypeNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objLocationTypeNotification = context.SelectObject<LocationTypeNotification>(LocationTypeNotificationID);
                }
            }

            return Json(objLocationTypeNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageLocationTypeNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageLocationTypeNotification(LocationTypeNotification objLocationTypeNotification)
        {
            string validations = string.Empty;

            if (objLocationTypeNotification.LocationTypeNotificationID > 0)
            {
                objLocationTypeNotification.ModifiedBy = ProjectSession.EmployeeID;
                objLocationTypeNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_LocationTypeNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objLocationTypeNotification.CreatedBy = ProjectSession.EmployeeID;
                objLocationTypeNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_LocationTypeNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "LocationTypeID", true))
                {
                    if (context.Save(objLocationTypeNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgLocationTypeNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteLocationTypeNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteLocationTypeNotification(int LocationTypeNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<LocationTypeNotification>(LocationTypeNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        #region WorkPriorityNotification

        [ActionName(Actions.WorkPriorityNotificationList)]
        public ActionResult WorkPriorityNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_WorkPriorityNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.WorkPriorityNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetWorkPriorityNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkPriorityNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkPriorityNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            WorkPriorityNotification areaSearch = new WorkPriorityNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.WorkPriorityNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetWorkPriorityNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkPriorityNotificationByID(int WorkPriorityNotificationID)
        {
            WorkPriorityNotification objWorkPriorityNotification = new WorkPriorityNotification();
            if (WorkPriorityNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objWorkPriorityNotification = context.SelectObject<WorkPriorityNotification>(WorkPriorityNotificationID);
                }
            }

            return Json(objWorkPriorityNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageWorkPriorityNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageWorkPriorityNotification(WorkPriorityNotification objWorkPriorityNotification)
        {
            string validations = string.Empty;

            if (objWorkPriorityNotification.WorkPriorityNotificationID > 0)
            {
                objWorkPriorityNotification.ModifiedBy = ProjectSession.EmployeeID;
                objWorkPriorityNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_WorkPriorityNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objWorkPriorityNotification.CreatedBy = ProjectSession.EmployeeID;
                objWorkPriorityNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_WorkPriorityNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "WorkPriorityID", true))
                {
                    if (context.Save(objWorkPriorityNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgWorkPriorityNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteWorkPriorityNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkPriorityNotification(int WorkPriorityNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<WorkPriorityNotification>(WorkPriorityNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region WorkTypeNotification

        [ActionName(Actions.WorkTypeNotificationList)]
        public ActionResult WorkTypeNotificationList()
        {
            if (ProjectSession.PermissionAccess.SLA_WorkTypeNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.WorkTypeNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        [ActionName(Actions.GetWorkTypeNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkTypeNotificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkTypeNotificationID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            WorkTypeNotification areaSearch = new WorkTypeNotification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.WorkTypeNotification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        [ActionName(Actions.GetWorkTypeNotificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkTypeNotificationByID(int WorkTypeNotificationID)
        {
            WorkTypeNotification objWorkTypeNotification = new WorkTypeNotification();
            if (WorkTypeNotificationID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objWorkTypeNotification = context.SelectObject<WorkTypeNotification>(WorkTypeNotificationID);
                }
            }

            return Json(objWorkTypeNotification, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.ManageWorkTypeNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageWorkTypeNotification(WorkTypeNotification objWorkTypeNotification)
        {
            string validations = string.Empty;

            if (objWorkTypeNotification.WorkTypeNotificationID > 0)
            {
                objWorkTypeNotification.ModifiedBy = ProjectSession.EmployeeID;
                objWorkTypeNotification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SLA_WorkTypeNotification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objWorkTypeNotification.CreatedBy = ProjectSession.EmployeeID;
                objWorkTypeNotification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SLA_WorkTypeNotification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("L2ID", "WorkTypeID", true))
                {
                    if (context.Save(objWorkTypeNotification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationSetup_MsgWorkTypeNotificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        [ActionName(Actions.DeleteWorkTypeNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkTypeNotification(int WorkTypeNotificationID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<WorkTypeNotification>(WorkTypeNotificationID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Nofitication Employees"

        [ActionName(Actions.GetAssetCategoryNotificationEmployeeList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetCategoryNotificationEmployeeList([DataSourceRequest]DataSourceRequest request, int notificationTypeID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "NotificationEmployeeID";
                sortDirection = "Ascending";
            }

            string strextraWhereClause = " AND NotificationEmployee.NotificationTypeID=" + notificationTypeID;

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            NotificationEmployee notificationEmployeeSearch = new NotificationEmployee();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(notificationEmployeeSearch, SystemEnum.Pages.NotificationEmployeeList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: strextraWhereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }


        [ActionName(Actions.SaveNotificationEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveNotificationEmployee(int employeeID, int notificationTypeId)
        {
            NotificationEmployee objnewNE = new NotificationEmployee();
            objnewNE.EmployeeID = employeeID;
            objnewNE.NotificationTypeID = notificationTypeId;
            objnewNE.CreatedDate = DateTime.Now;
            objnewNE.CreatedBy = ProjectSession.EmployeeID;
            objnewNE.NotificationEmployeeID = 0;
            string result = string.Empty;

            using (DapperContext context = new DapperContext())
            {
                if (context.Save(objnewNE) > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.FailureCodes_MsgFailureCodeAlreadyExists });
                }
            }
        }


        [ActionName(Actions.DeleteNotificationEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteNotificationEmployee(int NotificationEmployeeID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<NotificationEmployee>(NotificationEmployeeID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }



        #endregion


        #region "Notifications"

        //
        // GET: /Configurations/Notifications/
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        //[ActionName(Actions.JobRequestEscalationList)]
        [ActionName(Actions.NotificationList)]
        public ActionResult NotificationList()
        {
            if (ProjectSession.PermissionAccess.JobRequest_JobRequestEscalation_Allowaccess)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                TempData["Message"] = string.Empty;
                TempData["MessageType"] = string.Empty;
                return View(Views.ViewAllNotificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }


        [ActionName(Actions.GetNotificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetNotificationList([DataSourceRequest]DataSourceRequest request, string titleDescription, int? notificationTypeId, string startdate, string endDate)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkOrderNotification.CreatedDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string strWhereClause = " AND WorkOrderNotification.EmployeeID=" + ProjectSession.EmployeeID + " AND WorkOrderNotification.IsActive = 1";

            if (notificationTypeId != null)
            {
                strWhereClause = strWhereClause + " AND WorkOrderNotification.NotificationTypeID=" + notificationTypeId;
            }

            if (!string.IsNullOrWhiteSpace(titleDescription))
            {
                strWhereClause = strWhereClause + " AND (WorkOrderNotification.Title like '%" + titleDescription + "%' or WorkOrderNotification.Description like '%" + titleDescription + "%') ";
            }

            if ((!string.IsNullOrWhiteSpace(startdate)) && (!string.IsNullOrWhiteSpace(endDate)))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) between '" + startdate + "' and '" + endDate + "' ";
            }
            else if (!string.IsNullOrWhiteSpace(startdate))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) >= '" + startdate + "' ";
            }
            else if (!string.IsNullOrWhiteSpace(endDate))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) <= '" + endDate + "' ";
            }

            WorkOrderNotification areaSearch = new WorkOrderNotification();

            SearchFieldService obj = new SearchFieldService();


            List<WorkOrderNotification> lstWorkOrderNotification = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.ViewAllNotificationList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request, extraWhereClause: strWhereClause).ToList<WorkOrderNotification>();

            List<WorkOrderNotification> newlstWorkOrderNotification = new List<WorkOrderNotification>();

            foreach (WorkOrderNotification lst in lstWorkOrderNotification)
            {

                DateTime startTime = Convert.ToDateTime(lst.CreatedDate);
                DateTime endTime = DateTime.Now;

                TimeSpan span = endTime.Subtract(startTime);
                Console.WriteLine("Time Difference (seconds): " + span.Seconds);
                Console.WriteLine("Time Difference (minutes): " + span.Minutes);
                Console.WriteLine("Time Difference (hours): " + span.Hours);
                Console.WriteLine("Time Difference (days): " + span.Days);

                if (span.TotalSeconds < 60)
                {
                    lst.Postedon = "just now";
                }
                else if (span.TotalMinutes < 60)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalMinutes) + " minutes ago";
                }
                else if (span.TotalHours < 24)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalHours) + " hours ago";
                }
                else if (span.TotalDays > 0)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalDays) + " days ago";
                }
                newlstWorkOrderNotification.Add(lst);
            }

            var result = new DataSourceResult()
            {
                Data = newlstWorkOrderNotification,
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);

        }


        [ActionName(Actions.GetNotificationListTop5)]
        [HttpPost]
        public ActionResult GetNotificationListTop5([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "WorkOrderNotification.CreatedDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            string strWhereClause = " AND WorkOrderNotification.EmployeeID=" + ProjectSession.EmployeeID + " AND WorkOrderNotification.IsActive = 1 AND WorkOrderNotification.IsRead=0 ";


            WorkOrderNotification areaSearch = new WorkOrderNotification();

            SearchFieldService obj = new SearchFieldService();


            List<WorkOrderNotification> lstWorkOrderNotification = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.ViewAllNotificationList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request, extraWhereClause: strWhereClause).Take(5).ToList<WorkOrderNotification>();

            List<WorkOrderNotification> newlstWorkOrderNotification = new List<WorkOrderNotification>();

            foreach (WorkOrderNotification lst in lstWorkOrderNotification)
            {

                DateTime startTime = Convert.ToDateTime(lst.CreatedDate);
                DateTime endTime = DateTime.Now;

                TimeSpan span = endTime.Subtract(startTime);
                Console.WriteLine("Time Difference (seconds): " + span.Seconds);
                Console.WriteLine("Time Difference (minutes): " + span.Minutes);
                Console.WriteLine("Time Difference (hours): " + span.Hours);
                Console.WriteLine("Time Difference (days): " + span.Days);

                if (span.TotalSeconds < 60)
                {
                    lst.Postedon = "just now";
                }
                else if (span.TotalMinutes < 60)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalMinutes) + " minutes ago";
                }
                else if (span.TotalHours < 24)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalHours) + " hours ago";
                }
                else if (span.TotalDays > 0)
                {
                    lst.Postedon = Convert.ToInt32(span.TotalDays) + " days ago";
                }
                newlstWorkOrderNotification.Add(lst);
            }




            var result = new DataSourceResult()
            {
                Data = newlstWorkOrderNotification,
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);

        }



        [ActionName(Actions.InActiveNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult InActiveNotification(int workOrderNotificationID)
        {
            WorkOrderNotification objnewNotify = new WorkOrderNotification();
            objnewNotify.WorkOrderNotificationID = workOrderNotificationID;

            using (ServiceContext objContext = new ServiceContext())
            {
                objnewNotify = objContext.SearchAllByID<WorkOrderNotification>(objnewNotify, objnewNotify.WorkOrderNotificationID).FirstOrDefault();
            }

            objnewNotify.IsActive = false;
            objnewNotify.ModifiedDate = DateTime.Now;
            string result = string.Empty;

            using (DapperContext context = new DapperContext())
            {
                if (context.Save(objnewNotify) > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.FailureCodes_MsgFailureCodeAlreadyExists });
                }
            }
        }


        [ActionName(Actions.GetUnreadNotificationListCount)]
        [HttpPost]
        public JsonResult GetUnreadNotificationListCount()
        {

            string query = "select count(1) as NofiticationCount from NotificationEmployee inner join WorkOrderNotification on WorkOrderNotification.NotificationTypeID = NotificationEmployee.NotificationTypeID and WorkOrderNotification.EmployeeID = NotificationEmployee.EmployeeID Left join NotificationType on NotificationType.NotificationTypeID = WorkOrderNotification.NotificationTypeID " +
                " where WorkOrderNotification.EmployeeID=" + ProjectSession.EmployeeID + " AND WorkOrderNotification.IsActive = 1 AND WorkOrderNotification.IsRead=0 ";

            using (ServiceContext objContext = new ServiceContext())
            {
                return Json(objContext.ExecuteQuery<int>(query, null));
            }

        }

        [ActionName(Actions.GetTotalNotificationListCount)]
        [HttpPost]
        public JsonResult GetTotalNotificationListCount()
        {

            string query = "select count(1) as NofiticationCount from NotificationEmployee inner join WorkOrderNotification on WorkOrderNotification.NotificationTypeID = NotificationEmployee.NotificationTypeID and WorkOrderNotification.EmployeeID = NotificationEmployee.EmployeeID Left join NotificationType on NotificationType.NotificationTypeID = WorkOrderNotification.NotificationTypeID " +
                " where WorkOrderNotification.EmployeeID=" + ProjectSession.EmployeeID + " AND WorkOrderNotification.IsActive = 1 ";

            using (ServiceContext objContext = new ServiceContext())
            {
                return Json(objContext.ExecuteQuery<int>(query, null));
            }

        }



        [ActionName(Actions.MarkAsReadNotification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarkAsReadNotification(int workOrderNotificationID)
        {
            WorkOrderNotification objnewNotify = new WorkOrderNotification();
            objnewNotify.WorkOrderNotificationID = workOrderNotificationID;

            using (ServiceContext objContext = new ServiceContext())
            {
                objnewNotify = objContext.SearchAllByID<WorkOrderNotification>(objnewNotify, objnewNotify.WorkOrderNotificationID).FirstOrDefault();
            }

            objnewNotify.IsRead = true;
            objnewNotify.ModifiedDate = DateTime.Now;
            string result = string.Empty;
            using (DapperContext context = new DapperContext())
            {
                if (context.Save(objnewNotify) > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.FailureCodes_MsgFailureCodeAlreadyExists });
                }
            }
        }


        /// <summary>
        /// To get All Job Trades
        /// </summary>
        /// <returns>List Of Job Trades without any filtering</returns>
        [ActionName(Actions.NotificationTypeList)]
        public ActionResult NotificationTypeList()
        {
            NotificationType objNotificationType = new NotificationType();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll(objNotificationType);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [ActionName("ExportToExcel")]
        public FileResult ExportToExcel(string titleDescription, int? notificationTypeId, string startdate, string endDate)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            sortExpression = "WorkOrderNotification.CreatedDate";
            sortDirection = "Descending";

            string strWhereClause = " AND WorkOrderNotification.EmployeeID=" + ProjectSession.EmployeeID + " AND WorkOrderNotification.IsActive = 1";

            if (notificationTypeId != null)
            {
                strWhereClause = strWhereClause + " AND WorkOrderNotification.NotificationTypeID=" + notificationTypeId;
            }

            if (!string.IsNullOrWhiteSpace(titleDescription))
            {
                strWhereClause = strWhereClause + " AND (WorkOrderNotification.Title like '%" + titleDescription + "%' or WorkOrderNotification.Description like '%" + titleDescription + "%') ";
            }

            if ((!string.IsNullOrWhiteSpace(startdate)) && (!string.IsNullOrWhiteSpace(endDate)))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) between '" + startdate + "' and '" + endDate + "' ";
            }
            else if (!string.IsNullOrWhiteSpace(startdate))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) >= '" + startdate + "' ";
            }
            else if (!string.IsNullOrWhiteSpace(endDate))
            {
                strWhereClause = strWhereClause + " AND  convert(date,WorkOrderNotification.CreatedDate,103) <= '" + endDate + "' ";
            }

            WorkOrderNotification areaSearch = new WorkOrderNotification();

            SearchFieldService obj = new SearchFieldService();


            List<WorkOrderNotification> lstWorkOrderNotification = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.ViewAllNotificationList.GetHashCode(), ProjectSession.EmployeeID, 0, sortExpression, sortDirection, request: null, extraWhereClause: strWhereClause).ToList<WorkOrderNotification>();
            StringWriter objStringWriter = new StringWriter();



            List<WorkOrderNotificationExportExcel> lstWorkOrderNotificationExportExcel = new List<WorkOrderNotificationExportExcel>();
            foreach (WorkOrderNotification objWorkOrderNotification in lstWorkOrderNotification)
            {
                WorkOrderNotificationExportExcel objWorkOrderNotificationExportExcel = new WorkOrderNotificationExportExcel();
                objWorkOrderNotificationExportExcel.Title = objWorkOrderNotification.Title;
                objWorkOrderNotificationExportExcel.Description = objWorkOrderNotification.Description;
                objWorkOrderNotificationExportExcel.NotificationType = objWorkOrderNotification.NotificationTypeDesc;
                objWorkOrderNotificationExportExcel.WOWRNo = objWorkOrderNotification.WOWRNo;
                lstWorkOrderNotificationExportExcel.Add(objWorkOrderNotificationExportExcel);
            }

            if (lstWorkOrderNotification.Count <= 0)
            {
                lstWorkOrderNotificationExportExcel.Add(new WorkOrderNotificationExportExcel());
            }
            
            var gv = new GridView();
            gv.DataSource = lstWorkOrderNotificationExportExcel;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=NotificationListExport.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();            
            return File(objStringWriter.ToString(), "application/ms-excel", "NotificationListExport.xls");

        }


        #endregion


        #region "Notification Types"

        [ActionName(Actions.NotificationTypesList)]
        public ActionResult NotificationTypesList()
        {
            if (ProjectSession.PermissionAccess.SLA_AssetCategoryNotification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule && ProjectSession.PermissionAccess.SLA__ShowHideModule)
            {
                return View(Views.NotificationTypesList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Org.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// 
        [ValidateAntiForgeryToken]
        [ActionName(Actions.GetNotificationTypesList)]
        [HttpPost]
        public ActionResult GetNotificationTypesList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "NotificationTypeDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            NotificationType objNotificationType = new NotificationType();

            SearchFieldService obj = new SearchFieldService();

            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objNotificationType, SystemEnum.Pages.NotificationTypes.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }


        [ActionName(Actions.GetNotificationTypesListById)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetNotificationTypesListById(int notificationTypeId)
        {
            NotificationType objNotificationType = new NotificationType();
            if (notificationTypeId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objNotificationType = objDapperContext.SelectObject<NotificationType>(notificationTypeId);
                }
            }

            return Json(objNotificationType, JsonRequestBehavior.AllowGet);
        }


        [ActionName(Actions.ManageNotificationTypes)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageNotificationTypes(NotificationType objNotificationType)
        {
            string validations = string.Empty;

            if (objNotificationType.NotificationTypeID > 0)
            {
                if (ModelState.IsValid)
                {
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        if (objDapperContext.Save(objNotificationType) > 0)
                        {
                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                        else
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Organisation_MsgOrgCodeAlreadyExists });
                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                    }

                    if (string.IsNullOrEmpty(validations))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                    }
                }
            }
            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
        }


        #endregion


    }
}