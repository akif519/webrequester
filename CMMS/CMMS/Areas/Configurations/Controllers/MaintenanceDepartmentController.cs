﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Maintenances the department.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.MaintenanceDepartment)]
        public ActionResult MaintenanceDepartment()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDepartment_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.MaintenanceDepartment);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the maintenance department.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="maintDivID">The maintenance div identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceDepartment([DataSourceRequest]DataSourceRequest request, int maintDivID = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MaintDeptCode";
                sortDirection = "Ascending";
            }

            if (maintDivID > 0)
            {
                strWhere = " AND MainenanceDivision.MaintDivisionID = " + maintDivID;
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            MaintenanceDepartment maintenanceDepartmentSearch = new MaintenanceDepartment();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(maintenanceDepartmentSearch, SystemEnum.Pages.MaintenanceDepartment.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the maintenance department by identifier.
        /// </summary>
        /// <param name="maintDepartmentId">The maintenance department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDepartmentByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceDepartmentByID(int maintDepartmentId)
        {
            MaintenanceDepartment maintenanceDepartment = new MaintenanceDepartment();
            if (maintDepartmentId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    maintenanceDepartment = context.SelectObject<MaintenanceDepartment>(maintDepartmentId);
                    bool returnValue = context.HasReferencesRecords<MaintenanceDepartment>(maintenanceDepartment, maintDepartmentId);
                    maintenanceDepartment.IsEnableMaintDiv = returnValue;
                }
            }

            return Json(maintenanceDepartment, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the maintenance department.
        /// </summary>
        /// <param name="maintenanceDepartment">The maintenance department.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMaintenanceDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageMaintenanceDepartment(MaintenanceDepartment maintenanceDepartment)
        {
            string validations = string.Empty;

            if (maintenanceDepartment.MaintDeptID > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDepartment_ChangeMastersCodes)
                {
                    maintenanceDepartment.MaintDivisionCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDepartment_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }

                maintenanceDepartment.ModifiedBy = ProjectSession.EmployeeID.ToString();
                maintenanceDepartment.ModifiedDate = DateTime.Now;
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDepartment_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                maintenanceDepartment.CreatedBy = ProjectSession.EmployeeID.ToString();
                maintenanceDepartment.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("MaintDeptCode"))
                {
                    if (context.Save(maintenanceDepartment) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.MaintenanceDept_MsgMaintDeptCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the maintenance department.
        /// </summary>
        /// <param name="maintenanceDepartmentId">The maintenance department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMaintenanceDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMaintenanceDepartment(int maintenanceDepartmentId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<MaintenanceDepartment>(maintenanceDepartmentId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the maintenance department by division identifier.
        /// </summary>
        /// <param name="maintDivisionId">The maintenance division identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintDeptByDivisionID)]
        [HttpGet]
        public ActionResult GetMaintDeptByDivisionID(int maintDivisionId)
        {
            MaintenanceDepartment maintDepartment = new MaintenanceDepartment();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(maintDepartment).Where(o => o.MaintDivisionID == maintDivisionId);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the maintenance department.
        /// </summary>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="maintDivID">The maintenance div identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDepartmentbyPermission)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceDepartmentbyPermission(int empID, int maintDivID, [DataSourceRequest]DataSourceRequest request)
        {
            DataSourceResult result = new DataSourceResult();

            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;
                string strWhere = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }
                else
                {
                    sortExpression = "MaintDeptCode";
                    sortDirection = "Ascending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                MaintenanceDepartment maintenanceDepartmentSearch = new MaintenanceDepartment();

                DivisionService obj = new DivisionService();
                result = new DataSourceResult()
                {
                    Data = obj.GetMaintDeptPermissionVise(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, maintDivID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(result);
            }
        }
    }
}