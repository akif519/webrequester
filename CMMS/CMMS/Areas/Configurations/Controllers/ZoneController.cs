﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Zones this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Zone)]
        public ActionResult Zone()
        {
            if (ProjectSession.PermissionAccess.Locations_Street_Allowaccess && ProjectSession.PermissionAccess.Locations__ShowHideModule)
            {
                return View(Views.Zone);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the zone.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetZone)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetZone([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L4No";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L4 objL4 = new L4();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L4.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (L4.L3ID in ( " + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (L4.L3ID IS NULL))";
                }
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objL4, SystemEnum.Pages.Zone.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the zone by identifier.
        /// </summary>
        /// <param name="zoneId">The zone identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetZoneByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetZoneByID(int zoneId)
        {
            L4 objL4 = new L4();
            if (zoneId > 0)
            {
                using (ServiceContext zoneService = new ServiceContext())
                {
                    objL4 = zoneService.SelectObject<L4>(zoneId);
                    bool returnValue = zoneService.HasReferencesRecords<L4>(objL4, zoneId);
                    objL4.IsEnableCityArea = returnValue;
                    if (objL4 != null)
                    {
                        L2 objL2 = new L2();
                        using (DapperContext context = new DapperContext())
                        {
                            objL2 = context.SearchAll(objL2).Where(m => m.L2ID == objL4.L2ID).FirstOrDefault();
                            objL4.L1ID = objL2.L1ID;
                        }

                        L1 objL1 = new L1();
                        using (DapperContext context = new DapperContext())
                        {
                            objL1 = context.SelectObject<L1>(Convert.ToInt32(objL2.L1ID));
                            objL4.IsAreaEnableOrgWise = Convert.ToBoolean(objL1.IsAreaEnable);
                        }
                    }
                }
            }

            return Json(objL4, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the zone.
        /// </summary>
        /// <param name="objL4">The object l4.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageZone)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageZone(L4 objL4)
        {
            string validations = string.Empty;
            L2 objL2 = new L2();
            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = objL4.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(objL4.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            objL4.L2Code = objL2.L2Code;
            if (objL4.L4ID > 0)
            {
                if (!ProjectSession.PermissionAccess.Locations_Street_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }

                objL4.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objL4.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.Locations_Street_ChangeMastersCodes)
                {
                    objL4.L4No = null;
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.Locations_Street_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                objL4.CreatedBy = ProjectSession.EmployeeID.ToString();
                objL4.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext zoneService = new ServiceContext("L4No", "L2ID", true))
                {
                    if (zoneService.Save(objL4) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Street_MsgStreetCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the zone.
        /// </summary>
        /// <param name="zoneId">The zone identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteZone)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteZone(int zoneId)
        {
            using (ServiceContext zoneService = new ServiceContext())
            {
                int returnValue = zoneService.Delete<L4>(zoneId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the zone by l2 identifier.
        /// </summary>
        /// <param name="cityID">The project identifier.</param>
        /// <param name="areaID">Area ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetZoneByL2ID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetZoneByL2ID(int cityID, int areaID = 0)
        {
            L4 objL4 = new L4();
            objL4.L2ID = Convert.ToInt32(cityID);

            if (areaID > 0)
            {
                objL4.L3ID = areaID;
            }

            using (ServiceContext zoneService = new ServiceContext())
            {
                var result = zoneService.Search(objL4, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}