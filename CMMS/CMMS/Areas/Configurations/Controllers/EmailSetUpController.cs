﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Email SetUp Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Email Server Configuration"

        /// <summary>
        /// email server configuration.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.EmailServerConfiguration)]
        public ActionResult EmailServerConfiguration()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_EmailServerConfiguration_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                Configuration objconfiguration = new Configuration();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<Configuration> lst = objDapperContext.SearchAll(objconfiguration).Where(o => o.Module == "notification").ToList();
                    objconfiguration.smtp_server = lst.Where(o => o.Key == "smtp_server").FirstOrDefault().Value;
                    objconfiguration.smtp_server_port = lst.Where(o => o.Key == "smtp_server_port").FirstOrDefault().Value;
                    objconfiguration.smtp_username = lst.Where(o => o.Key == "smtp_username").FirstOrDefault().Value;
                    objconfiguration.smtp_password = lst.Where(o => o.Key == "smtp_password").FirstOrDefault().Value;
                    var uses_authentication = Convert.ToInt32(lst.Where(o => o.Key == "uses_authentication").FirstOrDefault().Value);
                    var uses_ssl = Convert.ToInt32(lst.Where(x => x.Key == "uses_ssl").FirstOrDefault().Value);
                    objconfiguration.uses_authentication = Convert.ToBoolean(uses_authentication);
                    objconfiguration.uses_ssl = Convert.ToBoolean(uses_ssl);
                }

                return View(Views.EmailServerConfiguration, objconfiguration);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Update Email Server Configuration.
        /// </summary>
        /// <param name="objConfiguration">The object configuration.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateEmailServerConfiguration)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult UpdateEmailServerConfiguration(Configuration objConfiguration)
        {
            if (!ProjectSession.PermissionAccess.SetupConfiguration_EmailServerConfiguration_Editrecords)
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
            }
            else
            {
                EmailConfigurationService objService = new EmailConfigurationService();
                List<Configuration> lstConfiguration = new List<Configuration>();
                string uses_authentication = "0";
                string uses_ssl = "0";
                Configuration objTempConfiguration = new Configuration();
                if (objConfiguration.smtp_server_port == null)
                {
                    objConfiguration.smtp_server = string.Empty;
                }

                if (objConfiguration.smtp_server == null)
                {
                    objConfiguration.smtp_server = string.Empty;
                }

                if (objConfiguration.uses_authentication)
                {
                    uses_authentication = "1";
                }

                if (objConfiguration.smtp_password == null)
                {
                    objConfiguration.smtp_password = string.Empty;
                }

                if (objConfiguration.smtp_username == null)
                {
                    objConfiguration.smtp_username = string.Empty;
                }

                if (objConfiguration.uses_ssl)
                {
                    uses_ssl = "1";
                }

                objTempConfiguration.Key = "smtp_server";
                objTempConfiguration.Value = objConfiguration.smtp_server;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "smtp_server_port";
                objTempConfiguration.Value = objConfiguration.smtp_server_port;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "uses_authentication";
                objTempConfiguration.Value = uses_authentication;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "uses_ssl";
                objTempConfiguration.Value = uses_ssl;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "smtp_username";
                objTempConfiguration.Value = objConfiguration.smtp_username;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "smtp_password";
                objTempConfiguration.Value = objConfiguration.smtp_password;

                lstConfiguration.Add(objTempConfiguration);

                bool isUpdated = false;

                isUpdated = objService.UpdateEmailConfigurations(lstConfiguration);

                if (isUpdated)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                }
                else
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }
            }

            return RedirectToAction(Actions.EmailServerConfiguration, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
        }

        /// <summary>
        /// Sends the test mail.
        /// </summary>
        /// <param name="objConfiguration">The object configuration.</param>
        /// <param name="emailID">The email identifier.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendTestMail(Configuration objConfiguration, string emailID)
        {
            try
            {
                System.Net.Mail.SmtpClient objectSmtpClient = new System.Net.Mail.SmtpClient();
                objectSmtpClient.Host = objConfiguration.smtp_server;
                objectSmtpClient.Port = Convert.ToInt32(objConfiguration.smtp_server_port);
                objectSmtpClient.EnableSsl = objConfiguration.uses_ssl;
                objectSmtpClient.UseDefaultCredentials = objConfiguration.uses_authentication;
                objectSmtpClient.Credentials = new System.Net.NetworkCredential(objConfiguration.smtp_username, objConfiguration.smtp_password);

                bool result = EmailServices.Send(emailID, null, null, ProjectConfiguration.TestEmailSubject, ProjectConfiguration.TestEmailBody, null, null, objectSmtpClient);

                if (result)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgMailSentSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgMailNotSentSuccessfully }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgMailNotSentSuccessfully }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "SMS Server Configuration"
        /// <summary>
        /// short messages service server configuration.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SMSServerConfiguration)]
        public ActionResult SMSServerConfiguration()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_SMSConfiguration_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                Configuration objconfiguration = new Configuration();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<Configuration> lst = objDapperContext.SearchAll(objconfiguration).Where(o => o.Module == "SMSNotification").ToList();
                    objconfiguration.SMS_GateWay_API_URL = lst.Where(o => o.Key == "SMS_GateWay_API_URL").FirstOrDefault().Value;
                    objconfiguration.SMS_UserName = lst.Where(o => o.Key == "SMS_UserName").FirstOrDefault().Value;
                    objconfiguration.SMS_Password = lst.Where(o => o.Key == "SMS_Password").FirstOrDefault().Value;
                    objconfiguration.SMS_ApiID = lst.Where(o => o.Key == "SMS_ApiID").FirstOrDefault().Value;
                    objconfiguration.Sender_Name = lst.Where(o => o.Key == "Sender_Name").FirstOrDefault().Value;
                }

                return View(Views.SMSServerConfiguration, objconfiguration);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Update short messages service Server Configuration.
        /// </summary>
        /// <param name="objConfiguration">The object configuration.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateSMSServerConfiguration)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult UpdateSMSServerConfiguration(Configuration objConfiguration)
        {
            if (!ProjectSession.PermissionAccess.SetupConfiguration_SMSConfiguration_Editrecords)
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
            }
            else
            {
                EmailConfigurationService objService = new EmailConfigurationService();
                List<Configuration> lstConfiguration = new List<Configuration>();
                Configuration objTempConfiguration = new Configuration();
                if (objConfiguration.Sender_Name == null)
                {
                    objConfiguration.Sender_Name = string.Empty;
                }

                objTempConfiguration.Key = "SMS_GateWay_API_URL";
                objTempConfiguration.Value = objConfiguration.SMS_GateWay_API_URL;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "SMS_UserName";
                objTempConfiguration.Value = objConfiguration.SMS_UserName;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "SMS_Password";
                objTempConfiguration.Value = objConfiguration.SMS_Password;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "SMS_ApiID";
                objTempConfiguration.Value = objConfiguration.SMS_ApiID;
                lstConfiguration.Add(objTempConfiguration);
                objTempConfiguration = new Configuration();
                objTempConfiguration.Key = "Sender_Name";
                objTempConfiguration.Value = objConfiguration.Sender_Name;
                lstConfiguration.Add(objTempConfiguration);

                bool isUpdated = false;

                isUpdated = objService.UpdateSMSConfigurations(lstConfiguration);

                if (isUpdated)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                }
                else
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }
            }

            return RedirectToAction(Actions.SMSServerConfiguration, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
        }

        /// <summary>
        /// Sends the test SMS.
        /// </summary>
        /// <param name="objConfiguration">The object configuration.</param>
        /// <param name="phoneNo">The phone no.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendTestSMS(Configuration objConfiguration, string phoneNo)
        {
            try
            {
                string result = ProjectSession.Resources.message.Common_MsgSMSNotSentSuccessfully;
                if (!string.IsNullOrEmpty(phoneNo))
                {
                    if (!string.IsNullOrEmpty(objConfiguration.SMS_GateWay_API_URL) && objConfiguration.SMS_GateWay_API_URL.Contains("clickatell"))
                    {
                        result = EmailServices.SendSMSFromAPI(objConfiguration.SMS_UserName, objConfiguration.SMS_Password, ProjectConfiguration.TestSMSBody, phoneNo, objConfiguration.SMS_ApiID);
                    }
                    else if (!string.IsNullOrEmpty(objConfiguration.SMS_GateWay_API_URL) && objConfiguration.SMS_GateWay_API_URL.Contains("mobily"))
                    {
                        result = EmailServices.SendSMSFromDll(objConfiguration.SMS_UserName, objConfiguration.SMS_Password, ProjectConfiguration.TestSMSBody, phoneNo, objConfiguration.Sender_Name);
                    }
                }

                if (result == string.Empty)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgSMSSentSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgSMSNotSentSuccessfully }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Email Templates"
        /// <summary>
        /// email Templates.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.EmailTemplates)]
        public ActionResult EmailTemplates()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_NotificationTemplates_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.EmailTemplates);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Email Templates.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailTemplates)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailTemplates([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LanguageRef";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailtemplate objEmailTemplates = new Emailtemplate();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objEmailTemplates, SystemEnum.Pages.EmailTemplates.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the email template field.
        /// </summary>
        /// <param name="fieldType">The Field Type</param>
        /// <param name="templatename">The Name of Template</param>
        /// <param name="isWOWR">The WOWR present or not</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailTemplateFields)]
        public ActionResult GetEmailTemplateFields(string fieldType, string templatename, bool isWOWR)
        {
            Emailtemplatefield emailtemplatefield = new Emailtemplatefield();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(emailtemplatefield).Where(o => o.FieldType == fieldType).ToList();

                if (isWOWR)
                {
                    result = result.Where(o => o.EmailTemplateDBFieldName != "WorkOrderList" || o.EmailTemplateDBFieldName != "WorkRequestList" || !o.EmailTemplateDBFieldName.Contains("Receiptant")).ToList();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else if (templatename == "Work Order Escalation" || templatename == "Work Request Escalation")
                {
                    result = result.Where(o => o.EmailTemplateDBFieldName == "WorkOrderList" || o.EmailTemplateDBFieldName == "WorkRequestList" || o.EmailTemplateDBFieldName.Contains("Receiptant")).ToList();
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// _s the edit email template.
        /// </summary>
        /// <param name="emailtemplateid">The email template.</param>
        /// <returns></returns>
        [ActionName(Actions._EditEmailTemplates)]
        public PartialViewResult _EditEmailTemplates(int emailtemplateid)
        {
            Emailtemplate objEmailTemp = new Emailtemplate();
            if (emailtemplateid > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objEmailTemp = context.SelectObject<Emailtemplate>(emailtemplateid);

                    IEnumerable<SelectListItem> templateFieldType = Enum.GetValues(typeof(SystemEnum.TemplateFieldType)).Cast<SystemEnum.TemplateFieldType>().Select(x => new SelectListItem()
                    {
                        Text = x.ToString(),
                        Value = SystemEnum.GetEnumName(typeof(SystemEnum.TemplateFieldType), (int)x)
                    }).ToList().Where(x => x.Value == objEmailTemp.EmailFieldType);
                    ViewBag.TemplateFieldType = templateFieldType;
                }
            }

            return PartialView(Pages.PartialViews.EditEmailTemplates, objEmailTemp);
        }

        /// <summary>
        /// Manages the Email Template.
        /// </summary>
        /// <param name="objEmailTemplate">The Email Template Object.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmailTemplates)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmailTemplates(Emailtemplate objEmailTemplate)
        {
            string validations = string.Empty;

            if (objEmailTemplate.EmailTemplateID > 0)
            {
                objEmailTemplate.ModifiedBy = Convert.ToInt32(ProjectSession.EmployeeID.ToString());
                objEmailTemplate.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_NotificationTemplates_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objEmailTemplate.CreatedBy = Convert.ToInt32(ProjectSession.EmployeeID.ToString());
                objEmailTemplate.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_NotificationTemplates_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    int emailtemplateID = context.Save(objEmailTemplate);
                    if (emailtemplateID > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        #endregion

        #region "Email Notification Rules"
        /// <summary>
        /// email Templates.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.EmailRules)]
        public ActionResult EmailRules()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_NotificationRules_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.EmailRules);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Email Templates.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRules)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRules([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "NotificationRuleName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrule objEmailRules = new Emailnotificationrule();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objEmailRules, SystemEnum.Pages.EmailNotificationRules.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// _s the edit email rules.
        /// </summary>
        /// <param name="notificationRuleId">The notification rule.</param>
        /// <returns></returns>
        [ActionName(Actions._EditEmailRules)]
        public PartialViewResult _EditEmailRules(int notificationRuleId)
        {
            Emailnotificationrule objEmailTemp = new Emailnotificationrule();
            if (notificationRuleId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objEmailTemp = context.SelectObject<Emailnotificationrule>(notificationRuleId);
                }
            }

            if (objEmailTemp.IsActive == null)
            {
                objEmailTemp.IsActive = false;
            }

            return PartialView(Pages.PartialViews.EditEmailRules, objEmailTemp);
        }

        /// <summary>
        /// Gets the Email Rules L2.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesL2)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesL2([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_L2 objEmailRules = new Emailnotificationrules_L2();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetL2DataForEmailNotification<Emailnotificationrules_L2>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules L5.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesL5)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesL5([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_L5 objEmailRules = new Emailnotificationrules_L5();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetL5DataForEmailNotification<Emailnotificationrules_L5>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetEmailRulesLocationType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesLocationType([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmailNotification_LocationType objEmailRules = new EmailNotification_LocationType();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetLocationTypeDataForEmailNotification<EmailNotification_LocationType>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetEmailRulesSubCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesSubCategory([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmailNotification_SubCategory objEmailRules = new EmailNotification_SubCategory();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetSubCategoryDataForEmailNotification<EmailNotification_SubCategory>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetEmailRulesCriticality)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesCriticality([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmailNotification_Criticality objEmailRules = new EmailNotification_Criticality();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetCriticalityDataForEmailNotification<EmailNotification_Criticality>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Employee.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesEmployees)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesEmployees([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_employee objEmailRules = new Emailnotificationrules_employee();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetEmployeeDataForEmailNotification<Emailnotificationrules_employee>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetEmailRulesSupplier)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesSupplier([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmailNotification_Supplier objEmailRules = new EmailNotification_Supplier();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetSupplierDataForEmailNotification<EmailNotification_Supplier>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Location.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesLocation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesLocation([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_location objEmailRules = new Emailnotificationrules_location();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetLocationDataForEmailNotification<Emailnotificationrules_location>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Work priority.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesWorkpriority)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesWorkpriority([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_workpriority objEmailRules = new Emailnotificationrules_workpriority();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetWorkPriorityDataForEmailNotification<Emailnotificationrules_workpriority>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Work Status.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesWorkStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesWorkStatus([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_workstatu objEmailRules = new Emailnotificationrules_workstatu();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetWorkStatusDataForEmailNotification<Emailnotificationrules_workstatu>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Work Trade.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesWorkTrade)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesWorkTrade([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_worktrade objEmailRules = new Emailnotificationrules_worktrade();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetWorkTradeDataForEmailNotification<Emailnotificationrules_worktrade>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Work Type.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesWorkType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesWorkType([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_worktype objEmailRules = new Emailnotificationrules_worktype();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetWorkTypeDataForEmailNotification<Emailnotificationrules_worktype>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Assets.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesAssets)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesAssets([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_asset objEmailRules = new Emailnotificationrules_asset();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetAssetsDataForEmailNotification<Emailnotificationrules_asset>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Email Rules Escalation.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <param name="notificationRuleID">The NotificationRuleID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmailRulesEscalation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmailRulesEscalation([DataSourceRequest]DataSourceRequest request, string notificationRuleID)
        {
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Emailnotificationrules_escalation objEmailRules = new Emailnotificationrules_escalation();

            EmailConfigurationService obj = new EmailConfigurationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetEmployeeEscalationDataForEmailNotification<Emailnotificationrules_escalation>(objEmailRules, Convert.ToInt32(notificationRuleID), request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Buildings All.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuildingDetailsAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBuildingDetailsAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            BuildingService obj = new BuildingService();
            var result = new DataSourceResult()
            {
                Data = obj.GetBuildingDetailsAll(pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetLocationTypeAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationTypeAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            BuildingService obj = new BuildingService();
            var result = new DataSourceResult()
            {
                Data = obj.GetLocationTypeAll(pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetCriticalityAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCriticalityAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            BuildingService obj = new BuildingService();
            var result = new DataSourceResult()
            {
                Data = obj.GetCriticalityAll(pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        [ActionName(Actions.GetSubCategoryAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSubCategoryAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            BuildingService obj = new BuildingService();
            var result = new DataSourceResult()
            {
                Data = obj.GetSubCategoryAll(pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalPages
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Locations All.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationDetailsAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationDetailsAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            LocationService objService = new LocationService();

            var result = new DataSourceResult()
            {
                Data = objService.GetLocationPage(string.Empty, 0, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the Assets All.
        /// </summary>
        /// <param name="request">The Request</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsDetailsAll)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetsDetailsAll([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetService objService = new AssetService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAssetPage(0, string.Empty, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the Rules.
        /// </summary>
        /// <param name="objEmailrule">The Rule.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmailRules)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmailRules(Emailnotificationrule objEmailrule)
        {
            string validations = string.Empty;

            if (objEmailrule.NotificationRuleID > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_NotificationRules_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_NotificationRules_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            objEmailrule.ModifiedBy = ProjectSession.EmployeeID.ToString();
            objEmailrule.ModifiedDate = DateTime.Now;

            objEmailrule.CreatedBy = ProjectSession.EmployeeID.ToString();
            objEmailrule.CreatedDate = DateTime.Now;

            objEmailrule.EmailTemplateName = SystemEnum.GetEnumName(typeof(SystemEnum.ModuleEvent), (int)objEmailrule.NotificationRuleEventID);

            if (ModelState.IsValid)
            {
                EmailConfigurationService objMailService = new EmailConfigurationService();
                using (DapperContext objDapperContext = new DapperContext("NotificationRuleName"))
                {
                    objEmailrule.NotificationRuleID = objDapperContext.Save(objEmailrule);
                    if (objEmailrule.NotificationRuleID > 0)
                    {
                        objMailService.SaveNotificationRule(objEmailrule);
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.NotificationEmailRules_MsgEmailRuleNameExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the EmailRules.
        /// </summary>
        /// <param name="notificationId">The EmailRules identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmailRules)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmailRules(int notificationId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                EmailConfigurationService objMailService = new EmailConfigurationService();
                objMailService.DeleteNotificationRules(notificationId);

                int returnValue = objDapperContext.Delete<Emailnotificationrule>(notificationId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
    }
}