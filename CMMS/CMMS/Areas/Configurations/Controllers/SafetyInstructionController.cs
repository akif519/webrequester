﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Safety Instruction Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Safeties the instruction.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SafetyInstruction)]
        public ActionResult SafetyInstruction()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_SafetyInstructions_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.SafetyInstruction);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the safety instruction.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSafetyInstruction)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSafetyInstruction([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SafetyNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            TblSafetyinstruction objSafetyinstruction = new TblSafetyinstruction();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and tblSafetyinstruction.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objSafetyinstruction, SystemEnum.Pages.SafetyInstruction.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the safety instruction by identifier.
        /// </summary>
        /// <param name="safetyInstructionId">The safety instruction identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSafetyInstructionByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSafetyInstructionByID(int safetyInstructionId)
        {
            TblSafetyinstruction objSafetyinstruction = new TblSafetyinstruction();
            if (safetyInstructionId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSafetyinstruction = context.SelectObject<TblSafetyinstruction>(safetyInstructionId);
                }
            }

            return Json(objSafetyinstruction, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the safety instruction.
        /// </summary>
        /// <param name="objSafetyinstruction">The object safety instruction.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSafetyInstruction)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSafetyInstruction(TblSafetyinstruction objSafetyinstruction)
        {
            string validations = string.Empty;

            L2 objL2 = new L2();
            using (DapperContext context = new DapperContext())
            {
                objL2 = objSafetyinstruction.L2Id > 0 ? context.SelectObject<L2>(Convert.ToInt32(objSafetyinstruction.L2Id)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (objSafetyinstruction.SafetyID > 0)
            {
                objSafetyinstruction.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objSafetyinstruction.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_SafetyInstructions_ChangeMastersCodes)
                {
                    objSafetyinstruction.SafetyNo = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_SafetyInstructions_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSafetyinstruction.CreatedBy = ProjectSession.EmployeeID.ToString();
                objSafetyinstruction.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_SafetyInstructions_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("SafetyNo"))
                {
                    int safetyID = context.Save(objSafetyinstruction);
                    if (safetyID > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.SafetyInstruction_MsgSafetyInstrNoAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the safety instruction.
        /// </summary>
        /// <param name="safetyInstructionId">The safety instruction identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSafetyInstruction)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSafetyInstruction(int safetyInstructionId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<TblSafetyinstruction>(safetyInstructionId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// _s the edit safety instruction.
        /// </summary>
        /// <param name="safetyInstructionId">The safety instruction identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditSafetyInstruction)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _EditSafetyInstruction(int safetyInstructionId)
        {
            TblSafetyinstruction objSafetyinstruction = new TblSafetyinstruction();
            if (safetyInstructionId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSafetyinstruction = context.SelectObject<TblSafetyinstruction>(safetyInstructionId);
                }
            }

            return PartialView(Pages.PartialViews.EditSafetyInstruction, objSafetyinstruction);
        }

        /// <summary>
        /// Gets the safety instruction item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="safetyInstructionId">The safety instruction identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSafetyInstructionItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSafetyInstructionItem([DataSourceRequest]DataSourceRequest request, string safetyInstructionId)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CustomSafetyInstruction obj = new CustomSafetyInstruction();
            var result = new DataSourceResult()
            {
                Data = obj.GetSafetyInstructionItemBySafetyID(Convert.ToInt32(safetyInstructionId), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /////// <summary>
        /////// Manages the safety instruction item.
        /////// </summary>
        /////// <param name="objSafetyinstructionItem">The object safety instruction item.</param>
        /////// <returns></returns>
        ////[ActionName(Actions.ManageSafetyInstructionItem)]
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////[ValidateInput(false)]
        ////public JsonResult ManageSafetyInstructionItem(TblSafetyInstructionItem objSafetyinstructionItem)
        ////{
        ////    string validations = string.Empty;

        ////    if (objSafetyinstructionItem.HdnSeqID > 0)
        ////    {
        ////        objSafetyinstructionItem.ModifiedBy = ProjectSession.EmployeeID.ToString();
        ////        objSafetyinstructionItem.ModifiedDate = DateTime.Now;
        ////    }
        ////    else
        ////    {
        ////        objSafetyinstructionItem.CreatedBy = ProjectSession.EmployeeID.ToString();
        ////        objSafetyinstructionItem.CreatedDate = DateTime.Now;
        ////    }

        ////    if (ModelState.IsValid)
        ////    {
        ////        TblSafetyInstructionItem oldObjTblSafetyInstructionItemResult = new TblSafetyInstructionItem();
        ////        var oldResult = new DapperContext().SearchAll(oldObjTblSafetyInstructionItemResult).Where(m => m.SafetyID == objSafetyinstructionItem.SafetyID).ToList();
        ////        if (objSafetyinstructionItem.HdnSeqID == 0)
        ////        {
        ////            if (oldResult.Count > 0)
        ////            {
        ////                int objcount = Convert.ToInt32(oldResult.Max(c => c.SeqID));
        ////                objSafetyinstructionItem.SeqID = objcount + 1;
        ////            }
        ////            else
        ////            {
        ////                objSafetyinstructionItem.SeqID = 1;
        ////            }
        ////        }
        ////        else
        ////        {
        ////            objSafetyinstructionItem.SeqID = objSafetyinstructionItem.SeqID;
        ////        }

        ////        CustomSafetyInstruction context = new CustomSafetyInstruction();

        ////        if (context.InsertUpdateSafetyInstructionItem(objSafetyinstructionItem))
        ////        {
        ////            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
        ////        }
        ////        else
        ////        {
        ////            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
        ////        }
        ////    }
        ////    else
        ////    {
        ////        foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
        ////        {
        ////            validations = validations + modelError.ErrorMessage + " ,";
        ////        }

        ////        if (string.IsNullOrEmpty(validations))
        ////        {
        ////            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
        ////        }
        ////        else
        ////        {
        ////            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
        ////        }
        ////    }
        ////}

        /// <summary>
        /// Deletes the safety instruction item.
        /// </summary>
        /// <param name="safetyId">The safety identifier.</param>
        /// <param name="seqId">The sequence identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSafetyInstructionItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSafetyInstructionItem(int safetyId, int seqId)
        {
            CustomSafetyInstruction context = new CustomSafetyInstruction();

            if (context.DeleteSafetyInstructionItem(safetyId, seqId))
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Gets the Safety Instructions All.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetSafetyInstructionAll)]
        public ActionResult GetSafetyInstructionAll()
        {
            TblSafetyinstruction objSafetyinstruction = new TblSafetyinstruction();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objSafetyinstruction);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the safety instruction item.
        /// </summary>
        /// <param name="objOldJobtask">The object old job task.</param>
        /// <param name="objNewJobtask">The object new job task.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSafetyInstructionItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSafetyInstructionItem(TblSafetyInstructionItem objOldJobtask, TblSafetyInstructionItem objNewJobtask, string kendogriddata)
        {
            List<TblSafetyInstructionItem> lstJobPlanItems = new List<TblSafetyInstructionItem>();
            lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TblSafetyInstructionItem>>(kendogriddata);

            CustomSafetyInstruction objService = new CustomSafetyInstruction();

            bool isSeqUpdated = false;

            int maxSeqId = 1;
            int minSeqId = 1;
            int difference = 0;

            if (lstJobPlanItems.Count > 0)
            {
                maxSeqId = lstJobPlanItems.Max(x => x.SeqID);
                minSeqId = lstJobPlanItems.Min(x => x.SeqID);
            }

            if (objOldJobtask.SeqID > -1 && (objNewJobtask.SeqID != objOldJobtask.SeqID) && objNewJobtask.SeqID > maxSeqId && objOldJobtask.SeqID == maxSeqId)
            {
                ////Updating Max Seq ID  Item With SeqID > Max ID, Then Do not update seqid, just update content
                objNewJobtask.SeqID = objOldJobtask.SeqID;
            }
            else
            {
                ////Adding/Updating Item with SeqID > MaxSeqID
                if (objNewJobtask.SeqID > maxSeqId + 1)
                {
                    objNewJobtask.SeqID = maxSeqId + 1;
                }
            }

            if (objOldJobtask.SeqID > -1 && (objNewJobtask.SeqID != objOldJobtask.SeqID))
            {
                ////Update Sequence
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqID == objOldJobtask.SeqID && x.Description == objOldJobtask.Description);
                lstJobPlanItems.Remove(objOldJobtask);
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOldJobtask.SeqID > -1 && (objNewJobtask.SeqID == objOldJobtask.SeqID))
            {
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqID == objOldJobtask.SeqID && x.Description == objOldJobtask.Description);
                lstJobPlanItems.Remove(objOldJobtask);
            }
            else if (objOldJobtask.SeqID == -1)
            {
                objNewJobtask.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.CreatedDate = DateTime.Now;
            }

            if (isSeqUpdated)
            {
                if (objOldJobtask.SeqID < objNewJobtask.SeqID)
                {
                    if (objNewJobtask.SeqID > maxSeqId)
                    {
                        objNewJobtask.SeqID = maxSeqId;
                    }

                    lstJobPlanItems.Where(x => x.SeqID > objOldJobtask.SeqID && x.SeqID <= objNewJobtask.SeqID).ToList().ForEach(p =>
                    {
                        p.SeqID = p.SeqID - 1;
                    });
                }
                else if (objOldJobtask.SeqID > objNewJobtask.SeqID)
                {
                    if (objNewJobtask.SeqID < minSeqId)
                    {
                        if (minSeqId > 1)
                        {
                            difference = minSeqId - objNewJobtask.SeqID;
                            minSeqId = 1;

                            objNewJobtask.SeqID = minSeqId;
                            lstJobPlanItems.ForEach(p =>
                            {
                                p.SeqID = p.SeqID - difference;
                            });
                        }
                    }
                    else
                    {
                        lstJobPlanItems.Where(x => x.SeqID >= objNewJobtask.SeqID && x.SeqID < objOldJobtask.SeqID).ToList().ForEach(p =>
                        {
                            p.SeqID = p.SeqID + 1;
                        });
                    }
                }
            }
            else
            {
                if (objOldJobtask.SeqID == -1 && lstJobPlanItems.Any(x => x.SeqID == objNewJobtask.SeqID))
                {
                    ////Inserting New Item in the list between any two Items
                    lstJobPlanItems.Where(x => x.SeqID >= objNewJobtask.SeqID).ToList().ForEach(p =>
                    {
                        p.SeqID = p.SeqID + 1;
                    });
                }
                else
                {
                    if (lstJobPlanItems.Count == 0)
                    {
                        objNewJobtask.SeqID = minSeqId;
                    }
                }
            }

            lstJobPlanItems.Add(objNewJobtask);

            bool result = objService.SaveSafetyInstrItem(ConvertTo.Integer(objNewJobtask.SafetyID), lstJobPlanItems);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}