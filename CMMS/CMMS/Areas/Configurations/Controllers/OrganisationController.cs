﻿using System.Collections.Generic;
using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using Newtonsoft.Json;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Org Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Org this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Organisation)]
        public ActionResult Organisation()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_Organisation_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.Organisation);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the Org.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetOrganisation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetOrganisation([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L1No";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L1 objOrganisation = new L1();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 23-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L1.L1ID in ( Select Distinct L1ID from L2 Where L2ID IN (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " )) ";
            }

            /*(End)Added By Pratik Telaviya on 23-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objOrganisation, SystemEnum.Pages.Organisation.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Organization by permission
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetOrganisationByPermission)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetOrganisationByPermission([DataSourceRequest]DataSourceRequest request, int empID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L1No";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L1 objOrganisation = new L1();
            OrganisationService objService = new OrganisationService();

            var result = new DataSourceResult()
            {
                Data = objService.GetOraganisationByPermission(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, pageNumber, sortExpression, sortDirection, request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the Org by identifier.
        /// </summary>
        /// <param name="orgId">The Org identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetOrganisationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetOrganisationByID(int orgId)
        {
            L1 objOrganisation = new L1();
            if (orgId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objOrganisation = objDapperContext.SelectObject<L1>(orgId);
                }
            }

            return Json(objOrganisation, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the org.
        /// </summary>
        /// <param name="objOrg">The object org.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageOrganisation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageOrganisation(L1 objOrg)
        {
            string validations = string.Empty;

            if (objOrg.L1ID > 0)
            {
                objOrg.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objOrg.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_Organisation_ChangeMastersCodes)
                {
                    objOrg.L1No = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Organisation_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objOrg.CreatedBy = ProjectSession.EmployeeID.ToString();
                objOrg.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Organisation_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("L1No"))
                {
                    if (objDapperContext.Save(objOrg) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Organisation_MsgOrgCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the org.
        /// </summary>
        /// <param name="orgId">The org identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteOrganisation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteOrganisation(int orgId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<L1>(orgId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all Org.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllOrganisation)]
        public ActionResult GetAllOrganisation()
        {
            OrganisationService objService = new OrganisationService();
            string sortExpression = "L1No";
            string sortDirection = "Ascending";
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objService.GetOraganisationByPermission(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, ProjectSession.EmployeeID, 0, sortExpression, sortDirection);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all enable organization.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllEnableOrganisation)]
        public ActionResult GetAllEnableOrganisation()
        {
            OrganisationService objService = new OrganisationService();
            string sortExpression = "L1No";
            string sortDirection = "Ascending";
            using (DapperContext objDapperContext = new DapperContext())
            {
                List<L1> lstL1 = objService.GetOraganisationByPermission(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, ProjectSession.EmployeeID, 0, sortExpression, sortDirection).ToList();
                if (lstL1 != null && lstL1.Count > 0)
                {
                    var result = lstL1.Where(x => x.IsAreaEnable == true);
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(null, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}