﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "PRStatus List"
        /// <summary>
        /// Purchase Status the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PRStatusList)]
        public ActionResult PRStatusList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PRStatusList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRStatusList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "status_desc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Purchase_req_statu objPRStatus = new Purchase_req_statu();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPRStatus, SystemEnum.Pages.PRStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the PR Status list by identifier.
        /// </summary>
        /// <param name="pr_status_id">The PR Status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRStatusListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRStatusListByID(int pr_status_id)
        {
            Purchase_req_statu objPRStatus = new Purchase_req_statu();
            IList<Purchase_req_statu> lstPRStatus = new List<Purchase_req_statu>();

            if (pr_status_id > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objPRStatus = objDapperContext.SelectObject<Purchase_req_statu>(pr_status_id);
                }

                return Json(objPRStatus, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objPRStatus = new Purchase_req_statu();
                    lstPRStatus = context.SearchAll(objPRStatus);
                }

                return Json(lstPRStatus, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the PRStatus.
        /// </summary>
        /// <param name="objPRStatus">The PRStatus.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePRStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePRStatus(Purchase_req_statu objPRStatus)
        {
            string validations = string.Empty;

            if (objPRStatus.Pr_status_id > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("status_desc"))
                {
                    if (objDapperContext.Save(objPRStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PRAuthoriseStatus_MsgPRStatusDescAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the PRStatus.
        /// </summary>
        /// <param name="pr_status_id">The PR Status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePRStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePRStatus(int pr_status_id)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Purchase_req_statu>(pr_status_id, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the PR Status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetPRStatus)]
        public ActionResult GetPRStatus()
        {
            Purchase_req_statu objPRStatus = new Purchase_req_statu();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objPRStatus);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "PR Authorisation List"
        /// <summary>
        /// Purchase Status the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PRAuthStatusList)]
        public ActionResult PRAuthStatusList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PRAuthStatusList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRAuthStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRAuthStatusList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "auth_status_desc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Pr_authorisation_statu objPRStatus = new Pr_authorisation_statu();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPRStatus, SystemEnum.Pages.PRAuthStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the PR Status list by identifier.
        /// </summary>
        /// <param name="auth_status_id">The PR Authorization Status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRAuthStatusListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRAuthStatusListByID(int auth_status_id)
        {
            Pr_authorisation_statu objPRStatus = new Pr_authorisation_statu();
            IList<Pr_authorisation_statu> lstPRStatus = new List<Pr_authorisation_statu>();

            if (auth_status_id > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objPRStatus = objDapperContext.SelectObject<Pr_authorisation_statu>(auth_status_id);
                }

                return Json(objPRStatus, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objPRStatus = new Pr_authorisation_statu();
                    lstPRStatus = context.SearchAll(objPRStatus);
                }

                return Json(lstPRStatus, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the PR Authorization Status.
        /// </summary>
        /// <param name="objPRStatus">The PRStatus.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePRAuthStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePRAuthStatus(Pr_authorisation_statu objPRStatus)
        {
            string validations = string.Empty;

            if (objPRStatus.Auth_status_id > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRAuthoriseStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("auth_status_desc"))
                {
                    if (objDapperContext.Save(objPRStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PRAuthoriseStatus_MsgPRStatusDescAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the PR Authorization Status.
        /// </summary>
        /// <param name="auth_status_id">The PR Authorization Status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePRAuthStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePRAuthStatus(int auth_status_id)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Pr_authorisation_statu>(auth_status_id, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the PR Status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetPRAuthStatus)]
        public ActionResult GetPRAuthStatus()
        {
            Pr_authorisation_statu objPRStatus = new Pr_authorisation_statu();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objPRStatus);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "PR Approval Levels"
        /// <summary>
        /// Purchase Status the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PRApprovalLevelList)]
        public ActionResult PRApprovalLevelList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevels_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PRApprovalLevel);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRApprovalLevelList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRApprovalLevelList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LevelName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPRApprovalLevel, SystemEnum.Pages.PRApprovalLevels.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the PR Status list by identifier.
        /// </summary>
        /// <param name="pr_approval_level_id">The PR Approval Levels identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRApprovalLevelListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRApprovalLevelListByID(int pr_approval_level_id)
        {
            PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
            if (pr_approval_level_id > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objPRApprovalLevel = objDapperContext.SelectObject<PRApprovalLevel>(pr_approval_level_id);
                }
            }

            return Json(objPRApprovalLevel, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the PR approval.
        /// </summary>
        /// <param name="objPRApprovalLevel">The PRApproval.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePRApprovalLevel)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePRApprovalLevel(PRApprovalLevel objPRApprovalLevel)
        {
            string validations = string.Empty;

            if (objPRApprovalLevel.PRApprovalLevelId > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevels_ChangeMastersCodes)
                {
                    objPRApprovalLevel.LevelNo = objPRApprovalLevel.LevelNo;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevels_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevels_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                DapperContext objDapperContext1 = new DapperContext();

                using (DapperContext objDapperContext = new DapperContext("LevelName", "LevelNo", false))
                {
                    var allPrevData = objDapperContext.SearchAll(objPRApprovalLevel);

                    ////Juhi : 3-3-2017 : Atleast one level must be final
                    if (!objPRApprovalLevel.IsFinalLevel && objPRApprovalLevel.PRApprovalLevelId == 0)
                    {
                        int finalcount = allPrevData.Where(x => x.IsFinalLevel == true).Count();
                        if (finalcount < 1)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Atleast one level should be final." });
                        }
                    }
                    else if (!objPRApprovalLevel.IsFinalLevel && objPRApprovalLevel.PRApprovalLevelId > 0)
                    {
                        int finalcount = allPrevData.Where(x => x.IsFinalLevel == true && x.PRApprovalLevelId != objPRApprovalLevel.PRApprovalLevelId).Count();
                        if (finalcount < 1)
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), "Atleast one level should be final." });
                        }
                    }
                    ////End Juhi : 3-3-2017 

                    int intMaxcount = 0;
                    if (allPrevData.Count > 0)
                    {
                        intMaxcount = allPrevData.Max(c => c.LevelNo);
                    }

                    objPRApprovalLevel.CreatedBy = ProjectSession.EmployeeID;
                    objPRApprovalLevel.Createddate = DateTime.Now;
                    objPRApprovalLevel.ModifiedBy = ProjectSession.EmployeeID;
                    objPRApprovalLevel.Modifieddate = DateTime.Now;

                    objPRApprovalLevel.PRApprovalLevelId = objDapperContext.Save(objPRApprovalLevel);
                    if (objPRApprovalLevel.PRApprovalLevelId > 0)
                    {
                        if (objPRApprovalLevel.IsFinalLevel && allPrevData.Count > 0)
                        {
                            var old = allPrevData.Where(c => c.IsFinalLevel == true && c.PRApprovalLevelId != objPRApprovalLevel.PRApprovalLevelId).FirstOrDefault();
                            if (old != null)
                            {
                                old.IsFinalLevel = false;
                                old.ModifiedBy = ProjectSession.EmployeeID;
                                objPRApprovalLevel.Createddate = DateTime.Now;
                                old.Modifieddate = DateTime.Now;
                                objDapperContext.Save(old);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgLevelNoNameAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the PR Approval Level.
        /// </summary>
        /// <param name="pr_approval_level_id">The PR Approval Level identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePRApprovalLevel)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePRApprovalLevel(int pr_approval_level_id)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
                objPRApprovalLevel = objDapperContext.SelectObject<PRApprovalLevel>(pr_approval_level_id);

                if (objPRApprovalLevel.IsFinalLevel)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRWorkFlow_MsgIsFinalLevel }, JsonRequestBehavior.AllowGet);
                }

                int returnValue = objDapperContext.Delete<PRApprovalLevel>(pr_approval_level_id, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the PR Status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetPRApprovalLevel)]
        public ActionResult GetPRApprovalLevel()
        {
            PRApprovalLevel objPRApprovalLevel = new PRApprovalLevel();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objPRApprovalLevel);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Purchase Request WorkFlow"
        /// <summary>
        /// purchase requisition the work flow.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PRWorkFlow)]
        public ActionResult PRWorkFlow()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevelMappings_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PRWorkFlow);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchase requisition work flow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRWorkFlow([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AutoId";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PRApprovalLevelMapping objPRApprovalLevelMapping = new PRApprovalLevelMapping();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L2.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPRApprovalLevelMapping, SystemEnum.Pages.PRWorkFlow.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchase requisition work flow by identifier.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPRWorkFlowByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPRWorkFlowByID(int workFlowId)
        {
            PRApprovalLevelMapping objPRApprovalLevelMapping = new PRApprovalLevelMapping();
            Substore objSubStore = new Substore();
            if (workFlowId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objPRApprovalLevelMapping = context.SelectObject<PRApprovalLevelMapping>(workFlowId);
                }
            }

            return Json(objPRApprovalLevelMapping, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// _s the edit purchase requisition work flow.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditPRWorkFlow)]
        public PartialViewResult _EditPRWorkFlow(int workFlowId)
        {
            PRApprovalLevelMapping objPRApprovalLevelMapping = new PRApprovalLevelMapping();
            if (workFlowId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objPRApprovalLevelMapping = context.SelectObject<PRApprovalLevelMapping>(workFlowId);

                    L2 objL2 = new L2();
                    objL2 = context.SelectObject<L2>(Convert.ToInt32(objPRApprovalLevelMapping.L2ID));
                    objPRApprovalLevelMapping.L1ID = Convert.ToInt32(objL2.L1ID);

                    Pr_authorisation_statu objauthStatus = new Pr_authorisation_statu();
                    objauthStatus = context.SelectObject<Pr_authorisation_statu>(Convert.ToInt32(objPRApprovalLevelMapping.Auth_status_id));
                    objPRApprovalLevelMapping.auth_status_desc = objauthStatus.Auth_status_desc;
                }
            }

            return PartialView(Pages.PartialViews.EditPRWorkFlow, objPRApprovalLevelMapping);
        }

        /// <summary>
        /// Gets the employee purchase requisition work flow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="autoID">The automatic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeePRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeePRWorkFlow([DataSourceRequest]DataSourceRequest request, int? autoID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            if (autoID == 0)
            {
                autoID = null;
            }

            CustomPRWorkFlow objCustomPRWorkFlow = new CustomPRWorkFlow();
            var result = new DataSourceResult()
            {
                Data = objCustomPRWorkFlow.GetPRApprovalLevelMappingsEmployeesInfoSearch(autoID, pageNumber, sortExpression, sortDirection).Distinct(),
                Total = objCustomPRWorkFlow.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the purchase requisition work flow.
        /// </summary>
        /// <param name="objPRApprovalLevelMapping">The object purchase requisition approval level mapping.</param>
        /// <param name="assignedEmployee">The assigned employee.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePRWorkFlow(PRApprovalLevelMapping objPRApprovalLevelMapping, string assignedEmployee)
        {
            string validations = string.Empty;

            if (objPRApprovalLevelMapping.AutoId > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevelMappings_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_PRApprovalLevelMappings_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            L2 objL2 = new L2();
            using (DapperContext context = new DapperContext())
            {
                objL2 = objPRApprovalLevelMapping.L2ID > 0 ? context.SelectObject<L2>(Convert.ToInt32(objPRApprovalLevelMapping.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            PRApprovalLevel objApprovalLevel = new PRApprovalLevel();
            Pr_authorisation_statu objauthStatus = new Pr_authorisation_statu();

            // For Final level Approval status must always be Approved
            if (objPRApprovalLevelMapping.AutoId == 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objApprovalLevel = context.SelectObject<PRApprovalLevel>(Convert.ToInt32(objPRApprovalLevelMapping.PRApprovalLevelId));
                    if (objApprovalLevel.IsFinalLevel)
                    {
                        objauthStatus = context.SelectObject<Pr_authorisation_statu>(Convert.ToInt32(objPRApprovalLevelMapping.Auth_status_id));
                        if (objauthStatus.Auth_status_desc.ToLower() != "approved")
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgMustbeApproved });
                        }
                    }
                    else
                    {
                        objauthStatus = context.SelectObject<Pr_authorisation_statu>(Convert.ToInt32(objPRApprovalLevelMapping.Auth_status_id));
                        if (objauthStatus.Auth_status_desc.ToLower() == "approved")
                        {
                            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemRequisition_MsgCannotbeApproved });
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                CustomPRWorkFlow objCustomPRWorkFlow = new CustomPRWorkFlow();
                if (objPRApprovalLevelMapping.AutoId > 0)
                {
                    int returnvalue = objCustomPRWorkFlow.CheckForDuplicatesPRReq(Convert.ToInt32(objPRApprovalLevelMapping.L2ID), objPRApprovalLevelMapping.PRApprovalLevelId, objPRApprovalLevelMapping.AutoId);
                    if (returnvalue == 0)
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            int autoId = context.Save(objPRApprovalLevelMapping);
                            if (autoId > 0)
                            {
                                if (assignedEmployee.Count() > 0 && assignedEmployee != string.Empty)
                                {
                                    bool flag = false;
                                    flag = objCustomPRWorkFlow.InsertPRApprovalLevelMappingsEmployeeBulk(autoId, assignedEmployee);
                                }
                            }

                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
                    }
                }
                else
                {
                    int returnvalue = objCustomPRWorkFlow.SavePRWorkFlowMapping(Convert.ToInt32(objPRApprovalLevelMapping.L2ID), objPRApprovalLevelMapping.PRApprovalLevelId);
                    if (returnvalue == 1)
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            int autoId = context.Save(objPRApprovalLevelMapping);
                            objCustomPRWorkFlow.NewPRApprovalLevelMappingInsertFinalLevel(Convert.ToInt32(objPRApprovalLevelMapping.L2ID));
                            if (autoId > 0)
                            {
                                if (assignedEmployee.Count() > 0 && assignedEmployee != string.Empty)
                                {
                                    bool flag = false;
                                    flag = objCustomPRWorkFlow.InsertPRApprovalLevelMappingsEmployeeBulk(autoId, assignedEmployee);
                                }
                            }

                            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                        }
                    }
                    else if (returnvalue == -3)
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.IRApprovalLevel_MsgFollowApprovalSeq });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the purchase requisition work flow.
        /// </summary>
        /// <param name="workFlowId">The work flow identifier.</param>
        /// <param name="intl2ID">The L2 ID.</param>
        /// <param name="purchaseApprovalLevelId">The Approval ID.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePRWorkFlow)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePRWorkFlow(int workFlowId, int intl2ID, int purchaseApprovalLevelId)
        {
            CustomPRWorkFlow context = new CustomPRWorkFlow();

            int returnValue = context.DeletePRWorkFlowMapping(workFlowId, intl2ID, purchaseApprovalLevelId);

            if (returnValue == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -2)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -3)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDontPermissionToDelete }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        
        [ActionName(Actions.GetOrderStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetOrderStatusList()
        {
            IList<SelectListItem> lst = new List<SelectListItem>();

            SelectListItem item1 = new SelectListItem { Text = "Authorised", Value = "Authorised" };
            SelectListItem item2 = new SelectListItem { Text = "Not Authorised", Value = "Not Authorised" };
            SelectListItem item3 = new SelectListItem { Text = "Cancelled", Value = "Cancelled" };

            lst.Add(item1);
            lst.Add(item2);
            lst.Add(item3);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [ActionName(Actions.GetDeliveryStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetDeliveryStatusList()
        {
            IList<SelectListItem> lst = new List<SelectListItem>();

            SelectListItem item1 = new SelectListItem { Text = "Partial Delivery", Value = "Partial Delivery" };
            SelectListItem item2 = new SelectListItem { Text = "Full Delivery", Value = "Full Delivery" };
            SelectListItem item3 = new SelectListItem { Text = "No Delivery", Value = "No Delivery" };

            lst.Add(item1);
            lst.Add(item2);
            lst.Add(item3);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}