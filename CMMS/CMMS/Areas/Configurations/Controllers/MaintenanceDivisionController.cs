﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Maintenances the division.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.MaintenanceDivision)]
        public ActionResult MaintenanceDivision()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDivision_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.MaintenanceDivision);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the maintenance division.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDivision)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceDivision([DataSourceRequest]DataSourceRequest request)
        {
            DataSourceResult result = new DataSourceResult();

            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }
                else
                {
                    sortExpression = "MaintDivisionCode";
                    sortDirection = "Ascending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                MainenanceDivision mainenanceDivisionSearch = new MainenanceDivision();

                SearchFieldService obj = new SearchFieldService();
                result = new DataSourceResult()
                {
                    Data = obj.AdvanceSearch(mainenanceDivisionSearch, SystemEnum.Pages.MaintenanceDivision.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(result);
            }
        }

        /// <summary>
        /// Gets the maintenance division by identifier.
        /// </summary>
        /// <param name="maintDivisionId">The Maintenance division identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDivisionByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceDivisionByID(int maintDivisionId)
        {
            MainenanceDivision mainenanceDivision = new MainenanceDivision();
            if (maintDivisionId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    mainenanceDivision = context.SelectObject<MainenanceDivision>(maintDivisionId);
                }
            }

            return Json(mainenanceDivision, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the maintenance division.
        /// </summary>
        /// <param name="mainenanceDivision">The maintenance division.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMaintenanceDivision)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageMaintenanceDivision(MainenanceDivision mainenanceDivision)
        {
            string validations = string.Empty;

            if (mainenanceDivision.MaintDivisionID > 0)
            {
                mainenanceDivision.ModifiedBy = ProjectSession.EmployeeID.ToString();
                mainenanceDivision.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDivision_ChangeMastersCodes)
                {
                    mainenanceDivision.MaintDivisionCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDivision_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceDivision_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                mainenanceDivision.CreatedBy = ProjectSession.EmployeeID.ToString();
                mainenanceDivision.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("MaintDivisionCode"))
                {
                    if (context.Save(mainenanceDivision) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.MaintDivision_MsgMaintDivisionCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the maintenance division.
        /// </summary>
        /// <param name="maintDivisionId">The maintenance division identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMaintenanceDivision)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMaintenanceDivision(int maintDivisionId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<MainenanceDivision>(maintDivisionId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the maintenance divisions.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDivisions)]
        public ActionResult GetMaintenanceDivisions()
        {
            IList<MainenanceDivision> list = new List<MainenanceDivision>();

            try
            {
                JobRequestService objService = new JobRequestService();

                list = objService.GetMaintDivisionByEmployeeIDOrProjectNumber(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, string.Empty);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the maintenance divisions.
        /// </summary>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceDivisionsByPermissions)]
        public ActionResult GetMaintenanceDivisionsByPermissions(int empID, [DataSourceRequest]DataSourceRequest request)
        {
            DataSourceResult result = new DataSourceResult();

            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }
                else
                {
                    sortExpression = "MaintDivisionCode";
                    sortDirection = "Ascending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                MainenanceDivision mainenanceDivisionSearch = new MainenanceDivision();

                DivisionService obj = new DivisionService();
                result = new DataSourceResult()
                {
                    Data = obj.GetMaintDivisionPermissionVise(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(result);
            }
        }

        /// <summary>
        /// Gets the maintenance divisions.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllMaintenanceDivisions)]
        public ActionResult GetAllMaintenanceDivisions()
        {
            MainenanceDivision mainenanceDivision = new MainenanceDivision();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(mainenanceDivision);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}