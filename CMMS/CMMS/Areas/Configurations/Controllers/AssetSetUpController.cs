﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Services;
using Kendo.Mvc.UI;
using CMMS.Models;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Asset Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Asset Category"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetCategoriesList)]
        public ActionResult AssetCategoriesList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetCategories_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AssetCategoriesList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetCategoriesList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetCategoriesList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetCatCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Assetcategorie areaSearch = new Assetcategorie();
            ////areaSearch.FailureCauseCode = failureCauseCode;
            ////areaSearch.FailureCauseDescription = failureCauseDescription;

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetCategories.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the asset categories list by identifier.
        /// </summary>
        /// <param name="assetCatID">The asset cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetCategoriesListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetCategoriesListByID(int assetCatID)
        {
            Assetcategorie objAssetcategorie = new Assetcategorie();
            if (assetCatID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetcategorie = context.SelectObject<Assetcategorie>(assetCatID);
                }
            }

            return Json(objAssetcategorie, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset categories.
        /// </summary>
        /// <param name="objAssetcategorie">The object asset category.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetCategories)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetCategories(Assetcategorie objAssetcategorie)
        {
            string validations = string.Empty;

            if (objAssetcategorie.AssetCatID > 0)
            {
                objAssetcategorie.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAssetcategorie.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCategories_ChangeMastersCodes)
                {
                    objAssetcategorie.AssetCatCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCategories_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetcategorie.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAssetcategorie.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCategories_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AssetCatCode", "AssetCategory", false))
                {
                    if (context.Save(objAssetcategorie) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetCategoryAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset categories.
        /// </summary>
        /// <param name="assetCatID">The asset cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetCategories)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetCategories(int assetCatID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Assetcategorie>(assetCatID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all asset categories.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllAssetCategories)]
        public ActionResult GetAllAssetCategories()
        {
            Assetcategorie objAssetcategorie = new Assetcategorie();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.Search<Assetcategorie>(objAssetcategorie, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Asset Class"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetClassList)]
        public ActionResult AssetClassList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetClass_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AssetClassList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetClassList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetClassList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetClassCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetClasse areaSearch = new AssetClasse();
            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetClass.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the asset class list by identifier.
        /// </summary>
        /// <param name="assetClassID">The asset class identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetClassListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetClassListByID(int assetClassID)
        {
            AssetClasse objAssetClass = new AssetClasse();
            if (assetClassID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetClass = context.SelectObject<AssetClasse>(assetClassID);
                }
            }

            return Json(objAssetClass, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset class.
        /// </summary>
        /// <param name="objAssetClass">The object asset class.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetClass)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetClass(AssetClasse objAssetClass)
        {
            string validations = string.Empty;

            if (objAssetClass.AssetClassID > 0)
            {
                objAssetClass.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAssetClass.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetClass_ChangeMastersCodes)
                {
                    objAssetClass.AssetClassCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetClass_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetClass.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAssetClass.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetClass_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AssetClassCode", "AssetClass", false))
                {
                    if (context.Save(objAssetClass) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetClassAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset class.
        /// </summary>
        /// <param name="assetClassID">The asset class identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetClass)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetClass(int assetClassID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<AssetClasse>(assetClassID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all asset class.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllAssetClass)]
        public ActionResult GetAllAssetClass()
        {
            AssetClasse objAssetClasse = new AssetClasse();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.Search<AssetClasse>(objAssetClasse, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all asset class by asset sub cat identifier.
        /// </summary>
        /// <param name="assetSubCatID">The asset sub cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllAssetClassByAssetSubCatID)]
        public ActionResult GetAllAssetClassByAssetSubCatID(int assetSubCatID)
        {
            AssetSubCategorie objAssetSubCategorie = new AssetSubCategorie();
            AssetClasse objAssetClasse = new AssetClasse();
            int assetClassID = 0;
            using (DapperContext objContext = new DapperContext())
            {
                objAssetSubCategorie = objContext.SearchAll<AssetSubCategorie>(objAssetSubCategorie).Where(x => x.AssetSubCatID == assetSubCatID).FirstOrDefault();
                if (objAssetSubCategorie != null)
                {
                    assetClassID = Convert.ToInt32(objAssetSubCategorie.AssetClassID);
                }

                var result = objContext.SearchAll<AssetClasse>(objAssetClasse).Where(x => x.AssetClassID == assetClassID);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Asset Conditions"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetConditionsList)]
        public ActionResult AssetConditionsList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetCondition_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AssetConditionsList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetConditionsList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetConditionsList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetConditionCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Assetcondition areaSearch = new Assetcondition();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetCondition.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the asset conditions list by identifier.
        /// </summary>
        /// <param name="assetConditionID">The asset condition identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetConditionsListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetConditionsListByID(int assetConditionID)
        {
            Assetcondition objAssetcondition = new Assetcondition();
            if (assetConditionID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetcondition = context.SelectObject<Assetcondition>(assetConditionID);
                }
            }

            return Json(objAssetcondition, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset conditions.
        /// </summary>
        /// <param name="objAssetcondition">The object asset condition.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetConditions)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetConditions(Assetcondition objAssetcondition)
        {
            string validations = string.Empty;

            if (objAssetcondition.AssetConditionID > 0)
            {
                objAssetcondition.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAssetcondition.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCondition_ChangeMastersCodes)
                {
                    objAssetcondition.AssetConditionCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCondition_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetcondition.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAssetcondition.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCondition_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AssetConditionCode", "AssetCondition", false))
                {
                    if (context.Save(objAssetcondition) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetConditionAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset conditions.
        /// </summary>
        /// <param name="assetConditionID">The asset condition identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetConditions)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetConditions(int assetConditionID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Assetcondition>(assetConditionID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all asset conditions.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllAssetConditions)]
        public ActionResult GetAllAssetConditions()
        {
            Assetcondition objAssetcondition = new Assetcondition();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<Assetcondition>(objAssetcondition);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Asset Status"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetStatusList)]
        public ActionResult AssetStatusList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AssetStatusList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetStatusList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetStatusDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetStatus areaSearch = new AssetStatus();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the asset status list by identifier.
        /// </summary>
        /// <param name="assetStatusID">The asset status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetStatusListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetStatusListByID(int assetStatusID)
        {
            AssetStatus objAssetStatus = new AssetStatus();
            if (assetStatusID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetStatus = context.SelectObject<AssetStatus>(assetStatusID);
                }
            }

            return Json(objAssetStatus, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset status.
        /// </summary>
        /// <param name="objAssetStatus">The object asset status.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetStatus(AssetStatus objAssetStatus)
        {
            string validations = string.Empty;

            if (objAssetStatus.AssetStatusID > 0)
            {
                objAssetStatus.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAssetStatus.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetStatus.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAssetStatus.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AssetStatusDesc"))
                {
                    if (context.Save(objAssetStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetStatusAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset status.
        /// </summary>
        /// <param name="assetStatusID">The asset status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetStatus(int assetStatusID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<AssetStatus>(assetStatusID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all asset status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllAssetStatus)]
        public ActionResult GetAllAssetStatus()
        {
            AssetStatus objAssetStatus = new AssetStatus();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<AssetStatus>(objAssetStatus);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Criticality"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CriticalityList)]
        public ActionResult CriticalityList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetCriticality_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.CriticalityList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the criticality list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCriticalityList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCriticalityList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Criticality";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Criticalities areaSearch = new Criticalities();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.Criticality.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the criticality list by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCriticalityListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCriticalityListByID(int id)
        {
            Criticalities objCriticalities = new Criticalities();
            if (id > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objCriticalities = context.SelectObject<Criticalities>(id);
                }
            }

            return Json(objCriticalities, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the criticality.
        /// </summary>
        /// <param name="objCriticalities">The object criticalities.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCriticality)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCriticality(Criticalities objCriticalities)
        {
            string validations = string.Empty;

            if (objCriticalities.Id > 0)
            {
                objCriticalities.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCriticalities.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCriticality_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCriticalities.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCriticalities.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetCriticality_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("Criticality"))
                {
                    if (context.Save(objCriticalities) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetCriticalityAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the criticality.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCriticality)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCriticality(int id)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Criticalities>(id, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all criticalities.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllCriticalities)]
        public ActionResult GetAllCriticalities()
        {
            Criticalities objCriticalities = new Criticalities();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<Criticalities>(objCriticalities);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "WarrantyContract"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.WarrantyContractList)]
        public ActionResult WarrantyContractList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetWarrantyContract_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.WarrantyContractList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the warranty contract list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWarrantyContractList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWarrantyContractList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Warranty_contract";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            WarrantyContract areaSearch = new WarrantyContract();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.WarrantyContract.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the warranty contract list by identifier.
        /// </summary>
        /// <param name="warranty_contractID">The warranty_contract identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWarrantyContractListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWarrantyContractListByID(int warranty_contractID)
        {
            WarrantyContract objWarrantyContract = new WarrantyContract();
            if (warranty_contractID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objWarrantyContract = context.SelectObject<WarrantyContract>(warranty_contractID);
                }
            }

            return Json(objWarrantyContract, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the warranty contract.
        /// </summary>
        /// <param name="objWarrantyContract">The object warranty contract.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageWarrantyContract)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageWarrantyContract(WarrantyContract objWarrantyContract)
        {
            string validations = string.Empty;

            if (objWarrantyContract.Warranty_contractID > 0)
            {
                objWarrantyContract.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objWarrantyContract.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetWarrantyContract_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objWarrantyContract.CreatedBy = ProjectSession.EmployeeID.ToString();
                objWarrantyContract.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetWarrantyContract_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("Warranty_contract"))
                {
                    if (context.Save(objWarrantyContract) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgAssetWarrantyAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the warranty contract.
        /// </summary>
        /// <param name="warranty_contractID">The warranty_contract identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteWarrantyContract)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWarrantyContract(int warranty_contractID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<WarrantyContract>(warranty_contractID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all warranty contract.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllWarrantyContract)]
        public ActionResult GetAllWarrantyContract()
        {
            WarrantyContract objWarrantyContract = new WarrantyContract();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<WarrantyContract>(objWarrantyContract);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Asset Sub Category"
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AssetSubCategoriesList)]
        public ActionResult AssetSubCategoriesList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AssetSubCategories_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AssetSubCategoriesList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the failure cause list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetSubCategoriesList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetSubCategoriesList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AssetSubCatCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AssetSubCategorie areaSearch = new AssetSubCategorie();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.AssetSubCategories.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the asset sub categories list by identifier.
        /// </summary>
        /// <param name="assetSubCatID">The asset sub cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetSubCategoriesListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAssetSubCategoriesListByID(int assetSubCatID)
        {
            AssetSubCategorie objAssetSubCategorie = new AssetSubCategorie();
            if (assetSubCatID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAssetSubCategorie = context.SelectObject<AssetSubCategorie>(assetSubCatID);
                }
            }

            return Json(objAssetSubCategorie, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the asset sub categories.
        /// </summary>
        /// <param name="objAssetSubCategorie">The object asset sub categories.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAssetSubCategories)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAssetSubCategories(AssetSubCategorie objAssetSubCategorie)
        {
            string validations = string.Empty;

            if (objAssetSubCategorie.AssetSubCatID > 0)
            {
                objAssetSubCategorie.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAssetSubCategorie.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetSubCategories_ChangeMastersCodes)
                {
                    objAssetSubCategorie.AssetCatCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetSubCategories_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAssetSubCategorie.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAssetSubCategorie.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AssetSubCategories_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AssetSubCatCode", "AssetSubCategory", false))
                {
                    if (context.Save(objAssetSubCategorie) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Setups_Configurations_MsgAssetSubCategoryAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the asset sub categories.
        /// </summary>
        /// <param name="assetSubCatID">The asset sub cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAssetSubCategories)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAssetSubCategories(int assetSubCatID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<AssetSubCategorie>(assetSubCatID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the sub asset categories by category identifier.
        /// </summary>
        /// <param name="assetCatID">The asset cat identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSubAssetCategoriesByCategoryID)]
        public ActionResult GetSubAssetCategoriesByCategoryID(int assetCatID)
        {
            AssetSubCategorie objAssetSubCategorie = new AssetSubCategorie();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<AssetSubCategorie>(objAssetSubCategorie).Where(x => x.AssetCatID == assetCatID);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}