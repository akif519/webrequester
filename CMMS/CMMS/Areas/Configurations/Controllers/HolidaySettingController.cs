﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Holiday Setting Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Holidays the setting.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.HolidaySetting)]
        public ActionResult HolidaySetting()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_HolidaySetting_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                IEnumerable<SelectListItem> weekDays = Enum.GetValues(typeof(SystemEnum.WeekDays)).Cast<SystemEnum.WeekDays>().Select(x => new SelectListItem()
                {
                    Text = SystemEnum.GetEnumName(typeof(SystemEnum.WeekDays), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();
                ViewBag.WeekDays = weekDays;

                return View(Views.HolidaySetting);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// _s the edit working day hours.
        /// </summary>
        /// <param name="workingdayId">The working day identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditWorkingDayHours)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _EditWorkingDayHours(int workingdayId)
        {
            WorkingDay objWorkingDay = new WorkingDay();

            IEnumerable<SelectListItem> weekDays = Enum.GetValues(typeof(SystemEnum.WeekDays)).Cast<SystemEnum.WeekDays>().Select(x => new SelectListItem()
            {
                Text = SystemEnum.GetEnumName(typeof(SystemEnum.WeekDays), (int)x),
                Value = ((int)x).ToString()
            }).ToList();
            ViewBag.WeekDays = weekDays;
            WorkingDayHour objWorkingDayHour = new WorkingDayHour();
            if (workingdayId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objWorkingDay = context.SelectObject<WorkingDay>(workingdayId);
                    objWorkingDayHour = new CustomHolidaySettingService().GetWorkingDayHoursByID(workingdayId);
                    objWorkingDay.objWorkingDayHour = objWorkingDayHour;
                }
            }
            else
            {
                objWorkingDay.StartDayOfWeek = SystemEnum.WeekDays.Sunday.GetHashCode();
                objWorkingDayHour.Sunday = true;
                objWorkingDayHour.Monday = true;
                objWorkingDayHour.Tuesday = true;
                objWorkingDayHour.Wednesday = true;
                objWorkingDayHour.Thursday = true;
                objWorkingDayHour.Friday = true;
                objWorkingDayHour.Saturday = true;
                objWorkingDay.L2ID = 0;
                objWorkingDay.L1ID = 0;
                objWorkingDay.objWorkingDayHour = objWorkingDayHour;
            }

            return PartialView(Pages.PartialViews.EditWorkingDayHours, objWorkingDay);
        }

        /// <summary>
        /// Gets the org code by city identifier.
        /// </summary>
        /// <param name="orgId">The org identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetOrgCodeByCityID)]
        public ActionResult GetOrgCodeByCityID(int? orgId)
        {
            L2Service objService = new L2Service();
            var result = objService.GetCityByPermissionAndL1ID(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, ProjectSession.EmployeeID, ConvertTo.Integer(orgId), 0, "", "Ascending", null);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// _s the edit holiday.
        /// </summary>
        /// <param name="holidayId">The holiday identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditHoliday)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _EditHoliday(int holidayId)
        {
            Holiday objHoliday = new Holiday();
            if (holidayId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objHoliday = context.SelectObject<Holiday>(holidayId);
                }
            }
            else
            {
                objHoliday.L2ID = 0;
                objHoliday.L1ID = 0;
                objHoliday.HolidayDate = DateTime.Now;
            }

            return PartialView(Pages.PartialViews.EditHoliday, objHoliday);
        }

        #region "Working Day"

        /// <summary>
        /// Gets the working day list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetWorkingDayList)]
        public ActionResult GetWorkingDayList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomHolidaySettingService objService = new CustomHolidaySettingService();

            var result = new DataSourceResult()
            {
                Data = objService.GetWorkingDay(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the working day by identifier.
        /// </summary>
        /// <param name="workingDayID">The working day identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkingDayByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkingDayByID(int workingDayID)
        {
            WorkingDay objWorkingDay = new WorkingDay();
            if (workingDayID > 0)
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    objWorkingDay = objService.SelectObject<WorkingDay>(workingDayID);
                }
            }

            return Json(objWorkingDay, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the working day.
        /// </summary>
        /// <param name="objWorkingDay">The object working day.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageWorkingDay)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageWorkingDay(WorkingDay objWorkingDay)
        {
            string validations = string.Empty;
            bool flagStatus = false;

            if (objWorkingDay.WorkingDayID > 0)
            {
                L2 objL2 = new L2();
                using (ServiceContext objService = new ServiceContext())
                {
                    objL2 = objWorkingDay.L2ID > 0 ? objService.SelectObject<L2>(Convert.ToInt32(objWorkingDay.L2ID)) : new L2();
                }

                if (objL2.Status == 0)
                {
                    flagStatus = true;
                }
            }
            else
            {
                string[] cityIds = objWorkingDay.L2IDs.Split(',').Select(sValue => sValue.Trim()).ToArray();
                foreach (var id in cityIds)
                {
                    L2 objL2 = new L2();
                    using (ServiceContext objService = new ServiceContext())
                    {
                        objL2 = Convert.ToInt32(id) > 0 ? objService.SelectObject<L2>(Convert.ToInt32(id)) : new L2();
                    }

                    if (objL2.Status == 0)
                    {
                        flagStatus = true;
                    }
                }
            }

            if (flagStatus)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (ModelState.IsValid)
            {
                CustomHolidaySettingService objService = new CustomHolidaySettingService();

                if (objService.SaveWorkingDay(objWorkingDay))
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the working day.
        /// </summary>
        /// <param name="workingDayID">The working day identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteWorkingDay)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkingDay(int workingDayID)
        {
            CustomHolidaySettingService objService = new CustomHolidaySettingService();

            int returnValue = objService.DeleteWorkingDayByID(workingDayID);

            if (returnValue == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -2)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Holiday List"

        /// <summary>
        /// Gets the holiday list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetHolidayList)]
        public ActionResult GetHolidayList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomHolidaySettingService objService = new CustomHolidaySettingService();

            var result = new DataSourceResult()
            {
                Data = objService.GetHoliday(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the holiday by identifier.
        /// </summary>
        /// <param name="holidayID">The holiday identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHolidayByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetHolidayByID(int holidayID)
        {
            Holiday objHoliday = new Holiday();
            if (holidayID > 0)
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    objHoliday = objService.SelectObject<Holiday>(holidayID);
                }
            }

            return Json(objHoliday, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the holiday.
        /// </summary>
        /// <param name="objHoliday">The object holiday.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageHoliday)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageHoliday(Holiday objHoliday)
        {
            string validations = string.Empty;

            bool flagStatus = false;

            if (objHoliday.HolidayID > 0)
            {
                L2 objL2 = new L2();
                using (ServiceContext objService = new ServiceContext())
                {
                    objL2 = objHoliday.L2ID > 0 ? objService.SelectObject<L2>(Convert.ToInt32(objHoliday.L2ID)) : new L2();
                }

                if (objL2.Status == 0)
                {
                    flagStatus = true;
                }
            }
            else
            {
                string[] cityIds = objHoliday.L2IDs.Split(',').Select(sValue => sValue.Trim()).ToArray();
                foreach (var id in cityIds)
                {
                    L2 objL2 = new L2();
                    using (ServiceContext objService = new ServiceContext())
                    {
                        objL2 = Convert.ToInt32(id) > 0 ? objService.SelectObject<L2>(Convert.ToInt32(id)) : new L2();
                    }

                    if (objL2.Status == 0)
                    {
                        flagStatus = true;
                    }
                }
            }

            if (flagStatus)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (ModelState.IsValid)
            {
                CustomHolidaySettingService objService = new CustomHolidaySettingService();

                if (objService.SaveHoliday(objHoliday))
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the holiday.
        /// </summary>
        /// <param name="holidayID">The holiday identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteHoliday)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteHoliday(int holidayID)
        {
            using (ServiceContext objService = new ServiceContext())
            {
                int returnValue = objService.Delete<Holiday>(holidayID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Working Hour"

        /// <summary>
        /// Gets the working hour list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetWorkingHourList)]
        public ActionResult GetWorkingHourList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomHolidaySettingService objService = new CustomHolidaySettingService();

            var result = new DataSourceResult()
            {
                Data = objService.GetWorkingHour(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the working hour by identifier.
        /// </summary>
        /// <param name="workingHourID">The working hour identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkingHourByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetWorkingHourByID(int workingHourID)
        {
            WorkingHour objWorkingHour = new WorkingHour();
            if (workingHourID > 0)
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    objWorkingHour = objService.SelectObject<WorkingHour>(workingHourID);
                }
            }

            return Json(objWorkingHour, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the working hour.
        /// </summary>
        /// <param name="objWorkingHour">The object working hour.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageWorkingHour)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageWorkingHour(WorkingHour objWorkingHour)
        {
            string validations = string.Empty;
            L2 objL2 = new L2();
            using (ServiceContext objService = new ServiceContext())
            {
                objL2 = objWorkingHour.L2ID > 0 ? objService.SelectObject<L2>(Convert.ToInt32(objWorkingHour.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            objWorkingHour.L2Code = objL2.L2Code;
            if (objWorkingHour.WorkingHourID > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_HolidaySetting_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }

                objWorkingHour.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objWorkingHour.ModifiedDate = DateTime.Now;
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_HolidaySetting_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                objWorkingHour.CreatedBy = ProjectSession.EmployeeID.ToString();
                objWorkingHour.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext objService = new ServiceContext("L2ID"))
                {
                    if (objService.Save(objWorkingHour) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.City_MsgCityCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the working hour.
        /// </summary>
        /// <param name="workingHourID">The working hour identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteWorkingHour)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteWorkingHour(int workingHourID)
        {
            using (ServiceContext objService = new ServiceContext())
            {
                int returnValue = objService.Delete<WorkingHour>(workingHourID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion
    }
}