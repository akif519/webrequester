﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service.ConfigurationService;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Maintenances the group.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.MaintenanceGroup)]
        public ActionResult MaintenanceGroup()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceGroups_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.MaintenanceGroup);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the maintenance group.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceGroup([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "GroupCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            MaintGroup maintGroupSearch = new MaintGroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(maintGroupSearch, SystemEnum.Pages.MaintenanceGroup.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the list of all maintenance groups having employees in technicians category
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceGroupHavingTechnicians)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetMaintenanceGroupHavingTechnicians([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "GroupCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Assigntoworkorder objWorkRequest = new Assigntoworkorder();
            MaintenanceGroupService objService = new MaintenanceGroupService();

            var result = new DataSourceResult()
            {
                Data = objService.GetMaintGroupsHavingTechnicians(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the maintenance group by identifier.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceGroupByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceGroupByID(int groupId)
        {
            MaintGroup maintGroup = new MaintGroup();
            if (groupId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    maintGroup = context.SelectObject<MaintGroup>(groupId);
                }
            }

            return Json(maintGroup, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the maintenance group.
        /// </summary>
        /// <param name="maintGroup">The maintenance group.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageMaintenanceGroup(MaintGroup maintGroup)
        {
            string validations = string.Empty;

            L2 objL2 = new L2();
            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = maintGroup.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(maintGroup.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (maintGroup.GroupID > 0)
            {
                maintGroup.ModifiedBy = ProjectSession.EmployeeID.ToString();
                maintGroup.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceGroups_ChangeMastersCodes)
                {
                    maintGroup.GroupCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceGroups_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                maintGroup.CreatedBy = ProjectSession.EmployeeID.ToString();
                maintGroup.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceGroups_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("GroupCode"))
                {
                    if (context.Save(maintGroup) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.MaintenanceGroup_MsgMaintGroupCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the maintenance group.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMaintenanceGroup(int groupId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<MaintGroup>(groupId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the employee maintenance group.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="maintGroupID">The maintenance group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeMaintenanceGroup([DataSourceRequest]DataSourceRequest request, int? maintGroupID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            if (maintGroupID == 0)
            {
                maintGroupID = null;
            }

            CustomMaintGroup objCustomMaintGroup = new CustomMaintGroup();
            var result = new DataSourceResult()
            {
                Data = objCustomMaintGroup.GetMaintGroupEmployeeInfoSearch(maintGroupID, pageNumber, sortExpression, sortDirection, request: request).Distinct(),
                Total = objCustomMaintGroup.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// _s the edit maintenance group.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditMaintenanceGroup)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _EditMaintenanceGroup(int groupId)
        {
            MaintGroup maintGroup = new MaintGroup();
            if (groupId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    maintGroup = context.SelectObject<MaintGroup>(groupId);
                }
            }

            return PartialView(Pages.PartialViews.EditMaintenanceGroup, maintGroup);
        }

        /// <summary>
        /// Manages the employee maintenance group.
        /// </summary>
        /// <param name="objEmployeesmaintgroup">The object employees group.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmployeeMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmployeeMaintenanceGroup(Employeesmaintgroup objEmployeesmaintgroup)
        {
            string validations = string.Empty;

            if (objEmployeesmaintgroup.MaintGroupId > 0)
            {
                objEmployeesmaintgroup.CreatedBy = ProjectSession.EmployeeID.ToString();
                objEmployeesmaintgroup.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                CustomMaintGroup context = new CustomMaintGroup();

                if (context.InsertUpdateEmployeeMaintGroup(objEmployeesmaintgroup))
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the employee maintenance group.
        /// </summary>
        /// <param name="maintGroupID">The maintenance group identifier.</param>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmployeeMaintenanceGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeMaintenanceGroup(int maintGroupID, int employeeId)
        {
            CustomMaintGroup context = new CustomMaintGroup();

            if (context.DeleteEmployeeMaintGroup(maintGroupID, employeeId))
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}