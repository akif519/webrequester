﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// General Configuration
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Generals the configuration.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GeneralConfiguration)]
        public ActionResult GeneralConfiguration()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_GeneralConfiguration_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                CMMS.Model.Configurations objconfiguration = new CMMS.Model.Configurations();
                using (DapperContext objDapperContext = new DapperContext())
                {
                    List<CMMS.Model.Configurations> lst = objDapperContext.SearchAll(objconfiguration).ToList();
                    objconfiguration.JobOrderAssignedToStatus = lst.Where(o => o.ConfigKey == "JobOrderAssignedToStatus").FirstOrDefault().ConfigValue;
                    objconfiguration.TechnicianCategory = lst.Where(o => o.ConfigKey == "TechnicianCategory").FirstOrDefault().ConfigValue;
                    objconfiguration.SubStoreConsume = lst.Where(o => o.ConfigKey == "SubStoreConsume").FirstOrDefault().ConfigValue;
                    objconfiguration.SubStorePersonal = lst.Where(o => o.ConfigKey == "SubStorePersonal").FirstOrDefault().ConfigValue;
                    objconfiguration.CityStore = lst.Where(o => o.ConfigKey == "CityStore").FirstOrDefault().ConfigValue;
                    objconfiguration.JobOrderMaterialRequestedStatus = lst.Where(o => o.ConfigKey == "JobOrderMaterialRequestedStatus").FirstOrDefault().ConfigValue;
                }

                return View(Views.GeneralConfiguration, objconfiguration);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Updates the general configuration.
        /// </summary>
        /// <param name="objConfigurations">The object configurations.</param>
        /// <returns></returns>
        [ActionName(Actions.UpdateGeneralConfiguration)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult UpdateGeneralConfiguration(CMMS.Model.Configurations objConfigurations)
        {
            if (!ProjectSession.PermissionAccess.SetupConfiguration_GeneralConfiguration_Editrecords)
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
            }
            else
            {
                L2 objL2 = new L2();
                using (ServiceContext objService = new ServiceContext())
                {
                    objL2 = Convert.ToInt32(objConfigurations.CityStore) > 0 ? objService.SelectObject<L2>(Convert.ToInt32(objConfigurations.CityStore)) : new L2();
                }

                if (objL2.Status == 0)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Asset_MsgSiteInactive;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }
                else
                {
                    EmailConfigurationService objService = new EmailConfigurationService();
                    List<CMMS.Model.Configurations> lstConfigurations = new List<CMMS.Model.Configurations>();

                    CMMS.Model.Configurations objTempConfigurations = new CMMS.Model.Configurations();

                    objTempConfigurations.ConfigKey = "JobOrderAssignedToStatus";
                    objTempConfigurations.ConfigValue = objConfigurations.JobOrderAssignedToStatus;
                    lstConfigurations.Add(objTempConfigurations);
                    objTempConfigurations = new CMMS.Model.Configurations();
                    objTempConfigurations.ConfigKey = "TechnicianCategory";
                    objTempConfigurations.ConfigValue = objConfigurations.TechnicianCategory;
                    lstConfigurations.Add(objTempConfigurations);
                    objTempConfigurations = new CMMS.Model.Configurations();
                    objTempConfigurations.ConfigKey = "SubStoreConsume";
                    objTempConfigurations.ConfigValue = objConfigurations.SubStoreConsume;
                    lstConfigurations.Add(objTempConfigurations);
                    objTempConfigurations = new CMMS.Model.Configurations();
                    objTempConfigurations.ConfigKey = "SubStorePersonal";
                    objTempConfigurations.ConfigValue = objConfigurations.SubStorePersonal;
                    lstConfigurations.Add(objTempConfigurations);
                    objTempConfigurations = new CMMS.Model.Configurations();
                    objTempConfigurations.ConfigKey = "CityStore";
                    objTempConfigurations.ConfigValue = objConfigurations.CityStore;
                    lstConfigurations.Add(objTempConfigurations);
                    objTempConfigurations = new CMMS.Model.Configurations();
                    objTempConfigurations.ConfigKey = "JobOrderMaterialRequestedStatus";
                    objTempConfigurations.ConfigValue = objConfigurations.JobOrderMaterialRequestedStatus;
                    lstConfigurations.Add(objTempConfigurations);

                    bool isUpdated = false;

                    isUpdated = objService.UpdateGeneralConfigurations(lstConfigurations);

                    if (isUpdated)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                }
            }

            return RedirectToAction(Actions.GeneralConfiguration, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
        }
    }
}