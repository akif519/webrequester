﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using CMMS.Controllers;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Entry Type
    /// </summary>
    public enum EntryType
    {
        File = 0,
        Directory
    }

    /// <summary>
    /// File Browser Entry
    /// </summary>
    public class FileBrowserEntry
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public EntryType Type { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public long Size { get; set; }
    }

    /// <summary>
    /// Image Size
    /// </summary>
    public class ImageSize
    {
        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Image Browser Controller
    /// </summary>
    public partial class ImageBrowserController : BaseController
    {
        /// <summary>
        /// The content folder root
        /// </summary>
        private const string ContentFolderRoot = "~/Uploads/";

        /// <summary>
        /// The pretty name
        /// </summary>
        private const string PrettyName = "HelpUpload/";

        /// <summary>
        /// The default filter
        /// </summary>
        private const string DefaultFilter = "*.png,*.gif,*.jpg,*.jpeg";

        /// <summary>
        /// The thumbnail height
        /// </summary>
        private const int ThumbnailHeight = 80;

        /// <summary>
        /// The thumbnail width
        /// </summary>
        private const int ThumbnailWidth = 80;

        /// <summary>
        /// The directory browser
        /// </summary>
        private readonly DirectoryBrowser directoryBrowser;

        /// <summary>
        /// The thumbnail creator
        /// </summary>
        private readonly ThumbnailCreator thumbnailCreator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageBrowserController"/> class.
        /// </summary>
        public ImageBrowserController()
        {
            directoryBrowser = new DirectoryBrowser();
            thumbnailCreator = new ThumbnailCreator();
        }

        /// <summary>
        /// Gets the content path.
        /// </summary>
        /// <value>
        /// The content path.
        /// </value>
        public string ContentPath
        {
            get
            {
                return Path.Combine(ContentFolderRoot, PrettyName);
            }
        }

        /// <summary>
        /// To the absolute.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <returns></returns>
        private string ToAbsolute(string virtualPath)
        {
            return VirtualPathUtility.ToAbsolute(virtualPath);
        }

        /// <summary>
        /// Combines the paths.
        /// </summary>
        /// <param name="basePath">The base path.</param>
        /// <param name="relativePath">The relative path.</param>
        /// <returns></returns>
        private string CombinePaths(string basePath, string relativePath)
        {
            return VirtualPathUtility.Combine(VirtualPathUtility.AppendTrailingSlash(basePath), relativePath);
        }

        /// <summary>
        /// Authorizes the read.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public virtual bool AuthorizeRead(string path)
        {
            return CanAccess(path);
        }

        /// <summary>
        /// Determines whether this instance can access the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        protected virtual bool CanAccess(string path)
        {
            return path.StartsWith(ToAbsolute(ContentPath), StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Normalizes the path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private string NormalizePath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return ToAbsolute(ContentPath);
            }

            return CombinePaths(ToAbsolute(ContentPath), path);
        }

        /// <summary>
        /// Reads the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        /// <exception cref="System.Web.HttpException">
        /// 404;File Not Found
        /// or
        /// 403;Forbidden
        /// </exception>
        public virtual JsonResult Read(string path)
        {
            path = NormalizePath(path);

            if (AuthorizeRead(path))
            {
                try
                {
                    directoryBrowser.Server = Server;

                    var result = directoryBrowser
                        .GetContent(path, DefaultFilter)
                        .Select(f => new
                        {
                            name = f.Name,
                            type = f.Type == EntryType.File ? "f" : "d",
                            size = f.Size
                        });

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                catch (DirectoryNotFoundException)
                {
                    throw new HttpException(404, "File Not Found");
                }
            }

            throw new HttpException(403, "Forbidden");
        }

        /// <summary>
        /// Authorizes the thumbnail.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public virtual bool AuthorizeThumbnail(string path)
        {
            return CanAccess(path);
        }

        /// <summary>
        /// Thumbnails the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        /// <exception cref="System.Web.HttpException">
        /// 404;File Not Found
        /// or
        /// 403;Forbidden
        /// </exception>
        [OutputCache(Duration = 3600, VaryByParam = "path")]
        public virtual ActionResult Thumbnail(string path)
        {
            path = NormalizePath(path);

            if (AuthorizeThumbnail(path))
            {
                var physicalPath = Server.MapPath(path);

                if (System.IO.File.Exists(physicalPath))
                {
                    Response.AddFileDependency(physicalPath);

                    return CreateThumbnail(physicalPath);
                }
                else
                {
                    throw new HttpException(404, "File Not Found");
                }
            }
            else
            {
                throw new HttpException(403, "Forbidden");
            }
        }

        /// <summary>
        /// Creates the thumbnail.
        /// </summary>
        /// <param name="physicalPath">The physical path.</param>
        /// <returns></returns>
        private FileContentResult CreateThumbnail(string physicalPath)
        {
            using (var fileStream = System.IO.File.OpenRead(physicalPath))
            {
                var desiredSize = new ImageSize
                {
                    Width = ThumbnailWidth,
                    Height = ThumbnailHeight
                };

                const string ContentType = "image/png";

                return File(thumbnailCreator.Create(fileStream, desiredSize, ContentType), ContentType);
            }
        }

        /// <summary>
        /// Destroys the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="name">The name.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="System.Web.HttpException">404;File Not Found</exception>
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Destroy(string path, string name, string type)
        {
            path = NormalizePath(path);

            if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(type))
            {
                path = CombinePaths(path, name);
                if (type.ToLowerInvariant() == "f")
                {
                    DeleteFile(path);
                }
                else
                {
                    DeleteDirectory(path);
                }

                return Json(null);
            }

            throw new HttpException(404, "File Not Found");
        }

        /// <summary>
        /// Authorizes the delete file.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public virtual bool AuthorizeDeleteFile(string path)
        {
            return CanAccess(path);
        }

        /// <summary>
        /// Authorizes the delete directory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public virtual bool AuthorizeDeleteDirectory(string path)
        {
            return CanAccess(path);
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="path">The path.</param>
        protected virtual void DeleteFile(string path)
        {
            if (!AuthorizeDeleteFile(path))
            {
                throw new HttpException(403, "Forbidden");
            }

            var physicalPath = Server.MapPath(path);

            if (System.IO.File.Exists(physicalPath))
            {
                System.IO.File.Delete(physicalPath);
            }
        }

        /// <summary>
        /// Deletes the directory.
        /// </summary>
        /// <param name="path">The path.</param>
        protected virtual void DeleteDirectory(string path)
        {
            if (!AuthorizeDeleteDirectory(path))
            {
                throw new HttpException(403, "Forbidden");
            }

            var physicalPath = Server.MapPath(path);

            if (Directory.Exists(physicalPath))
            {
                Directory.Delete(physicalPath, true);
            }
        }

        /// <summary>
        /// Authorizes the create directory.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public virtual bool AuthorizeCreateDirectory(string path, string name)
        {
            return CanAccess(path);
        }

        /// <summary>
        /// Creates the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="entry">The entry.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Create(string path, FileBrowserEntry entry)
        {
            path = NormalizePath(path);
            var name = entry.Name;

            if (!string.IsNullOrEmpty(name) && AuthorizeCreateDirectory(path, name))
            {
                var physicalPath = Path.Combine(Server.MapPath(path), name);

                if (!Directory.Exists(physicalPath))
                {
                    Directory.CreateDirectory(physicalPath);
                }

                return Json(null);
            }

            throw new HttpException(403, "Forbidden");
        }

        /// <summary>
        /// Authorizes the upload.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        public virtual bool AuthorizeUpload(string path, HttpPostedFileBase file)
        {
            return CanAccess(path) && IsValidFile(file.FileName);
        }

        /// <summary>
        /// Determines whether [is valid file] [the specified file name].
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        private bool IsValidFile(string fileName)
        {
            var extension = Path.GetExtension(fileName);
            var allowedExtensions = DefaultFilter.Split(',');

            return allowedExtensions.Any(e => e.EndsWith(extension, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Uploads the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Upload(string path, HttpPostedFileBase file)
        {
            path = NormalizePath(path);
            var fileName = Path.GetFileName(file.FileName);

            if (AuthorizeUpload(path, file))
            {
                file.SaveAs(Path.Combine(Server.MapPath(path), fileName));

                return Json(new { size = file.ContentLength, name = fileName, type = "f" }, "text/plain");
            }

            throw new HttpException(403, "Forbidden");
        }

        /// <summary>
        /// Images the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        [OutputCache(Duration = 360, VaryByParam = "path")]
        public ActionResult Image(string path)
        {
            path = NormalizePath(path);

            if (AuthorizeImage(path))
            {
                var physicalPath = Server.MapPath(path);

                if (System.IO.File.Exists(physicalPath))
                {
                    const string ContentType = "image/png";
                    return File(System.IO.File.OpenRead(physicalPath), ContentType);
                }
            }

            throw new HttpException(403, "Forbidden");
        }

        /// <summary>
        /// Authorizes the image.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public virtual bool AuthorizeImage(string path)
        {
            return CanAccess(path) && IsValidFile(Path.GetExtension(path));
        }
    }

    /// <summary>
    /// Directory Browser
    /// </summary>
    public class DirectoryBrowser
    {
        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        public System.Web.HttpServerUtilityBase Server { get; set; }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public IEnumerable<FileBrowserEntry> GetContent(string path, string filter)
        {
            return this.GetFiles(path, filter).Concat(this.GetDirectories(path));
        }

        /// <summary>
        /// Gets the files.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        private IEnumerable<FileBrowserEntry> GetFiles(string path, string filter)
        {
            var directory = new DirectoryInfo(this.Server.MapPath(path));

            var extensions = (filter ?? "*").Split(",|;".ToCharArray(), System.StringSplitOptions.RemoveEmptyEntries);

            return extensions.SelectMany(directory.GetFiles)
                .Select(file => new FileBrowserEntry
                {
                    Name = file.Name,
                    Size = file.Length,
                    Type = EntryType.File
                });
        }

        /// <summary>
        /// Gets the directories.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private IEnumerable<FileBrowserEntry> GetDirectories(string path)
        {
            var directory = new DirectoryInfo(this.Server.MapPath(path));

            return directory.GetDirectories()
                .Select(subDirectory => new FileBrowserEntry
                {
                    Name = subDirectory.Name,
                    Type = EntryType.Directory
                });
        }
    }

    /// <summary>
    /// Image Resizer
    /// </summary>
    public class ImageResizer
    {
        /// <summary>
        /// Resizes the specified original size.
        /// </summary>
        /// <param name="originalSize">Size of the original.</param>
        /// <param name="targetSize">Size of the target.</param>
        /// <returns></returns>
        public ImageSize Resize(ImageSize originalSize, ImageSize targetSize)
        {
            var aspectRatio = (float)originalSize.Width / (float)originalSize.Height;
            var width = targetSize.Width;
            var height = targetSize.Height;

            if (originalSize.Width > targetSize.Width || originalSize.Height > targetSize.Height)
            {
                if (aspectRatio > 1)
                {
                    height = (int)(targetSize.Height / aspectRatio);
                }
                else
                {
                    width = (int)(targetSize.Width * aspectRatio);
                }
            }
            else
            {
                width = originalSize.Width;
                height = originalSize.Height;
            }

            return new ImageSize
            {
                Width = Math.Max(width, 1),
                Height = Math.Max(height, 1)
            };
        }
    }

    /// <summary>
    /// Thumbnail Creator
    /// </summary>
    public class ThumbnailCreator
    {
        /// <summary>
        /// The image formats
        /// </summary>
        private static readonly IDictionary<string, ImageFormat> ImageFormats = new Dictionary<string, ImageFormat>
        {
            { "image/png", ImageFormat.Png },
            { "image/gif", ImageFormat.Gif },
            { "image/jpeg", ImageFormat.Jpeg }
        };

        /// <summary>
        /// The resizer
        /// </summary>
        private readonly ImageResizer resizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ThumbnailCreator"/> class.
        /// </summary>
        public ThumbnailCreator()
        {
            this.resizer = new ImageResizer();
        }

        /// <summary>
        /// Creates the specified source.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="desiredSize">Size of the desired.</param>
        /// <param name="contentType">Type of the content.</param>
        /// <returns></returns>
        public byte[] Create(Stream source, ImageSize desiredSize, string contentType)
        {
            using (var image = Image.FromStream(source))
            {
                var originalSize = new ImageSize
                {
                    Height = image.Height,
                    Width = image.Width
                };

                var size = this.resizer.Resize(originalSize, desiredSize);

                using (var thumbnail = new Bitmap(size.Width, size.Height))
                {
                    this.ScaleImage(image, thumbnail);

                    using (var memoryStream = new MemoryStream())
                    {
                        thumbnail.Save(memoryStream, ImageFormats[contentType]);

                        return memoryStream.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Scales the image.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        private void ScaleImage(Image source, Image destination)
        {
            using (var graphics = Graphics.FromImage(destination))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                graphics.DrawImage(source, 0, 0, destination.Width, destination.Height);
            }
        }
    }
}