﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Employees the category.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.EmployeeCategory)]
        public ActionResult EmployeeCategory()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_EmployeeCategory_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.EmployeeCategory);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the employee category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeCategory([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CategoryName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Employeecategorie employeecategorySearch = new Employeecategorie();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(employeecategorySearch, SystemEnum.Pages.EmployeeCategory.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the employee category by identifier.
        /// </summary>
        /// <param name="empCategoryId">The employee category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeCategoryByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeCategoryByID(int empCategoryId)
        {
            Employeecategorie empCategory = new Employeecategorie();
            IList<Employeecategorie> lstEmpCategory = new List<Employeecategorie>();

            if (empCategoryId > 0)
            {
                using (ServiceContext empCategoryService = new ServiceContext())
                {
                    empCategory = empCategoryService.SelectObject<Employeecategorie>(empCategoryId);
                }

                return Json(empCategory, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    empCategory = new Employeecategorie();
                    lstEmpCategory = context.SearchAll(empCategory);
                }

                return Json(lstEmpCategory, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the employee category.
        /// </summary>
        /// <param name="empCategory">The employee category.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmployeeCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmployeeCategory(Employeecategorie empCategory)
        {
            string validations = string.Empty;

            if (empCategory.CategoryID > 0)
            {
                empCategory.ModifiedBy = ProjectSession.EmployeeID.ToString();
                empCategory.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_EmployeeCategory_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                empCategory.CreatedBy = ProjectSession.EmployeeID.ToString();
                empCategory.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_EmployeeCategory_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext empCategoryService = new ServiceContext("CategoryName"))
                {
                    if (empCategoryService.Save(empCategory) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgEmployeeCategoryAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the employee category.
        /// </summary>
        /// <param name="empCategoryId">The employee category identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmployeeCategory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeCategory(int empCategoryId)
        {
            using (ServiceContext empCategoryService = new ServiceContext())
            {
                int returnValue = empCategoryService.Delete<Employeecategorie>(empCategoryId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// To get All Employee Categories
        /// </summary>
        /// <returns>List Of employee categories without any filtering</returns>
        [ActionName(Actions.GetEmpCategories)]
        public ActionResult GetEmpCategories()
        {
            Employeecategorie objEmpCat = new Employeecategorie();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<Employeecategorie>(objEmpCat);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}