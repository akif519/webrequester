﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.IO;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Model;
using CMMS.Services;
using CMMS.Infrastructure;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Suppliers this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Suppliers)]
        public ActionResult Suppliers()
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            if (ProjectSession.PermissionAccess.SetupConfiguration_Suppliers_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                IEnumerable<SelectListItem> supplierContractorCategory = Enum.GetValues(typeof(SystemEnum.SupplierContractorCategory)).Cast<SystemEnum.SupplierContractorCategory>().Select(x => new SelectListItem()
                {
                    Text = Enum.GetName(typeof(SystemEnum.SupplierContractorCategory), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();

                ViewBag.supplierContractorCategory = supplierContractorCategory;

                return View(Views.Suppliers);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the suppliers list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="supplierNo">The supplier no.</param>
        /// <param name="supplierName">Name of the supplier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSuppliersList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSuppliersList([DataSourceRequest]DataSourceRequest request, string supplierNo, string supplierName)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SupplierNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Supplier areaSearch = new Supplier();
            areaSearch.SupplierNo = supplierNo;
            areaSearch.SupplierName = supplierName;

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.Supplier.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the All suppliers list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllSupplierList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAllSupplierList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SupplierID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Supplier objSupplier = new Supplier();

            SupplierService obj = new SupplierService();
            var result = new DataSourceResult()
            {
                Data = obj.GetAllSuppliers(string.Empty, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the suppliers list by identifier.
        /// </summary>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSuppliersListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSuppliersListByID(int supplierID)
        {
            Supplier objSupplier = new Supplier();
            IList<Supplier> lstSupplier = new List<Supplier>();

            if (supplierID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSupplier = context.SelectObject<Supplier>(supplierID);
                }

                return Json(objSupplier, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objSupplier = new Supplier();
                    lstSupplier = context.SearchAll<Supplier>(objSupplier);
                }

                return Json(lstSupplier, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// _s the edit suppliers.
        /// </summary>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._EditSuppliers)]
        public ActionResult _EditSuppliers(int supplierID)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            Supplier objSupplier = new Supplier();
            if (supplierID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSupplier = context.SelectObject<Supplier>(supplierID);
                }
            }

            return View(Pages.PartialViews.EditSuppliers, objSupplier);
        }

        /// <summary>
        /// Manages the suppliers.
        /// </summary>
        /// <param name="objSupplier">The object supplier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSuppliers)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ManageSuppliers(Supplier objSupplier)
        {
            string validations = string.Empty;
            int newSupplierID = 0;
            if (ModelState.IsValid)
            {
                if (objSupplier.SupplierID > 0)
                {
                    objSupplier.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objSupplier.ModifiedDate = DateTime.Now;

                    if (!ProjectSession.PermissionAccess.SetupConfiguration_Suppliers_Editrecords)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        ////return RedirectToAction(Actions.Suppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
                        return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
                    }

                    bool result = SupplierService.CheckDupliacteSupplier(objSupplier.SupplierID, objSupplier.SupplierNo);
                    if (result)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Supplier_MsgSupplierNoAlreadyExists;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
                    }
                    else
                    {
                        SupplierService.UpdateSupplier(objSupplier);
                    }

                    newSupplierID = objSupplier.SupplierID;
                }
                else
                {
                    objSupplier.CreatedBy = ProjectSession.EmployeeID.ToString();
                    objSupplier.CreatedDate = DateTime.Now;

                    if (!ProjectSession.PermissionAccess.SetupConfiguration_Suppliers_Createnewrecords)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        ////return RedirectToAction(Actions.Suppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
                        return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
                    }

                    using (DapperContext context = new DapperContext("SupplierNo", "SupplierName", false))
                    {
                        newSupplierID = context.Save(objSupplier);
                    }
                }

                if (newSupplierID > 0)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                }
                else
                {
                    TempData["Message"] = ProjectSession.Resources.message.Supplier_MsgSupplierNoAlreadyExists;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    newSupplierID = 0;
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }
                else
                {
                    TempData["Message"] = validations.TrimEnd(',');
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }
            }

            ////return RedirectToAction(Actions.Suppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
            return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = newSupplierID });
            ////return PartialView(Pages.PartialViews.EditSuppliers, objSupplier);
        }

        /// <summary>
        /// Manages the suppliers contact.
        /// </summary>
        /// <param name="objSupplier">The object supplier.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSuppliersContact)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ManageSuppliersContact(Supplier objSupplier)
        {
            try
            {
                string validations = string.Empty;

                if (objSupplier.SupplierID > 0)
                {
                    objSupplier.ModifiedBy = ProjectSession.EmployeeID.ToString();
                    objSupplier.ModifiedDate = DateTime.Now;

                    if (!ProjectSession.PermissionAccess.SetupConfiguration_Suppliers_Editrecords)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
                    }
                }
                else
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
                }

                SupplierService.UpdateSupplierContact(objSupplier);
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                return RedirectToAction(Actions._EditSuppliers, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations, supplierID = objSupplier.SupplierID });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the suppliers.
        /// </summary>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSuppliers)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSuppliers(int supplierID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Supplier>(supplierID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the supplier category.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetSupplierCategory)]
        public ActionResult GetSupplierCategory()
        {
            SupplierCategory objSupplierCategory = new SupplierCategory();
            List<SupplierCategory> lstSupplierCategory = new List<SupplierCategory>();

            using (DapperContext context = new DapperContext())
            {
                lstSupplierCategory = context.SearchAll<SupplierCategory>(objSupplierCategory).ToList();
            }

            return Json(lstSupplierCategory, JsonRequestBehavior.AllowGet);
        }

        #region "Document"

        /// <summary>
        /// _s the supplier documents.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._SupplierDocuments)]
        public ActionResult _SupplierDocuments()
        {
            CmSupplierContractorDocument model = new CmSupplierContractorDocument();
            return View(PartialViews.SupplierDocuments, model);
        }

        /// <summary>
        /// Gets the supplier document.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="supplierID">The supplier identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSupplierDocument)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetSupplierDocument([DataSourceRequest]DataSourceRequest request, int supplierID)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            SupplierService objService = new SupplierService();

            var result = new DataSourceResult()
            {
                Data = objService.GetSupplierDocument(supplierID, pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the supplier document by identifier.
        /// </summary>
        /// <param name="docID">The document identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSupplierDocumentByID)]
        [HttpPost]
        public ActionResult GetSupplierDocumentByID(int docID)
        {
            CmSupplierContractorDocument objDoc = new CmSupplierContractorDocument();
            if (docID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objDoc = context.SelectObject<CmSupplierContractorDocument>(docID);
                }
            }

            return Json(objDoc, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the attachment by identifier.
        /// </summary>
        /// <param name="docID">The document identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAttachmentByID)]
        [HttpPost]
        public ActionResult GetAttachmentByID(int docID)
        {
            CmAttachment objAttachment = new CmAttachment();
            List<CmAttachment> lst = new List<CmAttachment>();
            objAttachment.FkId = Convert.ToString(docID);
            objAttachment.ModuleType = SystemEnum.ModuleType.Supplier.ToString();
            if (docID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    lst = context.Search<CmAttachment>(objAttachment).ToList();
                }
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Saves the supplier document.
        /// </summary>
        /// <param name="objDoc">The object document.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveSupplierDoc)]
        [HttpPost]
        public ActionResult SaveSupplierDoc(CmSupplierContractorDocument objDoc)
        {
            string validations = string.Empty;
            int newDocId = 0;
            List<HttpPostedFileBase> files = new List<HttpPostedFileBase>();
            HttpPostedFileBase file;
            if (objDoc.DocID > 0)
            {
                objDoc.ModifiedBy = ProjectSession.EmployeeID;
                objDoc.ModifiedDate = DateTime.Now;
            }
            else
            {
                objDoc.CreatedBy = ProjectSession.EmployeeID;
                objDoc.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                int count = SupplierService.checkDocumentDuplicate(objDoc.DocID, objDoc.DocName, Convert.ToInt32(objDoc.SupplierContractorID));
                if (count > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDocumentExists }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    using (DapperContext context = new DapperContext())
                    {
                        newDocId = context.Save(objDoc);
                    }
                }

                if (newDocId > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        file = Request.Files[i];
                        if (!("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(Request.Files[i].FileName) + ","))
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed }, JsonRequestBehavior.AllowGet);
                        }
                        else if (((Request.Files[i].ContentLength / 1024) / 1024) > ProjectSession.AllowedMaxFilesize)                        
                        {
                            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgFileSizeNotAllowed + ProjectSession.AllowedMaxFilesize + "MB." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            ////Save file in folder and db
                            string filePath = null;
                            string subPath = ProjectConfiguration.UploadPath + ProjectConfiguration.UploadPathSupplier + objDoc.SupplierContractorID;
                            bool folderExists = Directory.Exists(subPath);
                            if (!folderExists)
                            {
                                Directory.CreateDirectory(subPath);
                            }

                            string fileUniqueName = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
                            string fileName = fileUniqueName + "_" + Path.GetFileName(Request.Files[i].FileName);
                            filePath = subPath + @"\" + fileName;
                            if (System.IO.File.Exists(filePath))
                            {
                                System.IO.File.Delete(filePath);
                            }

                            file.SaveAs(filePath);

                            CmAttachment objcmAttachment = new CmAttachment();
                            objcmAttachment.FkId = Convert.ToString(newDocId);
                            objcmAttachment.FileLink = ProjectConfiguration.UploadPathSupplier + objDoc.SupplierContractorID + @"\" + fileName;
                            objcmAttachment.FileName = fileName;
                            objcmAttachment.ModuleType = SystemEnum.ModuleType.Supplier.ToString();
                            objcmAttachment.CreatedBy = ProjectSession.EmployeeID;
                            objcmAttachment.CreatedDate = DateTime.Now;

                            using (ServiceContext context = new ServiceContext())
                            {
                                int fileId = context.Save<CmAttachment>(objcmAttachment);
                            }
                        }
                    }

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgDocumentExists }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Deletes the supplier document.
        /// </summary>
        /// <param name="docID">The document identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSupplierDoc)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSupplierDoc(int docID)
        {
            int returnValue;
            if (docID > 0)
            {
                CmAttachment objAttachment = new CmAttachment();
                List<CmAttachment> lstAttachment = new List<CmAttachment>();
                objAttachment.FkId = Convert.ToString(docID);
                objAttachment.ModuleType = SystemEnum.ModuleType.Supplier.ToString();
                using (DapperContext context = new DapperContext())
                {
                    lstAttachment = context.Search<CmAttachment>(objAttachment).ToList();
                }

                if (lstAttachment != null && lstAttachment.Count > 0)
                {
                    for (int i = 0; i < lstAttachment.Count; i++)
                    {
                        if (System.IO.File.Exists(ProjectConfiguration.UploadPath + lstAttachment[i].FileLink))
                        {
                            System.IO.File.Delete(ProjectConfiguration.UploadPath + lstAttachment[i].FileLink);
                        }

                        using (DapperContext context = new DapperContext())
                        {
                            context.Delete<CmAttachment>(lstAttachment[i].AutoId);
                        }
                    }
                }

                ////Delete from cmSupplierContractorDocument
                using (DapperContext context = new DapperContext())
                {
                    returnValue = context.Delete<CmSupplierContractorDocument>(docID);
                }

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}