﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Auto Complete Controller
    /// </summary>
    public class AutoCompleteController : BaseController
    {
        /// <summary>
        /// Indexes the Class.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets Store Items List.
        /// </summary>
        /// <param name="text">The user input text to search.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodesAutoDDL)]
        public ActionResult GetStockCodesAutoDDL(string text)
        {
            string sortExpression = "StockID";
            string sortDirection = "Descending";
            JobOrderService objService = new JobOrderService();
            int subStoreID = 0;
            int pageNumber = 0;
            string strWhere = string.Empty;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere = " and (lower(StockNo) like N'%" + text + "%' or lower(StockDescription) like N'%" + text + "%' or lower(AltStockDescription) like N'%" + text + "%')";
            }
            else
            {
                strWhere = " and (StockNo like N'%" + text + "%' or StockDescription like N'%" + text + "%' or AltStockDescription like N'%" + text + "%')";
            }

            var result = objService.GetStockCodePageWithBalance(subStoreID, strWhere, pageNumber, sortExpression, sortDirection);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets Store Items List.
        /// </summary>
        /// <param name="text">The user input text to search.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationAutoDDL)]
        public ActionResult GetLocationAutoDDL(string text)
        {
            string sortExpression = "LocationID";
            string sortDirection = "Descending";
            LocationService objService = new LocationService();
            int pageNumber = 0;
            string strWhere = string.Empty;


            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere = " and (lower(LocationNo) like N'%" + text + "%' or lower(LocationDescription) like N'%" + text + "%' or lower(LocationAltDescription) like N'%" + text + "%')";
            }
            else
            {
                strWhere = " and (LocationNo like N'%" + text + "%' or LocationDescription like N'%" + text + "%' or LocationAltDescription like N'%" + text + "%')";
            }

            var result = objService.GetLocationPage(strWhere, ProjectSession.EmployeeID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets Suppliers List.
        /// </summary>
        /// <param name="text">The user input text to search.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSupplierAutoDDL)]
        public ActionResult GetSupplierAutoDDL(string text)
        {
            string sortExpression = "SupplierID";
            string sortDirection = "Descending";
            SupplierService objService = new SupplierService();
            int pageNumber = 0;
            string strWhere = string.Empty;


            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere = " and (lower(SupplierNo) like N'%" + text + "%' or lower(SupplierName) like N'%" + text + "%' or lower(AltSupplierName) like N'%" + text + "%')";
            }
            else
            {
                strWhere = " and (SupplierNo like N'%" + text + "%' or SupplierName like N'%" + text + "%' or AltSupplierName like N'%" + text + "%')";
            }

            var result = objService.GetAllSuppliers(strWhere, pageNumber, sortExpression, sortDirection);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the list of all employees by assignments
        /// </summary>
        /// <param name="cityID">City ID</param>
        /// <param name="type">The type.</param>
        /// <param name="text">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeAssignPageAutoDDL)]
        public ActionResult GetEmployeeAssignPageAutoDDL(int cityID = 0, int type = 0, string text = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;
            int empID = ProjectSession.EmployeeID;

            int pageNumber = 0;

            EmployeeService objService = new EmployeeService();

            if (type == 2)
            {
                string techCatID = Common.GetConfigKeyValue("TechnicianCategory");
                strWhere += " AND employees.CategoryID = " + (techCatID == string.Empty ? "0" : techCatID);
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(employees.EmployeeNO) like N'%" + text + "%' or lower(employees.Name) like N'%" + text + "%' or lower(employees.AltName) like N'%" + text + "%')";
            }
            else
            {

                strWhere += " and (employees.EmployeeNO like N'%" + text + "%' or employees.Name like N'%" + text + "%' or employees.AltName like N'%" + text + "%')";
            }

            if (type == 1 || type == 2)
            {  ////isJO = true
                var result = objService.GetEmployeeAssignPage(empID, cityID, pageNumber, strWhere, sortExpression, sortDirection);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objService.GetEmployeePage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, pageNumber, strWhere, sortExpression, sortDirection);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="cityID">City ID</param>
        /// <param name="areaID">Area ID</param>
        /// <param name="zoneID">Zone ID</param>
        /// <param name="text">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuildingDetailsAutoDDL)]
        public JsonResult GetBuildingDetailsAutoDDL(int cityID, int areaID, int zoneID, string text = "")
        {
            IList<L5> txtL5Description = new List<L5>();
            try
            {
                string sortExpression = "L5No";
                string sortDirection = string.Empty;

                int pageNumber = 0;

                BuildingService objService = new BuildingService();

                txtL5Description = objService.GetBuildingDetails(cityID, areaID, zoneID, pageNumber, sortExpression, sortDirection, text);

                return Json(txtL5Description, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="strWhere">Where Condition</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationPageAutoDDL)]
        public JsonResult GetLocationPageAutoDDL(string strWhere, string text = "")
        {
            IList<Location> lstBuildings = new List<Location>();
            try
            {
                string sortExpression = "LocationNo";
                string sortDirection = string.Empty;

                int empID = ProjectSession.EmployeeID;
                int pageNumber = 0;

                LocationService objService = new LocationService();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhere += " and (lower(location.LocationNo) like N'%" + text + "%' or lower(location.LocationDescription) like N'%" + text + "%' or lower(location.LocationAltDescription) like N'%" + text + "%')";
                }
                else
                {
                    strWhere += " and (location.LocationNo like N'%" + text + "%' or location.LocationDescription like N'%" + text + "%' or location.LocationAltDescription like N'%" + text + "%')";
                }


                lstBuildings = objService.GetLocationPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection);

                return Json(lstBuildings, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationForJobRequestAutoDDL)]
        public ActionResult GetAssetsByLocationForJobRequestAutoDDL(int locationID, string text = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "AssetNumber";
            sortDirection = "Ascending";

            int pageNumber = 0;

            string strWhere = " and l.L2ID = " + locationID;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(ass.AssetNumber) like N'%" + text + "%' or lower(ass.AssetDescription) like N'%" + text + "%' or lower(ass.AssetAltDescription) like N'%" + text + "%')";
            }
            else
            {
                strWhere += " and (ass.AssetNumber like N'%" + text + "%' or ass.AssetDescription like N'%" + text + "%' or ass.AssetAltDescription like N'%" + text + "%')";
            }


            AssetService obj = new AssetService();

            var result = obj.GetAssetPageWithAllRelatedData(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection);

            foreach (Asset objAsset in result)
            {
                if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now)
                {
                    HomeController obj1 = new HomeController();
                    if (objAsset.Warranty_ContractExpiry == null || objAsset.Warranty_ContractExpiry > DateTime.Now.Date)
                    {
                        if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpiredArabic.GetHashCode()).Data.ToString();
                        }
                        else
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.NotExpired.GetHashCode()).Data.ToString();
                        }
                    }
                    else
                    {
                        if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.ExpiredArabic.GetHashCode()).Data.ToString();
                        }
                        else
                        {
                            objAsset.AssetWarrentyStatus = obj1.GetEnumDescription("WarrentyStatus", SystemEnum.WarrentyStatus.Expired.GetHashCode()).Data.ToString();
                        }
                    }
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the All suppliers list.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllSupplierListAutoDDL)]
        public ActionResult GetAllSupplierListAutoDDL(string text = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            sortExpression = "SupplierID";
            sortDirection = "Descending";

            int pageNumber = 0;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere = " and (lower(SupplierNo) like N'%" + text + "%' or lower(SupplierName) like N'%" + text + "%' or lower(AltSupplierName) like N'%" + text + "%')";
            }
            else
            {
                strWhere = " and (SupplierNo like N'%" + text + "%' or SupplierName like N'%" + text + "%' or AltSupplierName like N'%" + text + "%')";
            }

            Supplier objSupplier = new Supplier();

            SupplierService obj = new SupplierService();

            var result = obj.GetAllSuppliers(strWhere, pageNumber, sortExpression, sortDirection);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the list of all maintenance groups having employees in technicians category
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceGroupHavingTechniciansAutoDDL)]
        public ActionResult GetMaintenanceGroupHavingTechniciansAutoDDL(string text = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "GroupCode";
            sortDirection = "Ascending";

            int pageNumber = 0;

            Assigntoworkorder objWorkRequest = new Assigntoworkorder();
            MaintenanceGroupService objService = new MaintenanceGroupService();

            var result = objService.GetMaintGroupsHavingTechnicians(pageNumber, sortExpression, sortDirection, text);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the list of all job Plans.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderSafetyInstrListAutoDDL)]
        public ActionResult GetWorkOrderSafetyInstrListAutoDDL(string text = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            sortExpression = "SafetyNo";
            sortDirection = "Ascending";

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause = " and (lower(SafetyNo) like N'%" + text + "%' or lower(SafetyName) like N'%" + text + "%' or lower(AltSafetyName) like N'%" + text + "%')";
            }
            else
            {
                whereClause = " and (SafetyNo like N'%" + text + "%' or SafetyName like N'%" + text + "%' or AltSafetyName like N'%" + text + "%')";
            }
            int pageNumber = 0;

            JobOrderService objService = new JobOrderService();

            TblSafetyinstruction objSafetyInstr = new TblSafetyinstruction();

            if (ProjectSession.IsCentral)
            {
                SearchFieldService obj = new SearchFieldService();
                var result = obj.AdvanceSearch(objSafetyInstr, SystemEnum.Pages.SafetyInstruction.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objService.GetSafetyInstrsByEmployee(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the suppliers list.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetSuppliersListAutoDDL)]
        public ActionResult GetSuppliersListAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;
            sortExpression = "SupplierNo";
            sortDirection = "Ascending";

            int pageNumber = 0;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause = " and (lower(SupplierNo) like N'%" + text + "%' or lower(SupplierName) like N'%" + text + "%' or lower(AltSupplierName) like N'%" + text + "%')";
            }
            else
            {
                whereClause = " and (SupplierNo like N'%" + text + "%' or SupplierName like N'%" + text + "%' or AltSupplierName like N'%" + text + "%')";
            }

            Supplier areaSearch = new Supplier();
            areaSearch.SupplierNo = string.Empty;
            areaSearch.SupplierName = string.Empty;

            SearchFieldService obj = new SearchFieldService();
            var result = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.Supplier.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the pm checklist.
        /// </summary>
        /// <param name="strWhere">The string where.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistAutoDDL)]
        public JsonResult GetPMChecklistAutoDDL(string strWhere, string text)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                int empID = ProjectSession.EmployeeID;
                int pageNumber = 0;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhere += " and (lower(pmchecklist.ChecklistNo) like N'%" + text + "%' or lower(pmchecklist.CheckListName) like N'%" + text + "%' or  lower(pmchecklist.AltCheckListName) like N'%" + text + "%')";
                }
                else
                {
                    strWhere += " and (pmchecklist.ChecklistNo like N'%" + text + "%' or pmchecklist.CheckListName like N'%" + text + "%' or  pmchecklist.AltCheckListName like N'%" + text + "%')";
                }
                PMCheckListService objService = new PMCheckListService();

                var result = objService.GetPMChecklistPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }


        [ActionName(Actions.GetCleaningChecklistAutoDDL)]
        public JsonResult GetCleaningChecklistAutoDDL(string strWhere, string text)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                int empID = ProjectSession.EmployeeID;
                int pageNumber = 0;

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
                {
                    text = text.ToLower();
                    strWhere += " and (lower(pmchecklist.ChecklistNo) like N'%" + text + "%' or lower(pmchecklist.CheckListName) like N'%" + text + "%' or  lower(pmchecklist.AltCheckListName) like N'%" + text + "%')";
                }
                else
                {
                    strWhere += " and (pmchecklist.ChecklistNo like N'%" + text + "%' or pmchecklist.CheckListName like N'%" + text + "%' or  pmchecklist.AltCheckListName like N'%" + text + "%')";
                }
                PMCheckListForCleaningService objService = new PMCheckListForCleaningService();

                var result = objService.GetCleaningChecklistPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the safety instruction.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetSafetyInstructionAutoDDL)]
        public ActionResult GetSafetyInstructionAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;
            sortExpression = "SafetyNo";
            sortDirection = "Ascending";

            int pageNumber = 0;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause += " and (lower(SafetyNo) like N'%" + text + "%' or lower(SafetyName) like N'%" + text + "%' or  lower(AltSafetyName) like N'%" + text + "%')";
            }
            else
            {
                whereClause += " and (SafetyNo like N'%" + text + "%' or SafetyName like N'%" + text + "%' or  AltSafetyName like N'%" + text + "%')";
            }

            TblSafetyinstruction objSafetyinstruction = new TblSafetyinstruction();

            SearchFieldService obj = new SearchFieldService();
            var result = obj.AdvanceSearch(objSafetyinstruction, SystemEnum.Pages.SafetyInstruction.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the stock code item.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetStockCodeItemAutoDDL)]
        public ActionResult GetStockCodeItemAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "StockNo";
            sortDirection = "Ascending";

            int pageNumber = 0;

            StockCode objStockCode = new StockCode();

            BoMService obj = new BoMService();
            var result = obj.GetAllItemDetail(pageNumber, sortExpression, sortDirection, null, text);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the maintenance group.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceGroupAutoDDL)]
        public ActionResult GetMaintenanceGroupAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;
            sortExpression = "GroupCode";
            sortDirection = "Ascending";
            int pageNumber = 0;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause += " and (lower(GroupCode) like N'%" + text + "%' or lower(GroupDesc) like N'%" + text + "%' or lower(GroupAltDesc) like N'%" + text + "%')";
            }
            else
            {
                whereClause += " and (GroupCode like N'%" + text + "%' or GroupDesc like N'%" + text + "%' or GroupAltDesc like N'%" + text + "%')";
            }

            MaintGroup maintGroupSearch = new MaintGroup();

            SearchFieldService obj = new SearchFieldService();
            var result = obj.AdvanceSearch(maintGroupSearch, SystemEnum.Pages.MaintenanceGroup.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the bo m list.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetBoMListAutoDDL)]
        public ActionResult GetBoMListAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            sortExpression = "PartsListNo";
            sortDirection = "Ascending";

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause += " and (lower(PartsListNo) like N'%" + text + "%' or lower(PartsListDescription) like N'%" + text + "%' or lower(PartsListAltDescription) like N'%" + text + "%')";
            }
            else
            {
                whereClause += " and (PartsListNo like N'%" + text + "%' or PartsListDescription like N'%" + text + "%' or PartsListAltDescription like N'%" + text + "%')";
            }

            int pageNumber = 0;

            PartsList objPartsList = new PartsList();
            SearchFieldService obj = new SearchFieldService();
            var result = obj.AdvanceSearch(objPartsList, SystemEnum.Pages.BoMList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationAutoDDL)]
        public ActionResult GetAssetsByLocationAutoDDL(int locationID, string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "AssetNumber";
            sortDirection = "Ascending";

            int pageNumber = 0;

            string strWhere = " and ass.locationId = " + locationID + " and ass.AssetId not in (select ChildId from assetRelationship) ";

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(AssetNumber) like N'%" + text + "%' or lower(AssetDescription) like N'%" + text + "%' or lower(AssetAltDescription) like N'%" + text + "%')";
            }
            else 
            {
                strWhere += " and (AssetNumber like N'%" + text + "%' or AssetDescription like N'%" + text + "%' or AssetAltDescription like N'%" + text + "%')";
            }
            
            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            var result = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection);

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the assets by location and asset identifier.
        /// </summary>
        /// <param name="locationID">The location identifier.</param>
        /// <param name="assetID">The asset identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByLocationAndAssetIDAutoDDL)]
        public ActionResult GetAssetsByLocationAndAssetIDAutoDDL(int? locationID, int? assetID, string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            sortExpression = "AssetNumber";
            sortDirection = "Ascending";

            int pageNumber = 0;

            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            string strWhere = " and ass.locationId = " + locationID + " and ass.assetId not in (select ChildId from assetRelationShip where assetId =  " + assetID + " ) and ass.assetId != " + assetID;

             if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(AssetNumber) like N'%" + text + "%' or lower(AssetDescription) like N'%" + text + "%' or lower(AssetAltDescription) like N'%" + text + "%')";
            }
             else
            {
                strWhere += " and (AssetNumber like N'%" + text + "%' or AssetDescription like N'%" + text + "%' or AssetAltDescription like N'%" + text + "%')";
            }

            var result = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection);

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the assets by l2 identifier.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAssetsByL2IDAutoDDL)]
        public ActionResult GetAssetsByL2IDAutoDDL(int? cityID, string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "AssetNumber";
            sortDirection = "Ascending";

            int pageNumber = 0;

            PartsList objPartsList = new PartsList();
            AssetService obj = new AssetService();

            string strWhere = "and l.L2ID = " + cityID;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lowet(AssetNumber) like N'%" + text + "%' or lower(AssetDescription) like N'%" + text + "%' or lower(AssetAltDescription) like N'%" + text + "%')";
            }
            else 
            {
                strWhere += " and (AssetNumber like N'%" + text + "%' or AssetDescription like N'%" + text + "%' or AssetAltDescription like N'%" + text + "%')";
            }
            var result = obj.GetAssetPage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, strWhere, pageNumber, sortExpression, sortDirection);

            PartsListDetail objPartsListDetail = new PartsListDetail();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// get pm meter master list
        /// </summary>
        /// <param name="cityID">get list by cityID</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMMeterMasterListByCityAutoDDL)]
        public ActionResult GetPMMeterMasterListByCityAutoDDL(int cityID, string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            sortExpression = "MeterNo";
            sortDirection = "Ascending";

            strWhere = " and PMMeterMaster.L2ID = " + cityID + " and PMMeterMaster.Active = 1 ";

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(MeterNo) like N'%" + text + "%' or lower(MeterDescription) like N'%" + text + "%' or lower(AltMeterDescription) like N'%" + text + "%')";
            }
            else 
            {
                strWhere += " and (MeterNo like N'%" + text + "%' or MeterDescription like N'%" + text + "%' or AltMeterDescription like N'%" + text + "%')";
            }
            
            int pageNumber = 0;

            PMMeterMaster objPMMeterMaster = new PMMeterMaster();

            SearchFieldService obj = new SearchFieldService();
            var result = obj.AdvanceSearch(objPMMeterMaster, SystemEnum.Pages.PMMeterMasterList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, strWhere);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the Items List
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="subStoreID">The sub store identifier.</param>
        /// <param name="text">auto search word</param>
        /// <param name="excludeStockId">The exclude stock identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStoreItemsListAutoDDL)]
        public ActionResult GetStoreItemsListAutoDDL(int cityID, int subStoreID, string text, int excludeStockId = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "StockID";
            sortDirection = "Descending";

            int pageNumber = 0;

            SearchFieldService obj = new SearchFieldService();
            string whereClause = string.Empty;

            if (cityID > 0 && subStoreID == 0)
            {
                whereClause = " and StockId in (Select Stock_Id From stockcode_levels l inner join substore s on l.sub_store_id = s.SubStoreID where s.L2Id=" + cityID.ToString() + ") ";
            }

            if (subStoreID > 0)
            {
                whereClause = " and StockId in (Select Stock_Id From stockcode_levels l where  l.sub_store_id = " + subStoreID.ToString() + ") ";
            }

            if (cityID == 0 && subStoreID == 0)
            {
                whereClause = string.Empty;
            }

            if (excludeStockId > 0)
            {
                whereClause += " and StockID Not in (" + excludeStockId.ToString() + ") ";
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                whereClause += " and (lower(StockNo) like N'%" + text + "%' or lower(StockDescription) like N'%" + text + "%' or lower(AltStockDescription) like N'%" + text + "%')";
            }
            else 
            {
                whereClause += " and (StockNo like N'%" + text + "%' or StockDescription like N'%" + text + "%' or AltStockDescription like N'%" + text + "%')";
            }

            StockCode objStockCode = new StockCode();
            var result = obj.AdvanceSearch(objStockCode, SystemEnum.Pages.StockCodesList.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the employees.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesAutoDDL)]
        public ActionResult GetEmployeesAutoDDL(string text)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            int pageNumber = 0;

            EmployeeService objService = new EmployeeService();

            var result = objService.GetEmployees(pageNumber, sortExpression, sortDirection, text: text);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the account code for pop up.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetAccountCodeForPopUpAutoDDL)]
        public ActionResult GetAccountCodeForPopUpAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            sortExpression = "AccountCode";
            sortDirection = "Ascending";

            int pageNumber = 0;

            AccountCodeService objService = new AccountCodeService();

            var result = objService.GetAccountCodes(pageNumber, sortExpression, sortDirection, text: text);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the currency for pop up.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetCurrencyForPopUpAutoDDL)]
        public ActionResult GetCurrencyForPopUpAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "CurrencyCode";
            sortDirection = "Ascending";

            int pageNumber = 0;

            CurrencyService objService = new CurrencyService();

            var result = objService.GetCurrencys(pageNumber, sortExpression, sortDirection, text: text);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the assets by location.
        /// </summary>
        /// <param name="cityID">The location identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetApprovedProjectListPageAutoDDL)]
        public ActionResult GetApprovedProjectListPageAutoDDL(string cityID, string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string where = string.Empty;

            sortExpression = "ProjectNumber";
            sortDirection = "Ascending";

            int pageNumber = 0;

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                where += " and (lower(ProjectNumber) like N'%" + text + "%' or lower(ProjectName) like N'%" + text + "%' or lower(AltProjectName) like N'%" + text + "%')";
            }
            else 
            {
                where += " and (ProjectNumber like N'%" + text + "%' or ProjectName like N'%" + text + "%' or AltProjectName like N'%" + text + "%')";
            }
            
            JobRequestService obj = new JobRequestService();
            var result = obj.GetApprovedProjectListPage(cityID, pageNumber, where, sortExpression, sortDirection);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the employee maintenance group.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <param name="maintGroupID">The maintenance group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeMaintenanceGroupAutoDDL)]
        public ActionResult GetEmployeeMaintenanceGroupAutoDDL(string text, int? maintGroupID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            sortExpression = "EmployeeID";

            int pageNumber = 0;

            if (maintGroupID == 0)
            {
                maintGroupID = null;
            }

            CustomMaintGroup objCustomMaintGroup = new CustomMaintGroup();

            var result = objCustomMaintGroup.GetMaintGroupEmployeeInfoSearch(maintGroupID, pageNumber, sortExpression, sortDirection, text: text).Distinct();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the list of all job Plans.
        /// </summary>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetWorkOrderJOPlanListAutoDDL)]
        public ActionResult GetWorkOrderJOPlanListAutoDDL(string text)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string where = string.Empty;

            sortExpression = "JobPlanNo";
            sortDirection = "Ascending";

            int pageNumber = 0;

            JobOrderService objService = new JobOrderService();

            TblJobPlan objJobPlan = new TblJobPlan();

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                where += " and (lower(JobPlanNo) like N'%" + text + "%' or lower(JobPlanName) like N'%" + text + "%' or lower(AltJobPlanName) like N'%" + text + "%')";
            }
            else 
            {
                where += " and (JobPlanNo like N'%" + text + "%' or JobPlanName like N'%" + text + "%' or AltJobPlanName like N'%" + text + "%')";
            }
            if (ProjectSession.IsCentral)
            {
                SearchFieldService obj = new SearchFieldService();
                var result = obj.AdvanceSearch(objJobPlan, SystemEnum.Pages.JobPlan.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, where);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = objService.GetJobPlansByEmployee(ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, text);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the employees by sub department identifier.
        /// </summary>
        /// <param name="subDeptID">The sub department identifier.</param>
        /// <param name="text">auto search word</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeesBySubDeptIDAutoDDL)]
        public ActionResult GetEmployeesBySubDeptIDAutoDDL(int subDeptID, string text)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            int pageNumber = 0;

            StockCodeService objService = new StockCodeService();

            var result = objService.GetEmployeesBySubDeptID(subDeptID, pageNumber, sortExpression, sortDirection, text: text);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Gets Store Items List.
        /// </summary>
        /// <param name="text">The user input text to search.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobOrderAutoDDL)]
        public ActionResult GetJobOrderAutoDDL(string text, int? cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            sortExpression = "workorders.workorderno";
            sortDirection = "Descending";

            JobOrderService objService = new JobOrderService();            
            int pageNumber = 0;
            string strWhere = string.Empty;

            if (cityID > 0)
            {
                strWhere = " and workorders.L2ID =  " + cityID + " and workorders.workstatusid <> 2 and workorders.workstatusid <> 3 ";
            }

            if (!ProjectSession.IsCentral)
            {
                strWhere += " and workorders.L2ID in (select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
                strWhere += " and workorders.maintsubdeptID in (select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + " union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID + " ) ";
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && !string.IsNullOrEmpty(text))
            {
                text = text.ToLower();
                strWhere += " and (lower(workorders.workorderno) like N'%" + text + "%')";
            }
            else
            {
                strWhere += " and (workorders.workorderno like N'%" + text + "%')";
            }

            var result = objService.GetJOPageWithOutLoad(strWhere, pageNumber, sortExpression, sortDirection);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}