﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Buildings this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Building)]
        public ActionResult Building()
        {
            if (ProjectSession.PermissionAccess.Locations_Buildings_Allowaccess && ProjectSession.PermissionAccess.Locations__ShowHideModule)
            {
                return View(Views.Building);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the building.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuilding)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBuilding([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L5No";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            L5 buildingSearch = new L5();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L5.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";

                if (ProjectSession.IsAreaFieldsVisible)
                {
                    whereClause += " and (L5.L3ID in ( " + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (L5.L3ID IS NULL))";
                }
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(buildingSearch, SystemEnum.Pages.Building.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the building by identifier.
        /// </summary>
        /// <param name="buildingId">The building identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuildingByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBuildingByID(int buildingId)
        {
            L5 objL5 = new L5();
            if (buildingId > 0)
            {
                using (ServiceContext buildingService = new ServiceContext())
                {
                    objL5 = buildingService.SelectObject<L5>(buildingId);
                    bool returnValue = buildingService.HasReferencesRecords<L5>(objL5, buildingId);
                    objL5.IsEnableCityArea = returnValue;
                    if (objL5 != null)
                    {
                        L2 objL2 = new L2();
                        using (DapperContext context = new DapperContext())
                        {
                            objL2 = context.SearchAll(objL2).Where(m => m.L2ID == objL5.L2ID).FirstOrDefault();
                            objL5.L1ID = objL2.L1ID;
                        }

                        L1 objL1 = new L1();
                        using (DapperContext context = new DapperContext())
                        {
                            objL1 = context.SelectObject<L1>(Convert.ToInt32(objL2.L1ID));
                            objL5.IsAreaEnableOrgWise = Convert.ToBoolean(objL1.IsAreaEnable);
                        }
                    }
                }
            }

            return Json(objL5, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the zone.
        /// </summary>
        /// <param name="objL5">The object l5.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageBuilding)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageBuilding(L5 objL5)
        {
            string validations = string.Empty;
            L2 objL2 = new L2();
            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = objL5.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(objL5.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (objL5.L5ID > 0)
            {
                if (!ProjectSession.PermissionAccess.Locations_Buildings_ChangeMastersCodes)
                {
                    objL5.L5No = null;
                }

                if (!ProjectSession.PermissionAccess.Locations_Buildings_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }

                objL5.ModifiedBy = ProjectSession.EmployeeID;
                objL5.ModifiedDate = DateTime.Now;
            }
            else
            {
                if (!ProjectSession.PermissionAccess.Locations_Buildings_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                objL5.CreatedBy = ProjectSession.EmployeeID;
                objL5.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext buildingService = new ServiceContext("L5No"))
                {
                    if (buildingService.Save(objL5) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Building_MsgBuildingCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the building.
        /// </summary>
        /// <param name="buildingId">The building identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteBuilding)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteBuilding(int buildingId)
        {
            using (ServiceContext buildingService = new ServiceContext())
            {
                int returnValue = buildingService.Delete<L5>(buildingId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the building by city identifier.
        /// </summary>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetBuildingByL2ID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetBuildingByL2ID(int cityID)
        {
            L5 objL5 = new L5();
            objL5.L2ID = Convert.ToInt32(cityID);

            using (ServiceContext context = new ServiceContext())
            {
                var result = context.Search(objL5, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}