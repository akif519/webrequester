﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Service.UserAuthentication;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// ErrorLog Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Errors the log.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ErrorLog)]
        public ActionResult ErrorLog()
        {
            return View(Views.ErrorLog);
        }

        /// <summary>
        /// Gets the error log list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        [ActionName(Actions.GetErrorLogList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetErrorLogList([DataSourceRequest]DataSourceRequest request, DateTime startDate, DateTime endDate)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ErrorDate";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ErrorLog objErrorLog = new ErrorLog();

            SearchFieldService obj = new SearchFieldService();

            string whereClause = string.Empty;
            whereClause = " and CAST(ErrorDate AS Date) between '" + startDate.ToShortDateString() + "' and '" + endDate.ToShortDateString() + "' ";

            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objErrorLog, SystemEnum.Pages.ErrorLog.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, true, whereClause),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// _s the error log detail.
        /// </summary>
        /// <param name="errorLogId">The error log identifier.</param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._ErrorLogDetail)]
        public PartialViewResult _ErrorLogDetail(int errorLogId)
        {
            ErrorLog objErrorLog = new ErrorLog();
            employees objEmployees = new employees();
            if (errorLogId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objErrorLog = context.SelectObject<ErrorLog>(errorLogId);
                    objEmployees = context.SelectObject<employees>(ConvertTo.Integer(objErrorLog.LoginID));
                    objErrorLog.Name = objEmployees.Name;
                    objErrorLog.AltName = objEmployees.AltName;
                }
            }

            return PartialView(Pages.PartialViews.ErrorLogTrace, objErrorLog);
        }

        /// <summary>
        /// Deletes the error log.
        /// </summary>
        /// <param name="errorLogIds">The error log ids.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName(Actions.DeleteErrorLog)]
        public JsonResult DeleteErrorLog(string errorLogIds)
        {
            UserAuthenticationData obj = new UserAuthenticationData();
            bool flag = obj.DeleteErrorLog(errorLogIds);
            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}