﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Maintenances the sub department.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.MaintenanceSubDepartment)]
        public ActionResult MaintenanceSubDepartment()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceSubDepartment_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.MaintenanceSubDepartment);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the maintenance sub department.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="maintDeptID">The maintenance department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceSubDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceSubDepartment([DataSourceRequest]DataSourceRequest request, int maintDeptID = 0)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MaintSubDeptCode";
                sortDirection = "Ascending";
            }

            if (maintDeptID > 0)
            {
                strWhere = " AND MaintSubDept.MaintDeptID = " + maintDeptID;
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            MaintSubDept maintSubDeptSearch = new MaintSubDept();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(maintSubDeptSearch, SystemEnum.Pages.MaintenanceSubDepartment.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: strWhere, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the maintenance sub department.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="maintDeptID">The maintenance department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceSubDepartmentWithCheckBox)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceSubDepartmentWithCheckBox([DataSourceRequest]DataSourceRequest request, int empID, int maintDeptID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string strWhere = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "MaintSubDeptCode";
                sortDirection = "Ascending";
            }

            if (maintDeptID > 0)
            {
                strWhere = " AND MaintSubDept.MaintDeptID = " + maintDeptID;
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            MaintSubDept maintSubDeptSearch = new MaintSubDept();

            EmployeeService obj = new EmployeeService();
            var result = new DataSourceResult()
            {
                Data = obj.GetSubDeptsWithCheckBox(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, maintDeptID, pageNumber, sortExpression, sortDirection),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the maintenance sub department by identifier.
        /// </summary>
        /// <param name="maintSubDepartmentId">The maintenance sub department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceSubDepartmentByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMaintenanceSubDepartmentByID(int maintSubDepartmentId)
        {
            MaintSubDept maintenanceSubDepartment = new MaintSubDept();
            MaintenanceDepartment objMaintDeaprtment = new MaintenanceDepartment();
            if (maintSubDepartmentId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    maintenanceSubDepartment = context.SelectObject<MaintSubDept>(maintSubDepartmentId);
                    objMaintDeaprtment = context.SelectObject<MaintenanceDepartment>(Convert.ToInt32(maintenanceSubDepartment.MaintDeptID));
                    maintenanceSubDepartment.MaintDivisionID = objMaintDeaprtment.MaintDivisionID;
                    bool returnValue = context.HasReferencesRecords<MaintSubDept>(maintenanceSubDepartment, maintSubDepartmentId);
                    maintenanceSubDepartment.IsEnableMaintDivDept = returnValue;
                }
            }

            return Json(maintenanceSubDepartment, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the maintenance sub department.
        /// </summary>
        /// <param name="maintSubDept">The maintenance sub department.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageMaintenanceSubDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageMaintenanceSubDepartment(MaintSubDept maintSubDept)
        {
            string validations = string.Empty;

            if (maintSubDept.MaintSubDeptID > 0)
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceSubDepartment_ChangeMastersCodes)
                {
                    maintSubDept.MaintSubDeptCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceSubDepartment_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }

                maintSubDept.ModifiedBy = ProjectSession.EmployeeID.ToString();
                maintSubDept.ModifiedDate = DateTime.Now;
            }
            else
            {
                if (!ProjectSession.PermissionAccess.SetupConfiguration_MaintenanceSubDepartment_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }

                maintSubDept.CreatedBy = ProjectSession.EmployeeID.ToString();
                maintSubDept.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("MaintSubDeptCode"))
                {
                    if (context.Save(maintSubDept) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.MaintenanceSubDept_MsgMaintSubDeptCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the maintenance sub department.
        /// </summary>
        /// <param name="maintSubDepartmentId">The maintenance sub department identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteMaintenanceSubDepartment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteMaintenanceSubDepartment(int maintSubDepartmentId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<MaintSubDept>(maintSubDepartmentId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the maintenance sub departments.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetMaintenanceSubDepartments)]
        public ActionResult GetMaintenanceSubDepartments()
        {
            MaintSubDept objMaintSubDept = new MaintSubDept();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objMaintSubDept);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}