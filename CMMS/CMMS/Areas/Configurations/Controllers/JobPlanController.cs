﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Job Plan Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Jobs the plan.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.JobPlan)]
        public ActionResult JobPlan()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_JobPlans_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.JobPlan);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the job plan.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobPlan)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobPlan([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "JobPlanNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            TblJobPlan objJobPlan = new TblJobPlan();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and tblJobPlan.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objJobPlan, SystemEnum.Pages.JobPlan.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the job plan by identifier.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobPlanByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobPlanByID(int jobPlanId)
        {
            TblJobPlan objJobPlan = new TblJobPlan();
            if (jobPlanId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobPlan = context.SelectObject<TblJobPlan>(jobPlanId);
                }
            }

            return Json(objJobPlan, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the job plan.
        /// </summary>
        /// <param name="objJobPlan">The object job plan.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobPlan)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobPlan(TblJobPlan objJobPlan)
        {
            string validations = string.Empty;

            L2 objL2 = new L2();
            using (DapperContext context = new DapperContext())
            {
                objL2 = objJobPlan.L2Id > 0 ? context.SelectObject<L2>(Convert.ToInt32(objJobPlan.L2Id)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (objJobPlan.JobPlanID > 0)
            {
                objJobPlan.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objJobPlan.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobPlans_ChangeMastersCodes)
                {
                    objJobPlan.JobPlanNo = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobPlans_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objJobPlan.CreatedBy = ProjectSession.EmployeeID.ToString();
                objJobPlan.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_JobPlans_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("JobPlanNo"))
                {
                    int jobPlanID = context.Save(objJobPlan);
                    if (jobPlanID > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.JobPlan_MsgJobPlanNoAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the job plan.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobPlan)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobPlan(int jobPlanId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<TblJobPlan>(jobPlanId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// _s the edit job plan.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <returns></returns>
        [ActionName(Actions._EditJobPlan)]
        [OutputCache(NoStore = true, Duration = 0)]
        public PartialViewResult _EditJobPlan(int jobPlanId)
        {
            TblJobPlan objJobPlan = new TblJobPlan();
            if (jobPlanId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objJobPlan = context.SelectObject<TblJobPlan>(jobPlanId);
                }
            }

            return PartialView(Pages.PartialViews.EditJobPlan, objJobPlan);
        }

        /// <summary>
        /// Gets the job plan item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="jobPlanID">The job plan identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetJobPlanItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetJobPlanItem([DataSourceRequest]DataSourceRequest request, string jobPlanID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SeqID";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            TblJobPlanItem objJobPlanItem = new TblJobPlanItem();

            objJobPlanItem.JobPlanID = Convert.ToInt32(jobPlanID);

            ////DapperContext obj = new DapperContext();
            ////var result = new DataSourceResult()
            ////{
            ////    Data = obj.Search(objJobPlanItem, pageNumber, sortExpression, sortDirection),
            ////    Total = obj.PagingInformation.TotalRecords
            ////};

            CustomJobPlan obj = new CustomJobPlan();
            var result = new DataSourceResult()
            {
                Data = obj.GetJobPlanItemByPlanID(Convert.ToInt32(jobPlanID), pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Manages the job plan item.
        /// </summary>
        /// <param name="objTblJobPlanItem">The object table job plan item.</param>
        /// <returns></returns>
        ////[ActionName(Actions.ManageJobPlanItem)]
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////[ValidateInput(false)]
        ////public JsonResult ManageJobPlanItem(TblJobPlanItem objTblJobPlanItem)
        ////{
        ////    string validations = string.Empty;

        ////    if (objTblJobPlanItem.HdnSeqID > 0)
        ////    {
        ////        objTblJobPlanItem.ModifiedBy = ProjectSession.EmployeeID.ToString();
        ////        objTblJobPlanItem.ModifiedDate = DateTime.Now;
        ////    }
        ////    else
        ////    {
        ////        objTblJobPlanItem.CreatedBy = ProjectSession.EmployeeID.ToString();
        ////        objTblJobPlanItem.CreatedDate = DateTime.Now;
        ////    }

        ////    if (ModelState.IsValid)
        ////    {
        ////        TblJobPlanItem oldObjTblJobPlanItemResult = new TblJobPlanItem();
        ////        var oldResult = new DapperContext().SearchAll(oldObjTblJobPlanItemResult).Where(m => m.JobPlanID == objTblJobPlanItem.JobPlanID).ToList();
        ////        if (objTblJobPlanItem.HdnSeqID == 0)
        ////        {
        ////            if (oldResult.Count > 0)
        ////            {
        ////                int objcount = Convert.ToInt32(oldResult.Max(c => c.SeqID));
        ////                objTblJobPlanItem.SeqID = objcount + 1;
        ////            }
        ////            else
        ////            {
        ////                objTblJobPlanItem.SeqID = 1;
        ////            }
        ////        }
        ////        else
        ////        {
        ////            objTblJobPlanItem.SeqID = objTblJobPlanItem.SeqID;
        ////        }

        ////        CustomJobPlan context = new CustomJobPlan();

        ////        if (context.InsertUpdateJobPlanItem(objTblJobPlanItem))
        ////        {
        ////            return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
        ////        }
        ////        else
        ////        {
        ////            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
        ////        }
        ////    }
        ////    else
        ////    {
        ////        foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
        ////        {
        ////            validations = validations + modelError.ErrorMessage + " ,";
        ////        }

        ////        if (string.IsNullOrEmpty(validations))
        ////        {
        ////            return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
        ////        }
        ////        else
        ////        {
        ////            return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
        ////        }
        ////    }
        ////}

        /// <summary>
        /// Manages the job plan item.
        /// </summary>
        /// <param name="objOldJobtask">The object old job task.</param>
        /// <param name="objNewJobtask">The object new job task.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageJobPlanItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageJobPlanItem(TblJobPlanItem objOldJobtask, TblJobPlanItem objNewJobtask, string kendogriddata)
        {
            List<TblJobPlanItem> lstJobPlanItems = new List<TblJobPlanItem>();
            lstJobPlanItems = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TblJobPlanItem>>(kendogriddata);
            
            CustomJobPlan objService = new CustomJobPlan();

            bool isSeqUpdated = false;

            if (objOldJobtask.SeqID > -1 && (objNewJobtask.SeqID != objOldJobtask.SeqID))
            {
                ////Update Sequence
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqID == objOldJobtask.SeqID && x.Description == objOldJobtask.Description);
                lstJobPlanItems.Remove(objOldJobtask);
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                isSeqUpdated = true;
            }
            else if (objOldJobtask.SeqID > -1 && (objNewJobtask.SeqID == objOldJobtask.SeqID))
            {
                objNewJobtask.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.ModifiedDate = DateTime.Now;
                objOldJobtask = lstJobPlanItems.Find(x => x.SeqID == objOldJobtask.SeqID && x.Description == objOldJobtask.Description);
                lstJobPlanItems.Remove(objOldJobtask);
            }
            else if (objOldJobtask.SeqID == -1)
            {
                objNewJobtask.CreatedBy = ProjectSession.EmployeeID.ToString();
                objNewJobtask.CreatedDate = DateTime.Now;
            }

            int maxSeqId = 0;

            if (lstJobPlanItems.Count > 0)
            {
                maxSeqId = lstJobPlanItems.Max(x => x.SeqID);
            }

            if (objNewJobtask.SeqID > maxSeqId + 1)
            {
                objNewJobtask.SeqID = maxSeqId + 1;
            }

            if (isSeqUpdated)
            {
                if (objOldJobtask.SeqID < objNewJobtask.SeqID)
                {
                    lstJobPlanItems.Where(x => x.SeqID > objOldJobtask.SeqID && x.SeqID <= objNewJobtask.SeqID).ToList().ForEach(p =>
                    {
                        p.SeqID = p.SeqID - 1;
                    });
                }
                else if (objOldJobtask.SeqID > objNewJobtask.SeqID)
                {
                    lstJobPlanItems.Where(x => x.SeqID >= objNewJobtask.SeqID && x.SeqID < objOldJobtask.SeqID).ToList().ForEach(p =>
                    {
                        p.SeqID = p.SeqID + 1;
                    });
                }
            }
            else
            {
                if (objOldJobtask.SeqID == -1 && lstJobPlanItems.Any(x => x.SeqID == objNewJobtask.SeqID))
                {
                    ////Inserting New Item in the list between any two Items
                    lstJobPlanItems.Where(x => x.SeqID >= objNewJobtask.SeqID).ToList().ForEach(p =>
                    {
                        p.SeqID = p.SeqID + 1;
                    });
                }
            }

            lstJobPlanItems.Add(objNewJobtask);

            bool result = objService.SaveJobPlanItemInsert(ConvertTo.Integer(objNewJobtask.JobPlanID), lstJobPlanItems);
            if (result)
            {
                return Json(new object[] { SystemEnum.MessageType.success.GetHashCode(), SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.GetHashCode(), SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the job plan item.
        /// </summary>
        /// <param name="jobPlanId">The job plan identifier.</param>
        /// <param name="seqId">The Sequence identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteJobPlanItem)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteJobPlanItem(int jobPlanId, int seqId)
        {
            CustomJobPlan context = new CustomJobPlan();

            if (context.DeleteJobPlanItem(jobPlanId, seqId))
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully });
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}