﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Cleaning Setup Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Cleaning Group"

        /// <summary>
        /// Cleanings the group.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningGroup)]
        public ActionResult CleaningGroup()
        { 
            if (ProjectSession.PermissionAccess.SetupConfiguration_CleaningGroup_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.CleaningGroup);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the cleaning group.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCleaningGroup([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "GroupDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Cleaninggroup objCleaningGroup = new Cleaninggroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCleaningGroup, SystemEnum.Pages.CleaningGroup.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the cleaning group by identifier.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningGroupByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCleaningGroupByID(int groupId)
        {
            Cleaninggroup objCleaningGroup = new Cleaninggroup();
            IList<Cleaninggroup> lstCleaningGroup = new List<Cleaninggroup>();

            if (groupId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objCleaningGroup = context.SelectObject<Cleaninggroup>(groupId);
                }

                return Json(objCleaningGroup, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    objCleaningGroup = new Cleaninggroup();
                    lstCleaningGroup = context.SearchAll<Cleaninggroup>(objCleaningGroup);
                }

                return Json(lstCleaningGroup, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the cleaning group.
        /// </summary>
        /// <param name="objCleaningGroup">The object cleaning group.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCleaningGroup(Cleaninggroup objCleaningGroup)
        {
            string validations = string.Empty;

            if (objCleaningGroup.CleaningGroupId > 0)
            {
                objCleaningGroup.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaningGroup.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CleaningGroup_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCleaningGroup.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaningGroup.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CleaningGroup_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("GroupDesc"))
                {
                    if (context.Save(objCleaningGroup) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningSetup_MsgCleaningGroupAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the cleaning group.
        /// </summary>
        /// <param name="groupId">The group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCleaningGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCleaningGroup(int groupId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Cleaninggroup>(groupId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Cleaning Classification"

        /// <summary>
        /// Cleanings the classification.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningClassification)]
        public ActionResult CleaningClassification()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_CleaningClassification_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.CleaningClassification);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the cleaning classification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningClassification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCleaningClassification([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Classification";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Cleaningclassification objCleaningClassification = new Cleaningclassification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCleaningClassification, SystemEnum.Pages.CleaningClassification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the cleaning classification by identifier.
        /// </summary>
        /// <param name="classificationId">The classification identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningClassificationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCleaningClassificationByID(int classificationId)
        {
            Cleaningclassification objCleaningClassification = new Cleaningclassification();
            if (classificationId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objCleaningClassification = context.SelectObject<Cleaningclassification>(classificationId);
                }
            }

            return Json(objCleaningClassification, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the cleaning classification.
        /// </summary>
        /// <param name="objCleaningClassification">The object cleaning classification.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningClassification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCleaningClassification(Cleaningclassification objCleaningClassification)
        {
            string validations = string.Empty;

            if (objCleaningClassification.CleaningClassificationID > 0)
            {
                objCleaningClassification.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaningClassification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CleaningClassification_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCleaningClassification.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaningClassification.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CleaningClassification_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("Classification"))
                {
                    if (context.Save(objCleaningClassification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CleaningSetup_MsgCleaningClassificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the cleaning classification.
        /// </summary>
        /// <param name="classificationId">The classification identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCleaningClassification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCleaningClassification(int classificationId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Cleaningclassification>(classificationId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all criticalities.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllCleaningClassification)]
        public ActionResult GetAllCleaningClassification()
        {
            Cleaningclassification objcleaningcs = new Cleaningclassification();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<Cleaningclassification>(objcleaningcs);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Cleaning Element"

        /// <summary>
        /// Gets the cleaning elements.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningElements)]
        public ActionResult GetCleaningElements()
        {
            Cleaningelement objCleaningElement = new Cleaningelement();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objCleaningElement);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Cleaning Weightage"

        /// <summary>
        /// Cleanings the weightage.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CleaningWeightage)]
        public ActionResult CleaningWeightage()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_CleaningWeightage_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];
                CleaningWeightage objCleaningWeightage = new CleaningWeightage();
                if (Convert.ToInt32(TempData["Id"]) != 0)
                {
                    objCleaningWeightage = new DapperContext().SelectObject<CleaningWeightage>(Convert.ToInt32(TempData["Id"]));
                }

                return View(Views.CleaningWeightage, objCleaningWeightage);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the cleaning weightage by l2 identifier.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCleaningWeightageByL2ID)]
        [ValidateAntiForgeryToken]
        public ActionResult GetCleaningWeightageByL2ID(int siteId)
        {
            CleaningWeightage objCleaningWeightage = new CleaningWeightage();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objCleaningWeightage).Where(m => m.L2ID == siteId).FirstOrDefault();
                if (result == null)
                {
                    result = new CleaningWeightage();
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the cleaning weightage.
        /// </summary>
        /// <param name="objCleaningWeightage">The object cleaning weightage.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCleaningWeightage)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageCleaningWeightage(CleaningWeightage objCleaningWeightage)
        {
            L2 objL2 = new L2();
            using (DapperContext context = new DapperContext())
            {
                objL2 = objCleaningWeightage.L2ID > 0 ? context.SelectObject<L2>(Convert.ToInt32(objCleaningWeightage.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (!ProjectSession.PermissionAccess.SetupConfiguration_CleaningWeightage_Editrecords)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
            }

            string validations = string.Empty;

            if (objCleaningWeightage.CleaningWeightageId > 0)
            {
                objCleaningWeightage.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCleaningWeightage.ModifiedDate = DateTime.Now;
            }
            else
            {
                objCleaningWeightage.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCleaningWeightage.CreatedDate = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    if (context.Save(objCleaningWeightage) > 0)
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                        TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    }
                    else
                    {
                        TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                }

                TempData["Id"] = objCleaningWeightage.CleaningWeightageId;

                return RedirectToAction(Actions.CleaningWeightage, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
            }
            else
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                TempData["Id"] = objCleaningWeightage.CleaningWeightageId;

                return RedirectToAction(Actions.CleaningWeightage, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
            }
        }

        #endregion
    }
}