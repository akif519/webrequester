﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Model;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// AccountCode Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Accounts the code.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.AccountCode)]
        public ActionResult AccountCode()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_AccountCode_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.AccountCode);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the account code.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAccountCode)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAccountCode([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AccountCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Accountcode objAccountCode = new Accountcode();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objAccountCode, SystemEnum.Pages.AccountCode.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// _s the edit account code.
        /// </summary>
        /// <param name="accountCodeId">The account code identifier.</param>
        /// <returns></returns> 
        [OutputCache(NoStore = true, Duration = 0)]
        [ActionName(Actions._EditAccountCode)]
        public PartialViewResult _EditAccountCode(int accountCodeId)
        {
            IEnumerable<SelectListItem> status = Enum.GetValues(typeof(SystemEnum.Status)).Cast<SystemEnum.Status>().Select(x => new SelectListItem()
            {
                Text = SystemEnum.GetEnumName(typeof(SystemEnum.Status), (int)x),
                Value = Convert.ToBoolean((int)x).ToString()
            }).ToList();
            ViewBag.Status = status;

            Accountcode objAccountCode = new Accountcode();
            objAccountCode.IsEnableCostCenter = false;
            if (accountCodeId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objAccountCode = context.SelectObject<Accountcode>(accountCodeId);
                    bool returnValue = context.HasReferencesRecords<Accountcode>(objAccountCode, accountCodeId);
                    objAccountCode.IsEnableCostCenter = returnValue;
                }
            }

            return PartialView(Pages.PartialViews.EditAccountCode, objAccountCode);
        }

        /// <summary>
        /// Manages the account code.
        /// </summary>
        /// <param name="objAccountCode">The object account Code.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageAccountCode)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageAccountCode(Accountcode objAccountCode)
        {
            string validations = string.Empty;

            if (objAccountCode.AccCodeid > 0)
            {
                objAccountCode.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objAccountCode.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_AccountCode_ChangeMastersCodes)
                {
                    objAccountCode.AccountCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AccountCode_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objAccountCode.CreatedBy = ProjectSession.EmployeeID.ToString();
                objAccountCode.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_AccountCode_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AccountCode"))
                {
                    if (objAccountCode.Status == null)
                    {
                        objAccountCode.Status = false;
                    }

                    if (objAccountCode.DateStart == null)
                    {
                        objAccountCode.DateStart = (DateTime?)null;
                    }

                    if (objAccountCode.DateEnd == null)
                    {
                        objAccountCode.DateEnd = (DateTime?)null;
                    }

                    if (context.Save(objAccountCode) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.AccountCode_MsgAccountCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the account code.
        /// </summary>
        /// <param name="accountCodeId">The account code identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAccountCode)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAccountCode(int accountCodeId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<Accountcode>(accountCodeId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all Account Codes.
        /// </summary>
        /// <param name="accountCodeId">The account code identifier.</param>
        /// <param name="costCenterId">The cost center identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAllAccountCodesForDDL)]
        [HttpGet]
        public ActionResult GetAllAccountCodesForDDL(int accountCodeId = 0, int costCenterId = 0)
        {
            Accountcode accountCode = new Accountcode();
            IList<Accountcode> lstAccountCodes = new List<Accountcode>();
            bool status = false;
            var result = new List<Accountcode>();

            ////if (accountCodeId > 0)
            ////{
            ////    accountCode.AccCodeid = accountCodeId;
            ////}

            ////if (costCenterId > 0)
            ////{
            ////    accountCode.CostCenterId = costCenterId;
            ////}

            using (ServiceContext objService = new ServiceContext())
            {
                ////lstAccountCodes = objService.Search<Accountcode>(accountCode);
                if (accountCodeId > 0)
                {
                    status = objService.SelectObject<Accountcode>(Convert.ToInt32(accountCodeId)).Status;
                    if (status)
                    {
                        result = objService.SearchAll<Accountcode>(accountCode).Where(x => x.Status == true && x.CostCenterId == costCenterId).ToList();
                    }
                    else
                    {
                        result = objService.SearchAll<Accountcode>(accountCode).Where(x => x.Status == true && x.CostCenterId == costCenterId).Union(objService.SearchAll<Accountcode>(accountCode).Where(x => x.AccCodeid == accountCodeId && x.CostCenterId == costCenterId)).ToList();
                    }
                }
                else
                {
                    result = objService.SearchAll<Accountcode>(accountCode).Where(x => x.Status == true && x.CostCenterId == costCenterId).ToList();
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the account code for pop up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAccountCodeForPopUp)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAccountCodeForPopUp([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AccountCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AccountCodeService objService = new AccountCodeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetAccountCodes(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }
    }
}