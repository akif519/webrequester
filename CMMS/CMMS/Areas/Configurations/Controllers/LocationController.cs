﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Location Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="strWhere">Where Condition</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationPage)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetLocationPage([DataSourceRequest]DataSourceRequest request, string strWhere)
        {
            IList<Location> lstBuildings = new List<Location>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                LocationService objService = new LocationService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetLocationPage(strWhere, empID, ProjectSession.IsCentral, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        #region "Location Type"

        /// <summary>
        /// Locations the type.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.LocationType)]
        public ActionResult LocationType()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_LocationType_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.LocationType);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the type of the location.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationType([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LocationTypeCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            LocationType objLocationType = new LocationType();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objLocationType, SystemEnum.Pages.LocationType.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the location type by identifier.
        /// </summary>
        /// <param name="locationTypeId">The location type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationTypeByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLocationTypeByID(int locationTypeId)
        {
            LocationType objLocationType = new LocationType();
            if (locationTypeId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objLocationType = objDapperContext.SelectObject<LocationType>(locationTypeId);
                }
            }

            return Json(objLocationType, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the type of the location.
        /// </summary>
        /// <param name="objLocationType">Type of the object location.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageLocationType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageLocationType(LocationType objLocationType)
        {
            string validations = string.Empty;

            if (objLocationType.LocationTypeID > 0)
            {
                objLocationType.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objLocationType.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_LocationType_ChangeMastersCodes)
                {
                    objLocationType.LocationTypeCode = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_LocationType_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objLocationType.CreatedBy = ProjectSession.EmployeeID.ToString();
                objLocationType.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_LocationType_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("LocationTypeCode"))
                {
                    if (objDapperContext.Save(objLocationType) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.LocationType_MsgLocationTypeCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the type of the location.
        /// </summary>
        /// <param name="locationTypeId">The location type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteLocationType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteLocationType(int locationTypeId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<LocationType>(locationTypeId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the location types.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetLocationTypes)]
        public ActionResult GetLocationTypes()
        {
            LocationType objLocationType = new LocationType();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objLocationType);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "Linked Locations"

        /// <summary>
        /// To Get Building Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="cityID">The city identifier.</param>
        /// <param name="parentLocationID">The parent location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetUnLinkedLocationsList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult GetUnLinkedLocationsList([DataSourceRequest]DataSourceRequest request, int cityID, int parentLocationID)
        {
            IList<Location> lstLocations = new List<Location>();
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }

                int empID = ProjectSession.EmployeeID;
                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                LocationService objService = new LocationService();

                var result = new DataSourceResult()
                {
                    Data = objService.GetUnLinkedLocationPage(cityID, parentLocationID, pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Save City Permission Data
        /// </summary>
        /// <param name="lstChangedObjs">serialized grid data</param>
        /// <param name="parentLocationID">The employee identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SaveLinkedLocations)]
        [ValidateAntiForgeryToken]
        public ActionResult SaveLinkedLocations(string lstChangedObjs, int parentLocationID)
        {
            try
            {
                List<int> lstLocations = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstChangedObjs);
                LocationService objService = new LocationService();
                int createdBy = ProjectSession.EmployeeID;
                bool result = true;

                if (lstLocations != null && lstLocations.Count > 0)
                {
                    result = objService.InsertLinkedLocationByList(lstLocations, parentLocationID);
                }

                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// Deletes the linked locations of specified location.
        /// </summary>
        /// <param name="parentLocationID">The parent location identifier.</param>
        /// <param name="locationID">The location identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletLinkedLocation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletLinkedLocation(int parentLocationID, int locationID)
        {
            try
            {
                bool result = false;
                LocationService objService = new LocationService();
                using (ServiceContext objContext = new ServiceContext())
                {
                    result = objService.DeleteLinkedLocationByparentLocationID(parentLocationID, locationID);
                }

                if (result)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}