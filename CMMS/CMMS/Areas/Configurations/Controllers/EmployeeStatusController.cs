﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Employees the status.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.EmployeeStatus)]
        public ActionResult EmployeeStatus()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_EmployeeStatus_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                IEnumerable<SelectListItem> displayInModule = Enum.GetValues(typeof(SystemEnum.DisplayInModule)).Cast<SystemEnum.DisplayInModule>().Select(x => new SelectListItem()
                {
                    Text = SystemEnum.GetEnumName(typeof(SystemEnum.DisplayInModule), (int)x),
                    Value = ((int)x).ToString()
                }).ToList();
                ViewBag.DisplayInModule = displayInModule;

                return View(Views.EmployeeStatus);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the employee status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeStatus([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "EmployeeStatus";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmployeeStatuse empStatusSearch = new EmployeeStatuse();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(empStatusSearch, SystemEnum.Pages.EmployeeStatus.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the employee status by identifier.
        /// </summary>
        /// <param name="empStatusId">The employee status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeStatusByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetEmployeeStatusByID(int empStatusId)
        {
            EmployeeStatuse empStatus = new EmployeeStatuse();
            IList<EmployeeStatuse> lstEmpStatus = new List<EmployeeStatuse>();

            if (empStatusId > 0)
            {
                using (ServiceContext empStatusService = new ServiceContext())
                {
                    empStatus = empStatusService.SelectObject<EmployeeStatuse>(empStatusId);
                }

                return Json(empStatus, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    empStatus = new EmployeeStatuse();
                    lstEmpStatus = context.SearchAll(empStatus);
                }

                return Json(lstEmpStatus, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the employee status.
        /// </summary>
        /// <param name="empStatus">The employee status.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageEmployeeStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageEmployeeStatus(EmployeeStatuse empStatus)
        {
            string validations = string.Empty;

            if (empStatus.EmployeeStatusId > 0)
            {
                empStatus.ModifiedBy = ProjectSession.EmployeeID;
                empStatus.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_EmployeeStatus_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                empStatus.CreatedBy = ProjectSession.EmployeeID;
                empStatus.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_EmployeeStatus_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext empStatusService = new ServiceContext("EmployeeStatus"))
                {
                    if (empStatusService.Save(empStatus) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgEmployeeStatusAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the employee status.
        /// </summary>
        /// <param name="empStatusId">The employee status identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmployeeStatus)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployeeStatus(int empStatusId)
        {
            using (ServiceContext empStatusService = new ServiceContext())
            {
                int returnValue = empStatusService.Delete<EmployeeStatuse>(empStatusId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// To get All Employee Categories
        /// </summary>
        /// <returns>List Of employee categories without any filtering</returns>
        [ActionName(Actions.GetEmpStatues)]
        public ActionResult GetEmpStatues()
        {
            EmployeeStatuse objEmpStatus = new EmployeeStatuse();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<EmployeeStatuse>(objEmpStatus);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
    }
}