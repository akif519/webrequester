﻿using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using Newtonsoft.Json;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Area Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Areas this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Area)]
        public ActionResult Area()
        {
            if (ProjectSession.IsAreaFieldsVisible && ProjectSession.PermissionAccess.SetupConfiguration_Area_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.Area);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetArea)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetArea([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                string sortExpression = string.Empty;
                string sortDirection = string.Empty;
                string whereClause = string.Empty;

                if (request.Sorts.Count > 0)
                {
                    sortExpression = request.Sorts[0].Member;
                    sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
                }
                else
                {
                    sortExpression = "L3No";
                    sortDirection = "Ascending";
                }

                int pageNumber = request.Page;
                ProjectSession.PageSize = request.PageSize;

                L3 objArea = new L3();

                SearchFieldService obj = new SearchFieldService();
                /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

                if (!ProjectSession.IsCentral)
                {
                    whereClause += " and (L3.L3ID in ( " + ProjectConfiguration.L3IDLISTPERMISSIONWISE + ") OR (L3.L3ID IS NULL))";
                }

                /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
                var result = new DataSourceResult()
                {
                    Data = obj.AdvanceSearch(objArea, SystemEnum.Pages.Area.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                    Total = obj.PagingInformation.TotalRecords
                };
                return Json(result);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(string.Empty);
            }
        }

        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAreaByPermissionAndL2ID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAreaByPermissionAndL2ID([DataSourceRequest]DataSourceRequest request, int empID, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "L3No";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            AreaService obj = new AreaService();

            var result = new DataSourceResult()
            {
                Data = obj.GetAreaByPermissionAndL2ID(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, empID, cityID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the area by identifier.
        /// </summary>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetAreaByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetAreaByID(int areaId)
        {
            L3 objArea = new L3();
            if (areaId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objArea = objDapperContext.SelectObject<L3>(areaId);
                    bool returnValue = objDapperContext.HasReferencesRecords<L3>(objArea, areaId);
                    objArea.IsEnableCity = returnValue;
                    if (objArea != null)
                    {
                        L2 objL2 = new L2();
                        using (DapperContext context = new DapperContext())
                        {
                            objL2 = context.SearchAll(objL2).Where(m => m.L2ID == objArea.L2ID).FirstOrDefault();
                            objArea.L1ID = objL2.L1ID;
                        }
                    }
                }
            }

            return Json(objArea, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the area.
        /// </summary>
        /// <param name="objArea">The object area.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageArea)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageArea(L3 objArea)
        {
            string validations = string.Empty;
            bool isEditModeAndEmpNotCentral = false;
            L2 objL2 = new L2();

            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = objArea.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(objArea.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            if (objArea.L3ID > 0)
            {
                objArea.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objArea.ModifiedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_Area_ChangeMastersCodes)
                {
                    objArea.L3No = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Area_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objArea.CreatedBy = ProjectSession.EmployeeID.ToString();
                objArea.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Area_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("L3No"))
                {
                    if (objArea.L3ID == 0 && ProjectSession.IsCentral == false)
                    {
                        isEditModeAndEmpNotCentral = true;
                    }

                    if (objDapperContext.Save(objArea) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, isEditModeAndEmpNotCentral });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Area_MsgAreaCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the area.
        /// </summary>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteArea)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteArea(int areaId)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                int returnValue = objDapperContext.Delete<L3>(areaId, true);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all area.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllArea)]
        public ActionResult GetAllArea()
        {
            L3 objArea = new L3();
            using (DapperContext objDapperContext = new DapperContext())
            {
                var result = objDapperContext.SearchAll(objArea);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To Get Area Details
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="cityID">City ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetAreabyL2ID)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetAreabyL2ID([DataSourceRequest]DataSourceRequest request, int cityID)
        {
            try
            {
                AreaService objService = new AreaService();
                var result = objService.GetAreaDetailsByL2ID(cityID, 1);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}