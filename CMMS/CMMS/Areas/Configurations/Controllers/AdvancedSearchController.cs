﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Service;
using Newtonsoft.Json;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// _Advanced Search
        /// </summary>
        /// <param name="title">The Title</param>
        /// <param name="pageId">The pageId</param>
        /// <returns></returns>
        [ActionName(Actions._AdvancedSearch)]
        public PartialViewResult _AdvancedSearch(string title, int pageId)
        {
            ViewBag.Title = title;
            ViewBag.PageID = pageId;
            return PartialView(Pages.PartialViews.AdvancedSearch);
        }

        /// <summary>
        /// Save And Search
        /// </summary>
        /// <param name="objAdvanceSearch">Advance Search</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SaveAndAdvancedSearch)]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveAndAdvancedSearch(SearchFilter objAdvanceSearch)
        {
            try
            {
                SearchFilter objSearchFilter = null;

                SearchFilterService obj = new SearchFilterService();
                int pageid = objAdvanceSearch.PageID;
                obj.DeletePreviousFilter<SearchFilter>(pageid, ProjectSession.EmployeeID);

                using (ServiceContext customReportFilterService = new ServiceContext())
                {
                    List<SearchFilter> lstReportFilter = JsonConvert.DeserializeObject<List<SearchFilter>>(objAdvanceSearch.CustomReportFilter);
                    foreach (SearchFilter objReportFilter in lstReportFilter)
                    {
                        objSearchFilter = new SearchFilter();
                        objSearchFilter.PageID = pageid;
                        objSearchFilter.SearchFilterID = 0;
                        //////objCustomReportFilter.CustomReportID = retval;
                        objSearchFilter.SearchFieldID = objReportFilter.SearchFieldID;
                        objSearchFilter.OperatorID = objReportFilter.OperatorID;
                        objSearchFilter.Value1 = objReportFilter.Value1;
                        objSearchFilter.Value2 = objReportFilter.Value2;
                        objSearchFilter.Condition = objReportFilter.Condition;
                        objSearchFilter.IsSaved = true;
                        objSearchFilter.EmployeeID = ProjectSession.EmployeeID;
                        objSearchFilter.CreatedBy = ProjectSession.EmployeeID;
                        objSearchFilter.CreatedOn = DateTime.Now;

                        customReportFilterService.Save<SearchFilter>(objSearchFilter);
                    }
                }

                return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Save And Search
        /// </summary>
        /// <param name="objAdvanceSearch">Advance Search</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SaveSearch)]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveSearch(SearchFilter objAdvanceSearch)
        {
            try
            {
                SearchFilter objCustomReportFilter = null;

                SearchFilterService obj = new SearchFilterService();
                int pageid = SystemEnum.Pages.Sector.GetHashCode();
                int sessionEmpId = ProjectSession.EmployeeID;
                obj.DeletePreviousFilter<SearchFilter>(pageid, sessionEmpId, false);

                using (ServiceContext customReportFilterService = new ServiceContext())
                {
                    List<SearchFilter> lstReportFilter = JsonConvert.DeserializeObject<List<SearchFilter>>(objAdvanceSearch.CustomReportFilter);
                    foreach (SearchFilter objReportFilter in lstReportFilter)
                    {
                        objCustomReportFilter = new SearchFilter();
                        objCustomReportFilter.PageID = SystemEnum.Pages.Sector.GetHashCode();
                        objCustomReportFilter.SearchFilterID = objReportFilter.SearchFilterID;
                        objCustomReportFilter.SearchFieldID = objReportFilter.SearchFieldID;
                        objCustomReportFilter.OperatorID = objReportFilter.OperatorID;
                        objCustomReportFilter.Value1 = objReportFilter.Value1;
                        objCustomReportFilter.Value2 = objReportFilter.Value2;
                        objCustomReportFilter.Condition = objReportFilter.Condition;
                        objCustomReportFilter.IsSaved = false;
                        objCustomReportFilter.EmployeeID = ProjectSession.EmployeeID;
                        customReportFilterService.Save<SearchFilter>(objCustomReportFilter);
                    }
                }

                return Json(new object[] { false, SystemEnum.MessageType.success.ToString(), string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the advanced search.
        /// </summary>
        /// <param name="pageID">The page identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAdvancedSearch)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAdvancedSearch(int pageID)
        {
            try
            {
                SearchFilterService obj = new SearchFilterService();
                obj.DeletePreviousFilter<SearchFilter>(pageID, ProjectSession.EmployeeID);
                return Json(new object[] { false, SystemEnum.MessageType.success.ToString(), string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Search Fields By Page ID and Session Employee ID
        /// </summary>
        /// <param name="pageID">Page ID From SysEnum</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetSearchDataByPageID)]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult GetSearchDataByPageID(int pageID)
        {
            try
            {
                SearchFilter filterSearch = new SearchFilter();
                filterSearch.PageID = pageID;

                SearchFilterService obj = new SearchFilterService();

                var result = obj.GetAdvanceSearchFiltersByPage(filterSearch, pageID, ProjectSession.EmployeeID).ToList();
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new object[] { false, SystemEnum.MessageType.danger.ToString(), ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}