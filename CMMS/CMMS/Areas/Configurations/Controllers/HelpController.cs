﻿using System;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Help Controller
    /// </summary>
    [ValidateInput(false)]
    public partial class ConfigurationsController : BaseController
    {
        #region "Help List"

        /// <summary>
        /// Helps the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.HelpList)]
        public ActionResult HelpList()
        {
            return View(Views.HelpList);
        }

        /// <summary>
        /// Gets the help modules list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetHelpModulesList)]
        public ActionResult GetHelpModulesList()
        {
            CustomHelpService objService = new CustomHelpService();

            var result = objService.GetHelpModuleList(ProjectSession.Culture);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the help list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="helpModuleID">The help module identifier.</param>
        /// <param name="pageName">Name of the page.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetHelpList)]
        public ActionResult GetHelpList([DataSourceRequest]DataSourceRequest request, int? helpModuleID, string pageName)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;

            ProjectSession.PageSize = request.PageSize;
            CustomHelpService objService = new CustomHelpService();

            var result = new DataSourceResult()
            {
                Data = objService.GetHelpModuleGrid(ConvertTo.Integer(helpModuleID), pageName, pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the help modules.
        /// </summary>
        /// <param name="helpModuleID">The help module identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpModules)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetHelpModules(int helpModuleID)
        {
            HelpModule objHelpModule = new HelpModule();
            if (helpModuleID > 0)
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    ProjectSession.UseHelpDB = true;
                    objHelpModule = objService.SelectObject<HelpModule>(helpModuleID);
                }
            }

            return Json(objHelpModule, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Deletes the help modules.
        /// </summary>
        /// <param name="helpModuleID">The help module identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteHelpModules)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteHelpModules(int helpModuleID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                ProjectSession.UseHelpDB = true;

                HelpModule objOldHelpModule = new HelpModule();
                List<HelpModule> lstHelpModule = new List<HelpModule>();
                lstHelpModule = new DapperContext().SearchAll(objOldHelpModule).Where(o => o.ParentID == helpModuleID).ToList();
                if (lstHelpModule.Count > 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }

                CustomHelpService objService = new CustomHelpService();
                int returnValue = objService.DeleteHelpModuleById(helpModuleID);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Help Detail"

        /// <summary>
        /// Helps the detail.
        /// </summary>
        /// <param name="helpModuleID">The help module identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.HelpDetail)]
        [ValidateInput(false)]
        public ActionResult HelpDetail(int? helpModuleID)
        {
            HelpModule objHelpModule = new HelpModule();

            CustomHelpService objService = new CustomHelpService();

            if (helpModuleID > 0)
            {
                objHelpModule = objService.GetHelpModuleGrid(ConvertTo.Integer(helpModuleID), string.Empty, 0, "HelpModuleID", "Descending").Where(o => o.HelpModuleID == helpModuleID).FirstOrDefault();
            }

            return View(Views.HelpDetail, objHelpModule);
        }

        /// <summary>
        /// Gets the help page detail.
        /// </summary>
        /// <param name="helpModuleDetailID">The help module detail identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpPageDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult GetHelpPageDetail(int helpModuleDetailID)
        {
            HelpModule_Detail helpPageDetail = new HelpModule_Detail();

            if (helpModuleDetailID > 0)
            {
                helpPageDetail.HelpModuelDetailID = helpModuleDetailID;
                using (ServiceContext helpPageDetailService = new ServiceContext())
                {
                    ProjectSession.UseHelpDB = true;

                    helpPageDetail = helpPageDetailService.Search(helpPageDetail).FirstOrDefault();
                }
            }

            if (helpPageDetail == null)
            {
                helpPageDetail = new HelpModule_Detail();
                helpPageDetail.HelpModuelDetailID = helpModuleDetailID;
            }

            return Json(helpPageDetail, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the help detail.
        /// </summary>
        /// <param name="objHelpModuleDetail">The object help module detail.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageHelpDetail)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageHelpDetail(HelpModule_Detail objHelpModuleDetail)
        {
            string validations = string.Empty;
            bool flag = false;

            CustomHelpService objService = new CustomHelpService();
            flag = objService.InsertHelpDetailPage(objHelpModuleDetail);
            objHelpModuleDetail.HelpDescription = HttpUtility.HtmlEncode(objHelpModuleDetail.HelpDescription);

            if (flag)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully, objHelpModuleDetail.HelpModuleID }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        #endregion

        #region "Other Method"

        /// <summary>
        /// Gets the language list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetLanguageList)]
        public ActionResult GetLanguageList()
        {
            Language objLanguage = new Language();
            using (DapperContext objDapperContext = new DapperContext())
            {
                ProjectSession.UseHelpDB = true;
                var result = objDapperContext.SearchAll(objLanguage);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the module access list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetModuleAccessList)]
        public ActionResult GetModuleAccessList()
        {
            IEnumerable<SelectListItem> modulesAccess = Enum.GetValues(typeof(SystemEnum.ModulesAccess)).Cast<SystemEnum.ModulesAccess>().Select(x => new SelectListItem()
            {
                Text = SystemEnum.GetEnumName(typeof(SystemEnum.ModulesAccess), (int)x),
                Value = ((int)x).ToString()
            }).ToList();

            return Json(modulesAccess, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region "Help Chapter"

        /// <summary>
        /// Gets the chapter list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="helpModuelDetailID">The help module detail identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetChapterList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetChapterList([DataSourceRequest]DataSourceRequest request, int helpModuelDetailID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            CustomHelpService objService = new CustomHelpService();
            var result = new DataSourceResult()
            {
                Data = objService.GetChapterList(ConvertTo.Integer(helpModuelDetailID), pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the chapter.
        /// </summary>
        /// <param name="helpChapterID">The help chapter identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetChapter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetChapter(int helpChapterID)
        {
            HelpChapter helpChapter = new HelpChapter();
            if (helpChapterID > 0)
            {
                using (ServiceContext helpChapterService = new ServiceContext())
                {
                    ProjectSession.UseHelpDB = true;
                    helpChapter = helpChapterService.SelectObject<HelpChapter>(helpChapterID);
                }
            }

            return Json(helpChapter, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the chapter.
        /// </summary>
        /// <param name="helpChapter">The help chapter.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageChapter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageChapter(HelpChapter helpChapter)
        {
            string validations = string.Empty;

            if (helpChapter.HelpChapterID > 0)
            {
                helpChapter.ModifiedBy = ProjectSession.EmployeeID;
                helpChapter.ModifiedOn = DateTime.Now;
            }
            else
            {
                helpChapter.CreatedBy = ProjectSession.EmployeeID;
                helpChapter.CreatedOn = DateTime.Now;
            }

            if (ModelState.IsValid)
            {
                using (DapperContext helpChapterService = new DapperContext())
                {
                    ProjectSession.UseHelpDB = true;
                    if (helpChapterService.Save(helpChapter) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Help_MsgChapterTitleAlreadyExit });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the chapter.
        /// </summary>
        /// <param name="helpChapterID">The help chapter identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteChapter)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteChapter(int helpChapterID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                ProjectSession.UseHelpDB = true;
                int returnValue = objDapperContext.Delete<HelpChapter>(helpChapterID, false);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region "Related Topics"

        /// <summary>
        /// Gets the help related topic list.
        /// </summary>
        /// <param name="hasHelpDescription">The has help description.</param>
        /// <param name="helpDetailID">The help detail identifier.</param>
        /// <param name="childHelpModuleDetailID">The child help module detail identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpRelatedTopicList)]
        public ActionResult GetHelpRelatedTopicList(int hasHelpDescription, int helpDetailID, int? childHelpModuleDetailID)
        {
            CustomHelpService objService = new CustomHelpService();

            var result = objService.GetHelpRelatedTopicsList(ProjectSession.Culture, hasHelpDescription, helpDetailID, childHelpModuleDetailID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the help related topics.
        /// </summary>
        /// <param name="relatedTopicID">The related topic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpRelatedTopics)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetHelpRelatedTopics(int relatedTopicID)
        {
            HelpRelatedTopic helpRelatedTopic = new HelpRelatedTopic();
            if (relatedTopicID > 0)
            {
                using (ServiceContext helpChapterService = new ServiceContext())
                {
                    ProjectSession.UseHelpDB = true;
                    helpRelatedTopic = helpChapterService.SelectObject<HelpRelatedTopic>(relatedTopicID);
                }
            }

            return Json(helpRelatedTopic, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the related topic list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="helpModuelDetailID">The help module detail identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetRelatedTopicList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetRelatedTopicList([DataSourceRequest]DataSourceRequest request, int helpModuelDetailID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;
            CustomHelpService objService = new CustomHelpService();
            var result = new DataSourceResult()
            {
                Data = objService.GetHelpRelatedTopics(ConvertTo.Integer(helpModuelDetailID), pageNumber, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Manages the help related topics.
        /// </summary>
        /// <param name="helpRelatedTopic">The help related topic.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageHelpRelatedTopics)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageHelpRelatedTopics(HelpRelatedTopic helpRelatedTopic)
        {
            string validations = string.Empty;

            if (ModelState.IsValid)
            {
                using (DapperContext helpChapterService = new DapperContext())
                {
                    ProjectSession.UseHelpDB = true;
                    if (helpChapterService.Save(helpRelatedTopic) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Help_MsgRelatedTopicsAlreadyExit });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the help related topics.
        /// </summary>
        /// <param name="relatedTopicID">The related topic identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteHelpRelatedTopics)]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteHelpRelatedTopics(int relatedTopicID)
        {
            using (DapperContext objDapperContext = new DapperContext())
            {
                ProjectSession.UseHelpDB = true;
                int returnValue = objDapperContext.Delete<HelpRelatedTopic>(relatedTopicID, false);

                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion
    }
}