﻿using System.Collections.Generic;
using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using Newtonsoft.Json;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Item Type"

        /// <summary>
        /// Items the type.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ItemType)]
        public ActionResult ItemType()
        {
            if (ProjectSession.PermissionAccess.Stores_ItemType_Allowaccess && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                return View(Views.ItemType);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the type of the item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemType([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AttrName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Attrgroup itemTypeSearch = new Attrgroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(itemTypeSearch, SystemEnum.Pages.ItemType.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets all Item Sub Types
        /// </summary>
        /// <param name="attrID">The Item Type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemSubTypesByItemTypeDDL)]
        [HttpGet]
        public ActionResult GetItemSubTypesByItemTypeDDL(int attrID)
        {
            Attrsubgroup attrSubType = new Attrsubgroup();
            IList<Attrsubgroup> lstItemSubTypes = new List<Attrsubgroup>();

            if (attrID > 0)
            {
                attrSubType.AttrID = attrID;
            }

            using (ServiceContext objService = new ServiceContext())
            {
                lstItemSubTypes = objService.Search<Attrsubgroup>(attrSubType);
            }

            return Json(lstItemSubTypes, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets all Item Types
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllItemTypesDDL)]
        [HttpGet]
        public ActionResult GetAllItemTypesDDL()
        {
            Attrgroup attrgroup = new Attrgroup();
            IList<Attrgroup> lstItemTypes = new List<Attrgroup>();

            using (ServiceContext objService = new ServiceContext())
            {
                lstItemTypes = objService.Search<Attrgroup>(attrgroup);
            }

            return Json(lstItemTypes, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the item type by identifier.
        /// </summary>
        /// <param name="itemTypeID">The item type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemTypeByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemTypeByID(int itemTypeID)
        {
            Attrgroup objItemType = new Attrgroup();
            IList<Attrgroup> lstItemTypes = new List<Attrgroup>();

            if (itemTypeID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objItemType = context.SelectObject<Attrgroup>(itemTypeID);
                }

                return Json(objItemType, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    lstItemTypes = objService.Search(objItemType);
                }

                return Json(lstItemTypes, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the type of the item.
        /// </summary>
        /// <param name="objItemType">Type of the object item.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageItemType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageItemType(Attrgroup objItemType)
        {
            string validations = string.Empty;

            if (objItemType.AttrID > 0)
            {
                objItemType.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objItemType.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemType_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objItemType.CreatedBy = ProjectSession.EmployeeID.ToString();
                objItemType.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemType_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AttrName"))
                {
                    if (context.Save(objItemType) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemType_MsgItemTypeAlreadyExists }); 
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the type of the item.
        /// </summary>
        /// <param name="itemTypeID">The item type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteItemType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteItemType(int itemTypeID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Attrgroup>(itemTypeID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the item types.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetItemTypes)]
        public ActionResult GetItemTypes()
        {
            Attrgroup objItemType = new Attrgroup();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objItemType);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion 

        #region "Item Sub Type"

        /// <summary>
        /// Items the type of the sub.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ItemSubType)]
        public ActionResult ItemSubType()
        {
            if (ProjectSession.PermissionAccess.Stores_ItemSubType_Allowaccess && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                return View(Views.ItemSubType);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the type of the item sub.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemSubType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemSubType([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "AttrSubName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Attrsubgroup itemSubTypeSearch = new Attrsubgroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(itemSubTypeSearch, SystemEnum.Pages.ItemSubType.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item sub type by identifier.
        /// </summary>
        /// <param name="itemSubTypeID">The item sub type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemSubTypeByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemSubTypeByID(int itemSubTypeID)
        {
            Attrsubgroup objItemSubType = new Attrsubgroup();
            if (itemSubTypeID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objItemSubType = context.SelectObject<Attrsubgroup>(itemSubTypeID);
                }
            }

            return Json(objItemSubType, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the type of the item sub.
        /// </summary>
        /// <param name="objItemSubType">Type of the object item sub.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageItemSubType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageItemSubType(Attrsubgroup objItemSubType)
        {
            string validations = string.Empty;

            if (objItemSubType.AttrSubID > 0)
            {
                objItemSubType.ModifiedBy = ProjectSession.EmployeeID;
                objItemSubType.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemSubType_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objItemSubType.CreatedBy = ProjectSession.EmployeeID;
                objItemSubType.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemSubType_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("AttrSubName"))
                {
                    if (context.Save(objItemSubType) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.ItemSubType_MsgItemSubTypeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the type of the item sub.
        /// </summary>
        /// <param name="itemSubTypeID">The item sub type identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteItemSubType)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteItemSubType(int itemSubTypeID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Attrsubgroup>(itemSubTypeID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion 

        #region "Stores List"

        /// <summary>
        /// Stores this instance.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Stores)]
        public ActionResult Stores()
        {
            if (ProjectSession.PermissionAccess.Stores_Stores_Allowaccess && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                return View(Views.Stores);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the stores.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStores)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetStores([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SubStoreCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Substore substoreSearch = new Substore();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and substore.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(substoreSearch, SystemEnum.Pages.Stores.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the stores by identifier.
        /// </summary>
        /// <param name="storesId">The stores identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStoresByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetStoresByID(int storesId)
        {
            Substore objSubstore = new Substore();
            IList<Substore> lstSubstore = new List<Substore>();

            if (storesId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSubstore = context.SelectObject<Substore>(storesId);
                    bool returnValue = context.HasReferencesRecords<Substore>(objSubstore, storesId);
                    objSubstore.IsEnableCity = returnValue;
                }

                return Json(objSubstore, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (ServiceContext objService = new ServiceContext())
                {
                    lstSubstore = objService.Search(objSubstore);
                }

                return Json(lstSubstore, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Manages the stores.
        /// </summary>
        /// <param name="objSubstore">The object stores.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageStores)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageStores(Substore objSubstore)
        {
            string validations = string.Empty;

            L2 objL2 = new L2();
            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = objSubstore.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(objSubstore.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            } 

            if (objSubstore.SubStoreID > 0)
            {
                objSubstore.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objSubstore.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_Stores_ChangeMastersCodes)
                {
                    objSubstore.SubStoreCode = null;
                }

                if (!ProjectSession.PermissionAccess.Stores_Stores_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSubstore.CreatedBy = ProjectSession.EmployeeID.ToString();
                objSubstore.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_Stores_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("SubStoreCode", "L2ID", true))
                {
                    objSubstore.SubStoreID = context.Save(objSubstore);
                    if (objSubstore.SubStoreID > 0)
                    {
                        if (Convert.ToBoolean(objSubstore.IsDefault))
                        {
                            Substore objOldSubstore = new Substore();
                            var oldResult = context.SearchAll(objOldSubstore).Where(c => c.IsDefault == true && c.SubStoreID != objSubstore.SubStoreID).FirstOrDefault();
                            if (oldResult != null)
                            {
                                oldResult.IsDefault = false;
                                oldResult.ModifiedBy = ProjectSession.EmployeeID.ToString();
                                oldResult.ModifiedDate = DateTime.Now;
                                context.Save(oldResult);
                            }
                        }

                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Store_MsgStoreCodeAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }
         
        /// <summary>
        /// Deletes the stores.
        /// </summary>
        /// <param name="storesId">The stores identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteStores)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteStores(int storesId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Substore>(storesId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the stores list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetStoresList)]
        public ActionResult GetStoresList()
        {
            Substore objSubstore = new Substore();
            using (DapperContext context = new DapperContext())
            {
                var result = context.SearchAll(objSubstore);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the stores list by l2 identifier.
        /// </summary>
        /// <param name="siteId">The site identifier.</param>
        /// <param name="storeIdToExclude">The store identifier to exclude.</param>
        /// <returns></returns>
        [ActionName(Actions.GetStoresListByL2ID)]
        public ActionResult GetStoresListByL2ID(int siteId, int storeIdToExclude = 0)
        {
            Substore objSubstore = new Substore();
            using (DapperContext context = new DapperContext())
            {
                List<Substore> result = context.SearchAll(objSubstore).Where(r => r.L2ID == siteId).ToList();
                Substore objSubStore = new Substore();

                if (storeIdToExclude > 0 && result != null && result.Count > 0)
                {
                    result.RemoveAll(o => o.SubStoreID == storeIdToExclude);
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion  

        #region "Item Section"

        /// <summary>
        /// Items the section.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ItemSection)]
        public ActionResult ItemSection()
        {
            if (ProjectSession.PermissionAccess.Stores_ItemLocation_Allowaccess && ProjectSession.PermissionAccess.Stores__ShowHideModule)
            {
                return View(Views.ItemSection);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the item section.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemSection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemSection([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PartLocation";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PartLocations itemSectionSearch = new PartLocations();

            SearchFieldService obj = new SearchFieldService();
            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and L2.L2ID in ( " + ProjectConfiguration.L2IDLISTPERMISSIONWISE + ") ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(itemSectionSearch, SystemEnum.Pages.ItemSection.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the item section by identifier.
        /// </summary>
        /// <param name="itemSectionID">The item section identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetItemSectionByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemSectionByID(int itemSectionID)
        {
            PartLocations objItemSection = new PartLocations();
            Substore objSubStore = new Substore();
            if (itemSectionID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objItemSection = context.SelectObject<PartLocations>(itemSectionID);
                    objSubStore = context.SelectObject<Substore>(Convert.ToInt32(objItemSection.SubStoreID));
                    objItemSection.L2ID = objSubStore.L2ID;
                    bool returnValue = context.HasReferencesRecords<PartLocations>(objItemSection, itemSectionID);
                    objItemSection.IsEnableCityStore = returnValue;
                }
            }

            return Json(objItemSection, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the item section.
        /// </summary>
        /// <param name="objItemSection">The object item section.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageItemSection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageItemSection(PartLocations objItemSection)
        {
            string validations = string.Empty;

            L2 objL2 = new L2();
            using (ServiceContext zoneService = new ServiceContext())
            {
                objL2 = objItemSection.L2ID > 0 ? zoneService.SelectObject<L2>(Convert.ToInt32(objItemSection.L2ID)) : new L2();
            }

            if (objL2.Status == 0)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Asset_MsgSiteInactive });
            }

            objItemSection.L2Code = objL2.L2Code;

            if (objItemSection.ID > 0)
            {
                objItemSection.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objItemSection.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemLocation_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objItemSection.CreatedBy = ProjectSession.EmployeeID.ToString();
                objItemSection.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Stores_ItemLocation_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("PartLocation", "SubStoreID", true))
                {
                    if (context.Save(objItemSection) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Location_MsgItemLocationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the item section.
        /// </summary>
        /// <param name="itemSectionID">The item section identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteItemSection)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteItemSection(int itemSectionID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<PartLocations>(itemSectionID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        [ActionName(Actions.GetItemStatusList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetItemStatusList()
        {
            IList<SelectListItem> lst = new List<SelectListItem>();

            SelectListItem item1 = new SelectListItem { Text = "Active", Value = "1" };
            SelectListItem item2 = new SelectListItem { Text = "Not Active", Value = "2" };

            lst.Add(item1);
            lst.Add(item2);

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}