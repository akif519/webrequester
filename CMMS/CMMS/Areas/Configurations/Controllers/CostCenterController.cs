﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Services;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Costs the center.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.CostCenter)]
        public ActionResult CostCenter()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_CostCenter_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.CostCenter);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the cost center.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCostCenter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCostCenter([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CostCenterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CostCenter costCenterSearch = new CostCenter();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(costCenterSearch, SystemEnum.Pages.CostCenter.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the cost center by identifier.
        /// </summary>
        /// <param name="costCenterId">The cost center identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCostCenterByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCostCenterByID(int costCenterId)
        {
            CostCenter costCenter = new CostCenter();
            IList<CostCenter> lstCostCenter = new List<CostCenter>();

            if (costCenterId > 0)
            {
                using (ServiceContext costCenterService = new ServiceContext())
                {
                    costCenter = costCenterService.SelectObject<CostCenter>(costCenterId);
                }

                return Json(costCenter, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    costCenter = new CostCenter();
                    lstCostCenter = context.SearchAll<CostCenter>(costCenter);
                }

                return Json(lstCostCenter, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets all cost center by identifier.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllCostCenterForDDL)]
        [HttpGet]
        public ActionResult GetAllCostCenterForDDL()
        {
            CostCenter costCenter = new CostCenter();
            IList<CostCenter> lstCostCenters = new List<CostCenter>();

            using (ServiceContext costCenterService = new ServiceContext())
            {
                lstCostCenters = costCenterService.Search<CostCenter>(costCenter);
            }

            return Json(lstCostCenters, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the cost center.
        /// </summary>
        /// <param name="costCenter">The cost center.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageCostCenter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageCostCenter(CostCenter costCenter)
        {
            string validations = string.Empty;

            if (costCenter.CostCenterId > 0)
            {
                costCenter.ModifiedBy = ProjectSession.EmployeeID.ToString();
                costCenter.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CostCenter_ChangeMastersCodes)
                {
                    costCenter.CostCenterNo = null;
                }

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CostCenter_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                costCenter.CreatedBy = ProjectSession.EmployeeID.ToString();
                costCenter.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_CostCenter_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (ServiceContext costCenterService = new ServiceContext("CostCenterNo"))
                {
                    if (costCenterService.Save(costCenter) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.CostCenter_MsgCostCenterNoAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the cost center.
        /// </summary>
        /// <param name="costCenterId">The cost center identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteCostCenter)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteCostCenter(int costCenterId)
        {
            using (ServiceContext costCenterService = new ServiceContext())
            {
                int returnValue = costCenterService.Delete<CostCenter>(costCenterId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all cost center.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCostCenters)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCostCenters([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CostCenterNo";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CostCenterService objService = new CostCenterService();
            var result = new DataSourceResult()
            {
                Data = objService.GetCostCenters(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }
        
        /// <summary>
        /// Gets the pm checklist by identifier.
        /// </summary>
        /// <param name="checklistId">The checklist identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPMChecklistByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPmChecklistByID(int checklistId)
        {
            PMCheckList pmChecklist = new PMCheckList();
            IList<PMCheckList> lstPmChecklist = new List<PMCheckList>();

            if (checklistId > 0)
            {
                using (ServiceContext costCenterService = new ServiceContext())
                {
                    pmChecklist = costCenterService.SelectObject<PMCheckList>(checklistId);
                }

                return Json(pmChecklist, JsonRequestBehavior.AllowGet);
            }
            else
            {
                using (DapperContext context = new DapperContext())
                {
                    pmChecklist = new PMCheckList();
                    lstPmChecklist = context.SearchAll<PMCheckList>(pmChecklist);
                }

                return Json(lstPmChecklist, JsonRequestBehavior.AllowGet);
            }
        }
    }
}