﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.Service.ConfigurationService;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// PurchasingSetup Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        #region "Purchasing Currency"

        /// <summary>
        /// Purchasing the currency.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchasingCurrency)]
        public ActionResult PurchasingCurrency()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PurchsingCurrency_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PurchasingCurrency);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchasing currency.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingCurrency)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingCurrency([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CurrencyCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Currency objCurrency = new Currency();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objCurrency, SystemEnum.Pages.PurchasingCurrency.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchasing currency by identifier.
        /// </summary>
        /// <param name="currencyId">The currency identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingCurrencyByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingCurrencyByID(int currencyId)
        {
            Currency objCurrency = new Currency();
            if (currencyId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objCurrency = context.SelectObject<Currency>(currencyId);
                }
            }

            return Json(objCurrency, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the purchasing currency.
        /// </summary>
        /// <param name="objCurrency">The object currency.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchasingCurrency)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePurchasingCurrency(Currency objCurrency)
        {
            string validations = string.Empty;

            if (objCurrency.CurrencyID > 0)
            {
                objCurrency.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objCurrency.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingCurrency_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCurrency.CreatedBy = ProjectSession.EmployeeID.ToString();
                objCurrency.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingCurrency_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("CurrencyCode"))
                {
                    if (context.Save(objCurrency) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchasingSetup_MsgPurchasingCurrencyAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the purchasing currency.
        /// </summary>
        /// <param name="currencyId">The currency identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchasingCurrency)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchasingCurrency(int currencyId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Currency>(currencyId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the currency for pop up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCurrencyForPopUp)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetCurrencyForPopUp([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "CurrencyCode";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            CurrencyService objService = new CurrencyService();
         
            var result = new DataSourceResult()
            {
                Data = objService.GetCurrencys(pageNumber, sortExpression, sortDirection, request: request),
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }
        #endregion

        #region "Purchasing Term of Sale"

        /// <summary>
        /// Purchasing the term of sale.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchasingTermofSale)]
        public ActionResult PurchasingTermofSale()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PurchsingTermOfSale_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PurchasingTermofSale);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchasing term of sale.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingTermofSale)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingTermofSale([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "TermofSaleDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            TermofSale objTermofSale = new TermofSale();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objTermofSale, SystemEnum.Pages.PurchasingTermofSale.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchasing term of sale by identifier.
        /// </summary>
        /// <param name="termofSaleId">The term of sale identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingTermofSaleByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingTermofSaleByID(int termofSaleId)
        {
            TermofSale objTermofSale = new TermofSale();
            if (termofSaleId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objTermofSale = context.SelectObject<TermofSale>(termofSaleId);
                }
            }

            return Json(objTermofSale, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the purchasing term of sale.
        /// </summary>
        /// <param name="objTermofSale">The object term of sale.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchasingTermofSale)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePurchasingTermofSale(TermofSale objTermofSale)
        {
            string validations = string.Empty;

            if (objTermofSale.ID > 0)
            {
                objTermofSale.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objTermofSale.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingTermOfSale_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objTermofSale.CreatedBy = ProjectSession.EmployeeID.ToString();
                objTermofSale.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingTermOfSale_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("TermofSaleDesc"))
                {
                    if (context.Save(objTermofSale) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchasingSetup_MsgPurchasingTermofSaleAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the purchasing term of sale.
        /// </summary>
        /// <param name="termofSaleId">The term of sale identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchasingTermofSale)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchasingTermofSale(int termofSaleId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<TermofSale>(termofSaleId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the term of sales.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetTermofSales)]
        public ActionResult GetTermofSales()
        {
            TermofSale objTermofSale = new TermofSale();
            using (DapperContext objContext = new DapperContext())
            {
                List<TermofSale> result = objContext.SearchAll<TermofSale>(objTermofSale).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Purchasing Shipped Via"

        /// <summary>
        /// Purchasing the shipped via.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchasingShippedVia)]
        public ActionResult PurchasingShippedVia()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PurchsingShippedVia_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PurchasingShippedVia);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchasing shipped via.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingShippedVia)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingShippedVia([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ShippedViaDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            ShippedVia objShippedVia = new ShippedVia();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objShippedVia, SystemEnum.Pages.PurchasingShippedVia.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchasing shipped via by identifier.
        /// </summary>
        /// <param name="shippedViaId">The shipped via identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingShippedViaByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingShippedViaByID(int shippedViaId)
        {
            ShippedVia objShippedVia = new ShippedVia();
            if (shippedViaId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objShippedVia = context.SelectObject<ShippedVia>(shippedViaId);
                }
            }

            return Json(objShippedVia, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the purchasing shipped via.
        /// </summary>
        /// <param name="objShippedVia">The object shipped via.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchasingShippedVia)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePurchasingShippedVia(ShippedVia objShippedVia)
        {
            string validations = string.Empty;

            if (objShippedVia.ID > 0)
            {
                objShippedVia.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objShippedVia.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingShippedVia_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objShippedVia.CreatedBy = ProjectSession.EmployeeID.ToString();
                objShippedVia.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingShippedVia_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("ShippedViaDesc"))
                {
                    if (context.Save(objShippedVia) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchasingSetup_MsgPurchasingShppedViaAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the purchasing shipped via.
        /// </summary>
        /// <param name="shippedViaId">The shipped via identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchasingShippedVia)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchasingShippedVia(int shippedViaId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<ShippedVia>(shippedViaId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the shipped via.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetShippedVias)]
        public ActionResult GetShippedVias()
        {
            ShippedVia objShippedVia = new ShippedVia();
            using (DapperContext objContext = new DapperContext())
            {
                List<ShippedVia> result = objContext.SearchAll<ShippedVia>(objShippedVia).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Purchasing Payment Terms"

        /// <summary>
        /// Purchasing the payment terms.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.PurchasingPaymentTerms)]
        public ActionResult PurchasingPaymentTerms()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_PurchsingPaymentTerms_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.PurchasingPaymentTerms);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the purchasing payment terms.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingPaymentTerms)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingPaymentTerms([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "PaymentTermsDesc";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            PaymentTerm objPaymentTerm = new PaymentTerm();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objPaymentTerm, SystemEnum.Pages.PurchasingPaymentTerms.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the purchasing payment terms by identifier.
        /// </summary>
        /// <param name="paymentTermId">The payment term identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetPurchasingPaymentTermsByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPurchasingPaymentTermsByID(int paymentTermId)
        {
            PaymentTerm objPaymentTerm = new PaymentTerm();
            if (paymentTermId > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objPaymentTerm = context.SelectObject<PaymentTerm>(paymentTermId);
                }
            }

            return Json(objPaymentTerm, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the purchasing payment terms.
        /// </summary>
        /// <param name="objPaymentTerm">The object payment term.</param>
        /// <returns></returns>
        [ActionName(Actions.ManagePurchasingPaymentTerms)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManagePurchasingPaymentTerms(PaymentTerm objPaymentTerm)
        {
            string validations = string.Empty;

            if (objPaymentTerm.ID > 0)
            {
                objPaymentTerm.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objPaymentTerm.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingPaymentTerms_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objPaymentTerm.CreatedBy = ProjectSession.EmployeeID.ToString();
                objPaymentTerm.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_PurchsingPaymentTerms_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("PaymentTermsDesc"))
                {
                    if (context.Save(objPaymentTerm) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.PurchasingSetup_MsgPurchasingPaymentTermAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the purchasing payment terms.
        /// </summary>
        /// <param name="paymentTermId">The payment term identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeletePurchasingPaymentTerms)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeletePurchasingPaymentTerms(int paymentTermId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<PaymentTerm>(paymentTermId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the payment terms.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetPaymentTerms)]
        public ActionResult GetPaymentTerms()
        {
            PaymentTerm objPaymentTerm = new PaymentTerm();
            using (DapperContext objContext = new DapperContext())
            {
                List<PaymentTerm> result = objContext.SearchAll<PaymentTerm>(objPaymentTerm).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Default Setups"

        /// <summary>
        /// Defaults the setups.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.DefaultSetups)]
        public ActionResult DefaultSetups()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_DefaultSetups_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                ViewBag.Message = TempData["Message"];
                ViewBag.MessageType = TempData["MessageType"];

                DefaultSetup objDefaultSetup = new DefaultSetup();
                using (DapperContext context = new DapperContext())
                {
                    List<DefaultSetup> lst = context.SearchAll(objDefaultSetup).ToList();
                    objDefaultSetup.DefaultGST = lst.Where(o => o.SettingName == "Default GST %").FirstOrDefault().Value;
                    objDefaultSetup.DefaultTax = lst.Where(o => o.SettingName == "Default Tax %").FirstOrDefault().Value;
                }

                return View(Views.DefaultSetups, objDefaultSetup);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Manages the default setups.
        /// </summary>
        /// <param name="objDefaultSetup">The object default setup.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageDefaultSetups)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ManageDefaultSetups(DefaultSetup objDefaultSetup)
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_DefaultSetups_Editrecords)
            {
                PurchasingSetupService objPurchasingSetupService = new PurchasingSetupService();
                List<DefaultSetup> lstDefaultSetup = new List<DefaultSetup>();
                DefaultSetup objDefaultSetupNew = new DefaultSetup();
                if (objDefaultSetup.DefaultGST == null)
                {
                    objDefaultSetup.DefaultGST = null;
                }

                if (objDefaultSetup.DefaultTax == null)
                {
                    objDefaultSetup.DefaultTax = null;
                }

                objDefaultSetupNew.SettingName = "Default GST %";
                objDefaultSetupNew.Value = objDefaultSetup.DefaultGST;
                lstDefaultSetup.Add(objDefaultSetupNew);
                objDefaultSetupNew = new DefaultSetup();
                objDefaultSetupNew.SettingName = "Default Tax %";
                objDefaultSetupNew.Value = objDefaultSetup.DefaultTax;
                lstDefaultSetup.Add(objDefaultSetupNew);

                bool isUpdated = false;

                isUpdated = objPurchasingSetupService.UpdateDefaultSetup(lstDefaultSetup);

                if (isUpdated)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                }
                else
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                    TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                }

                return RedirectToAction(Actions.DefaultSetups, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
            }
            else
            {
                TempData["Message"] = ProjectSession.Resources.message.Common_MsgPermissionNotUpdate;
                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();

                return RedirectToAction(Actions.DefaultSetups, Pages.Controllers.Configurations, new { Area = Pages.Areas.Configurations });
            }
        }

        #endregion
    }
}