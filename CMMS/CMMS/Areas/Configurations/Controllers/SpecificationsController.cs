﻿using System.Collections.Generic;
using System;
using System.Web.Mvc;
using System.ComponentModel;
using System.Linq;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Services;
using CMMS.Model;
using CMMS.Controllers;
using CMMS.Service;
using Newtonsoft.Json;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Specifications  Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Specifications the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.SpecificationsList)]
        public ActionResult SpecificationsList()
        {
            if (ProjectSession.PermissionAccess.SetupConfiguration_Specifications_Allowaccess && ProjectSession.PermissionAccess.SetupConfiguration__ShowHideModule)
            {
                return View(Views.SpecificationList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the specification list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSpecificationList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSpecificationList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "SpecID";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Specification areaSearch = new Specification();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(areaSearch, SystemEnum.Pages.Specification.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the specification list by identifier.
        /// </summary>
        /// <param name="specID">The spec identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetSpecificationListByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSpecificationListByID(int specID)
        {
            Specification objSpecification = new Specification();
            if (specID > 0)
            {
                using (DapperContext context = new DapperContext())
                {
                    objSpecification = context.SelectObject<Specification>(specID);
                }
            }

            return Json(objSpecification, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the specification.
        /// </summary>
        /// <param name="objSpecification">The object specification.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageSpecification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageSpecification(Specification objSpecification)
        {
            string validations = string.Empty;

            if (objSpecification.SpecID > 0)
            {
                objSpecification.ModifiedBy = ProjectSession.EmployeeID;
                objSpecification.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.SetupConfiguration_Specifications_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objSpecification.CreatedBy = ProjectSession.EmployeeID;
                objSpecification.CreatedDate = DateTime.Now;
                if (!ProjectSession.PermissionAccess.SetupConfiguration_Specifications_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext("SpecDesc"))
                {
                    if (context.Save(objSpecification) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Specifications_MsgSpecificationAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// Deletes the specification.
        /// </summary>
        /// <param name="specID">The spec identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteSpecification)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteSpecification(int specID)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Specification>(specID, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets all specification for DDL.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetAllSpecificationForDDL)]
        [HttpGet]
        public ActionResult GetAllSpecificationForDDL()
        {
            Specification obj = new Specification();
            List<Specification> lst = new List<Specification>();
            using (ServiceContext objService = new ServiceContext())
            {
                lst = objService.SearchAll<Specification>(obj).ToList();
            }

            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}