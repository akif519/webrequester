﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Service;
using CMMS.Infrastructure;

namespace CMMS.Areas.Configurations.Controllers
{
    /// <summary>
    /// Configurations Controller
    /// </summary>
    public partial class ConfigurationsController : BaseController
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets the l2 by identifier.
        /// </summary>
        /// <param name="projectId">The project identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetL2ByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetL2ByID(int projectId)
        {
            L2 objL2 = new L2();
            Sector objSector = new Sector();
            if (projectId > 0)
            {
                using (ServiceContext porjectService = new ServiceContext())
                {
                    objL2 = porjectService.SelectObject<L2>(projectId);
                    objSector = porjectService.SelectObject<Sector>(Convert.ToInt32(objL2.SectorID));
                    objL2.SectorCode = objSector.SectorCode;
                    objL2.SectorName = objSector.SectorName;
                }
            }

            return Json(objL2, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the L2S.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetL2s)]
        public ActionResult GetL2s()
        {
            L2 objL2 = new L2();
            using (ServiceContext objL2Service = new ServiceContext())
            {
                var result = objL2Service.Search(objL2, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the currencies.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetCurrencies)]
        public ActionResult GetCurrencies()
        {
            Currency objCurrency = new Currency();
            using (ServiceContext objCurrencyService = new ServiceContext())
            {
                var result = objCurrencyService.Search(objCurrency, null, null, null);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        
        /// <summary>
        /// Gets the Months
        /// </summary>
        /// <returns></returns>
        [ActionName("GetMonths")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetMonths()
        {
            IList<SelectListItem> lst = Common.GetStartMonths();
            lst.RemoveAt(0);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the Years
        /// </summary>
        /// <returns></returns>
        [ActionName("GetYears")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetYears()
        {
            IList<SelectListItem> lst = Common.GetYearDropDown();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
    }
}