﻿using System.Web.Mvc;

/// <summary>
/// Configuration Area Registration
/// </summary>
namespace CMMS.Areas.Configurations
{
    /// <summary>
    /// Configuration Area Registration
    /// </summary>
    public class ConfigurationsAreaRegistration : AreaRegistration 
    {
        /// <summary>
        /// Area Name
        /// </summary>
        public override string AreaName 
        {
            get 
            {
                return Pages.Areas.Configurations;
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
            name: "Configurations_default",
            url: Pages.Areas.Configurations + "/{action}",
            defaults: new { controller = Pages.Controllers.Configurations, action = "Index" });

            context.MapRoute(
                name: "Configurations",
                url: Pages.Areas.Configurations + "/{controller}/{action}/{id}",
                defaults: new { controller = Pages.Controllers.Configurations, action = "Index", id = UrlParameter.Optional });
        }
    }
}