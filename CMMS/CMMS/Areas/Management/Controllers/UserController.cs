﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.IO;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Services;
using Newtonsoft.Json;
using CMMS.Service;
using CMMS.Service.UserAuthentication;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// User Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        #region "Employee"
        /// <summary>
        /// Gets all the users in system.
        /// </summary>
        /// <param name="frmTransaction">The From Transaction</param>
        /// <returns></returns>
        [ActionName(Actions.UserList)]
        public ActionResult UsersList(int frmTransaction = 0)
        {
            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];
            ViewBag.IsFromTranscation = frmTransaction;
            ////if ((frmTransaction == 0 && ProjectSession.PermissionAccess.Management_User_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            ////    || (frmTransaction == 1 && ProjectSession.PermissionAccess.Employee_Employee_Allowaccess && ProjectSession.PermissionAccess.Employee__ShowHideModule))
            ////{
            if (ProjectSession.PermissionAccess.Management_User_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.UserList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// saves employee data in database
        /// </summary>
        /// <param name="objEmp">Object Of Employee Model</param>
        /// <returns></returns>
        [ActionName(Actions.SaveEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveEmployee(employees objEmp)
        {
            int result = 0;
            string directoryName = ProjectConfiguration.UploadPath + "UserImages";
            string fileName = string.Empty;
            string oldFileName = string.Empty;
            string validations = string.Empty;
            bool createRecords = false;
            bool editRecords = false;

            try
            {
                if (ModelState.IsValid)
                {
                    Account objAccount = UserAuthenticationData.GetAccount(ProjectSession.AccountID);
                    if (Convert.ToBoolean(objAccount.IsNamedUser))
                    {
                        using (DapperContext context = new DapperContext())
                        {
                            int totalEmployees = EmployeeService.GetAllEmployees().Where(x => x.EmployeeStatus == SystemEnum.employeeStatus.Active.ToString() && x.EmployeeID != objEmp.EmployeeID).Count();
                            if (objEmp.EmployeeStatusId == SystemEnum.employeeStatus.Active.GetHashCode())
                            {
                                if (totalEmployees >= objAccount.NoOfLicenses)
                                {
                                    if (objEmp.EmployeeID > 0)
                                    {
                                        TempData["Message"] = ProjectSession.Resources.message.Employee_MsgLicensesExceeded;
                                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                                        return RedirectToAction(Actions.GetEmployeeByID, Pages.Controllers.Management, new { id = objEmp.EmployeeID, frm = objEmp.isfrmTransaction });
                                    }
                                    else
                                    {
                                        ViewBag.Message = ProjectSession.Resources.message.Employee_MsgLicensesExceeded;
                                        ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                                        return View(Views.Employee, objEmp);
                                    }
                                }
                            }
                        }
                    }

                    if (objEmp.isfrmTransaction == 0)
                    {
                        createRecords = ProjectSession.PermissionAccess.Management_User_Createnewrecords;
                        editRecords = ProjectSession.PermissionAccess.Management_User_Editrecords;
                    }
                    else
                    {
                        createRecords = ProjectSession.PermissionAccess.Employee_Employee_Createnewrecords;
                        editRecords = ProjectSession.PermissionAccess.Employee_Employee_Editrecords;
                    }

                    employees objemployee = new employees();

                    IList<employees> lstEmployees = new List<employees>();
                    using (DapperContext objDapper = new DapperContext())
                    {
                        lstEmployees = objDapper.SearchAll<employees>(objemployee);
                    }

                    if (objEmp.UserID != null)
                    {
                        if (lstEmployees.Any(x => !string.IsNullOrEmpty(x.UserID) && x.UserID.Trim() == objEmp.UserID.Trim() && x.EmployeeID != objEmp.EmployeeID))
                        {
                            if (objEmp.EmployeeID > 0)
                            {
                                TempData["Message"] = ProjectSession.Resources.message.Employee_MsgUserIdAlreadyExist;
                                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            }
                            else
                            {
                                ViewBag.Message = ProjectSession.Resources.message.Employee_MsgUserIdAlreadyExist;
                                ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                                return View(Views.Employee, objEmp);
                            }
                        }
                    }

                    if (lstEmployees.Any(x => !string.IsNullOrEmpty(x.EmployeeNO) && Convert.ToString(x.EmployeeNO).Trim() == Convert.ToString(objEmp.EmployeeNO).Trim() && x.EmployeeID != objEmp.EmployeeID))
                    {
                        if (objEmp.EmployeeID > 0)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Employee_MsgEmpNoAlreadyExist;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                        }
                        else
                        {
                            ViewBag.Message = ProjectSession.Resources.message.Employee_MsgEmpNoAlreadyExist;
                            ViewBag.MessageType = SystemEnum.MessageType.danger.ToString();
                            return View(Views.Employee, objEmp);
                        }
                    }

                    if (Convert.ToString(TempData["Message"]) == string.Empty && Convert.ToString(TempData["MessageType"]) == string.Empty)
                    {
                        if (objEmp.EmployeeID > 0)
                        {
                            objEmp.ModifiedBy = ProjectSession.EmployeeID.ToString();
                            objEmp.ModifiedByName = ProjectSession.EmployeeName;
                            objEmp.ModifiedDate = DateTime.Now;                            
                        }
                        else
                        {
                            objEmp.CreatedBy = ProjectSession.EmployeeID.ToString();
                            objEmp.CreatedByName = ProjectSession.EmployeeName;
                            objEmp.CreatedDate = DateTime.Now;                            
                        }

                        if (!ProjectSession.PermissionAccess.Employee_Employee_ChangeMastersCodes && objEmp.EmployeeID > 0)
                        {
                            objEmp.EmployeeNO = null;

                            /*We Do not update null values to database that's why employee no is assigned null*/
                        }

                        using (DapperContext objContext = new DapperContext())
                        {
                            if (objEmp.EmployeeID > 0)
                            {
                                if (!editRecords)
                                {
                                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                if (!createRecords)
                                {
                                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            result = objContext.Save<employees>(objEmp);
                            try
                            {
                                if (objEmp.OldImagePath.Length > 0 && !string.IsNullOrEmpty(objEmp.OldImagePath))
                                {
                                    IOManager.DeleteFile(ProjectConfiguration.UploadPath + "UserImages\\" + Convert.ToString(objEmp.OldImagePath).Trim());
                                }
                            }
                            catch
                            {
                            }

                            if ((objEmp.EmployeeID == 0) && (result > 0) &&  (Convert.ToInt32(objEmp.UserGroupId) > 0) )
                            {
                                EmployeeService objServiceemp = new EmployeeService();
                                objServiceemp.CopyUserGroupPermissionsToEmployee(result, (Convert.ToInt32(objEmp.UserGroupId)));
                            }

                        }
                    }
                }
                else
                {
                    foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                    {
                        validations = validations + modelError.ErrorMessage + " ,";
                        TempData["Message"] = validations.TrimEnd(',');
                        TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                    }
                }

                if (result > 0)
                {
                    if (objEmp.EmployeeID == ProjectSession.EmployeeID)
                    {
                        ProjectSession.UserImage = objEmp.ImagePath;
                    }

                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                    return RedirectToAction(Actions.GetEmployeeByID, Pages.Controllers.Management, new { id = result, frm = objEmp.isfrmTransaction });
                }
                else
                {
                    return RedirectToAction(Actions.GetEmployeeByID, Pages.Controllers.Management, new { id = objEmp.EmployeeID, frm = objEmp.isfrmTransaction });
                }
            }
            catch
            {
                if (Convert.ToString(TempData["Message"]) == string.Empty)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                }

                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                return RedirectToAction(Actions.GetEmployeeByID, Pages.Controllers.Management, new { id = objEmp.EmployeeID, frm = objEmp.isfrmTransaction });
            }
        }

        /// <summary>
        ///  Changes the Employee photo.
        /// </summary>
        /// <param name="cropPointX">The crop point x.</param>
        /// <param name="cropPointY">The crop point y.</param>
        /// <param name="imageCropWidth">Width of the image crop.</param>
        /// <param name="imageCropHeight">Height of the image crop.</param>
        /// <param name="oldFileName">Old File Name</param>
        /// <returns></returns>
        [ActionName(Actions.ChangeEmployeePhoto)]
        [HttpPost]
        public ActionResult ChangeEmployeePhoto(int? cropPointX, int? cropPointY, int? imageCropWidth, int? imageCropHeight, string oldFileName)
        {
            HttpPostedFileBase employeePhoto = Request.Files["UploadedFile"];
            int allowedMaxSizeinMB = 1;

            Account objAccount = UserAuthenticationData.GetAccount(ProjectSession.AccountID);
            string[] allowedExt = new string[] { ".jpeg", ".jpg", ".gif", ".png", ".bmp", ".psd" };
            string[] allowedExtFromMaster = ProjectSession.AllowedFileTypes.ToLower().Split(',');
            int allowedMaxSizeinMBMaster = ProjectSession.AllowedMaxFilesize;
            if (employeePhoto.ContentLength > 0 && !allowedExt.Contains(employeePhoto.FileName.ToLower().Substring(employeePhoto.FileName.LastIndexOf('.'))))
            {                
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });                
            }
            else if (!allowedExtFromMaster.Contains(employeePhoto.FileName.ToLower().Substring(employeePhoto.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });                
            }

            if ((employeePhoto.ContentLength / 1024) / 1024 > allowedMaxSizeinMB)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadSizeImage, string.Empty });
            }

            string photoName = employeePhoto.FileName;
            oldFileName = Path.GetFileName(oldFileName);
            string filename = string.Empty;
            byte[] imageBytes = ConvertTo.Bytes(employeePhoto);
            byte[] croppedImage = IOManager.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            try
            {
                if (photoName.Length > 0)
                {
                    filename = System.Guid.NewGuid().ToString() + Path.GetExtension(photoName);
                    filename = IOManager.SaveFile(croppedImage, ProjectConfiguration.UploadPath + "UserImages\\", filename);
                }
            }
            catch
            {
            }

            return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Photo Uploaded Successfully.", filename, oldFileName });
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="employeeName">The employeeName.</param>
        /// <param name="employeeNo">The employeeNo.</param>
        /// <returns></returns>
        [ActionName(Actions.GetUserList)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetUserList([DataSourceRequest]DataSourceRequest request, string employeeName, string employeeNo)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            /*(Start)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and employees.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            /*(End)Added By Pratik Telaviya on 23-jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            employees objEmp = new employees();
            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objEmp, SystemEnum.Pages.Employee.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Get Employee by Id
        /// </summary>
        /// <param name="id">Employee ID</param>
        /// <param name="frm">To Check If Page is Opened From Transaction or Management</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeByID)]
        [HttpGet]
        public ActionResult GetEmployeeByID(int id, int frm = 0)
        {
            if (id == 0)
            {
                Account objAccount = UserAuthenticationData.GetAccount(ProjectSession.AccountID);
                if (Convert.ToBoolean(objAccount.IsNamedUser))
                {
                    using (DapperContext context = new DapperContext())
                    {
                        ////employees objemployees = new employees();
                        ////int totalEmployees = context.SearchAll<employees>(objemployees).Count;

                        int totalEmployees = EmployeeService.GetAllEmployees().Where(x => x.EmployeeStatus == SystemEnum.employeeStatus.Active.ToString()).Count();

                        if (totalEmployees >= objAccount.NoOfLicenses)
                        {
                            TempData["Message"] = ProjectSession.Resources.message.Employee_MsgLicensesExceeded;
                            TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                            return RedirectToAction(Actions.UserList, Pages.Controllers.Management, new { frmTransaction = frm });
                        }
                    }
                }
            }

            ViewBag.Message = TempData["Message"];
            ViewBag.MessageType = TempData["MessageType"];

            employees objEmp = new employees();
            if (id > 0)
            {
                objEmp.EmployeeID = id;
                using (DapperContext objContext = new DapperContext())
                {
                    objEmp = objContext.SearchAllByID<employees>(objEmp, id).FirstOrDefault();
                }
            }
            else
            {
                // objEmp.ImagePath = "../Uploads/UserImages/default_user.jpg";
                objEmp.LanguageCode = "ar-sa";
                objEmp.CategoryID = 1;
                objEmp.EmployeeStatusId = SystemEnum.employeeStatus.Active.GetHashCode();
                objEmp.POAuthorisationLimit = 0;
            }

            ////Check If Page is Opened From Transaction 
            objEmp.isfrmTransaction = frm;

            return View(Views.Employee, objEmp);
        }

        /// <summary>
        /// Gets the maintenance sub department by department id
        /// </summary>
        /// <param name="maintDeptid">Maintenance Department ID</param>
        /// <returns>List Of Sub Departments for the selected Department</returns>
        [ActionName(Actions.GetMaintSubDeptByDeptID)]
        public ActionResult GetMaintSubDeptByDeptID(int maintDeptid)
        {
            IList<MaintSubDept> list = new List<MaintSubDept>();

            try
            {
                list = DivisionService.GetMaintSubDivisionPermissionVise(maintDeptid);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To get All Employee Categories
        /// </summary>
        /// <returns>List Of employee categories without any filtering</returns>
        [ActionName(Actions.GetEmpProfession)]
        public ActionResult GetEmpProfession()
        {
            EmployeeProfession objEmpProfession = new EmployeeProfession();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<EmployeeProfession>(objEmpProfession);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// To get All Languages
        /// </summary>
        /// <returns>List Of Languages without any filtering</returns>
        [ActionName(Actions.GetLanguage)]
        public ActionResult GetLanguage()
        {
            return Json(TranslationsService.GetLanguages(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To get All Nationalities
        /// </summary>
        /// <returns>List Of employee categories without any filtering</returns>
        [ActionName(Actions.GetNationalities)]
        public ActionResult GetNationalities()
        {
            CmNationality objNationality = new CmNationality();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<CmNationality>(objNationality);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the Employee.
        /// </summary>
        /// <param name="employeeId">The Employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteEmployee(int employeeId)
        {
            using (ServiceContext areaService = new ServiceContext())
            {
                int returnValue = areaService.Delete<employees>(employeeId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        /// <summary>
        /// Gets the list of all employees by assignments
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="cityID">City ID</param>
        /// <param name="type">The type.</param>
        /// <param name="strWhere">The string where.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeeAssignPage)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetEmployeeAssignPage([DataSourceRequest]DataSourceRequest request, int cityID = 0, int type = 0, string strWhere = "")
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            int empID = ProjectSession.EmployeeID;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmployeeService objService = new EmployeeService();

            if (type == 2)
            {
                string techCatID = Common.GetConfigKeyValue("TechnicianCategory");
                strWhere += " AND employees.CategoryID = " + (techCatID == string.Empty ? "0" : techCatID);
            }

            var result = new DataSourceResult();
            if (type == 1 || type == 2)
            {  ////isJO = true
                result = new DataSourceResult()
                {
                    Data = objService.GetEmployeeAssignPage(empID, cityID, pageNumber, strWhere, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            }
            else
            {
                result = new DataSourceResult()
                {
                    Data = objService.GetEmployeePage(ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID, pageNumber, strWhere, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };
            }

            return Json(result);
        }

        /// <summary>
        /// Gets the list of all employees by assignments
        /// </summary>
        /// <param name="request">Request Object</param>
        /// <param name="cityID">The City or Store ID</param>
        /// <param name="autoID">Auto ID</param>
        /// <param name="pageId">Page ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployeePage)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetEmployeePage([DataSourceRequest]DataSourceRequest request, int cityID, int autoID, int pageId)
        {
            string strWhere = string.Empty;
            if (pageId == SystemEnum.Pages.IRWorkFlow.GetHashCode())
            {
                if (autoID > 0)
                {
                    strWhere = @" and employees.UserID <> '' and employees.Password <> ''  and 
                                    employees.EmployeeID not in (select EmployeeID from IRApprovalLevelMappings_employees where AutoId in (select AutoId from IRApprovalLevelMappings where 
                                    SubStoreId =" + cityID + "and AutoId = " + autoID + "))";
                }
                else
                {
                    strWhere = @" and employees.UserID <> '' and employees.Password <> ''  and 
                                    employees.EmployeeID not in (select EmployeeID from IRApprovalLevelMappings_employees where AutoId in (select AutoId from IRApprovalLevelMappings where 
                                    SubStoreId =" + cityID + "))";
                }
            }
            else if (pageId == SystemEnum.Pages.PRWorkFlow.GetHashCode())
            {
                if (autoID > 0)
                {
                    strWhere = @" and employees.UserID <> '' and employees.Password <> ''  and 
                                    employees.EmployeeID not in (select EmployeeID from PRApprovalLevelMappings_employees where AutoId in (select AutoId from PRApprovalLevelMappings where 
                                    L2ID =" + cityID + "and AutoId = " + autoID + "))";
                }
                else
                {
                    strWhere = @" and employees.UserID <> '' and employees.Password <> ''  and 
                                    employees.EmployeeID not in (select EmployeeID from PRApprovalLevelMappings_employees where AutoId in (select AutoId from PRApprovalLevelMappings where 
                                    L2ID =" + cityID + "))";
                }
            }

            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmployeeService objService = new EmployeeService();

            var result = new DataSourceResult()
            {
                Data = objService.GetEmployeePage(ProjectSession.EmployeeID, pageNumber, strWhere, sortExpression, sortDirection),
                Total = objService.PagingInformation.TotalRecords
            };

            return Json(result);
        }

        /// <summary>
        /// Gets the employees.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetEmployees)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetEmployees([DataSourceRequest]DataSourceRequest request)
        {
            ////Location Module : Employee PopUp
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            EmployeeService objService = new EmployeeService();

            var result = new DataSourceResult()
                {
                    Data = objService.GetEmployees(pageNumber, sortExpression, sortDirection, request: request),
                    Total = objService.PagingInformation.TotalRecords
                };

            return Json(result);
        }

        /// <summary>
        /// Copy the User Group Permission to Employee.
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        /// <param name="userGroupID">The user group identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.CopyUserPermissionToEmployee)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CopyUserPermissionToEmployee(int employeeID, int userGroupID)
        {
            try
            {
                bool result = false;
                EmployeeService objService = new EmployeeService();

                result = objService.CopyUserGroupPermissionsToEmployee(employeeID, userGroupID);

                if (result)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Employee Documents"

        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetEmployeeDocuments)]
        public ActionResult _PartialEmpoyeeDocuments()
        {
            EmployeeDocuments model = new EmployeeDocuments();
            return View(PartialViews.EmployeeDocuments, model);
        }

        /// <summary>
        /// Gets Documents List By Employee.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="kendogriddata">The grid data</param>
        /// <param name="employeeID">The employeeID.</param>
        /// <param name="dbcall">Decide If fetch data from Database or not</param>
        /// <returns></returns>
        [ActionName(Actions.GetDocumentsByEmployee)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetDocumentsByEmployee([DataSourceRequest]DataSourceRequest request, string kendogriddata, int employeeID, bool dbcall = false)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = 1;
            ProjectSession.PageSize = 5000;

            List<EmployeeDocuments> lstEmployeeDocuments = new List<EmployeeDocuments>();
            lstEmployeeDocuments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeDocuments>>(kendogriddata);

            var result = new DataSourceResult();
            if (dbcall)
            {
                Cmemployeedocument objEmpDocs = new Cmemployeedocument();

                if (employeeID > 0)
                {
                    objEmpDocs.EmployeeID = employeeID;
                }

                List<EmployeeDocuments> lst = null;
                EmployeeService objEmpService = new EmployeeService();
                lst = objEmpService.GetDocumentsByEmployee(objEmpDocs, pageNumber, sortExpression, sortDirection);
                result = new DataSourceResult()
               {
                   Data = lst,
                   Total = 1
               };
            }
            else if (TempData["GetEmpoyeeDocumentListdata"] != null)
            {
                lstEmployeeDocuments = (List<EmployeeDocuments>)TempData["GetEmpoyeeDocumentListdata"];
                result = new DataSourceResult()
                {
                    Data = lstEmployeeDocuments,
                    Total = 1
                };
            }

            // return JsonConvert.SerializeObject(result);
            return Json(result);
        }

        /// <summary>
        /// Get EmployeeDocument
        /// </summary>
        /// <param name="autoGuid">Auto GUID</param>
        /// <param name="kendogriddata">Grid Data</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetEmpoyeeDocumentDetails(string autoGuid, string kendogriddata)
        {
            List<EmployeeDocuments> lstEmployeeDocuments = new List<EmployeeDocuments>();
            lstEmployeeDocuments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeDocuments>>(kendogriddata);
            EmployeeDocuments model = new EmployeeDocuments();
            if (!string.IsNullOrEmpty(autoGuid))
            {
                Guid id = new Guid(autoGuid);
                model = lstEmployeeDocuments.FirstOrDefault(l => l.Guid_DocID == id);
            }

            TempData["GetEmpoyeeDocumentListdata"] = lstEmployeeDocuments;
            return Json(model);
        }

        /// <summary>
        /// Add Edit EmployeeDocument
        /// </summary>
        /// <param name="objModel">Model Data</param>
        /// <param name="kendoDocgriddata">Document Grid Data</param>
        /// <param name="kendoAttachmentgriddata">Attachment Grid Data</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult _PartialEmpoyeeDocuments(EmployeeDocuments objModel, string kendoDocgriddata, string kendoAttachmentgriddata)
        {
            List<HttpPostedFileBase> files = new List<HttpPostedFileBase>();
            if (!string.IsNullOrEmpty(objModel.employeedocs.DocName) && objModel.employeedocs.Doc_ExpiryDate.Value != DateTime.MaxValue)
            {
                List<EmployeeDocuments> lstEmployeeDocuments = new List<EmployeeDocuments>();
                lstEmployeeDocuments = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeDocuments>>(kendoDocgriddata);
                int index = 0;
                foreach (string file in Request.Files)
                {
                    if (("," + ProjectSession.AllowUploadFileFormats + ",").Contains("," + Path.GetExtension(Request.Files[index].FileName).ToLower() + ","))
                    {
                        bool flagValidation = true;
                        string[] allowedExtFromMaster = ProjectSession.AllowedFileTypes.ToLower().Split(',');
                        int allowedMaxSizeinMBMaster = ProjectSession.AllowedMaxFilesize;
                        if (!allowedExtFromMaster.Contains(Request.Files[file].FileName.ToLower().Substring(Request.Files[file].FileName.LastIndexOf('.'))))
                        {
                            flagValidation = false;
                            //return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgFileTypeNotAllowed, string.Empty });
                            return Json(1);
                        }

                        if ((Request.Files[file].ContentLength / 1024) / 1024 > allowedMaxSizeinMBMaster)
                        {
                            flagValidation = false;
                            return Json(2);
                            //return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgFileSizeNotAllowed + allowedMaxSizeinMBMaster + "MB" , string.Empty });
                        }

                        if(flagValidation)                        
                            files.Add(Request.Files[file]);
                    }

                    index += 1;
                }

                HomeController objHomeCntrl = new HomeController();
                objHomeCntrl.SaveUploadedFiles(objModel, files, SystemEnum.ModuleType.Employee.ToString());

                if (objModel.Guid_DocID == new Guid())
                {
                    objModel.Guid_DocID = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(kendoAttachmentgriddata))
                    {
                        objModel.lstAttachment.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsextassetfiles>>(kendoAttachmentgriddata));
                    }

                    lstEmployeeDocuments.Add(objModel);
                }
                else
                {
                    EmployeeDocuments editEmployeeDocuments = lstEmployeeDocuments.FirstOrDefault(l => l.Guid_DocID == objModel.Guid_DocID);
                    objModel.employeedocs.DocID = editEmployeeDocuments.employeedocs.DocID; // add for edit
                    int editIndex = lstEmployeeDocuments.IndexOf(editEmployeeDocuments);

                    if (!string.IsNullOrEmpty(kendoAttachmentgriddata))
                    {
                        objModel.lstAttachment.AddRange(Newtonsoft.Json.JsonConvert.DeserializeObject<List<clsextassetfiles>>(kendoAttachmentgriddata));
                    }

                    lstEmployeeDocuments.RemoveAt(editIndex);
                    lstEmployeeDocuments.Insert(editIndex, objModel);
                }

                TempData["GetEmpoyeeDocumentListdata"] = lstEmployeeDocuments;
                return Json(lstEmployeeDocuments);
            }
            else
            {
                return Json(0);
            }
        }

        /// <summary>
        /// saves employee data in database
        /// </summary>
        /// <param name="employeeID">The employee identifier.</param>
        /// <param name="kendogriddata">The kendo grid data.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveEmployeeDocuments)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveEmployeeDocuments(int employeeID, string kendogriddata)
        {
            int result = -1;

            try
            {
                employees objEmp = new employees();
                EmployeeService objService = new EmployeeService();
                objEmp.JsonlstEmployeeDocumentModel = kendogriddata;

                if (!string.IsNullOrEmpty(kendogriddata))
                {
                    objEmp.lstEmployeeDocumentModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeDocuments>>(objEmp.JsonlstEmployeeDocumentModel);
                }

                objEmp.EmployeeID = employeeID;
                objEmp.ModifiedBy = ProjectSession.EmployeeID.ToString();
                objEmp.ModifiedByName = ProjectSession.EmployeeName;
                objEmp.ModifiedDate = DateTime.Now;

                if (objEmp.EmployeeID > 0)
                {
                    result = objService.UpdateEmployeeDocuments(objEmp);
                }

                if (result == 1)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Deletes the Attachment.
        /// </summary>
        /// <param name="attachmentID">The Attachment identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAttachment)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAttachment(string attachmentID)
        {
            if (!string.IsNullOrEmpty(attachmentID) && attachmentID != "0")
            {
                string filePath = string.Empty;
                int attID = Convert.ToInt32(attachmentID);
                int returnValue = 0;
                CmAttachment obj = new CmAttachment();

                using (DapperContext contextAttachment = new DapperContext())
                {
                    obj = contextAttachment.SelectObject<CmAttachment>(attID);

                    returnValue = contextAttachment.Delete<CmAttachment>(attID);
                }

                if (returnValue == 0)
                {
                    filePath = ProjectConfiguration.UploadPath + obj.FileLink;
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);
                    }

                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Download Attachments 
        /// </summary>
        /// <param name="attachmentName">Attachment Name</param>
        /// <returns></returns>
        [ActionName(Actions.DownloadEmpAttachment)]
        public FileResult DownloadEmployeeDocAttachment(string attachmentName)
        {
            try
            {
                if (!string.IsNullOrEmpty(attachmentName))
                {
                    string fileName = attachmentName;
                    string downloadName = fileName;
                    string filePath = ProjectConfiguration.UploadPath + "Attachments/" + fileName;
                    string extension = Path.GetExtension(filePath);
                    string contentType = "application/" + extension;
                    return File(filePath, contentType, downloadName);
                }
            }
            catch (FileNotFoundException fex)
            {
                throw new Exception("The file could not be found.");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        /// <summary>
        /// Delete Employee Document by AutoGuid
        /// </summary>
        /// <param name="autoGuid">Document Row Identifier</param>
        /// <param name="kendogriddata">Grid Data</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.DeleteEmpDocument)]
        public JsonResult DeleteEmpoyeeDocument(string autoGuid, string kendogriddata)
        {
            try
            {
                List<EmployeeDocuments> lstEmployeeDocumentModel = new List<EmployeeDocuments>();
                lstEmployeeDocumentModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<EmployeeDocuments>>(kendogriddata);
                EmployeeDocuments model = new EmployeeDocuments();
                int result = 0;
                if (!string.IsNullOrEmpty(autoGuid))
                {
                    Guid id = new Guid(autoGuid);
                    EmployeeDocuments editEmployeeDocument = lstEmployeeDocumentModel.FirstOrDefault(l => l.Guid_DocID == id);
                    if (editEmployeeDocument.employeedocs.DocID != 0)
                    {
                        EmployeeService objService = new EmployeeService();
                        result = objService.DeleteEmployeeDocumentByDocId(editEmployeeDocument.employeedocs.DocID);
                    }

                    if (result == 0)
                    {
                        int editIndex = lstEmployeeDocumentModel.IndexOf(editEmployeeDocument);
                        lstEmployeeDocumentModel.RemoveAt(editIndex);
                        TempData["GetEmpoyeeDocumentListdata"] = lstEmployeeDocumentModel;
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                    }
                    else if (result == -2)
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgEntityHasRelationShip }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(string.Empty);
                }
            }
            catch
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Return if file extension is image extension or not
        /// </summary>
        /// <param name="ext">file extension</param>
        /// <returns>boolean value</returns>
        public bool IsImageExtension(string ext)
        {
            return ProjectConfiguration._validExtensions.Contains(ext);
        }

        #endregion

        #region "Login Info Tab"

        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetEmployeeLoginPage)]
        public ActionResult GetEmployeeLoginPage(employees model)
        {
            return View(PartialViews.EmployeeLogin, model);
        }

        /// <summary>
        /// saves employee data in database
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveEmployeeLogin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveEmployeeLogin(employees model)
        {
            bool result = false;
            int employeeID = model.EmployeeID;
            string userID = model.UserID;
            string passWord = model.Password;
            int tabTransaction = Convert.ToInt32(model.TBtransaction);
            int tabSetup = Convert.ToInt32(model.TBSetup);
            int tabReport = Convert.ToInt32(model.TBReport);
            int tabDashboard = Convert.ToInt32(model.TBDashboard);
            int central = Convert.ToInt32(model.Central);
            int superwiser = Convert.ToInt32(model.IsStoredSupervisor);

            try
            {
                EmployeeService objService = new EmployeeService();
                employees objemployee = new employees();
                IList<employees> lstEmployees = new List<employees>();
                using (DapperContext objDapper = new DapperContext())
                {
                    lstEmployees = objDapper.SearchAll<employees>(objemployee);
                }

                if (model.UserID != null)
                {
                    if (lstEmployees.Any(x => !string.IsNullOrEmpty(x.UserID) && x.UserID.Trim() == model.UserID.Trim() && x.EmployeeID != model.EmployeeID))
                    {
                        return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Employee_MsgUserIdAlreadyExist }, JsonRequestBehavior.AllowGet);
                    }
                }

                result = objService.UpdateEmployeeLogin(employeeID, userID, passWord, tabTransaction, tabSetup, tabReport, tabDashboard, model.POAuthorisationLimit, central, superwiser);

                if (result)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region "City Permission Tab"

        /// <summary>
        /// Gets all the users in system.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetCityPermissions)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetCityPermissions([DataSourceRequest]DataSourceRequest request, int empID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            IList<Employees_L2> lstCityPermissions = new List<Employees_L2>();
            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }

            int pageNumber = 0;
            ProjectSession.PageSize = request.PageSize;

            employees objEmp = new employees();
            EmployeeService objService = new EmployeeService();
            lstCityPermissions = objService.GetCitiesWithCheckBox(empID, pageNumber, sortExpression, sortDirection, request);

            var result = new DataSourceResult()
            {
                Data = lstCityPermissions,
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// To Save City Permission Data
        /// </summary>
        /// <param name="lstChangedObjs">serialized grid data</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SaveL2Permissions)]
        [ValidateAntiForgeryToken]
        public ActionResult SaveL2Permissions(string lstChangedObjs)
        {
            try
            {
                List<Employees_L2> lstEmployeePermissionModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employees_L2>>(lstChangedObjs);
                EmployeeService objService = new EmployeeService();
                Employees_L2 objEmpL2 = new Employees_L2();
                int createdBy = ProjectSession.EmployeeID;

                if (lstEmployeePermissionModel != null && lstEmployeePermissionModel.Count > 0)
                {
                    lstEmployeePermissionModel.ForEach(p =>
                    {
                        if (p.IsInserted == false)
                        {
                            objService.DeleteEmployeeL2(p.EmpID, p.L2ID);
                        }
                        else if (p.IsInserted == true)
                        {
                            p.CreatedBy = createdBy.ToString();
                            p.CreatedDate = DateTime.Now;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                objService.InsertEmployeeL2(p);
                            }
                        }
                    });
                }

                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        /// <summary>
        /// To Save City Permission Data
        /// </summary>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="lstChangedOrgs">The List of changed Organizations.</param>
        /// <param name="lstChangedCity">The List of changed Cities.</param>
        /// <param name="lstChangedAreas">The List of changed Areas.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Some Error Occurred while Saving the records.
        /// or
        /// Some Error Occurred while Saving the records.
        /// </exception>
        [HttpPost]
        [ActionName(Actions.SaveLevelWisePermissions)]
        [ValidateAntiForgeryToken]
        public ActionResult SaveLevelWisePermissions(int empID, string lstChangedOrgs, string lstChangedCity, string lstChangedAreas)
        {
            try
            {
                List<Employees_L2> lstCities = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employees_L2>>(lstChangedCity);
                List<Employees_L3> lstAreas = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employees_L3>>(lstChangedAreas);
                List<int> lstrOrgs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstChangedOrgs);
                EmployeeService objService = new EmployeeService();
                List<Employees_L2> lstInsertEmpL2 = new List<Employees_L2>();
                List<Employees_L2> lstDeleteEmpL2 = new List<Employees_L2>();
                List<Employees_L3> lstInsertEmpL3 = new List<Employees_L3>();
                List<Employees_L3> lstDeleteEmpL3 = new List<Employees_L3>();
                int createdBy = ProjectSession.EmployeeID;
                bool result = true;

                if (lstChangedCity != null && lstCities.Count > 0)
                {
                    lstCities.ForEach(p =>
                    {
                        if (p.IsInserted == false)
                        {
                            lstDeleteEmpL2.Add(p);
                        }
                        else if (p.IsInserted == true)
                        {
                            p.CreatedBy = createdBy.ToString();
                            p.CreatedDate = DateTime.Now;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                lstInsertEmpL2.Add(p);
                            }
                        }
                    });
                }

                if (lstDeleteEmpL2 != null && lstDeleteEmpL2.Count > 0)
                {
                    result = objService.DeleteEmployee_L2ByList(lstDeleteEmpL2, empID);
                }

                if (lstInsertEmpL2 != null && lstInsertEmpL2.Count > 0 && result == true)
                {
                    result = objService.InsertEmployee_L2ByList(lstInsertEmpL2);
                }

                if (result)
                {
                    if (lstChangedAreas != null && lstAreas.Count > 0)
                    {
                        lstAreas.ForEach(p =>
                        {
                            if (p.IsInserted == false)
                            {
                                lstDeleteEmpL3.Add(p);
                            }
                            else if (p.IsInserted == true)
                            {
                                p.CreatedBy = createdBy.ToString();
                                p.CreatedDate = DateTime.Now;

                                using (ServiceContext objContext = new ServiceContext())
                                {
                                    lstInsertEmpL3.Add(p);
                                }
                            }
                        });
                    }

                    if (lstDeleteEmpL3 != null && lstDeleteEmpL3.Count > 0)
                    {
                        result = objService.DeleteEmployee_L3ByList(lstDeleteEmpL3, empID);
                    }

                    ////if (lstInsertEmpL3 != null && lstInsertEmpL3.Count > 0 && result == true)
                    ////{
                    result = objService.InsertEmployee_L3ByList(lstInsertEmpL3, lstInsertEmpL2);
                    ////}

                    if (result)
                    {
                        result = objService.InsertEmployee_L2L3ByL1(lstrOrgs, empID);
                    }

                    ////result = objService.InsertEmployee_MaintSubDeptByMaintDivAndDept(lstMaintenanceDept, lstrOrgs, empID);

                    if (result)
                    {
                        return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        throw new Exception("Some Error Occurred while Saving the records by Divisions and Departments.");
                    }
                }
                else
                {
                    throw new Exception("Some Error Occurred while Saving the records by query.");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
        #endregion

        #region "Department Permissions Tab"

        /// <summary>
        /// To Save City Permission Data
        /// </summary>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="lstChangedDivs">The List of changed divisions.</param>
        /// <param name="lstChangedDepts">The List of changed departments.</param>
        /// <param name="lstChangedSubDepts">The List of changed sub departments.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Some Error Occurred while Saving the records.
        /// or
        /// Some Error Occurred while Saving the records.
        /// </exception>
        [HttpPost]
        [ActionName(Actions.SaveSubDeptPermissions)]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubDeptPermissions(int empID, string lstChangedDivs, string lstChangedDepts, string lstChangedSubDepts)
        {
            try
            {
                List<Employees_MaintSubDept> lstEmployeePermissionModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employees_MaintSubDept>>(lstChangedSubDepts);
                List<int> lstMaintenanceDept = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstChangedDepts);
                List<int> lstMaintenanceDivs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<int>>(lstChangedDivs);
                EmployeeService objService = new EmployeeService();
                List<Employees_MaintSubDept> lstInsertEmpMaintDept = new List<Employees_MaintSubDept>();
                List<Employees_MaintSubDept> lstDeleteEmpMaintDept = new List<Employees_MaintSubDept>();
                int createdBy = ProjectSession.EmployeeID;
                bool result = true;

                if (lstChangedSubDepts != null && lstEmployeePermissionModel.Count > 0)
                {
                    lstEmployeePermissionModel.ForEach(p =>
                    {
                        if (p.IsInserted == false)
                        {
                            lstDeleteEmpMaintDept.Add(p);
                        }
                        else if (p.IsInserted == true)
                        {
                            p.CreatedBy = createdBy.ToString();
                            p.CreatedDate = DateTime.Now;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                lstInsertEmpMaintDept.Add(p);
                            }
                        }
                    });
                }

                if (lstDeleteEmpMaintDept != null && lstDeleteEmpMaintDept.Count > 0)
                {
                    result = objService.DeleteEmployee_MaintSubDeptByList(lstDeleteEmpMaintDept, empID);
                }

                if (lstInsertEmpMaintDept != null && lstInsertEmpMaintDept.Count > 0 && result == true)
                {
                    result = objService.InsertEmployee_MaintSubDeptByList(lstInsertEmpMaintDept);
                }

                if (result)
                {
                    result = objService.InsertEmployee_MaintSubDeptByMaintDivAndDept(lstMaintenanceDept, lstMaintenanceDivs, empID, ProjectSession.IsCentral ? 0 : ProjectSession.EmployeeID);

                    if (result)
                    {
                        return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        throw new Exception("Some Error Occurred while Saving the records by Divisions and Departments.");
                    }
                }
                else
                {
                    throw new Exception("Some Error Occurred while Saving the records by query.");
                }
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }

        #endregion

        #region "Location Permission Tab"

        /// <summary>
        /// Gets all the users in system.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="empID">The employee identifier.</param>
        /// <param name="cityID">The city identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLocationPermissions)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult GetLocationPermissions([DataSourceRequest]DataSourceRequest request, int empID, int cityID)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            IList<Employees_Location> lstCityPermissions = new List<Employees_Location>();
            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            
            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            employees objEmp = new employees();
            EmployeeService objService = new EmployeeService();
            lstCityPermissions = objService.GetLocationsCheckBox(empID, cityID, pageNumber, sortExpression, sortDirection, request);

            var result = new DataSourceResult()
            {
                Data = lstCityPermissions,
                Total = objService.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// To Save City Permission Data
        /// </summary>
        /// <param name="lstChangedObjs">serialized grid data</param>
        /// <param name="empID">The employee identifier.</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SaveLocationPermissions)]
        [ValidateAntiForgeryToken]
        public ActionResult SaveLocationPermissions(string lstChangedObjs, int empID)
        {
            try
            {
                List<Employees_Location> lstEmployeePermissionModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Employees_Location>>(lstChangedObjs);
                EmployeeService objService = new EmployeeService();
                Employees_Location objEmpL2 = new Employees_Location();
                List<Employees_Location> lstInsertEmpLocation = new List<Employees_Location>();
                List<Employees_Location> lstDeleteEmpLocation = new List<Employees_Location>();
                int createdBy = ProjectSession.EmployeeID;
                bool result = true;

                if (lstEmployeePermissionModel != null && lstEmployeePermissionModel.Count > 0)
                {
                    lstEmployeePermissionModel.ForEach(p =>
                    {
                        if (p.IsInserted == false)
                        {
                            lstDeleteEmpLocation.Add(p);
                        }
                        else if (p.IsInserted == true)
                        {
                            p.CreatedBy = createdBy.ToString();
                            p.CreatedDate = DateTime.Now;

                            using (ServiceContext objContext = new ServiceContext())
                            {
                                lstInsertEmpLocation.Add(p);
                            }
                        }
                    });
                }

                if (lstDeleteEmpLocation != null && lstDeleteEmpLocation.Count > 0)
                {
                    result = objService.DeleteEmployee_LocationByList(lstDeleteEmpLocation, empID);
                }

                if (lstInsertEmpLocation != null && lstInsertEmpLocation.Count > 0 && result == true)
                {
                    result = objService.InsertEmployee_LocationByList(lstInsertEmpLocation);
                }

                return Json(new object[] { 1, SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
        #endregion
    }
}