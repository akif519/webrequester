﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// User Group Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Sectors the list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.UserGroupList)]
        [HttpGet]
        public ActionResult UserGroupList()
        {
            if (ProjectSession.PermissionAccess.Management_UserGroups_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.UserGroupList);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// To get All User Groups
        /// </summary>
        /// <returns>List Of employee categories without any filtering</returns>
        [ActionName(Actions.GetUserGroups)]
        public ActionResult GetUserGroups()
        {
            Usergroup objUserGroups = new Usergroup();
            using (DapperContext objContext = new DapperContext())
            {
                var result = objContext.SearchAll<Usergroup>(objUserGroups);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the area list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetUserGroupList)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetUserGroupList([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "GroupName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Usergroup userGroupSearch = new Usergroup();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(userGroupSearch, SystemEnum.Pages.UserGroup.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// To Delete User Group
        /// </summary>
        /// <param name="groupId">Group Id of the group to be deleted</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteUserGroupById)]
        public ActionResult DeleteUserGroupById(int groupId = 0)
        {
            int returnValue = 0;
            using (DapperContext objContext = new DapperContext())
            {
                returnValue = objContext.Delete<Usergroup>(groupId, true);
            }

            if (returnValue == 0)
            {
                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
            }
            else if (returnValue == -2)
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Saves the user group.
        /// </summary>
        /// <param name="userGroup">The instance of User Group.</param>
        /// <returns></returns>
        [ActionName(Actions.SaveUserGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult SaveUserGroup(Usergroup userGroup)
        {
            string validations = string.Empty;

            if (userGroup.UserGroupId > 0)
            {
                userGroup.ModifiedBy = ProjectSession.EmployeeID.ToString();
                userGroup.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Management_UserGroups_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                userGroup.CreatedBy = ProjectSession.EmployeeID.ToString();
                userGroup.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Management_UserGroups_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext objDapperContext = new DapperContext("GroupName"))
                {
                    userGroup.AltGroupName = userGroup.AltGroupName == null ? string.Empty : userGroup.AltGroupName;
                    if (objDapperContext.Save(userGroup) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.UserGroup_MsgUserGroupAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// To Get User Group By Id
        /// </summary>
        /// <param name="userGroupId">Group Id of the group to be fetched</param>
        /// <returns></returns>
        [ActionName(Actions.GetUserGroupById)]
        public ActionResult GetUserGroupById(int userGroupId = 0)
        {
            Usergroup objUserGroup = new Usergroup();
            if (userGroupId > 0)
            {
                using (DapperContext objDapperContext = new DapperContext())
                {
                    objUserGroup = objDapperContext.SelectObject<Usergroup>(userGroupId);
                }
            }

            return Json(objUserGroup, JsonRequestBehavior.AllowGet);
        }
    }
}