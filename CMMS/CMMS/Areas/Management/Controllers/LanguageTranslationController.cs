﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using CMMS.Service.ManagementService;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Language Translation Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Languages the translation.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.LanguageTranslation)]
        public ActionResult LanguageTranslation()
        {
            if (ProjectSession.PermissionAccess.Management_LanguageTranslations_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.LanguageTranslation);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the language translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLanguageTranslation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLanguageTranslation([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "ModuleName";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Translation objTranslation = new Translation();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(objTranslation, SystemEnum.Pages.LanguageTranslation.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Gets the language translation by identifier.
        /// </summary>
        /// <param name="translationId">The translation identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLanguageTranslationByID)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLanguageTranslationByID(int translationId)
        {
            Translation objTranslation = new Translation();
            if (translationId > 0)
            {
                using (ServiceContext costCenterService = new ServiceContext())
                {
                    objTranslation = costCenterService.SelectObject<Translation>(translationId);
                }
            }

            return Json(objTranslation, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Manages the language translation.
        /// </summary>
        /// <param name="objTranslation">The object translation.</param>
        /// <returns></returns>
        [ActionName(Actions.ManageLanguageTranslation)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public JsonResult ManageLanguageTranslation(Translation objTranslation)
        {
            string validations = string.Empty;

            if (objTranslation.TranslationID > 0)
            {
                objTranslation.ModifiedBy = ProjectSession.EmployeeID;
                objTranslation.ModifiedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Management_LanguageTranslations_Editrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotUpdate }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objTranslation.CreatedBy = ProjectSession.EmployeeID;
                objTranslation.CreatedDate = DateTime.Now;

                if (!ProjectSession.PermissionAccess.Management_LanguageTranslations_Createnewrecords)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgPermissionNotAdd }, JsonRequestBehavior.AllowGet);
                }
            }

            if (ModelState.IsValid)
            {
                using (DapperContext context = new DapperContext())
                {
                    if (context.Save(objTranslation) > 0)
                    {
                        return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
                    }
                    else
                    {
                        return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgRecordAlreadyExists });
                    }
                }
            }
            else
            {
                foreach (var modelError in ModelState.SelectMany(keyValuePair => keyValuePair.Value.Errors))
                {
                    validations = validations + modelError.ErrorMessage + " ,";
                }

                if (string.IsNullOrEmpty(validations))
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), validations.TrimEnd(',') });
                }
            }
        }

        /// <summary>
        /// _s the un translated word list.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions._UnTranslatedWordList)]
        public PartialViewResult _UnTranslatedWordList()
        {
            Tbl_LanguageSelection objLanguage = new Tbl_LanguageSelection();
            using (DapperContext context = new DapperContext())
            {
                var result = context.Search(objLanguage, null, null, null).Where(m => m.DefaultLanguage == true).FirstOrDefault();
                ViewBag.IsDefaultLanguage = result.Language;
            }

            return PartialView(Pages.PartialViews.UnTranslatedWordList);
        }

        /// <summary>
        /// Gets the language code.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.GetLanguageCode)]
        public ActionResult GetLanguageCode()
        {
            Tbl_LanguageSelection objLanguage = new Tbl_LanguageSelection();
            using (DapperContext context = new DapperContext())
            {
                var result = context.Search(objLanguage, null, null, null).Where(m => m.DefaultLanguage != true);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the un translated word list by code.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="languageCode">The language code.</param>
        /// <returns></returns>
        [ActionName(Actions.GetUnTranslatedWordListByCode)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetUnTranslatedWordListByCode([DataSourceRequest]DataSourceRequest request, string languageCode)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Labelname";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Untranslatedword objUntranslatedword = new Untranslatedword();

            if (languageCode == string.Empty)
            {
                languageCode = "ar-sa";
            }

            LanguageTranslationService obj = new LanguageTranslationService();
            var result = new DataSourceResult()
            {
                Data = obj.GetUnTranslatedWordByCode(languageCode, 0, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// News the translations.
        /// </summary>
        /// <param name="languageCode">The language code.</param>
        /// <param name="lstJsonTranslations">The LST translations.</param>
        /// <returns></returns>
        [ActionName(Actions.NewTranslations)]
        public ActionResult NewTranslations(string languageCode, string lstJsonTranslations)
        {
            try
            {
                List<Untranslatedword> lstTranslation = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Untranslatedword>>(lstJsonTranslations);

                lstTranslation = lstTranslation.Where(x => x.ToWord != null && x.ToWord.Trim() != string.Empty).ToList();
                for (int i = 0; i < lstTranslation.Count; i++)
                {
                    if (lstTranslation[i].ToWord != null && lstTranslation[i].ToWord.Trim() != string.Empty)
                    {
                        Translation objTranslation = new Translation();
                        objTranslation.Labelname = lstTranslation[i].Labelname;
                        objTranslation.FromLanguageCode = lstTranslation[i].LanguageCode;
                        objTranslation.FromWord = lstTranslation[i].Word;
                        objTranslation.ToLanguageCode = languageCode;
                        objTranslation.ModuleName = lstTranslation[i].ModuleName;
                        objTranslation.ToWord = lstTranslation[i].ToWord;
                        objTranslation.Type = lstTranslation[i].Type;
                        objTranslation.CreatedBy = ProjectSession.EmployeeID;
                        objTranslation.CreatedDate = DateTime.Now;

                        using (ServiceContext context = new ServiceContext())
                        {
                            int result = context.Save<Translation>(objTranslation);
                        }
                    }
                }

                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}