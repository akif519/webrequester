﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;
using Kendo.Mvc.UI;
using CMMS.Services;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Database Audit Trail Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Actives the session.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.DatabaseAuditTrail)]
        public ActionResult DatabaseAuditTrail()
        {
            if (ProjectSession.PermissionAccess.Management_AuditTrail_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.DatabaseAuditTrail);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// get name by property
        /// </summary>
        /// <param name="lstColumnName">list of column name</param>
        /// <param name="tableName">Table Name of property</param>
        /// <returns></returns>
        [ActionName(Actions.GetPropertyName)]
        public string GetPropertyNameList(string lstColumnName, string tableName)
        {
            string finalTranslate = string.Empty;
            List<string> columnNames = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(lstColumnName);
            List<string> translateNames = new List<string>();

            foreach (string property in columnNames)
            {
                string propertyCmp = property.ToLower();
                string propertyName = "AuditTrail_" + tableName.ToLower() + "_" + property.ToLower();
                try
                {
                    if (!(propertyCmp == "modifiedby" || propertyCmp == "modifieddate"))
                    {
                        ////AuditTrail_workorders_workorderno
                        var translateProperty = ProjectSession.Resources.audit.GetType().GetProperty(propertyName).GetValue(ProjectSession.Resources.audit).ToString();
                        translateNames.Add(translateProperty);
                    }
                    else if (propertyCmp == "modifiedby")
                    {
                        var translateProperty = ProjectSession.Resources.label.GetType().GetProperty("ItemRequisition_TextBlockModifiedBy").GetValue(ProjectSession.Resources.label).ToString();
                        translateNames.Add(translateProperty);
                    }
                    else if (propertyCmp == "modifieddate")
                    {
                        var translateProperty = ProjectSession.Resources.label.GetType().GetProperty("ItemRequisition_TextBlockModifiedDate").GetValue(ProjectSession.Resources.label).ToString();
                        translateNames.Add(translateProperty);
                    }
                    else
                    {
                        translateNames.Add(property);
                    }
                }
                catch
                {
                    translateNames.Add(property);
                }
            }

            finalTranslate = Newtonsoft.Json.JsonConvert.SerializeObject(translateNames);
            return finalTranslate;
        }

        /// <summary>
        /// Delete all record from selected table
        /// </summary>
        /// <param name="tableName">Delete all record from table by tableName</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteAuditTablesAllRecords)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteAuditTablesAllRecords(string tableName)
        {
            if (!string.IsNullOrEmpty(tableName))
            {
                DatabaseAuditTrailServices context = new DatabaseAuditTrailServices();

                tableName = "CDC.dbo_" + tableName + "_CT";

                bool returnValue = context.DeleteAuditTrialAllRecords(tableName);

                if (returnValue)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get Asset Details By AssetID
        /// </summary>
        /// <param name="tableName">The AssetDetails identifier</param>
        /// <param name="searchType">The AssetDetails </param>
        /// <returns></returns>
        [ActionName(Actions.GetDatabaseAuditTableDetails)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetDatabaseAuditTableDetails(string tableName, string searchType)
        {
            string strWhere = string.Empty;
            string query = string.Empty;
            tableName = tableName.ToLower();
            if (!(string.IsNullOrEmpty(searchType) || searchType == "null"))
            {
                searchType = HttpUtility.UrlDecode(searchType);
                strWhere = "and __$operation in (" + searchType + ")";
            }

            DatabaseAuditTrailServices context = new DatabaseAuditTrailServices();

            DataSourceResult dataSourceResult = null;

            switch (tableName)
            {
                case "workorders":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_workorders_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "assigntoworkorder":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_assigntoworkorder_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "workorderlabor":
                    dataSourceResult = new DataSourceResult()
                     {
                         Data = context.GetDataBaseAuditTrial<Dbo_workorders_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                     };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "direct":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_direct_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "issue":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_issue_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "return":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_return_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "workorderelements":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_workorderelements_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "workorderitems":
                    dataSourceResult = new DataSourceResult()
                   {
                       Data = context.GetDataBaseAuditTrial<Dbo_workorderitems_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                   };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "jobtask":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_jobtask_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "sitask":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_sitask_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_j":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "assets":
                    dataSourceResult = new DataSourceResult()
                 {
                     Data = context.GetDataBaseAuditTrial<Dbo_assets_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                 };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_a":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "l4":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_L4_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "l5":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_L5_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "location":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_location_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmschedule":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_pmschedule_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "multiplepm":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_multiplepm_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmmetermaster":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_pmmetermaster_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmmeters":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_pmmeters_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmmeters_assignto":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_pmmeters_assignto_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmchecklist":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_pmchecklist_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "checklistelements":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_checklistelements_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "checklistinv":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_checklistinv_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_p":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "substore":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_substore_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "attrgroup":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_attrgroup_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "attrsubgroup":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_attrsubgroup_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "partlocation":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_partlocation_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "stockcode":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_stockcode_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "stock_alternative":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_stock_alternative_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "stock_code_supplier":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_stock_code_supplier_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "stockcode_levels":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_stockcode_levels_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_s":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "material_req_master":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_material_req_master_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "material_req_details":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_material_req_details_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "employees":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_employees_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "l2":
                    dataSourceResult = new DataSourceResult()
                     {
                         Data = context.GetDataBaseAuditTrial<Dbo_L2_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                     };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "l3":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_L3_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "mainenancedivision":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_mainenancedivision_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "maintenancedepartment":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_maintenancedepartment_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "maintsubdept":
                    dataSourceResult = new DataSourceResult()
                                  {
                                      Data = context.GetDataBaseAuditTrial<Dbo_maintsubdept_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                                  };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "maintgroup":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_maintgroup_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "failurecause":
                    dataSourceResult = new DataSourceResult()
                 {
                     Data = context.GetDataBaseAuditTrial<Dbo_failurecause_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                 };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "suppliers":
                    dataSourceResult = new DataSourceResult()
                  {
                      Data = context.GetDataBaseAuditTrial<Dbo_suppliers_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                  };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "partslist":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_partslist_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "partslistdetails":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_partslistdetails_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "costcenter":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_costcenter_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "accountcode":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_accountcode_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "tbljobplan":
                    dataSourceResult = new DataSourceResult()
                        {
                            Data = context.GetDataBaseAuditTrial<Dbo_tbljobplan_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                        };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "tbljobplanitems":
                    dataSourceResult = new DataSourceResult()
                 {
                     Data = context.GetDataBaseAuditTrial<Dbo_tbljobplanitems_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                 };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "tblsafetyinstruction":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_tblsafetyinstruction_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "tblsafetyinstructionitems":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_tblsafetyinstructionitems_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "mr_authorisation_status":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_mr_authorisation_status_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "irapprovallevels":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_irapprovallevels_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "irapprovallevelmappings":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_irapprovallevelmappings_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "irapprovallevelmappings_employees":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_irapprovallevelmappings_employees_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "workpriority":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_workpriority_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "workstatus":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_workstatus_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "worktype":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_worktype_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "worktrade":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_worktrade_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "assetcategories":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_assetcategories_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "assetstatus":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_assetstatus_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "criticality":
                    dataSourceResult = new DataSourceResult()
                 {
                     Data = context.GetDataBaseAuditTrial<Dbo_criticality_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                 };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "warrantycontract":
                    dataSourceResult = new DataSourceResult()
                {
                    Data = context.GetDataBaseAuditTrial<Dbo_warrantycontract_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "employeecategories":
                    dataSourceResult = new DataSourceResult()
               {
                   Data = context.GetDataBaseAuditTrial<Dbo_employeecategories_CT>(0, strWhere, string.Empty, string.Empty, tableName)
               };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "employeestatuses":
                    dataSourceResult = new DataSourceResult()
              {
                  Data = context.GetDataBaseAuditTrial<Dbo_employeestatuses_CT>(0, strWhere, string.Empty, string.Empty, tableName)
              };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmchecklist_clean":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_pmchecklist_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "checklistelements_clean":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_checklistelements_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "checklistinv_clean":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_checklistinv_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_clean":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmschedule_clean":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_pmschedule_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "pmschedule_asset":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_pmschedule_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "multiplepm_asset":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_multiplepm_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "cleaninginspectiongroup":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_CleaningInspectionGroup_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "cleaninginspectiongroupsequencenumber":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_CleaningInspectionGroupSequenceNumber_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "cleaninginspectionmaster":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_cleaninginspectionmaster_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "cleaninginspectiondetails":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_cleaninginspectiondetails_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "purchaserequest":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_PurchaseRequest_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "purchaserequestitems":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_PurchaseRequestItems_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_pr":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "purchaseorder":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_PurchaseOrder_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "purchaseorderitems":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_PurchaseOrderItems_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_po":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "worequest":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_WoRequest_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                case "extassetfiles_r":
                    dataSourceResult = new DataSourceResult()
                    {
                        Data = context.GetDataBaseAuditTrial<Dbo_extassetfiles_CT>(0, strWhere, string.Empty, string.Empty, tableName)
                    };
                    return Json(dataSourceResult.Data, JsonRequestBehavior.AllowGet);

                default:
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgNoRecordsFound }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}