﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Management Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}