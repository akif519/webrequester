﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Active Session Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Actives the session.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.ActiveSession)]
        public ActionResult ActiveSession()
        {
            if (ProjectSession.PermissionAccess.Management_ActiveSessions_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.ActiveSession);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the active session.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetActiveSession)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetActiveSession([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "Logintime";
                sortDirection = "Ascending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            Session sessionSearch = new Session();

            SearchFieldService obj = new SearchFieldService();
            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(sessionSearch, SystemEnum.Pages.ActiveSession.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }

        /// <summary>
        /// Deletes the active session.
        /// </summary>
        /// <param name="sessionId">The session identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.DeleteActiveSession)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteActiveSession(int sessionId)
        {
            using (DapperContext context = new DapperContext())
            {
                int returnValue = context.Delete<Session>(sessionId, true);
                if (returnValue == 0)
                {
                    return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgCommonRecordDeletedSuccessfully }, JsonRequestBehavior.AllowGet);
                }
                else if (returnValue == -2)
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgReferencedInOtherModules }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}