﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Service;
using CMMS.Pages;
using CMMS.Model;
using CMMS.Infrastructure;
using Kendo.Mvc.UI;
using Kendo.Mvc;
using CMMS.Services;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Permission Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="userGroupId">User Group ID</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions._GetPermissions)]
        public ActionResult __GetPermissions(int employeeId, int userGroupId)
        {
            employees emp = new employees();
            emp.EmployeeID = employeeId;
            emp.UserGroupId = userGroupId;
            return View(PartialViews.Permissions, emp);
        }

        /// <summary>
        /// Partial view Employee Documents
        /// </summary>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="userGroupId">User Group ID</param>
        /// <param name="entityName">Name of the Entity</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.Permissions)]
        public ActionResult Permissions(int? employeeId = 0, int? userGroupId = 0, string entityName = "")
        {
            ViewBag.EmployeeID = employeeId;
            ViewBag.UserGroupID = userGroupId;
            ViewBag.EntityName = entityName;
            return View(Views.Permissions);
        }

        /// <summary>
        /// Permission List
        /// </summary>
        /// <param name="request">DataSource request object</param>
        /// <param name="employeeId">Employee ID</param>
        /// <param name="userGroupId">User Group ID</param>
        /// <returns></returns>
        [ActionName(Actions.GetPermissionsByEmpAndGroup)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPermissionsByEmpAndGroup([DataSourceRequest] DataSourceRequest request, int? employeeId = 0, int? userGroupId = 0)
        {
            PermissionService objService = new PermissionService();
            List<Permission> lstOfPermission = objService.GetEmployeepermissionListByEmployee(new employees() { EmployeeID = employeeId.Value, UserGroupId = userGroupId.Value }).ToList();

            // lstOfPermission = lstOfPermission.OrderBy(m => m.PermissionId).ToList();
            var result = new DataSourceResult()
            {
                Data = lstOfPermission,
                Total = lstOfPermission.Count
            };
            return Json(result);
        }

        /// <summary>
        /// To Save Permission Data
        /// </summary>
        /// <param name="employeeId">employee Id </param>
        /// <param name="groupId">group id</param>
        /// <param name="kendogriddata">serialized grid data</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.SavePermissions)]
        public ActionResult SavePermissions(int employeeId, int groupId, string kendogriddata)
        {
            try
            {
                var lstEmployeePermissionModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Permission>>(kendogriddata);
                PermissionService objService = new PermissionService();
                if (employeeId > 0)
                {
                    objService.UpdatePermissionByEmployee(lstEmployeePermissionModel, employeeId);
                }
                else if (groupId > 0)
                {
                    objService.UpdatePermissionByUserGroup(lstEmployeePermissionModel, groupId);
                }

                if (ProjectSession.EmployeeID == employeeId)
                {
                    employees objemployees = new employees();
                    using (DapperContext objDapperContext = new DapperContext())
                    {
                        objemployees.EmployeeID = employeeId;
                        objemployees = objDapperContext.Search<employees>(objemployees).FirstOrDefault();
                    }

                    ProjectSession.PermissionAccess = PermissionService.SetEmployeePermission(objemployees);
                }

                return Json(new object[] { SystemEnum.MessageType.success.ToString(), ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully });
            }
            catch
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin });
            }
        }
    }
}