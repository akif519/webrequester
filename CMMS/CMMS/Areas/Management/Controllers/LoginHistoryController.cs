﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using CMMS.Controllers;
using CMMS.Pages;
using Kendo.Mvc.UI;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.Management.Controllers
{
    /// <summary>
    /// Login History Controller
    /// </summary>
    public partial class ManagementController : BaseController
    {
        /// <summary>
        /// Logins the history.
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.LoginHistory)]
        public ActionResult LoginHistory()
        {
            if (ProjectSession.PermissionAccess.Management_LoginHistory_Allowaccess && ProjectSession.PermissionAccess.Management__ShowHideModule)
            {
                return View(Views.LoginHistory);
            }
            else
            {
                return RedirectToAction(Actions.AccessDenied, Pages.Controllers.Account, new { area = string.Empty });
            }
        }

        /// <summary>
        /// Gets the login history.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [ActionName(Actions.GetLoginHistory)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetLoginHistory([DataSourceRequest]DataSourceRequest request)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;
            string whereClause = string.Empty;

            if (request.Sorts.Count > 0)
            {
                sortExpression = request.Sorts[0].Member;
                sortDirection = request.Sorts[0].SortDirection == ListSortDirection.Ascending ? "Ascending" : "Descending";
            }
            else
            {
                sortExpression = "LoginDateTime";
                sortDirection = "Descending";
            }

            int pageNumber = request.Page;
            ProjectSession.PageSize = request.PageSize;

            LoginLog loginLogSearch = new LoginLog();

            SearchFieldService obj = new SearchFieldService();

            /*(Start)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            if (!ProjectSession.IsCentral)
            {
                whereClause += " and Employees.L2ID in (" + ProjectConfiguration.L2IDLISTPERMISSIONWISE + " ) ";
            }

            /*(End)Added By Pratik Telaviya on 27-Jan-2017 To Implement Task: TA263 In WorkSpace, Apply Area and Location Permission */

            var result = new DataSourceResult()
            {
                Data = obj.AdvanceSearch(loginLogSearch, SystemEnum.Pages.LoginHistory.GetHashCode(), ProjectSession.EmployeeID, pageNumber, sortExpression, sortDirection, extraWhereClause: whereClause, request: request),
                Total = obj.PagingInformation.TotalRecords
            };
            return Json(result);
        }
    }
}