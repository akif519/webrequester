﻿using System.Web.Mvc;

/// <summary>
/// Management Area Registration
/// </summary>
namespace CMMS.Areas.Management
{
    /// <summary>
    /// Management Area Registration
    /// </summary>
    public class ManagementAreaRegistration : AreaRegistration 
    {
        /// <summary>
        /// Area Name
        /// </summary>
        public override string AreaName 
        {
            get 
            {
                return Pages.Areas.Management;
            }
        }

        /// <summary>
        /// Register Area
        /// </summary>
        /// <param name="context">Area Registration Context</param>
        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
            name: "Management_default",
            url: Pages.Areas.Management + "/{action}",
            defaults: new { controller = Pages.Controllers.Management, action = "Index" });

            context.MapRoute(
                name: "Management",
                url: Pages.Areas.Management + "/{controller}/{action}/{id}",
                defaults: new { controller = Pages.Controllers.Management, action = "Index", id = UrlParameter.Optional });
        }
    }
}