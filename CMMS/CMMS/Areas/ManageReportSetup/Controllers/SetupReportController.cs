﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Service;

namespace CMMS.Areas.ManageReportSetup.Controllers
{
    /// <summary>
    /// Setup Report Controller
    /// </summary>
    public class SetupReportController : BaseController
    {
        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Edits the organization details.
        /// </summary>
        /// <returns></returns>
        [ActionName("edit-organization-details")]
        public ActionResult EditOrganizationDetails()
        {
            OrganizationDetails orgdtls = new OrganizationDetails();

            using (DapperContext objDapper = new DapperContext())
            {
                orgdtls = objDapper.SearchAll<OrganizationDetails>(orgdtls).FirstOrDefault();
            }

            return View("EditOrganizationDetails", orgdtls);
        }

        /// <summary>
        /// Changes the Modal photo.
        /// </summary>
        /// <param name="cropPointX">The crop point x.</param>
        /// <param name="cropPointY">The crop point y.</param>
        /// <param name="imageCropWidth">Width of the image crop.</param>
        /// <param name="imageCropHeight">Height of the image crop.</param>
        /// <param name="oldFileName">Old File Name</param>
        /// <returns></returns>
        [ActionName(Actions.ChangeModaPhoto)]
        [HttpPost]
        public ActionResult ChangeModaLogo(int? cropPointX, int? cropPointY, int? imageCropWidth, int? imageCropHeight, string oldFileName)
        {
            HttpPostedFileBase changeModaLogo = Request.Files["UploadedFile"];
            int allowedMaxSizeinMB = 1;            
            string[] allowedExt = new string[] { ".jpg", ".gif", ".png", ".bmp", ".psd" };
            string[] allowedExtFromMaster = ProjectSession.AllowedFileTypes.ToLower().Split(',');
            int allowedMaxSizeinMBMaster = ProjectSession.AllowedMaxFilesize;
            if (changeModaLogo.ContentLength > 0 && !allowedExt.Contains(changeModaLogo.FileName.ToLower().Substring(changeModaLogo.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }
            else if (!allowedExtFromMaster.Contains(changeModaLogo.FileName.ToLower().Substring(changeModaLogo.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }

            if ((changeModaLogo.ContentLength / 1024) / 1024 > allowedMaxSizeinMB)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadSizeImage, string.Empty });
            }

            string photoName = changeModaLogo.FileName;
            oldFileName = Path.GetFileName(oldFileName);
            string filename = string.Empty;
            byte[] imageBytes = ConvertTo.Bytes(changeModaLogo);
            byte[] croppedImage = IOManager.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            try
            {
                if (oldFileName.Length > 0 && !string.IsNullOrEmpty(oldFileName))
                {
                    IOManager.DeleteFile(ProjectConfiguration.UploadPath + "ModaImages\\" + Convert.ToString(oldFileName).Trim());
                }

                if (photoName.Length > 0)
                {
                    filename = System.Guid.NewGuid().ToString() + Path.GetExtension(photoName);
                    filename = IOManager.SaveFile(croppedImage, ProjectConfiguration.UploadPath + "ModaImages\\", filename);
                }
            }
            catch
            {
            }

            return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Photo Uploaded Successfully.", filename });
        }

        /// <summary>
        ///  Changes the Branch photo.
        /// </summary>
        /// <param name="cropPointX">The crop point x.</param>
        /// <param name="cropPointY">The crop point y.</param>
        /// <param name="imageCropWidth">Width of the image crop.</param>
        /// <param name="imageCropHeight">Height of the image crop.</param>
        /// <param name="oldFileName">Old File Name</param>
        /// <returns></returns>
        [ActionName(Actions.ChangeBranchPhoto)]
        [HttpPost]
        public ActionResult ChangeBranchLogo(int? cropPointX, int? cropPointY, int? imageCropWidth, int? imageCropHeight, string oldFileName)
        {
            HttpPostedFileBase ChangeBranchLogo = Request.Files["UploadedFile"];
            int allowedMaxSizeinMB = 1;
            string[] allowedExt = new string[] { ".jpg", ".gif", ".png", ".bmp", ".psd" };
            string[] allowedExtFromMaster = ProjectSession.AllowedFileTypes.ToLower().Split(',');
            int allowedMaxSizeinMBMaster = ProjectSession.AllowedMaxFilesize;
            if (ChangeBranchLogo.ContentLength > 0 && !allowedExt.Contains(ChangeBranchLogo.FileName.ToLower().Substring(ChangeBranchLogo.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }
            else if (!allowedExtFromMaster.Contains(ChangeBranchLogo.FileName.ToLower().Substring(ChangeBranchLogo.FileName.LastIndexOf('.'))))
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadValidImage, string.Empty });
            }


            if ((ChangeBranchLogo.ContentLength / 1024) / 1024 > allowedMaxSizeinMB)
            {
                return Json(new object[] { 0, SystemEnum.MessageType.danger.ToString(), @ProjectSession.Resources.message.Common_MsgUploadSizeImage, string.Empty });
            }

            string photoName = ChangeBranchLogo.FileName;
            oldFileName = Path.GetFileName(oldFileName);
            string filename = string.Empty;
            byte[] imageBytes = ConvertTo.Bytes(ChangeBranchLogo);
            byte[] croppedImage = IOManager.CropImage(imageBytes, cropPointX.Value, cropPointY.Value, imageCropWidth.Value, imageCropHeight.Value);

            try
            {
                if (oldFileName.Length > 0 && !string.IsNullOrEmpty(oldFileName))
                {
                    IOManager.DeleteFile(ProjectConfiguration.UploadPath + "BranchImages\\" + Convert.ToString(oldFileName).Trim());
                }

                if (photoName.Length > 0)
                {
                    filename = System.Guid.NewGuid().ToString() + Path.GetExtension(photoName);
                    filename = IOManager.SaveFile(croppedImage, ProjectConfiguration.UploadPath + "BranchImages\\", filename);
                }
            }
            catch
            {
            }

            return Json(new object[] { SystemEnum.MessageType.success.ToString(), "Photo Uploaded Successfully.", filename });
        }

        /// <summary>
        /// Saves the organization details.
        /// </summary>
        /// <param name="orgDtls">The org DTLS.</param>
        /// <returns></returns>
        public ActionResult SaveOrganizationDetails(OrganizationDetails orgDtls)
        {
            int result = 0;

            try
            {
                if (ModelState.IsValid)
                {
                    using (DapperContext objContext = new DapperContext())
                    {
                        result = objContext.Save<OrganizationDetails>(orgDtls);
                    }
                }

                if (result > 0)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgRecordSavedSuccessfully;
                    TempData["MessageType"] = SystemEnum.MessageType.success.ToString();
                }

                return RedirectToAction(Actions.ManageOrganizationDetails, Pages.Controllers.SetupReport, new { area = Pages.Areas.ManageReportSetup });
            }
            catch
            {
                if (Convert.ToString(TempData["Message"]) == string.Empty)
                {
                    TempData["Message"] = ProjectSession.Resources.message.Common_MsgErrorContactAdmin;
                }

                TempData["MessageType"] = SystemEnum.MessageType.danger.ToString();
                return RedirectToAction(Actions.ManageOrganizationDetails, Pages.Controllers.SetupReport, new { area = Pages.Areas.ManageReportSetup });
            }
        }
    }
}