﻿using System.Web.Mvc;

namespace CMMS.Areas.ManageReportSetup
{
    /// <summary>
    /// Manage Report Setup
    /// </summary>
    public class ManageReportSetupAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Gets the name of the area to register.
        /// </summary>
        /// <returns>The name of the area to register.</returns>
        public override string AreaName
        {
            get
            {
                return "ManageReportSetup";
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            //context.MapRoute(
            //    "ManageReportSetup_default",
            //    "ManageReportSetup/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
            context.MapRoute(
            name: "SetupReport_default",
            url: Pages.Areas.ManageReportSetup + "/{action}",
            defaults: new { controller = Pages.Controllers.SetupReport, action = "Index" });

            context.MapRoute(
                "SetReports_defaultValue",
                "ManageReportSetup/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional });
        }
    }
}