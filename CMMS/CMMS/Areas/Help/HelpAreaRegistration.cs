﻿using System.Web.Mvc;

namespace CMMS.Areas.Help
{
    /// <summary>
    /// Help Area Registration
    /// </summary>
    public class HelpAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Help Area Registration
        /// </summary>
        public override string AreaName
        {
            get
            {
                return Pages.Areas.Help;
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
            name: "Help_default",
            url: Pages.Areas.Help + "/{action}",
            defaults: new { controller = Pages.Controllers.Help, action = "Index" });

            context.MapRoute(
               name: "Help",
               url: Pages.Areas.Help + "/{controller}/{action}/{id}",
               defaults: new { controller = Pages.Controllers.Help, action = "Index", id = UrlParameter.Optional });
        }
    }
}