﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMMS.Controllers;
using CMMS.Pages;
using CMMS.Service;
using CMMS.Model;
using CMMS.Infrastructure;

namespace CMMS.Areas.Help.Controllers
{
    /// <summary>
    /// Help Controller
    /// </summary>
    public partial class HelpController : BaseController
    {
        /// <summary>
        /// Load Help Page
        /// </summary>
        /// <returns></returns>
        [ActionName(Actions.Help)]
        public ActionResult Help()
        {
            return View(Views.HelpPage);
        }

        /// <summary>
        /// Gets the asset tree.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpTree)]
        public JsonResult GetHelpTree(int id = 0)
        {
            List<CustomHelpTree> lstTreeClass = new List<CustomHelpTree>();
            try
            {
                Language objLanguage = new Language();
                CMMS.Infrastructure.ProjectSession.UseHelpDB = true;
                objLanguage = new DapperContext().SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();

                int languageID = objLanguage.LanguageID;

                int employeeID = ProjectSession.EmployeeID;

                string level = string.Empty;

                IList<HelpModule> lstL1;
                IList<HelpModule> lstL2;

                if (id == 0)
                {
                    lstL1 = HelpServices.GetTopics(0, 1, employeeID, languageID);
                    lstTreeClass = lstL1.Select(x => new CustomHelpTree() { ID = Convert.ToString(x.HelpModuleID), Name = x.HelpTitle, HasChild = x.ParentID == 0 ? true : false, HasHelpDescription = x.HasHelpDescription, HelpModuelDetailID = x.HelpModuelDetailID }).ToList();
                }
                else
                {
                    lstL2 = HelpServices.GetTopics(id, 2, employeeID, languageID);

                    lstTreeClass = lstL2.Select(x => new CustomHelpTree() { ID = Convert.ToString(x.HelpModuleID), Name = x.HelpTitle, HasChild = x.ChildCount > 0 ? true : false, HasHelpDescription = x.HasHelpDescription, HelpModuelDetailID = x.HelpModuelDetailID }).ToList();
                }

                return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Gets the asset tree By Index.
        /// </summary>
        /// <param name="index">The identifier.</param>
        /// <returns></returns>
        [ActionName(Actions.GetHelpTreeIndex)]
        public JsonResult GetHelpTreeIndex(string index)
        {
            List<CustomHelpTree> lstTreeClass = new List<CustomHelpTree>();
            try
            {
                Language objLanguage = new Language();
                CMMS.Infrastructure.ProjectSession.UseHelpDB = true;
                objLanguage = new DapperContext().SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();

                int languageID = objLanguage.LanguageID;
                int employeeID = ProjectSession.EmployeeID;
                string level = string.Empty;

                IList<HelpModule> lstL1;
                IList<HelpModule> lstL2;

                if (index == null)
                {
                    lstL1 = HelpServices.GetTopicsByIndex(null, 1, employeeID, languageID);
                    lstTreeClass = lstL1.Select(x => new CustomHelpTree() { ID = x.HelpTitle, Name = x.HelpTitle, HasChild = true, HasHelpDescription = false }).ToList();
                }
                else
                {
                    lstL2 = HelpServices.GetTopicsByIndex(index, 2, employeeID, languageID);
                    lstTreeClass = lstL2.Select(x => new CustomHelpTree() { ID = Convert.ToString(x.HelpModuleID), Name = x.HelpTitle, HasChild = false, HasHelpDescription = true, HelpModuelDetailID = x.HelpModuelDetailID }).ToList();
                }

                return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(lstTreeClass, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Partial view Help Detail
        /// </summary>
        /// <param name="helpModuelDetailID">Help Detail identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName(Actions.GetHelpDetail)]
        public ActionResult GetHelpDetail(int helpModuelDetailID)
        {
            Language objLanguage = new Language();
            CMMS.Infrastructure.ProjectSession.UseHelpDB = true;
            objLanguage = new DapperContext().SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();

            int languageID = objLanguage.LanguageID;
            int employeeID = ProjectSession.EmployeeID;
            HelpModule_Detail obj = new HelpModule_Detail();
            IList<HelpModule> lstL2;

            if (helpModuelDetailID > 0)
            {
                obj = HelpServices.GetHelpDetails(helpModuelDetailID, languageID);

                obj.CustomHelpRelatedItem = HelpServices.GetHelpRelatedItem(helpModuelDetailID);

                HelpChapter objHelpChapter = new HelpChapter();
                using (DapperContext context = new DapperContext())
                {
                    ProjectSession.UseHelpDB = true;
                    obj.HelpChapter = context.SearchAll(objHelpChapter).Where(m => m.HelpModuleDetailID == helpModuelDetailID).ToList();
                }

                HelpBookmarks objHelpBookmarks = new HelpBookmarks();
                using (DapperContext context = new DapperContext())
                {
                    objHelpBookmarks = context.SearchAll(objHelpBookmarks).Where(m => m.HelpModuleID == obj.HelpModuleID && m.EmployeeID == ProjectSession.EmployeeID).FirstOrDefault();
                }

                if (objHelpBookmarks != null)
                {
                    obj.isBookmarked = true;
                }
                else
                {
                    obj.isBookmarked = false;
                }

                lstL2 = HelpServices.GetNextPrev(employeeID, languageID);

                if (lstL2 != null)
                {
                    int index = lstL2.FindIndex(a => a.HelpModuelDetailID == helpModuelDetailID);
                    if (index == 0)
                    {
                        obj.PrevHelpModuelDetailID = 0;
                    }
                    else
                    {
                        obj.PrevHelpModuelDetailID = lstL2[index - 1].HelpModuelDetailID;
                    }

                    if (index == (lstL2.Count - 1))
                    {
                        obj.NextHelpModuelDetailID = 0;
                    }
                    else
                    {
                        obj.NextHelpModuelDetailID = lstL2[index + 1].HelpModuelDetailID;
                    }
                }
            }

            return View(PartialViews._HelpDetailTemplate, obj);
        }

        /// <summary>
        /// get bookmarks
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.GetHelpBookmarks)]
        public JsonResult GetHelpBookmarks()
        {
            IList<HelpBookmarks> lstBookmarks = new List<HelpBookmarks>();

            try
            {
                Language objLanguage = new Language();
                CMMS.Infrastructure.ProjectSession.UseHelpDB = true;
                objLanguage = new DapperContext().SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();

                int languageID = objLanguage.LanguageID;
                HelpBookmarks obj = new HelpBookmarks();

                lstBookmarks = HelpServices.GetHelpBookmarks(ProjectSession.EmployeeID, languageID);

                return Json(lstBookmarks, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(lstBookmarks, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Search Help
        /// </summary>
        /// <param name="searchText">search by Text</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.HelpSearch)]
        public JsonResult HelpSearch(string searchText)
        {
            IList<HelpModule> lstHelp = new List<HelpModule>();
            try
            {
                Language objLanguage = new Language();
                CMMS.Infrastructure.ProjectSession.UseHelpDB = true;
                objLanguage = new DapperContext().SearchAll(objLanguage).Where(o => o.LanguageCode == ProjectSession.Culture).FirstOrDefault();

                int languageID = objLanguage.LanguageID;
                HelpModule obj = new HelpModule();
                                
                lstHelp = HelpServices.GetSearchHelp(searchText, languageID);

                return Json(lstHelp, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CMMS.Models.Common.Log(ex);
                return Json(lstHelp, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// add/remove bookmark
        /// </summary>
        /// <param name="helpID">bookmark identifier </param>
        /// <returns></returns>
        [HttpPost]
        [ActionName(Actions.BookmarHelp)]
        public JsonResult BookmarHelp(int helpID)
        {
            int employeeID = ProjectSession.EmployeeID;
            bool isAdd = false;
            HelpBookmarks objHelpBookmarks = new HelpBookmarks();
            using (DapperContext context = new DapperContext())
            {
                objHelpBookmarks = context.SearchAll(objHelpBookmarks).Where(m => m.HelpModuleID == helpID && m.EmployeeID == employeeID).FirstOrDefault();
            }

            if (objHelpBookmarks != null)
            {
                isAdd = false;
            }
            else
            {
                isAdd = true;
            }

            bool bookmarks = HelpServices.AddRemoveBookmark(helpID, employeeID, isAdd);

            if (bookmarks)
            {
                if (isAdd)
                {
                    return Json(new object[] { "added" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new object[] { "removed" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new object[] { SystemEnum.MessageType.danger.ToString(), ProjectSession.Resources.message.Common_MsgErrorContactAdmin }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}