﻿/*(Start)Basic Information Tab*/
function BindParameters() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        jobTypeId: 0
    };
}

function BindParametersCmProjects() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == 'undefined' ? 0 : $('#L2ID').val())
    };
}

function BindParametersForWorkPriority() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        jobPriorityId: 0
    };
}

function BindParametersWOByAssetId() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        assetID: ($('#AssetID').val() == '' || $('#AssetID').val() == 'undefined' ? 0 : $('#AssetID').val())
    };
}

function BindParametersWOByAssetIdAndLocationID() {

    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        assetID: ($('#AssetID').val() == '' || $('#AssetID').val() == 'undefined' ? 0 : $('#AssetID').val()),
        locationID: ($('#LocationID').val() == '' || $('#LocationID').val() == 'undefined' ? 0 : $('#LocationID').val())
    };
}
function BindParametersWRByAssetId() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        assetID: ($('#AssetID').val() == '' || $('#AssetID').val() == 'undefined' ? 0 : $('#AssetID').val())
    };
}

function onDataBoundEmployee(arg) {
    gridProperPaging(this);
}

function onDataBoundCmProject(arg) {
    gridProperPaging(this);
}

function onDataBoundjobOrder(arg) {
    gridProperPaging(this);
}

function onDataBoundBuildings(arg) {
    gridProperPaging(this);
}

function onDataBoundLocation(arg) {
    gridProperPaging(this);
}

function onDataBoundJOStatusTrack(arg) {
    gridProperPaging(this);
}

function BindParametersJOStatusTrack() {
    return {
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}

//for Location Auto complete
function onLocationAdditionalData() {
    return {
        strWhere: " and location.L2ID =" + $('#L2ID').val(),
        text: $("#txtLocationDescription").val(),
    };
}

function BindParametersLocation() {
    return {
        strWhere: " and location.L2ID =" + $('#L2ID').val(),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}

//for Asset Auto complete
function onAssetAdditionalData() {
    return {
        locationID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        text: $("#txtAssetDescription").val(),
    };
}

function BindParametersAsset() {
    return {
        locationID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}


//for Assign to tab Employee Auto complete
function onAssignTOEmployeeAdditionalData() {
    return {
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        type: 2,
        text: $("#txtAssignTOEmpName").val(),
    };
}


//for Employee Auto complete
function onEmployeeAdditionalData() {
    return {
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        type: 1,
        text: $("#txtName").val(),
    };
}

//for JO close tab Employee Auto complete
function onJOCloseEmployeeAdditionalData() {
    return {
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        type: 1,
        text: $("#txtAcceptedByEmpName").val(),
    };
}

//for JO Labor tab Employee Auto complete
function onJOLaborEmployeeAdditionalData() {
    return {
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        type: 1,
        text: $("#txtLaborEmpName").val(),
    };
}

function BindParametersEmployee() {
    var strWhere = '';
    var intType = 1;
    if (employeeFromPage == 1) {
        ///Assign To Tab
        intType = 2;
    }

    return {
        cityID: ($('#L2ID').val() == '' || $('#L2ID').val() == undefined) ? 0 : $('#L2ID').val(),
        type: intType,
        strWhere: strWhere,
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}

//for Building Auto complete
function onL5AdditionalData() {
    var zoneID1 = $('#ddlZones').val();
    var cityID1 = $('#L2ID').val();
    var areaID1 = $('#ddlAreas').val();

    if (zoneID1 == '' || zoneID1 == 'undefined' || zoneID1 == null) {
        zoneID1 = 0;
    }
    if (cityID1 == '' || cityID1 == 'undefined' || cityID1 == null) {
        cityID1 = 0;
    }
    if (areaID1 == '' || areaID1 == 'undefined' || areaID1 == null) {
        areaID1 = 0;
    }
    return {
        cityID: cityID1,
        areaID: areaID1,
        zoneID: zoneID1,
        text: $("#txtL5Description").val(),
    };
}

function BindParametersBuilding() {

    var zoneID1 = $('#ddlZones').val();
    var cityID1 = $('#L2ID').val();
    var areaID1 = $('#ddlAreas').val();

    if (zoneID1 == '' || zoneID1 == 'undefined' || zoneID1 == null) {
        zoneID1 = 0;
    }
    if (cityID1 == '' || cityID1 == 'undefined' || cityID1 == null) {
        cityID1 = 0;
    }
    if (areaID1 == '' || areaID1 == 'undefined' || areaID1 == null) {
        areaID1 = 0;
    }

    return {
        cityID: cityID1,
        areaID: areaID1,
        zoneID: zoneID1,
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function BindParametersWRByRequester() {
    return {
        requesterID: ($('#RequesterID').val() == '' || $('#RequesterID').val() == null) ? 0 : $('#RequesterID').val(),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}


function addRequestVerificationToken(data) {
    data.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();
    return data;
};

function ChangeL4(e) {
    //var dataItem = this.dataItem(e.item);
    //dataItem.L4ID
    ClearLocation();
    ClearAsset();
    ClearBuilding();
    ReBindKendoGrid("buildingGrid");
}
function EnableDisableButton(value) {
    if (value == 'undefined' || value == null || value == '') {
        $('#btnOpenEmployeePopUp').addClass('disabled');
        $('#btnClearEmployee').addClass('disabled');
        $('#txtName').addClass('disabled');
        $('#btnOpenBuildingPopup').addClass('disabled');
        $('#btnClearBuilding').addClass('disabled');
        $('#txtL5Description').addClass('disabled');
        $('#btnOpenLocationPopup').addClass('disabled');
        $('#btnClearLocation').addClass('disabled');
        $('#txtLocationDescription').addClass('disabled');
        $('#btnOpenAssetPopup').addClass('disabled');
        $('#btnClearAsset').addClass('disabled');
        $('#txtAssetDescription').addClass('disabled');
    }
    else {
        $('#btnOpenEmployeePopUp').removeClass('disabled');
        $('#btnClearEmployee').removeClass('disabled');
        $('#txtName').removeClass('disabled');
        $('#btnOpenBuildingPopup').removeClass('disabled');
        $('#btnClearBuilding').removeClass('disabled');
        $('#txtL5Description').removeClass('disabled');
        $('#btnOpenLocationPopup').removeClass('disabled');
        $('#btnClearLocation').removeClass('disabled');
        $('#txtLocationDescription').removeClass('disabled');
        $('#btnOpenAssetPopup').removeClass('disabled');
        $('#btnClearAsset').removeClass('disabled');
        $('#txtAssetDescription').removeClass('disabled');
    }

}
var employeeFromPage = 0;
function OpenEmployeePopUp(fromPage) {
    employeeFromPage = fromPage;
    var grid = $("#employeeGrid").data("kendoGrid");
    ReBindKendoGrid('employeeGrid');

    if (employeeFromPage == 1) {
        //Assign To Page

    }
    if (employeeFromPage == 3 && grid != 'undefined') {
        //From Labor Tab
        grid.showColumn("HourlySalary");
        grid.showColumn("OverTime1");
        grid.showColumn("OverTime2");
        grid.showColumn("OverTime3");
    }
    else {
        if (grid != 'undefined') {
            grid.hideColumn("HourlySalary");
            grid.hideColumn("OverTime1");
            grid.hideColumn("OverTime2");
            grid.hideColumn("OverTime3");
        }
    }
    $("#employeeGridModal").modal("show");
}

function OpenCMProjectPopUp() {
    $("#cmProjectGridModal").modal("show");
}

function OpenBuildingPopUp() {
    $("#buildingGridModal").modal("show");
}

function OpenLocationPopUp() {
    $("#locationGridModal").modal("show");
}
function OpenAssetPopUp() {
    $("#AssetGridModal").modal("show");
}
function onDataBoundAsset(arg) {
    gridProperPaging(this);
}

function OpenJoExistingPopup() {
    $("#ExistingWOByAssetIdAndLocationIDrGridModal").modal("show");
}
function OpenCheckListPopUp() {
    $("#PMChecklistGridModal").modal("show");
}
/*(Start)Check list Pop up*/
function onDataBoundCheckList(arg) {
    gridProperPaging(this);
}


function onCheckListAdditionalData() {

    var l2ID = $('#L2ID').val() == "" ? 0 : $('#L2ID').val();
    return {
        //siteID: $('#ddlCity').val(),
        strWhere: " AND ( pmchecklist.L2ID = " + l2ID + " OR pmchecklist.L2ID IS NULL )",
        text: $("#txtCheckListName").val(),
    };
}

function BindParametersCheckList() {

    var l2ID = $('#L2ID').val() == "" ? 0 : $('#L2ID').val();
    return {
        //siteID: $('#ddlCity').val(),
        strWhere: " AND ( pmchecklist.L2ID = " + l2ID + " OR pmchecklist.L2ID IS NULL )",
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}

function ClearCheckListData() {
    $("#hdnChecklistID").val("");
    $("#txtPMCheckListNo").val("");
    $("#txtCheckListName").val("");
    $("#txtAltCheckListName").val("");
}


/*(End)Check list Pop up*/

function onJOCloseEmployeeAutoSelect(e) {
    var model = this.dataItem(e.item.index());
    //For JO Close TO Tab
    $('#hdnAcceptedByEmpID').val(model.EmployeeID);
    $("#txtAcceptedByEmpNo").val(model.EmployeeNO);
    //$("#txtAcceptedByEmpName").val(model.Name);
    //$("#txtAcceptedByEmpAltName").val(model.AltName);
}

function onEmployeeAutoSelect(e) {
    var model = this.dataItem(e.item.index());
    $('#RequestorID').val(model.EmployeeID);
    $("#txtEmployeeNO").val(model.EmployeeNO);
    $("#EmployeeNO").val(model.EmployeeNO);
    $("#Name").val(model.Name);
    //$("#Name").val($("#txtName").val());
    try { $("#AltName").val(model.AltName); } catch (e) { }
    //$("#AltName").val($("#txtAltName").val());
    $("#txtEmail").val(model.Email);
    $("#txtTelephoneNo").val(model.WorkPhone);
    $("#txtFaxno").val(model.Fax);
    $("#txtMobileNo").val(model.HandPhone);
}

function onJOLaborEmployeeAutoSelect(e) {
    var model = this.dataItem(e.item.index());
    //For Labor Tab
    $('#hdnLaborEmployeeID').val(model.EmployeeID);
    $("#txtLaborEmpNo").val(model.EmployeeNO);
    //$("#txtLaborEmpName").val(model.Name);
    //$("#txtLaborAltEmpName").val(model.AltName);

    $('#hdnLaborHourlySalary').val(model.HourlySalary);
    $('#hdnLaborOT1').val(model.OverTime1);
    $('#hdnLaborOT2').val(model.OverTime2);
    $('#hdnLaborOT3').val(model.OverTime3);
}


$("#employeeGrid tbody").on("dblclick", "tr[role='row']", function (e) {
    var grid = $("#employeeGrid").data("kendoGrid");
    var model = grid.dataItem(this);
    $("#employeeGridModal").modal("hide");
    //Employee

    if (employeeFromPage == 0) {
        //For Basic Info Tab
        $('#RequestorID').val(model.EmployeeID);
        $("#txtEmployeeNO").val(model.EmployeeNO);
        $("#EmployeeNO").val(model.EmployeeNO);
        $("#txtName").val(model.Name);
        $("#Name").val($("#txtName").val());
        //$("#AltName").val($("#txtAltName").val());
        $("#txtEmail").val(model.Email);
        $("#txtTelephoneNo").val(model.WorkPhone);
        $("#txtFaxno").val(model.Fax);
        $("#txtMobileNo").val(model.HandPhone);
    }
    else if (employeeFromPage == 1) {
        //For Assign TO Tab
        $('#hdnAssignTOEmployeeID').val(model.EmployeeID);
        $("#txtAssignTOEmpNO").val(model.EmployeeNO);
        $("#txtAssignTOEmpName").val(model.Name);
        $('#hdnSaveFrom').val(1);
        ClearAssignToCheckBoxes();
        ClearAssignToMaintGroups();
    }
    else if (employeeFromPage == 2) {
        //For JoClosing Tab
        $('#hdnAcceptedByEmpID').val(model.EmployeeID);
        $("#txtAcceptedByEmpNo").val(model.EmployeeNO);
        $("#txtAcceptedByEmpName").val(model.Name);
    }
    else if (employeeFromPage == 3) {
        //For Labor Tab
        $('#hdnLaborEmployeeID').val(model.EmployeeID);
        $("#txtLaborEmpNo").val(model.EmployeeNO);
        $("#txtLaborEmpName").val(model.Name);
        $('#hdnLaborHourlySalary').val(model.HourlySalary);
        $('#hdnLaborOT1').val(model.OverTime1);
        $('#hdnLaborOT2').val(model.OverTime2);
        $('#hdnLaborOT3').val(model.OverTime3);
    }

});

$("#cmProjectGrid tbody").on("dblclick", "tr[role='row']", function (e) {
    var grid = $("#cmProjectGrid").data("kendoGrid");
    var model = grid.dataItem(this);
    $("#cmProjectGridModal").modal("hide");

    //CmProject
    $('#txtProjectNumber').val(model.ProjectNumber);
    if (model.ProjectNumber != null && model.ProjectNumber != undefined) {
        $('#btnClearCMProject').addClass('disabled');
    }
    else { $('#btnClearCMProject').removeClass('disabled'); }
    setTimeout(function () { $("#ddlMaintDivision").data("kendoDropDownList").dataSource.read(); }, 500);
});

//$("#joborderByAssetIDAndLocationIDGrid tbody").on("dblclick", "tr[role='row']", function (e) {
//    var grid = $("#joborderByAssetIDAndLocationIDGrid").data("kendoGrid");
//    var model = grid.dataItem(this);
//    $("#ExistingWOByAssetIdAndLocationIDrGridModal").modal("hide");

//    //Employee
//    $('#WorkorderNo').val(model.WorkorderNo);
//    $("#WorkStatus").val(model.WorkStatus);
//    $("#AltWorkStatus").val(model.AltWorkStatus);
//});

function FillAssetValues(asset) {
    ClearAsset();
    if (asset != 'undefined' && asset != null) {
        $("#txtAssetNumber").val(asset.AssetNumber);
        $('#AssetNumber').val(asset.AssetNumber);
        $('#txtAssetNoPopUp2').val(asset.AssetNumber);

        $("#txtAuthEmployeeName").val('@Lang' ? asset.Name : asset.AltName);

        $("#txtAssetStatusDesc").val(asset.AssetStatusDesc);
        $("#txtAltAssetStatusDesc").val(asset.AltAssetStatusDesc);

        $('#txtAssetWarrentyExpiryDate').val(DateTemplate(asset.Warranty_ContractExpiry));

        $('#txtAssetWarrentyStatus').val(asset.AssetWarrentyStatus);

        $('#txtNotesToTechAsset').val(asset.NotesToTech);

        if (projectCulture == 'ar') {
            $('#txtAssetDescription').val(asset.AssetAltDescription);
        }
        else {
            $('#txtAssetDescription').val(asset.AssetDescription);
        }

        $('#Asset_AssetDescription').val(asset.AssetDescription);
        $('#txtAssetDescPopUp2').val(asset.AssetDescription);

        $('#Asset_AssetAltDescription').val(asset.AssetAltDescription);
        $('#txtAssetAltDescPopUp2').val(asset.AssetAltDescription);

        $('#Asset_Warranty_contract').val('@Lang' ? asset.Warranty_contract : asset.AltWarranty_contract);
        $('#txtWarrentyContractPopUp2').val('@Lang' ? asset.Warranty_contract : asset.AltWarranty_contract);

        $('#Asset_Warranty_ContractExpiry').val(DateTemplate(asset.Warranty_ContractExpiry));
        $('#txtWarrentyExpiryPopUp2').val(DateTemplate(asset.Warranty_ContractExpiry));

        $('#AuthorisedEmployeeId').val(asset.EmployeeID);

        $('#AssetID').val(asset.AssetID);
    }
}

function FillLocationValues(location) {
    $("#txtLocationNo").val(location.LocationNo);
    $("#txtlocationNoPopUp2").val(location.LocationNo);

    if (projectCulture == 'ar') {
        $("#txtLocationDescription").val(location.LocationAltDescription);
    }
    else {
        $("#txtLocationDescription").val(location.LocationDescription);
    }

    $("#txtlocationDescPopUp2").val(location.LocationDescription);
    $("#txtlocationAltDescPopUp2").val(location.LocationAltDescription);
    $('#txtLocationAuthEmployeeName').val(location.LocationAuthEmployeeName);
    
    if (location.NotetoTech == undefined)
    {
        $("#txtNotesToTechLocation").val(location.NoteToTech);
    }
    if (location.NoteToTech == undefined) {
        $("#txtNotesToTechLocation").val(location.NotetoTech);
    }
    
    if (location.L4ID != null || location.L4ID != 'undefined') {
        $('#ddlZones').data("kendoDropDownList").value(location.L4ID);
    }

    $('#LocationID').val(location.LocationID);
    FillBuildingValues(location);
}

function FillBuildingValues(L5) {
    $('#L5ID').val(L5.L5ID);
    $("#txtL5No").val(L5.L5No);
    if (projectCulture == 'ar') {
        $("#txtL5Description").val(L5.L5AltDescription);
    }
    else {
        $("#txtL5Description").val(L5.L5Description);
    }
}

function onLocationAutoSelect(e) {
    var model = this.dataItem(e.item.index());
    GetJoCountByAssetIDAndLocationID(0, model.LocationID, model);
    $("#dvViewDiagram").show();
}


$("#locationGrid tbody").on("dblclick", "tr[role='row']", function (e) {
    var grid = $("#locationGrid").data("kendoGrid");
    var model = grid.dataItem(this);
    $("#locationGridModal").modal("hide");
    GetJoCountByAssetIDAndLocationID(0, model.LocationID, model);
    //FillLocationValues(model);
    $("#dvViewDiagram").show();
});


function onL5AutoSelect(e) {
    var model = this.dataItem(e.item.index());
    FillBuildingValues(model);
}


$("#buildingGridModal tbody").on("dblclick", "tr[role='row']", function (e) {
    var grid = $("#buildingGrid").data("kendoGrid");
    var model = grid.dataItem(this);
    $("#buildingGridModal").modal("hide");
    FillBuildingValues(model);
});
function ClearEmployeeData() {
    $("#txtEmployeeNO").val("");
    $("#txtName").val("");
    $("#txtAltName").val("");
    $("#txtEmail").val("");
    $("#txtTelephoneNo").val("");
    $("#txtFaxno").val("");
    $("#txtMobileNo").val("");
    $('#btnViewReq').css('display', 'none');
}
function ClearCMProjectData() {
    $("#txtProjectNumber").val("");
    setTimeout(function () { $("#ddlMaintDivision").data("kendoDropDownList").dataSource.read(); }, 500);
}
function ClearBuilding() {
    $("#txtL5No").val("");
    $("#txtL5Description").val("");
    $("#txtL5AltDescription").val("");
    return false;
}
function ClearLocation() {
    $("#LocationID").val("0");
    $("#txtLocationNo").val("");
    $("#txtLocationDescription").val("");
    $("#txtLocationAltDescription").val("");
    $("#txtNotesToTechLocation").val("");
    $("#txtLocationAuthEmployeeName").val("");
    $("#dvViewDiagram").hide();
}
function ClearAsset() {
    $("#txtAssetNumber").val("");
    $("#txtAuthEmployeeName").val("");
    $("#txtAssetStatusDesc").val("");
    $("#txtAltAssetStatusDesc").val("");
    $('#txtAssetWarrentyExpiryDate').val("");
    $('#txtAssetWarrentyStatus').val("");
    $('#txtNotesToTechAsset').val("");
    $('#txtAssetDescription').val("");
    $('#txtAltAssetDescription').val("");
}

function EnableDisableControlsEditCase(requestStatusID) {
    switch (requestStatusID) {
        case "8":
            //WORaised
            $('#bnGenerateWR').css('display', 'none');
            $('#btnCancelWR').css('display', 'none');
            $('#btnSave').css('display', 'none');
            $('#btnCloseJO').css('display', 'none');
            EnableDisableFields(false);
            break;
        case "1":
            //Open
            EnableDisableFields(true);
            break;
        case "3":
            //Cancelled
            $('#bnGenerateWR').css('display', 'none');
            $('#btnCancelWR').css('display', 'none');
            $('#btnSave').css('display', 'none');
            EnableDisableFields(false);
            break;
        default:
            return false;
    }
}
var enableDDL = true;
function EnableDisableFields(enable) {
    var grid = '';
    if (!enable) {
        enableDDL = false;
        try {
            $("#frmWorkOrders :input").prop("disabled", true);
            $("#btnCancelJO").removeAttr('disabled');
            $("#btnPrintJO11").removeAttr('disabled');
            $("#btnPrintJO21").removeAttr('disabled');
            $("#btnPrintJOWithAttachment1").removeAttr('disabled');

            $(".box-footer #btnCancelJO").removeAttr('disabled');
            $("#btnPrintJO1").removeAttr('disabled');
            $("#btnPrintJO2").removeAttr('disabled');
            $("#btnPrintJOWithAttachment").removeAttr('disabled');

            //(Start)Basic Info Tb
            $('#ProblemDesc').attr('disabled', 'disabled');
            $('#Remarks').attr('disabled', 'disabled');
            $('#txtRecievedDate').attr('disabled', 'disabled');
            $('#ddlReceivedTime').data("kendoTimePicker").enable(false);
            $('#txtRequiredDate').attr('disabled', 'disabled');
            $('#ddlRequiredTime').data("kendoTimePicker").enable(false);

            //$('#ddlJobType').data("kendoDropDownList").enable(false);
            $('#ddlJobType').data("kendoDropDownList").enable(false);
            $('#ddlJobPriority').data("kendoDropDownList").enable(false);
            $('#ddlJobTrade').data("kendoDropDownList").enable(false);
            $('#ddlMaintDivision').data("kendoDropDownList").enable(false);

            try {
                
                $('#btnOpenBuildingPopup').addClass('disabled');
                $('#btnClearBuilding').addClass('disabled');
                $('#txtL5Description').addClass('disabled');
                $('#btnOpenLocationPopup').addClass('disabled');
                $('#btnClearLocation').addClass('disabled');
                $('#txtLocationDescription').addClass('disabled');
                $('#btnOpenAssetPopup').addClass('disabled');
                $('#btnClearAsset').addClass('disabled');
                $('#txtAssetDescription').addClass('disabled');
            }
            catch (err) { }

            $('#btnOpenEmployeePopUp').addClass('disabled');
            $('#btnClearEmployee').addClass('disabled');
            $('#txtName').addClass('disabled');


            try { $('#btnOpenCMProjectPopUp').addClass('disabled'); } catch (err) { }
            try { $('#btnClearCMProject').addClass('disabled'); } catch (err) { }

            try { $('#btnOpenCIInfoPopUp').addClass('disabled'); } catch (err) { }
            try { $('#btnClearCGIInfo').addClass('disabled'); } catch (err) { }

            $('#btnSave').css('display', 'none');
            $('#divProject').css('display', 'none');
            //(End)Basic Info Tab
        } catch (e) { }

        try {
            //(Start)Assign To Tab
            try {
                $('#btnOpenAssignToSupplierPopUp').addClass('disabled');
                $('#txtAssignTOSupplierName').addClass('disabled');
            }
            catch (err) { }
            try { $('#btnClearAssignTOSupplier').addClass('disabled'); } catch (err) { }
            $('#btnSaveSupplier').css('display', 'none');
            $('#btnOpenAddAssignedToWindow').css('display', 'none');
            grid = $("#WOAssignToGrid").data("kendoGrid");
            grid.hideColumn("EmployeeID");
            //(End)Assign To Tab
        } catch (e) { }

        try {
            //(Start) JoClosing Tab
            $('#btnSaveJoClosing').css('display', 'none');
            $('#ddlJobStatusJoClosing').data("kendoDropDownList").enable(false);
            //(End) JoClosing Tab
        } catch (e) { }

        try {
            //(Start) Labour Tab
            $('#btnOpenAddLabourWindow').css('display', 'none');
            grid = $("#WOlaborGrid").data("kendoGrid");
            grid.hideColumn("WorkorderLaborID");
            //(End) Labour Tab
        } catch (e) { }

        try {
            //(Start) Direct Issue Tab
            $('#btnOpenDirectIssueWin').css('display', 'none');
            grid = $("#DirectIssueGrid").data("kendoGrid");
            grid.hideColumn("DirectID");
            //(End) Direct Issue Tab
        } catch (e) { }

        try {
            //(Start) Items Tab
            $('#btnOpenAddIssueWin').css('display', 'none');
            $('#btnOpenAddReturnWin').css('display', 'none');
            //(End) Items Tab
        } catch (e) { }

        try {
            //(Start) Tools Tab
            $('#btnOpenAddJOTool').css('display', 'none');
            grid = $("#WoRequiredTools").data("kendoGrid");
            grid.hideColumn(4);
            //(End) Tools Tab
        } catch (e) {
        }

        try {
            //(Start) PMCheckList Items Tab
            grid = $("#WOPmCheckListGrid").data("kendoGrid");
            grid.hideColumn(4);
            //(End) PMCheckList Items Tab
        } catch (e) { }

        try {
            //(Start) Notes Tab
            $('#btnSaveNotes').css('display', 'none');
            $('#txtWONotes').attr('disabled', 'disabled');
            //(End) Notes Tab
        } catch (e) { }

        try { //(Start) Attachments Tab
            $('#btnSave1').css('display', 'none');
            //(End) Attachments Tab
        } catch (e) { }

        try {
            //(Start) Safety Instr Tab
            try {
                $('#btnOpenSafetyInstrPopUp').addClass('disabled');
                $('#txtSafetyName').addClass('disabled');
            }
            catch (err) { }
            try { $('#btnClearSafetyInstr').addClass('disabled'); } catch (err) { }
            $('#btnSaveSafetyInstruction').css('display', 'none');
            $('#btnOpenAddSafetyInstrItemWin').css('display', 'none');
            grid = $("#WOJobSITaskGrid").data("kendoGrid");
            grid.hideColumn(3);
            //(End) Safety instr Tab
        } catch (e) {
        }

        try { //(Start) JOPlan Tab
            try { $('#btnOpenJobPlansPopUp').addClass('disabled'); } catch (err) { }
            try { $('#btnClearJobPlan').addClass('disabled'); } catch (err) { }
            $('#btnSaveJobPlan').css('display', 'none');
            $('#btnAddOpenJobPlanItemsWin').css('display', 'none');
            grid = $("#WOJobTaskGrid").data("kendoGrid");
            grid.hideColumn(3);
            //(End) JOPlan Tab
        } catch (e) { }

        try {  //(Start) PO/PR Tab
            $('#btnAddPurchaseReqPage').css('display', 'none');
            $('#btnAddPurchaseReqPage').attr('href', 'javascript:void(0)');

            $('#btnAddPurchaseOrderPage').css('display', 'none');
            $('#btnAddPurchaseOrderPage').attr('href', 'javascript:void(0)');
            //(End) PO/PR Tab
        } catch (e) { }

    }
}

function LoadvaluesToControl(requestStatusID) {
    if (parseInt(requestStatusID) == 3) //Cancelled
    {
        $('#btnAmmendJO').show();
        EnableDisableFields(false);
    }
    else if (parseInt(requestStatusID) == 2) //Closed)
    {
        EnableDisableFields(false);
    }
    else { EnableDisableFields(true); }
}
/*(End)Basic Information Tab*/

/*(Start)Assign TO Tab*/
function onDataBoundForWOAssignTO(arg) {
    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallbackWoAssinTO, confirmDirection);
    gridProperPaging(this);
}
function BindParametersForWOAssignTO() {
    return {
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function ClearAssignToEmployee() {
    $('#hdnAssignTOEmployeeID').val('');
    $("#txtAssignTOEmpNO").val('');
    $("#txtAssignTOEmpName").val('');
    //$("#txtAssignTOEstStartDate").val('');
   // $("#txtAssignTOEstEndDate").val('');
    $("#ddlEstStartTime").val('12:00 AM');
    $("#ddlEstEndTime").val('12:00 AM');
}

function ClearAssignToCheckBoxes() {
    $('#chkAuthEmpAsset').prop('checked', false);
    $('#chkAuthEmpLocation').prop('checked', false);
}

function onDataAssignToMaintGroup(arg) {
    gridProperPaging(this);
}

//for MaintGroups Auto complete
function onMaintenanceAdditionalData() {
    return {
        text: $("#txtAssignTOMaintgroupDesc").val(),
    };
}

function BindParametersAssignToMaintGroups() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function OpenAssignToMaintGroups() {
    $("#MaintGroupModal").modal("show");
}
function ClearAssignToMaintGroups() {
    $('#hdnAssignToMaintGroupID').val('');
    $("#txtAssignTOMaintgroupCode").val('');
    $("#txtAssignTOMaintgroupDesc").val('');
    $("#txtAssignTOMaintgroupAltDesc").val('');
}

//Supplier Modal
function OpenAssignTOSupplierPopUp() {
    try { $("#supplierAssignTOGridModal").modal("show"); } catch (err) { alert(err); }
}

//for Building Auto complete
function onSupplierAdditionalData() {
    return {
        text: $("#txtAssignTOSupplierName").val(),
    };
}

function BindParametersAssignTOSuppliers() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function onDataBoundAssignTOSupplier(arg) {
    gridProperPaging(this);
}

function ClearAssignToSupplier() {
    $('#hdnAssignToSupplierID').val("");
    $('#txtAssignToSupplierNO').val("");
    $('#txtAssignTOSupplierName').val("");
    $('#txtAssignTOAltSupplierName').val("");
}

function closeAssignTOSupplierModel() {
    $("#supplierAssignTOGridModal").modal("hide");
}

/*(End)Assign TO Tab*/

//(Start) JoClossing Tab
function ClearAcceptedBy() {
    $('#hdnAcceptedByEmpID').val('');
    $("#txtAcceptedByEmpNo").val('');
    $("#txtAcceptedByEmpName").val('');
    $("#txtAcceptedByEmpAltName").val('');
}

//(End) JoClossing Tab

//(start) Labor Tab
function onDataBoundForWOLabor(arg) {

    var TotalHour = 0;
    var TotalCost = 0;
    var grid = $("[id ^= 'WOlaborGrid']").data("kendoGrid");
    var gridData = grid.dataSource.view();
    for (var i = 0; i < gridData.length; i++) {
        TotalHour += parseFloat(gridData[i].TotHour);
        TotalCost += parseFloat(gridData[i].TotCost);
    }
    $("#txtTotalHours").val(TotalHour.toFixed(2));
    $("#txtTotalCost").val(TotalCost.toFixed(2));

    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallbackWOLabor, confirmDirection);
    gridProperPaging(this);
}

function BindParametersForWOLabor() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())
    };
}

function ClearLaborEmployee() {
    $('#hdnLaborEmployeeID').val('');
    $("#txtLaborEmpNo").val('');
    $("#txtLaborEmpName").val('');
    $("#txtLaborAltEmpName").val('');
    $('#txtLaborStartDate').val('');
    $('#ddlLaborStartTime').val('12:00 AM');
    $('#txtLaborEndDate').val('');
    $('#ddlEndTime').val('12:00 AM');
    $('#txtLaborComment').val('');
    $('#txtLaborNormalHours').val('0.00');
    $('#txtLborOT1Hours').val('0.00');
    $('#txtLborOT2Hours').val('0.00');
    $('#txtLborOT3Hours').val('0.00');
    $('#txtLaborHours').val('0.00');
    $('#txtLborCost').val('0.00');
    $("#hdnLaborHourlySalary").val('0');
    $("#hdnLaborOT1").val('0');
    $("#hdnLaborOT2").val('0');
    $("#hdnLaborOT3").val('0');

    //ClearAllInputs('frmWorkOrderLabors');
}

//(end) Labor Tab

//(Start) Items Tab
function onDataBoundForWOIssues(args) {
    gridProperPaging(this);
    setTotalReturnedCost();
}

function setTotalReturnedCost() {
    try {
        var TotalIssueCost = 0;
        var TotalReturnCost = 0;

        var grid = $("[id ^= 'WOIssuesGrid']").data("kendoGrid");
        var gridData = grid.dataSource.view();
        for (var i = 0; i < gridData.length; i++) {
            TotalIssueCost += parseFloat(gridData[i].TotalPrice);
        }

        grid = $("[id ^= 'WOReturnPartsGrid']").data("kendoGrid");
        gridData = grid.dataSource.view();
        for (var i = 0; i < gridData.length; i++) {
            TotalReturnCost += parseFloat(gridData[i].TotalPrice);
        }
        $("#txtTotalReturnCost").val((TotalIssueCost - TotalReturnCost).toFixed(2));
    }
    catch (err)
    { console.log('Error Occured While Calculating Total Returned Cost: ' + err); }
}
function BindParametersForWOIssues() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())
    };
}

function onDataBoundForWOParts(args) {
    gridProperPaging(this);
    setTotalReturnedCost();
}

function BindParametersForWOparts() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())
    };
}

function onDataBoundForWOTools(args) {
    try {
        gridProperPaging(this);
        confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallBackJoToolsDelete, confirmDirection);
    } catch (err) {
        alert(err);
    }
}

function BindParametersForWOTools() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        workTypeID: ($('#WorkTypeID').val() == '' || $('#WorkTypeID').val() == 'undefined' ? 0 : $('#WorkTypeID').val())
    };
}
function onDataBoundForWOPPE(args) {
    gridProperPaging(this);
}

function BindParametersForWOPPE() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        workTypeID: ($('#WorkTypeID').val() == '' || $('#WorkTypeID').val() == 'undefined' ? 0 : $('#WorkTypeID').val())
    };
}
function onDataBoundForWOItems(args) {
    gridProperPaging(this);
}

function BindParametersForWOItems() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        workTypeID: ($('#WorkTypeID').val() == '' || $('#WorkTypeID').val() == 'undefined' ? 0 : $('#WorkTypeID').val())
    };
}
//(End) Items Tab

//(Start) JoPlan Tab
function onDataBoundForWOJobPlanItems(arg) {
    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallbackJOPlan, confirmDirection);
    gridProperPaging(this);
}
function BindParametersForWOJobPlanItems() {
    return {
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function ClearWOJobPlans() {
    $('#hdnJOPlanID').val('');
    $("#txtJOPlanNo").val('');
    $("#txtJobPlanName").val('');
    $("#txtAltJobPlanName").val('');
}
//(End) JOPlan Tab
//(Start) Safety Instr Tab
function onDataBoundForWOSafetyInstrs(arg) {
    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallBackJoSafetyInstrDelete, confirmDirection);
    gridProperPaging(this);
}
function BindParametersForWOSafetyInstr() {
    return {
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val()),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}
function ClearWOJobPlans() {
    $('#hdnJOPlanID').val('');
    $("#txtJOPlanNo").val('');
    $("#txtJobPlanName").val('');
    $("#txtAltJobPlanName").val('');
}
//(End)  Safety Instr Tab
//(Start)Attachments Tab

function onDataBoundDocument(arg) {
    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallbackDocument, confirmDirection);
    gridProperPaging(this);
}

function BindParametersDocument() {
    return {
        foreignKeyID: $('#hdnFkId').val(),
        moduleType: $('#hdnDocumentModuleType').val(),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}

//(End) Attachemnts Tab
//start Purchase Request
function BindParametersForPurchaseReq() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        jobOrderID: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())
    };
}

function onDataBoundForPurchaseReq(arg) {

    gridProperPaging(this);
}
//end Purchase Request


//start Purchase order
function BindParametersForPurchaseOrder() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        jobOrderID: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())
    };
}

function onDataBoundForPurchaseOrder(arg) {

    gridProperPaging(this);
}
//end Purchase order

//start Direct Issue
function BindParametersDirectIssue() {
    return {
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val(),
        workOrderNo: ($('#WorkorderNo').val() == '' || $('#WorkorderNo').val() == 'undefined' ? 0 : $('#WorkorderNo').val())

    };
}
function onDataBoundDirectIssue(arg) {

    var total = 0.000;
    var grid = $("[id ^= 'DirectIssueGrid']").data("kendoGrid");
    var gridData = grid.dataSource.view();
    for (var i = 0; i < gridData.length; i++) {
        total += gridData[i].TotalPrice;
    }
    $("#txtSubTotalPrice").val(total.toFixed(2));

    confirmDeleteMsg(msgText, okText, cancelText, onConfirmCallback, confirmDirection);
    gridProperPaging(this);
}

//end Direct Issue 

function onDataBoundLocationDiagram(arg) {
    gridProperPaging(this);
}

function BindParametersLocationDiagram() {
    return {
        foreignKeyID: $('#hdnFkIdForLocationDiagram').val(),
        __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val()
    };
}