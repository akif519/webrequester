﻿var filtercnt = 1;
var $table = $("#customFilters");
var parentId = "";
var seqId = "";

function OpenAdvancedSearch(title, pageId) {
    $("#search-order").html('');
    var url = partialViewUrl + "?title=" + title.toString().replace(" ", "%20").replace(" ", "%20").replace(" ", "%20").replace(" ", "%20") + "&pageId=" + pageId;
    $("#search-order").load(url);
}

function addFilters() {
    
    var output = [];
    bindFieldTRDropDown(output);
    return false;
}

function bindFieldTRDropDown(nodes) {

    var tr = "<div number='" + filtercnt + "' class='row' id=rowfilters" + filtercnt + ">";

    tr += "<input type='hidden' id='hdnFilterId" + filtercnt + "'>";

    tr += "<div class='form-group'>";
    tr += "<div class='min-height-50'>";
    tr += "<select number=" + filtercnt + "  id=ddlfield" + filtercnt + " class='form-control ddlfield' data-live-search='true'></select>";
    tr += "</div>";
    tr += "</div>";

    tr += "<div class='form-group'>";
    tr += "<div class='min-height-50'>";
    tr += "<select number=" + filtercnt + " id=ddloperator" + filtercnt + "  class='form-control ddloperator'></select>";
    tr += "</div>";
    tr += "</div>";

    tr += "<div class='form-group'  id='divMultiSelectBoxHide'>";
    tr += "<div class='min-height-50" + projectCultureKendoClass + "'>";
    tr += "<input type='text' id='txtone" + filtercnt + "' class='form-control txtvalue txtone' required>";
    tr += "</div>";
    tr += "</div>";

    tr += "<div class='form-group' id='divMultiSelectBoxShow'>";
    tr += "<div class='min-height-50' id='divSelect" + filtercnt + "'  >";
    tr += "</div>";
    tr += "</div>";

    //tr += "<div id='divAnd" + filtercnt + "' class='col-lg-1 col-sm-1' style='display:none;'>";
    //tr += "<div class='min-height-50'>";
    //tr += "<span style='border:none;text-align:center;-webkit-box-shadow:none;' class='form-control'>And</span>";
    //tr += "</div>";
    //tr += "</div>";

    tr += "<div id='divBetween" + filtercnt + "' class='form-group' style='display:none;'>";
    tr += "<div class='min-height-50'>";
    tr += "<input type='text' id='txttwo" + filtercnt + "' class='form-control txtvalue txttwo' required>";
    tr += "</div>";
    tr += "</div>";

    //tr += "<div class='form-group'>";
    //tr += "<div class='frm-group'>";
    tr += "<button class='btn btn-default remove-btn icon-btn' type='button' onclick='deleteFilter(this);'><img src='../images/close.png' alt=''></button>";
    //tr += "</div>";
    //tr += "</div>";
    tr += "</div>";

    if ($table.children().length >= 1) {
        var trCond = "<div class='row filtercondition' id=rowcond" + filtercnt + "><div class='form-group'><select id=ddlCond" + filtercnt + " class='form-control'><option value='AND'>And</option><option value='OR'>OR</option></select></div>"
        $table.append(trCond);
    }

    $table.append(tr);

    var varOptions = "";

    $.each(filterFields, function (Key, dataX) {
        var strName = dataX.DisplayName
        varOptions += "<option type='" + dataX.DBColumnType + "' viewNo='3' value='" + dataX.SearchFieldID + "'>" + strName + "</option>";
    });

    $("#ddlfield" + filtercnt).append(varOptions);

    var varOperatorOpt = "";

    $.each(stringCondition, function (key, dataX) {
        varOperatorOpt += "<option value='" + dataX.value + "'>" + dataX.text + "</option>";
    });
    
    $("#ddloperator" + filtercnt).append(varOperatorOpt);
    
    seqId = filtercnt;
    parentId = "rowfilters" + seqId;
    $("#" + parentId).find("#divMultiSelectBoxHide").show();
    $("#" + parentId).find("#divMultiSelectBoxShow").hide();

    filtercnt++;

}

function deleteFilter(obj) {
    var currentRow = $(obj).closest("div[id*=rowfilters]");
    if (currentRow.prev().length == 0)
        currentRow.next("div[id*=rowcond]").remove();
    else
        currentRow.prev("div[id*=rowcond]").remove();
    currentRow.remove();
}

function GetFilterData(intPageID) {
    filtercnt = 1;
    $("#customFilters").html('');
    $(".multiSelectList").change();
    $.ajax({
        url: filterUrl,
        type: "POST",
        dataType: "json",
        async:true,
        data: addRequestVerificationToken({ pageID: intPageID }),
        success: function (result) {
            if (result.length > 0) {
                var itemCount = 0;
                while (itemCount < result.length) {
                    addFilters();
                    setTimeout(2200)
                    $("#ddlfield" + (itemCount + 1)).val(result[itemCount].SearchFieldID).change();

                    $("#ddloperator" + (itemCount + 1)).find("option[value=" + result[itemCount].OperatorID + "]").attr("selected", "selected").change();

                    if (itemCount > 0) {
                        $("#ddlCond" + (itemCount + 1)).find("option[value=" + result[itemCount - 1].Condition + "]").attr("selected", "selected");
                    }

                    if ($("#ddlfield" + (itemCount + 1) + ' option:selected').attr('type') == 5) {
                        var dataarray = result[itemCount].Value1.split(",");
                        // Set the value
                        $("#divSelect" + (itemCount + 1) + " #txtone" + (itemCount + 1)).val(dataarray);
                        // Then refresh
                        $("#divSelect" + (itemCount + 1) + " #txtone" + (itemCount + 1)).multiselect("refresh");
                    }
                    else if ($("#ddlfield" + (itemCount + 1) + ' option:selected').attr('type') == 4) {
                        var datepicker = $("#txtone" + (itemCount + 1)).data("kendoDatePicker");
                        if (datepicker != undefined)
                            datepicker.destroy();

                        $("#txtone" + (itemCount + 1)).kendoDatePicker({
                            format: dateFormat
                        });

                        $("#txtone" + (itemCount + 1)).data("kendoDatePicker").value(result[itemCount].Value1);

                        var datepicker = $("#txttwo" + (itemCount + 1)).data("kendoDatePicker");
                        if (datepicker != undefined)
                            datepicker.destroy();

                        $("#txttwo" + (itemCount + 1)).kendoDatePicker({
                            format: dateFormat
                        });

                        $("#txttwo" + (itemCount + 1)).data("kendoDatePicker").value(result[itemCount].Value2);

                    }
                    else {
                        $("#txtone" + (itemCount + 1)).val(result[itemCount].Value1);
                        $("#txttwo" + (itemCount + 1)).val(result[itemCount].Value2);
                    }

                    $("#hdnFilterId" + (itemCount + 1)).val(result[itemCount].SearchFilterID);

                    itemCount = itemCount + 1;
                }
                return true;
            }
            else {
                return false;
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            //showErrorMessage();
        }
    });
}

function ClearAdvencedSearch(pageid) {
    $.ajax({
        url: clearSearchUrl,
        type: "POST",
        dataType: "json",
        data: addRequestVerificationToken({ pageID: pageid }),
        success: function (result) {
            ReBindKendoGrid(kendoGridId);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //showErrorMessage();
        }
    });
}

$("body").on("change", ".ddlfield", function () {
    var sfieldType = $('option:selected', this).attr('type');

    var fieldType = "1";
    if (sfieldType == "nvarchar")
    {
        fieldType = "1";
    } else if (sfieldType == "int") {
        fieldType = "2";
    }
    else if (sfieldType == "decimal") {
        fieldType = "3";
    }
    else if (sfieldType == "datetime") {
        fieldType = "4";
    }
    else if (sfieldType == "multidropdown") {
        fieldType = "5";
    }

    var seqNumber = $(this).attr('number');
    seqId = seqNumber;

    parentId = "rowfilters" + seqId;
    MultiSelectShow(false);
    var OperatorOpt = "";

    switch (fieldType) {
        case "1":
            $.each(stringCondition, function (key, dataX) {
                OperatorOpt += "<option value='" + dataX.value + "'>" + dataX.text + "</option>";
            });
            break;
        case "2":
        case "3":
        case "4":
            $.each(numericCondition, function (key, dataX) {
                OperatorOpt += "<option value='" + dataX.value + "'>" + dataX.text + "</option>";
            });
            break;
        case "5":
            $.each(DropdownCondition, function (key, dataX) {
                OperatorOpt += "<option value='" + dataX.value + "'>" + dataX.text + "</option>";
            });

    }
    var inputs = $(this).closest("div[id*=rowfilters]").find(".txtvalue");

    inputs.val("");

    if (fieldType == "2") {
        inputs.addClass("allowIntegerOnly");
    }
    else {
        inputs.removeClass("allowIntegerOnly");
    }

    if (fieldType == "3") {
        inputs.addClass("allowNumberOnly");
    }
    else {
        inputs.removeClass("allowNumberOnly");
    }

    if (fieldType == "4") {
        inputs.each(function () {
            bindDatePicker($(this).attr("id"));
            $(this).attr("placeholder", "Select Date");
        });
    }

    if (fieldType == "5") {

        MultiSelectShow(true);
        //bindMultiSelectDropdown($("#ddlfield" + seqId).val());
    }

    $("#ddloperator" + seqNumber).html(OperatorOpt);

    $("#ddloperator" + seqNumber).trigger("change");
});

$("#btnSaveSearch").on("click", function () {
    var objCondition = [];
    var objCustomSearchFilter = [];

    $("#customFilters").find('div[id*=rowfilters]').each(function () {
        var obj = new Object();
        
        obj.FilterColumn = $(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).val();
        obj.OperatorId = $(this).find(".ddloperator").val();

        if ($(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).attr('type') == "5") {
            if ($(this).find("#divSelect" + $(this).attr("number") + " #txtone" + $(this).attr("number")).val() == null) {
                obj.Value1 = "";
            }
            else {
                obj.Value1 = $(this).find("#divSelect" + $(this).attr("number") + " #txtone" + $(this).attr("number")).val().join().replace('multiselect-all,', '').replace('multiselect-all', '');
            }
            obj.Value2 = $(this).find(".txttwo").val();
        }
        else if ($(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).attr('type') == "datetime") {
            obj.Value1 = $(this).find("#txtone" + $(this).attr("number")).val();
            if ($(this).find("#divBetween" + $(this).attr("number")).css('display') != "none")
                obj.Value2 = $(this).find("#txttwo" + $(this).attr("number")).val();
        }
        else {
            obj.Value1 = $(this).find(".txtone").val();
            obj.Value2 = $(this).find(".txttwo").val();
        }

        var nextElement = $(this).next();
        if (nextElement == undefined)
            obj.Condition = "undefined";
        else
            obj.Condition = nextElement.find("select").val();

        var objReportFilter = new Object();

        if ($("#hdnFilterId" + +$(this).attr("number")).val() != "") {
            objReportFilter.SearchFilterID = $("#hdnFilterId" + +$(this).attr("number")).val();
        }
        else {
            objReportFilter.SearchFilterID = 0;
        }

        objReportFilter.SearchFieldID = $(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).val();
        objReportFilter.EmployeeID = employeeId;
        objReportFilter.OperatorId = obj.OperatorId;
        objReportFilter.Value1 = obj.Value1;
        objReportFilter.Value2 = obj.Value2;
        objReportFilter.Condition = obj.Condition;
        objCustomSearchFilter.push(objReportFilter);

        objCondition.push(obj);
    });

    var customSearchFilterObject = JSON.stringify(objCustomSearchFilter);

    var objAdvanceSearch = {
        CustomReportFilter: customSearchFilterObject,
        PageID: intPageID
    };
    objAdvanceSearch.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    $.ajax({
        url: saveSearchUrl,
        type: "POST",
        daatType: "json",
        data: objAdvanceSearch,
        success: function (result) {
            $("#search-order").modal("hide");
            ReBindKendoGrid(kendoGridId);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showErrorMessage();
        }
    });
});

$("#btnSearch").on("click", function () {
    var objCondition = [];
    var objCustomSearchFilter = [];

    $("#customFilters").find('div[id*=rowfilters]').each(function () {
        var obj = new Object();

        obj.FilterColumn = $(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).val();
        obj.OperatorId = $(this).find(".ddloperator").val();

        if ($(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).attr('type') == "5") {
            if ($(this).find("#divSelect" + $(this).attr("number") + " #txtone" + $(this).attr("number")).val() == null) {
                obj.Value1 = "";
            }
            else {
                obj.Value1 = $(this).find("#divSelect" + $(this).attr("number") + " #txtone" + $(this).attr("number")).val().join().replace('multiselect-all,', '').replace('multiselect-all', '');
            }
            obj.Value2 = $(this).find(".txttwo").val();
        }
        else if ($(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).attr('type') == "4") {
            obj.Value1 = $(this).find("#txtone" + $(this).attr("number")).val();
            if ($(this).find("#divBetween" + $(this).attr("number")).css('display') != "none")
                obj.Value2 = $(this).find("#txttwo" + $(this).attr("number")).val();
        }
        else {
            obj.Value1 = $(this).find(".txtone").val();
            obj.Value2 = $(this).find(".txttwo").val();
        }

        var nextElement = $(this).next();
        if (nextElement == undefined)
            obj.Condition = "undefined";
        else
            obj.Condition = nextElement.find("select").val();

        var objReportFilter = new Object();

        if ($("#hdnFilterId" + +$(this).attr("number")).val() != "") {
            objReportFilter.SearchFilterID = $("#hdnFilterId" + +$(this).attr("number")).val();
        }
        else {
            objReportFilter.SearchFilterID = 0;
        }

        objReportFilter.SearchFieldID = $(this).find("#ddlfield" + $(this).attr("number")).find('option:selected', this).val();
        objReportFilter.EmployeeID = employeeId;
        objReportFilter.OperatorId = obj.OperatorId;
        objReportFilter.Value1 = obj.Value1;
        objReportFilter.Value2 = obj.Value2;
        objReportFilter.Condition = obj.Condition;
        objCustomSearchFilter.push(objReportFilter);

        objCondition.push(obj);
    });

    var customSearchFilterObject = JSON.stringify(objCustomSearchFilter);

    var objAdvanceSearch = {
        CustomReportFilter: customSearchFilterObject,
    };
    objAdvanceSearch.__RequestVerificationToken = $('input[name=__RequestVerificationToken]').val();

    $.ajax({
        url: saveSearchUrl,
        type: "POST",
        daatType: "json",
        data: objAdvanceSearch,
        success: function (result) {
            $("#search-order").modal("hide");
            ReBindKendoGrid(kendoGridId);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            showErrorMessage();
        }
    });
});

$("body").on("change", ".ddlDatePickerBetween", function () {
    var seqNumber = $(this).attr('number');

    if ($(this).val() == "1") {
        $("#divAnd" + seqNumber).show();
        $("#divBetween" + seqNumber).show();
    }
    else {
        $("#divAnd" + seqNumber).hide();
        $("#divBetween" + seqNumber).hide();
    }
});

function MultiSelectShow(value) {
    if (value) {
        $("#" + parentId).find("#divMultiSelectBoxHide").hide();
        $("#" + parentId).find("#divMultiSelectBoxShow").show();
    }
    else {
        $("#" + parentId).find("#divMultiSelectBoxHide").show();
        $("#" + parentId).find("#divMultiSelectBoxShow").hide();
    }

}

function bindMultiSelectDropdown()
{

}
function bindDatePicker(objid) {
    if (objid != undefined) {
        var datepicker = $("#" + objid).data("kendoDatePicker");
        if (datepicker != undefined)
            datepicker.destroy();

        $("#" + objid).kendoDatePicker({
            format: dateFormat,
        });
    }
}

$("body").on("change", ".ddloperator", function () {

    var fieldType = $('option:selected', this).attr('type');
    var seqNumber = $(this).attr('number');

    if ($(this).val() == "5") {
        $("#divAnd" + seqNumber).show();
        $("#divBetween" + seqNumber).show();
    }
    else {
        $("#divAnd" + seqNumber).hide();
        $("#divBetween" + seqNumber).hide();
    }
});