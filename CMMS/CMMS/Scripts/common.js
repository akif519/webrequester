﻿function ShowMessage(type, message, divId, fadOutTime) {
    fadOutTime = typeof fadOutTime !== 'undefined' ? fadOutTime : 5000;

    var imagepath = imageServerPath;

    var mainDiv = ' <div class="alert alert-' + type + ' alert-dismissible" role="alert">';
    mainDiv = mainDiv + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    mainDiv = mainDiv + ('<img src="' + imagepath + '" title="' + type + '"/>').replace("#type#", type) + '&nbsp;' + message;

    mainDiv = mainDiv + '</div>';

    $(divId).html(mainDiv);
    $(divId).slideDown('slow');

    if (type == dangerType && fadOutTime == 5000) {
        fadOutTime = 6000;
    }
    window.scrollTo(0, 0);
    setTimeout(function () { $(divId).slideUp('slow'); $(divId).html(''); }, fadOutTime);

}

function ShowMessageWithoutScroll(type, message, divId, fadOutTime) {
    fadOutTime = typeof fadOutTime !== 'undefined' ? fadOutTime : 5000;

    var imagepath = imageServerPath;

    var mainDiv = ' <div class="alert alert-' + type + ' alert-dismissible" role="alert">';
    mainDiv = mainDiv + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    mainDiv = mainDiv + ('<img src="' + imagepath + '" title="' + type + '"/>').replace("#type#", type) + '&nbsp;' + message;

    mainDiv = mainDiv + '</div>';

    $(divId).html(mainDiv);
    $(divId).slideDown('slow');

    if (type == dangerType && fadOutTime == 5000) {
        fadOutTime = 6000;
    }
    setTimeout(function () { $(divId).slideUp('slow'); $(divId).html(''); }, fadOutTime);

}

function FieldValidation(textbox, message) {
    if (textbox.value == '') {
        textbox.setCustomValidity(message);
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

function EmailValidation(textbox, message, message1) {
    if (textbox.value == '') {
        textbox.setCustomValidity(message);
    }
    else if (textbox.validity.typeMismatch) {
        textbox.setCustomValidity(message1);
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}

function OpenConfirmDialog(message, callbackfunction, data, height, width) {
    try {
        try { $('#loading').hide(); } catch (exc) { }
        if (typeof height == undefined || height == null || height <= 150)
            height = 150;

        if (typeof width == undefined || width == null || width <= 420)
            width = 420;

        var $div;
        if ($('#dialog-confirm').length) {
            $('#dialog-confirm').remove();
        }

        $div = $('<div ><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 1px 0;">' + '</span>' + message + '</div>').appendTo('body');
        $div.attr('id', 'dialog-confirm');
        //$div.html(message);
        $div.dialog({
            resizable: false,
            modal: true,
            title: "Confirmation",
            height: height,
            width: width,
            buttons: {
                "Yes": function () {
                    $(this).dialog('close');
                    callbackfunction(true, data);
                },
                "No": function () {
                    $(this).dialog('close');
                    callbackfunction(false, data);
                }
            }
        });
    } catch (err) {
        alert('FunctionName:OpenConfirmDialog , Error:' + err.message);
    }
}

function OpenConfirmDialogActive(message, callbackfunction, data, height, width) {
    try {

        if (typeof height == undefined || height == null || height <= 150)
            height = 150;

        if (typeof width == undefined || width == null || width <= 420)
            width = 420;

        var $div;
        if ($('#dialog-confirm').length) {
            $('#dialog-confirm').remove();
        }

        $div = $('<div ><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 1px 0;">' + '</span>' + message + '</div>').appendTo('body');
        $div.attr('id', 'dialog-confirm');
        //$div.html(message);
        $div.dialog({
            resizable: false,
            modal: true,
            title: "Confirmation",
            height: height,
            width: width,
            buttons: {
                "Activate now": function () {
                    $(this).dialog('close');
                    callbackfunction(true, data);
                },
                "Keep Inactive ": function () {
                    $(this).dialog('close');
                    callbackfunction(false, data);
                }
            }
        });
    } catch (err) {
        alert('FunctionName:OpenConfirmDialog , Error:' + err.message);
    }
}

$(document).ready(function () {

    jQuery.validator.addMethod("emailButAllowTrailingWhitespace", function (value, element) {
        return (this.optional(element) || jQuery.validator.methods.email.call(this, jQuery.trim(value), element));
    }, "Please enter a valid email");

    $('.IsDecimalNumber').keypress(function (event) {
        return isNumber(event, this)
    });

    function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    //this function is created for set focus on first element when pop up is opened
    $('.modal').on('shown.bs.modal', function (e) {
        $(this).find(":input").not(":button,:hidden").first().focus();
        $(document).off("focusin.modal");
        $(this).find("#divMessageForgot").html('');
        CreateCalendarsPicker();
    });

    $('.modal').on('hide.bs.modal', function (e) {
        if (validator != "")
            validator.resetForm();
        //ClearAllInputs($(this).find("form").attr("id"));
        $(this).find(".kendoDrpRequired").removeClass("kendoDrpRequired");
    });


    $(".allowIntegerOnly").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    $(".allowNumberOnly").keydown(function (e) {
        // allow: backspace, delete, tab, escape, enter and .
        //var arr = [46, 8, 9, 27, 13, 110, 190];
        var arr = [46, 8, 9, 27, 13];

        //if ($(this).val().indexOf(".") >= 0) {
        //    arr = [46, 8, 9, 27, 13]
        //}

        if ($.inArray(e.keyCode, arr) !== -1 ||
            // allow: ctrl+a, command+a
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }

        // ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });
    $(":input[type='text'],textarea").keypress(function (e) {

        if ($.inArray(e.keyCode, [60, 62]) > -1) {
            e.preventDefault();
        }
    });
    $(":input[type='text'],textarea").change(function (e) {
        var str = $(this).val();

        if (str.indexOf("<") > -1) {
            str = str.replace(/</g, '');
            $(this).val(str);
        }
        if (str.indexOf(">") > -1) {
            str = str.replace(/>/g, '');
            $(this).val(str);
        }
    });

    $('.decimalnumber').on('keypress', function (evt) {
        var val = $(this).val();
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode != 46 && iKeyCode != 39 && iKeyCode != 37 && iKeyCode != 45 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) {
            return false;
        }
        if (iKeyCode == 46 && val.indexOf('.') > -1) {
            return false;
        }
        return true;
    });

    CreateCalendarsPicker();

    //$('.k-widget.k-grid').data().kendoGrid.bind('dataBound', function (e) {
    //    this.element.find('tbody tr:first').addClass('k-state-selected')
    //})

    var touchtime = 0;
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(".k-widget.k-grid tbody").on("click", "tr[role='row']", function (e) {
            if (touchtime == 0) {
                touchtime = new Date().getTime();
            } else {
                if (((new Date().getTime()) - touchtime) < 3000) {
                    touchtime = 0;
                    $(this).dblclick();
                } else {
                    touchtime = new Date().getTime();
                }
            }
        });
    }
    else {
        touchtime = 0;
    }

});

//clear all input of the form/reset form
function ClearAllInputs(formId) {
    var isMultiselect = false;


    $('#' + formId).trigger("reset");

    // $('#' + formId).trigger("reset");




    $('#' + formId + ' select.multiSelectList.form-control').each(function () {

        $(this).multiselect('destroy');
        isMultiselect = true;

    });

    //alert(isMultiselect);
    //$(formId).find('input:text, input:password, input:file, select, textarea').val('');
    //$(formId).find('input:radio, input:checkbox').prop('checked', false).prop('selected', false);
    $('#' + formId + ' input[type="checkbox"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.prop('checked', false).prop('selected', false);;
    });

    $('#' + formId + ' input[type="radio"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.prop('checked', false).prop('selected', false);;
    });


    $('#' + formId + ' input[type="text"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.val("");
    });

    $('#' + formId + ' input[type="textarea"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.val("");
    });

    $('#' + formId + ' input[type="select"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.val("");
    });

    //$('#' + formId + ' [data-role="dropdownlist"]').each(function () {
    //    var dropdownlist = $("#" + $(this).attr("id")).data("kendoDropDownList");
    //    dropdownlist.value(0);
    //});

    if (isMultiselect) {
        $(".multiSelectList").multiselect({
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 250,
            buttonWidth: '100%',
            buttonClass: 'form-control',
            numberDisplayed: 1,
            onChange: function (option, checked, select) {
            }
        });

    }

    //$('#' + formId).find(":hidden").val('');

    $('#' + formId + ' input[type="hidden"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.val("");
    });

    $('#' + formId + ' input[type="file"]').each(function () {
        var file = $("#" + $(this).attr("id"));
        file.val("");
    });

}

function gridProperPaging(object) {
    if (object.dataSource.view().length == 0) {
        var currentPage = object.dataSource.page();
        if (currentPage > 1) {
            object.dataSource.page(currentPage - 1);
        }
    }

    if (object.dataSource.total() == 0) {
        var colCount = object.columns.length;
        if (colCount > 5) {
            $(object.wrapper)
                .find('tbody')
                .append('<tr class="kendo-data-row"><td colspan="5">No Records Found</td></tr>');
        }
        else {
            $(object.wrapper)
             .find('tbody')
             .append('<tr class="kendo-data-row"><td colspan="' + colCount + '">No Records Found</td></tr>');
        }
        if (isNotUndefinedNullOrBlankOrZero(object, false) && isNotUndefinedNullOrBlankOrZero(object.pager) && isNotUndefinedNullOrBlankOrZero(object.pager.element)) {
            object.pager.element.hide();
        }
    }
    else {
        if (object.pager != undefined) {
            object.pager.element.show();
        }
    }
}

//rebind kendo grid
function ReBindKendoGrid(gridId) {
    try {

        $('#' + gridId).data('kendoGrid').dataSource.read();
        $('#' + gridId).data('kendoGrid').refresh();
        IsAdvanceSearch();
    }
    catch (ex) {
    }

}

//show delete popover/confirm message 
function confirmDeleteMsg(title, okLabel, cancelLabel, onConfirmCallback, direction) {
    $('.delete-button').confirmation({
        title: title,
        popout: true,
        singleton: true,
        html: false,
        href: "javascript:void(0);",
        placement: direction,
        btnOkLabel: okLabel,
        btnCancelLabel: cancelLabel,
        onConfirm: onConfirmCallback,
        onCancel: onCancel,
        btnOkClass: "btn btn-primary",
        btnCancelClass: "btn btn-default"
    });
}

//Revalidate Kendo Dropdown
function reValidateDropDowns(frm) {
    var drp = $(frm).find("[data-role='dropdownlist']");
    var isSuccess = true;
    $(drp).each(function (index) {
        var item = $(this);
        if (item.attr("required") == "required" && item.val() == "") {
            $(item).prev(".k-dropdown-wrap").addClass("kendoDrpRequired");
            $(item).focus();
            bindChangeEvent($(item).attr("id"));
            isSuccess = false;
        }
    });
    return isSuccess;
}

function bindChangeEvent(drp) {
    $("#" + drp).change(function (e) {
        if ($(this).val() != "") {
            $(this).prev(".k-dropdown-wrap").removeClass("kendoDrpRequired");
            $(this).parent(".k-picker-wrap").removeClass("kendodate");
        }
    })
}

function onCancel() {
    $('.delete-button-Innner').confirmation('hide');
}

//function ExportToExcel(gridId) {
//    $("#" + gridId).getKendoGrid().hideColumn("SectorID");
//    $("#" + gridId).getKendoGrid().saveAsExcel();
//    setTimeout(function () { $("#" + gridId).getKendoGrid().showColumn("SectorID"); }, 1000);

//    return false;
//}
var gid;
function ExportToExcel(gridId, Id) {
    colID = Id;
    exportFlag = false;
    gid = gridId;
    $("#" + gridId).getKendoGrid().hideColumn(Id);
    $("#" + gridId).getKendoGrid().saveAsExcel();
    //$('#' + gridId).find('[role="grid"]').attr('style', 'width:100%');
    //setTimeout(function () { $("#" + gridId).getKendoGrid() });
    return false;
}


function excelExport(e) {

    if (!exportFlag) {
        //e.sender.saveAsExcel();
        e.sender.showColumn(colID);
        $('#' + gid).find('[role="grid"]').attr('style', 'width:100%');
        exportFlag = true;

    } else {
        exportFlag = false;
    }
}

function ExportToExcelAsset(gridId, Id) {
    $("#" + gridId).getKendoGrid().hideColumn(Id);
    $("#" + gridId).getKendoGrid().saveAsExcel();
    setTimeout(function () { $("#" + gridId).getKendoGrid().showColumn(Id); }, 10000);
    return false;
}

//show delete popover/confirm message 
function confirmDeleteInnerMsg(title, okLabel, cancelLabel, onConfirmCallback, direction) {
    $('.delete-button-Innner').confirmation({
        title: title,
        popout: true,
        singleton: true,
        html: false,
        href: "javascript:void(0);",
        placement: direction,
        btnOkLabel: okLabel,
        btnCancelLabel: cancelLabel,
        onConfirm: onConfirmCallback,
        onCancel: onCancel,
        btnOkClass: "btn btn-primary",
        btnCancelClass: "btn btn-default"
    });
}


////------------------------------------------------------Date Picker--------------------------------------//
//function CalculateTargetDate(date,id)
//{
//    alert('comon');
//}

// Create CalendarsPicker Controls
function CreateCalendarsPicker() {

    var myculture = $('#hiddenDateCulture').val();
    if (myculture != 'undefined') {
        $(".calendarsPicker").each(function () {
            if ($(this).has("maxdate")) {
                if (!$(this).attr("readonly")) {
                    $(this).calendarsPicker({
                        calendar: $.calendars.instance(myculture),
                        //onClose: calendarsPickerOnClose,
                        onSelect: function (date) { CalculateTargetDate(date[0].formatDate('yyyy/mm/dd'), $(this).attr("id")) },
                        dateFormat: $("#hiddenDefaultDateFormat").val().toLowerCase(),
                        maxDate: $(this).attr("maxdate")
                    });
                }
            }
            else {
                $(this).calendarsPicker({
                    calendar: $.calendars.instance(myculture),
                    //onClose: calendarsPickerOnClose,
                    // onSelect: function (date) { CalculateTargetDate(); },
                    dateFormat: $("#hiddenDefaultDateFormat").val().toLowerCase()
                });
            }


        });

        $(".calendarsPicker").attr("maxlength", $("#hiddenDefaultDateFormat").val().length)// Maxlength
        $(".calendarsPicker").each(function () {
            if (!$(this).next().next().hasClass("date-conversation")) {
                $(this).parent().append('<span id="ApprovalDate" class="k-button date-conversation k-state-disabled" style="" type="text" readonly="ReadOnly" placeholder="yyy/mm/dd"/>');
                $(this).bind("change", function () {
                    DateConversation($(this).val(), this);
                });
                setInterval('$(".calendarsPicker").trigger("change");', 500);
                //$(".calendarsPicker").trigger("change");
            }
        });

        $(".calendarsPicker").each(function () {
            $(this).on('focusout', function () {
                if ($(this).val() != "") {

                    var a = moment($(this).val(), 'YYYY/MM/DD', true).isValid();
                    //var a = moment.lang('ar_SA')($('#CR_ExpiryDate').val()).format('YYYY/MM/DD').isValid();
                    if (!a) {
                        $(this).val("");
                    }
                }
            });
        });

    }
}


// set converted date
function DateConversation(dates, calendarPiker) {

    try {

        if ($(calendarPiker).val().length > 0) {

            var culture = $('#hiddenDateCulture').val() != 'ummalqura' ? 'ummalqura' : 'gregorian';
            var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
            dates = new Date(dates);
            var calendar = $.calendars.instance($('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian');
            var date = calendar.parseDate(format, dates.getFullYear() + "/" + Number(dates.getMonth() + 1) + "/" + dates.getDate());

            calendar = $.calendars.instance(culture);
            date = calendar.fromJD(date.toJD());

            date = calendar.formatDate(format, date);
            $(calendarPiker).parent().find(".date-conversation").html(date);
        }
        else {
            $(calendarPiker).parent().find(".date-conversation").html("");
        }

    }
    catch (ex) {
    }
}
function GetCultureDateWithFormant(objectdate) {
    if (objectdate != undefined && objectdate != null && objectdate != 'Invalid Date') {
        var culture = $('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian';
        var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
        var date = objectdate;
        var calendar = $.calendars.instance('gregorian');
        var date = calendar.parseDate(format, date.getFullYear() + '/' + Number(date.getMonth() + 1) + '/' + date.getDate());
        calendar = $.calendars.instance(culture);
        date = calendar.fromJD(date.toJD());
        date = calendar.formatDate(format, date);
        return date;
    }
    else {
        return "";
    }
}

function DateTemplate(date) {

    if (date != undefined && date != null) {
        try {
            if (date.indexOf('/Date(') > -1) {
                date = new Date(parseInt(date.substr(6)));
            }
        } catch (err) { //do not throw err here
        }
        return GetCultureDateWithFormant(date);
    }
    return "";
}

function DateTemplateWithTime(date) {


    if (date != undefined && date != null) {
        try {
            if (date.indexOf('/Date(') > -1) {
                date = new Date(parseInt(date.substr(6)));
            }
        } catch (err) { //do not throw err here
        }
        var a = GetCultureDateWithFormant(date);

        var b = formatAMPM(date)
        return a + " " + b;
    }
    return "";
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function SetJsondate(jsondate) {
    var culture = $('#hiddenDateCulture').val() == 'ummalqura' ? 'ummalqura' : 'gregorian';
    var format = $("#hiddenDefaultDateFormat").val().toLowerCase();
    var date;
    if (jsondate != null && jsondate.toString().indexOf("Date") != -1) {
        date = eval(jsondate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
    }
    else {
        date = eval(jsondate);
    }
    //var date = eval(jsondate.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));
    var calendar = $.calendars.instance('gregorian');
    var date = calendar.parseDate(format, date.getFullYear() + '/' + Number(date.getMonth() + 1) + '/' + date.getDate());
    calendar = $.calendars.instance(culture);
    date = calendar.fromJD(date.toJD());
    date = calendar.formatDate(format, date);
    //alert(date);
    //$('#formatDate').val(calendar.formatDate(formats[$('#formatFormat').val()], date));
    return date;
}
//------------------------------------------------------End Date Picker--------------------------------------//
function isNotUndefinedNullOrBlankOrZero(id, blnCheckZeroAlso) {
    try {
        if (blnCheckZeroAlso == 'undefined' || blnCheckZeroAlso == null || blnCheckZeroAlso == '') {
            blnCheckZeroAlso = false;
        }

        var IsZero = null;

        if (typeof $('#' + id).val() != 'undefined' && $('#' + id).val() != null && $('#' + id).val() != '') {
            if (blnCheckZeroAlso) {
                IsZero = ($('#' + id).val() == 0 || $('#' + id).val() == '0') ? false : true;

                return IsZero;
            }
            return true;
        }
        else {
            return false;
        }
    } catch (err) {
        return false;
    }
}

function DecimalTempalte(data, NoOfPlace) {
    if (data != undefined && data != null) {
        return data.toFixed(NoOfPlace);
    }
    else {
        return "";
    }
}


function validateDate(dateVal) {
    var dateVal = dateVal;

    if (dateVal == null)
        return false;

    var validatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;

    dateValues = dateVal.match(validatePattern);

    if (dateValues == null)
        return false;

    var dtYear = dateValues[1];
    dtMonth = dateValues[3];
    dtDay = dateValues[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }

    return true;
}

function getFormattedDateBy(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + '/' + month + '/' + day;
}

function itemTemplateForStatus(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Status#'/><span>#= Status ? active : notActive #</span></label></span><br>"
    }
}

function itemTemplateForIsDefault(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=IsDefault#'/><span>#= IsDefault  ? isDefault : isNotDefault #</span></label></span><br>"
    }
}

function itemTemplateForDisplayInModule(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=DisplayInModule#'/><span>#= DisplayInModule  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForIsActive(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=IsActive#'/><span>#= IsActive ? active : notActive #</span></label></span><br>"
    }
}

function itemTemplateForMandatory(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Mandatory#'/><span>#= Mandatory  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForPrinting(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=IsPrintingEnable#'/><span>#= IsPrintingEnable  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForIsFinalLevel(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=IsFinalLevel#'/><span>#= IsFinalLevel  ? isFinalLevel : isNotFinalLevel #</span></label></span><br>"
    }
}


function itemTemplateForSunday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Sunday#'/><span>#= Sunday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForMonday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Monday#'/><span>#= Monday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForSaturday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Saturday#'/><span>#= Saturday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForFriday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Friday#'/><span>#= Friday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForThursday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Thursday#'/><span>#= Thursday  ? txtYes : txtNo #</span></label></span><br>"
    }
}


function itemTemplateForWednesday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Wednesday#'/><span>#= Wednesday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForTuesday(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Tuesday#'/><span>#= Tuesday  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForCriticalityID(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=CriticalityID#'/><span>#= CriticalityID  ? txtYes : txtNo #</span></label></span><br>"
    }
}

function itemTemplateForItemStatus(e) {
    if (e.field == "all") {
        return "<div><label><strong><input type='checkbox' /><span>#= all#</span></strong></label></div>";
    } else {
        return "<span><label><input type='checkbox' name='" + e.field + "' value='#=Status#'/><span>#= Status == '1' ? active : notActive #</span></label></span><br>"
    }
}