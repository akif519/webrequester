﻿function ShowMessageForDashboard(type, message, divId, fadOutTime) {
    fadOutTime = typeof fadOutTime !== 'undefined' ? fadOutTime : 5000;

    var imagepath = imageServerPath;

    var mainDiv = ' <div class="alert alert-' + type + ' alert-dismissible" role="alert">';
    mainDiv = mainDiv + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
    mainDiv = mainDiv + ('<img src="' + imagepath + '" title="' + type + '"/>').replace("#type#", type) + '&nbsp;' + message;

    mainDiv = mainDiv + '</div>';

    $(divId).html(mainDiv);
    $(divId).slideDown('slow');

    if (type == dangerType && fadOutTime == 5000) {
        fadOutTime = 6000;
    }
    setTimeout(function () { $(divId).slideUp('slow'); $(divId).html(''); }, fadOutTime);

}

/* Print PopUp Grid Start */

function printPopUpGrid(gridID) {
    var gridElemntByID = $('#' + gridID).find(".k-grid").attr("id");
    var chartTitle = $("#" + gridID).parent().parent().parent().parent().parent().find("#lblModalGridTitle").html();
    var gridElement = $('#' + gridElemntByID),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
        doc = win.document.open();

    var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>' + chartTitle + '</title>' +
            '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grouping-header, .k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
            // '.k-grid-pager { display: none; }' + // optional: hide the whole pager
            '</style>' +
            '</head>' +
            '<body>';

    var htmlEnd =
            '</body>' +
            '</html>';

    var gridHeader = gridElement.children('.k-grid-header');
    if (gridHeader[0]) {
        var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
        printableContent = gridElement
            .clone()
                .children('.k-grid-header').remove()
            .end()
                .children('.k-grid-content')
                    .find('table')
                        .first()
                            .children('tbody').before(thead)
                        .end()
                    .end()
                .end()
            .end()[0].outerHTML;
    } else {
        printableContent = gridElement.clone()[0].outerHTML;
    }

    doc.write(htmlStart + printableContent + htmlEnd);
    doc.close();
    win.print();
}

/* Print PopUp Grid End */

/* Print Grid Start */

function printGridByGridID(gridID) {
    var chartTitle = $("#" + gridID).parent().parent().parent().parent().find(".sub-title").find("h5").html();
    var gridElement = $('#' + gridID),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
        doc = win.document.open();

    var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>' + chartTitle + '</title>' +
            '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grouping-header, .k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
            '.customprogress {border-radius:1px; margin-bottom:0px;}' +
            '.customprogress .progress-bar-open {background-color:#cc3300 !important;}' +
            '.customprogress .progress-bar-closep {background-color:#006600 !important;}' +
            '.customprogress .progress-bar-cancel {background-color:#007a99 !important;}' +
            // '.k-grid-pager { display: none; }' + // optional: hide the whole pager
            '</style>' +
            '</head>' +
            '<body>';

    var htmlEnd =
            '</body>' +
            '</html>';

    var gridHeader = gridElement.children('.k-grid-header');
    if (gridHeader[0]) {
        var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
        printableContent = gridElement
            .clone()
                .children('.k-grid-header').remove()
            .end()
                .children('.k-grid-content')
                    .find('table')
                        .first()
                            .children('tbody').before(thead)
                        .end()
                    .end()
                .end()
            .end()[0].outerHTML;
    } else {
        printableContent = gridElement.clone()[0].outerHTML;
    }

    doc.write(htmlStart + printableContent + htmlEnd);
    doc.close();
    win.print();
}

function printGridByActivityGridID(gridID) {
    var chartTitle = $("#" + gridID).parent().parent().parent().parent().find(".sub-title").find("h5").html().replace(/<\/?[^>]+(>|$)/g, "");
    var gridElement = $('#' + gridID),
        printableContent = '',
        win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
        doc = win.document.open();

    var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>' + chartTitle + '</title>' +
            '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grouping-header, .k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
            '.customprogress {border-radius:1px; margin-bottom:0px;}' +
            '.customprogress .progress-bar-open {background-color:#cc3300 !important;}' +
            '.customprogress .progress-bar-closep {background-color:#006600 !important;}' +
            '.customprogress .progress-bar-cancel {background-color:#007a99 !important;}' +
            // '.k-grid-pager { display: none; }' + // optional: hide the whole pager
            '</style>' +
            '</head>' +
            '<body>';

    var htmlEnd =
            '</body>' +
            '</html>';

    var gridHeader = gridElement.children('.k-grid-header');
    if (gridHeader[0]) {
        var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
        printableContent = gridElement
            .clone()
                .children('.k-grid-header').remove()
            .end()
                .children('.k-grid-content')
                    .find('table')
                        .first()
                            .children('tbody').before(thead)
                        .end()
                    .end()
                .end()
            .end()[0].outerHTML;
    } else {
        printableContent = gridElement.clone()[0].outerHTML;
    }

    doc.write(htmlStart + printableContent + htmlEnd);
    doc.close();
    win.print();
}

/* Print Grid End */

/* Date Format Validation Start */

function validateDateFormat(dateVal) {
    var dateVal = dateVal;

    if (dateVal == null)
        return false;

    var validatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;

    dateValues = dateVal.match(validatePattern);

    if (dateValues == null)
        return false;

    var dtYear = dateValues[1];
    dtMonth = dateValues[3];
    dtDay = dateValues[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }

    return true;
}

/* Date Format Validation End */

/* Print Graph Start */

function PrintGraph(divID) {
    var chartHtml = $("#" + divID).html();
    var chartTitle = $("#" + divID).parent().parent().parent().find(".sub-title").find("h5").html();
    var mywindow = window.open('', 'new div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>' + chartTitle + '</title>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div>');
    mywindow.document.write(chartHtml);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
}

function PrintPopUpGraph(divID) {
    var chartHtml = $("#" + divID).html();
    var chartTitle = $("#" + divID).parent().parent().parent().parent().find("#lblModalTitle").html();
    var mywindow = window.open('', 'new div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>' + chartTitle + '</title>');
    mywindow.document.write('</head><body>');
    mywindow.document.write('<div>');
    mywindow.document.write(chartHtml);
    mywindow.document.write('</div>');
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
}

/* Print Graph End */
function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


/*   */
function chartDivOpen(divID, chartId, title) {
    var chartHtml = $("#" + divID).html();
    chartHtml = chartHtml.replace(chartId + '"', chartId + 'Model"');
    chartHtml = chartHtml.replace(chartId + '"', chartId + 'Model"');
    $("#lblModalTitle").html(title);
    $("#divChart").html(chartHtml);
}

function gridDivOpen(divID, chartId, title) {
    var chartHtml = $("#" + divID).html();
    chartHtml = chartHtml.replace(chartId + '"', chartId + 'Model"');
    chartHtml = chartHtml.replace(chartId + '"', chartId + 'Model"');
    $("#lblModalGridTitle").html(title);
    $("#divGrid").html(chartHtml);
}

function RebindKendoChart(chartID) {
    $('#' + chartID).data('kendoChart').dataSource.read();
    //$('#' + chartID).data('kendoChart').refresh();
}

function onDataBound(arg) {
    gridProperPaging(this);
}
