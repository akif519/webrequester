

// tab
function ABC(obj) {
    $(obj).parent().toggleClass("Open");
}

// ================================   
$(document).ready(function () {

    //$(".pm-checklist").keyup(function () {
        
    //    var txtid = $(this).find('[data-role="autocomplete"]').attr('id');

    //    var wd = $(this).innerWidth();
        
        
       
        
    //    $(".k-list-container.k-popup.k-group.k-reset").innerWidth(wd - 2);
        
    //    var pos = $('#' + txtid).parent().parent().parent().position();
    //    //var pos2 = $(".k-animation-container").css("left", pos.left + "px");
    //    $(".k-animation-container").css("left", pos.left + "px");
    
    //    //if (pos2 != null)
    //    //{
    //    //    var l = pos2.left - pos.left - 15;
    //    //    $("#" + txtid + "-list").css("margin-left", "-" + l +"px");
    //    //}
        
        
        
    //   // var leftPositon = $("#" + txtid + "-list").closest(".k-animation-container").position();
    //  //  alert(leftPositon);
        
    //    //$(".k-list-container.k-popup.k-group.k-reset").hasClass(".k-animation-container").innerWidth(wd - 2);
    //});


    //Hide left menu  By: Akif519
    $(".menu-left").find(".active").removeClass();
    $("#main").toggleClass("active");
    $("#wrapper").toggleClass("footspac");

    $(".menu-left").find(".active").removeClass();

    // Sub nav Open
    $(".menu-left li a").click(function () {
        var $this = $(this).closest('li');
        if ($this.hasClass('active')) {
            $this.removeClass('active').find('.active').removeClass('active');
            $this.find(".sub-menu-nav").stop(true, true).slideUp();
        } else {
            $this.siblings('li').removeClass('active').find(".sub-menu-nav").stop(true, true).slideUp().find('li.active').removeClass('active');
            $this.addClass('active');
            $this.find(">.sub-menu-nav").stop(true, true).slideDown();
        }
        if (intPageID != 'undefined') {
            $('a[data-action="' + intPageID + '"]').closest("li").addClass('active');
        }
    });

    if (intPageID != 'undefined') {
        $('a[data-action="' + intPageID + '"]').closest("li").addClass('active');
        $('a[data-action="' + intPageID + '"]').closest("li").parents(".dro-menu").addClass('active');

        $('a[data-action="' + intPageID + '"]').closest("li").parents(".sub-menu-nav").show();
    }

    //fast click for touch devices  
    FastClick.attach(document.body);
});

// ================================
$(document).ready(function ($) {

    $(".theme-colors .btn").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if (!$this.hasClass('active')) {
            $this.addClass('active').siblings('.btn').removeClass('active');
            $("body").attr("class", $this.attr("data-color"));
        }

    });

    var xx = 0.8;
    var yy = 1 / $(".left-menu ul li").length;
    $(".left-menu ul li").each(function () {
        $(this).find("a").css("background-color", "rgba(0, 0, 0, " + xx + ")");
        xx = xx - yy;

    });
});


// ================================



// myscrl
$(function () {
    $('#myscrl').slimScroll({
        height: '100%',
    });
});



// menu-toggle
$("#menu-toggle").click(function (e) {    
    e.preventDefault();
    $("#main").toggleClass("active");
    $("#wrapper").toggleClass("footspac");
    
});


// date-pick ==============================

$(document).ready(function () {
    //$(".date-pick").each(function () {
    //    var $this = $(this);
    //    $this.find(".form-control").datepicker({
    //        format: "dd/mm/yyyy",
    //        autoclose: true
    //    }).on("show", function () {
    //        $(".datepicker").click(function (e) {
    //            e.stopImmediatePropagation();
    //        });
    //    });

    //});


    $(".filter-dropdown-menu .form-horizontal, .language-block .form-control").on("click", function (e) {
        e.stopImmediatePropagation();
    });
});


//// Equalheight ==============================

equalheight = function (container) {

    var currentTallest = 0,
	currentRowStart = 0,
	rowDivs = new Array(),
	$el,
	topPosition = 0;
    $(container).each(function () {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function () {
    equalheight('.dashboard .box');
});


$(window).resize(function () {
    equalheight('.dashboard .box');
});

function KendoToolTipTemplate(value) {
    var htmlForDDLTemplate = '';

    if (value != 'undefined' && value != null && $.trim(value) != '') { ///Three Column Table
        htmlForDDLTemplate = kendo.format("{0}",
              value
          );
    }
    else { htmlForDDLTemplate = ''; }
    return htmlForDDLTemplate;
}

function KendoDDLTemplate(code, enName, arName) {
    var htmlForDDLTemplate = '';

    if (code != 'undefined' && code != null && $.trim(code) != '') { ///Three Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table'><span class='code-no'>{0}</span><span class='english'>{1}</span><span class='arabic'>{2}</span></div>",
              code == null ? '' : code,
              enName == null ? '' : enName,
              arName == null ? '' : arName
          );
    }
    else {
        ///Two Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table'><span class='english'>{0}</span><span class='arabic'>{1}</span></div>",
              enName == null ? '' : enName,
              arName == null ? '' : arName
          );
    }

    return htmlForDDLTemplate;
}

function KendoDDLTemplateForTwoColumn(code, enName) {
    var htmlForDDLTemplate = ''; 
    htmlForDDLTemplate = kendo.format("<div class='ddl-table'><span class='english'>{0}</span><span class='onlyenglish'>{1}</span>",
              code == null ? '' : code,
              enName == null ? '' : enName
          );
    return htmlForDDLTemplate;
}

function KendoDDLHeaderTemplate(type) {
    var htmlForDDLTemplate = '';

    if (type == 1) { ///Three Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table ddl-table-hed'><span class='code-no'>Code</span><span class='english'>English</span><span class='arabic'>Arabic</span></div>");
    }
    else {
        ///Two Column Table
        htmlForDDLTemplate = kendo.format("<div class='ddl-table ddl-table-hed'><span class='english'>English</span><span class='arabic'>Arabic</span></div>");
    }
    
    return htmlForDDLTemplate;
}

function GetImage(image) {
    var returnString = '';
    if (image != 'undefined' && image != null && $.trim(image) != '')
    {
        var returnString = '<img src="' + image + '" height=\"24\" />';
    }
    else
    {
        var returnString = '<img src="../Images/default_item.png"  height=\"24\" />';
    }
    return returnString;
}

function GetUserImage(image) {
    var returnString = '';
    if (image != 'undefined' && image != null && $.trim(image) != '') {
        var returnString = '<img src="' + image + '" height=\"24\" />';
    }
    else {
        var returnString = '<img src="../Images/default_user.jpg"  height=\"24\" />';
    }
    return returnString;
}

////form===============================

//$(window).load(function () {
//    equalheight('.form-height .form-group');
//});


//$(window).resize(function () {
//    equalheight('.form-height .form-group');
//});

