﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Print Safety Instruction Report
    /// </summary>
    public partial class PrintSafetyInstructionReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintSafetyInstructionReport"/> class.
        /// </summary>
        public PrintSafetyInstructionReport()
        {
            InitializeComponent();
        }
    }
}
