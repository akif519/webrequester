﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Sub Store Item Report
    /// </summary>
    public partial class SubStoreItemReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubStoreItemReport"/> class.
        /// </summary>
        public SubStoreItemReport()
        {
            InitializeComponent();
        }
    }
}
