﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// PM Job Order Report
    /// </summary>
    public partial class PMJobOrderReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PMJobOrderReport"/> class.
        /// </summary>
        public PMJobOrderReport()
        {
            InitializeComponent();
        }
    }
}
