﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Print JobOrder List Report
    /// </summary>
    public partial class PrintJobOrderListReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintJobOrderListReport"/> class.
        /// </summary>
        public PrintJobOrderListReport()
        {
            InitializeComponent();
        }
    }
}
