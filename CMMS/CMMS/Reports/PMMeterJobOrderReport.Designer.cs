﻿namespace CMMS.Reports
{
    partial class PMMeterJobOrderReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter3 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter4 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter5 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter6 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle1 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.Panel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.Panel52 = new DevExpress.XtraReports.UI.XRPanel();
            this.TimeCompleted = new DevExpress.XtraReports.UI.XRLabel();
            this.DateComplete = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeCopleted = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel51 = new DevExpress.XtraReports.UI.XRPanel();
            this.Time = new DevExpress.XtraReports.UI.XRLabel();
            this.Date = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeStarted = new DevExpress.XtraReports.UI.XRLabel();
            this.EmployeeDetailTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.EmployeeName = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmployeeNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.Normal = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DI = new DevExpress.XtraReports.UI.XRLabel();
            this.ItemDetailLabl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.jobOrderNo = new DevExpress.XtraReports.UI.XRLabel();
            this.JOPlanNo = new DevExpress.XtraReports.UI.XRLabel();
            this.label1 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobTrade = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobPriority = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.assignedSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.CreatedBy = new DevExpress.XtraReports.UI.XRLabel();
            this.creatdBy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.rcvDatetime = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.reqDatetime = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.requester = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.phoneNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.locationNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.txtLocationNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.LocationDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.txtlocationDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.Asset = new DevExpress.XtraReports.UI.XRLabel();
            this.txtAsset = new DevExpress.XtraReports.UI.XRLabel();
            this.AssetDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.txtAssetDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintDivCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintDivCode = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintSubDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintSubDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.znCode = new DevExpress.XtraReports.UI.XRLabel();
            this.ZoneCode = new DevExpress.XtraReports.UI.XRLabel();
            this.BuildingCode = new DevExpress.XtraReports.UI.XRLabel();
            this.buildCode = new DevExpress.XtraReports.UI.XRLabel();
            this.lablPmNo = new DevExpress.XtraReports.UI.XRLabel();
            this.txtPmNo = new DevExpress.XtraReports.UI.XRLabel();
            this.failCode = new DevExpress.XtraReports.UI.XRLabel();
            this.FailureCode = new DevExpress.XtraReports.UI.XRLabel();
            this.CityCode = new DevExpress.XtraReports.UI.XRLabel();
            this.ctyCode = new DevExpress.XtraReports.UI.XRLabel();
            this.AreaCode = new DevExpress.XtraReports.UI.XRLabel();
            this.areaCod = new DevExpress.XtraReports.UI.XRLabel();
            this.ProblemDesc = new DevExpress.XtraReports.UI.XRLabel();
            this.problemDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.NotestoAssets = new DevExpress.XtraReports.UI.XRLabel();
            this.notesAssets = new DevExpress.XtraReports.UI.XRLabel();
            this.NotesToLocation = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.txtPrevTaken = new DevExpress.XtraReports.UI.XRLabel();
            this.txtActionTaken = new DevExpress.XtraReports.UI.XRLabel();
            this.txtCauseDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel7 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.SafetyInstructionsDetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.SafetyInstructionDetails = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.seqId = new DevExpress.XtraReports.UI.XRTableCell();
            this.objectDataSource2 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SafetyInstruction = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.objectDataSource3 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.CheckListDetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.CheckListDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.checkListTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.seqNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.taskDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.altTaskDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.CheckListHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ItemDetailTable = new DevExpress.XtraReports.UI.XRTable();
            this.TableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDetailTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkListTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel38});
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 35.00001F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.BackColor = System.Drawing.Color.Gainsboro;
            this.xrLabel38.Dpi = 100F;
            this.xrLabel38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel38.KeepTogether = true;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(3.499905F, 0F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(824.9999F, 35.00001F);
            this.xrLabel38.StylePriority.UseBackColor = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "PM Meter Job Order Report";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 1F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 982.0833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel17,
            this.Panel1,
            this.Panel2,
            this.Panel3,
            this.Panel4,
            this.xrLabel15,
            this.Panel6,
            this.Panel7,
            this.Panel5});
            this.xrPanel1.Dpi = 100F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0.999999F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(830F, 976.2499F);
            // 
            // Panel5
            // 
            this.Panel5.BorderColor = System.Drawing.Color.LightGray;
            this.Panel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.ItemDetailTable,
            this.Panel52,
            this.Panel51,
            this.EmployeeDetailTable,
            this.DI,
            this.ItemDetailLabl});
            this.Panel5.Dpi = 100F;
            this.Panel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 482.6251F);
            this.Panel5.Name = "Panel5";
            this.Panel5.SizeF = new System.Drawing.SizeF(830F, 341.6666F);
            this.Panel5.StylePriority.UseBorderColor = false;
            this.Panel5.StylePriority.UseBorders = false;
            // 
            // Panel52
            // 
            this.Panel52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TimeCompleted,
            this.DateComplete,
            this.DateTimeCopleted});
            this.Panel52.Dpi = 100F;
            this.Panel52.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 294.4167F);
            this.Panel52.Name = "Panel52";
            this.Panel52.SizeF = new System.Drawing.SizeF(824.4998F, 38.54166F);
            // 
            // TimeCompleted
            // 
            this.TimeCompleted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TimeCompleted.BorderWidth = 0F;
            this.TimeCompleted.Dpi = 100F;
            this.TimeCompleted.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeCompleted.LocationFloat = new DevExpress.Utils.PointFloat(616.9789F, 10F);
            this.TimeCompleted.Name = "TimeCompleted";
            this.TimeCompleted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TimeCompleted.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.TimeCompleted.StylePriority.UseBorders = false;
            this.TimeCompleted.StylePriority.UseBorderWidth = false;
            this.TimeCompleted.StylePriority.UseFont = false;
            this.TimeCompleted.StylePriority.UseTextAlignment = false;
            this.TimeCompleted.Text = "Time :";
            this.TimeCompleted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateComplete
            // 
            this.DateComplete.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateComplete.BorderWidth = 0F;
            this.DateComplete.Dpi = 100F;
            this.DateComplete.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateComplete.LocationFloat = new DevExpress.Utils.PointFloat(430.5206F, 9.999911F);
            this.DateComplete.Name = "DateComplete";
            this.DateComplete.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateComplete.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.DateComplete.StylePriority.UseBorders = false;
            this.DateComplete.StylePriority.UseBorderWidth = false;
            this.DateComplete.StylePriority.UseFont = false;
            this.DateComplete.StylePriority.UseTextAlignment = false;
            this.DateComplete.Text = "Date :";
            this.DateComplete.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateTimeCopleted
            // 
            this.DateTimeCopleted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateTimeCopleted.BorderWidth = 0F;
            this.DateTimeCopleted.Dpi = 100F;
            this.DateTimeCopleted.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimeCopleted.LocationFloat = new DevExpress.Utils.PointFloat(2F, 9.999847F);
            this.DateTimeCopleted.Name = "DateTimeCopleted";
            this.DateTimeCopleted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateTimeCopleted.SizeF = new System.Drawing.SizeF(176.3546F, 23.00002F);
            this.DateTimeCopleted.StylePriority.UseBorders = false;
            this.DateTimeCopleted.StylePriority.UseBorderWidth = false;
            this.DateTimeCopleted.StylePriority.UseFont = false;
            this.DateTimeCopleted.StylePriority.UseTextAlignment = false;
            this.DateTimeCopleted.Text = "Date and Time work was Completed";
            this.DateTimeCopleted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Panel51
            // 
            this.Panel51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.Time,
            this.Date,
            this.DateTimeStarted});
            this.Panel51.Dpi = 100F;
            this.Panel51.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 251.3332F);
            this.Panel51.Name = "Panel51";
            this.Panel51.SizeF = new System.Drawing.SizeF(824.4998F, 39.08348F);
            // 
            // Time
            // 
            this.Time.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Time.BorderWidth = 0F;
            this.Time.Dpi = 100F;
            this.Time.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.LocationFloat = new DevExpress.Utils.PointFloat(616.9789F, 5F);
            this.Time.Name = "Time";
            this.Time.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Time.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.Time.StylePriority.UseBorders = false;
            this.Time.StylePriority.UseBorderWidth = false;
            this.Time.StylePriority.UseFont = false;
            this.Time.StylePriority.UseTextAlignment = false;
            this.Time.Text = "Time :";
            this.Time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Date
            // 
            this.Date.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Date.BorderWidth = 0F;
            this.Date.Dpi = 100F;
            this.Date.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date.LocationFloat = new DevExpress.Utils.PointFloat(430.5206F, 4.999911F);
            this.Date.Name = "Date";
            this.Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Date.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.Date.StylePriority.UseBorders = false;
            this.Date.StylePriority.UseBorderWidth = false;
            this.Date.StylePriority.UseFont = false;
            this.Date.StylePriority.UseTextAlignment = false;
            this.Date.Text = "Date :";
            this.Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateTimeStarted
            // 
            this.DateTimeStarted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateTimeStarted.BorderWidth = 0F;
            this.DateTimeStarted.Dpi = 100F;
            this.DateTimeStarted.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimeStarted.LocationFloat = new DevExpress.Utils.PointFloat(2F, 4.99986F);
            this.DateTimeStarted.Name = "DateTimeStarted";
            this.DateTimeStarted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateTimeStarted.SizeF = new System.Drawing.SizeF(176.3546F, 23.00002F);
            this.DateTimeStarted.StylePriority.UseBorders = false;
            this.DateTimeStarted.StylePriority.UseBorderWidth = false;
            this.DateTimeStarted.StylePriority.UseFont = false;
            this.DateTimeStarted.StylePriority.UseTextAlignment = false;
            this.DateTimeStarted.Text = "Date and Time work was started";
            this.DateTimeStarted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // EmployeeDetailTable
            // 
            this.EmployeeDetailTable.Dpi = 100F;
            this.EmployeeDetailTable.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 149.3332F);
            this.EmployeeDetailTable.Name = "EmployeeDetailTable";
            this.EmployeeDetailTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
            this.EmployeeDetailTable.SizeF = new System.Drawing.SizeF(824.4998F, 100F);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.EmployeeName,
            this.EmployeeNo,
            this.Normal,
            this.OT1,
            this.OT2,
            this.OT3});
            this.xrTableRow5.Dpi = 100F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // EmployeeName
            // 
            this.EmployeeName.BackColor = System.Drawing.Color.LightGray;
            this.EmployeeName.BorderColor = System.Drawing.Color.LightGray;
            this.EmployeeName.Dpi = 100F;
            this.EmployeeName.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.StylePriority.UseBackColor = false;
            this.EmployeeName.StylePriority.UseBorderColor = false;
            this.EmployeeName.StylePriority.UseFont = false;
            this.EmployeeName.StylePriority.UseTextAlignment = false;
            this.EmployeeName.Text = "Employee Name";
            this.EmployeeName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.EmployeeName.Weight = 1.873895041925387D;
            // 
            // EmployeeNo
            // 
            this.EmployeeNo.BackColor = System.Drawing.Color.LightGray;
            this.EmployeeNo.Dpi = 100F;
            this.EmployeeNo.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.EmployeeNo.Name = "EmployeeNo";
            this.EmployeeNo.StylePriority.UseBackColor = false;
            this.EmployeeNo.StylePriority.UseFont = false;
            this.EmployeeNo.StylePriority.UseTextAlignment = false;
            this.EmployeeNo.Text = "Employee No";
            this.EmployeeNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.EmployeeNo.Weight = 1.8021000671386716D;
            // 
            // Normal
            // 
            this.Normal.BackColor = System.Drawing.Color.LightGray;
            this.Normal.Dpi = 100F;
            this.Normal.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.Normal.Name = "Normal";
            this.Normal.StylePriority.UseBackColor = false;
            this.Normal.StylePriority.UseFont = false;
            this.Normal.StylePriority.UseTextAlignment = false;
            this.Normal.Text = "Normal";
            this.Normal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Normal.Weight = 0.99790008544921882D;
            // 
            // OT1
            // 
            this.OT1.BackColor = System.Drawing.Color.LightGray;
            this.OT1.Dpi = 100F;
            this.OT1.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.OT1.Name = "OT1";
            this.OT1.StylePriority.UseBackColor = false;
            this.OT1.StylePriority.UseFont = false;
            this.OT1.StylePriority.UseTextAlignment = false;
            this.OT1.Text = "OT1";
            this.OT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT1.Weight = 1.0520834096272784D;
            // 
            // OT2
            // 
            this.OT2.BackColor = System.Drawing.Color.LightGray;
            this.OT2.Dpi = 100F;
            this.OT2.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.OT2.Name = "OT2";
            this.OT2.StylePriority.UseBackColor = false;
            this.OT2.StylePriority.UseFont = false;
            this.OT2.StylePriority.UseTextAlignment = false;
            this.OT2.Text = "OT2";
            this.OT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT2.Weight = 1.1910790952996777D;
            // 
            // OT3
            // 
            this.OT3.BackColor = System.Drawing.Color.LightGray;
            this.OT3.Dpi = 100F;
            this.OT3.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.OT3.Multiline = true;
            this.OT3.Name = "OT3";
            this.OT3.StylePriority.UseBackColor = false;
            this.OT3.StylePriority.UseFont = false;
            this.OT3.StylePriority.UseTextAlignment = false;
            this.OT3.Text = "OT3\r\n";
            this.OT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT3.Weight = 1.2547522289597628D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell31,
            this.TableCell32,
            this.TableCell33,
            this.TableCell34,
            this.TableCell35,
            this.TableCell36});
            this.xrTableRow6.Dpi = 100F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // TableCell31
            // 
            this.TableCell31.Dpi = 100F;
            this.TableCell31.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell31.Name = "TableCell31";
            this.TableCell31.StylePriority.UseFont = false;
            this.TableCell31.StylePriority.UseTextAlignment = false;
            this.TableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell31.Weight = 1.873895041925387D;
            // 
            // TableCell32
            // 
            this.TableCell32.Dpi = 100F;
            this.TableCell32.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell32.Name = "TableCell32";
            this.TableCell32.StylePriority.UseFont = false;
            this.TableCell32.StylePriority.UseTextAlignment = false;
            this.TableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell32.Weight = 1.8021000671386716D;
            // 
            // TableCell33
            // 
            this.TableCell33.Dpi = 100F;
            this.TableCell33.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell33.Name = "TableCell33";
            this.TableCell33.StylePriority.UseFont = false;
            this.TableCell33.StylePriority.UseTextAlignment = false;
            this.TableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell33.Weight = 0.99790008544921882D;
            // 
            // TableCell34
            // 
            this.TableCell34.Dpi = 100F;
            this.TableCell34.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell34.Name = "TableCell34";
            this.TableCell34.StylePriority.UseFont = false;
            this.TableCell34.StylePriority.UseTextAlignment = false;
            this.TableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell34.Weight = 1.0520834096272784D;
            // 
            // TableCell35
            // 
            this.TableCell35.Dpi = 100F;
            this.TableCell35.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell35.Name = "TableCell35";
            this.TableCell35.StylePriority.UseFont = false;
            this.TableCell35.StylePriority.UseTextAlignment = false;
            this.TableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell35.Weight = 1.1910790952996777D;
            // 
            // TableCell36
            // 
            this.TableCell36.Dpi = 100F;
            this.TableCell36.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell36.Name = "TableCell36";
            this.TableCell36.StylePriority.UseFont = false;
            this.TableCell36.StylePriority.UseTextAlignment = false;
            this.TableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell36.Weight = 1.2547522289597628D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell37,
            this.TableCell38,
            this.TableCell39,
            this.TableCell40,
            this.TableCell41,
            this.TableCell42});
            this.xrTableRow7.Dpi = 100F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // TableCell37
            // 
            this.TableCell37.Dpi = 100F;
            this.TableCell37.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell37.Name = "TableCell37";
            this.TableCell37.StylePriority.UseFont = false;
            this.TableCell37.StylePriority.UseTextAlignment = false;
            this.TableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell37.Weight = 1.873895041925387D;
            // 
            // TableCell38
            // 
            this.TableCell38.Dpi = 100F;
            this.TableCell38.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell38.Name = "TableCell38";
            this.TableCell38.StylePriority.UseFont = false;
            this.TableCell38.StylePriority.UseTextAlignment = false;
            this.TableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell38.Weight = 1.8021000671386716D;
            // 
            // TableCell39
            // 
            this.TableCell39.Dpi = 100F;
            this.TableCell39.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell39.Name = "TableCell39";
            this.TableCell39.StylePriority.UseFont = false;
            this.TableCell39.StylePriority.UseTextAlignment = false;
            this.TableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell39.Weight = 0.99790008544921882D;
            // 
            // TableCell40
            // 
            this.TableCell40.Dpi = 100F;
            this.TableCell40.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell40.Name = "TableCell40";
            this.TableCell40.StylePriority.UseFont = false;
            this.TableCell40.StylePriority.UseTextAlignment = false;
            this.TableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell40.Weight = 1.0520834096272784D;
            // 
            // TableCell41
            // 
            this.TableCell41.Dpi = 100F;
            this.TableCell41.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell41.Name = "TableCell41";
            this.TableCell41.StylePriority.UseFont = false;
            this.TableCell41.StylePriority.UseTextAlignment = false;
            this.TableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell41.Weight = 1.1910790952996777D;
            // 
            // TableCell42
            // 
            this.TableCell42.Dpi = 100F;
            this.TableCell42.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell42.Name = "TableCell42";
            this.TableCell42.StylePriority.UseFont = false;
            this.TableCell42.StylePriority.UseTextAlignment = false;
            this.TableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell42.Weight = 1.2547522289597628D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell43,
            this.TableCell44,
            this.TableCell45,
            this.TableCell46,
            this.TableCell47,
            this.TableCell48});
            this.xrTableRow8.Dpi = 100F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // TableCell43
            // 
            this.TableCell43.Dpi = 100F;
            this.TableCell43.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell43.Name = "TableCell43";
            this.TableCell43.StylePriority.UseFont = false;
            this.TableCell43.StylePriority.UseTextAlignment = false;
            this.TableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell43.Weight = 1.873895041925387D;
            // 
            // TableCell44
            // 
            this.TableCell44.Dpi = 100F;
            this.TableCell44.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell44.Name = "TableCell44";
            this.TableCell44.StylePriority.UseFont = false;
            this.TableCell44.StylePriority.UseTextAlignment = false;
            this.TableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell44.Weight = 1.8021000671386716D;
            // 
            // TableCell45
            // 
            this.TableCell45.Dpi = 100F;
            this.TableCell45.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell45.Name = "TableCell45";
            this.TableCell45.StylePriority.UseFont = false;
            this.TableCell45.StylePriority.UseTextAlignment = false;
            this.TableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell45.Weight = 0.99790008544921882D;
            // 
            // TableCell46
            // 
            this.TableCell46.Dpi = 100F;
            this.TableCell46.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell46.Name = "TableCell46";
            this.TableCell46.StylePriority.UseFont = false;
            this.TableCell46.StylePriority.UseTextAlignment = false;
            this.TableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell46.Weight = 1.0520834096272784D;
            // 
            // TableCell47
            // 
            this.TableCell47.Dpi = 100F;
            this.TableCell47.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell47.Name = "TableCell47";
            this.TableCell47.StylePriority.UseFont = false;
            this.TableCell47.StylePriority.UseTextAlignment = false;
            this.TableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell47.Weight = 1.1910790952996777D;
            // 
            // TableCell48
            // 
            this.TableCell48.Dpi = 100F;
            this.TableCell48.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell48.Name = "TableCell48";
            this.TableCell48.StylePriority.UseFont = false;
            this.TableCell48.StylePriority.UseTextAlignment = false;
            this.TableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell48.Weight = 1.2547522289597628D;
            // 
            // DI
            // 
            this.DI.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DI.BorderWidth = 0F;
            this.DI.Dpi = 100F;
            this.DI.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DI.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 128.1667F);
            this.DI.Name = "DI";
            this.DI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DI.SizeF = new System.Drawing.SizeF(244.7917F, 16.74983F);
            this.DI.StylePriority.UseBorders = false;
            this.DI.StylePriority.UseBorderWidth = false;
            this.DI.StylePriority.UseFont = false;
            this.DI.StylePriority.UseTextAlignment = false;
            this.DI.Text = "** D = Direct Issue, I = Inventory";
            this.DI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ItemDetailLabl
            // 
            this.ItemDetailLabl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ItemDetailLabl.BorderWidth = 0F;
            this.ItemDetailLabl.Dpi = 100F;
            this.ItemDetailLabl.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemDetailLabl.LocationFloat = new DevExpress.Utils.PointFloat(281.7083F, 3F);
            this.ItemDetailLabl.Name = "ItemDetailLabl";
            this.ItemDetailLabl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ItemDetailLabl.SizeF = new System.Drawing.SizeF(244.7917F, 23.74992F);
            this.ItemDetailLabl.StylePriority.UseBorders = false;
            this.ItemDetailLabl.StylePriority.UseBorderWidth = false;
            this.ItemDetailLabl.StylePriority.UseFont = false;
            this.ItemDetailLabl.StylePriority.UseTextAlignment = false;
            this.ItemDetailLabl.Text = "Items And Labour Details";
            this.ItemDetailLabl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(3.999997F, 951.0417F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Please rate Our Services :";
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(221.3383F, 951.0417F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(291.4738F, 18.8335F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "1.Poor 2.Fair 3.Good 4.Very Good 5.Excellent";
            // 
            // Panel1
            // 
            this.Panel1.BorderColor = System.Drawing.Color.LightGray;
            this.Panel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.jobOrderNo,
            this.JOPlanNo,
            this.label1,
            this.JobTrade,
            this.label2,
            this.JobStatus,
            this.xrLabel2,
            this.JobType,
            this.xrLabel3,
            this.JobPriority});
            this.Panel1.Dpi = 100F;
            this.Panel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.374981F);
            this.Panel1.Name = "Panel1";
            this.Panel1.SizeF = new System.Drawing.SizeF(830F, 82.29164F);
            this.Panel1.StylePriority.UseBorderColor = false;
            this.Panel1.StylePriority.UseBorders = false;
            // 
            // jobOrderNo
            // 
            this.jobOrderNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.jobOrderNo.Dpi = 100F;
            this.jobOrderNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobOrderNo.LocationFloat = new DevExpress.Utils.PointFloat(2.499938F, 10.29161F);
            this.jobOrderNo.Name = "jobOrderNo";
            this.jobOrderNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.jobOrderNo.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.jobOrderNo.StylePriority.UseBorders = false;
            this.jobOrderNo.StylePriority.UseFont = false;
            this.jobOrderNo.StylePriority.UseTextAlignment = false;
            this.jobOrderNo.Text = "Job Order No :";
            this.jobOrderNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JOPlanNo
            // 
            this.JOPlanNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JOPlanNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkorderNo")});
            this.JOPlanNo.Dpi = 100F;
            this.JOPlanNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JOPlanNo.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 10.29164F);
            this.JOPlanNo.Multiline = true;
            this.JOPlanNo.Name = "JOPlanNo";
            this.JOPlanNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JOPlanNo.SizeF = new System.Drawing.SizeF(140.2083F, 23F);
            this.JOPlanNo.StylePriority.UseBorders = false;
            this.JOPlanNo.StylePriority.UseFont = false;
            this.JOPlanNo.StylePriority.UsePadding = false;
            this.JOPlanNo.StylePriority.UseTextAlignment = false;
            this.JOPlanNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label1.Dpi = 100F;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 33.2916F);
            this.label1.Name = "label1";
            this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label1.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.label1.StylePriority.UseBorders = false;
            this.label1.StylePriority.UseFont = false;
            this.label1.StylePriority.UseTextAlignment = false;
            this.label1.Text = "Job Trade :";
            this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobTrade
            // 
            this.JobTrade.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobTrade.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkTrade")});
            this.JobTrade.Dpi = 100F;
            this.JobTrade.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobTrade.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 33.29163F);
            this.JobTrade.Multiline = true;
            this.JobTrade.Name = "JobTrade";
            this.JobTrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobTrade.SizeF = new System.Drawing.SizeF(140.2083F, 23F);
            this.JobTrade.StylePriority.UseBorders = false;
            this.JobTrade.StylePriority.UseFont = false;
            this.JobTrade.StylePriority.UsePadding = false;
            this.JobTrade.StylePriority.UseTextAlignment = false;
            this.JobTrade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label2.Dpi = 100F;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 56.29163F);
            this.label2.Name = "label2";
            this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label2.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.label2.StylePriority.UseBorders = false;
            this.label2.StylePriority.UseFont = false;
            this.label2.StylePriority.UseTextAlignment = false;
            this.label2.Text = "Job Status :";
            this.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobStatus
            // 
            this.JobStatus.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobStatus.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkStatus")});
            this.JobStatus.Dpi = 100F;
            this.JobStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobStatus.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 56.29166F);
            this.JobStatus.Multiline = true;
            this.JobStatus.Name = "JobStatus";
            this.JobStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobStatus.SizeF = new System.Drawing.SizeF(140.2083F, 23F);
            this.JobStatus.StylePriority.UseBorders = false;
            this.JobStatus.StylePriority.UseFont = false;
            this.JobStatus.StylePriority.UsePadding = false;
            this.JobStatus.StylePriority.UseTextAlignment = false;
            this.JobStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 33.2916F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(147.2501F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Job Type :";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobType
            // 
            this.JobType.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobType.Dpi = 100F;
            this.JobType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobType.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 33.2916F);
            this.JobType.Multiline = true;
            this.JobType.Name = "JobType";
            this.JobType.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobType.SizeF = new System.Drawing.SizeF(157.8126F, 23F);
            this.JobType.StylePriority.UseBorders = false;
            this.JobType.StylePriority.UseFont = false;
            this.JobType.StylePriority.UsePadding = false;
            this.JobType.StylePriority.UseTextAlignment = false;
            this.JobType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 56.29168F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(147.2501F, 23F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Job Priority :";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobPriority
            // 
            this.JobPriority.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobPriority.Dpi = 100F;
            this.JobPriority.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobPriority.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 56.29168F);
            this.JobPriority.Multiline = true;
            this.JobPriority.Name = "JobPriority";
            this.JobPriority.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobPriority.SizeF = new System.Drawing.SizeF(157.8126F, 23F);
            this.JobPriority.StylePriority.UseBorders = false;
            this.JobPriority.StylePriority.UseFont = false;
            this.JobPriority.StylePriority.UsePadding = false;
            this.JobPriority.StylePriority.UseTextAlignment = false;
            this.JobPriority.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Panel2
            // 
            this.Panel2.BorderColor = System.Drawing.Color.LightGray;
            this.Panel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.assignedSupplier,
            this.CreatedBy,
            this.creatdBy,
            this.xrLabel5,
            this.rcvDatetime,
            this.xrLabel6,
            this.reqDatetime,
            this.xrLabel7,
            this.requester,
            this.xrLabel8,
            this.phoneNumber});
            this.Panel2.Dpi = 100F;
            this.Panel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 89.66659F);
            this.Panel2.Name = "Panel2";
            this.Panel2.SizeF = new System.Drawing.SizeF(830F, 81.25004F);
            this.Panel2.StylePriority.UseBorderColor = false;
            this.Panel2.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 9.770806F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(147.2501F, 23F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Assigned To Supplier :";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // assignedSupplier
            // 
            this.assignedSupplier.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.assignedSupplier.Dpi = 100F;
            this.assignedSupplier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignedSupplier.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 9.770809F);
            this.assignedSupplier.Multiline = true;
            this.assignedSupplier.Name = "assignedSupplier";
            this.assignedSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.assignedSupplier.SizeF = new System.Drawing.SizeF(157.8126F, 23F);
            this.assignedSupplier.StylePriority.UseBorders = false;
            this.assignedSupplier.StylePriority.UseFont = false;
            this.assignedSupplier.StylePriority.UsePadding = false;
            this.assignedSupplier.StylePriority.UseTextAlignment = false;
            this.assignedSupplier.Text = "txtSupplier";
            this.assignedSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // CreatedBy
            // 
            this.CreatedBy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CreatedBy.Dpi = 100F;
            this.CreatedBy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedBy.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 9.770806F);
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.CreatedBy.SizeF = new System.Drawing.SizeF(152.4583F, 23.00001F);
            this.CreatedBy.StylePriority.UseBorders = false;
            this.CreatedBy.StylePriority.UseFont = false;
            this.CreatedBy.StylePriority.UseTextAlignment = false;
            this.CreatedBy.Text = "Created By :";
            this.CreatedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // creatdBy
            // 
            this.creatdBy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.creatdBy.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "Name")});
            this.creatdBy.Dpi = 100F;
            this.creatdBy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creatdBy.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 9.770806F);
            this.creatdBy.Multiline = true;
            this.creatdBy.Name = "creatdBy";
            this.creatdBy.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.creatdBy.SizeF = new System.Drawing.SizeF(140.2082F, 23.00001F);
            this.creatdBy.StylePriority.UseBorders = false;
            this.creatdBy.StylePriority.UseFont = false;
            this.creatdBy.StylePriority.UsePadding = false;
            this.creatdBy.StylePriority.UseTextAlignment = false;
            this.creatdBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 32.77079F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Received Date/Time :";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // rcvDatetime
            // 
            this.rcvDatetime.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.rcvDatetime.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DateReceived")});
            this.rcvDatetime.Dpi = 100F;
            this.rcvDatetime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rcvDatetime.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 32.77079F);
            this.rcvDatetime.Multiline = true;
            this.rcvDatetime.Name = "rcvDatetime";
            this.rcvDatetime.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.rcvDatetime.SizeF = new System.Drawing.SizeF(140.2082F, 23F);
            this.rcvDatetime.StylePriority.UseBorders = false;
            this.rcvDatetime.StylePriority.UseFont = false;
            this.rcvDatetime.StylePriority.UsePadding = false;
            this.rcvDatetime.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:dd-MMM-yy}";
            this.rcvDatetime.Summary = xrSummary1;
            this.rcvDatetime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 55.77084F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Required Date/Time :";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // reqDatetime
            // 
            this.reqDatetime.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.reqDatetime.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "DateRequired")});
            this.reqDatetime.Dpi = 100F;
            this.reqDatetime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reqDatetime.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 55.77084F);
            this.reqDatetime.Multiline = true;
            this.reqDatetime.Name = "reqDatetime";
            this.reqDatetime.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.reqDatetime.SizeF = new System.Drawing.SizeF(140.2082F, 23F);
            this.reqDatetime.StylePriority.UseBorders = false;
            this.reqDatetime.StylePriority.UseFont = false;
            this.reqDatetime.StylePriority.UsePadding = false;
            this.reqDatetime.StylePriority.UseTextAlignment = false;
            this.reqDatetime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 32.77076F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(147.2501F, 23F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Requester :";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // requester
            // 
            this.requester.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.requester.Dpi = 100F;
            this.requester.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.requester.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 32.77076F);
            this.requester.Multiline = true;
            this.requester.Name = "requester";
            this.requester.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.requester.SizeF = new System.Drawing.SizeF(157.8126F, 23F);
            this.requester.StylePriority.UseBorders = false;
            this.requester.StylePriority.UseFont = false;
            this.requester.StylePriority.UsePadding = false;
            this.requester.StylePriority.UseTextAlignment = false;
            this.requester.Text = "txtRequester";
            this.requester.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 55.77084F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(147.2501F, 23F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Phone Number :";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // phoneNumber
            // 
            this.phoneNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.phoneNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "WorkPhone")});
            this.phoneNumber.Dpi = 100F;
            this.phoneNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNumber.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 55.77084F);
            this.phoneNumber.Multiline = true;
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.phoneNumber.SizeF = new System.Drawing.SizeF(157.8126F, 23F);
            this.phoneNumber.StylePriority.UseBorders = false;
            this.phoneNumber.StylePriority.UseFont = false;
            this.phoneNumber.StylePriority.UsePadding = false;
            this.phoneNumber.StylePriority.UseTextAlignment = false;
            this.phoneNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Panel3
            // 
            this.Panel3.BorderColor = System.Drawing.Color.LightGray;
            this.Panel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.locationNumber,
            this.txtLocationNumber,
            this.LocationDescription,
            this.txtlocationDescription,
            this.Asset,
            this.txtAsset,
            this.AssetDescription,
            this.txtAssetDescription,
            this.MaintDivCode,
            this.txtMaintDivCode,
            this.MaintDeptCode,
            this.txtMaintDeptCode,
            this.MaintSubDeptCode,
            this.txtMaintSubDeptCode,
            this.znCode,
            this.ZoneCode,
            this.BuildingCode,
            this.buildCode,
            this.lablPmNo,
            this.txtPmNo,
            this.failCode,
            this.FailureCode,
            this.CityCode,
            this.ctyCode,
            this.AreaCode,
            this.areaCod,
            this.ProblemDesc,
            this.problemDescription,
            this.NotestoAssets,
            this.notesAssets,
            this.NotesToLocation,
            this.xrLabel14});
            this.Panel3.Dpi = 100F;
            this.Panel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 175.625F);
            this.Panel3.Name = "Panel3";
            this.Panel3.SizeF = new System.Drawing.SizeF(830F, 218.7499F);
            this.Panel3.StylePriority.UseBorderColor = false;
            this.Panel3.StylePriority.UseBorders = false;
            // 
            // locationNumber
            // 
            this.locationNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.locationNumber.Dpi = 100F;
            this.locationNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.locationNumber.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 56.00004F);
            this.locationNumber.Name = "locationNumber";
            this.locationNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.locationNumber.SizeF = new System.Drawing.SizeF(147.2501F, 23.00002F);
            this.locationNumber.StylePriority.UseBorders = false;
            this.locationNumber.StylePriority.UseFont = false;
            this.locationNumber.StylePriority.UseTextAlignment = false;
            this.locationNumber.Text = "Location No :";
            this.locationNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtLocationNumber
            // 
            this.txtLocationNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtLocationNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LocationNo")});
            this.txtLocationNumber.Dpi = 100F;
            this.txtLocationNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocationNumber.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 56.00001F);
            this.txtLocationNumber.Multiline = true;
            this.txtLocationNumber.Name = "txtLocationNumber";
            this.txtLocationNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtLocationNumber.SizeF = new System.Drawing.SizeF(157.8126F, 23.00002F);
            this.txtLocationNumber.StylePriority.UseBorders = false;
            this.txtLocationNumber.StylePriority.UseFont = false;
            this.txtLocationNumber.StylePriority.UsePadding = false;
            this.txtLocationNumber.StylePriority.UseTextAlignment = false;
            this.txtLocationNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LocationDescription
            // 
            this.LocationDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LocationDescription.Dpi = 100F;
            this.LocationDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationDescription.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 79.00006F);
            this.LocationDescription.Name = "LocationDescription";
            this.LocationDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.LocationDescription.SizeF = new System.Drawing.SizeF(147.2501F, 22.99998F);
            this.LocationDescription.StylePriority.UseBorders = false;
            this.LocationDescription.StylePriority.UseFont = false;
            this.LocationDescription.StylePriority.UseTextAlignment = false;
            this.LocationDescription.Text = "Location Description :";
            this.LocationDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtlocationDescription
            // 
            this.txtlocationDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtlocationDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "LocationDescription")});
            this.txtlocationDescription.Dpi = 100F;
            this.txtlocationDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlocationDescription.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 79.00006F);
            this.txtlocationDescription.Multiline = true;
            this.txtlocationDescription.Name = "txtlocationDescription";
            this.txtlocationDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtlocationDescription.SizeF = new System.Drawing.SizeF(157.8126F, 22.99995F);
            this.txtlocationDescription.StylePriority.UseBorders = false;
            this.txtlocationDescription.StylePriority.UseFont = false;
            this.txtlocationDescription.StylePriority.UsePadding = false;
            this.txtlocationDescription.StylePriority.UseTextAlignment = false;
            this.txtlocationDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Asset
            // 
            this.Asset.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Asset.Dpi = 100F;
            this.Asset.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Asset.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 102F);
            this.Asset.Name = "Asset";
            this.Asset.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Asset.SizeF = new System.Drawing.SizeF(147.2501F, 23.00002F);
            this.Asset.StylePriority.UseBorders = false;
            this.Asset.StylePriority.UseFont = false;
            this.Asset.StylePriority.UseTextAlignment = false;
            this.Asset.Text = "Asset :";
            this.Asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtAsset
            // 
            this.txtAsset.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtAsset.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetNumber")});
            this.txtAsset.Dpi = 100F;
            this.txtAsset.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsset.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 102F);
            this.txtAsset.Multiline = true;
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtAsset.SizeF = new System.Drawing.SizeF(157.8126F, 23.00005F);
            this.txtAsset.StylePriority.UseBorders = false;
            this.txtAsset.StylePriority.UseFont = false;
            this.txtAsset.StylePriority.UsePadding = false;
            this.txtAsset.StylePriority.UseTextAlignment = false;
            this.txtAsset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AssetDescription
            // 
            this.AssetDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.AssetDescription.Dpi = 100F;
            this.AssetDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 125.0001F);
            this.AssetDescription.Name = "AssetDescription";
            this.AssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.AssetDescription.SizeF = new System.Drawing.SizeF(147.2501F, 22.99998F);
            this.AssetDescription.StylePriority.UseBorders = false;
            this.AssetDescription.StylePriority.UseFont = false;
            this.AssetDescription.StylePriority.UseTextAlignment = false;
            this.AssetDescription.Text = "Asset Description :";
            this.AssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtAssetDescription
            // 
            this.txtAssetDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtAssetDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AssetDescription")});
            this.txtAssetDescription.Dpi = 100F;
            this.txtAssetDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 125.0001F);
            this.txtAssetDescription.Multiline = true;
            this.txtAssetDescription.Name = "txtAssetDescription";
            this.txtAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtAssetDescription.SizeF = new System.Drawing.SizeF(157.8126F, 22.99995F);
            this.txtAssetDescription.StylePriority.UseBorders = false;
            this.txtAssetDescription.StylePriority.UseFont = false;
            this.txtAssetDescription.StylePriority.UsePadding = false;
            this.txtAssetDescription.StylePriority.UseTextAlignment = false;
            this.txtAssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintDivCode
            // 
            this.MaintDivCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintDivCode.Dpi = 100F;
            this.MaintDivCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintDivCode.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 148.0001F);
            this.MaintDivCode.Name = "MaintDivCode";
            this.MaintDivCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintDivCode.SizeF = new System.Drawing.SizeF(147.2501F, 22.99998F);
            this.MaintDivCode.StylePriority.UseBorders = false;
            this.MaintDivCode.StylePriority.UseFont = false;
            this.MaintDivCode.StylePriority.UseTextAlignment = false;
            this.MaintDivCode.Text = "Maint Div. Code :";
            this.MaintDivCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintDivCode
            // 
            this.txtMaintDivCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintDivCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaintDivisionCode")});
            this.txtMaintDivCode.Dpi = 100F;
            this.txtMaintDivCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintDivCode.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 148.0001F);
            this.txtMaintDivCode.Multiline = true;
            this.txtMaintDivCode.Name = "txtMaintDivCode";
            this.txtMaintDivCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintDivCode.SizeF = new System.Drawing.SizeF(157.8126F, 22.99995F);
            this.txtMaintDivCode.StylePriority.UseBorders = false;
            this.txtMaintDivCode.StylePriority.UseFont = false;
            this.txtMaintDivCode.StylePriority.UsePadding = false;
            this.txtMaintDivCode.StylePriority.UseTextAlignment = false;
            this.txtMaintDivCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintDeptCode
            // 
            this.MaintDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintDeptCode.Dpi = 100F;
            this.MaintDeptCode.Font = new System.Drawing.Font("Arial", 8.2F, System.Drawing.FontStyle.Bold);
            this.MaintDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 171F);
            this.MaintDeptCode.Name = "MaintDeptCode";
            this.MaintDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintDeptCode.SizeF = new System.Drawing.SizeF(147.2501F, 23.00003F);
            this.MaintDeptCode.StylePriority.UseBorders = false;
            this.MaintDeptCode.StylePriority.UseFont = false;
            this.MaintDeptCode.StylePriority.UseTextAlignment = false;
            this.MaintDeptCode.Text = "Maint Dept. Code :";
            this.MaintDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintDeptCode
            // 
            this.txtMaintDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintDeptCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaintDeptCode")});
            this.txtMaintDeptCode.Dpi = 100F;
            this.txtMaintDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 171F);
            this.txtMaintDeptCode.Multiline = true;
            this.txtMaintDeptCode.Name = "txtMaintDeptCode";
            this.txtMaintDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintDeptCode.SizeF = new System.Drawing.SizeF(157.8126F, 23.00002F);
            this.txtMaintDeptCode.StylePriority.UseBorders = false;
            this.txtMaintDeptCode.StylePriority.UseFont = false;
            this.txtMaintDeptCode.StylePriority.UsePadding = false;
            this.txtMaintDeptCode.StylePriority.UseTextAlignment = false;
            this.txtMaintDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintSubDeptCode
            // 
            this.MaintSubDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintSubDeptCode.Dpi = 100F;
            this.MaintSubDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintSubDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(520.729F, 194.0001F);
            this.MaintSubDeptCode.Name = "MaintSubDeptCode";
            this.MaintSubDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintSubDeptCode.SizeF = new System.Drawing.SizeF(147.2501F, 22.99998F);
            this.MaintSubDeptCode.StylePriority.UseBorders = false;
            this.MaintSubDeptCode.StylePriority.UseFont = false;
            this.MaintSubDeptCode.StylePriority.UseTextAlignment = false;
            this.MaintSubDeptCode.Text = "Maint SubDept. Code :";
            this.MaintSubDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintSubDeptCode
            // 
            this.txtMaintSubDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintSubDeptCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "MaintSubDeptCode")});
            this.txtMaintSubDeptCode.Dpi = 100F;
            this.txtMaintSubDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintSubDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(668.3748F, 194.0001F);
            this.txtMaintSubDeptCode.Multiline = true;
            this.txtMaintSubDeptCode.Name = "txtMaintSubDeptCode";
            this.txtMaintSubDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintSubDeptCode.SizeF = new System.Drawing.SizeF(157.8126F, 22.99995F);
            this.txtMaintSubDeptCode.StylePriority.UseBorders = false;
            this.txtMaintSubDeptCode.StylePriority.UseFont = false;
            this.txtMaintSubDeptCode.StylePriority.UsePadding = false;
            this.txtMaintSubDeptCode.StylePriority.UseTextAlignment = false;
            this.txtMaintSubDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // znCode
            // 
            this.znCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.znCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L4No")});
            this.znCode.Dpi = 100F;
            this.znCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.znCode.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 148F);
            this.znCode.Multiline = true;
            this.znCode.Name = "znCode";
            this.znCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.znCode.SizeF = new System.Drawing.SizeF(140.2083F, 22.99998F);
            this.znCode.StylePriority.UseBorders = false;
            this.znCode.StylePriority.UseFont = false;
            this.znCode.StylePriority.UsePadding = false;
            this.znCode.StylePriority.UseTextAlignment = false;
            this.znCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ZoneCode
            // 
            this.ZoneCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ZoneCode.Dpi = 100F;
            this.ZoneCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoneCode.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 148F);
            this.ZoneCode.Name = "ZoneCode";
            this.ZoneCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ZoneCode.SizeF = new System.Drawing.SizeF(152.4583F, 22.99998F);
            this.ZoneCode.StylePriority.UseBorders = false;
            this.ZoneCode.StylePriority.UseFont = false;
            this.ZoneCode.StylePriority.UseTextAlignment = false;
            this.ZoneCode.Text = "Zone Code :";
            this.ZoneCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // BuildingCode
            // 
            this.BuildingCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BuildingCode.Dpi = 100F;
            this.BuildingCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuildingCode.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 171F);
            this.BuildingCode.Name = "BuildingCode";
            this.BuildingCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.BuildingCode.SizeF = new System.Drawing.SizeF(152.4583F, 22.99998F);
            this.BuildingCode.StylePriority.UseBorders = false;
            this.BuildingCode.StylePriority.UseFont = false;
            this.BuildingCode.StylePriority.UseTextAlignment = false;
            this.BuildingCode.Text = "Building Code :";
            this.BuildingCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // buildCode
            // 
            this.buildCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.buildCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L5No")});
            this.buildCode.Dpi = 100F;
            this.buildCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildCode.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 171F);
            this.buildCode.Multiline = true;
            this.buildCode.Name = "buildCode";
            this.buildCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.buildCode.SizeF = new System.Drawing.SizeF(140.2083F, 22.99998F);
            this.buildCode.StylePriority.UseBorders = false;
            this.buildCode.StylePriority.UseFont = false;
            this.buildCode.StylePriority.UsePadding = false;
            this.buildCode.StylePriority.UseTextAlignment = false;
            this.buildCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lablPmNo
            // 
            this.lablPmNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lablPmNo.Dpi = 100F;
            this.lablPmNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablPmNo.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 194.0001F);
            this.lablPmNo.Name = "lablPmNo";
            this.lablPmNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lablPmNo.SizeF = new System.Drawing.SizeF(152.4583F, 22.99998F);
            this.lablPmNo.StylePriority.UseBorders = false;
            this.lablPmNo.StylePriority.UseFont = false;
            this.lablPmNo.StylePriority.UseTextAlignment = false;
            this.lablPmNo.Text = "PM No :";
            this.lablPmNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtPmNo
            // 
            this.txtPmNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtPmNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "PMNo")});
            this.txtPmNo.Dpi = 100F;
            this.txtPmNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmNo.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 194.0001F);
            this.txtPmNo.Multiline = true;
            this.txtPmNo.Name = "txtPmNo";
            this.txtPmNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtPmNo.SizeF = new System.Drawing.SizeF(140.2083F, 22.99998F);
            this.txtPmNo.StylePriority.UseBorders = false;
            this.txtPmNo.StylePriority.UseFont = false;
            this.txtPmNo.StylePriority.UsePadding = false;
            this.txtPmNo.StylePriority.UseTextAlignment = false;
            this.txtPmNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // failCode
            // 
            this.failCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.failCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "FailureCauseCode")});
            this.failCode.Dpi = 100F;
            this.failCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.failCode.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 79.00006F);
            this.failCode.Multiline = true;
            this.failCode.Name = "failCode";
            this.failCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.failCode.SizeF = new System.Drawing.SizeF(140.2083F, 22.99998F);
            this.failCode.StylePriority.UseBorders = false;
            this.failCode.StylePriority.UseFont = false;
            this.failCode.StylePriority.UsePadding = false;
            this.failCode.StylePriority.UseTextAlignment = false;
            this.failCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // FailureCode
            // 
            this.FailureCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.FailureCode.Dpi = 100F;
            this.FailureCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FailureCode.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 79.00006F);
            this.FailureCode.Name = "FailureCode";
            this.FailureCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.FailureCode.SizeF = new System.Drawing.SizeF(152.4583F, 22.99998F);
            this.FailureCode.StylePriority.UseBorders = false;
            this.FailureCode.StylePriority.UseFont = false;
            this.FailureCode.StylePriority.UseTextAlignment = false;
            this.FailureCode.Text = "Failure Code :";
            this.FailureCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // CityCode
            // 
            this.CityCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CityCode.Dpi = 100F;
            this.CityCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CityCode.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 102F);
            this.CityCode.Multiline = true;
            this.CityCode.Name = "CityCode";
            this.CityCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.CityCode.SizeF = new System.Drawing.SizeF(152.4583F, 23.00005F);
            this.CityCode.StylePriority.UseBorders = false;
            this.CityCode.StylePriority.UseFont = false;
            this.CityCode.StylePriority.UseTextAlignment = false;
            this.CityCode.Text = "City Code :";
            this.CityCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ctyCode
            // 
            this.ctyCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ctyCode.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L2Code")});
            this.ctyCode.Dpi = 100F;
            this.ctyCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctyCode.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 102F);
            this.ctyCode.Multiline = true;
            this.ctyCode.Name = "ctyCode";
            this.ctyCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ctyCode.SizeF = new System.Drawing.SizeF(140.2083F, 23.00005F);
            this.ctyCode.StylePriority.UseBorders = false;
            this.ctyCode.StylePriority.UseFont = false;
            this.ctyCode.StylePriority.UsePadding = false;
            this.ctyCode.StylePriority.UseTextAlignment = false;
            this.ctyCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AreaCode
            // 
            this.AreaCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.AreaCode.Dpi = 100F;
            this.AreaCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AreaCode.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 125.0001F);
            this.AreaCode.Name = "AreaCode";
            this.AreaCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.AreaCode.SizeF = new System.Drawing.SizeF(152.4583F, 22.99998F);
            this.AreaCode.StylePriority.UseBorders = false;
            this.AreaCode.StylePriority.UseFont = false;
            this.AreaCode.StylePriority.UseTextAlignment = false;
            this.AreaCode.Text = "Area Code :";
            this.AreaCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // areaCod
            // 
            this.areaCod.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.areaCod.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "L3No")});
            this.areaCod.Dpi = 100F;
            this.areaCod.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.areaCod.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 125.0001F);
            this.areaCod.Multiline = true;
            this.areaCod.Name = "areaCod";
            this.areaCod.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.areaCod.SizeF = new System.Drawing.SizeF(140.2083F, 22.99998F);
            this.areaCod.StylePriority.UseBorders = false;
            this.areaCod.StylePriority.UseFont = false;
            this.areaCod.StylePriority.UsePadding = false;
            this.areaCod.StylePriority.UseTextAlignment = false;
            this.areaCod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ProblemDesc
            // 
            this.ProblemDesc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ProblemDesc.Dpi = 100F;
            this.ProblemDesc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProblemDesc.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 9.999974F);
            this.ProblemDesc.Name = "ProblemDesc";
            this.ProblemDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ProblemDesc.SizeF = new System.Drawing.SizeF(152.4583F, 23.00002F);
            this.ProblemDesc.StylePriority.UseBorders = false;
            this.ProblemDesc.StylePriority.UseFont = false;
            this.ProblemDesc.StylePriority.UseTextAlignment = false;
            this.ProblemDesc.Text = "Problem Desc :";
            this.ProblemDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // problemDescription
            // 
            this.problemDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.problemDescription.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "ProblemDescription")});
            this.problemDescription.Dpi = 100F;
            this.problemDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.problemDescription.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 9.999943F);
            this.problemDescription.Multiline = true;
            this.problemDescription.Name = "problemDescription";
            this.problemDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.problemDescription.SizeF = new System.Drawing.SizeF(496.2379F, 23.00002F);
            this.problemDescription.StylePriority.UseBorders = false;
            this.problemDescription.StylePriority.UseFont = false;
            this.problemDescription.StylePriority.UsePadding = false;
            this.problemDescription.StylePriority.UseTextAlignment = false;
            this.problemDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NotestoAssets
            // 
            this.NotestoAssets.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NotestoAssets.Dpi = 100F;
            this.NotestoAssets.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotestoAssets.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 33.00009F);
            this.NotestoAssets.Name = "NotestoAssets";
            this.NotestoAssets.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NotestoAssets.SizeF = new System.Drawing.SizeF(152.4583F, 23.00003F);
            this.NotestoAssets.StylePriority.UseBorders = false;
            this.NotestoAssets.StylePriority.UseFont = false;
            this.NotestoAssets.StylePriority.UseTextAlignment = false;
            this.NotestoAssets.Text = "Notes To Tech.(Asset) :";
            this.NotestoAssets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // notesAssets
            // 
            this.notesAssets.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.notesAssets.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NoteToTechLocation")});
            this.notesAssets.Dpi = 100F;
            this.notesAssets.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesAssets.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 32.99996F);
            this.notesAssets.Multiline = true;
            this.notesAssets.Name = "notesAssets";
            this.notesAssets.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.notesAssets.SizeF = new System.Drawing.SizeF(357.0708F, 23F);
            this.notesAssets.StylePriority.UseBorders = false;
            this.notesAssets.StylePriority.UseFont = false;
            this.notesAssets.StylePriority.UsePadding = false;
            this.notesAssets.StylePriority.UseTextAlignment = false;
            this.notesAssets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NotesToLocation
            // 
            this.NotesToLocation.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NotesToLocation.Dpi = 100F;
            this.NotesToLocation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotesToLocation.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 56.00001F);
            this.NotesToLocation.Name = "NotesToLocation";
            this.NotesToLocation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NotesToLocation.SizeF = new System.Drawing.SizeF(152.4583F, 23.00006F);
            this.NotesToLocation.StylePriority.UseBorders = false;
            this.NotesToLocation.StylePriority.UseFont = false;
            this.NotesToLocation.StylePriority.UseTextAlignment = false;
            this.NotesToLocation.Text = "Notes To Tech.(Location) :";
            this.NotesToLocation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "NoteToTechLocation")});
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(155.7412F, 56.00001F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(357.0708F, 23.00005F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UsePadding = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Panel4
            // 
            this.Panel4.BorderColor = System.Drawing.Color.LightGray;
            this.Panel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtPrevTaken,
            this.txtActionTaken,
            this.txtCauseDescription,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel1});
            this.Panel4.Dpi = 100F;
            this.Panel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 395.125F);
            this.Panel4.Name = "Panel4";
            this.Panel4.SizeF = new System.Drawing.SizeF(830F, 87.50006F);
            this.Panel4.StylePriority.UseBorderColor = false;
            this.Panel4.StylePriority.UseBorders = false;
            // 
            // txtPrevTaken
            // 
            this.txtPrevTaken.BorderColor = System.Drawing.Color.LightGray;
            this.txtPrevTaken.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtPrevTaken.Dpi = 100F;
            this.txtPrevTaken.LocationFloat = new DevExpress.Utils.PointFloat(559.5609F, 23.74984F);
            this.txtPrevTaken.Name = "txtPrevTaken";
            this.txtPrevTaken.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtPrevTaken.SizeF = new System.Drawing.SizeF(268.6265F, 60.12513F);
            this.txtPrevTaken.StylePriority.UseBorderColor = false;
            this.txtPrevTaken.StylePriority.UseBorders = false;
            // 
            // txtActionTaken
            // 
            this.txtActionTaken.BorderColor = System.Drawing.Color.LightGray;
            this.txtActionTaken.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtActionTaken.Dpi = 100F;
            this.txtActionTaken.LocationFloat = new DevExpress.Utils.PointFloat(281.5304F, 23.74998F);
            this.txtActionTaken.Name = "txtActionTaken";
            this.txtActionTaken.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtActionTaken.SizeF = new System.Drawing.SizeF(268.6265F, 60.12513F);
            this.txtActionTaken.StylePriority.UseBorderColor = false;
            this.txtActionTaken.StylePriority.UseBorders = false;
            // 
            // txtCauseDescription
            // 
            this.txtCauseDescription.BorderColor = System.Drawing.Color.LightGray;
            this.txtCauseDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtCauseDescription.Dpi = 100F;
            this.txtCauseDescription.LocationFloat = new DevExpress.Utils.PointFloat(2.499906F, 23.74989F);
            this.txtCauseDescription.Name = "txtCauseDescription";
            this.txtCauseDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtCauseDescription.SizeF = new System.Drawing.SizeF(268.6265F, 60.12513F);
            this.txtCauseDescription.StylePriority.UseBorderColor = false;
            this.txtCauseDescription.StylePriority.UseBorders = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.BorderWidth = 0F;
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(3F, 3F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(244.7917F, 16.74995F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cause Description";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.BorderWidth = 0F;
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(295.6041F, 3F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(244.7917F, 16.74995F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Action Taken";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.BorderWidth = 0F;
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(570.6039F, 3.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(222.9672F, 16.74995F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseBorderWidth = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Prevention Taken";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderColor = System.Drawing.Color.LightGray;
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.BorderWidth = 0F;
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 827.7499F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(824.4998F, 16.74988F);
            this.xrLabel15.StylePriority.UseBorderColor = false;
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseBorderWidth = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Certificate work complition";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Panel6
            // 
            this.Panel6.BorderColor = System.Drawing.Color.LightGray;
            this.Panel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel35,
            this.xrLabel34,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel21,
            this.xrLabel24,
            this.xrLabel25});
            this.Panel6.Dpi = 100F;
            this.Panel6.LocationFloat = new DevExpress.Utils.PointFloat(4.49992F, 852.5F);
            this.Panel6.Name = "Panel6";
            this.Panel6.SizeF = new System.Drawing.SizeF(398.4183F, 95.54175F);
            this.Panel6.StylePriority.UseBorderColor = false;
            this.Panel6.StylePriority.UseBorders = false;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Dpi = 100F;
            this.xrLabel35.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(233.9586F, 24.58324F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "............................................";
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Dpi = 100F;
            this.xrLabel34.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(2F, 24.58324F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.Text = "............................................";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 8.6F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(2F, 5.749878F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(255.6264F, 18.83337F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "Name And Signature of Technician/Engineer";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.BorderWidth = 0F;
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(2.000006F, 43.58323F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(38.54166F, 23.00002F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseBorderWidth = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Name";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.BorderWidth = 0F;
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(2.000006F, 67.58321F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(38.54165F, 23.00002F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseBorderWidth = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Date :";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.BorderWidth = 0F;
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(232.7083F, 43.5833F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(71.87495F, 22.99995F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseBorderWidth = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Signature";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.BorderWidth = 0F;
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(232.7083F, 67.58331F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(60.41666F, 23.00006F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseBorderWidth = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Time :";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Panel7
            // 
            this.Panel7.BorderColor = System.Drawing.Color.LightGray;
            this.Panel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.xrLabel20,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel36});
            this.Panel7.Dpi = 100F;
            this.Panel7.LocationFloat = new DevExpress.Utils.PointFloat(404.4182F, 852.4999F);
            this.Panel7.Name = "Panel7";
            this.Panel7.SizeF = new System.Drawing.SizeF(423.7692F, 95.54181F);
            this.Panel7.StylePriority.UseBorderColor = false;
            this.Panel7.StylePriority.UseBorders = false;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.Dpi = 100F;
            this.xrLabel37.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(233.9478F, 23.83327F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.Text = "............................................";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 4.999974F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(275.1128F, 18.83335F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Name And Signature of Requester/Supervisior";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.BorderWidth = 0F;
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 43.83335F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(43.30912F, 23.00002F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseBorderWidth = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Name";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.BorderWidth = 0F;
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(11.23965F, 66.83337F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(43.30912F, 23.00002F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseBorderWidth = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Date :";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.BorderWidth = 0F;
            this.xrLabel27.Dpi = 100F;
            this.xrLabel27.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(233.9479F, 66.83344F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(60.41669F, 23.00006F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseBorderWidth = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Time :";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.BorderWidth = 0F;
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(233.9478F, 42.83344F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(71.87495F, 22.99994F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseBorderWidth = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Signature";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Dpi = 100F;
            this.xrLabel36.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 23.83334F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.Text = "............................................";
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "GetJobOrderDetailsForDataSource";
            this.objectDataSource1.DataSource = typeof(CMMS.Service.JobOrderService);
            this.objectDataSource1.Name = "objectDataSource1";
            parameter1.Name = "employeeId";
            parameter1.Type = typeof(string);
            parameter2.Name = "NonCentral";
            parameter2.Type = typeof(int);
            parameter2.ValueInfo = "0";
            this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1,
            parameter2});
            // 
            // SafetyInstructionsDetailReport
            // 
            this.SafetyInstructionsDetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.SafetyInstructionDetails,
            this.SafetyInstruction});
            this.SafetyInstructionsDetailReport.DataSource = this.objectDataSource3;
            this.SafetyInstructionsDetailReport.Dpi = 100F;
            this.SafetyInstructionsDetailReport.Level = 0;
            this.SafetyInstructionsDetailReport.Name = "SafetyInstructionsDetailReport";
            this.SafetyInstructionsDetailReport.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
            // 
            // SafetyInstructionDetails
            // 
            this.SafetyInstructionDetails.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
            this.SafetyInstructionDetails.Dpi = 100F;
            this.SafetyInstructionDetails.HeightF = 30.43054F;
            this.SafetyInstructionDetails.Name = "SafetyInstructionDetails";
            // 
            // xrPanel2
            // 
            this.xrPanel2.BorderColor = System.Drawing.Color.LightGray;
            this.xrPanel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.xrPanel2.Dpi = 100F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.999919F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(831F, 28.43062F);
            this.xrPanel2.StylePriority.UseBorderColor = false;
            this.xrPanel2.StylePriority.UseBorders = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(2.579742F, 5.000056F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(825.4203F, 18.43062F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.seqId,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.73722488403320274D;
            // 
            // seqId
            // 
            this.seqId.BorderWidth = 0F;
            this.seqId.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource2, "SeqId")});
            this.seqId.Dpi = 100F;
            this.seqId.KeepTogether = true;
            this.seqId.Name = "seqId";
            this.seqId.StylePriority.UseBorderWidth = false;
            this.seqId.StylePriority.UseTextAlignment = false;
            this.seqId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.seqId.Weight = 0.34723740497213573D;
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.DataMember = "GetSafetyInsturctionDetailsForDataSource";
            this.objectDataSource2.DataSource = typeof(CMMS.Service.JobOrderService);
            this.objectDataSource2.Name = "objectDataSource2";
            parameter3.Name = "employeeId";
            parameter3.Type = typeof(string);
            parameter4.Name = "NonCentral";
            parameter4.Type = typeof(int);
            parameter4.ValueInfo = "0";
            this.objectDataSource2.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter3,
            parameter4});
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderWidth = 0F;
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource2, "Details")});
            this.xrTableCell2.Dpi = 100F;
            this.xrTableCell2.KeepTogether = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorderWidth = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1.5191747831800806D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderWidth = 0F;
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource2, "AltDetails")});
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.KeepTogether = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorderWidth = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 1.1273972623534352D;
            // 
            // SafetyInstruction
            // 
            this.SafetyInstruction.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel33});
            this.SafetyInstruction.Dpi = 100F;
            this.SafetyInstruction.HeightF = 27.83337F;
            this.SafetyInstruction.Name = "SafetyInstruction";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.BorderWidth = 0F;
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(2.50002F, 9F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Suppress;
            this.xrLabel33.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseBorderWidth = false;
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.Text = "Safety Instructions :";
            // 
            // objectDataSource3
            // 
            this.objectDataSource3.DataMember = "GetJobOrderPMCheckListForDataSource";
            this.objectDataSource3.DataSource = typeof(CMMS.Service.JobOrderService);
            this.objectDataSource3.Name = "objectDataSource3";
            parameter5.Name = "employeeId";
            parameter5.Type = typeof(string);
            parameter6.Name = "NonCentral";
            parameter6.Type = typeof(int);
            parameter6.ValueInfo = "0";
            this.objectDataSource3.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter5,
            parameter6});
            // 
            // CheckListDetailReport
            // 
            this.CheckListDetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.CheckListDetail,
            this.CheckListHeader});
            this.CheckListDetailReport.BorderColor = System.Drawing.Color.LightGray;
            this.CheckListDetailReport.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.CheckListDetailReport.DataSource = this.objectDataSource3;
            this.CheckListDetailReport.Dpi = 100F;
            this.CheckListDetailReport.Level = 1;
            this.CheckListDetailReport.Name = "CheckListDetailReport";
            // 
            // CheckListDetail
            // 
            this.CheckListDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.checkListTable});
            this.CheckListDetail.Dpi = 100F;
            this.CheckListDetail.HeightF = 30.20833F;
            this.CheckListDetail.Name = "CheckListDetail";
            // 
            // checkListTable
            // 
            this.checkListTable.Dpi = 100F;
            this.checkListTable.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkListTable.LocationFloat = new DevExpress.Utils.PointFloat(3.499905F, 5.208302F);
            this.checkListTable.Name = "checkListTable";
            this.checkListTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.checkListTable.SizeF = new System.Drawing.SizeF(825.4203F, 25.00003F);
            this.checkListTable.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.seqNo,
            this.taskDesc,
            this.altTaskDesc,
            this.xrTableCell1});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // seqNo
            // 
            this.seqNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SeqNo")});
            this.seqNo.Dpi = 100F;
            this.seqNo.Name = "seqNo";
            this.seqNo.StylePriority.UseTextAlignment = false;
            this.seqNo.Text = "Seq No";
            this.seqNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.seqNo.Weight = 0.53485266970346312D;
            // 
            // taskDesc
            // 
            this.taskDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TaskDesc")});
            this.taskDesc.Dpi = 100F;
            this.taskDesc.Name = "taskDesc";
            this.taskDesc.StylePriority.UseTextAlignment = false;
            this.taskDesc.Text = "TaskDesc";
            this.taskDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.taskDesc.Weight = 1.1739354166357425D;
            // 
            // altTaskDesc
            // 
            this.altTaskDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AltTaskDesc")});
            this.altTaskDesc.Dpi = 100F;
            this.altTaskDesc.Name = "altTaskDesc";
            this.altTaskDesc.StylePriority.UseTextAlignment = false;
            this.altTaskDesc.Text = "AltTaskDesc";
            this.altTaskDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.altTaskDesc.Weight = 1.0506155191616895D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrShape1});
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.18897610672682394D;
            // 
            // xrShape1
            // 
            this.xrShape1.Dpi = 100F;
            this.xrShape1.LocationFloat = new DevExpress.Utils.PointFloat(13.6239F, 2.791641F);
            this.xrShape1.Name = "xrShape1";
            this.xrShape1.Shape = shapeRectangle1;
            this.xrShape1.SizeF = new System.Drawing.SizeF(27.0092F, 20.20842F);
            this.xrShape1.Stretch = true;
            // 
            // CheckListHeader
            // 
            this.CheckListHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel28,
            this.xrLabel29,
            this.xrLabel30,
            this.xrLabel31,
            this.xrLabel32});
            this.CheckListHeader.Dpi = 100F;
            this.CheckListHeader.HeightF = 53.125F;
            this.CheckListHeader.Name = "CheckListHeader";
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.Dpi = 100F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(2.50002F, 4.93869F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(283.1249F, 16.57095F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.Text = "Refer To Attached PM Checklist";
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(2.500017F, 24.29161F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(104.3161F, 26.18633F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "PM Checklist No:";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource1, "ChecklistNo")});
            this.xrLabel30.Dpi = 100F;
            this.xrLabel30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(108.2046F, 24.29161F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(177.4203F, 26.18633F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(469.6353F, 24.2916F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(151.4598F, 26.18633F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "PM Checklist Desc:";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource1, "CauseDescription")});
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(621.0951F, 24.29161F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(208.745F, 26.18633F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupFooter
            // 
            this.GroupFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
            this.GroupFooter.Dpi = 100F;
            this.GroupFooter.HeightF = 28.87179F;
            this.GroupFooter.Name = "GroupFooter";
            this.GroupFooter.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
            this.GroupFooter.PrintAtBottom = true;
            this.GroupFooter.RepeatEveryPage = true;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.Format = "{0:dd-MMM-yy}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(6.81604F, 2F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(100.0001F, 23F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "Page {0} of {1}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(718.3333F, 2.000014F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100.0001F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // ItemDetailTable
            // 
            this.ItemDetailTable.Dpi = 100F;
            this.ItemDetailTable.LocationFloat = new DevExpress.Utils.PointFloat(2.999997F, 26.74993F);
            this.ItemDetailTable.Name = "ItemDetailTable";
            this.ItemDetailTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.TableRow1,
            this.TableRow2,
            this.TableRow3,
            this.TableRow4});
            this.ItemDetailTable.SizeF = new System.Drawing.SizeF(824.4998F, 101.4167F);
            // 
            // TableRow1
            // 
            this.TableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell1,
            this.TableCell2,
            this.TableCell3,
            this.TableCell4,
            this.TableCell5,
            this.TableCell6});
            this.TableRow1.Dpi = 100F;
            this.TableRow1.Name = "TableRow1";
            this.TableRow1.Weight = 1D;
            // 
            // TableCell1
            // 
            this.TableCell1.BackColor = System.Drawing.Color.LightGray;
            this.TableCell1.BorderColor = System.Drawing.Color.LightGray;
            this.TableCell1.Dpi = 100F;
            this.TableCell1.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell1.Name = "TableCell1";
            this.TableCell1.StylePriority.UseBackColor = false;
            this.TableCell1.StylePriority.UseBorderColor = false;
            this.TableCell1.StylePriority.UseFont = false;
            this.TableCell1.StylePriority.UseTextAlignment = false;
            this.TableCell1.Text = "Item No.";
            this.TableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell1.Weight = 1.8020832824707032D;
            // 
            // TableCell2
            // 
            this.TableCell2.BackColor = System.Drawing.Color.LightGray;
            this.TableCell2.Dpi = 100F;
            this.TableCell2.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell2.Name = "TableCell2";
            this.TableCell2.StylePriority.UseBackColor = false;
            this.TableCell2.StylePriority.UseFont = false;
            this.TableCell2.StylePriority.UseTextAlignment = false;
            this.TableCell2.Text = "Item Description";
            this.TableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell2.Weight = 1.8021000671386716D;
            // 
            // TableCell3
            // 
            this.TableCell3.BackColor = System.Drawing.Color.LightGray;
            this.TableCell3.Dpi = 100F;
            this.TableCell3.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell3.Name = "TableCell3";
            this.TableCell3.StylePriority.UseBackColor = false;
            this.TableCell3.StylePriority.UseFont = false;
            this.TableCell3.StylePriority.UseTextAlignment = false;
            this.TableCell3.Text = "Issue Type(D/I)";
            this.TableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell3.Weight = 0.99790008544921882D;
            // 
            // TableCell4
            // 
            this.TableCell4.BackColor = System.Drawing.Color.LightGray;
            this.TableCell4.Dpi = 100F;
            this.TableCell4.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell4.Name = "TableCell4";
            this.TableCell4.StylePriority.UseBackColor = false;
            this.TableCell4.StylePriority.UseFont = false;
            this.TableCell4.StylePriority.UseTextAlignment = false;
            this.TableCell4.Text = "UOM";
            this.TableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell4.Weight = 1.0520834096272784D;
            // 
            // TableCell5
            // 
            this.TableCell5.BackColor = System.Drawing.Color.LightGray;
            this.TableCell5.Dpi = 100F;
            this.TableCell5.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell5.Name = "TableCell5";
            this.TableCell5.StylePriority.UseBackColor = false;
            this.TableCell5.StylePriority.UseFont = false;
            this.TableCell5.StylePriority.UseTextAlignment = false;
            this.TableCell5.Text = "Qty Used";
            this.TableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell5.Weight = 1.1910790952996777D;
            // 
            // TableCell6
            // 
            this.TableCell6.BackColor = System.Drawing.Color.LightGray;
            this.TableCell6.Dpi = 100F;
            this.TableCell6.Font = new System.Drawing.Font("Arial", 8.6F, System.Drawing.FontStyle.Bold);
            this.TableCell6.Name = "TableCell6";
            this.TableCell6.StylePriority.UseBackColor = false;
            this.TableCell6.StylePriority.UseFont = false;
            this.TableCell6.StylePriority.UseTextAlignment = false;
            this.TableCell6.Text = "Qty Returned";
            this.TableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell6.Weight = 1.2547522289597628D;
            // 
            // TableRow2
            // 
            this.TableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell7,
            this.TableCell8,
            this.TableCell9,
            this.TableCell10,
            this.TableCell11,
            this.TableCell12});
            this.TableRow2.Dpi = 100F;
            this.TableRow2.Name = "TableRow2";
            this.TableRow2.Weight = 1D;
            // 
            // TableCell7
            // 
            this.TableCell7.Dpi = 100F;
            this.TableCell7.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell7.Name = "TableCell7";
            this.TableCell7.StylePriority.UseFont = false;
            this.TableCell7.StylePriority.UseTextAlignment = false;
            this.TableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell7.Weight = 1.8020832824707032D;
            // 
            // TableCell8
            // 
            this.TableCell8.Dpi = 100F;
            this.TableCell8.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell8.Name = "TableCell8";
            this.TableCell8.StylePriority.UseFont = false;
            this.TableCell8.StylePriority.UseTextAlignment = false;
            this.TableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell8.Weight = 1.8021000671386716D;
            // 
            // TableCell9
            // 
            this.TableCell9.Dpi = 100F;
            this.TableCell9.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell9.Name = "TableCell9";
            this.TableCell9.StylePriority.UseFont = false;
            this.TableCell9.StylePriority.UseTextAlignment = false;
            this.TableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell9.Weight = 0.99790008544921882D;
            // 
            // TableCell10
            // 
            this.TableCell10.Dpi = 100F;
            this.TableCell10.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell10.Name = "TableCell10";
            this.TableCell10.StylePriority.UseFont = false;
            this.TableCell10.StylePriority.UseTextAlignment = false;
            this.TableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell10.Weight = 1.0520834096272784D;
            // 
            // TableCell11
            // 
            this.TableCell11.Dpi = 100F;
            this.TableCell11.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell11.Name = "TableCell11";
            this.TableCell11.StylePriority.UseFont = false;
            this.TableCell11.StylePriority.UseTextAlignment = false;
            this.TableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell11.Weight = 1.1910790952996777D;
            // 
            // TableCell12
            // 
            this.TableCell12.Dpi = 100F;
            this.TableCell12.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell12.Name = "TableCell12";
            this.TableCell12.StylePriority.UseFont = false;
            this.TableCell12.StylePriority.UseTextAlignment = false;
            this.TableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell12.Weight = 1.2547522289597628D;
            // 
            // TableRow3
            // 
            this.TableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell13,
            this.TableCell14,
            this.TableCell15,
            this.TableCell16,
            this.TableCell17,
            this.TableCell18});
            this.TableRow3.Dpi = 100F;
            this.TableRow3.Name = "TableRow3";
            this.TableRow3.Weight = 1D;
            // 
            // TableCell13
            // 
            this.TableCell13.Dpi = 100F;
            this.TableCell13.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell13.Name = "TableCell13";
            this.TableCell13.StylePriority.UseFont = false;
            this.TableCell13.StylePriority.UseTextAlignment = false;
            this.TableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell13.Weight = 1.8020832824707032D;
            // 
            // TableCell14
            // 
            this.TableCell14.Dpi = 100F;
            this.TableCell14.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell14.Name = "TableCell14";
            this.TableCell14.StylePriority.UseFont = false;
            this.TableCell14.StylePriority.UseTextAlignment = false;
            this.TableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell14.Weight = 1.8021000671386716D;
            // 
            // TableCell15
            // 
            this.TableCell15.Dpi = 100F;
            this.TableCell15.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell15.Name = "TableCell15";
            this.TableCell15.StylePriority.UseFont = false;
            this.TableCell15.StylePriority.UseTextAlignment = false;
            this.TableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell15.Weight = 0.99790008544921882D;
            // 
            // TableCell16
            // 
            this.TableCell16.Dpi = 100F;
            this.TableCell16.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell16.Name = "TableCell16";
            this.TableCell16.StylePriority.UseFont = false;
            this.TableCell16.StylePriority.UseTextAlignment = false;
            this.TableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell16.Weight = 1.0520834096272784D;
            // 
            // TableCell17
            // 
            this.TableCell17.Dpi = 100F;
            this.TableCell17.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell17.Name = "TableCell17";
            this.TableCell17.StylePriority.UseFont = false;
            this.TableCell17.StylePriority.UseTextAlignment = false;
            this.TableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell17.Weight = 1.1910790952996777D;
            // 
            // TableCell18
            // 
            this.TableCell18.Dpi = 100F;
            this.TableCell18.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell18.Name = "TableCell18";
            this.TableCell18.StylePriority.UseFont = false;
            this.TableCell18.StylePriority.UseTextAlignment = false;
            this.TableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell18.Weight = 1.2547522289597628D;
            // 
            // TableRow4
            // 
            this.TableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell19,
            this.TableCell20,
            this.TableCell21,
            this.TableCell22,
            this.TableCell23,
            this.TableCell24});
            this.TableRow4.Dpi = 100F;
            this.TableRow4.Name = "TableRow4";
            this.TableRow4.Weight = 1D;
            // 
            // TableCell19
            // 
            this.TableCell19.Dpi = 100F;
            this.TableCell19.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell19.Name = "TableCell19";
            this.TableCell19.StylePriority.UseFont = false;
            this.TableCell19.StylePriority.UseTextAlignment = false;
            this.TableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell19.Weight = 1.8020832824707032D;
            // 
            // TableCell20
            // 
            this.TableCell20.Dpi = 100F;
            this.TableCell20.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell20.Name = "TableCell20";
            this.TableCell20.StylePriority.UseFont = false;
            this.TableCell20.StylePriority.UseTextAlignment = false;
            this.TableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell20.Weight = 1.8021000671386716D;
            // 
            // TableCell21
            // 
            this.TableCell21.Dpi = 100F;
            this.TableCell21.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell21.Name = "TableCell21";
            this.TableCell21.StylePriority.UseFont = false;
            this.TableCell21.StylePriority.UseTextAlignment = false;
            this.TableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell21.Weight = 0.99790008544921882D;
            // 
            // TableCell22
            // 
            this.TableCell22.Dpi = 100F;
            this.TableCell22.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell22.Name = "TableCell22";
            this.TableCell22.StylePriority.UseFont = false;
            this.TableCell22.StylePriority.UseTextAlignment = false;
            this.TableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell22.Weight = 1.0520834096272784D;
            // 
            // TableCell23
            // 
            this.TableCell23.Dpi = 100F;
            this.TableCell23.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell23.Name = "TableCell23";
            this.TableCell23.StylePriority.UseFont = false;
            this.TableCell23.StylePriority.UseTextAlignment = false;
            this.TableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell23.Weight = 1.1910790952996777D;
            // 
            // TableCell24
            // 
            this.TableCell24.Dpi = 100F;
            this.TableCell24.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell24.Name = "TableCell24";
            this.TableCell24.StylePriority.UseFont = false;
            this.TableCell24.StylePriority.UseTextAlignment = false;
            this.TableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell24.Weight = 1.2547522289597628D;
            // 
            // PMMeterJobOrderReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.SafetyInstructionsDetailReport,
            this.CheckListDetailReport,
            this.GroupFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1,
            this.objectDataSource2,
            this.objectDataSource3});
            this.DataSource = this.objectDataSource1;
            this.Margins = new System.Drawing.Printing.Margins(10, 9, 35, 1);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDetailTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkListTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        public DevExpress.XtraReports.UI.XRPanel Panel1;
        public DevExpress.XtraReports.UI.XRLabel jobOrderNo;
        public DevExpress.XtraReports.UI.XRLabel JOPlanNo;
        public DevExpress.XtraReports.UI.XRLabel label1;
        public DevExpress.XtraReports.UI.XRLabel JobTrade;
        public DevExpress.XtraReports.UI.XRLabel label2;
        public DevExpress.XtraReports.UI.XRLabel JobStatus;
        public DevExpress.XtraReports.UI.XRLabel xrLabel2;
        public DevExpress.XtraReports.UI.XRLabel JobType;
        public DevExpress.XtraReports.UI.XRLabel xrLabel3;
        public DevExpress.XtraReports.UI.XRLabel JobPriority;
        public DevExpress.XtraReports.UI.XRPanel Panel2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel4;
        public DevExpress.XtraReports.UI.XRLabel assignedSupplier;
        public DevExpress.XtraReports.UI.XRLabel CreatedBy;
        public DevExpress.XtraReports.UI.XRLabel creatdBy;
        public DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel rcvDatetime;
        public DevExpress.XtraReports.UI.XRLabel xrLabel6;
        public DevExpress.XtraReports.UI.XRLabel reqDatetime;
        public DevExpress.XtraReports.UI.XRLabel xrLabel7;
        public DevExpress.XtraReports.UI.XRLabel requester;
        public DevExpress.XtraReports.UI.XRLabel xrLabel8;
        public DevExpress.XtraReports.UI.XRLabel phoneNumber;
        public DevExpress.XtraReports.UI.XRPanel Panel3;
        public DevExpress.XtraReports.UI.XRLabel locationNumber;
        public DevExpress.XtraReports.UI.XRLabel txtLocationNumber;
        public DevExpress.XtraReports.UI.XRLabel LocationDescription;
        public DevExpress.XtraReports.UI.XRLabel txtlocationDescription;
        public DevExpress.XtraReports.UI.XRLabel Asset;
        public DevExpress.XtraReports.UI.XRLabel txtAsset;
        public DevExpress.XtraReports.UI.XRLabel AssetDescription;
        public DevExpress.XtraReports.UI.XRLabel txtAssetDescription;
        public DevExpress.XtraReports.UI.XRLabel MaintDivCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintDivCode;
        public DevExpress.XtraReports.UI.XRLabel MaintDeptCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintDeptCode;
        public DevExpress.XtraReports.UI.XRLabel MaintSubDeptCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintSubDeptCode;
        public DevExpress.XtraReports.UI.XRLabel znCode;
        public DevExpress.XtraReports.UI.XRLabel ZoneCode;
        public DevExpress.XtraReports.UI.XRLabel BuildingCode;
        public DevExpress.XtraReports.UI.XRLabel buildCode;
        public DevExpress.XtraReports.UI.XRLabel lablPmNo;
        public DevExpress.XtraReports.UI.XRLabel txtPmNo;
        public DevExpress.XtraReports.UI.XRLabel failCode;
        public DevExpress.XtraReports.UI.XRLabel FailureCode;
        public DevExpress.XtraReports.UI.XRLabel CityCode;
        public DevExpress.XtraReports.UI.XRLabel ctyCode;
        public DevExpress.XtraReports.UI.XRLabel AreaCode;
        public DevExpress.XtraReports.UI.XRLabel areaCod;
        public DevExpress.XtraReports.UI.XRLabel ProblemDesc;
        public DevExpress.XtraReports.UI.XRLabel problemDescription;
        public DevExpress.XtraReports.UI.XRLabel NotestoAssets;
        public DevExpress.XtraReports.UI.XRLabel notesAssets;
        public DevExpress.XtraReports.UI.XRLabel NotesToLocation;
        public DevExpress.XtraReports.UI.XRLabel xrLabel14;
        public DevExpress.XtraReports.UI.XRPanel Panel4;
        public DevExpress.XtraReports.UI.XRLabel txtPrevTaken;
        public DevExpress.XtraReports.UI.XRLabel txtActionTaken;
        public DevExpress.XtraReports.UI.XRLabel txtCauseDescription;
        public DevExpress.XtraReports.UI.XRLabel xrLabel10;
        public DevExpress.XtraReports.UI.XRLabel xrLabel9;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRPanel Panel5;
        private DevExpress.XtraReports.UI.XRPanel Panel52;
        private DevExpress.XtraReports.UI.XRLabel TimeCompleted;
        private DevExpress.XtraReports.UI.XRLabel DateComplete;
        private DevExpress.XtraReports.UI.XRLabel DateTimeCopleted;
        public DevExpress.XtraReports.UI.XRPanel Panel51;
        public DevExpress.XtraReports.UI.XRLabel Time;
        public DevExpress.XtraReports.UI.XRLabel Date;
        public DevExpress.XtraReports.UI.XRLabel DateTimeStarted;
        public DevExpress.XtraReports.UI.XRTable EmployeeDetailTable;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        public DevExpress.XtraReports.UI.XRTableCell EmployeeName;
        public DevExpress.XtraReports.UI.XRTableCell EmployeeNo;
        public DevExpress.XtraReports.UI.XRTableCell Normal;
        public DevExpress.XtraReports.UI.XRTableCell OT1;
        public DevExpress.XtraReports.UI.XRTableCell OT2;
        public DevExpress.XtraReports.UI.XRTableCell OT3;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        public DevExpress.XtraReports.UI.XRTableCell TableCell31;
        public DevExpress.XtraReports.UI.XRTableCell TableCell32;
        public DevExpress.XtraReports.UI.XRTableCell TableCell33;
        public DevExpress.XtraReports.UI.XRTableCell TableCell34;
        public DevExpress.XtraReports.UI.XRTableCell TableCell35;
        public DevExpress.XtraReports.UI.XRTableCell TableCell36;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        public DevExpress.XtraReports.UI.XRTableCell TableCell37;
        public DevExpress.XtraReports.UI.XRTableCell TableCell38;
        public DevExpress.XtraReports.UI.XRTableCell TableCell39;
        public DevExpress.XtraReports.UI.XRTableCell TableCell40;
        public DevExpress.XtraReports.UI.XRTableCell TableCell41;
        public DevExpress.XtraReports.UI.XRTableCell TableCell42;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        public DevExpress.XtraReports.UI.XRTableCell TableCell43;
        public DevExpress.XtraReports.UI.XRTableCell TableCell44;
        public DevExpress.XtraReports.UI.XRTableCell TableCell45;
        public DevExpress.XtraReports.UI.XRTableCell TableCell46;
        public DevExpress.XtraReports.UI.XRTableCell TableCell47;
        public DevExpress.XtraReports.UI.XRTableCell TableCell48;
        public DevExpress.XtraReports.UI.XRLabel DI;
        public DevExpress.XtraReports.UI.XRLabel ItemDetailLabl;
        public DevExpress.XtraReports.UI.XRLabel xrLabel15;
        public DevExpress.XtraReports.UI.XRPanel Panel6;
        public DevExpress.XtraReports.UI.XRLabel xrLabel35;
        public DevExpress.XtraReports.UI.XRLabel xrLabel34;
        public DevExpress.XtraReports.UI.XRLabel xrLabel18;
        public DevExpress.XtraReports.UI.XRLabel xrLabel19;
        public DevExpress.XtraReports.UI.XRLabel xrLabel21;
        public DevExpress.XtraReports.UI.XRLabel xrLabel24;
        public DevExpress.XtraReports.UI.XRLabel xrLabel25;
        public DevExpress.XtraReports.UI.XRPanel Panel7;
        public DevExpress.XtraReports.UI.XRLabel xrLabel37;
        public DevExpress.XtraReports.UI.XRLabel xrLabel20;
        public DevExpress.XtraReports.UI.XRLabel xrLabel22;
        public DevExpress.XtraReports.UI.XRLabel xrLabel23;
        public DevExpress.XtraReports.UI.XRLabel xrLabel27;
        public DevExpress.XtraReports.UI.XRLabel xrLabel26;
        public DevExpress.XtraReports.UI.XRLabel xrLabel36;
        public DevExpress.XtraReports.UI.XRLabel xrLabel16;
        public DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.DetailReportBand SafetyInstructionsDetailReport;
        private DevExpress.XtraReports.UI.DetailBand SafetyInstructionDetails;
        private DevExpress.XtraReports.UI.ReportHeaderBand SafetyInstruction;
        public DevExpress.XtraReports.UI.XRLabel xrLabel33;
        public DevExpress.XtraReports.UI.XRPanel xrPanel2;
        public DevExpress.XtraReports.UI.XRTable xrTable1;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        public DevExpress.XtraReports.UI.XRTableCell seqId;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.DetailReportBand CheckListDetailReport;
        private DevExpress.XtraReports.UI.DetailBand CheckListDetail;
        private DevExpress.XtraReports.UI.ReportHeaderBand CheckListHeader;
        public DevExpress.XtraReports.UI.XRLabel xrLabel28;
        public DevExpress.XtraReports.UI.XRLabel xrLabel29;
        public DevExpress.XtraReports.UI.XRLabel xrLabel30;
        public DevExpress.XtraReports.UI.XRLabel xrLabel31;
        public DevExpress.XtraReports.UI.XRLabel xrLabel32;
        public DevExpress.XtraReports.UI.XRTable checkListTable;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        public DevExpress.XtraReports.UI.XRTableCell seqNo;
        public DevExpress.XtraReports.UI.XRTableCell taskDesc;
        public DevExpress.XtraReports.UI.XRTableCell altTaskDesc;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter;
        public DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        public DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource3;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRShape xrShape1;
        public DevExpress.XtraReports.UI.XRTable ItemDetailTable;
        public DevExpress.XtraReports.UI.XRTableRow TableRow1;
        public DevExpress.XtraReports.UI.XRTableCell TableCell1;
        public DevExpress.XtraReports.UI.XRTableCell TableCell2;
        public DevExpress.XtraReports.UI.XRTableCell TableCell3;
        public DevExpress.XtraReports.UI.XRTableCell TableCell4;
        public DevExpress.XtraReports.UI.XRTableCell TableCell5;
        public DevExpress.XtraReports.UI.XRTableCell TableCell6;
        public DevExpress.XtraReports.UI.XRTableRow TableRow2;
        public DevExpress.XtraReports.UI.XRTableCell TableCell7;
        public DevExpress.XtraReports.UI.XRTableCell TableCell8;
        public DevExpress.XtraReports.UI.XRTableCell TableCell9;
        public DevExpress.XtraReports.UI.XRTableCell TableCell10;
        public DevExpress.XtraReports.UI.XRTableCell TableCell11;
        public DevExpress.XtraReports.UI.XRTableCell TableCell12;
        public DevExpress.XtraReports.UI.XRTableRow TableRow3;
        public DevExpress.XtraReports.UI.XRTableCell TableCell13;
        public DevExpress.XtraReports.UI.XRTableCell TableCell14;
        public DevExpress.XtraReports.UI.XRTableCell TableCell15;
        public DevExpress.XtraReports.UI.XRTableCell TableCell16;
        public DevExpress.XtraReports.UI.XRTableCell TableCell17;
        public DevExpress.XtraReports.UI.XRTableCell TableCell18;
        public DevExpress.XtraReports.UI.XRTableRow TableRow4;
        public DevExpress.XtraReports.UI.XRTableCell TableCell19;
        public DevExpress.XtraReports.UI.XRTableCell TableCell20;
        public DevExpress.XtraReports.UI.XRTableCell TableCell21;
        public DevExpress.XtraReports.UI.XRTableCell TableCell22;
        public DevExpress.XtraReports.UI.XRTableCell TableCell23;
        public DevExpress.XtraReports.UI.XRTableCell TableCell24;
    }
}
