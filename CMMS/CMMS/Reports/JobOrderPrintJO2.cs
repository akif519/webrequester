﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Job Order Print JO2
    /// </summary>
    public partial class JobOrderPrintJO2 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobOrderPrintJO2"/> class.
        /// </summary>
        public JobOrderPrintJO2()
        {
            InitializeComponent();
        }
    }
}
