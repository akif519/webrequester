﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Job Plan Report
    /// </summary>
    public partial class JobPlanReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobPlanReport"/> class.
        /// </summary>
        public JobPlanReport()
        {
            InitializeComponent();
        }
    }
}
