﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Job Order Print JO1
    /// </summary>
    public partial class JobOrderPrintJO1 : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JobOrderPrintJO1"/> class.
        /// </summary>
        public JobOrderPrintJO1()
        {
            InitializeComponent();
        }
    }
}
