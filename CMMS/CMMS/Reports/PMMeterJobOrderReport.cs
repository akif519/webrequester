﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// PM meter Job Order Report
    /// </summary>
    public partial class PMMeterJobOrderReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PMMeterJobOrderReport"/> class.
        /// </summary>
        public PMMeterJobOrderReport()
        {
            InitializeComponent();
        }
    }
}
