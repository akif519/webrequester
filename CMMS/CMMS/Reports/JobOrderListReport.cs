﻿using System.Linq;
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using CMMS.Infrastructure;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using CMMS.Service;

namespace CMMS.Reports
{
    public partial class JobOrderListReport : DevExpress.XtraReports.UI.XtraReport
    {
        List<CMMS.Model.SITask> lstSITask = new List<Model.SITask>();
        List<CMMS.Model.SITask> lstSITaskFiltered = new List<Model.SITask>();
        List<CMMS.Model.Workorderelement> lstWoElem = new List<Model.Workorderelement>();
        List<CMMS.Model.Workorderelement> lstWoElemFiltered = new List<Model.Workorderelement>();
        JobOrderService objService = new JobOrderService();
        string woNo = string.Empty;

        public JobOrderListReport()
        {
            InitializeComponent();
            lstSITask = objService.GetSafetyInstructionByWorkOrderNo(string.Empty, 0, "SeqId", "Ascending").ToList();
            lstWoElem = objService.GetWorkOrderElementsByWorkOrderNo(string.Empty, 0, "SeqNo", "Ascending").ToList();
        }

        private void xrSubreport1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                woNo = Convert.ToString(GetCurrentColumnValue("WorkorderNo"));
                lstSITaskFiltered = lstSITask.ToList().Where(o => o.WoNo == woNo).ToList();
                ((XRSubreport)sender).ReportSource.DataSource = lstSITaskFiltered;
            }
            catch (Exception ex)
            {
                
            }
        }

        private void xrSubreport2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                woNo = Convert.ToString(GetCurrentColumnValue("WorkorderNo"));
                lstWoElemFiltered = lstWoElem.ToList().Where(o => o.WorkorderNo == woNo).ToList();
                ((XRSubreport)sender).ReportSource.DataSource = lstWoElemFiltered;
            }
            catch (Exception ex)
            {

            }
        }
    }
}
