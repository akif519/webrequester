﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Purchase Requisitions Report
    /// </summary>
    public partial class PurchaseRequisitionsReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseRequisitionsReport"/> class.
        /// </summary>
        public PurchaseRequisitionsReport()
        {
            InitializeComponent();
        }
    }
}
