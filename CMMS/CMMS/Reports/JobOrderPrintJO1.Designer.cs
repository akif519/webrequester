﻿namespace CMMS.Reports
{
    partial class JobOrderPrintJO1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter1 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.DataAccess.ObjectBinding.Parameter parameter2 = new DevExpress.DataAccess.ObjectBinding.Parameter();
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle1 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.jobOrderNo = new DevExpress.XtraReports.UI.XRLabel();
            this.JOPlanNo = new DevExpress.XtraReports.UI.XRLabel();
            this.label1 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobTrade = new DevExpress.XtraReports.UI.XRLabel();
            this.label2 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobType = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.JobPriority = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel143 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.assignedSupplier = new DevExpress.XtraReports.UI.XRLabel();
            this.CreatedBy = new DevExpress.XtraReports.UI.XRLabel();
            this.creatdBy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.rcvDatetime = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.reqDatetime = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.requester = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.phoneNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel155 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel152 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel151 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.altCityCode = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.altAreaCode = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.altZoneCode = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.altBuildingCode = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.locationNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.txtLocationNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.LocationDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.txtlocationDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.Asset = new DevExpress.XtraReports.UI.XRLabel();
            this.txtAsset = new DevExpress.XtraReports.UI.XRLabel();
            this.AssetDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.txtAssetDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintDivCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintDivCode = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.MaintSubDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMaintSubDeptCode = new DevExpress.XtraReports.UI.XRLabel();
            this.znCode = new DevExpress.XtraReports.UI.XRLabel();
            this.ZoneCode = new DevExpress.XtraReports.UI.XRLabel();
            this.BuildingCode = new DevExpress.XtraReports.UI.XRLabel();
            this.buildCode = new DevExpress.XtraReports.UI.XRLabel();
            this.lablPmNo = new DevExpress.XtraReports.UI.XRLabel();
            this.txtPmNo = new DevExpress.XtraReports.UI.XRLabel();
            this.failCode = new DevExpress.XtraReports.UI.XRLabel();
            this.FailureCode = new DevExpress.XtraReports.UI.XRLabel();
            this.CityCode = new DevExpress.XtraReports.UI.XRLabel();
            this.ctyCode = new DevExpress.XtraReports.UI.XRLabel();
            this.AreaCode = new DevExpress.XtraReports.UI.XRLabel();
            this.areaCod = new DevExpress.XtraReports.UI.XRLabel();
            this.ProblemDesc = new DevExpress.XtraReports.UI.XRLabel();
            this.problemDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.NotestoAssets = new DevExpress.XtraReports.UI.XRLabel();
            this.notesAssets = new DevExpress.XtraReports.UI.XRLabel();
            this.NotesToLocation = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtPrevTaken = new DevExpress.XtraReports.UI.XRLabel();
            this.txtActionTaken = new DevExpress.XtraReports.UI.XRLabel();
            this.txtCauseDescription = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel106 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel52 = new DevExpress.XtraReports.UI.XRPanel();
            this.TimeCompleted = new DevExpress.XtraReports.UI.XRLabel();
            this.DateComplete = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeCopleted = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel51 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.Time = new DevExpress.XtraReports.UI.XRLabel();
            this.Date = new DevExpress.XtraReports.UI.XRLabel();
            this.DateTimeStarted = new DevExpress.XtraReports.UI.XRLabel();
            this.EmployeeDetailTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.EmployeeName = new DevExpress.XtraReports.UI.XRTableCell();
            this.EmployeeNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.Normal = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.OT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DI = new DevExpress.XtraReports.UI.XRLabel();
            this.ItemDetailTable = new DevExpress.XtraReports.UI.XRTable();
            this.TableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ItemDetailLabl = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel6 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.Panel7 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.SafetyInstructions = new DevExpress.XtraReports.UI.DetailReportBand();
            this.SafetyDetails = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.seqId = new DevExpress.XtraReports.UI.XRTableCell();
            this.objectDataSource1 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.SafetyHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel121 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.objectDataSource2 = new DevExpress.DataAccess.ObjectBinding.ObjectDataSource(this.components);
            this.CheckListReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.CheckListDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.checkListTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.seqNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.taskDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.altTaskDesc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.CheckListHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel129 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.PMChecklistReportItems = new DevExpress.XtraReports.UI.DetailReportBand();
            this.PMChecklistReportItemsDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.PMItemsSeqNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMItemsNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMItemsDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMItemsAltDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMItemsQty = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportItemsHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
            this.PMChecklistReportTools = new DevExpress.XtraReports.UI.DetailReportBand();
            this.PMChecklistReportToolsDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.PMChecklistReportToolsDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportToolsAltDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportToolsSeqNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportToolsHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.PMChecklistReportPPE = new DevExpress.XtraReports.UI.DetailReportBand();
            this.PMChecklistReportPPEDetail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.PMChecklistReportPPESeqNo = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportPPEDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportPPEAltDescription = new DevExpress.XtraReports.UI.XRTableCell();
            this.PMChecklistReportPPEHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel161 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDetailTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkListTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 100F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.BackColor = System.Drawing.Color.Gainsboro;
            this.TopMargin.BorderColor = System.Drawing.Color.LightGray;
            this.TopMargin.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle});
            this.TopMargin.Dpi = 100F;
            this.TopMargin.HeightF = 35F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.StylePriority.UseBackColor = false;
            this.TopMargin.StylePriority.UseBorderColor = false;
            this.TopMargin.StylePriority.UseBorders = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // rptTitle
            // 
            this.rptTitle.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.rptTitle.Dpi = 100F;
            this.rptTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(7.947286E-06F, 0F);
            this.rptTitle.Name = "rptTitle";
            this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.rptTitle.SizeF = new System.Drawing.SizeF(829.9999F, 32.81814F);
            this.rptTitle.StylePriority.UseBorders = false;
            this.rptTitle.StylePriority.UseFont = false;
            this.rptTitle.StylePriority.UseTextAlignment = false;
            this.rptTitle.Text = "Job Order Print Report";
            this.rptTitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 100F;
            this.BottomMargin.HeightF = 5F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.ReportHeader.Dpi = 100F;
            this.ReportHeader.HeightF = 972.7105F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrPanel1
            // 
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel120,
            this.xrLabel105,
            this.xrLabel16,
            this.xrLabel17,
            this.Panel1,
            this.Panel2,
            this.Panel3,
            this.Panel4,
            this.Panel5,
            this.xrLabel15,
            this.Panel6,
            this.Panel7});
            this.xrPanel1.Dpi = 100F;
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(829.0744F, 967.9188F);
            // 
            // xrLabel120
            // 
            this.xrLabel120.Dpi = 100F;
            this.xrLabel120.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(534.026F, 943.1249F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(291.4738F, 18.8335F);
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.Text = "1.سيء  2.مقبول   3.جيد   4. جيد جدا    5.ممتاز";
            // 
            // xrLabel105
            // 
            this.xrLabel105.BorderColor = System.Drawing.Color.LightGray;
            this.xrLabel105.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel105.BorderWidth = 0F;
            this.xrLabel105.Dpi = 100F;
            this.xrLabel105.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(470.2084F, 816.029F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(216.0885F, 25.47083F);
            this.xrLabel105.StylePriority.UseBorderColor = false;
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.StylePriority.UseBorderWidth = false;
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "شهادة إنجاز العمل";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Dpi = 100F;
            this.xrLabel16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(4.00001F, 942.0417F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Please rate Our Services :";
            // 
            // xrLabel17
            // 
            this.xrLabel17.Dpi = 100F;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(221.3383F, 942.0417F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(291.4738F, 18.8335F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "1.Poor 2.Fair 3.Good 4.Very Good 5.Excellent";
            // 
            // Panel1
            // 
            this.Panel1.BorderColor = System.Drawing.Color.LightGray;
            this.Panel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel139,
            this.xrLabel140,
            this.xrLabel55,
            this.xrLabel49,
            this.xrLabel39,
            this.xrLabel130,
            this.xrLabel131,
            this.xrLabel132,
            this.xrLabel133,
            this.xrLabel29,
            this.xrLabel30,
            this.xrLabel28,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.jobOrderNo,
            this.JOPlanNo,
            this.label1,
            this.JobTrade,
            this.label2,
            this.JobStatus,
            this.xrLabel2,
            this.JobType,
            this.xrLabel3,
            this.JobPriority});
            this.Panel1.Dpi = 100F;
            this.Panel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.3749847F);
            this.Panel1.Name = "Panel1";
            this.Panel1.SizeF = new System.Drawing.SizeF(829.0744F, 82.29163F);
            this.Panel1.StylePriority.UseBorderColor = false;
            this.Panel1.StylePriority.UseBorders = false;
            // 
            // xrLabel139
            // 
            this.xrLabel139.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel139.Dpi = 100F;
            this.xrLabel139.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 33.29166F);
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel139.StylePriority.UseBorders = false;
            this.xrLabel139.StylePriority.UseFont = false;
            this.xrLabel139.StylePriority.UseTextAlignment = false;
            this.xrLabel139.Text = ":";
            this.xrLabel139.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel140
            // 
            this.xrLabel140.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel140.Dpi = 100F;
            this.xrLabel140.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 56.29176F);
            this.xrLabel140.Name = "xrLabel140";
            this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel140.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel140.StylePriority.UseBorders = false;
            this.xrLabel140.StylePriority.UseFont = false;
            this.xrLabel140.StylePriority.UseTextAlignment = false;
            this.xrLabel140.Text = ":";
            this.xrLabel140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.Dpi = 100F;
            this.xrLabel55.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 56.52097F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(10.50014F, 20.77075F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = ":";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.Dpi = 100F;
            this.xrLabel49.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 28.62501F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(10.50014F, 27.66662F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = ":";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.Dpi = 100F;
            this.xrLabel39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 5.62501F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(10.50014F, 23F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = ":";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel130
            // 
            this.xrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel130.Dpi = 100F;
            this.xrLabel130.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(710.2829F, 34.29166F);
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(9.564026F, 22.99994F);
            this.xrLabel130.StylePriority.UseBorders = false;
            this.xrLabel130.StylePriority.UseFont = false;
            this.xrLabel130.StylePriority.UseTextAlignment = false;
            this.xrLabel130.Text = ":";
            this.xrLabel130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel131
            // 
            this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel131.Dpi = 100F;
            this.xrLabel131.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 34.29163F);
            this.xrLabel131.Multiline = true;
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(103.9359F, 21.99996F);
            this.xrLabel131.StylePriority.UseBorders = false;
            this.xrLabel131.StylePriority.UseFont = false;
            this.xrLabel131.StylePriority.UseTextAlignment = false;
            this.xrLabel131.Text = "نوع الطلب\r\n";
            this.xrLabel131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel132.Dpi = 100F;
            this.xrLabel132.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 57.29166F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(9.564026F, 23.29159F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.StylePriority.UseFont = false;
            this.xrLabel132.StylePriority.UseTextAlignment = false;
            this.xrLabel132.Text = ":";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel133
            // 
            this.xrLabel133.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel133.Dpi = 100F;
            this.xrLabel133.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 56.29166F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel133.StylePriority.UseBorders = false;
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.StylePriority.UseTextAlignment = false;
            this.xrLabel133.Text = "أولوية الطلب";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.Dpi = 100F;
            this.xrLabel29.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 56.29169F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = ":";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.Dpi = 100F;
            this.xrLabel30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 56.29166F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "حالة الطلب";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.Dpi = 100F;
            this.xrLabel28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 33.2916F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = ":";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Dpi = 100F;
            this.xrLabel13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(662.4947F, 5.62501F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(9.564026F, 27.66663F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = ":";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.Dpi = 100F;
            this.xrLabel12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 33.29163F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "فئة الطلب";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.Dpi = 100F;
            this.xrLabel11.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(672.366F, 5.62501F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(152.0298F, 27.66663F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "رقم أمر العمل";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // jobOrderNo
            // 
            this.jobOrderNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.jobOrderNo.Dpi = 100F;
            this.jobOrderNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.jobOrderNo.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 5.62501F);
            this.jobOrderNo.Name = "jobOrderNo";
            this.jobOrderNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.jobOrderNo.SizeF = new System.Drawing.SizeF(153.4583F, 27.66663F);
            this.jobOrderNo.StylePriority.UseBorders = false;
            this.jobOrderNo.StylePriority.UseFont = false;
            this.jobOrderNo.StylePriority.UseTextAlignment = false;
            this.jobOrderNo.Text = "Job Order No";
            this.jobOrderNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JOPlanNo
            // 
            this.JOPlanNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JOPlanNo.Dpi = 100F;
            this.JOPlanNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JOPlanNo.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 5.62501F);
            this.JOPlanNo.Multiline = true;
            this.JOPlanNo.Name = "JOPlanNo";
            this.JOPlanNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JOPlanNo.SizeF = new System.Drawing.SizeF(145.625F, 27.66663F);
            this.JOPlanNo.StylePriority.UseBorders = false;
            this.JOPlanNo.StylePriority.UseFont = false;
            this.JOPlanNo.StylePriority.UsePadding = false;
            this.JOPlanNo.StylePriority.UseTextAlignment = false;
            this.JOPlanNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label1.Dpi = 100F;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 33.29163F);
            this.label1.Name = "label1";
            this.label1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label1.SizeF = new System.Drawing.SizeF(153.4583F, 23F);
            this.label1.StylePriority.UseBorders = false;
            this.label1.StylePriority.UseFont = false;
            this.label1.StylePriority.UseTextAlignment = false;
            this.label1.Text = "Job Trade";
            this.label1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobTrade
            // 
            this.JobTrade.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobTrade.Dpi = 100F;
            this.JobTrade.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobTrade.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 33.29163F);
            this.JobTrade.Multiline = true;
            this.JobTrade.Name = "JobTrade";
            this.JobTrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobTrade.SizeF = new System.Drawing.SizeF(145.625F, 23F);
            this.JobTrade.StylePriority.UseBorders = false;
            this.JobTrade.StylePriority.UseFont = false;
            this.JobTrade.StylePriority.UsePadding = false;
            this.JobTrade.StylePriority.UseTextAlignment = false;
            this.JobTrade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label2.Dpi = 100F;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 56.29166F);
            this.label2.Name = "label2";
            this.label2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.label2.SizeF = new System.Drawing.SizeF(153.4583F, 23F);
            this.label2.StylePriority.UseBorders = false;
            this.label2.StylePriority.UseFont = false;
            this.label2.StylePriority.UseTextAlignment = false;
            this.label2.Text = "Job Status";
            this.label2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobStatus
            // 
            this.JobStatus.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobStatus.Dpi = 100F;
            this.JobStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobStatus.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 56.29166F);
            this.JobStatus.Multiline = true;
            this.JobStatus.Name = "JobStatus";
            this.JobStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobStatus.SizeF = new System.Drawing.SizeF(145.625F, 23F);
            this.JobStatus.StylePriority.UseBorders = false;
            this.JobStatus.StylePriority.UseFont = false;
            this.JobStatus.StylePriority.UsePadding = false;
            this.JobStatus.StylePriority.UseTextAlignment = false;
            this.JobStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Dpi = 100F;
            this.xrLabel2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(469.729F, 33.29163F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(94.4512F, 23F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Job Type";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobType
            // 
            this.JobType.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobType.Dpi = 100F;
            this.JobType.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobType.LocationFloat = new DevExpress.Utils.PointFloat(575.2202F, 34.29163F);
            this.JobType.Multiline = true;
            this.JobType.Name = "JobType";
            this.JobType.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobType.SizeF = new System.Drawing.SizeF(135.0527F, 21.99996F);
            this.JobType.StylePriority.UseBorders = false;
            this.JobType.StylePriority.UseFont = false;
            this.JobType.StylePriority.UsePadding = false;
            this.JobType.StylePriority.UseTextAlignment = false;
            this.JobType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Dpi = 100F;
            this.xrLabel3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(469.729F, 56.29166F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(94.45117F, 23F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Job Priority";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobPriority
            // 
            this.JobPriority.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.JobPriority.Dpi = 100F;
            this.JobPriority.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JobPriority.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 56.29166F);
            this.JobPriority.Multiline = true;
            this.JobPriority.Name = "JobPriority";
            this.JobPriority.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.JobPriority.SizeF = new System.Drawing.SizeF(134.6758F, 23F);
            this.JobPriority.StylePriority.UseBorders = false;
            this.JobPriority.StylePriority.UseFont = false;
            this.JobPriority.StylePriority.UsePadding = false;
            this.JobPriority.StylePriority.UseTextAlignment = false;
            this.JobPriority.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Panel2
            // 
            this.Panel2.BorderColor = System.Drawing.Color.LightGray;
            this.Panel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel143,
            this.xrLabel141,
            this.xrLabel142,
            this.xrLabel137,
            this.xrLabel57,
            this.xrLabel42,
            this.xrLabel43,
            this.xrLabel44,
            this.xrLabel45,
            this.xrLabel46,
            this.xrLabel47,
            this.xrLabel40,
            this.xrLabel41,
            this.xrLabel33,
            this.xrLabel32,
            this.xrLabel4,
            this.assignedSupplier,
            this.CreatedBy,
            this.creatdBy,
            this.xrLabel5,
            this.rcvDatetime,
            this.xrLabel6,
            this.reqDatetime,
            this.xrLabel7,
            this.requester,
            this.xrLabel8,
            this.phoneNumber,
            this.xrLabel136});
            this.Panel2.Dpi = 100F;
            this.Panel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 86.6666F);
            this.Panel2.Name = "Panel2";
            this.Panel2.SizeF = new System.Drawing.SizeF(829.0744F, 73.56944F);
            this.Panel2.StylePriority.UseBorderColor = false;
            this.Panel2.StylePriority.UseBorders = false;
            // 
            // xrLabel143
            // 
            this.xrLabel143.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel143.Dpi = 100F;
            this.xrLabel143.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel143.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 47.77089F);
            this.xrLabel143.Name = "xrLabel143";
            this.xrLabel143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel143.SizeF = new System.Drawing.SizeF(8.552124F, 23F);
            this.xrLabel143.StylePriority.UseBorders = false;
            this.xrLabel143.StylePriority.UseFont = false;
            this.xrLabel143.StylePriority.UseTextAlignment = false;
            this.xrLabel143.Text = ":";
            this.xrLabel143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel141
            // 
            this.xrLabel141.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel141.Dpi = 100F;
            this.xrLabel141.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 4.000061F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel141.StylePriority.UseBorders = false;
            this.xrLabel141.StylePriority.UseFont = false;
            this.xrLabel141.StylePriority.UseTextAlignment = false;
            this.xrLabel141.Text = ":";
            this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel142
            // 
            this.xrLabel142.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel142.Dpi = 100F;
            this.xrLabel142.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 27.00017F);
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(8.552124F, 14.99983F);
            this.xrLabel142.StylePriority.UseBorders = false;
            this.xrLabel142.StylePriority.UseFont = false;
            this.xrLabel142.StylePriority.UseTextAlignment = false;
            this.xrLabel142.Text = ":";
            this.xrLabel142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel137.Dpi = 100F;
            this.xrLabel137.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 48.99998F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(10.50014F, 20.77075F);
            this.xrLabel137.StylePriority.UseBorders = false;
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.StylePriority.UseTextAlignment = false;
            this.xrLabel137.Text = ":";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.Dpi = 100F;
            this.xrLabel57.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 28.22924F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(10.50014F, 20.77075F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = ":";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.Dpi = 100F;
            this.xrLabel42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 1.562529F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = ":";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.Dpi = 100F;
            this.xrLabel43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 1.562529F);
            this.xrLabel43.Multiline = true;
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "مخصص  لمورد\r\n";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.Dpi = 100F;
            this.xrLabel44.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 26.33324F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(9.539246F, 15.66674F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = ":";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.Dpi = 100F;
            this.xrLabel45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 25.33324F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "مقدم الطلب";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.Dpi = 100F;
            this.xrLabel46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 45.68752F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = ":";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.Dpi = 100F;
            this.xrLabel47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 48.68752F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "رقم الهاتف";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.Dpi = 100F;
            this.xrLabel40.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 49.12497F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = ":";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.Dpi = 100F;
            this.xrLabel41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 49.12503F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "التاريخ/الوقت المطلوب";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.Dpi = 100F;
            this.xrLabel33.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 25.77075F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = ":";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.Dpi = 100F;
            this.xrLabel32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 25.99998F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "تاريخ/وقت الاستلام";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Dpi = 100F;
            this.xrLabel4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(426.5387F, 1.770814F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(137.6415F, 23F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Assigned To Supplier";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // assignedSupplier
            // 
            this.assignedSupplier.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.assignedSupplier.Dpi = 100F;
            this.assignedSupplier.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignedSupplier.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 1.770806F);
            this.assignedSupplier.Multiline = true;
            this.assignedSupplier.Name = "assignedSupplier";
            this.assignedSupplier.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.assignedSupplier.SizeF = new System.Drawing.SizeF(134.6758F, 23F);
            this.assignedSupplier.StylePriority.UseBorders = false;
            this.assignedSupplier.StylePriority.UseFont = false;
            this.assignedSupplier.StylePriority.UsePadding = false;
            this.assignedSupplier.StylePriority.UseTextAlignment = false;
            this.assignedSupplier.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // CreatedBy
            // 
            this.CreatedBy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CreatedBy.Dpi = 100F;
            this.CreatedBy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreatedBy.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 1.770782F);
            this.CreatedBy.Name = "CreatedBy";
            this.CreatedBy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.CreatedBy.SizeF = new System.Drawing.SizeF(153.4583F, 23F);
            this.CreatedBy.StylePriority.UseBorders = false;
            this.CreatedBy.StylePriority.UseFont = false;
            this.CreatedBy.StylePriority.UseTextAlignment = false;
            this.CreatedBy.Text = "Created By";
            this.CreatedBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // creatdBy
            // 
            this.creatdBy.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.creatdBy.Dpi = 100F;
            this.creatdBy.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creatdBy.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 1.770775F);
            this.creatdBy.Multiline = true;
            this.creatdBy.Name = "creatdBy";
            this.creatdBy.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.creatdBy.SizeF = new System.Drawing.SizeF(145.625F, 23F);
            this.creatdBy.StylePriority.UseBorders = false;
            this.creatdBy.StylePriority.UseFont = false;
            this.creatdBy.StylePriority.UsePadding = false;
            this.creatdBy.StylePriority.UseTextAlignment = false;
            this.creatdBy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.Dpi = 100F;
            this.xrLabel5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 24.77077F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(153.4583F, 23.00001F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Received Date/Time";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // rcvDatetime
            // 
            this.rcvDatetime.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.rcvDatetime.Dpi = 100F;
            this.rcvDatetime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rcvDatetime.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 24.77076F);
            this.rcvDatetime.Multiline = true;
            this.rcvDatetime.Name = "rcvDatetime";
            this.rcvDatetime.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.rcvDatetime.SizeF = new System.Drawing.SizeF(145.625F, 23F);
            this.rcvDatetime.StylePriority.UseBorders = false;
            this.rcvDatetime.StylePriority.UseFont = false;
            this.rcvDatetime.StylePriority.UsePadding = false;
            this.rcvDatetime.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:dd-MMM-yy}";
            this.rcvDatetime.Summary = xrSummary1;
            this.rcvDatetime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Dpi = 100F;
            this.xrLabel6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 47.77082F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(153.4583F, 23F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Required Date/Time";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // reqDatetime
            // 
            this.reqDatetime.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.reqDatetime.Dpi = 100F;
            this.reqDatetime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reqDatetime.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 47.77081F);
            this.reqDatetime.Multiline = true;
            this.reqDatetime.Name = "reqDatetime";
            this.reqDatetime.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.reqDatetime.SizeF = new System.Drawing.SizeF(145.625F, 23F);
            this.reqDatetime.StylePriority.UseBorders = false;
            this.reqDatetime.StylePriority.UseFont = false;
            this.reqDatetime.StylePriority.UsePadding = false;
            this.reqDatetime.StylePriority.UseTextAlignment = false;
            this.reqDatetime.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel7.Dpi = 100F;
            this.xrLabel7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(454.0387F, 24.77077F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(110.1415F, 23.00002F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Requester";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // requester
            // 
            this.requester.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.requester.Dpi = 100F;
            this.requester.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.requester.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 24.77076F);
            this.requester.Multiline = true;
            this.requester.Name = "requester";
            this.requester.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.requester.SizeF = new System.Drawing.SizeF(134.6758F, 23F);
            this.requester.StylePriority.UseBorders = false;
            this.requester.StylePriority.UseFont = false;
            this.requester.StylePriority.UsePadding = false;
            this.requester.StylePriority.UseTextAlignment = false;
            this.requester.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Dpi = 100F;
            this.xrLabel8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(454.0387F, 47.77088F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(110.1415F, 23F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Phone Number";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // phoneNumber
            // 
            this.phoneNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.phoneNumber.Dpi = 100F;
            this.phoneNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.phoneNumber.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 47.77084F);
            this.phoneNumber.Multiline = true;
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.phoneNumber.SizeF = new System.Drawing.SizeF(134.6758F, 23F);
            this.phoneNumber.StylePriority.UseBorders = false;
            this.phoneNumber.StylePriority.UseFont = false;
            this.phoneNumber.StylePriority.UsePadding = false;
            this.phoneNumber.StylePriority.UseTextAlignment = false;
            this.phoneNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel136
            // 
            this.xrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel136.Dpi = 100F;
            this.xrLabel136.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 4.00006F);
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(10.50014F, 20.77075F);
            this.xrLabel136.StylePriority.UseBorders = false;
            this.xrLabel136.StylePriority.UseFont = false;
            this.xrLabel136.StylePriority.UseTextAlignment = false;
            this.xrLabel136.Text = ":";
            this.xrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Panel3
            // 
            this.Panel3.BorderColor = System.Drawing.Color.LightGray;
            this.Panel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel155,
            this.xrLabel154,
            this.xrLabel153,
            this.xrLabel152,
            this.xrLabel151,
            this.xrLabel82,
            this.xrLabel38,
            this.xrLabel150,
            this.xrLabel144,
            this.xrLabel145,
            this.xrLabel146,
            this.xrLabel147,
            this.xrLabel148,
            this.xrLabel149,
            this.xrLabel138,
            this.xrLabel135,
            this.xrLabel72,
            this.xrLabel73,
            this.xrLabel74,
            this.xrLabel75,
            this.xrLabel70,
            this.xrLabel71,
            this.xrLabel58,
            this.xrLabel59,
            this.xrLabel60,
            this.xrLabel61,
            this.xrLabel62,
            this.xrLabel63,
            this.xrLabel64,
            this.xrLabel65,
            this.xrLabel66,
            this.xrLabel67,
            this.xrLabel68,
            this.xrLabel69,
            this.xrLabel52,
            this.xrLabel53,
            this.xrLabel54,
            this.altCityCode,
            this.xrLabel56,
            this.altAreaCode,
            this.xrLabel31,
            this.altZoneCode,
            this.xrLabel48,
            this.altBuildingCode,
            this.xrLabel50,
            this.xrLabel51,
            this.locationNumber,
            this.txtLocationNumber,
            this.LocationDescription,
            this.txtlocationDescription,
            this.Asset,
            this.txtAsset,
            this.AssetDescription,
            this.txtAssetDescription,
            this.MaintDivCode,
            this.txtMaintDivCode,
            this.MaintDeptCode,
            this.txtMaintDeptCode,
            this.MaintSubDeptCode,
            this.txtMaintSubDeptCode,
            this.znCode,
            this.ZoneCode,
            this.BuildingCode,
            this.buildCode,
            this.lablPmNo,
            this.txtPmNo,
            this.failCode,
            this.FailureCode,
            this.CityCode,
            this.ctyCode,
            this.AreaCode,
            this.areaCod,
            this.ProblemDesc,
            this.problemDescription,
            this.NotestoAssets,
            this.notesAssets,
            this.NotesToLocation,
            this.xrLabel14});
            this.Panel3.Dpi = 100F;
            this.Panel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 165.625F);
            this.Panel3.Name = "Panel3";
            this.Panel3.SizeF = new System.Drawing.SizeF(829.0744F, 234.8333F);
            this.Panel3.StylePriority.UseBorderColor = false;
            this.Panel3.StylePriority.UseBorders = false;
            // 
            // xrLabel155
            // 
            this.xrLabel155.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel155.Dpi = 100F;
            this.xrLabel155.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel155.LocationFloat = new DevExpress.Utils.PointFloat(157.2084F, 187.0001F);
            this.xrLabel155.Name = "xrLabel155";
            this.xrLabel155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel155.SizeF = new System.Drawing.SizeF(10.50014F, 23.00002F);
            this.xrLabel155.StylePriority.UseBorders = false;
            this.xrLabel155.StylePriority.UseFont = false;
            this.xrLabel155.StylePriority.UseTextAlignment = false;
            this.xrLabel155.Text = ":";
            this.xrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel154
            // 
            this.xrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel154.Dpi = 100F;
            this.xrLabel154.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(157.2084F, 164F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(10.50014F, 22.99995F);
            this.xrLabel154.StylePriority.UseBorders = false;
            this.xrLabel154.StylePriority.UseFont = false;
            this.xrLabel154.StylePriority.UseTextAlignment = false;
            this.xrLabel154.Text = ":";
            this.xrLabel154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel153
            // 
            this.xrLabel153.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel153.Dpi = 100F;
            this.xrLabel153.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(157.2084F, 141F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(10.50014F, 23.00002F);
            this.xrLabel153.StylePriority.UseBorders = false;
            this.xrLabel153.StylePriority.UseFont = false;
            this.xrLabel153.StylePriority.UseTextAlignment = false;
            this.xrLabel153.Text = ":";
            this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel152
            // 
            this.xrLabel152.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel152.Dpi = 100F;
            this.xrLabel152.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel152.LocationFloat = new DevExpress.Utils.PointFloat(157.2084F, 118.0001F);
            this.xrLabel152.Name = "xrLabel152";
            this.xrLabel152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel152.SizeF = new System.Drawing.SizeF(10.50014F, 22.99992F);
            this.xrLabel152.StylePriority.UseBorders = false;
            this.xrLabel152.StylePriority.UseFont = false;
            this.xrLabel152.StylePriority.UseTextAlignment = false;
            this.xrLabel152.Text = ":";
            this.xrLabel152.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel151
            // 
            this.xrLabel151.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel151.Dpi = 100F;
            this.xrLabel151.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel151.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 72.00006F);
            this.xrLabel151.Name = "xrLabel151";
            this.xrLabel151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel151.SizeF = new System.Drawing.SizeF(10.50014F, 22.99997F);
            this.xrLabel151.StylePriority.UseBorders = false;
            this.xrLabel151.StylePriority.UseFont = false;
            this.xrLabel151.StylePriority.UseTextAlignment = false;
            this.xrLabel151.Text = ":";
            this.xrLabel151.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.Dpi = 100F;
            this.xrLabel82.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 48.00002F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(10.50014F, 23.00005F);
            this.xrLabel82.StylePriority.UseBorders = false;
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.StylePriority.UseTextAlignment = false;
            this.xrLabel82.Text = ":";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Dpi = 100F;
            this.xrLabel38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 25.00009F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(10.50014F, 23.00002F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = ":";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel150
            // 
            this.xrLabel150.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel150.Dpi = 100F;
            this.xrLabel150.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 71.99997F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel150.StylePriority.UseBorders = false;
            this.xrLabel150.StylePriority.UseFont = false;
            this.xrLabel150.StylePriority.UseTextAlignment = false;
            this.xrLabel150.Text = ":";
            this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel144
            // 
            this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel144.Dpi = 100F;
            this.xrLabel144.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 116.125F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel144.StylePriority.UseBorders = false;
            this.xrLabel144.StylePriority.UseFont = false;
            this.xrLabel144.StylePriority.UseTextAlignment = false;
            this.xrLabel144.Text = ":";
            this.xrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel145
            // 
            this.xrLabel145.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel145.Dpi = 100F;
            this.xrLabel145.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 94.99997F);
            this.xrLabel145.Name = "xrLabel145";
            this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel145.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel145.StylePriority.UseBorders = false;
            this.xrLabel145.StylePriority.UseFont = false;
            this.xrLabel145.StylePriority.UseTextAlignment = false;
            this.xrLabel145.Text = ":";
            this.xrLabel145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel146.Dpi = 100F;
            this.xrLabel146.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 163.7706F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel146.StylePriority.UseBorders = false;
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.StylePriority.UseTextAlignment = false;
            this.xrLabel146.Text = ":";
            this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel147.Dpi = 100F;
            this.xrLabel147.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 139.9999F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel147.StylePriority.UseBorders = false;
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = ":";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel148
            // 
            this.xrLabel148.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel148.Dpi = 100F;
            this.xrLabel148.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 187.1249F);
            this.xrLabel148.Name = "xrLabel148";
            this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel148.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel148.StylePriority.UseBorders = false;
            this.xrLabel148.StylePriority.UseFont = false;
            this.xrLabel148.StylePriority.UseTextAlignment = false;
            this.xrLabel148.Text = ":";
            this.xrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel149
            // 
            this.xrLabel149.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel149.Dpi = 100F;
            this.xrLabel149.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 210.9583F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(9.539246F, 21.16681F);
            this.xrLabel149.StylePriority.UseBorders = false;
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.StylePriority.UseTextAlignment = false;
            this.xrLabel149.Text = ":";
            this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel138
            // 
            this.xrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel138.Dpi = 100F;
            this.xrLabel138.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(157.2083F, 1.770687F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(10.50014F, 23.00002F);
            this.xrLabel138.StylePriority.UseBorders = false;
            this.xrLabel138.StylePriority.UseFont = false;
            this.xrLabel138.StylePriority.UseTextAlignment = false;
            this.xrLabel138.Text = ":";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel135
            // 
            this.xrLabel135.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel135.Dpi = 100F;
            this.xrLabel135.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(157.2084F, 94.99997F);
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(10.50014F, 23.00006F);
            this.xrLabel135.StylePriority.UseBorders = false;
            this.xrLabel135.StylePriority.UseFont = false;
            this.xrLabel135.StylePriority.UseTextAlignment = false;
            this.xrLabel135.Text = ":";
            this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.Dpi = 100F;
            this.xrLabel72.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 3.999985F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(9.539307F, 23F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.StylePriority.UseTextAlignment = false;
            this.xrLabel72.Text = ":";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Dpi = 100F;
            this.xrLabel73.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 3.000037F);
            this.xrLabel73.Multiline = true;
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(103.9359F, 21.77069F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "وصف المشكلة\r\n";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel74.Dpi = 100F;
            this.xrLabel74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 26.99998F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel74.StylePriority.UseBorders = false;
            this.xrLabel74.StylePriority.UseFont = false;
            this.xrLabel74.StylePriority.UseTextAlignment = false;
            this.xrLabel74.Text = ":";
            this.xrLabel74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.Dpi = 100F;
            this.xrLabel75.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 24.7707F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "ملاحظات للفني";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.Dpi = 100F;
            this.xrLabel70.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 211.8333F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel70.StylePriority.UseBorders = false;
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = ":";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Dpi = 100F;
            this.xrLabel71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 210.8333F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(103.9359F, 22.16682F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "رمز قسم فرعي";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.Dpi = 100F;
            this.xrLabel58.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 72.87497F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = ":";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.Dpi = 100F;
            this.xrLabel59.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 71.875F);
            this.xrLabel59.Multiline = true;
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(103.9359F, 21.77067F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "رقم الموقع";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel60.Dpi = 100F;
            this.xrLabel60.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 95.87497F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel60.StylePriority.UseBorders = false;
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = ":";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.Dpi = 100F;
            this.xrLabel61.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 93.64568F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "وصف الموقع";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.Dpi = 100F;
            this.xrLabel62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 117F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.StylePriority.UseFont = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = ":";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel63.Dpi = 100F;
            this.xrLabel63.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 117F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel63.StylePriority.UseBorders = false;
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "رقم الأصل:";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.Dpi = 100F;
            this.xrLabel64.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 140.8749F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = ":";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Dpi = 100F;
            this.xrLabel65.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 140.8749F);
            this.xrLabel65.Multiline = true;
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "وصف الاصل";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.Dpi = 100F;
            this.xrLabel66.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 164.6456F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = ":";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Dpi = 100F;
            this.xrLabel67.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 164.6456F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(103.9359F, 22.35428F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "رمز إدارة الصيانة";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.Dpi = 100F;
            this.xrLabel68.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(709.8959F, 187.9999F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(9.539276F, 20.77074F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = ":";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Dpi = 100F;
            this.xrLabel69.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(720.4599F, 186.9999F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(103.9359F, 23F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "رمز قسم";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.Dpi = 100F;
            this.xrLabel52.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 72.00005F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = ":";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.Dpi = 100F;
            this.xrLabel53.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 71.00007F);
            this.xrLabel53.Multiline = true;
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(100F, 21.7707F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "رمز الاخفاق";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.Dpi = 100F;
            this.xrLabel54.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 95.00005F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(8.552063F, 20.77074F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = ":";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // altCityCode
            // 
            this.altCityCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.altCityCode.Dpi = 100F;
            this.altCityCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altCityCode.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 93.77076F);
            this.altCityCode.Name = "altCityCode";
            this.altCityCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.altCityCode.SizeF = new System.Drawing.SizeF(100F, 22.99998F);
            this.altCityCode.StylePriority.UseBorders = false;
            this.altCityCode.StylePriority.UseFont = false;
            this.altCityCode.StylePriority.UseTextAlignment = false;
            this.altCityCode.Text = "رمز المدينة";
            this.altCityCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.Dpi = 100F;
            this.xrLabel56.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 116.125F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = ":";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // altAreaCode
            // 
            this.altAreaCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.altAreaCode.Dpi = 100F;
            this.altAreaCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altAreaCode.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 117.125F);
            this.altAreaCode.Name = "altAreaCode";
            this.altAreaCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.altAreaCode.SizeF = new System.Drawing.SizeF(100F, 22.99998F);
            this.altAreaCode.StylePriority.UseBorders = false;
            this.altAreaCode.StylePriority.UseFont = false;
            this.altAreaCode.StylePriority.UseTextAlignment = false;
            this.altAreaCode.Text = "رمز المنطقة";
            this.altAreaCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Dpi = 100F;
            this.xrLabel31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 140F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = ":";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // altZoneCode
            // 
            this.altZoneCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.altZoneCode.Dpi = 100F;
            this.altZoneCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altZoneCode.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 141F);
            this.altZoneCode.Multiline = true;
            this.altZoneCode.Name = "altZoneCode";
            this.altZoneCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.altZoneCode.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.altZoneCode.StylePriority.UseBorders = false;
            this.altZoneCode.StylePriority.UseFont = false;
            this.altZoneCode.StylePriority.UseTextAlignment = false;
            this.altZoneCode.Text = "رمز الشارع";
            this.altZoneCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.Dpi = 100F;
            this.xrLabel48.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 163.7707F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = ":";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // altBuildingCode
            // 
            this.altBuildingCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.altBuildingCode.Dpi = 100F;
            this.altBuildingCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altBuildingCode.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 164.7707F);
            this.altBuildingCode.Name = "altBuildingCode";
            this.altBuildingCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.altBuildingCode.SizeF = new System.Drawing.SizeF(100F, 22.35428F);
            this.altBuildingCode.StylePriority.UseBorders = false;
            this.altBuildingCode.StylePriority.UseFont = false;
            this.altBuildingCode.StylePriority.UseTextAlignment = false;
            this.altBuildingCode.Text = "رمز المبنى";
            this.altBuildingCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.Dpi = 100F;
            this.xrLabel50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(316.4289F, 187.125F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(8.552063F, 23F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = ":";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Dpi = 100F;
            this.xrLabel51.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(325.9167F, 187.125F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "طلب صيانة رقم";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // locationNumber
            // 
            this.locationNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.locationNumber.Dpi = 100F;
            this.locationNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.locationNumber.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 72.00003F);
            this.locationNumber.Name = "locationNumber";
            this.locationNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.locationNumber.SizeF = new System.Drawing.SizeF(129.5331F, 23.00003F);
            this.locationNumber.StylePriority.UseBorders = false;
            this.locationNumber.StylePriority.UseFont = false;
            this.locationNumber.StylePriority.UseTextAlignment = false;
            this.locationNumber.Text = "Location No";
            this.locationNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtLocationNumber
            // 
            this.txtLocationNumber.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtLocationNumber.Dpi = 100F;
            this.txtLocationNumber.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocationNumber.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 72.00006F);
            this.txtLocationNumber.Multiline = true;
            this.txtLocationNumber.Name = "txtLocationNumber";
            this.txtLocationNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtLocationNumber.SizeF = new System.Drawing.SizeF(134.6758F, 22.99997F);
            this.txtLocationNumber.StylePriority.UseBorders = false;
            this.txtLocationNumber.StylePriority.UseFont = false;
            this.txtLocationNumber.StylePriority.UsePadding = false;
            this.txtLocationNumber.StylePriority.UseTextAlignment = false;
            this.txtLocationNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // LocationDescription
            // 
            this.LocationDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.LocationDescription.Dpi = 100F;
            this.LocationDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LocationDescription.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 95.00005F);
            this.LocationDescription.Name = "LocationDescription";
            this.LocationDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.LocationDescription.SizeF = new System.Drawing.SizeF(129.5331F, 22.99997F);
            this.LocationDescription.StylePriority.UseBorders = false;
            this.LocationDescription.StylePriority.UseFont = false;
            this.LocationDescription.StylePriority.UseTextAlignment = false;
            this.LocationDescription.Text = "Location Description";
            this.LocationDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtlocationDescription
            // 
            this.txtlocationDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtlocationDescription.Dpi = 100F;
            this.txtlocationDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlocationDescription.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 95.00005F);
            this.txtlocationDescription.Multiline = true;
            this.txtlocationDescription.Name = "txtlocationDescription";
            this.txtlocationDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtlocationDescription.SizeF = new System.Drawing.SizeF(134.6758F, 23.00003F);
            this.txtlocationDescription.StylePriority.UseBorders = false;
            this.txtlocationDescription.StylePriority.UseFont = false;
            this.txtlocationDescription.StylePriority.UsePadding = false;
            this.txtlocationDescription.StylePriority.UseTextAlignment = false;
            this.txtlocationDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Asset
            // 
            this.Asset.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Asset.Dpi = 100F;
            this.Asset.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Asset.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 118F);
            this.Asset.Name = "Asset";
            this.Asset.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Asset.SizeF = new System.Drawing.SizeF(129.5331F, 23.00002F);
            this.Asset.StylePriority.UseBorders = false;
            this.Asset.StylePriority.UseFont = false;
            this.Asset.StylePriority.UseTextAlignment = false;
            this.Asset.Text = "Asset";
            this.Asset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtAsset
            // 
            this.txtAsset.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtAsset.Dpi = 100F;
            this.txtAsset.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAsset.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 118F);
            this.txtAsset.Multiline = true;
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtAsset.SizeF = new System.Drawing.SizeF(134.6758F, 22.99995F);
            this.txtAsset.StylePriority.UseBorders = false;
            this.txtAsset.StylePriority.UseFont = false;
            this.txtAsset.StylePriority.UsePadding = false;
            this.txtAsset.StylePriority.UseTextAlignment = false;
            this.txtAsset.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AssetDescription
            // 
            this.AssetDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.AssetDescription.Dpi = 100F;
            this.AssetDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 141.0001F);
            this.AssetDescription.Name = "AssetDescription";
            this.AssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.AssetDescription.SizeF = new System.Drawing.SizeF(129.5331F, 22.99998F);
            this.AssetDescription.StylePriority.UseBorders = false;
            this.AssetDescription.StylePriority.UseFont = false;
            this.AssetDescription.StylePriority.UseTextAlignment = false;
            this.AssetDescription.Text = "Asset Description";
            this.AssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtAssetDescription
            // 
            this.txtAssetDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtAssetDescription.Dpi = 100F;
            this.txtAssetDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAssetDescription.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 141.0001F);
            this.txtAssetDescription.Multiline = true;
            this.txtAssetDescription.Name = "txtAssetDescription";
            this.txtAssetDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtAssetDescription.SizeF = new System.Drawing.SizeF(134.6758F, 23.00006F);
            this.txtAssetDescription.StylePriority.UseBorders = false;
            this.txtAssetDescription.StylePriority.UseFont = false;
            this.txtAssetDescription.StylePriority.UsePadding = false;
            this.txtAssetDescription.StylePriority.UseTextAlignment = false;
            this.txtAssetDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintDivCode
            // 
            this.MaintDivCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintDivCode.Dpi = 100F;
            this.MaintDivCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintDivCode.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 164.0002F);
            this.MaintDivCode.Name = "MaintDivCode";
            this.MaintDivCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintDivCode.SizeF = new System.Drawing.SizeF(129.5331F, 22.99992F);
            this.MaintDivCode.StylePriority.UseBorders = false;
            this.MaintDivCode.StylePriority.UseFont = false;
            this.MaintDivCode.StylePriority.UseTextAlignment = false;
            this.MaintDivCode.Text = "Maint Div. Code";
            this.MaintDivCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintDivCode
            // 
            this.txtMaintDivCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintDivCode.Dpi = 100F;
            this.txtMaintDivCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintDivCode.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 164.0002F);
            this.txtMaintDivCode.Multiline = true;
            this.txtMaintDivCode.Name = "txtMaintDivCode";
            this.txtMaintDivCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintDivCode.SizeF = new System.Drawing.SizeF(134.6758F, 22.99995F);
            this.txtMaintDivCode.StylePriority.UseBorders = false;
            this.txtMaintDivCode.StylePriority.UseFont = false;
            this.txtMaintDivCode.StylePriority.UsePadding = false;
            this.txtMaintDivCode.StylePriority.UseTextAlignment = false;
            this.txtMaintDivCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintDeptCode
            // 
            this.MaintDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintDeptCode.Dpi = 100F;
            this.MaintDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 187F);
            this.MaintDeptCode.Name = "MaintDeptCode";
            this.MaintDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintDeptCode.SizeF = new System.Drawing.SizeF(129.5331F, 23.00009F);
            this.MaintDeptCode.StylePriority.UseBorders = false;
            this.MaintDeptCode.StylePriority.UseFont = false;
            this.MaintDeptCode.StylePriority.UseTextAlignment = false;
            this.MaintDeptCode.Text = "Maint Dept. Code";
            this.MaintDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintDeptCode
            // 
            this.txtMaintDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintDeptCode.Dpi = 100F;
            this.txtMaintDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 187F);
            this.txtMaintDeptCode.Multiline = true;
            this.txtMaintDeptCode.Name = "txtMaintDeptCode";
            this.txtMaintDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintDeptCode.SizeF = new System.Drawing.SizeF(134.6758F, 23.00009F);
            this.txtMaintDeptCode.StylePriority.UseBorders = false;
            this.txtMaintDeptCode.StylePriority.UseFont = false;
            this.txtMaintDeptCode.StylePriority.UsePadding = false;
            this.txtMaintDeptCode.StylePriority.UseTextAlignment = false;
            this.txtMaintDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // MaintSubDeptCode
            // 
            this.MaintSubDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.MaintSubDeptCode.Dpi = 100F;
            this.MaintSubDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaintSubDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(434.6471F, 210.0001F);
            this.MaintSubDeptCode.Name = "MaintSubDeptCode";
            this.MaintSubDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaintSubDeptCode.SizeF = new System.Drawing.SizeF(129.5331F, 22.99992F);
            this.MaintSubDeptCode.StylePriority.UseBorders = false;
            this.MaintSubDeptCode.StylePriority.UseFont = false;
            this.MaintSubDeptCode.StylePriority.UseTextAlignment = false;
            this.MaintSubDeptCode.Text = "Maint SubDept. Code";
            this.MaintSubDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtMaintSubDeptCode
            // 
            this.txtMaintSubDeptCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtMaintSubDeptCode.Dpi = 100F;
            this.txtMaintSubDeptCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaintSubDeptCode.LocationFloat = new DevExpress.Utils.PointFloat(575.2201F, 210.0001F);
            this.txtMaintSubDeptCode.Multiline = true;
            this.txtMaintSubDeptCode.Name = "txtMaintSubDeptCode";
            this.txtMaintSubDeptCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtMaintSubDeptCode.SizeF = new System.Drawing.SizeF(134.6758F, 23.00002F);
            this.txtMaintSubDeptCode.StylePriority.UseBorders = false;
            this.txtMaintSubDeptCode.StylePriority.UseFont = false;
            this.txtMaintSubDeptCode.StylePriority.UsePadding = false;
            this.txtMaintSubDeptCode.StylePriority.UseTextAlignment = false;
            this.txtMaintSubDeptCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // znCode
            // 
            this.znCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.znCode.Dpi = 100F;
            this.znCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.znCode.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 141F);
            this.znCode.Multiline = true;
            this.znCode.Name = "znCode";
            this.znCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.znCode.SizeF = new System.Drawing.SizeF(145.625F, 22.99998F);
            this.znCode.StylePriority.UseBorders = false;
            this.znCode.StylePriority.UseFont = false;
            this.znCode.StylePriority.UsePadding = false;
            this.znCode.StylePriority.UseTextAlignment = false;
            this.znCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ZoneCode
            // 
            this.ZoneCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ZoneCode.Dpi = 100F;
            this.ZoneCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoneCode.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 141F);
            this.ZoneCode.Name = "ZoneCode";
            this.ZoneCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ZoneCode.SizeF = new System.Drawing.SizeF(153.4583F, 23.00002F);
            this.ZoneCode.StylePriority.UseBorders = false;
            this.ZoneCode.StylePriority.UseFont = false;
            this.ZoneCode.StylePriority.UseTextAlignment = false;
            this.ZoneCode.Text = "Zone Code";
            this.ZoneCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // BuildingCode
            // 
            this.BuildingCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.BuildingCode.Dpi = 100F;
            this.BuildingCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuildingCode.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 164F);
            this.BuildingCode.Name = "BuildingCode";
            this.BuildingCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.BuildingCode.SizeF = new System.Drawing.SizeF(153.4583F, 22.99995F);
            this.BuildingCode.StylePriority.UseBorders = false;
            this.BuildingCode.StylePriority.UseFont = false;
            this.BuildingCode.StylePriority.UseTextAlignment = false;
            this.BuildingCode.Text = "Building Code";
            this.BuildingCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // buildCode
            // 
            this.buildCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.buildCode.Dpi = 100F;
            this.buildCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buildCode.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 164F);
            this.buildCode.Multiline = true;
            this.buildCode.Name = "buildCode";
            this.buildCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.buildCode.SizeF = new System.Drawing.SizeF(145.625F, 22.99998F);
            this.buildCode.StylePriority.UseBorders = false;
            this.buildCode.StylePriority.UseFont = false;
            this.buildCode.StylePriority.UsePadding = false;
            this.buildCode.StylePriority.UseTextAlignment = false;
            this.buildCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lablPmNo
            // 
            this.lablPmNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lablPmNo.Dpi = 100F;
            this.lablPmNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lablPmNo.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 187.0001F);
            this.lablPmNo.Name = "lablPmNo";
            this.lablPmNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lablPmNo.SizeF = new System.Drawing.SizeF(153.4583F, 23.00002F);
            this.lablPmNo.StylePriority.UseBorders = false;
            this.lablPmNo.StylePriority.UseFont = false;
            this.lablPmNo.StylePriority.UseTextAlignment = false;
            this.lablPmNo.Text = "PM No";
            this.lablPmNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // txtPmNo
            // 
            this.txtPmNo.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtPmNo.Dpi = 100F;
            this.txtPmNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPmNo.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 187.0001F);
            this.txtPmNo.Multiline = true;
            this.txtPmNo.Name = "txtPmNo";
            this.txtPmNo.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.txtPmNo.SizeF = new System.Drawing.SizeF(145.625F, 22.99998F);
            this.txtPmNo.StylePriority.UseBorders = false;
            this.txtPmNo.StylePriority.UseFont = false;
            this.txtPmNo.StylePriority.UsePadding = false;
            this.txtPmNo.StylePriority.UseTextAlignment = false;
            this.txtPmNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // failCode
            // 
            this.failCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.failCode.Dpi = 100F;
            this.failCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.failCode.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 72.00006F);
            this.failCode.Multiline = true;
            this.failCode.Name = "failCode";
            this.failCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.failCode.SizeF = new System.Drawing.SizeF(145.625F, 22.99998F);
            this.failCode.StylePriority.UseBorders = false;
            this.failCode.StylePriority.UseFont = false;
            this.failCode.StylePriority.UsePadding = false;
            this.failCode.StylePriority.UseTextAlignment = false;
            this.failCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // FailureCode
            // 
            this.FailureCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.FailureCode.Dpi = 100F;
            this.FailureCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FailureCode.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 72.00006F);
            this.FailureCode.Name = "FailureCode";
            this.FailureCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.FailureCode.SizeF = new System.Drawing.SizeF(153.4583F, 22.99997F);
            this.FailureCode.StylePriority.UseBorders = false;
            this.FailureCode.StylePriority.UseFont = false;
            this.FailureCode.StylePriority.UseTextAlignment = false;
            this.FailureCode.Text = "Failure Code";
            this.FailureCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // CityCode
            // 
            this.CityCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.CityCode.Dpi = 100F;
            this.CityCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CityCode.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 94.99998F);
            this.CityCode.Multiline = true;
            this.CityCode.Name = "CityCode";
            this.CityCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.CityCode.SizeF = new System.Drawing.SizeF(153.4583F, 23.00006F);
            this.CityCode.StylePriority.UseBorders = false;
            this.CityCode.StylePriority.UseFont = false;
            this.CityCode.StylePriority.UseTextAlignment = false;
            this.CityCode.Text = "City Code ";
            this.CityCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ctyCode
            // 
            this.ctyCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ctyCode.Dpi = 100F;
            this.ctyCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctyCode.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 95F);
            this.ctyCode.Multiline = true;
            this.ctyCode.Name = "ctyCode";
            this.ctyCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.ctyCode.SizeF = new System.Drawing.SizeF(145.625F, 23.00005F);
            this.ctyCode.StylePriority.UseBorders = false;
            this.ctyCode.StylePriority.UseFont = false;
            this.ctyCode.StylePriority.UsePadding = false;
            this.ctyCode.StylePriority.UseTextAlignment = false;
            this.ctyCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // AreaCode
            // 
            this.AreaCode.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.AreaCode.Dpi = 100F;
            this.AreaCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AreaCode.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 118.0001F);
            this.AreaCode.Name = "AreaCode";
            this.AreaCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.AreaCode.SizeF = new System.Drawing.SizeF(153.4583F, 22.99992F);
            this.AreaCode.StylePriority.UseBorders = false;
            this.AreaCode.StylePriority.UseFont = false;
            this.AreaCode.StylePriority.UseTextAlignment = false;
            this.AreaCode.Text = "Area Code";
            this.AreaCode.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // areaCod
            // 
            this.areaCod.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.areaCod.Dpi = 100F;
            this.areaCod.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.areaCod.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 118.0001F);
            this.areaCod.Multiline = true;
            this.areaCod.Name = "areaCod";
            this.areaCod.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.areaCod.SizeF = new System.Drawing.SizeF(145.625F, 22.99998F);
            this.areaCod.StylePriority.UseBorders = false;
            this.areaCod.StylePriority.UseFont = false;
            this.areaCod.StylePriority.UsePadding = false;
            this.areaCod.StylePriority.UseTextAlignment = false;
            this.areaCod.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ProblemDesc
            // 
            this.ProblemDesc.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ProblemDesc.Dpi = 100F;
            this.ProblemDesc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProblemDesc.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 1.770687F);
            this.ProblemDesc.Name = "ProblemDesc";
            this.ProblemDesc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ProblemDesc.SizeF = new System.Drawing.SizeF(153.4583F, 23.00002F);
            this.ProblemDesc.StylePriority.UseBorders = false;
            this.ProblemDesc.StylePriority.UseFont = false;
            this.ProblemDesc.StylePriority.UseTextAlignment = false;
            this.ProblemDesc.Text = "Problem Desc";
            this.ProblemDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // problemDescription
            // 
            this.problemDescription.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.problemDescription.Dpi = 100F;
            this.problemDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.problemDescription.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 1.99995F);
            this.problemDescription.Name = "problemDescription";
            this.problemDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.problemDescription.SizeF = new System.Drawing.SizeF(540.2276F, 23.00003F);
            this.problemDescription.StylePriority.UseBorders = false;
            this.problemDescription.StylePriority.UseFont = false;
            this.problemDescription.StylePriority.UsePadding = false;
            this.problemDescription.StylePriority.UseTextAlignment = false;
            this.problemDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NotestoAssets
            // 
            this.NotestoAssets.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NotestoAssets.Dpi = 100F;
            this.NotestoAssets.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotestoAssets.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 25.0001F);
            this.NotestoAssets.Name = "NotestoAssets";
            this.NotestoAssets.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NotestoAssets.SizeF = new System.Drawing.SizeF(153.4583F, 23.00002F);
            this.NotestoAssets.StylePriority.UseBorders = false;
            this.NotestoAssets.StylePriority.UseFont = false;
            this.NotestoAssets.StylePriority.UseTextAlignment = false;
            this.NotestoAssets.Text = "Notes To Tech.(Asset)";
            this.NotestoAssets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // notesAssets
            // 
            this.notesAssets.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.notesAssets.Dpi = 100F;
            this.notesAssets.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesAssets.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 24.99997F);
            this.notesAssets.Name = "notesAssets";
            this.notesAssets.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.notesAssets.SizeF = new System.Drawing.SizeF(540.2276F, 23F);
            this.notesAssets.StylePriority.UseBorders = false;
            this.notesAssets.StylePriority.UseFont = false;
            this.notesAssets.StylePriority.UsePadding = false;
            this.notesAssets.StylePriority.UseTextAlignment = false;
            this.notesAssets.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // NotesToLocation
            // 
            this.NotesToLocation.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.NotesToLocation.Dpi = 100F;
            this.NotesToLocation.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NotesToLocation.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 48.00002F);
            this.NotesToLocation.Name = "NotesToLocation";
            this.NotesToLocation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.NotesToLocation.SizeF = new System.Drawing.SizeF(153.4583F, 23.00005F);
            this.NotesToLocation.StylePriority.UseBorders = false;
            this.NotesToLocation.StylePriority.UseFont = false;
            this.NotesToLocation.StylePriority.UseTextAlignment = false;
            this.NotesToLocation.Text = "Notes To Tech.(Location)";
            this.NotesToLocation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Dpi = 100F;
            this.xrLabel14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(169.6682F, 48.00002F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(540.2276F, 23.00005F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UsePadding = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Panel4
            // 
            this.Panel4.BorderColor = System.Drawing.Color.LightGray;
            this.Panel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel78,
            this.xrLabel77,
            this.xrLabel76,
            this.txtPrevTaken,
            this.txtActionTaken,
            this.txtCauseDescription,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel1});
            this.Panel4.Dpi = 100F;
            this.Panel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 406.125F);
            this.Panel4.Name = "Panel4";
            this.Panel4.SizeF = new System.Drawing.SizeF(829.0744F, 83.49362F);
            this.Panel4.StylePriority.UseBorderColor = false;
            this.Panel4.StylePriority.UseBorders = false;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.BorderWidth = 0F;
            this.xrLabel78.Dpi = 100F;
            this.xrLabel78.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(690.6787F, 1.999919F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(133.7171F, 16.74986F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseBorderWidth = false;
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "الصيانة الوقائية المتخذة";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel77.BorderWidth = 0F;
            this.xrLabel77.Dpi = 100F;
            this.xrLabel77.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(409.2708F, 1.999919F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(142.9581F, 16.74992F);
            this.xrLabel77.StylePriority.UseBorders = false;
            this.xrLabel77.StylePriority.UseBorderWidth = false;
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.StylePriority.UseTextAlignment = false;
            this.xrLabel77.Text = "الاجراء المتخذ";
            this.xrLabel77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.BorderWidth = 0F;
            this.xrLabel76.Dpi = 100F;
            this.xrLabel76.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(133.1683F, 1.999919F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(137.9581F, 16.74992F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.StylePriority.UseBorderWidth = false;
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "وصف السبب";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txtPrevTaken
            // 
            this.txtPrevTaken.BorderColor = System.Drawing.Color.LightGray;
            this.txtPrevTaken.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtPrevTaken.Dpi = 100F;
            this.txtPrevTaken.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrevTaken.LocationFloat = new DevExpress.Utils.PointFloat(565.1802F, 18.74987F);
            this.txtPrevTaken.Name = "txtPrevTaken";
            this.txtPrevTaken.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtPrevTaken.SizeF = new System.Drawing.SizeF(261.903F, 60.12513F);
            this.txtPrevTaken.StylePriority.UseBorderColor = false;
            this.txtPrevTaken.StylePriority.UseBorders = false;
            this.txtPrevTaken.StylePriority.UseFont = false;
            // 
            // txtActionTaken
            // 
            this.txtActionTaken.BorderColor = System.Drawing.Color.LightGray;
            this.txtActionTaken.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtActionTaken.Dpi = 100F;
            this.txtActionTaken.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtActionTaken.LocationFloat = new DevExpress.Utils.PointFloat(282.7083F, 18.75F);
            this.txtActionTaken.Name = "txtActionTaken";
            this.txtActionTaken.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtActionTaken.SizeF = new System.Drawing.SizeF(269.5208F, 60.12513F);
            this.txtActionTaken.StylePriority.UseBorderColor = false;
            this.txtActionTaken.StylePriority.UseBorders = false;
            this.txtActionTaken.StylePriority.UseFont = false;
            // 
            // txtCauseDescription
            // 
            this.txtCauseDescription.BorderColor = System.Drawing.Color.LightGray;
            this.txtCauseDescription.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtCauseDescription.Dpi = 100F;
            this.txtCauseDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCauseDescription.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 18.74987F);
            this.txtCauseDescription.Name = "txtCauseDescription";
            this.txtCauseDescription.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtCauseDescription.SizeF = new System.Drawing.SizeF(268.8763F, 60.12513F);
            this.txtCauseDescription.StylePriority.UseBorderColor = false;
            this.txtCauseDescription.StylePriority.UseBorders = false;
            this.txtCauseDescription.StylePriority.UseFont = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.BorderWidth = 0F;
            this.xrLabel10.Dpi = 100F;
            this.xrLabel10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 2.00001F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(129.1682F, 16.74992F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Cause Description";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.BorderWidth = 0F;
            this.xrLabel9.Dpi = 100F;
            this.xrLabel9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(282.7083F, 1.999982F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(126.5626F, 16.74992F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Action Taken";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.BorderWidth = 0F;
            this.xrLabel1.Dpi = 100F;
            this.xrLabel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(565.1801F, 1.999919F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(125.4986F, 16.74989F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseBorderWidth = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Prevention Taken";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Panel5
            // 
            this.Panel5.BorderColor = System.Drawing.Color.LightGray;
            this.Panel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel134,
            this.xrLabel109,
            this.xrLabel108,
            this.xrLabel107,
            this.xrLabel106,
            this.Panel52,
            this.Panel51,
            this.EmployeeDetailTable,
            this.DI,
            this.ItemDetailTable,
            this.ItemDetailLabl});
            this.Panel5.Dpi = 100F;
            this.Panel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 489.6186F);
            this.Panel5.Name = "Panel5";
            this.Panel5.SizeF = new System.Drawing.SizeF(829.0744F, 326.4104F);
            this.Panel5.StylePriority.UseBorderColor = false;
            this.Panel5.StylePriority.UseBorders = false;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel134.BorderWidth = 0F;
            this.xrLabel134.Dpi = 100F;
            this.xrLabel134.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(470.2084F, 2.999994F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(124.8965F, 16.74991F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.StylePriority.UseBorderWidth = false;
            this.xrLabel134.StylePriority.UseFont = false;
            this.xrLabel134.StylePriority.UseTextAlignment = false;
            this.xrLabel134.Text = "تفاصيل المواد والعمالة";
            this.xrLabel134.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel109.BorderWidth = 0F;
            this.xrLabel109.Dpi = 100F;
            this.xrLabel109.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(652.4166F, 128.1667F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(60.67645F, 16.74977F);
            this.xrLabel109.StylePriority.UseBorders = false;
            this.xrLabel109.StylePriority.UseBorderWidth = false;
            this.xrLabel109.StylePriority.UseFont = false;
            this.xrLabel109.StylePriority.UseTextAlignment = false;
            this.xrLabel109.Text = "من المخزون";
            this.xrLabel109.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.BorderWidth = 0F;
            this.xrLabel108.Dpi = 100F;
            this.xrLabel108.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(739.1349F, 128.1668F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(59.31604F, 16.74976F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.StylePriority.UseBorderWidth = false;
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "شراء مباشر،";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel107.BorderWidth = 0F;
            this.xrLabel107.Dpi = 100F;
            this.xrLabel107.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(713.0931F, 128.1667F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(25.43152F, 16.74983F);
            this.xrLabel107.StylePriority.UseBorders = false;
            this.xrLabel107.StylePriority.UseBorderWidth = false;
            this.xrLabel107.StylePriority.UseFont = false;
            this.xrLabel107.StylePriority.UseTextAlignment = false;
            this.xrLabel107.Text = "= I ,";
            this.xrLabel107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel106
            // 
            this.xrLabel106.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel106.BorderWidth = 0F;
            this.xrLabel106.Dpi = 100F;
            this.xrLabel106.Font = new System.Drawing.Font("Arial", 8.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel106.LocationFloat = new DevExpress.Utils.PointFloat(798.451F, 128.1667F);
            this.xrLabel106.Name = "xrLabel106";
            this.xrLabel106.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel106.SizeF = new System.Drawing.SizeF(25F, 16.74983F);
            this.xrLabel106.StylePriority.UseBorders = false;
            this.xrLabel106.StylePriority.UseBorderWidth = false;
            this.xrLabel106.StylePriority.UseFont = false;
            this.xrLabel106.StylePriority.UseTextAlignment = false;
            this.xrLabel106.Text = "= D";
            this.xrLabel106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // Panel52
            // 
            this.Panel52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.TimeCompleted,
            this.DateComplete,
            this.DateTimeCopleted,
            this.xrLabel80});
            this.Panel52.Dpi = 100F;
            this.Panel52.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 286.4167F);
            this.Panel52.Name = "Panel52";
            this.Panel52.SizeF = new System.Drawing.SizeF(823.2497F, 31.53534F);
            // 
            // TimeCompleted
            // 
            this.TimeCompleted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TimeCompleted.BorderWidth = 0F;
            this.TimeCompleted.Dpi = 100F;
            this.TimeCompleted.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeCompleted.LocationFloat = new DevExpress.Utils.PointFloat(470.2083F, 4.999974F);
            this.TimeCompleted.Name = "TimeCompleted";
            this.TimeCompleted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TimeCompleted.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.TimeCompleted.StylePriority.UseBorders = false;
            this.TimeCompleted.StylePriority.UseBorderWidth = false;
            this.TimeCompleted.StylePriority.UseFont = false;
            this.TimeCompleted.StylePriority.UseTextAlignment = false;
            this.TimeCompleted.Text = "Time :";
            this.TimeCompleted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateComplete
            // 
            this.DateComplete.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateComplete.BorderWidth = 0F;
            this.DateComplete.Dpi = 100F;
            this.DateComplete.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateComplete.LocationFloat = new DevExpress.Utils.PointFloat(309.5206F, 4.999911F);
            this.DateComplete.Name = "DateComplete";
            this.DateComplete.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateComplete.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.DateComplete.StylePriority.UseBorders = false;
            this.DateComplete.StylePriority.UseBorderWidth = false;
            this.DateComplete.StylePriority.UseFont = false;
            this.DateComplete.StylePriority.UseTextAlignment = false;
            this.DateComplete.Text = "Date :";
            this.DateComplete.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateTimeCopleted
            // 
            this.DateTimeCopleted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateTimeCopleted.BorderWidth = 0F;
            this.DateTimeCopleted.Dpi = 100F;
            this.DateTimeCopleted.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimeCopleted.LocationFloat = new DevExpress.Utils.PointFloat(1.749902F, 4.999911F);
            this.DateTimeCopleted.Name = "DateTimeCopleted";
            this.DateTimeCopleted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateTimeCopleted.SizeF = new System.Drawing.SizeF(217.7083F, 23.00002F);
            this.DateTimeCopleted.StylePriority.UseBorders = false;
            this.DateTimeCopleted.StylePriority.UseBorderWidth = false;
            this.DateTimeCopleted.StylePriority.UseFont = false;
            this.DateTimeCopleted.StylePriority.UseTextAlignment = false;
            this.DateTimeCopleted.Text = "Date and Time work was Completed";
            this.DateTimeCopleted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.Dpi = 100F;
            this.xrLabel80.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(684.4548F, 4.541702F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(136.7461F, 23.00002F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "تاريخ ووقت اكمال العمل";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Panel51
            // 
            this.Panel51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel79,
            this.Time,
            this.Date,
            this.DateTimeStarted});
            this.Panel51.Dpi = 100F;
            this.Panel51.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 251.9584F);
            this.Panel51.Name = "Panel51";
            this.Panel51.SizeF = new System.Drawing.SizeF(824.8329F, 31.33023F);
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.Dpi = 100F;
            this.xrLabel79.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(684.4548F, 4.999974F);
            this.xrLabel79.Multiline = true;
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(136.7461F, 22.99995F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "تاريخ ووقت بداية العمل";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Time
            // 
            this.Time.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Time.BorderWidth = 0F;
            this.Time.Dpi = 100F;
            this.Time.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.LocationFloat = new DevExpress.Utils.PointFloat(470.2083F, 5.00004F);
            this.Time.Name = "Time";
            this.Time.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Time.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.Time.StylePriority.UseBorders = false;
            this.Time.StylePriority.UseBorderWidth = false;
            this.Time.StylePriority.UseFont = false;
            this.Time.StylePriority.UseTextAlignment = false;
            this.Time.Text = "Time :";
            this.Time.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Date
            // 
            this.Date.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Date.BorderWidth = 0F;
            this.Date.Dpi = 100F;
            this.Date.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Date.LocationFloat = new DevExpress.Utils.PointFloat(309.5206F, 4.999911F);
            this.Date.Name = "Date";
            this.Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Date.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.Date.StylePriority.UseBorders = false;
            this.Date.StylePriority.UseBorderWidth = false;
            this.Date.StylePriority.UseFont = false;
            this.Date.StylePriority.UseTextAlignment = false;
            this.Date.Text = "Date :";
            this.Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DateTimeStarted
            // 
            this.DateTimeStarted.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DateTimeStarted.BorderWidth = 0F;
            this.DateTimeStarted.Dpi = 100F;
            this.DateTimeStarted.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DateTimeStarted.LocationFloat = new DevExpress.Utils.PointFloat(1.749902F, 4.999911F);
            this.DateTimeStarted.Name = "DateTimeStarted";
            this.DateTimeStarted.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DateTimeStarted.SizeF = new System.Drawing.SizeF(217.7083F, 23.00002F);
            this.DateTimeStarted.StylePriority.UseBorders = false;
            this.DateTimeStarted.StylePriority.UseBorderWidth = false;
            this.DateTimeStarted.StylePriority.UseFont = false;
            this.DateTimeStarted.StylePriority.UseTextAlignment = false;
            this.DateTimeStarted.Text = "Date and Time work was started";
            this.DateTimeStarted.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // EmployeeDetailTable
            // 
            this.EmployeeDetailTable.Dpi = 100F;
            this.EmployeeDetailTable.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 149.3332F);
            this.EmployeeDetailTable.Name = "EmployeeDetailTable";
            this.EmployeeDetailTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
            this.EmployeeDetailTable.SizeF = new System.Drawing.SizeF(822.1456F, 100F);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.EmployeeName,
            this.EmployeeNo,
            this.Normal,
            this.OT1,
            this.OT2,
            this.OT3});
            this.xrTableRow5.Dpi = 100F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // EmployeeName
            // 
            this.EmployeeName.BackColor = System.Drawing.Color.LightGray;
            this.EmployeeName.BorderColor = System.Drawing.Color.LightGray;
            this.EmployeeName.Dpi = 100F;
            this.EmployeeName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.EmployeeName.Name = "EmployeeName";
            this.EmployeeName.StylePriority.UseBackColor = false;
            this.EmployeeName.StylePriority.UseBorderColor = false;
            this.EmployeeName.StylePriority.UseFont = false;
            this.EmployeeName.StylePriority.UseTextAlignment = false;
            this.EmployeeName.Text = "Employee Name";
            this.EmployeeName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.EmployeeName.Weight = 1.8020832824707032D;
            // 
            // EmployeeNo
            // 
            this.EmployeeNo.BackColor = System.Drawing.Color.LightGray;
            this.EmployeeNo.Dpi = 100F;
            this.EmployeeNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.EmployeeNo.Name = "EmployeeNo";
            this.EmployeeNo.StylePriority.UseBackColor = false;
            this.EmployeeNo.StylePriority.UseFont = false;
            this.EmployeeNo.StylePriority.UseTextAlignment = false;
            this.EmployeeNo.Text = "Employee No";
            this.EmployeeNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.EmployeeNo.Weight = 1.8021000671386716D;
            // 
            // Normal
            // 
            this.Normal.BackColor = System.Drawing.Color.LightGray;
            this.Normal.Dpi = 100F;
            this.Normal.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.Normal.Name = "Normal";
            this.Normal.StylePriority.UseBackColor = false;
            this.Normal.StylePriority.UseFont = false;
            this.Normal.StylePriority.UseTextAlignment = false;
            this.Normal.Text = "Normal";
            this.Normal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.Normal.Weight = 0.99790008544921882D;
            // 
            // OT1
            // 
            this.OT1.BackColor = System.Drawing.Color.LightGray;
            this.OT1.Dpi = 100F;
            this.OT1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.OT1.Name = "OT1";
            this.OT1.StylePriority.UseBackColor = false;
            this.OT1.StylePriority.UseFont = false;
            this.OT1.StylePriority.UseTextAlignment = false;
            this.OT1.Text = "OT1";
            this.OT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT1.Weight = 1.0520834096272784D;
            // 
            // OT2
            // 
            this.OT2.BackColor = System.Drawing.Color.LightGray;
            this.OT2.Dpi = 100F;
            this.OT2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.OT2.Name = "OT2";
            this.OT2.StylePriority.UseBackColor = false;
            this.OT2.StylePriority.UseFont = false;
            this.OT2.StylePriority.UseTextAlignment = false;
            this.OT2.Text = "OT2";
            this.OT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT2.Weight = 1.1910790952996777D;
            // 
            // OT3
            // 
            this.OT3.BackColor = System.Drawing.Color.LightGray;
            this.OT3.Dpi = 100F;
            this.OT3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.OT3.Multiline = true;
            this.OT3.Name = "OT3";
            this.OT3.StylePriority.UseBackColor = false;
            this.OT3.StylePriority.UseFont = false;
            this.OT3.StylePriority.UseTextAlignment = false;
            this.OT3.Text = "OT3\r\n";
            this.OT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.OT3.Weight = 1.2547522289597628D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell31,
            this.TableCell32,
            this.TableCell33,
            this.TableCell34,
            this.TableCell35,
            this.TableCell36});
            this.xrTableRow6.Dpi = 100F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // TableCell31
            // 
            this.TableCell31.Dpi = 100F;
            this.TableCell31.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell31.Name = "TableCell31";
            this.TableCell31.StylePriority.UseFont = false;
            this.TableCell31.StylePriority.UseTextAlignment = false;
            this.TableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell31.Weight = 1.8020832824707032D;
            // 
            // TableCell32
            // 
            this.TableCell32.Dpi = 100F;
            this.TableCell32.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell32.Name = "TableCell32";
            this.TableCell32.StylePriority.UseFont = false;
            this.TableCell32.StylePriority.UseTextAlignment = false;
            this.TableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell32.Weight = 1.8021000671386716D;
            // 
            // TableCell33
            // 
            this.TableCell33.Dpi = 100F;
            this.TableCell33.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell33.Name = "TableCell33";
            this.TableCell33.StylePriority.UseFont = false;
            this.TableCell33.StylePriority.UseTextAlignment = false;
            this.TableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell33.Weight = 0.99790008544921882D;
            // 
            // TableCell34
            // 
            this.TableCell34.Dpi = 100F;
            this.TableCell34.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell34.Name = "TableCell34";
            this.TableCell34.StylePriority.UseFont = false;
            this.TableCell34.StylePriority.UseTextAlignment = false;
            this.TableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell34.Weight = 1.0520834096272784D;
            // 
            // TableCell35
            // 
            this.TableCell35.Dpi = 100F;
            this.TableCell35.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell35.Name = "TableCell35";
            this.TableCell35.StylePriority.UseFont = false;
            this.TableCell35.StylePriority.UseTextAlignment = false;
            this.TableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell35.Weight = 1.1910790952996777D;
            // 
            // TableCell36
            // 
            this.TableCell36.Dpi = 100F;
            this.TableCell36.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell36.Name = "TableCell36";
            this.TableCell36.StylePriority.UseFont = false;
            this.TableCell36.StylePriority.UseTextAlignment = false;
            this.TableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell36.Weight = 1.2547522289597628D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell37,
            this.TableCell38,
            this.TableCell39,
            this.TableCell40,
            this.TableCell41,
            this.TableCell42});
            this.xrTableRow7.Dpi = 100F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // TableCell37
            // 
            this.TableCell37.Dpi = 100F;
            this.TableCell37.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell37.Name = "TableCell37";
            this.TableCell37.StylePriority.UseFont = false;
            this.TableCell37.StylePriority.UseTextAlignment = false;
            this.TableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell37.Weight = 1.8020832824707032D;
            // 
            // TableCell38
            // 
            this.TableCell38.Dpi = 100F;
            this.TableCell38.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell38.Name = "TableCell38";
            this.TableCell38.StylePriority.UseFont = false;
            this.TableCell38.StylePriority.UseTextAlignment = false;
            this.TableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell38.Weight = 1.8021000671386716D;
            // 
            // TableCell39
            // 
            this.TableCell39.Dpi = 100F;
            this.TableCell39.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell39.Name = "TableCell39";
            this.TableCell39.StylePriority.UseFont = false;
            this.TableCell39.StylePriority.UseTextAlignment = false;
            this.TableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell39.Weight = 0.99790008544921882D;
            // 
            // TableCell40
            // 
            this.TableCell40.Dpi = 100F;
            this.TableCell40.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell40.Name = "TableCell40";
            this.TableCell40.StylePriority.UseFont = false;
            this.TableCell40.StylePriority.UseTextAlignment = false;
            this.TableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell40.Weight = 1.0520834096272784D;
            // 
            // TableCell41
            // 
            this.TableCell41.Dpi = 100F;
            this.TableCell41.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell41.Name = "TableCell41";
            this.TableCell41.StylePriority.UseFont = false;
            this.TableCell41.StylePriority.UseTextAlignment = false;
            this.TableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell41.Weight = 1.1910790952996777D;
            // 
            // TableCell42
            // 
            this.TableCell42.Dpi = 100F;
            this.TableCell42.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell42.Name = "TableCell42";
            this.TableCell42.StylePriority.UseFont = false;
            this.TableCell42.StylePriority.UseTextAlignment = false;
            this.TableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell42.Weight = 1.2547522289597628D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell43,
            this.TableCell44,
            this.TableCell45,
            this.TableCell46,
            this.TableCell47,
            this.TableCell48});
            this.xrTableRow8.Dpi = 100F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // TableCell43
            // 
            this.TableCell43.Dpi = 100F;
            this.TableCell43.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell43.Name = "TableCell43";
            this.TableCell43.StylePriority.UseFont = false;
            this.TableCell43.StylePriority.UseTextAlignment = false;
            this.TableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell43.Weight = 1.8020832824707032D;
            // 
            // TableCell44
            // 
            this.TableCell44.Dpi = 100F;
            this.TableCell44.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell44.Name = "TableCell44";
            this.TableCell44.StylePriority.UseFont = false;
            this.TableCell44.StylePriority.UseTextAlignment = false;
            this.TableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell44.Weight = 1.8021000671386716D;
            // 
            // TableCell45
            // 
            this.TableCell45.Dpi = 100F;
            this.TableCell45.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell45.Name = "TableCell45";
            this.TableCell45.StylePriority.UseFont = false;
            this.TableCell45.StylePriority.UseTextAlignment = false;
            this.TableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell45.Weight = 0.99790008544921882D;
            // 
            // TableCell46
            // 
            this.TableCell46.Dpi = 100F;
            this.TableCell46.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell46.Name = "TableCell46";
            this.TableCell46.StylePriority.UseFont = false;
            this.TableCell46.StylePriority.UseTextAlignment = false;
            this.TableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell46.Weight = 1.0520834096272784D;
            // 
            // TableCell47
            // 
            this.TableCell47.Dpi = 100F;
            this.TableCell47.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell47.Name = "TableCell47";
            this.TableCell47.StylePriority.UseFont = false;
            this.TableCell47.StylePriority.UseTextAlignment = false;
            this.TableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell47.Weight = 1.1910790952996777D;
            // 
            // TableCell48
            // 
            this.TableCell48.Dpi = 100F;
            this.TableCell48.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell48.Name = "TableCell48";
            this.TableCell48.StylePriority.UseFont = false;
            this.TableCell48.StylePriority.UseTextAlignment = false;
            this.TableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell48.Weight = 1.2547522289597628D;
            // 
            // DI
            // 
            this.DI.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DI.BorderWidth = 0F;
            this.DI.Dpi = 100F;
            this.DI.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DI.LocationFloat = new DevExpress.Utils.PointFloat(4.00001F, 128.1667F);
            this.DI.Name = "DI";
            this.DI.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DI.SizeF = new System.Drawing.SizeF(244.7917F, 16.74983F);
            this.DI.StylePriority.UseBorders = false;
            this.DI.StylePriority.UseBorderWidth = false;
            this.DI.StylePriority.UseFont = false;
            this.DI.StylePriority.UseTextAlignment = false;
            this.DI.Text = "** D = Direct Issue, I = Inventory";
            this.DI.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ItemDetailTable
            // 
            this.ItemDetailTable.Dpi = 100F;
            this.ItemDetailTable.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 22.74996F);
            this.ItemDetailTable.Name = "ItemDetailTable";
            this.ItemDetailTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.TableRow1,
            this.TableRow2,
            this.TableRow3,
            this.TableRow4});
            this.ItemDetailTable.SizeF = new System.Drawing.SizeF(823.2497F, 100.0001F);
            // 
            // TableRow1
            // 
            this.TableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell1,
            this.TableCell2,
            this.TableCell3,
            this.TableCell4,
            this.TableCell5,
            this.TableCell6});
            this.TableRow1.Dpi = 100F;
            this.TableRow1.Name = "TableRow1";
            this.TableRow1.Weight = 1D;
            // 
            // TableCell1
            // 
            this.TableCell1.BackColor = System.Drawing.Color.LightGray;
            this.TableCell1.BorderColor = System.Drawing.Color.LightGray;
            this.TableCell1.Dpi = 100F;
            this.TableCell1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell1.Name = "TableCell1";
            this.TableCell1.StylePriority.UseBackColor = false;
            this.TableCell1.StylePriority.UseBorderColor = false;
            this.TableCell1.StylePriority.UseFont = false;
            this.TableCell1.StylePriority.UseTextAlignment = false;
            this.TableCell1.Text = "Item No";
            this.TableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell1.Weight = 1.8020832824707032D;
            // 
            // TableCell2
            // 
            this.TableCell2.BackColor = System.Drawing.Color.LightGray;
            this.TableCell2.Dpi = 100F;
            this.TableCell2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell2.Name = "TableCell2";
            this.TableCell2.StylePriority.UseBackColor = false;
            this.TableCell2.StylePriority.UseFont = false;
            this.TableCell2.StylePriority.UseTextAlignment = false;
            this.TableCell2.Text = "item Description";
            this.TableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell2.Weight = 1.8021000671386716D;
            // 
            // TableCell3
            // 
            this.TableCell3.BackColor = System.Drawing.Color.LightGray;
            this.TableCell3.Dpi = 100F;
            this.TableCell3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell3.Name = "TableCell3";
            this.TableCell3.StylePriority.UseBackColor = false;
            this.TableCell3.StylePriority.UseFont = false;
            this.TableCell3.StylePriority.UseTextAlignment = false;
            this.TableCell3.Text = "Issue Type(D/I)";
            this.TableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell3.Weight = 0.99790008544921882D;
            // 
            // TableCell4
            // 
            this.TableCell4.BackColor = System.Drawing.Color.LightGray;
            this.TableCell4.Dpi = 100F;
            this.TableCell4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell4.Name = "TableCell4";
            this.TableCell4.StylePriority.UseBackColor = false;
            this.TableCell4.StylePriority.UseFont = false;
            this.TableCell4.StylePriority.UseTextAlignment = false;
            this.TableCell4.Text = "UOM";
            this.TableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell4.Weight = 1.0520834096272784D;
            // 
            // TableCell5
            // 
            this.TableCell5.BackColor = System.Drawing.Color.LightGray;
            this.TableCell5.Dpi = 100F;
            this.TableCell5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell5.Name = "TableCell5";
            this.TableCell5.StylePriority.UseBackColor = false;
            this.TableCell5.StylePriority.UseFont = false;
            this.TableCell5.StylePriority.UseTextAlignment = false;
            this.TableCell5.Text = "Qty Used";
            this.TableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell5.Weight = 1.1910790952996777D;
            // 
            // TableCell6
            // 
            this.TableCell6.BackColor = System.Drawing.Color.LightGray;
            this.TableCell6.Dpi = 100F;
            this.TableCell6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TableCell6.Name = "TableCell6";
            this.TableCell6.StylePriority.UseBackColor = false;
            this.TableCell6.StylePriority.UseFont = false;
            this.TableCell6.StylePriority.UseTextAlignment = false;
            this.TableCell6.Text = "Qty Returned";
            this.TableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell6.Weight = 1.2547522289597628D;
            // 
            // TableRow2
            // 
            this.TableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell7,
            this.TableCell8,
            this.TableCell9,
            this.TableCell10,
            this.TableCell11,
            this.TableCell12});
            this.TableRow2.Dpi = 100F;
            this.TableRow2.Name = "TableRow2";
            this.TableRow2.Weight = 1D;
            // 
            // TableCell7
            // 
            this.TableCell7.Dpi = 100F;
            this.TableCell7.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell7.Name = "TableCell7";
            this.TableCell7.StylePriority.UseFont = false;
            this.TableCell7.StylePriority.UseTextAlignment = false;
            this.TableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell7.Weight = 1.8020832824707032D;
            // 
            // TableCell8
            // 
            this.TableCell8.Dpi = 100F;
            this.TableCell8.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell8.Name = "TableCell8";
            this.TableCell8.StylePriority.UseFont = false;
            this.TableCell8.StylePriority.UseTextAlignment = false;
            this.TableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell8.Weight = 1.8021000671386716D;
            // 
            // TableCell9
            // 
            this.TableCell9.Dpi = 100F;
            this.TableCell9.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell9.Name = "TableCell9";
            this.TableCell9.StylePriority.UseFont = false;
            this.TableCell9.StylePriority.UseTextAlignment = false;
            this.TableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell9.Weight = 0.99790008544921882D;
            // 
            // TableCell10
            // 
            this.TableCell10.Dpi = 100F;
            this.TableCell10.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell10.Name = "TableCell10";
            this.TableCell10.StylePriority.UseFont = false;
            this.TableCell10.StylePriority.UseTextAlignment = false;
            this.TableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell10.Weight = 1.0520834096272784D;
            // 
            // TableCell11
            // 
            this.TableCell11.Dpi = 100F;
            this.TableCell11.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell11.Name = "TableCell11";
            this.TableCell11.StylePriority.UseFont = false;
            this.TableCell11.StylePriority.UseTextAlignment = false;
            this.TableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell11.Weight = 1.1910790952996777D;
            // 
            // TableCell12
            // 
            this.TableCell12.Dpi = 100F;
            this.TableCell12.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell12.Name = "TableCell12";
            this.TableCell12.StylePriority.UseFont = false;
            this.TableCell12.StylePriority.UseTextAlignment = false;
            this.TableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell12.Weight = 1.2547522289597628D;
            // 
            // TableRow3
            // 
            this.TableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell13,
            this.TableCell14,
            this.TableCell15,
            this.TableCell16,
            this.TableCell17,
            this.TableCell18});
            this.TableRow3.Dpi = 100F;
            this.TableRow3.Name = "TableRow3";
            this.TableRow3.Weight = 1D;
            // 
            // TableCell13
            // 
            this.TableCell13.Dpi = 100F;
            this.TableCell13.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell13.Name = "TableCell13";
            this.TableCell13.StylePriority.UseFont = false;
            this.TableCell13.StylePriority.UseTextAlignment = false;
            this.TableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell13.Weight = 1.8020832824707032D;
            // 
            // TableCell14
            // 
            this.TableCell14.Dpi = 100F;
            this.TableCell14.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell14.Name = "TableCell14";
            this.TableCell14.StylePriority.UseFont = false;
            this.TableCell14.StylePriority.UseTextAlignment = false;
            this.TableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell14.Weight = 1.8021000671386716D;
            // 
            // TableCell15
            // 
            this.TableCell15.Dpi = 100F;
            this.TableCell15.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell15.Name = "TableCell15";
            this.TableCell15.StylePriority.UseFont = false;
            this.TableCell15.StylePriority.UseTextAlignment = false;
            this.TableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell15.Weight = 0.99790008544921882D;
            // 
            // TableCell16
            // 
            this.TableCell16.Dpi = 100F;
            this.TableCell16.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell16.Name = "TableCell16";
            this.TableCell16.StylePriority.UseFont = false;
            this.TableCell16.StylePriority.UseTextAlignment = false;
            this.TableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell16.Weight = 1.0520834096272784D;
            // 
            // TableCell17
            // 
            this.TableCell17.Dpi = 100F;
            this.TableCell17.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell17.Name = "TableCell17";
            this.TableCell17.StylePriority.UseFont = false;
            this.TableCell17.StylePriority.UseTextAlignment = false;
            this.TableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell17.Weight = 1.1910790952996777D;
            // 
            // TableCell18
            // 
            this.TableCell18.Dpi = 100F;
            this.TableCell18.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell18.Name = "TableCell18";
            this.TableCell18.StylePriority.UseFont = false;
            this.TableCell18.StylePriority.UseTextAlignment = false;
            this.TableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell18.Weight = 1.2547522289597628D;
            // 
            // TableRow4
            // 
            this.TableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TableCell19,
            this.TableCell20,
            this.TableCell21,
            this.TableCell22,
            this.TableCell23,
            this.TableCell24});
            this.TableRow4.Dpi = 100F;
            this.TableRow4.Name = "TableRow4";
            this.TableRow4.Weight = 1D;
            // 
            // TableCell19
            // 
            this.TableCell19.Dpi = 100F;
            this.TableCell19.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell19.Name = "TableCell19";
            this.TableCell19.StylePriority.UseFont = false;
            this.TableCell19.StylePriority.UseTextAlignment = false;
            this.TableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell19.Weight = 1.8020832824707032D;
            // 
            // TableCell20
            // 
            this.TableCell20.Dpi = 100F;
            this.TableCell20.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell20.Name = "TableCell20";
            this.TableCell20.StylePriority.UseFont = false;
            this.TableCell20.StylePriority.UseTextAlignment = false;
            this.TableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell20.Weight = 1.8021000671386716D;
            // 
            // TableCell21
            // 
            this.TableCell21.Dpi = 100F;
            this.TableCell21.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell21.Name = "TableCell21";
            this.TableCell21.StylePriority.UseFont = false;
            this.TableCell21.StylePriority.UseTextAlignment = false;
            this.TableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell21.Weight = 0.99790008544921882D;
            // 
            // TableCell22
            // 
            this.TableCell22.Dpi = 100F;
            this.TableCell22.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell22.Name = "TableCell22";
            this.TableCell22.StylePriority.UseFont = false;
            this.TableCell22.StylePriority.UseTextAlignment = false;
            this.TableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell22.Weight = 1.0520834096272784D;
            // 
            // TableCell23
            // 
            this.TableCell23.Dpi = 100F;
            this.TableCell23.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell23.Name = "TableCell23";
            this.TableCell23.StylePriority.UseFont = false;
            this.TableCell23.StylePriority.UseTextAlignment = false;
            this.TableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell23.Weight = 1.1910790952996777D;
            // 
            // TableCell24
            // 
            this.TableCell24.Dpi = 100F;
            this.TableCell24.Font = new System.Drawing.Font("Arial", 8.6F);
            this.TableCell24.Name = "TableCell24";
            this.TableCell24.StylePriority.UseFont = false;
            this.TableCell24.StylePriority.UseTextAlignment = false;
            this.TableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.TableCell24.Weight = 1.2547522289597628D;
            // 
            // ItemDetailLabl
            // 
            this.ItemDetailLabl.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.ItemDetailLabl.BorderWidth = 0F;
            this.ItemDetailLabl.Dpi = 100F;
            this.ItemDetailLabl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemDetailLabl.LocationFloat = new DevExpress.Utils.PointFloat(282.7083F, 2.999994F);
            this.ItemDetailLabl.Name = "ItemDetailLabl";
            this.ItemDetailLabl.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ItemDetailLabl.SizeF = new System.Drawing.SizeF(187.5001F, 16.74991F);
            this.ItemDetailLabl.StylePriority.UseBorders = false;
            this.ItemDetailLabl.StylePriority.UseBorderWidth = false;
            this.ItemDetailLabl.StylePriority.UseFont = false;
            this.ItemDetailLabl.StylePriority.UseTextAlignment = false;
            this.ItemDetailLabl.Text = "Items And Labour Details";
            this.ItemDetailLabl.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel15
            // 
            this.xrLabel15.BorderColor = System.Drawing.Color.LightGray;
            this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel15.BorderWidth = 0F;
            this.xrLabel15.Dpi = 100F;
            this.xrLabel15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(282.7083F, 816.029F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(187.5001F, 25.47083F);
            this.xrLabel15.StylePriority.UseBorderColor = false;
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseBorderWidth = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Certificate work complition";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Panel6
            // 
            this.Panel6.BorderColor = System.Drawing.Color.LightGray;
            this.Panel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel114,
            this.xrLabel115,
            this.xrLabel112,
            this.xrLabel113,
            this.xrLabel110,
            this.xrLabel35,
            this.xrLabel34,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel21,
            this.xrLabel24,
            this.xrLabel25});
            this.Panel6.Dpi = 100F;
            this.Panel6.LocationFloat = new DevExpress.Utils.PointFloat(4.00001F, 841.5F);
            this.Panel6.Name = "Panel6";
            this.Panel6.SizeF = new System.Drawing.SizeF(397.9182F, 95.54175F);
            this.Panel6.StylePriority.UseBorderColor = false;
            this.Panel6.StylePriority.UseBorders = false;
            // 
            // xrLabel114
            // 
            this.xrLabel114.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel114.BorderWidth = 0F;
            this.xrLabel114.Dpi = 100F;
            this.xrLabel114.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(338.6266F, 46.27086F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel114.StylePriority.UseBorders = false;
            this.xrLabel114.StylePriority.UseBorderWidth = false;
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "التوقيع";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel115.BorderWidth = 0F;
            this.xrLabel115.Dpi = 100F;
            this.xrLabel115.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(338.6266F, 70.27087F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel115.StylePriority.UseBorders = false;
            this.xrLabel115.StylePriority.UseBorderWidth = false;
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "الوقت";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel112.BorderWidth = 0F;
            this.xrLabel112.Dpi = 100F;
            this.xrLabel112.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(160.2917F, 45.58332F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.StylePriority.UseBorderWidth = false;
            this.xrLabel112.StylePriority.UseFont = false;
            this.xrLabel112.StylePriority.UseTextAlignment = false;
            this.xrLabel112.Text = "الاسم";
            this.xrLabel112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel113.BorderWidth = 0F;
            this.xrLabel113.Dpi = 100F;
            this.xrLabel113.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(160.2918F, 69.58333F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel113.StylePriority.UseBorders = false;
            this.xrLabel113.StylePriority.UseBorderWidth = false;
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = "التاريخ";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel110
            // 
            this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel110.BorderWidth = 0F;
            this.xrLabel110.Dpi = 100F;
            this.xrLabel110.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(265.6265F, 4.999987F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(130.2917F, 19.58326F);
            this.xrLabel110.StylePriority.UseBorders = false;
            this.xrLabel110.StylePriority.UseBorderWidth = false;
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.Text = "توقيع الفني/المهندس";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Dpi = 100F;
            this.xrLabel35.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(233.9586F, 24.58324F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.Text = "............................";
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Dpi = 100F;
            this.xrLabel34.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 24.58324F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.Text = "............................";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Dpi = 100F;
            this.xrLabel18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10.00007F, 5.749878F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(255.6264F, 18.83337F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "Name And Signature of Technician/Engineer";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.BorderWidth = 0F;
            this.xrLabel19.Dpi = 100F;
            this.xrLabel19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00006F, 43.58325F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseBorderWidth = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Name";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.BorderWidth = 0F;
            this.xrLabel21.Dpi = 100F;
            this.xrLabel21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.00007F, 67.58325F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseBorderWidth = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Date :";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.BorderWidth = 0F;
            this.xrLabel24.Dpi = 100F;
            this.xrLabel24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(232.7083F, 43.58331F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(60.41666F, 22.99994F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseBorderWidth = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Signature";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.BorderWidth = 0F;
            this.xrLabel25.Dpi = 100F;
            this.xrLabel25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(232.7083F, 67.58331F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(60.41666F, 23.00006F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseBorderWidth = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Time :";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Panel7
            // 
            this.Panel7.BorderColor = System.Drawing.Color.LightGray;
            this.Panel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Panel7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel118,
            this.xrLabel119,
            this.xrLabel116,
            this.xrLabel117,
            this.xrLabel37,
            this.xrLabel20,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel27,
            this.xrLabel26,
            this.xrLabel36,
            this.xrLabel111});
            this.Panel7.Dpi = 100F;
            this.Panel7.LocationFloat = new DevExpress.Utils.PointFloat(410.4186F, 841.4999F);
            this.Panel7.Name = "Panel7";
            this.Panel7.SizeF = new System.Drawing.SizeF(415.0812F, 95.54181F);
            this.Panel7.StylePriority.UseBorderColor = false;
            this.Panel7.StylePriority.UseBorders = false;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel118.BorderWidth = 0F;
            this.xrLabel118.Dpi = 100F;
            this.xrLabel118.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(355.7408F, 46.27094F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel118.StylePriority.UseBorders = false;
            this.xrLabel118.StylePriority.UseBorderWidth = false;
            this.xrLabel118.StylePriority.UseFont = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = "التوقيع";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel119.BorderWidth = 0F;
            this.xrLabel119.Dpi = 100F;
            this.xrLabel119.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(355.7408F, 69.83337F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel119.StylePriority.UseBorders = false;
            this.xrLabel119.StylePriority.UseBorderWidth = false;
            this.xrLabel119.StylePriority.UseFont = false;
            this.xrLabel119.StylePriority.UseTextAlignment = false;
            this.xrLabel119.Text = "الوقت";
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel116
            // 
            this.xrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel116.BorderWidth = 0F;
            this.xrLabel116.Dpi = 100F;
            this.xrLabel116.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(162.895F, 45.66669F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel116.StylePriority.UseBorders = false;
            this.xrLabel116.StylePriority.UseBorderWidth = false;
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "الاسم";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.BorderWidth = 0F;
            this.xrLabel117.Dpi = 100F;
            this.xrLabel117.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(162.895F, 68.66669F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.StylePriority.UseBorderWidth = false;
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.StylePriority.UseTextAlignment = false;
            this.xrLabel117.Text = "التاريخ";
            this.xrLabel117.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.Dpi = 100F;
            this.xrLabel37.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(261.5726F, 23.83327F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.Text = "............................";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 100F;
            this.xrLabel20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 5.000114F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(266.394F, 18.83335F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Name And Signature of Requester/Supervisior";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.BorderWidth = 0F;
            this.xrLabel22.Dpi = 100F;
            this.xrLabel22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 43.83337F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseBorderWidth = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Name";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.BorderWidth = 0F;
            this.xrLabel23.Dpi = 100F;
            this.xrLabel23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(11.23965F, 66.83337F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(57.29166F, 23.00002F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseBorderWidth = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Date :";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.BorderWidth = 0F;
            this.xrLabel27.Dpi = 100F;
            this.xrLabel27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(233.9479F, 68.83344F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(60.41669F, 23.00006F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseBorderWidth = false;
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Time :";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.BorderWidth = 0F;
            this.xrLabel26.Dpi = 100F;
            this.xrLabel26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(233.9478F, 44.83344F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(60.41669F, 22.99994F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseBorderWidth = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Signature";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Dpi = 100F;
            this.xrLabel36.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(11.23962F, 23.83334F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(151.4598F, 18.83337F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.Text = "............................";
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel111.BorderWidth = 0F;
            this.xrLabel111.Dpi = 100F;
            this.xrLabel111.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(280.2601F, 4.249827F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(132.7723F, 19.58326F);
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.StylePriority.UseBorderWidth = false;
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "توقيع الطالب/المشرف";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // SafetyInstructions
            // 
            this.SafetyInstructions.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.SafetyDetails,
            this.SafetyHeader});
            this.SafetyInstructions.DataSource = this.objectDataSource2;
            this.SafetyInstructions.Dpi = 100F;
            this.SafetyInstructions.Level = 4;
            this.SafetyInstructions.Name = "SafetyInstructions";
            // 
            // SafetyDetails
            // 
            this.SafetyDetails.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
            this.SafetyDetails.Dpi = 100F;
            this.SafetyDetails.HeightF = 32.70511F;
            this.SafetyDetails.Name = "SafetyDetails";
            // 
            // xrPanel2
            // 
            this.xrPanel2.BorderColor = System.Drawing.Color.LightGray;
            this.xrPanel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.xrPanel2.Dpi = 100F;
            this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(5.753398F, 0F);
            this.xrPanel2.Name = "xrPanel2";
            this.xrPanel2.SizeF = new System.Drawing.SizeF(821.3296F, 32.70511F);
            this.xrPanel2.StylePriority.UseBorderColor = false;
            this.xrPanel2.StylePriority.UseBorders = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 100F;
            this.xrTable1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.000046F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(821.3297F, 28.76384F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.seqId,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Dpi = 100F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.73722488403320274D;
            // 
            // seqId
            // 
            this.seqId.BorderWidth = 0F;
            this.seqId.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource1, "SeqId")});
            this.seqId.Dpi = 100F;
            this.seqId.Font = new System.Drawing.Font("Arial", 9F);
            this.seqId.KeepTogether = true;
            this.seqId.Name = "seqId";
            this.seqId.StylePriority.UseBorderWidth = false;
            this.seqId.StylePriority.UseFont = false;
            this.seqId.StylePriority.UseTextAlignment = false;
            this.seqId.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.seqId.Weight = 0.34723740497213573D;
            // 
            // objectDataSource1
            // 
            this.objectDataSource1.DataMember = "PrintSafetyInsturctionDetails";
            this.objectDataSource1.DataSource = typeof(CMMS.Service.JobOrderService);
            this.objectDataSource1.Name = "objectDataSource1";
            parameter1.Name = "workOrderNo";
            parameter1.Type = typeof(string);
            this.objectDataSource1.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter1});
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.BorderWidth = 0F;
            this.xrTableCell2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource1, "Details")});
            this.xrTableCell2.Dpi = 100F;
            this.xrTableCell2.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTableCell2.KeepTogether = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorderWidth = false;
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell2.Weight = 1.3461555205257143D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.BorderWidth = 0F;
            this.xrTableCell3.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", this.objectDataSource1, "AltDetails")});
            this.xrTableCell3.Dpi = 100F;
            this.xrTableCell3.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTableCell3.KeepTogether = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorderWidth = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell3.Weight = 1.19304199908221D;
            // 
            // SafetyHeader
            // 
            this.SafetyHeader.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.SafetyHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel121,
            this.xrLabel83});
            this.SafetyHeader.Dpi = 100F;
            this.SafetyHeader.HeightF = 35.55489F;
            this.SafetyHeader.Name = "SafetyHeader";
            this.SafetyHeader.StylePriority.UseBorders = false;
            // 
            // xrLabel121
            // 
            this.xrLabel121.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel121.BorderWidth = 0F;
            this.xrLabel121.Dpi = 100F;
            this.xrLabel121.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel121.LocationFloat = new DevExpress.Utils.PointFloat(720.0248F, 7.999889F);
            this.xrLabel121.Name = "xrLabel121";
            this.xrLabel121.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel121.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Suppress;
            this.xrLabel121.SizeF = new System.Drawing.SizeF(109.0497F, 27.55497F);
            this.xrLabel121.StylePriority.UseBorders = false;
            this.xrLabel121.StylePriority.UseBorderWidth = false;
            this.xrLabel121.StylePriority.UseFont = false;
            this.xrLabel121.StylePriority.UseTextAlignment = false;
            this.xrLabel121.Text = "تعليمات السلامة";
            this.xrLabel121.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel83
            // 
            this.xrLabel83.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel83.BorderWidth = 0F;
            this.xrLabel83.Dpi = 100F;
            this.xrLabel83.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(41.85605F, 8.000023F);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.ProcessDuplicatesMode = DevExpress.XtraReports.UI.ProcessDuplicatesMode.Suppress;
            this.xrLabel83.SizeF = new System.Drawing.SizeF(151.4598F, 27.55483F);
            this.xrLabel83.StylePriority.UseBorders = false;
            this.xrLabel83.StylePriority.UseBorderWidth = false;
            this.xrLabel83.StylePriority.UseFont = false;
            this.xrLabel83.StylePriority.UseTextAlignment = false;
            this.xrLabel83.Text = "Safety Instructions :";
            this.xrLabel83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // objectDataSource2
            // 
            this.objectDataSource2.DataMember = "PrintWorkOrderElement";
            this.objectDataSource2.DataSource = typeof(CMMS.Service.JobOrderService);
            this.objectDataSource2.Name = "objectDataSource2";
            parameter2.Name = "workOrderNo";
            parameter2.Type = typeof(string);
            this.objectDataSource2.Parameters.AddRange(new DevExpress.DataAccess.ObjectBinding.Parameter[] {
            parameter2});
            // 
            // CheckListReport
            // 
            this.CheckListReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.CheckListDetail,
            this.CheckListHeader});
            this.CheckListReport.DataSource = this.objectDataSource2;
            this.CheckListReport.Dpi = 100F;
            this.CheckListReport.Level = 0;
            this.CheckListReport.Name = "CheckListReport";
            // 
            // CheckListDetail
            // 
            this.CheckListDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.checkListTable});
            this.CheckListDetail.Dpi = 100F;
            this.CheckListDetail.HeightF = 33.33333F;
            this.CheckListDetail.Name = "CheckListDetail";
            // 
            // checkListTable
            // 
            this.checkListTable.Dpi = 100F;
            this.checkListTable.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkListTable.LocationFloat = new DevExpress.Utils.PointFloat(5.753398F, 0F);
            this.checkListTable.Name = "checkListTable";
            this.checkListTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.checkListTable.SizeF = new System.Drawing.SizeF(821.3297F, 28.76384F);
            this.checkListTable.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.seqNo,
            this.taskDesc,
            this.altTaskDesc,
            this.xrTableCell1});
            this.xrTableRow2.Dpi = 100F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // seqNo
            // 
            this.seqNo.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "SeqNo")});
            this.seqNo.Dpi = 100F;
            this.seqNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seqNo.Name = "seqNo";
            this.seqNo.StylePriority.UseFont = false;
            this.seqNo.StylePriority.UseTextAlignment = false;
            this.seqNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.seqNo.Weight = 0.35039432160139017D;
            // 
            // taskDesc
            // 
            this.taskDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "TaskDesc")});
            this.taskDesc.Dpi = 100F;
            this.taskDesc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.taskDesc.Name = "taskDesc";
            this.taskDesc.StylePriority.UseFont = false;
            this.taskDesc.StylePriority.UseTextAlignment = false;
            this.taskDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.taskDesc.Weight = 1.3583937647378155D;
            // 
            // altTaskDesc
            // 
            this.altTaskDesc.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "AltTaskDesc")});
            this.altTaskDesc.Dpi = 100F;
            this.altTaskDesc.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altTaskDesc.Name = "altTaskDesc";
            this.altTaskDesc.StylePriority.UseFont = false;
            this.altTaskDesc.StylePriority.UseTextAlignment = false;
            this.altTaskDesc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.altTaskDesc.Weight = 1.0506155191616895D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrShape1});
            this.xrTableCell1.Dpi = 100F;
            this.xrTableCell1.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell1.Weight = 0.2479172116413369D;
            // 
            // xrShape1
            // 
            this.xrShape1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrShape1.Dpi = 100F;
            this.xrShape1.LineWidth = 2;
            this.xrShape1.LocationFloat = new DevExpress.Utils.PointFloat(14.62391F, 3F);
            this.xrShape1.Name = "xrShape1";
            this.xrShape1.Shape = shapeRectangle1;
            this.xrShape1.SizeF = new System.Drawing.SizeF(37.11313F, 25.00006F);
            this.xrShape1.Stretch = true;
            this.xrShape1.StylePriority.UseBorders = false;
            // 
            // CheckListHeader
            // 
            this.CheckListHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel81,
            this.xrLabel128,
            this.xrLabel129,
            this.xrLabel126,
            this.xrLabel127,
            this.xrPanel3,
            this.xrLabel84,
            this.xrLabel85,
            this.xrLabel86,
            this.xrLabel87,
            this.xrLabel88});
            this.CheckListHeader.Dpi = 100F;
            this.CheckListHeader.HeightF = 147.132F;
            this.CheckListHeader.Name = "CheckListHeader";
            // 
            // xrLabel81
            // 
            this.xrLabel81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel81.Dpi = 100F;
            this.xrLabel81.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(566.6111F, 0F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(256.2847F, 24.57097F);
            this.xrLabel81.StylePriority.UseBorders = false;
            this.xrLabel81.StylePriority.UseFont = false;
            this.xrLabel81.StylePriority.UseTextAlignment = false;
            this.xrLabel81.Text = "الرجوع إلى مرفقات قائمة فحص الصيانة الوقائية";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel128
            // 
            this.xrLabel128.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel128.Dpi = 100F;
            this.xrLabel128.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(664.196F, 109.4804F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(9.862732F, 23F);
            this.xrLabel128.StylePriority.UseBorders = false;
            this.xrLabel128.StylePriority.UseFont = false;
            this.xrLabel128.StylePriority.UseTextAlignment = false;
            this.xrLabel128.Text = ":";
            this.xrLabel128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel129
            // 
            this.xrLabel129.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel129.Dpi = 100F;
            this.xrLabel129.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel129.LocationFloat = new DevExpress.Utils.PointFloat(674.0588F, 109.4804F);
            this.xrLabel129.Name = "xrLabel129";
            this.xrLabel129.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel129.SizeF = new System.Drawing.SizeF(148.837F, 26.1863F);
            this.xrLabel129.StylePriority.UseBorders = false;
            this.xrLabel129.StylePriority.UseFont = false;
            this.xrLabel129.StylePriority.UseTextAlignment = false;
            this.xrLabel129.Text = "وصف قائمة الفحص";
            this.xrLabel129.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel126
            // 
            this.xrLabel126.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel126.Dpi = 100F;
            this.xrLabel126.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(311.7708F, 109.4803F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(7.460297F, 26.18629F);
            this.xrLabel126.StylePriority.UseBorders = false;
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.StylePriority.UseTextAlignment = false;
            this.xrLabel126.Text = ":";
            this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel127
            // 
            this.xrLabel127.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel127.Dpi = 100F;
            this.xrLabel127.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(320.1668F, 109.4803F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(99.99997F, 26.1863F);
            this.xrLabel127.StylePriority.UseBorders = false;
            this.xrLabel127.StylePriority.UseFont = false;
            this.xrLabel127.StylePriority.UseTextAlignment = false;
            this.xrLabel127.Text = "رقم قائمة الفحص";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrPanel3
            // 
            this.xrPanel3.BorderColor = System.Drawing.Color.LightGray;
            this.xrPanel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel122,
            this.xrLabel123,
            this.xrLabel124,
            this.xrLabel125,
            this.xrLabel89,
            this.xrLabel90,
            this.xrLabel91,
            this.xrLabel92,
            this.xrLabel93,
            this.xrLabel94,
            this.xrLabel95,
            this.xrLabel96,
            this.xrLabel97,
            this.xrLabel98,
            this.xrLabel99,
            this.xrLabel100,
            this.xrLabel101,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel104});
            this.xrPanel3.Dpi = 100F;
            this.xrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(2.250107F, 25.57093F);
            this.xrPanel3.Name = "xrPanel3";
            this.xrPanel3.SizeF = new System.Drawing.SizeF(823.2497F, 82.29163F);
            this.xrPanel3.StylePriority.UseBorderColor = false;
            this.xrPanel3.StylePriority.UseBorders = false;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.Dpi = 100F;
            this.xrLabel122.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(661.9459F, 33.2916F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(9.862732F, 23F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.StylePriority.UseTextAlignment = false;
            this.xrLabel122.Text = ":";
            this.xrLabel122.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.Dpi = 100F;
            this.xrLabel123.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(671.8087F, 33.29163F);
            this.xrLabel123.Multiline = true;
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(148.837F, 23F);
            this.xrLabel123.StylePriority.UseBorders = false;
            this.xrLabel123.StylePriority.UseFont = false;
            this.xrLabel123.StylePriority.UseTextAlignment = false;
            this.xrLabel123.Text = "نوع الطلب\r\n";
            this.xrLabel123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel124
            // 
            this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel124.Dpi = 100F;
            this.xrLabel124.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(661.9459F, 56.29168F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(9.862732F, 23F);
            this.xrLabel124.StylePriority.UseBorders = false;
            this.xrLabel124.StylePriority.UseFont = false;
            this.xrLabel124.StylePriority.UseTextAlignment = false;
            this.xrLabel124.Text = ":";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel125
            // 
            this.xrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel125.Dpi = 100F;
            this.xrLabel125.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(671.8087F, 56.29171F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(148.837F, 23.00001F);
            this.xrLabel125.StylePriority.UseBorders = false;
            this.xrLabel125.StylePriority.UseFont = false;
            this.xrLabel125.StylePriority.UseTextAlignment = false;
            this.xrLabel125.Text = "أولوية الطلب";
            this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel89.Dpi = 100F;
            this.xrLabel89.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(309.5206F, 56.29169F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(7.460297F, 23F);
            this.xrLabel89.StylePriority.UseBorders = false;
            this.xrLabel89.StylePriority.UseFont = false;
            this.xrLabel89.StylePriority.UseTextAlignment = false;
            this.xrLabel89.Text = ":";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel90
            // 
            this.xrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel90.Dpi = 100F;
            this.xrLabel90.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(317.9167F, 56.29169F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel90.StylePriority.UseBorders = false;
            this.xrLabel90.StylePriority.UseFont = false;
            this.xrLabel90.StylePriority.UseTextAlignment = false;
            this.xrLabel90.Text = "حالة الطلب";
            this.xrLabel90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.Dpi = 100F;
            this.xrLabel91.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(309.5206F, 33.29163F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(7.460297F, 23F);
            this.xrLabel91.StylePriority.UseBorders = false;
            this.xrLabel91.StylePriority.UseFont = false;
            this.xrLabel91.StylePriority.UseTextAlignment = false;
            this.xrLabel91.Text = ":";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.Dpi = 100F;
            this.xrLabel92.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(661.9459F, 7F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(9.862732F, 23F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = ":";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel93.Dpi = 100F;
            this.xrLabel93.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(317.9167F, 33.2916F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel93.StylePriority.UseBorders = false;
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.Text = "فئة الطلب";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.Dpi = 100F;
            this.xrLabel94.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(671.8087F, 5.333328F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(148.837F, 27.66663F);
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "رقم أمر العمل";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.Dpi = 100F;
            this.xrLabel95.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(1.749902F, 5.62501F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(152.4583F, 27.66663F);
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.StylePriority.UseTextAlignment = false;
            this.xrLabel95.Text = "Job Order No :";
            this.xrLabel95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.Dpi = 100F;
            this.xrLabel96.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(155.7084F, 5.62501F);
            this.xrLabel96.Multiline = true;
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(140.2083F, 27.66663F);
            this.xrLabel96.StylePriority.UseBorders = false;
            this.xrLabel96.StylePriority.UseFont = false;
            this.xrLabel96.StylePriority.UsePadding = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel97.Dpi = 100F;
            this.xrLabel97.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(1.749902F, 33.29163F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.xrLabel97.StylePriority.UseBorders = false;
            this.xrLabel97.StylePriority.UseFont = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = "Job Trade :";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel98.Dpi = 100F;
            this.xrLabel98.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(155.7084F, 33.29163F);
            this.xrLabel98.Multiline = true;
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(140.2083F, 23F);
            this.xrLabel98.StylePriority.UseBorders = false;
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UsePadding = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel99.Dpi = 100F;
            this.xrLabel99.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(1.749902F, 56.29165F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(152.4583F, 23F);
            this.xrLabel99.StylePriority.UseBorders = false;
            this.xrLabel99.StylePriority.UseFont = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = "Job Status :";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel100
            // 
            this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel100.Dpi = 100F;
            this.xrLabel100.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(155.7084F, 56.29165F);
            this.xrLabel100.Multiline = true;
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(140.2083F, 23F);
            this.xrLabel100.StylePriority.UseBorders = false;
            this.xrLabel100.StylePriority.UseFont = false;
            this.xrLabel100.StylePriority.UsePadding = false;
            this.xrLabel100.StylePriority.UseTextAlignment = false;
            this.xrLabel100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel101.Dpi = 100F;
            this.xrLabel101.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(418.7289F, 33.2916F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(131.2501F, 23F);
            this.xrLabel101.StylePriority.UseBorders = false;
            this.xrLabel101.StylePriority.UseFont = false;
            this.xrLabel101.StylePriority.UseTextAlignment = false;
            this.xrLabel101.Text = "Job Type :";
            this.xrLabel101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.Dpi = 100F;
            this.xrLabel102.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(549.9789F, 33.2916F);
            this.xrLabel102.Multiline = true;
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(109.5344F, 23F);
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UsePadding = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.Dpi = 100F;
            this.xrLabel103.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(418.7289F, 56.29168F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(131.2501F, 23F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "Job Priority :";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel104.Dpi = 100F;
            this.xrLabel104.Font = new System.Drawing.Font("Arial", 9F);
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(549.9789F, 56.29168F);
            this.xrLabel104.Multiline = true;
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(109.5344F, 23F);
            this.xrLabel104.StylePriority.UseBorders = false;
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.StylePriority.UsePadding = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.Dpi = 100F;
            this.xrLabel84.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(3.499908F, 0F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(283.1249F, 24.57097F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "Refer To Attached PM Checklist";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.Dpi = 100F;
            this.xrLabel85.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(0F, 110.9456F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(153.9597F, 26.18633F);
            this.xrLabel85.StylePriority.UseBorders = false;
            this.xrLabel85.StylePriority.UseFont = false;
            this.xrLabel85.StylePriority.UseTextAlignment = false;
            this.xrLabel85.Text = "PM Checklist No:";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.Dpi = 100F;
            this.xrLabel86.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(157.9585F, 110.9456F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(132.7449F, 26.18633F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel87.Dpi = 100F;
            this.xrLabel87.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(420.979F, 109.4803F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(131.25F, 26.18633F);
            this.xrLabel87.StylePriority.UseBorders = false;
            this.xrLabel87.StylePriority.UseFont = false;
            this.xrLabel87.StylePriority.UseTextAlignment = false;
            this.xrLabel87.Text = "PM Checklist Desc:";
            this.xrLabel87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel88.Dpi = 100F;
            this.xrLabel88.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(552.229F, 109.4803F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(109.5344F, 26.18633F);
            this.xrLabel88.StylePriority.UseBorders = false;
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.StylePriority.UseTextAlignment = false;
            this.xrLabel88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1,
            this.xrPageInfo2});
            this.GroupFooter1.Dpi = 100F;
            this.GroupFooter1.HeightF = 24.92857F;
            this.GroupFooter1.Name = "GroupFooter1";
            this.GroupFooter1.PrintAtBottom = true;
            this.GroupFooter1.RepeatEveryPage = true;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 100F;
            this.xrPageInfo1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo1.Format = "{0:dd-MMM-yy}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(4.00001F, 1.928571F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo1.StylePriority.UseFont = false;
            // 
            // xrPageInfo2
            // 
            this.xrPageInfo2.Dpi = 100F;
            this.xrPageInfo2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrPageInfo2.Format = "Page {0} of {1}";
            this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(729.0745F, 0F);
            this.xrPageInfo2.Name = "xrPageInfo2";
            this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo2.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrPageInfo2.StylePriority.UseFont = false;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Dpi = 100F;
            this.GroupHeader1.Expanded = false;
            this.GroupHeader1.HeightF = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            // 
            // PMChecklistReportItems
            // 
            this.PMChecklistReportItems.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.PMChecklistReportItemsDetail,
            this.PMChecklistReportItemsHeader});
            this.PMChecklistReportItems.DataSource = this.objectDataSource2;
            this.PMChecklistReportItems.Dpi = 100F;
            this.PMChecklistReportItems.Level = 1;
            this.PMChecklistReportItems.Name = "PMChecklistReportItems";
            // 
            // PMChecklistReportItemsDetail
            // 
            this.PMChecklistReportItemsDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.PMChecklistReportItemsDetail.Dpi = 100F;
            this.PMChecklistReportItemsDetail.HeightF = 30.28253F;
            this.PMChecklistReportItemsDetail.Name = "PMChecklistReportItemsDetail";
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 100F;
            this.xrTable2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(32.16832F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable2.SizeF = new System.Drawing.SizeF(792.5297F, 28.76384F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.PMItemsSeqNo,
            this.PMItemsNo,
            this.PMItemsDescription,
            this.PMItemsAltDescription,
            this.PMItemsQty});
            this.xrTableRow3.Dpi = 100F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // PMItemsSeqNo
            // 
            this.PMItemsSeqNo.Dpi = 100F;
            this.PMItemsSeqNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMItemsSeqNo.Name = "PMItemsSeqNo";
            this.PMItemsSeqNo.StylePriority.UseFont = false;
            this.PMItemsSeqNo.StylePriority.UseTextAlignment = false;
            this.PMItemsSeqNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.PMItemsSeqNo.Weight = 0.18307633060955272D;
            // 
            // PMItemsNo
            // 
            this.PMItemsNo.Dpi = 100F;
            this.PMItemsNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMItemsNo.Name = "PMItemsNo";
            this.PMItemsNo.StylePriority.UseFont = false;
            this.PMItemsNo.StylePriority.UseTextAlignment = false;
            this.PMItemsNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMItemsNo.Weight = 0.71654700306782748D;
            // 
            // PMItemsDescription
            // 
            this.PMItemsDescription.Dpi = 100F;
            this.PMItemsDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMItemsDescription.Name = "PMItemsDescription";
            this.PMItemsDescription.StylePriority.UseFont = false;
            this.PMItemsDescription.StylePriority.UseTextAlignment = false;
            this.PMItemsDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMItemsDescription.Weight = 0.91538169092835664D;
            // 
            // PMItemsAltDescription
            // 
            this.PMItemsAltDescription.Dpi = 100F;
            this.PMItemsAltDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMItemsAltDescription.Name = "PMItemsAltDescription";
            this.PMItemsAltDescription.StylePriority.UseFont = false;
            this.PMItemsAltDescription.StylePriority.UseTextAlignment = false;
            this.PMItemsAltDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMItemsAltDescription.Weight = 0.91538172129827611D;
            // 
            // PMItemsQty
            // 
            this.PMItemsQty.Dpi = 100F;
            this.PMItemsQty.Font = new System.Drawing.Font("Arial", 9F);
            this.PMItemsQty.Name = "PMItemsQty";
            this.PMItemsQty.StylePriority.UseFont = false;
            this.PMItemsQty.StylePriority.UseTextAlignment = false;
            this.PMItemsQty.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.PMItemsQty.Weight = 0.2112816039039036D;
            // 
            // PMChecklistReportItemsHeader
            // 
            this.PMChecklistReportItemsHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel156,
            this.xrLabel157});
            this.PMChecklistReportItemsHeader.Dpi = 100F;
            this.PMChecklistReportItemsHeader.HeightF = 45.83333F;
            this.PMChecklistReportItemsHeader.Name = "PMChecklistReportItemsHeader";
            // 
            // xrLabel156
            // 
            this.xrLabel156.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel156.Dpi = 100F;
            this.xrLabel156.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(41.85605F, 10F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(283.1249F, 24.57097F);
            this.xrLabel156.StylePriority.UseBorders = false;
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.StylePriority.UseTextAlignment = false;
            this.xrLabel156.Text = "PM Checklist Items";
            this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel157
            // 
            this.xrLabel157.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel157.Dpi = 100F;
            this.xrLabel157.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(568.4133F, 10F);
            this.xrLabel157.Name = "xrLabel157";
            this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel157.SizeF = new System.Drawing.SizeF(256.2847F, 24.57097F);
            this.xrLabel157.StylePriority.UseBorders = false;
            this.xrLabel157.StylePriority.UseFont = false;
            this.xrLabel157.StylePriority.UseTextAlignment = false;
            this.xrLabel157.Text = "البنود المرجعية بيإم";
            this.xrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PMChecklistReportTools
            // 
            this.PMChecklistReportTools.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.PMChecklistReportToolsDetail,
            this.PMChecklistReportToolsHeader});
            this.PMChecklistReportTools.DataSource = this.objectDataSource2;
            this.PMChecklistReportTools.Dpi = 100F;
            this.PMChecklistReportTools.Level = 2;
            this.PMChecklistReportTools.Name = "PMChecklistReportTools";
            // 
            // PMChecklistReportToolsDetail
            // 
            this.PMChecklistReportToolsDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.PMChecklistReportToolsDetail.Dpi = 100F;
            this.PMChecklistReportToolsDetail.HeightF = 30.45107F;
            this.PMChecklistReportToolsDetail.Name = "PMChecklistReportToolsDetail";
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 100F;
            this.xrTable3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(53.00169F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable3.SizeF = new System.Drawing.SizeF(774.0817F, 28.76384F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.PMChecklistReportToolsDescription,
            this.PMChecklistReportToolsAltDescription,
            this.PMChecklistReportToolsSeqNo});
            this.xrTableRow4.Dpi = 100F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // PMChecklistReportToolsDescription
            // 
            this.PMChecklistReportToolsDescription.Dpi = 100F;
            this.PMChecklistReportToolsDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportToolsDescription.Name = "PMChecklistReportToolsDescription";
            this.PMChecklistReportToolsDescription.StylePriority.UseFont = false;
            this.PMChecklistReportToolsDescription.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportToolsDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMChecklistReportToolsDescription.Weight = 1.3372921300498819D;
            // 
            // PMChecklistReportToolsAltDescription
            // 
            this.PMChecklistReportToolsAltDescription.Dpi = 100F;
            this.PMChecklistReportToolsAltDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportToolsAltDescription.Name = "PMChecklistReportToolsAltDescription";
            this.PMChecklistReportToolsAltDescription.StylePriority.UseFont = false;
            this.PMChecklistReportToolsAltDescription.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportToolsAltDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMChecklistReportToolsAltDescription.Weight = 1.2261642268291775D;
            // 
            // PMChecklistReportToolsSeqNo
            // 
            this.PMChecklistReportToolsSeqNo.Dpi = 100F;
            this.PMChecklistReportToolsSeqNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportToolsSeqNo.Name = "PMChecklistReportToolsSeqNo";
            this.PMChecklistReportToolsSeqNo.StylePriority.UseFont = false;
            this.PMChecklistReportToolsSeqNo.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportToolsSeqNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.PMChecklistReportToolsSeqNo.Weight = 0.25591138120686163D;
            // 
            // PMChecklistReportToolsHeader
            // 
            this.PMChecklistReportToolsHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel158,
            this.xrLabel159});
            this.PMChecklistReportToolsHeader.Dpi = 100F;
            this.PMChecklistReportToolsHeader.HeightF = 50F;
            this.PMChecklistReportToolsHeader.Name = "PMChecklistReportToolsHeader";
            // 
            // xrLabel158
            // 
            this.xrLabel158.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel158.Dpi = 100F;
            this.xrLabel158.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(41.85605F, 9.999974F);
            this.xrLabel158.Name = "xrLabel158";
            this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel158.SizeF = new System.Drawing.SizeF(283.1249F, 24.57097F);
            this.xrLabel158.StylePriority.UseBorders = false;
            this.xrLabel158.StylePriority.UseFont = false;
            this.xrLabel158.StylePriority.UseTextAlignment = false;
            this.xrLabel158.Text = "PM Checklist Tools";
            this.xrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel159
            // 
            this.xrLabel159.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel159.Dpi = 100F;
            this.xrLabel159.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(570.6897F, 9.999974F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(256.2847F, 24.57097F);
            this.xrLabel159.StylePriority.UseBorders = false;
            this.xrLabel159.StylePriority.UseFont = false;
            this.xrLabel159.StylePriority.UseTextAlignment = false;
            this.xrLabel159.Text = "أدوات المرجعية بيإم";
            this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // PMChecklistReportPPE
            // 
            this.PMChecklistReportPPE.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.PMChecklistReportPPEDetail,
            this.PMChecklistReportPPEHeader});
            this.PMChecklistReportPPE.DataSource = this.objectDataSource2;
            this.PMChecklistReportPPE.Dpi = 100F;
            this.PMChecklistReportPPE.Level = 3;
            this.PMChecklistReportPPE.Name = "PMChecklistReportPPE";
            // 
            // PMChecklistReportPPEDetail
            // 
            this.PMChecklistReportPPEDetail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.PMChecklistReportPPEDetail.Dpi = 100F;
            this.PMChecklistReportPPEDetail.HeightF = 33.91991F;
            this.PMChecklistReportPPEDetail.Name = "PMChecklistReportPPEDetail";
            // 
            // xrTable4
            // 
            this.xrTable4.Dpi = 100F;
            this.xrTable4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(5.753376F, 0F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable4.SizeF = new System.Drawing.SizeF(821.33F, 28.76384F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.PMChecklistReportPPESeqNo,
            this.PMChecklistReportPPEDescription,
            this.PMChecklistReportPPEAltDescription});
            this.xrTableRow9.Dpi = 100F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // PMChecklistReportPPESeqNo
            // 
            this.PMChecklistReportPPESeqNo.Dpi = 100F;
            this.PMChecklistReportPPESeqNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportPPESeqNo.Name = "PMChecklistReportPPESeqNo";
            this.PMChecklistReportPPESeqNo.StylePriority.UseFont = false;
            this.PMChecklistReportPPESeqNo.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportPPESeqNo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.PMChecklistReportPPESeqNo.Weight = 0.3601278991907001D;
            // 
            // PMChecklistReportPPEDescription
            // 
            this.PMChecklistReportPPEDescription.Dpi = 100F;
            this.PMChecklistReportPPEDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportPPEDescription.Name = "PMChecklistReportPPEDescription";
            this.PMChecklistReportPPEDescription.StylePriority.UseFont = false;
            this.PMChecklistReportPPEDescription.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportPPEDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMChecklistReportPPEDescription.Weight = 1.427827150573056D;
            // 
            // PMChecklistReportPPEAltDescription
            // 
            this.PMChecklistReportPPEAltDescription.Dpi = 100F;
            this.PMChecklistReportPPEAltDescription.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PMChecklistReportPPEAltDescription.Name = "PMChecklistReportPPEAltDescription";
            this.PMChecklistReportPPEAltDescription.StylePriority.UseFont = false;
            this.PMChecklistReportPPEAltDescription.StylePriority.UseTextAlignment = false;
            this.PMChecklistReportPPEAltDescription.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.PMChecklistReportPPEAltDescription.Weight = 1.3029066032090846D;
            // 
            // PMChecklistReportPPEHeader
            // 
            this.PMChecklistReportPPEHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel160,
            this.xrLabel161});
            this.PMChecklistReportPPEHeader.Dpi = 100F;
            this.PMChecklistReportPPEHeader.HeightF = 47.91667F;
            this.PMChecklistReportPPEHeader.Name = "PMChecklistReportPPEHeader";
            // 
            // xrLabel160
            // 
            this.xrLabel160.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel160.Dpi = 100F;
            this.xrLabel160.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(41.85605F, 10F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(283.1249F, 24.57097F);
            this.xrLabel160.StylePriority.UseBorders = false;
            this.xrLabel160.StylePriority.UseFont = false;
            this.xrLabel160.StylePriority.UseTextAlignment = false;
            this.xrLabel160.Text = "PM Checklist PPE";
            this.xrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel161
            // 
            this.xrLabel161.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel161.Dpi = 100F;
            this.xrLabel161.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel161.LocationFloat = new DevExpress.Utils.PointFloat(568.4133F, 10F);
            this.xrLabel161.Name = "xrLabel161";
            this.xrLabel161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel161.SizeF = new System.Drawing.SizeF(256.2847F, 24.57097F);
            this.xrLabel161.StylePriority.UseBorders = false;
            this.xrLabel161.StylePriority.UseFont = false;
            this.xrLabel161.StylePriority.UseTextAlignment = false;
            this.xrLabel161.Text = "بيإم المرجعية ب";
            this.xrLabel161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // JobOrderPrintJO1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.SafetyInstructions,
            this.CheckListReport,
            this.GroupFooter1,
            this.GroupHeader1,
            this.PMChecklistReportItems,
            this.PMChecklistReportTools,
            this.PMChecklistReportPPE});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.objectDataSource1,
            this.objectDataSource2});
            this.Margins = new System.Drawing.Printing.Margins(10, 10, 35, 5);
            this.Version = "16.2";
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDetailTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemDetailTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectDataSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkListTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        public DevExpress.XtraReports.UI.DetailBand Detail;
        public DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        public DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        public DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        public DevExpress.XtraReports.UI.XRPanel xrPanel1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel16;
        public DevExpress.XtraReports.UI.XRLabel xrLabel17;
        public DevExpress.XtraReports.UI.XRPanel Panel1;
        public DevExpress.XtraReports.UI.XRLabel jobOrderNo;
        public DevExpress.XtraReports.UI.XRLabel JOPlanNo;
        public DevExpress.XtraReports.UI.XRLabel label1;
        public DevExpress.XtraReports.UI.XRLabel JobTrade;
        public DevExpress.XtraReports.UI.XRLabel label2;
        public DevExpress.XtraReports.UI.XRLabel JobStatus;
        public DevExpress.XtraReports.UI.XRLabel xrLabel2;
        public DevExpress.XtraReports.UI.XRLabel JobType;
        public DevExpress.XtraReports.UI.XRLabel xrLabel3;
        public DevExpress.XtraReports.UI.XRLabel JobPriority;
        public DevExpress.XtraReports.UI.XRPanel Panel2;
        public DevExpress.XtraReports.UI.XRLabel xrLabel4;
        public DevExpress.XtraReports.UI.XRLabel assignedSupplier;
        public DevExpress.XtraReports.UI.XRLabel CreatedBy;
        public DevExpress.XtraReports.UI.XRLabel creatdBy;
        public DevExpress.XtraReports.UI.XRLabel xrLabel5;
        public DevExpress.XtraReports.UI.XRLabel rcvDatetime;
        public DevExpress.XtraReports.UI.XRLabel xrLabel6;
        public DevExpress.XtraReports.UI.XRLabel reqDatetime;
        public DevExpress.XtraReports.UI.XRLabel xrLabel7;
        public DevExpress.XtraReports.UI.XRLabel requester;
        public DevExpress.XtraReports.UI.XRLabel xrLabel8;
        public DevExpress.XtraReports.UI.XRLabel phoneNumber;
        public DevExpress.XtraReports.UI.XRPanel Panel3;
        public DevExpress.XtraReports.UI.XRLabel locationNumber;
        public DevExpress.XtraReports.UI.XRLabel txtLocationNumber;
        public DevExpress.XtraReports.UI.XRLabel LocationDescription;
        public DevExpress.XtraReports.UI.XRLabel txtlocationDescription;
        public DevExpress.XtraReports.UI.XRLabel Asset;
        public DevExpress.XtraReports.UI.XRLabel txtAsset;
        public DevExpress.XtraReports.UI.XRLabel AssetDescription;
        public DevExpress.XtraReports.UI.XRLabel txtAssetDescription;
        public DevExpress.XtraReports.UI.XRLabel MaintDivCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintDivCode;
        public DevExpress.XtraReports.UI.XRLabel MaintDeptCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintDeptCode;
        public DevExpress.XtraReports.UI.XRLabel MaintSubDeptCode;
        public DevExpress.XtraReports.UI.XRLabel txtMaintSubDeptCode;
        public DevExpress.XtraReports.UI.XRLabel znCode;
        public DevExpress.XtraReports.UI.XRLabel ZoneCode;
        public DevExpress.XtraReports.UI.XRLabel BuildingCode;
        public DevExpress.XtraReports.UI.XRLabel buildCode;
        public DevExpress.XtraReports.UI.XRLabel lablPmNo;
        public DevExpress.XtraReports.UI.XRLabel txtPmNo;
        public DevExpress.XtraReports.UI.XRLabel failCode;
        public DevExpress.XtraReports.UI.XRLabel FailureCode;
        public DevExpress.XtraReports.UI.XRLabel CityCode;
        public DevExpress.XtraReports.UI.XRLabel ctyCode;
        public DevExpress.XtraReports.UI.XRLabel AreaCode;
        public DevExpress.XtraReports.UI.XRLabel areaCod;
        public DevExpress.XtraReports.UI.XRLabel ProblemDesc;
        public DevExpress.XtraReports.UI.XRLabel problemDescription;
        public DevExpress.XtraReports.UI.XRLabel NotestoAssets;
        public DevExpress.XtraReports.UI.XRLabel notesAssets;
        public DevExpress.XtraReports.UI.XRLabel NotesToLocation;
        public DevExpress.XtraReports.UI.XRLabel xrLabel14;
        public DevExpress.XtraReports.UI.XRPanel Panel4;
        public DevExpress.XtraReports.UI.XRLabel txtPrevTaken;
        public DevExpress.XtraReports.UI.XRLabel txtActionTaken;
        public DevExpress.XtraReports.UI.XRLabel txtCauseDescription;
        public DevExpress.XtraReports.UI.XRLabel xrLabel10;
        public DevExpress.XtraReports.UI.XRLabel xrLabel9;
        public DevExpress.XtraReports.UI.XRLabel xrLabel1;
        public DevExpress.XtraReports.UI.XRPanel Panel5;
        public DevExpress.XtraReports.UI.XRPanel Panel52;
        public DevExpress.XtraReports.UI.XRLabel TimeCompleted;
        public DevExpress.XtraReports.UI.XRLabel DateComplete;
        public DevExpress.XtraReports.UI.XRLabel DateTimeCopleted;
        public DevExpress.XtraReports.UI.XRPanel Panel51;
        public DevExpress.XtraReports.UI.XRTable EmployeeDetailTable;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        public DevExpress.XtraReports.UI.XRTableCell EmployeeName;
        public DevExpress.XtraReports.UI.XRTableCell EmployeeNo;
        public DevExpress.XtraReports.UI.XRTableCell Normal;
        public DevExpress.XtraReports.UI.XRTableCell OT1;
        public DevExpress.XtraReports.UI.XRTableCell OT2;
        public DevExpress.XtraReports.UI.XRTableCell OT3;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        public DevExpress.XtraReports.UI.XRTableCell TableCell31;
        public DevExpress.XtraReports.UI.XRTableCell TableCell32;
        public DevExpress.XtraReports.UI.XRTableCell TableCell33;
        public DevExpress.XtraReports.UI.XRTableCell TableCell34;
        public DevExpress.XtraReports.UI.XRTableCell TableCell35;
        public DevExpress.XtraReports.UI.XRTableCell TableCell36;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        public DevExpress.XtraReports.UI.XRTableCell TableCell37;
        public DevExpress.XtraReports.UI.XRTableCell TableCell38;
        public DevExpress.XtraReports.UI.XRTableCell TableCell39;
        public DevExpress.XtraReports.UI.XRTableCell TableCell40;
        public DevExpress.XtraReports.UI.XRTableCell TableCell41;
        public DevExpress.XtraReports.UI.XRTableCell TableCell42;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        public DevExpress.XtraReports.UI.XRTableCell TableCell43;
        public DevExpress.XtraReports.UI.XRTableCell TableCell44;
        public DevExpress.XtraReports.UI.XRTableCell TableCell45;
        public DevExpress.XtraReports.UI.XRTableCell TableCell46;
        public DevExpress.XtraReports.UI.XRTableCell TableCell47;
        public DevExpress.XtraReports.UI.XRTableCell TableCell48;
        public DevExpress.XtraReports.UI.XRLabel DI;
        public DevExpress.XtraReports.UI.XRLabel ItemDetailLabl;
        public DevExpress.XtraReports.UI.XRLabel xrLabel15;
        public DevExpress.XtraReports.UI.XRPanel Panel6;
        public DevExpress.XtraReports.UI.XRLabel xrLabel35;
        public DevExpress.XtraReports.UI.XRLabel xrLabel34;
        public DevExpress.XtraReports.UI.XRLabel xrLabel18;
        public DevExpress.XtraReports.UI.XRLabel xrLabel19;
        public DevExpress.XtraReports.UI.XRLabel xrLabel21;
        public DevExpress.XtraReports.UI.XRLabel xrLabel24;
        public DevExpress.XtraReports.UI.XRLabel xrLabel25;
        public DevExpress.XtraReports.UI.XRPanel Panel7;
        public DevExpress.XtraReports.UI.XRLabel xrLabel37;
        public DevExpress.XtraReports.UI.XRLabel xrLabel20;
        public DevExpress.XtraReports.UI.XRLabel xrLabel22;
        public DevExpress.XtraReports.UI.XRLabel xrLabel23;
        public DevExpress.XtraReports.UI.XRLabel xrLabel27;
        public DevExpress.XtraReports.UI.XRLabel xrLabel26;
        public DevExpress.XtraReports.UI.XRLabel xrLabel36;
        public DevExpress.XtraReports.UI.XRLabel xrLabel11;
        public DevExpress.XtraReports.UI.XRLabel xrLabel13;
        public DevExpress.XtraReports.UI.XRLabel xrLabel12;
        public DevExpress.XtraReports.UI.XRLabel xrLabel29;
        public DevExpress.XtraReports.UI.XRLabel xrLabel30;
        public DevExpress.XtraReports.UI.XRLabel xrLabel28;
        public DevExpress.XtraReports.UI.XRLabel xrLabel42;
        public DevExpress.XtraReports.UI.XRLabel xrLabel43;
        public DevExpress.XtraReports.UI.XRLabel xrLabel44;
        public DevExpress.XtraReports.UI.XRLabel xrLabel45;
        public DevExpress.XtraReports.UI.XRLabel xrLabel46;
        public DevExpress.XtraReports.UI.XRLabel xrLabel47;
        public DevExpress.XtraReports.UI.XRLabel xrLabel40;
        public DevExpress.XtraReports.UI.XRLabel xrLabel41;
        public DevExpress.XtraReports.UI.XRLabel xrLabel33;
        public DevExpress.XtraReports.UI.XRLabel xrLabel32;
        public DevExpress.XtraReports.UI.XRLabel xrLabel52;
        public DevExpress.XtraReports.UI.XRLabel xrLabel53;
        public DevExpress.XtraReports.UI.XRLabel xrLabel54;
        public DevExpress.XtraReports.UI.XRLabel altCityCode;
        public DevExpress.XtraReports.UI.XRLabel xrLabel56;
        public DevExpress.XtraReports.UI.XRLabel altAreaCode;
        public DevExpress.XtraReports.UI.XRLabel xrLabel31;
        public DevExpress.XtraReports.UI.XRLabel altZoneCode;
        public DevExpress.XtraReports.UI.XRLabel xrLabel48;
        public DevExpress.XtraReports.UI.XRLabel altBuildingCode;
        public DevExpress.XtraReports.UI.XRLabel xrLabel50;
        public DevExpress.XtraReports.UI.XRLabel xrLabel51;
        public DevExpress.XtraReports.UI.XRLabel xrLabel72;
        public DevExpress.XtraReports.UI.XRLabel xrLabel73;
        public DevExpress.XtraReports.UI.XRLabel xrLabel74;
        public DevExpress.XtraReports.UI.XRLabel xrLabel75;
        public DevExpress.XtraReports.UI.XRLabel xrLabel70;
        public DevExpress.XtraReports.UI.XRLabel xrLabel71;
        public DevExpress.XtraReports.UI.XRLabel xrLabel58;
        public DevExpress.XtraReports.UI.XRLabel xrLabel59;
        public DevExpress.XtraReports.UI.XRLabel xrLabel60;
        public DevExpress.XtraReports.UI.XRLabel xrLabel61;
        public DevExpress.XtraReports.UI.XRLabel xrLabel62;
        public DevExpress.XtraReports.UI.XRLabel xrLabel63;
        public DevExpress.XtraReports.UI.XRLabel xrLabel64;
        public DevExpress.XtraReports.UI.XRLabel xrLabel65;
        public DevExpress.XtraReports.UI.XRLabel xrLabel66;
        public DevExpress.XtraReports.UI.XRLabel xrLabel67;
        public DevExpress.XtraReports.UI.XRLabel xrLabel68;
        public DevExpress.XtraReports.UI.XRLabel xrLabel69;
        public DevExpress.XtraReports.UI.XRLabel xrLabel78;
        public DevExpress.XtraReports.UI.XRLabel xrLabel77;
        public DevExpress.XtraReports.UI.XRLabel xrLabel76;
        public DevExpress.XtraReports.UI.XRTable ItemDetailTable;
        public DevExpress.XtraReports.UI.XRTableRow TableRow1;
        public DevExpress.XtraReports.UI.XRTableCell TableCell1;
        public DevExpress.XtraReports.UI.XRTableCell TableCell2;
        public DevExpress.XtraReports.UI.XRTableCell TableCell3;
        public DevExpress.XtraReports.UI.XRTableCell TableCell4;
        public DevExpress.XtraReports.UI.XRTableCell TableCell5;
        public DevExpress.XtraReports.UI.XRTableCell TableCell6;
        public DevExpress.XtraReports.UI.XRTableRow TableRow2;
        public DevExpress.XtraReports.UI.XRTableCell TableCell7;
        public DevExpress.XtraReports.UI.XRTableCell TableCell8;
        public DevExpress.XtraReports.UI.XRTableCell TableCell9;
        public DevExpress.XtraReports.UI.XRTableCell TableCell10;
        public DevExpress.XtraReports.UI.XRTableCell TableCell11;
        public DevExpress.XtraReports.UI.XRTableCell TableCell12;
        public DevExpress.XtraReports.UI.XRTableRow TableRow3;
        public DevExpress.XtraReports.UI.XRTableCell TableCell13;
        public DevExpress.XtraReports.UI.XRTableCell TableCell14;
        public DevExpress.XtraReports.UI.XRTableCell TableCell15;
        public DevExpress.XtraReports.UI.XRTableCell TableCell16;
        public DevExpress.XtraReports.UI.XRTableCell TableCell17;
        public DevExpress.XtraReports.UI.XRTableCell TableCell18;
        public DevExpress.XtraReports.UI.XRTableRow TableRow4;
        public DevExpress.XtraReports.UI.XRTableCell TableCell19;
        public DevExpress.XtraReports.UI.XRTableCell TableCell20;
        public DevExpress.XtraReports.UI.XRTableCell TableCell21;
        public DevExpress.XtraReports.UI.XRTableCell TableCell22;
        public DevExpress.XtraReports.UI.XRTableCell TableCell23;
        public DevExpress.XtraReports.UI.XRTableCell TableCell24;
        public DevExpress.XtraReports.UI.XRLabel xrLabel80;
        public DevExpress.XtraReports.UI.XRLabel xrLabel79;
        public DevExpress.XtraReports.UI.DetailReportBand SafetyInstructions;
        public DevExpress.XtraReports.UI.DetailBand SafetyDetails;
        public DevExpress.XtraReports.UI.ReportHeaderBand SafetyHeader;
        public DevExpress.XtraReports.UI.XRLabel xrLabel83;
        public DevExpress.XtraReports.UI.XRPanel xrPanel2;
        public DevExpress.XtraReports.UI.XRTable xrTable1;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        public DevExpress.XtraReports.UI.XRTableCell seqId;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        public DevExpress.XtraReports.UI.DetailReportBand CheckListReport;
        public DevExpress.XtraReports.UI.DetailBand CheckListDetail;
        public DevExpress.XtraReports.UI.ReportHeaderBand CheckListHeader;
        public DevExpress.XtraReports.UI.XRLabel xrLabel84;
        public DevExpress.XtraReports.UI.XRLabel xrLabel85;
        public DevExpress.XtraReports.UI.XRLabel xrLabel86;
        public DevExpress.XtraReports.UI.XRLabel xrLabel87;
        public DevExpress.XtraReports.UI.XRLabel xrLabel88;
        public DevExpress.XtraReports.UI.XRPanel xrPanel3;
        public DevExpress.XtraReports.UI.XRLabel xrLabel89;
        public DevExpress.XtraReports.UI.XRLabel xrLabel90;
        public DevExpress.XtraReports.UI.XRLabel xrLabel91;
        public DevExpress.XtraReports.UI.XRLabel xrLabel92;
        public DevExpress.XtraReports.UI.XRLabel xrLabel93;
        public DevExpress.XtraReports.UI.XRLabel xrLabel94;
        public DevExpress.XtraReports.UI.XRLabel xrLabel95;
        public DevExpress.XtraReports.UI.XRLabel xrLabel96;
        public DevExpress.XtraReports.UI.XRLabel xrLabel97;
        public DevExpress.XtraReports.UI.XRLabel xrLabel98;
        public DevExpress.XtraReports.UI.XRLabel xrLabel99;
        public DevExpress.XtraReports.UI.XRLabel xrLabel100;
        public DevExpress.XtraReports.UI.XRLabel xrLabel101;
        public DevExpress.XtraReports.UI.XRLabel xrLabel102;
        public DevExpress.XtraReports.UI.XRLabel xrLabel103;
        public DevExpress.XtraReports.UI.XRLabel xrLabel104;
        public DevExpress.XtraReports.UI.XRTable checkListTable;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        public DevExpress.XtraReports.UI.XRTableCell seqNo;
        public DevExpress.XtraReports.UI.XRTableCell taskDesc;
        public DevExpress.XtraReports.UI.XRTableCell altTaskDesc;
        public DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        public DevExpress.XtraReports.UI.XRShape xrShape1;
        public DevExpress.XtraReports.UI.XRLabel xrLabel105;
        public DevExpress.XtraReports.UI.XRLabel xrLabel109;
        public DevExpress.XtraReports.UI.XRLabel xrLabel108;
        public DevExpress.XtraReports.UI.XRLabel xrLabel107;
        public DevExpress.XtraReports.UI.XRLabel xrLabel106;
        public DevExpress.XtraReports.UI.XRLabel xrLabel120;
        public DevExpress.XtraReports.UI.XRLabel xrLabel114;
        public DevExpress.XtraReports.UI.XRLabel xrLabel115;
        public DevExpress.XtraReports.UI.XRLabel xrLabel112;
        public DevExpress.XtraReports.UI.XRLabel xrLabel113;
        public DevExpress.XtraReports.UI.XRLabel xrLabel110;
        public DevExpress.XtraReports.UI.XRLabel xrLabel118;
        public DevExpress.XtraReports.UI.XRLabel xrLabel119;
        public DevExpress.XtraReports.UI.XRLabel xrLabel116;
        public DevExpress.XtraReports.UI.XRLabel xrLabel117;
        public DevExpress.XtraReports.UI.XRLabel xrLabel111;
        public DevExpress.XtraReports.UI.XRLabel xrLabel121;
        public DevExpress.XtraReports.UI.XRLabel xrLabel122;
        public DevExpress.XtraReports.UI.XRLabel xrLabel123;
        public DevExpress.XtraReports.UI.XRLabel xrLabel124;
        public DevExpress.XtraReports.UI.XRLabel xrLabel125;
        public DevExpress.XtraReports.UI.XRLabel xrLabel126;
        public DevExpress.XtraReports.UI.XRLabel xrLabel127;
        public DevExpress.XtraReports.UI.XRLabel xrLabel128;
        public DevExpress.XtraReports.UI.XRLabel xrLabel129;
        public DevExpress.XtraReports.UI.XRLabel xrLabel130;
        public DevExpress.XtraReports.UI.XRLabel xrLabel131;
        public DevExpress.XtraReports.UI.XRLabel xrLabel132;
        public DevExpress.XtraReports.UI.XRLabel xrLabel133;
        public DevExpress.XtraReports.UI.XRLabel xrLabel134;
        public DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource1;
        private DevExpress.DataAccess.ObjectBinding.ObjectDataSource objectDataSource2;
        public DevExpress.XtraReports.UI.XRLabel Time;
        public DevExpress.XtraReports.UI.XRLabel Date;
        public DevExpress.XtraReports.UI.XRLabel DateTimeStarted;
        public DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        public DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        public DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        public DevExpress.XtraReports.UI.XRLabel rptTitle;
        public DevExpress.XtraReports.UI.XRLabel xrLabel135;
        public DevExpress.XtraReports.UI.XRLabel xrLabel55;
        public DevExpress.XtraReports.UI.XRLabel xrLabel49;
        public DevExpress.XtraReports.UI.XRLabel xrLabel39;
        public DevExpress.XtraReports.UI.XRLabel xrLabel57;
        public DevExpress.XtraReports.UI.XRLabel xrLabel139;
        public DevExpress.XtraReports.UI.XRLabel xrLabel140;
        public DevExpress.XtraReports.UI.XRLabel xrLabel143;
        public DevExpress.XtraReports.UI.XRLabel xrLabel141;
        public DevExpress.XtraReports.UI.XRLabel xrLabel142;
        public DevExpress.XtraReports.UI.XRLabel xrLabel137;
        public DevExpress.XtraReports.UI.XRLabel xrLabel136;
        public DevExpress.XtraReports.UI.XRLabel xrLabel150;
        public DevExpress.XtraReports.UI.XRLabel xrLabel144;
        public DevExpress.XtraReports.UI.XRLabel xrLabel145;
        public DevExpress.XtraReports.UI.XRLabel xrLabel146;
        public DevExpress.XtraReports.UI.XRLabel xrLabel147;
        public DevExpress.XtraReports.UI.XRLabel xrLabel148;
        public DevExpress.XtraReports.UI.XRLabel xrLabel149;
        public DevExpress.XtraReports.UI.XRLabel xrLabel138;
        public DevExpress.XtraReports.UI.XRLabel xrLabel155;
        public DevExpress.XtraReports.UI.XRLabel xrLabel154;
        public DevExpress.XtraReports.UI.XRLabel xrLabel153;
        public DevExpress.XtraReports.UI.XRLabel xrLabel152;
        public DevExpress.XtraReports.UI.XRLabel xrLabel151;
        public DevExpress.XtraReports.UI.XRLabel xrLabel82;
        public DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.ReportHeaderBand PMChecklistReportItemsHeader;
        private DevExpress.XtraReports.UI.ReportHeaderBand PMChecklistReportToolsHeader;
        private DevExpress.XtraReports.UI.ReportHeaderBand PMChecklistReportPPEHeader;
        public DevExpress.XtraReports.UI.XRLabel xrLabel156;
        public DevExpress.XtraReports.UI.XRLabel xrLabel157;
        public DevExpress.XtraReports.UI.XRLabel xrLabel158;
        public DevExpress.XtraReports.UI.XRLabel xrLabel159;
        public DevExpress.XtraReports.UI.XRLabel xrLabel160;
        public DevExpress.XtraReports.UI.XRLabel xrLabel161;
        public DevExpress.XtraReports.UI.XRTable xrTable2;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        public DevExpress.XtraReports.UI.XRTableCell PMItemsSeqNo;
        public DevExpress.XtraReports.UI.XRTableCell PMItemsDescription;
        public DevExpress.XtraReports.UI.XRTableCell PMItemsAltDescription;
        public DevExpress.XtraReports.UI.XRTableCell PMItemsQty;
        public DevExpress.XtraReports.UI.DetailReportBand PMChecklistReportItems;
        public DevExpress.XtraReports.UI.DetailBand PMChecklistReportItemsDetail;
        public DevExpress.XtraReports.UI.XRTableCell PMItemsNo;
        public DevExpress.XtraReports.UI.XRTable xrTable3;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportToolsDescription;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportToolsAltDescription;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportToolsSeqNo;
        public DevExpress.XtraReports.UI.XRTable xrTable4;
        public DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportPPESeqNo;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportPPEDescription;
        public DevExpress.XtraReports.UI.XRTableCell PMChecklistReportPPEAltDescription;
        public DevExpress.XtraReports.UI.DetailReportBand PMChecklistReportTools;
        public DevExpress.XtraReports.UI.DetailBand PMChecklistReportToolsDetail;
        public DevExpress.XtraReports.UI.DetailReportBand PMChecklistReportPPE;
        public DevExpress.XtraReports.UI.DetailBand PMChecklistReportPPEDetail;
    }
}
