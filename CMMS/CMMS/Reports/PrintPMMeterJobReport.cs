﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Print PM Meter JobReport
    /// </summary>
    public partial class PrintPMMeterJobReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrintPMMeterJobReport"/> class.
        /// </summary>
        public PrintPMMeterJobReport()
        {
            InitializeComponent();
        }
    }
}
