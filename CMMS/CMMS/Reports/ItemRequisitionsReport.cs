﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CMMS.Reports
{
    /// <summary>
    /// Item Requisitions Report
    /// </summary>
    public partial class ItemRequisitionsReport : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemRequisitionsReport"/> class.
        /// </summary>
        public ItemRequisitionsReport()
        {
            InitializeComponent();
        }
    }
}
