﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CMMS.Model;
using CMMS.Service;
using CMMS.Infrastructure;

namespace CMMS.Models
{
    /// <summary>
    /// Common Class
    /// </summary>
    public static class Common
    {
        #region Error Log - Start

        /// <summary>
        /// Logs the specified ex.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static int Log(Exception ex)
        {
            int errorLogID = 0;
            ErrorLog objErrorLog = new ErrorLog();
            objErrorLog.ErrorDate = DateTime.Now;
            objErrorLog.LoginID = ProjectSession.EmployeeID;
            objErrorLog.IPAddress = HttpContext.Current.Request.UserHostAddress == "::1" ? System.Net.Dns.GetHostName() : HttpContext.Current.Request.UserHostAddress;
            objErrorLog.ClientBrowser = HttpContext.Current.Request.Browser != null ? HttpContext.Current.Request.Browser.Browser + " " + HttpContext.Current.Request.Browser.Version : string.Empty;
            objErrorLog.ErrorMessage = ex.Message;
            objErrorLog.ErrorStackTrace = ex.StackTrace;
            objErrorLog.URL = HttpContext.Current.Request.Url.PathAndQuery;
            if ((HttpContext.Current.Request.UrlReferrer == null) == false)
            {
                objErrorLog.URLReferrer = HttpContext.Current.Request.UrlReferrer.PathAndQuery;
            }

            objErrorLog.ErrorSource = ex.Source;
            objErrorLog.ErrorTargetSite = ex.TargetSite != null ? ex.TargetSite.Name : string.Empty;
            objErrorLog.QueryString = HttpContext.Current.Request.QueryString.ToString();
            objErrorLog.PostData = HttpContext.Current.Request.Form.ToString();

            ////Get Session Data
            System.Text.StringBuilder sessionInfo = new System.Text.StringBuilder();
            System.Collections.Specialized.NameObjectCollectionBase.KeysCollection objSessionKeys = HttpContext.Current.Session.Keys;
            int i = 0;
            while (i <= (objSessionKeys.Count - 1)) 
            {
                if ((HttpContext.Current.Session[i] == null) == false)
                {
                    sessionInfo.Append(System.Environment.NewLine + ((i + 1).ToString() + (":->Name: " + objSessionKeys[i].ToString())));
                    if ((HttpContext.Current.Session[i].GetType().Name == "String") || HttpContext.Current.Session[i].GetType().Name.Contains("Int"))
                    {
                        sessionInfo.Append(" <-->  Value:" + HttpContext.Current.Session[ConvertTo.String(objSessionKeys[i])]);
                    }

                    if (HttpContext.Current.Session[i].GetType().Name == "DataTable")
                    {
                        sessionInfo.Append(" <-->  Value: Number of rows:" + ((System.Data.DataTable)HttpContext.Current.Session[ConvertTo.String(objSessionKeys[i])]).Rows.Count.ToString());
                    }
                }

                i = i + 1;
            }

            objErrorLog.SessionInfo = sessionInfo.ToString();
            
            using (DapperContext context = new DapperContext())
            {
                errorLogID = context.Save(objErrorLog);
            }

            return errorLogID;
        }

        #endregion
    }
}