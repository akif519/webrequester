﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMMS.Models
{
    public class LoginModel
    {
        //[Required(ErrorMessage = "*")]
        public string Username { get; set; }

        //[Required(ErrorMessage = "*")]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
        public string DomainName { get; set; }
        public string DomainSuffix { get; set; }
        public string ClientCode { get; set; }
        public string isActiveDirectory { get; set; }
    }
}