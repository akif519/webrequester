﻿using System.Web;
using System.Web.Optimization;

namespace CMMS
{
    /// <summary>
    /// Bundle Config
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// Register Bundles
        /// </summary>
        /// <param name="bundles">Bundle Collection</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
                        "~/Scripts/jquery-ui.min.js")); 

            bundles.Add(new ScriptBundle("~/bundles/bootstrapDeleteConform").Include(
                      "~/Scripts/bootstrap-tooltip.js",
                      "~/Scripts/bootstrap-confirmation.js"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
                      "~/Scripts/fastclick.min.js",
                      "~/Scripts/custom-tabs.js",
                      "~/Scripts/general.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                    "~/Scripts/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/advancedSearch").Include(
                   "~/Scripts/AdvancedSearch.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/kendo.all.min.js",
                ////"~/Scripts/kendo/kendo.timezones.min.js", // uncomment if using the Scheduler
                "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo/jszip.min.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.plus.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.picker.js",
                "~/Scripts/JqueryCalendar/jquery.calendars.ummalqura.js",
                "~/Scripts/moment.js",
                "~/Scripts/moment-with-locales.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrapCSS").Include(
                      "~/Content/css/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/BundleCSS").Include(
                     "~/Content/css/custom-tabs.css",
                     "~/Content/css/style.css",
                     "~/Content/css/font.css",
                     "~/Content/css/theme.css",
                      "~/Content/css/custom.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/kendocss").Include(
                        "~/Content/kendo/kendo.common-bootstrap.min.css",
                        "~/Content/kendo/kendo.bootstrap.min.css",
                        "~/Content/kendo/kendo.bootstrap.mobile.min.css",
                        "~/Content/kendo/kendo.rtl.min.css",
                        "~/Content/JqueryCalendar/jquery.calendars.picker.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
