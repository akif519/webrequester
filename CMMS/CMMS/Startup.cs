﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CMMS.Startup))]

namespace CMMS
{
    /// <summary>
    /// Start up
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Configurations the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
        }
    }
}
