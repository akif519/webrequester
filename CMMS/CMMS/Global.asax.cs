﻿using CMMS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CMMS
{
    /// <summary>
    /// MVC Application
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application_s the start.
        /// </summary>
        protected void Application_Start()
        {
            DevExpress.XtraReports.Web.ReportDesigner.Native.ReportDesignerBootstrapper.SessionState = System.Web.SessionState.SessionStateBehavior.Required;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            System.Data.Entity.Database.SetInitializer<CMMS.Model.CMMSDBContext>(null);
        }

        #region Error Log - Start

        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            Response.Clear();
            try
            {
                if (!HttpContext.Current.Request.Path.Contains("fontawesome-webfont") && !HttpContext.Current.Request.Path.Contains("segoeui_5-webfont"))
                {
                    if (!(HttpContext.Current.Response.StatusCode == 200 && exception.Message == "Server cannot set content type after HTTP headers have been sent.") &&
                        !(HttpContext.Current.Response.StatusCode == 200 && exception.Message == "The anti-forgery cookie token and form field token do not match.") &&
                        !(HttpContext.Current.Response.StatusCode == 200 && exception.Message.Contains("The provided anti-forgery token was meant for user")) &&
                        !(HttpContext.Current.Response.StatusCode == 200 && exception.Message.Contains("This request has been blocked because sensitive information could")))
                    {
                        int errorLogId = CMMS.Models.Common.Log(exception);
                    }
                }
            }
            catch
            {
            }

            if (CMMS.Infrastructure.ProjectSession.SessionID > 0)
            {
                var httpException = exception as HttpException;

                string controller = string.Empty;
                string action = string.Empty;

                controller = "Error";
                action = "Error";

                if (httpException != null)
                {
                    if (httpException.GetHttpCode() == 404)
                    {
                        action = "PageNotFound";
                    }
                }

                var routeData = new RouteData();
                routeData.Values.Add("controller", controller);
                routeData.Values.Add("action", action);
                routeData.Values.Add("area", string.Empty);
                ////Pass exception details to the target error View.
                routeData.Values.Add("exception", exception);
                ////Clear the error on server.
                Server.ClearError();
                IController errorController = new CMMS.Controllers.ErrorController();
                errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            }
        }

        /// <summary>
        /// Application_s the begin request.
        /// </summary>
        protected void Application_BeginRequest()
        {
            if (ProjectConfiguration.IsSecure == true && !Context.Request.IsSecureConnection)
            {
                Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
            }
        }
        #endregion Error Log - End
    }
}
