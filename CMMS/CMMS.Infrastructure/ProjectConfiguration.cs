﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CMMS.Infrastructure
{
    public class ProjectConfiguration
    {
        #region lang code

        //public static string ArabicCultureCode = "ar-KW";
        //public static string EnglishCultureCode = "en-GB";

        public static string ArabicCultureCode = "ar-sa";
        public static string EnglishCultureCode = "en-us";

        public static string GregorianDate = "gregorian";
        public static string HijriDate = "ummalqura";

        #endregion

        #region layout

        /// <summary>
        /// Arabic layout used for RTL
        /// </summary>
        public static string ArabicLayout = "arabic";

        /// <summary>
        /// English layout used for LTL
        /// </summary>
        public static string EnglishLayOut = "english";

        #endregion

        #region class

        /// <summary>
        /// The arabic culture kendo class
        /// </summary>
        public static string ArabicCultureKendoClass = "k-rtl";
        /// <summary>
        /// The english culture kendo class
        /// </summary>
        public static string EnglishCultureKendoClass = "k-ltr";

        #endregion

        #region class

        /// <summary>
        /// The direction eng
        /// </summary>
        public static string DirectionEng = "left";
        /// <summary>
        /// The direction ara
        /// </summary>
        public static string DirectionAra = "right";

        #endregion

        #region theme

        /// <summary>
        /// The green
        /// </summary>
        public static string Green = "green";

        /// <summary>
        /// The gray
        /// </summary>
        public static string Gray = "gray";

        /// <summary>
        /// The blue
        /// </summary>
        public static string Blue = "blue";

        #endregion

        //public enum DBTYPE
        //{
        //    MSSQL = 1,
        //    MYSQL = 2,
        //    ORACLE = 3
        //}

        /// <summary>
        /// Gets the Active Directory key values. By: Akif519
        /// </summary>
        /// <value>
        /// Check IsActiveDirectory true or false.
        /// </value>
        public static string IsActiveDirectory
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["IsActiveDirectory"]); }
        }
        public static int[] DefaultEmployeeValues
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultEmployeeValues"] != null ? ConfigurationManager.AppSettings["DefaultEmployeeValues"].ToString().Split(',').Select(p => Convert.ToInt32(p)).ToArray() : new int[] { 1237, 1, 3, 5, 872, 5641, 230 };
            }
        }

        public static string DomainSuffix
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DomainSuffix"]);
            }
        }
        public static string DomainName
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DomainName"]);
            }
        }
        public static string ClientCode
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ClientCode"]);
            }
        }

        /// <summary>
        /// Gets the cookies validity.
        /// </summary>
        /// <value>
        /// The cookies validity.
        /// </value>
        public static int CookiesValidity
        {
            get
            {
                return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["CookiesValidity"]);
            }
        }

        public static int PageSize
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize"));
            }
        }
        /// <summary>
        /// Gets the Date Format
        /// </summary>
        public static string DefaultDateFormat
        {
            get
            {
                return "yyyy/MM/dd";
            }
        }

        /// <summary>
        /// Gets the Grid Date Format
        /// </summary>
        public static string GridDateTimeFormat
        {
            get
            {
                return "{0:yyyy/MM/dd hh:mm}";
            }
        }

        public static string ApplicationRootPath
        {
            get
            {
                string rootPath = HttpContext.Current.Server.MapPath("~");
                if (rootPath.EndsWith("\\", StringComparison.CurrentCulture))
                {
                    return rootPath;
                }
                else
                {
                    return rootPath + "\\";
                }
            }
        }

        public static bool TestMode
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["TestMode"]);
            }
        }

        public static string IISSiteName
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["IISSiteName"]);
            }
        }

        public static string TestEmailAddress
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["TestEmailAddress"]);
            }
        }

        public static string FromEmailAddress
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["FromEmailAddress"]);
            }
        }

        //public static bool IsItemNoAutoIncrement
        //{
        //    get
        //    {
        //        return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsItemNoAutoIncrement"]);
        //    }
        //}

        public static bool IsSecure
        {
            get
            {
                return Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsSecure"]);
            }
        }

        //public static Int32 DashboardAutoRefreshMinute
        //{
        //    get
        //    {
        //        return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DashboardAutoRefreshMinute"]); 
        //    }
        //}

        public static string TestEmailSubject
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["TestEmailSubject"]);
            }
        }

        public static string TestEmailBody
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["TestEmailBody"]);
            }
        }

        public static string TestSMSBody
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["TestSMSBody"]);
            }
        }
        /// <summary>
        /// Gets the upload path.
        /// </summary>
        /// <value>
        /// Upload folder path
        /// </value>
        public static string UploadPath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["UploadPath"])))
                {
                    return HttpContext.Current.Server.MapPath("~/" + Convert.ToString(HttpContext.Current.Session["AccountCode"]) + "/") + Convert.ToString(HttpContext.Current.Session["AccountCode"]) + "\\";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UploadPath"]) + "\\" + Convert.ToString(HttpContext.Current.Session["AccountCode"]) + "\\";
                }
            }
        }

        public static string UploadPathAsset
        {
            get
            {
                return "Attachments\\Asset\\";
            }
        }

        public static string UploadPathjobRequest
        {
            get
            {
                return "Attachments\\JobRequest\\";
            }
        }

        public static string UploadPathJobOrder
        {
            get
            {
                return "Attachments\\JobOrder\\";
            }
        }

        public static string UploadPathPreventive
        {
            get
            {
                return "Attachments\\Preventive\\";
            }
        }

        public static string UploadPathLocation
        {
            get
            {
                return "Attachments\\Location\\";
            }
        }

        public static string UploadPathLocationDiagram
        {
            get
            {
                return "Attachments\\LocationDiagram\\";
            }
        }

        public static string UploadPathConsumables
        {
            get
            {
                return "Attachments\\Consumables\\";
            }
        }

        public static string UploadPathItems
        {
            get
            {
                return "Attachments\\StockCode\\";
            }
        }

        public static string UploadPathPurchaseRequest
        {
            get
            {
                return "Attachments\\PurchaseRequest\\";
            }
        }

        public static string UploadPathPurchaseOrder
        {
            get
            {
                return "Attachments\\PurchaseOrder\\";
            }
        }

        public static string UploadPathSupplier
        {
            get
            {
                return "Attachments\\Supplier\\";
            }
        }

        /// <summary>
        /// Gets the Image Extension list.
        /// </summary>
        /// <value>
        /// All Image Extensions
        /// </value>
        public static readonly string[] _validExtensions = { ".jpg", ".bmp", ".gif", ".png", ".jpeg" };

        /// <summary>
        /// Gets the Decimal Place
        /// </summary>
        public static int DecimalPlace
        {
            get
            {
                return 2;
            }
        }

        public static string HeaderImagePath
        {
            get
            {
               // return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["HeaderImagePath"]);
                if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["UploadPath"])))
                {
                    return HttpContext.Current.Server.MapPath("~/Uploads/");
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UploadPath"]);
                }
            }
        }

        public static string VirtualDirectoryAccessURL
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["VirtualDirectoryAccessURL"]);              
            }
        }

        public static readonly string HEADERTEMPLATEWITHCODE = "<div class='ddl-table ddl-table-hed'><span class='code-no'>Code</span><span class='english'>English</span><span class='arabic'>Arabic</span></div>";

        public static readonly string HEADERTEMPLATEWITHOUTCODE = "<div class='ddl-table ddl-table-hed'><span class='english'>English</span><span class='arabic'>Arabic</span></div>";

        /// <summary>
        /// Gets the date fromate
        /// </summary>
        /// <value>
        /// </value>
        public static string DATEFORMATDDMMMYYYHHMMTT
        {
            get
            {
                if (ProjectSession.DateCulture == HijriDate)
                {
                    return "dd dddd , MMMM, yyyy hh:mm tt";
                }
                else
                {
                    return "dd-MMM-yyyy hh:mm tt";
                }
            }

        }

        /// <summary>
        /// Gets the date fromate
        /// </summary>
        /// <value>
        /// </value>
        public static string DATEFORMATDDMMMYYY
        {
            get
            {
                if (ProjectSession.DateCulture == HijriDate)
                {
                    return "dd dddd , MMMM, yyyy";
                }
                else
                {
                    return "dd-MMM-yyyy";
                }
            }

        }

        /// <summary>
        /// Gets the permission sql city wise
        /// </summary>
        /// <value>
        /// </value>
        public static string L2IDLISTPERMISSIONWISE
        {
            get
            {
                return "select L2Id from employees_L2 where empID = " + ProjectSession.EmployeeID + " union Select L2Id from employees where employeeId = " + ProjectSession.EmployeeID;
            }

        }

        /// <summary>
        /// Gets the permission sql area wise
        /// </summary>
        /// <value>
        /// </value>
        public static string L3IDLISTPERMISSIONWISE
        {
            get
            {
                return "select L3Id from Employees_L3 where empID = " + ProjectSession.EmployeeID;
            }
        }

        /// <summary>
        /// Gets the permission sql location wise
        /// </summary>
        /// <value>
        /// </value>
        public static string LOCATIONIDLISTPERMISSIONWISE
        {
            get
            {
                return "select LocationID from Employees_Location where empID = " + ProjectSession.EmployeeID;
            }
        }

        /// <summary>
        /// Gets the permission sql Maintenance Sub Department wise
        /// </summary>
        /// <value>
        /// </value>
        public static string SUBDEPTIDLISTPERMISSIONWISE
        {
            get
            {
                return "select MaintSubDeptID from Employees_MaintSubDept where empID = " + ProjectSession.EmployeeID + "  union Select MaintSubDeptID from employees where employeeId = " + ProjectSession.EmployeeID;
            }

        }
    }
}
