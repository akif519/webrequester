﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Infrastructure
{
    public class ProjectSession
    {
        #region Properties

        /// <summary>
        /// current culture of user
        /// </summary>
        public static string Culture
        {
            get
            {
                if (HttpContext.Current.Session["Culture"] == null)
                {
                    return System.Configuration.ConfigurationManager.AppSettings["DefaultCulture"].ToString();
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["Culture"]);
                }
            }
            set
            {
                HttpContext.Current.Session["Culture"] = value;
            }
        }

        /// <summary>
        /// current culture of user
        /// </summary>
        public static string LayOutLanguage
        {
            get
            {
                if (HttpContext.Current.Session["LayOutLanguage"] == null)
                {
                    return ProjectConfiguration.EnglishLayOut;
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["LayOutLanguage"]);
                }
            }
            set
            {
                HttpContext.Current.Session["LayOutLanguage"] = value;
            }
        }

        /// <summary>
        /// current culture of user
        /// </summary>
        public static string CultureKendoClass
        {
            get
            {
                if (HttpContext.Current.Session["CultureKendoClass"] == null)
                {
                    if (ProjectSession.Culture == ProjectConfiguration.ArabicCultureCode)
                    {
                        return ProjectConfiguration.ArabicCultureKendoClass;
                    }
                    else
                    {
                        return ProjectConfiguration.EnglishCultureKendoClass;
                    }
                }
                else
                {
                    return HttpContext.Current.Session["CultureKendoClass"].ToString();
                }
            }
            set { HttpContext.Current.Session["CultureKendoClass"] = value; }

        }

        /// <summary>
        /// Gets or sets the direction.
        /// </summary>
        /// <value>
        /// The direction.
        /// </value>
        public static string Direction
        {
            get
            {
                if (HttpContext.Current.Session["Direction"] == null)
                {
                    return ProjectConfiguration.DirectionEng;
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["Direction"]);
                }
            }
            set
            {
                HttpContext.Current.Session["Direction"] = value;
            }
        }

        /// <summary>
        /// current theme of user
        /// </summary>
        public static string UserTheme
        {
            get
            {
                if (HttpContext.Current.Session["UserTheme"] == null)
                {
                    return ProjectConfiguration.Blue;
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UserTheme"]);
                }
            }
            set
            {
                HttpContext.Current.Session["UserTheme"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the date culture.
        /// </summary>
        /// <value>
        /// The date culture.
        /// </value>
        public static string DateCulture
        {
            get
            {
                if (HttpContext.Current.Session["DateCulture"] == null)
                {
                    return ProjectConfiguration.GregorianDate;
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["DateCulture"]);
                }
            }
            set
            {
                HttpContext.Current.Session["DateCulture"] = value;
            }
        }

        public static string ConnectionString
        {
            get
            {
                if (HttpContext.Current.Session["ConnectionString"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["ConnectionString"]);
                }
            }
            set
            {
                HttpContext.Current.Session["ConnectionString"] = value;
            }
        }

        /// <summary>
        /// Header Name in English
        /// </summary>
        public static string HeaderNameEn
        {
            get
            {
                if (String.IsNullOrEmpty(HttpContext.Current.Session["HeaderNameEn"].ToString()))
                {
                    return "NUB Facility Management System";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["HeaderNameEn"]);
                }
            }
            set
            {
                HttpContext.Current.Session["HeaderNameEn"] = value;
            }
        }

        /// <summary>
        /// Header Name in Arabic
        /// </summary>
        public static string HeaderNameAr
        {
            get
            {
                if (String.IsNullOrEmpty(HttpContext.Current.Session["HeaderNameAr"].ToString()))
                {
                    return "NUB Facility Management System";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["HeaderNameAr"]);
                }
            }
            set
            {
                HttpContext.Current.Session["HeaderNameAr"] = value;
            }
        }

        /// <summary>
        /// Header Logo in English
        /// </summary>
        public static string HeaderLogo
        {
            get
            {
                if (HttpContext.Current.Session["HeaderLogo"] == null || HttpContext.Current.Session["HeaderLogo"] == string.Empty)
                {
                    return "~/images/logo.png";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["HeaderLogo"]);
                }
            }
            set
            {
                HttpContext.Current.Session["HeaderLogo"] = value;
            }
        }

        /// <summary>
        /// Header Logo in English
        /// </summary>
        public static string UserImage
        {
            get
            {
                if (HttpContext.Current.Session["UserImage"] == null)
                {
                    return "~/images/default_user.jpg";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UserImage"]);
                }
            }
            set
            {
                HttpContext.Current.Session["UserImage"] = value;
            }
        }

        public static int DbType
        {
            get
            {
                if (HttpContext.Current.Session["DbType"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt16(HttpContext.Current.Session["DbType"]);
                }
            }
            set
            {
                HttpContext.Current.Session["DbType"] = value;
            }
        }

        public static int AccountSessionTimeout
        {
            get
            {
                if (HttpContext.Current.Session["AccountSessionTimeout"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt16(HttpContext.Current.Session["AccountSessionTimeout"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AccountSessionTimeout"] = value;
            }
        }


        public static int PageSize
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (Convert.ToInt32(HttpContext.Current.Session["PageSize"]) == 0)
                    {
                        HttpContext.Current.Session["PageSize"] = ProjectConfiguration.PageSize;
                    }

                    return Convert.ToInt32(HttpContext.Current.Session["PageSize"]);

                }
                else
                {
                    return 15;
                }
            }

            set
            {
                HttpContext.Current.Session["PageSize"] = value;
            }
        }

        public static string CurrentTheme
        {
            get
            {
                if (HttpContext.Current.Session["CurrentTheme"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["CurrentTheme"]);
                }
            }
            set
            {
                HttpContext.Current.Session["CurrentTheme"] = value;
            }
        }

        public static string EmployeeName
        {
            get
            {
                if (HttpContext.Current.Session["EmployeeName"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["EmployeeName"]);
                }
            }
            set
            {
                HttpContext.Current.Session["EmployeeName"] = value;
            }
        }

        public static string EmployeeNo
        {
            get
            {
                if (HttpContext.Current.Session["EmployeeNo"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["EmployeeNo"]);
                }
            }
            set
            {
                HttpContext.Current.Session["EmployeeNo"] = value;
            }
        }

        public static string AccountCode
        {
            get
            {
                if (HttpContext.Current.Session["AccountCode"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["AccountCode"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AccountCode"] = value;
            }
        }

        public static string UploadPath
        {
            get
            {
                if (HttpContext.Current.Session["UploadPath"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UploadPath"]); 
                }
            }
            set
            {
                HttpContext.Current.Session["UploadPath"] = HttpContext.Current.Server.MapPath("~/" + value + "/");
            }
        }

        public static string AllowedFileTypes
        {
            get
            {
                if (HttpContext.Current.Session["AllowedFileTypes"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["AllowedFileTypes"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AllowedFileTypes"] = value;
            }
        }

        public static int AllowedMaxFilesize
        {
            get
            {
                if (HttpContext.Current.Session["AllowedMaxFilesize"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["AllowedMaxFilesize"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AllowedMaxFilesize"] = value;
            }
        }



        public static int AccountID
        {
            get
            {
                if (HttpContext.Current.Session["AccountID"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["AccountID"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AccountID"] = value;
            }
        }

        public static int EmployeeID
        {
            get
            {
                if (HttpContext.Current.Session["EmployeeID"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["EmployeeID"]);
                }
            }
            set
            {
                HttpContext.Current.Session["EmployeeID"] = value;
            }
        }

        public static int DashboardAutoRefreshMinute
        {
            get
            {
                if (HttpContext.Current.Session["DashboardAutoRefreshMinute"] == null)
                {
                    return 1;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["DashboardAutoRefreshMinute"]);
                }
            }
            set
            {
                HttpContext.Current.Session["DashboardAutoRefreshMinute"] = value;
            }
        }

        public static bool IsItemNoAutoIncrement
        {
            get
            {
                if (HttpContext.Current.Session["IsItemNoAutoIncrement"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsItemNoAutoIncrement"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsItemNoAutoIncrement"] = value;
            }
        }

        public static bool UseMasterDB
        {
            get
            {
                if (HttpContext.Current.Session["UseMasterDB"] == null)
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["UseMasterDB"]);
                }
            }
            set
            {
                HttpContext.Current.Session["UseMasterDB"] = value;
            }
        }

        public static bool UseHelpDB
        {
            get
            {
                if (HttpContext.Current.Session["UseHelpDB"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["UseHelpDB"]);
                }
            }
            set
            {
                HttpContext.Current.Session["UseHelpDB"] = value;
            }
        }

        public static int SessionID
        {
            get
            {
                if (HttpContext.Current.Session["SessionID"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["SessionID"]);
                }
            }
            set
            {
                HttpContext.Current.Session["SessionID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for Resources
        /// </summary>
        public static CMMS.Service.Resources Resources
        {
            get
            {
                if (HttpContext.Current.Session["Resources"] == null)
                {
                    return null;
                }
                else
                {
                    return (CMMS.Service.Resources)HttpContext.Current.Session["Resources"];
                }
            }

            set
            {
                HttpContext.Current.Session["Resources"] = value;
            }
        }

        public static bool IsCentral
        {
            get
            {
                if (HttpContext.Current.Session["IsCentral"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsCentral"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsCentral"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for Resources
        /// </summary>
        public static CMMS.Model.PermissionAccess PermissionAccess
        {
            get
            {
                if (HttpContext.Current.Session["PermissionAccess"] == null)
                {
                    return new CMMS.Model.PermissionAccess();
                }
                else
                {
                    return (CMMS.Model.PermissionAccess)HttpContext.Current.Session["PermissionAccess"];
                }
            }

            set
            {
                HttpContext.Current.Session["PermissionAccess"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for Report Access
        /// </summary>
        public static CMMS.Model.ReportAccess ReportAccess
        {
            get
            {
                if (HttpContext.Current.Session["ReportAccess"] == null)
                {
                    return new CMMS.Model.ReportAccess();
                }
                else
                {
                    return (CMMS.Model.ReportAccess)HttpContext.Current.Session["ReportAccess"];
                }
            }

            set
            {
                HttpContext.Current.Session["ReportAccess"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for Tab Access
        /// </summary>
        public static CMMS.Model.TabAccess TabAccess
        {
            get
            {
                if (HttpContext.Current.Session["TabAccess"] == null)
                {
                    return new CMMS.Model.TabAccess();
                }
                else
                {
                    return (CMMS.Model.TabAccess)HttpContext.Current.Session["TabAccess"];
                }
            }

            set
            {
                HttpContext.Current.Session["TabAccess"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for Modules Access
        /// </summary>
        public static CMMS.Model.ModulesAccess ModulesAccess
        {
            get
            {
                if (HttpContext.Current.Session["ModulesAccess"] == null)
                {
                    return null;
                }
                else
                {
                    return (CMMS.Model.ModulesAccess)HttpContext.Current.Session["ModulesAccess"];
                }
            }

            set
            {
                HttpContext.Current.Session["ModulesAccess"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for FirstLevelTbl
        /// </summary>
        public static CMMS.Model.FirstLevelTbl FirstLevelTbl
        {
            get
            {
                if (HttpContext.Current.Session["FirstLevelTbl"] == null)
                {
                    return null;
                }
                else
                {
                    return (CMMS.Model.FirstLevelTbl)HttpContext.Current.Session["FirstLevelTbl"];
                }
            }

            set
            {
                HttpContext.Current.Session["FirstLevelTbl"] = value;
            }
        }

        /// <summary>
        /// Gets or sets Properties to store project session for FirstLevelTbl
        /// </summary>
        public static CMMS.Model.LevelNamesTbl LevelNamesTbl
        {
            get
            {
                if (HttpContext.Current.Session["LevelNamesTbl"] == null)
                {
                    return null;
                }
                else
                {
                    return (CMMS.Model.LevelNamesTbl)HttpContext.Current.Session["LevelNamesTbl"];
                }
            }

            set
            {
                HttpContext.Current.Session["LevelNamesTbl"] = value;
            }
        }
        public static bool IsAreaFieldsVisible
        {
            get
            {
                if (HttpContext.Current.Session["IsAreaFieldsVisible"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsAreaFieldsVisible"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsAreaFieldsVisible"] = value;
            }
        }

        public static bool IsCMProjectFieldsVisible
        {
            get
            {
                if (HttpContext.Current.Session["IsCMProjectFieldsVisible"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsCMProjectFieldsVisible"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsCMProjectFieldsVisible"] = value;
            }
        }

        public static bool IsCMProjectFieldMandatory
        {
            get
            {
                if (HttpContext.Current.Session["IsCMProjectFieldMandatory"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsCMProjectFieldMandatory"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsCMProjectFieldMandatory"] = value;
            }
        }

        public static bool IsAdvanceSearch
        {
            get
            {
                if (HttpContext.Current.Session["IsAdvanceSearch"] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(HttpContext.Current.Session["IsAdvanceSearch"]);
                }
            }
            set
            {
                HttpContext.Current.Session["IsAdvanceSearch"] = value;
            }
        }

        public static string AllowUploadFileFormats
        {
            get
            {
                if (HttpContext.Current.Session["AllowUploadFileFormats"] == null)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["AllowUploadFileFormats"] == null || Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AllowUploadFileFormats"]) == string.Empty)
                    {
                        return ".pdf,.doc,.docx,.xls,.xlsx,.jpeg,.jpg";
                    }
                    else
                    {
                        return System.Configuration.ConfigurationManager.AppSettings["AllowUploadFileFormats"].ToString();
                    }
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["AllowUploadFileFormats"]);
                }
            }
            set
            {
                HttpContext.Current.Session["AllowUploadFileFormats"] = value;
            }
        }

        public static IList<CMMS.Model.Configurations> LSTConfigs
        {
            get
            {
                if (HttpContext.Current.Session["LSTConfigs"] == null)
                {
                    return new List<CMMS.Model.Configurations>();
                }
                else
                {
                    return (List<CMMS.Model.Configurations>)HttpContext.Current.Session["LSTConfigs"];
                }
            }
            set
            {
                HttpContext.Current.Session["LSTConfigs"] = value;
            }
        }

        public static float? Latitude
        {
            get
            {
                if (HttpContext.Current.Session["Latitude"] == null)
                {
                    return null;
                }
                else
                {
                    return (float)(HttpContext.Current.Session["Latitude"]);
                }
            }
            set
            {
                HttpContext.Current.Session["Latitude"] = value;
            }
        }

        public static float? Longitude
        {
            get
            {
                if (HttpContext.Current.Session["Longitude"] == null)
                {
                    return null;
                }
                else
                {
                    return (float)(HttpContext.Current.Session["Longitude"]);
                }
            }
            set
            {
                HttpContext.Current.Session["Longitude"] = value;
            }
        }

        public static string WidhetClass
        {
            get
            {
                if (HttpContext.Current.Session["WidhetClass"] == null)
                {
                    return "col-lg-4";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["WidhetClass"]);
                }
            }
            set
            {
                HttpContext.Current.Session["WidhetClass"] = value;
            }
        }

        public static Dictionary<string, object> SaveSearchFilters
        {
            get
            {
                return HttpContext.Current.Session["SaveSearchFilters"] == null
                           ? new Dictionary<string, object>()
                           : (Dictionary<string, object>)HttpContext.Current.Session["SaveSearchFilters"];
            }
            set
            {
                HttpContext.Current.Session["SaveSearchFilters"] = value;
            }
        }

        public static string ErrorMessage
        {
            get
            {
                if (HttpContext.Current.Session["ErrorMessage"] == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["ErrorMessage"]);
                }
            }
            set
            {
                HttpContext.Current.Session["ErrorMessage"] = value;
            }
        }

        public static string CEMULToken
        {
            get
            {
                if (HttpContext.Current.Session["CEMULToken"] == null)
                {
                    return "http://www.clinicalequipmentusermanuallibrary.com/login/";
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["CEMULToken"]);
                }
            }
            set
            {
                HttpContext.Current.Session["CEMULToken"] = value;
            }
        }

        #endregion
    }

    public static class FilterHelper
    {
        public static object GetSearchFilterFromDictionary(string controllerName)
        {
            var obj = ProjectSession.SaveSearchFilters;
            if (!obj.ContainsKey(controllerName))
            {
                return null;
            }

            object model;
            obj.TryGetValue(controllerName, out model);
            return model;
        }

        public static void AddSearchFilterInDictionary(string controllerName, object model)
        {
            var obj = ProjectSession.SaveSearchFilters;
            obj.Clear();
            obj.Add(controllerName, model);
            ProjectSession.SaveSearchFilters = obj;
        }

        public static void ClearSearchFilter(string controllerName = "")
        {
            var obj = ProjectSession.SaveSearchFilters;
            obj.Clear();
            ProjectSession.SaveSearchFilters = obj;
        }
    }
}
