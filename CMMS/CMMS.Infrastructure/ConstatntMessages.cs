﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Infrastructure
{
    public class ConstatntMessages
    {
        public const string IncorrectUsernamePassword = "Username/Password do not match";
        public const string LoginSuccess = "Logged in Successfully";
        public const string LoginFail = "Logged in Fail";
        public const string IncorrectParameter = "Your enterd parameters are not correct";
        public const string ErrorMessage = " An error ocured while processing your request";
        public const string WrongPasswordMessage = "Username/Password do not match with the system";
        public const string SuccessPasswordMessage = "Password changed Successfully";
        public const string SuccessAddWRMessage = "New Work Request Added Successfully";
        public const string SuccessUpdateWRMessage = "Work Request updated Successfully";
        public const string SuccessWorkRequestStatus = "Work Request Status changed Successfully";
        public const string FileAlreadyExistMessage = "Your uploaded file already exists";
        public const string FileSuccessfullyUploadMessage = "File uploaded successfully";
        public const string FileSuccessfullyDeleteMessage = "File deleted successfully";
        public const string FileDeleteNotExistMessage = "File you want to delete do not exist";
        public const string FileNoAttachedMessage = "You have not attached file to upload";
        public const string SuccessUpdateWOMessage = "Work Order updated Successfully";
        public const string SuccessGeneratedWOMessage = "Work Order throgh work request Generated Successfully";
        public const string InActiveUserMessage = "Your username is not Active. Please contact your System Administrator";
        public const string SameUserAlreadyLoginMessage = "User is already logged on another device. Please try again later or contact your system administrator for assistance";
        public const string LogoutSuccessMessage = "Loggedout Successfully";
        public const string LogoutFailMessage = "Loggedout Fail";
        public const string ConlimitExceedMessage = "Connection limit has been exceed. Please contact the administrator";
        public const string DeptLimitExceedMessage = "Dept license limit has been exceeded. Please contact the administrator";
        public const string ForeignKeyViolation = "Foreign Key violation occurs with your attempt";
        public const string WorkOrderDoesNotExists = "Work Order does not exist";
    }
}
