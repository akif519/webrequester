﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CMMS.Infrastructure
{
    public static class Common
    {
        static Common()
        {

        }

        public static string FormatDateTime(DateTime? dt, string format)
        {
            try
            {
                string ConvertedDate = null;
                if (dt != null)
                {
                    ConvertedDate = Convert.ToDateTime(dt).ToString(format, System.Globalization.CultureInfo.CurrentCulture);
                }
                else
                {
                    return string.Empty;
                }
                return ConvertedDate;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public static object GetEnglishDate(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd HH:mm", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("yyyy/MM/dd HH:mm", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }

        public static object GetEnglishDateOnly(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("dd-MMM-yyyy", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }

        public static object GetArabicDateOnly(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("dd-yyyy-MMMM", new System.Globalization.CultureInfo("ar-SA").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("dd-yyyy-MMMM", new System.Globalization.CultureInfo("ar-SA").DateTimeFormat);
            }
            return ConvertedDate;
        }

        public static DateTime ParseDateInEnglishFormate(string date)
        {
            return DateTime.Parse(date, new System.Globalization.CultureInfo("en-US"));
        }

        public static object GetDateInDBFromat(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd HH:mm:ss");
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("yyyy/MM/dd HH:mm:ss");
            }
            return ConvertedDate;
        }

        public static object GetEnglishDateSecond(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd hh:mm:ss tt", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("yyyy/MM/dd hh:mm:ss tt", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }
        public static object GetEnglishDateWithoutTime(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }

            return ConvertedDate;
        }

        public static string setQuote(string value)
        {
            if (value != null && value != string.Empty)
            {
                return value.Replace("'", "''");
            }

            return string.Empty;
        }

        public static string GetConfigKeyValue(string key)
        {
            CMMS.Model.Configurations objConfig = new Model.Configurations();
            if (key != null && key != string.Empty)
            {
                objConfig = ProjectSession.LSTConfigs.Where(m => m.ConfigKey.ToUpper() == key.ToUpper()).FirstOrDefault();
            }

            if (objConfig != null && objConfig.ConfigValue != null)
            {
                return objConfig.ConfigValue;
            }
            return string.Empty;
        }

        public static List<SelectListItem> GetFrequencyUnit()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem() { Text = "Days (1)", Value = "1" });
            obj.Add(new SelectListItem() { Text = "Weeks (7)", Value = "7" });
            obj.Add(new SelectListItem() { Text = "Months (28)", Value = "28" });
            obj.Add(new SelectListItem() { Text = "Year (Hijri) (355)", Value = "355" });
            obj.Add(new SelectListItem() { Text = "Year (365)", Value = "365" });

            return obj;
        }

        public static List<SelectListItem> GetWeekOfMonth()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem() { Text = "First Week", Value = "1" });
            obj.Add(new SelectListItem() { Text = "Second Week", Value = "2" });
            obj.Add(new SelectListItem() { Text = "Third Week", Value = "3" });
            obj.Add(new SelectListItem() { Text = "Fourth Week", Value = "4" });
            obj.Add(new SelectListItem() { Text = "Last Week", Value = "5" });

            return obj;
        }

        public static List<SelectListItem> GetDayOfWeek()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem() { Text = "Sunday", Value = "1" });
            obj.Add(new SelectListItem() { Text = "Monday", Value = "2" });
            obj.Add(new SelectListItem() { Text = "Tuesday", Value = "3" });
            obj.Add(new SelectListItem() { Text = "Wednesday", Value = "4" });
            obj.Add(new SelectListItem() { Text = "Thursday", Value = "5" });
            obj.Add(new SelectListItem() { Text = "Friday", Value = "6" });
            obj.Add(new SelectListItem() { Text = "Saturday", Value = "7" });

            return obj;
        }

        public static List<SelectListItem> GetStartMonths()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            obj.Add(new SelectListItem() { Text = "January", Value = "1" });
            obj.Add(new SelectListItem() { Text = "February", Value = "2" });
            obj.Add(new SelectListItem() { Text = "March", Value = "3" });
            obj.Add(new SelectListItem() { Text = "April", Value = "4" });
            obj.Add(new SelectListItem() { Text = "May", Value = "5" });
            obj.Add(new SelectListItem() { Text = "June", Value = "6" });
            obj.Add(new SelectListItem() { Text = "July", Value = "7" });
            obj.Add(new SelectListItem() { Text = "August", Value = "8" });
            obj.Add(new SelectListItem() { Text = "September", Value = "9" });
            obj.Add(new SelectListItem() { Text = "October", Value = "10" });
            obj.Add(new SelectListItem() { Text = "November", Value = "11" });
            obj.Add(new SelectListItem() { Text = "December", Value = "12" });

            return obj;
        }

        public static List<SelectListItem> GetYearDropDown()
        {
            List<SelectListItem> obj = new List<SelectListItem>();
            int year = DateTime.Now.Year;

            for (int i = year - 10; i < year + 10; i++)
            {
                obj.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }

            return obj;
        }

        public static string GetDateRangeText(DateTime dateFrom, DateTime dateTo)
        {
            if (dateFrom == DateTime.MinValue || dateTo == DateTime.MinValue)
                return "";

            string str = ProjectSession.Resources.label.Purchasing_TextBlockFromDate + ": ";
            str += GetEnglishDateWithoutTime(dateFrom);
            str += "   ";
            str += ProjectSession.Resources.label.Purchasing_TextBlockToDate + ": ";
            str += GetEnglishDateWithoutTime(dateTo);

            return str;
        }

        public static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;

            string logFilePath = "C:\\Logs\\";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }  

    }
}
