﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Web.UI.WebControls.WebParts;

    /// <summary>
    /// This class is used to define all IO related operation
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    public class IOManager
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="IOManager" /> class.
        /// </summary>
        public IOManager()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Upload the File
        /// </summary>
        /// <param name="directoryPath">directory Path</param>
        /// <param name="fileUpload">file Upload</param>
        /// <param name="fileName">File Name</param>
        /// <returns>Return File Upload name</returns>
        public static string UploadFile(string directoryPath, HttpPostedFileBase fileUpload, string fileName = "")
        {
            try
            {
                if (fileUpload.ContentLength > 0)
                {
                    if (!System.IO.Directory.Exists(directoryPath))
                    {
                        System.IO.Directory.CreateDirectory(directoryPath);
                    }

                    if (fileName == string.Empty)
                    {
                        fileName = fileUpload.FileName;
                        if (fileName.IndexOf('\\') > -1)            //for IE
                            fileName = fileUpload.FileName.Substring(fileUpload.FileName.LastIndexOf('\\') + 1);
                        else if (fileName.IndexOf('/') > -1)
                            fileName = fileUpload.FileName.Substring(fileUpload.FileName.LastIndexOf('/') + 1);
                    }
                    else
                    {
                        fileName = fileName + fileUpload.FileName.Substring(fileUpload.FileName.LastIndexOf('.'));
                    }

                    fileName = fileName.Replace("%20", "_");

                    if (!File.Exists(directoryPath + fileName))
                    {
                        fileUpload.SaveAs(directoryPath + fileName);
                        return fileName;
                    }
                    else
                    {
                        string strNewFileName = fileName.Replace(".", "_" + DateTime.Today.Month.ToString() + "_" + DateTime.Today.Day.ToString() + "_" + DateTime.Today.Year.ToString()
                            + "_" + DateTime.Now.Hour.ToString() + "_" + DateTime.Now.Minute.ToString() + "_" + DateTime.Now.Second.ToString() + ".");

                        if (!File.Exists(directoryPath + strNewFileName))
                        {
                            fileUpload.SaveAs(directoryPath + strNewFileName);
                            return strNewFileName;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return "";
            }

            return "";
        }

        public static string SaveFile(byte[] content, string directoryPath, string filename)
        {
            try
            {
                if (content.Length > 0 && !string.IsNullOrEmpty(filename))
                {
                    if (!System.IO.Directory.Exists(directoryPath))
                    {
                        System.IO.Directory.CreateDirectory(directoryPath);
                    }
                    filename = filename.Replace("%20", "_");

                    //Save file
                    if (!File.Exists(directoryPath + filename))
                    {
                        using (System.IO.FileStream str = System.IO.File.Create(directoryPath + filename))
                        {
                            str.Write(content, 0, content.Length);
                        }

                        return filename;
                    }
                    else
                    {
                        string strNewFileName = filename;

                        if (!File.Exists(directoryPath + strNewFileName))
                        {
                            using (System.IO.FileStream str = System.IO.File.Create(directoryPath + strNewFileName))
                            {
                                str.Write(content, 0, content.Length);
                            }
                            return strNewFileName;
                        }
                    }

                }
            }
            catch (Exception)
            {
                return "";
            }

            return "";
        }

        public static byte[] CropImage(byte[] content, int x, int y, int width, int height)
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(content))
            {
                return CropImage(stream, x, y, width, height);
            }
        }

        public static byte[] CropImage(System.IO.Stream content, int x, int y, int width, int height)
        {
            //Parsing stream to bitmap
            using (System.Drawing.Bitmap sourceBitmap = new System.Drawing.Bitmap(content))
            {
                //Get new dimensions
                double sourceWidth = Convert.ToDouble(sourceBitmap.Size.Width);
                double sourceHeight = Convert.ToDouble(sourceBitmap.Size.Height);
                System.Drawing.Rectangle cropRect = new System.Drawing.Rectangle(x, y, width, height);

                //Creating new bitmap with valid dimensions
                using (System.Drawing.Bitmap newBitMap = new System.Drawing.Bitmap(cropRect.Width, cropRect.Height))
                {
                    using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(newBitMap))
                    {
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                        g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                        g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;

                        g.DrawImage(sourceBitmap, new System.Drawing.Rectangle(0, 0, newBitMap.Width, newBitMap.Height), cropRect, System.Drawing.GraphicsUnit.Pixel);

                        return GetBitmapBytes(newBitMap);
                    }
                }
            }
        }

        public static byte[] GetBitmapBytes(System.Drawing.Bitmap source)
        {
            //Settings to increase quality of the image
            System.Drawing.Imaging.ImageCodecInfo codec = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders()[4];
            System.Drawing.Imaging.EncoderParameters parameters = new System.Drawing.Imaging.EncoderParameters(1);
            parameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);

            //Temporary stream to save the bitmap
            using (System.IO.MemoryStream tmpStream = new System.IO.MemoryStream())
            {
                source.Save(tmpStream, codec, parameters);

                //Get image bytes from temporary stream
                byte[] result = new byte[tmpStream.Length];
                tmpStream.Seek(0, System.IO.SeekOrigin.Begin);
                tmpStream.Read(result, 0, (int)tmpStream.Length);

                return result;
            }
        }

        /// <summary>
        /// Create Directory
        /// </summary>
        /// <param name="directory">directory Name</param>
        /// <returns>Return Created Directory Status</returns>
        public static bool CreateDirectory(string directory)
        {
            try
            {
                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Create File
        /// </summary>
        /// <param name="fileName">file Name</param>
        /// <returns>Return File Created Status</returns>
        public static bool CreateFile(string fileName)
        {
            try
            {
                if (!System.IO.File.Exists(fileName))
                {
                    System.IO.File.Create(fileName);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Delete the supplied File
        /// </summary>
        /// <param name="fileName">file Name</param>
        /// <returns>Return File Delete Status</returns>
        public static bool DeleteFile(string fileName)
        {
            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        public static bool MoveFile(string fromPath, string toPath)
        {
            try
            {
                if (System.IO.File.Exists(fromPath))
                {
                    System.IO.File.Move(fromPath, toPath);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        /// <summary>
        /// Rename the supplied File
        /// </summary>
        /// <param name="oldFileName">old file Name</param>
        /// <param name="newFilename">new File name</param>
        /// <returns>Return File Rename Status</returns>
        public static bool RenameFile(string oldFileName, string newFilename)
        {
            try
            {
                if (File.Exists(newFilename))
                {
                    System.IO.File.Delete(newFilename);
                }

                System.IO.File.Move(oldFileName, newFilename);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Read the File and return the file content
        /// </summary>
        /// <param name="fileName">file Name</param>
        /// <returns>Return File Data</returns>
        public static string ReadFileData(string fileName)
        {
            string fileData = string.Empty;
            if (System.IO.File.Exists(fileName))
            {
                System.IO.StreamReader streamContent = new System.IO.StreamReader(fileName);
                fileData = streamContent.ReadToEnd();
                streamContent.Close();
                streamContent.Dispose();
                streamContent = null;
            }

            return fileData;
        }

        /// <summary>
        /// Function converts map path file in byte array
        /// </summary>
        /// <param name="filename">file name</param>
        /// <returns>Return file data as byte</returns>
        public static byte[] StreamFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] imageData = new byte[fs.Length];
            fs.Read(imageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return imageData;
        }

        /// <summary>
        /// Write Data into File
        /// </summary>
        /// <param name="filepath">file path</param>
        /// <param name="content">File content</param>
        /// <returns>Write data to file</returns>
        public static bool WriteToFile(string filepath, string content)
        {
            try
            {
                File.WriteAllText(filepath, content);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Delete all files from directory.
        /// </summary>
        /// <param name="directoryName">Directory Name</param>
        /// <returns>Return Files deleted Status</returns>
        public static bool DeleteAllfilesFromDirectory(string directoryName)
        {
            try
            {
                if (Directory.Exists(directoryName))
                {
                    string[] files = Directory.GetFiles(directoryName);
                    foreach (string file in files)
                    {
                        if (File.Exists(file) && File.GetCreationTime(file) < DateTime.Today.AddHours(-1))
                        {
                            File.Delete(file);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
