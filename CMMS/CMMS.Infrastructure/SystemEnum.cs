﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.Infrastructure
{
    public class SystemEnum
    {
        #region "Enum"
        public enum DBTYPE
        {
            SQL = 1,
            MYSQL = 2,
            ORACLE = 3
        }

        public enum Pages
        {
            Sector = 1,
            MaintenanceDivision = 2,
            MaintenanceDepartment = 3,
            MaintenanceSubDepartment = 4,
            MaintenanceGroup = 5,
            Zone = 6,
            FailureCause = 7,
            Supplier = 8,
            CostCenter = 9,
            EmployeeCategory = 10,
            SiteClass = 11,
            AssetCategories = 12,
            AssetClass = 13,
            AssetCondition = 14,
            AssetStatus = 15,
            Criticality = 16,
            WarrantyContract = 17,
            EmployeeStatus = 18,
            AssetSubCategories = 19,
            ItemType = 20,
            ItemSubType = 21,
            Stores = 22,
            ItemSection = 23,
            JobPriority = 24,
            JobStatus = 25,
            JobType = 26,
            JobTrade = 27,
            ActiveSession = 28,
            IRReqStatus = 29,
            IRStatus = 30,
            IRApprovalLevels = 31,
            LoginHistory = 32,
            UserGroup = 33,
            PRStatus = 34,
            PRAuthStatus = 35,
            PRApprovalLevels = 36,
            IRWorkFlow = 37,
            AssetList = 38,
            BoMList = 39,
            Site = 40,
            Building = 41,
            CleaningGroup = 42,
            CleaningClassification = 43,
            Employee = 44,
            EmployeeTransaction = 441,
            JobPlan = 45,
            JobPlanItem = 46,
            SafetyInstruction = 47,
            PRWorkFlow = 48,
            LanguageTranslation = 49,
            AccountCode = 50,
            EmailTemplates = 51,
            EmailNotificationRules = 52,
            PurchasingCurrency = 53,
            PurchasingTermofSale = 54,
            PurchasingShippedVia = 55,
            PurchasingPaymentTerms = 56,
            Organisation = 57,
            Area = 58,
            JobRequest = 59,
            AssetTree = 60,
            PurchasingTaxes = 61,
            CleaningWeightage = 62,
            Section = 63,
            PMSchedule = 64,
            JobOrder = 65,
            PurchaseRequestItem = 66,
            PurchaseOrderItem = 67,
            PMScheduleByAsset = 68,
            DirectIssueList = 69,
            EmailCongifuration = 70,
            SMSCongifuration = 71,
            CEMUL = 72,
            PMCheckList = 73,
            PMMeterMasterList = 74,
            PMMeterList = 75,
            AssetDetailsByAssetID = 76,
            PMMeterReadingList = 77,
            PMMeterReadingSchedule = 78,
            PMMetersJOGenerate = 79,
            PMJoGeneration = 80,
            PurchaseRequest = 81,
            PurchaseOrder = 82,
            GoodReceipt = 83,
            Dashboard = 84,
            ToolCategory = 85,
            ToolSubCategory = 86,
            Tool = 87,
            LocationType = 88,
            DataBaseAudit = 89,
            ConsumableList = 90,
            PersonalList = 91,
            StockCodesList = 92,
            CleaningInspectionList = 93,
            PMCheckListForCleaning = 94,
            CleaningSchedule = 95,
            CleaningList = 96,
            CleaningInspectionGroupList = 97,
            CIGSequenceList = 98,
            ErrorLog = 99,
            HelpList = 100,
            TransactionList = 101,
            Specification = 102,
            ItemRequisitionList = 103,
            GeneralConfiguration = 104,
            HolidaySetting = 105,
            ReportOrganizationDetails = 106,
            RptAssetList = 107,
            RptAssetDetails = 108,
            RptAssetMaintenanceHistory = 109,
            RptTop10AssetDowntime = 110,
            RptTop10AssetBreakdowns = 111,
            RptAssetsMTBF = 112,
            RptAssetBreakdownFrequency = 113,
            RptAssetBreakdownDowntime = 114,
            RptManHoursByAsset = 115,
            RptAssetMTTR = 116,
            RptAssetMaintHistorynocost = 117,
            RptJobOrderList = 118,
            RptJobOrderStatusStatistics = 119,
            RptJobOrdersAging = 120,
            RptJobOrderCostByLocationChart = 121,
            RptAllJobOrderStatus = 122,
            RptAllJobOrderStatusChart = 123,
            RptFailurePercentageAnalysisChart = 124,
            RptJobOrderMan_HoursByLocationChart = 125,
            RptJobOrderCostbyJobTypeChart = 126,
            RptJobOrderCostbyMaintDivDeptSubDept = 127,
            RptJobOrderDistributionChart = 128,
            RptFailureDowntimeAnaylsisChart = 129,
            RptBreakdownFailureSummary = 130,
            RptJobOrderCostByCostCenter = 131,
            RptJobOrdersByEmployees = 132,
            RptEmployeeJoborderManHours = 133,
            RptJobOrderRatingsReport = 134,
            RptPMJobOrderAgingList = 135,
            RptPMJobOrderStatus = 136,
            RptPMJobOrderList = 137,
            RptPMJobOrderStatusChart = 138,
            RptJOCount = 139,
            RptPMChecklistList = 140,
            RptPMChecklistListTask = 141,
            RptPMCompliance = 142,
            RptPMMaterialRequirements = 143,
            RptAnnualPMSchedule = 144,
            RptItemList = 145,
            RptStockTakeList = 146,
            RptStockBalanceList = 147,
            RptTransactionList = 148,
            RptTransactionPartReceiveQtyList = 149,
            RptTransactionPartIssueQtyList = 150,
            RptTransactionPartReceiveTotalCostList = 151,
            RptTransactionPartIssueTotalCostList = 152,
            RptPurchaseProposalNormalList = 153,
            RptPurchaseProposalCriticalList = 154,
            RptDormantParts = 155,
            RptItemIssueByWorkOrder = 156,
            RptRequesterList = 157,
            RptRequesterDetails = 158,
            RptPhysicalLocationList = 159,
            RptSupplierList = 160,
            RptPurchaseRequest = 161,
            RptPurchaseRequestDetailsList = 162,
            RptPurchaseOrderList = 163,
            RptPurchaseOrderDetailsList = 164,
            RptPurchaseOrderCost = 165,
            RptCleaningInspection = 166,
            RptCleaningInspectionDetailsList = 167,
            ClientList = 168,
            AdminList = 169,
            AssetCategoryNotification = 200,
            CriticalityNotification = 201,
            JOStatusNotification = 202,
            JRStatusNotification = 203,
            LocationTypeNotification = 204,
            WorkPriorityNotification = 205,
            WorkTypeNotification = 206,
            JobRequestEscalationList = 207,
            JobOrderEscalationList = 208,
            CleaningJoGeneration = 209,
            NotificationEmployeeList = 210,
            ViewAllNotificationList = 211,
            NotificationTypes = 212
        }


        public enum WebServiceModuleType
        {
            [Description("J")]
            WorkOrder = 1,
            [Description("R")]
            WorkRequest = 2,
            [Description("A")]
            Asset = 3,

        }

        public enum MessageType
        {
            /// <summary>
            /// for Success message Class
            /// </summary>
            success,

            /// <summary>
            /// for error message Class
            /// </summary>
            danger,

            /// <summary>
            /// for Warning message Class
            /// </summary>
            warning
        }

        public enum SupplierContractorCategory
        {
            [Description("ممون")]
            Supplier = 1,
            [Description("مقاول")]
            Contractor = 2,
            [Description("مقاول فرعي")]
            SubContractor = 3,
            [Description("على حد سواء")]
            Both = 0
        }

        public enum SupplierClassificationLevel
        {
            Level1 = 1,
            Level2 = 2,
            Level3 = 3,
            Level4 = 4,
            Level5 = 5
        }

        public enum SearchDBType
        {
            String = 1,
            Int = 2,
            Numeric = 3,
            Date = 4,
            ForeignKey = 5
        }

        public enum SearchFieldsOperator
        {
            IsEqualTo = 1,
            IsNotEqualTo = 2,
            GreaterThan = 3,
            LessThan = 4,
            Between = 5,
            Contains = 6,
            StartsWith = 7,
            GreaterThanEqualTo = 8,
            LessThanEqualTo = 9,
            NotContains = 10
        }

        public enum Status
        {
            [Description("InActive")]
            NotActive = 0,
            [Description("Active")]
            Active = 1
        }

        public enum WarrentyStatus
        {
            [Description("Not Expired")]
            NotExpired = 0,
            [Description("ساري المفعول")]
            NotExpiredArabic = 1,
            [Description("Expired")]
            Expired = 2,
            [Description("منتهية الصلاحية")]
            ExpiredArabic = 3
        }

        public enum DefaultCalendar
        {
            [Description("Gregorian")]
            Gregorian = 0,
            [Description("Hijri")]
            Hijri = 1
        }

        public enum TimeZone
        {
            [Description("UTC -12:00")]
            UTC12AM = -12,
            [Description("UTC -11:00")]
            UTC11AM = -11,
            [Description("UTC -10:00")]
            UTC10AM = -10,
            [Description("UTC -09:00")]
            UTC9AM = -9,
            [Description("UTC -08:00")]
            UTC8AM = -8,
            [Description("UTC -07:00")]
            UTC7AM = -7,
            [Description("UTC -06:00")]
            UTC6AM = -6,
            [Description("UTC -05:00")]
            UTC5AM = -5,
            [Description("UTC -04:00")]
            UTC4AM = -4,
            [Description("UTC -03:00")]
            UTC3AM = -3,
            [Description("UTC -02:00")]
            UTC2AM = -2,
            [Description("UTC -01:00")]
            UTC1AM = -1,
            [Description("UTC 01:00")]
            UTC1PM = 1,
            [Description("UTC 02:00")]
            UTC2PM = 2,
            [Description("UTC 03:00")]
            UTC3PM = 3,
            [Description("UTC 04:00")]
            UTC4PM = 4,
            [Description("UTC 05:00")]
            UTC5PM = 5,
            [Description("UTC 06:00")]
            UTC6PM = 6,
            [Description("UTC 07:00")]
            UTC7PM = 7,
            [Description("UTC 08:00")]
            UTC8PM = 8,
            [Description("UTC 09:00")]
            UTC9PM = 9,
            [Description("UTC 10:00")]
            UTC10PM = 10,
            [Description("UTC 11:00")]
            UTC11PM = 11,
            [Description("UTC 12:00")]
            UTC12PM = 12,
        }

        public enum DisplayInModule
        {
            [Description("No")]
            No = 0,
            [Description("Yes")]
            Yes = 1
        }

        public enum ModuleType
        {
            [Description("Project")]
            Project,
            [Description("Letter")]
            Letter,
            [Description("Supplier")]
            Supplier,
            [Description("Employee")]
            Employee,
            [Description("ContractTemplate")]
            ContractTemplate,
            [Description("SubContractor")]
            SubContractor,
            [Description("BOQInvoice")]
            BOQInvoice,
            [Description("BOQMInvoice")]
            BOQMInvoice,
            [Description("ContractorProposal")]
            ContractorProposal
        }

        public enum DocumentModuleType
        {
            [Description("Asset")]
            A,

            [Description("JobRequest")]
            R,

            [Description("Preventive")]
            P,

            [Description("JobOrder")]
            J,

            [Description("Location")]
            L,

            [Description("LocationDiagram")]
            D,

            [Description("Consumables")]
            C,

            [Description("StoreItems")]
            S,

            [Description("PurchaseRequest")]
            PR,

            [Description("PurchaseOrder")]
            PO,

        }

        public enum TemplateFieldType
        {
            [Description("WO")]
            WorkOrder,

            [Description("WR")]
            WorkRequest,

            [Description("Material")]
            Material,
        }

        public enum ModuleCode
        {
            [Description("Work Request")]
            WorkRequest = 100,

            [Description("Work Order")]
            WorkOrder = 101,

            [Description("Materials")]
            Materials = 102,

            [Description("Supplier")]
            Supplier = 103
        }

        public enum ModuleEvent
        {
            [Description("Material Module")]
            Material = 0,

            [Description("Work Request Creation")]
            JobRequestCreation = 200,

            [Description("Work Request Status Change")]
            JobRequestJobStatusChange = 201,

            [Description("Work Request Escalation")]
            JobRequestOpenStatusEscalation = 202,

            [Description("Work Order Creation")]
            JobOrderCreation = 211,

            [Description("Work Order Status Change")]
            JobOrderJobStatusChange = 212,

            [Description("Work Order Escalation")]
            JobOrderOpenStatusEscalation = 213,

            [Description("Supplier Module")]
            Supplier = 214,
        }

        public enum WorkRequestStatus
        {
            NotSet = 0,
            Open = 1,
            Closed = 2,
            Cancelled = 3,
            WaitingforParts = 4,
            Approval = 5,
            WaitingforAccess = 6,
            Completed = 7,
            WORaised = 8
        }

        public enum WorkType
        {
            Corrective = 1,
            Preventive = 2,
            Breakdown = 3,
            Projects = 4,
            Reimbursable = 5,
            Inspection = 6,
            ManualPreventive = 7,
            Predictive = 8
        }

        public enum DashboardModule
        {
            [Description("PM Job Order Status")]
            PMJobOrderStatus = 1,
            [Description("Job Order Status")]
            JobOrderStatus = 2,
            [Description("Job Order Type Distribution")]
            JobOrderTypeDistribution = 3,
            [Description("PM Compilance")]
            PMCompilance = 4,
            [Description("Top 10 Asset Breakdown")]
            Top10AssetBreakdown = 5,
            [Description("Top 10 Asset Downtime")]
            Top10AssetDowntime = 6,
            [Description("Job Order & Request Trending")]
            JobOrderAndRequestTrending = 7,
            [Description("No Of Job Orders By Type")]
            NoOfJobOrdersByType = 8,
            [Description("Job Order Status By Site")]
            JobOrderStatusBySite = 9,
            [Description("Job Priority Distribution")]
            JobPriorityDistribution = 10,
            [Description("Job Trade Distribution")]
            JobTradeDistribution = 11,
            [Description("Outstanding Jobs By Priority")]
            OutstandingJobsByPriority = 12,
            [Description("Outstanding Jobs By Trade")]
            OutstandingJobsByTrade = 13,
            [Description("Outstanding Jobs By Type")]
            OutstandingJobsByType = 14,
            [Description("Work Status Report")]
            WorkStatusReport = 15
        }

        public enum JobOrderStatusCriteria
        {
            [Description("ComboBoxCityCode")]
            ComboBoxCityCode = 1,
            [Description("MultiAreaUserControl")]
            MultiAreaUserControl = 2,
            [Description("MultiStreetUserControl")]
            MultiStreetUserControl = 3,
            [Description("MultiBuildingUserControl")]
            MultiBuildingUserControl = 4,
            [Description("ComboBoxMaintDiv")]
            ComboBoxMaintDiv = 5,
            [Description("MultiMaintDeptUserControl")]
            MultiMaintDeptUserControl = 6,
            [Description("MultiMaintSubDeptUserControl")]
            MultiMaintSubDeptUserControl = 7,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 8,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 9,
            [Description("MultiJobTypeUserControl")]
            MultiJobTypeUserControl = 10,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 11
        }

        public enum JobOrderStatusBySiteCriteria
        {
            [Description("MultiCityUserControl")]
            MultiCityUserControl = 1,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 2,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 3,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 4
        }

        public enum TopTenAssetCriteria
        {
            [Description("ComboBoxCityCode")]
            ComboBoxCityCode = 1,
            [Description("MultiAreaUserControl")]
            MultiAreaUserControl = 2,
            [Description("MultiStreetUserControl")]
            MultiStreetUserControl = 3,
            [Description("MultiBuildingUserControl")]
            MultiBuildingUserControl = 4,
            [Description("TextBoxLocationNo")]
            TextBoxLocationNo = 5,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 6,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 7,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 8
        }

        public enum PMComplianceCriteria
        {
            [Description("ComboBoxCityCode")]
            ComboBoxCityCode = 1,
            [Description("MultiAreaUserControl")]
            MultiAreaUserControl = 2,
            [Description("MultiStreetUserControl")]
            MultiStreetUserControl = 3,
            [Description("MultiBuildingUserControl")]
            MultiBuildingUserControl = 4,
            [Description("ComboBoxMaintDiv")]
            ComboBoxMaintDiv = 5,
            [Description("MultiMaintDeptUserControl")]
            MultiMaintDeptUserControl = 6,
            [Description("MultiMaintSubDeptUserControl")]
            MultiMaintSubDeptUserControl = 7,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 8,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 9,
            [Description("MultiJobTradeUserControl")]
            MultiJobTradeUserControl = 10,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 11
        }

        public enum JobTradingCriteria
        {
            [Description("ComboBoxCityCode")]
            ComboBoxCityCode = 1,
            [Description("MultiAreaUserControl")]
            MultiAreaUserControl = 2,
            [Description("MultiStreetUserControl")]
            MultiStreetUserControl = 3,
            [Description("MultiBuildingUserControl")]
            MultiBuildingUserControl = 4,
            [Description("ComboBoxMaintDiv")]
            ComboBoxMaintDiv = 5,
            [Description("MultiMaintDeptUserControl")]
            MultiMaintDeptUserControl = 6,
            [Description("MultiMaintSubDeptUserControl")]
            MultiMaintSubDeptUserControl = 7,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 8,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 9,
            [Description("MultiJobTradeUserControl")]
            MultiJobTradeUserControl = 10,
            [Description("MultiJobTypeUserControl")]
            MultiJobTypeUserControl = 11,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 12
        }

        public enum WorkStatusReportCriteria
        {
            [Description("ComboBoxCityCode")]
            ComboBoxCityCode = 1,
            [Description("MultiAreaUserControl")]
            MultiAreaUserControl = 2,
            [Description("MultiStreetUserControl")]
            MultiStreetUserControl = 3,
            [Description("MultiBuildingUserControl")]
            MultiBuildingUserControl = 4,
            [Description("ComboBoxMaintDiv")]
            ComboBoxMaintDiv = 5,
            [Description("MultiMaintDeptUserControl")]
            MultiMaintDeptUserControl = 6,
            [Description("MultiMaintSubDeptUserControl")]
            MultiMaintSubDeptUserControl = 7,
            [Description("MultiJobTradeUserControl")]
            MultiJobTradeUserControl = 8,
            [Description("MultiJobTypeUserControl")]
            MultiJobTypeUserControl = 9,
            [Description("DatePickerJODateFrom")]
            DatePickerJODateFrom = 10,
            [Description("DatePickerJODateTo")]
            DatePickerJODateTo = 11,
            [Description("ComboBoxDatePicker")]
            ComboBoxDatePicker = 12,
            [Description("ComboBoxOrg")]
            ComboBoxOrg = 13
        }

        public enum DatabaseAuditTableName
        {
            L1,
            L2,
            L3,
            L4,
            L5
        }

        public enum ModulesAccess
        {
            JobRequest = 1,
            JobOrder = 2,
            Assets = 3,
            Locations = 4,
            PreventiveMaintenance = 5,
            Store = 6,
            Purchasing = 7,
            Employee = 8,
            Cleaning = 9,
            CEUML = 10,
            AuditTrail = 11,
            Sector = 12,
            Zone = 13,
            Building = 14,
            Division = 15,
            Department = 16,
            SubDept = 17,
            Groups = 18,
            FailureCodes = 19,
            Suppliers_Contractors = 20,
            BoMList = 21,
            CostCenter = 22,
            AccountCode = 23,
            JobPlans = 24,
            SafetyInstructions = 25,
            SiteSetups = 26,
            StoreSetups = 27,
            PurchaseRequisition = 28,
            JobManagement = 29,
            AssetSetup = 30,
            Employee_User = 31,
            CleaningSetup = 32,
            PurchaseSetup = 33,
            NotificationSetup = 34,
            SLA = 35,
        }

        public enum TransactionType
        {
            [Description("Receive")]
            Receive = 1,
            [Description("Issue")]
            Issue = 2,
            [Description("Return")]
            Return = 3,
            [Description("Adjustment")]
            Adjustment = 4,
            [Description("Transfer")]
            Transfer = 5,
            [Description("Purchase Proposal")]
            PurchaseProposal = 6
        }

        public enum TransactionStatus
        {
            [Description("Active")]
            Active = 1,
            [Description("Cancelled")]
            Cancelled = 2
        }

        public enum WeekDays
        {
            [Description("Monday")]
            Monday = 1,
            [Description("Tuesday")]
            Tuesday = 2,
            [Description("Wednesday")]
            Wednesday = 3,
            [Description("Thursday")]
            Thursday = 4,
            [Description("Friday")]
            Friday = 5,
            [Description("Saturday")]
            Saturday = 6,
            [Description("Sunday")]
            Sunday = 7
        }

        public enum employeeStatus
        {
            Active = 1,
            Terminated = 2,
            InActive = 3,
            Retired = 4,
        }

        public enum NotificationType
        {
            AssetCategoryNotification = 1,
            CriticalityNotification = 2,
            LocationTypeNotification = 3,
            WorkPriorityNotification = 4,
            WorkTypeNotification = 5,
            JOStatusNotification = 6,
            JRStatusNotification = 7,
        }

        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the ID value.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Name value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        public string Description { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Get the Enum Name from Enum Value
        /// </summary>
        /// <param name="objEnumType">Enum Type like typeof(EnumType)</param>
        /// <param name="value">enum value</param>
        /// <returns>string - Name of Enum</returns>
        public static string GetEnumName(Type objEnumType, int value)
        {
            SystemEnumList lstEnum = GetEnumList(objEnumType);
            SystemEnum objSystemEnum;
            objSystemEnum = lstEnum.Find(delegate(SystemEnum systemEnum)
            {
                return systemEnum.ID == value;
            });

            if (objSystemEnum != null)
            {
                return string.IsNullOrEmpty(objSystemEnum.Description) ? objSystemEnum.Name : objSystemEnum.Description;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get the SystemEnumList from given Enum type
        /// </summary>
        /// <param name="objEnumType">Enum Type like typeof(EnumType)</param>
        /// <returns>List of Enum with Name Value pair</returns>
        public static SystemEnumList GetEnumList(Type objEnumType)
        {
            Array values = Enum.GetValues(objEnumType);
            SystemEnumList lstEnum = new SystemEnumList();
            SystemEnum objEnum;
            for (int i = 0; i < values.Length; i++)
            {
                objEnum = new SystemEnum();
                objEnum.ID = values.GetValue(i).GetHashCode();
                objEnum.Name = Convert.ToString(values.GetValue(i), CultureInfo.CurrentCulture).Replace("_", " ");
                objEnum.Description = string.Empty;

                var memInfo = objEnumType.GetMember(Convert.ToString(values.GetValue(i), CultureInfo.CurrentCulture));
                var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attributes.Count() > 0)
                {
                    objEnum.Description = ((DescriptionAttribute)attributes[0]).Description;
                }

                lstEnum.Add(objEnum);
            }

            return lstEnum;
        }


        #endregion
    }

    public class SystemEnumList : List<SystemEnum>
    {
    }
}
