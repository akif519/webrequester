﻿////-----------------------------------------------------------------------
// <copyright file="DBExecute.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    //using OverlayPlatform.Infrastructure;
    using Microsoft.VisualBasic;
    using System.IO;

    /// <summary>
    /// This class is used to define Execute Database query
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    public abstract class DBExecute
    {

        public IDbConnection Connection { get; set; }

        private IDbTransaction _Transaction { get; set; }

        /// <summary>
        /// Gets Default Page Size
        /// </summary>
        public int DefaultPageSize
        {
            get
            {
                return 10;
            }
        }

        /// <summary>
        /// Gets Default Page Size
        /// </summary>
        public int DefaultPagerSize
        {
            get
            {
                return 5;
            }
        }


        /// <summary>
        /// Gets or sets Pagination Information
        /// </summary>
        public virtual Pagination PagingInformation { get; set; }

        /// <summary>
        /// Execute Procedure for custom methods
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="executeType">execute type</param>
        /// <param name="parameters">parameter list</param>
        /// <returns>return procedure output</returns>
        public virtual object ExecuteProcedure(string procedureName, ExecuteType executeType, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            try { return DBClient.ExecuteProcedure(procedureName, executeType, parameters); }
            catch (Exception ex) { throw ex; }

        }

        /// <summary>
        /// Executes the procedurewith out parameter.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="executeType">Type of the execute.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        public virtual object ExecuteProcedurewithOutParameter(string procedureName, ExecuteType executeType, System.Collections.ObjectModel.Collection<DBParameters> parameters, out Dictionary<string, string> dic)
        {
            try { return DBClient.ExecuteProcedurewithOutParameter(procedureName, executeType, parameters, out dic); }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Execute Procedure for custom methods
        /// </summary>
        /// <typeparam name="TEntity">Entity Type which require to be return as list</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">parameter list</param>
        /// <returns>return procedure output</returns>
        public virtual IList<TEntity> ExecuteProcedure<TEntity>(string procedureName, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            try
            {
                var list = DBClient.ExecuteProcedure<TEntity>(procedureName, parameters);
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(Convert.ToInt32(GetPropertyValue(list[0], "TotalRecords"), CultureInfo.InvariantCulture));
                }

                return list;
            }
            catch (Exception ex) { throw ex; }

        }

        /// <summary>
        /// Execute Procedure for custom methods
        /// </summary>
        /// <typeparam name="TEntity">Entity Type which require to be return as list</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">parameter list</param>
        /// <returns>return procedure output</returns>
        public virtual IList<TEntity> ExecuteQuery<TEntity>(string strQuery, System.Collections.ObjectModel.Collection<DBParameters> parameters, int? pageNo)
        {
            try
            {
                var list = DapperDBClient.ExecuteQuery<TEntity>(strQuery, parameters);
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(list.Count());
                }

                if (pageNo > 0)
                {
                    return list.Skip(this.StartRowIndex(pageNo) - 1).Take(this.PagingInformation.PageSize).ToList();
                }
                else
                {
                    return list;
                }
            }
            catch (Exception ex) { throw ex; }

        }

        public virtual IList<TEntity> ExecuteProcedurewithOutParameter<TEntity>(string procedureName, System.Collections.ObjectModel.Collection<DBParameters> parameters, out Dictionary<string, string> dictionary)
        {

            try
            {
                var list = DBClient.ExecuteProcedurewithOutParameter<TEntity>(procedureName, parameters, out dictionary);
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(Convert.ToInt32(GetPropertyValue(list[0], "TotalRecords"), CultureInfo.InvariantCulture));
                }

                return list;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Get Property Value of given Property Name
        /// </summary>
        /// <typeparam name="TEntity">Entity Type</typeparam>
        /// <param name="entity">Model to Extract</param>
        /// <param name="propertyName">property name</param>
        /// <returns>Table Name</returns>
        protected static string GetPropertyValue<TEntity>(TEntity entity, string propertyName)
        {
            try
            {
                PropertyInfo info = entity.GetType().GetProperties().FirstOrDefault(prop => prop.Name.ToLower(System.Globalization.CultureInfo.CurrentCulture) == propertyName.ToLower(System.Globalization.CultureInfo.CurrentCulture));

                if (info != null)
                {
                    if (info.PropertyType == typeof(string))
                    {
                        return Convert.ToString(info.GetValue(entity, null), CultureInfo.InvariantCulture).Trim();
                    }

                    return Convert.ToString(info.GetValue(entity, null), CultureInfo.InvariantCulture);
                }

                return string.Empty;
            }
            catch (Exception)
            {
                Console.WriteLine("There is an error in getting property value for " + propertyName);
                throw;
            }
        }

        /// <summary>
        /// Gets or sets Start Row Index for Current Page
        /// </summary>
        /// <param name="pageNo">page no</param>
        /// <returns>return start row index</returns>
        protected int StartRowIndex(int? pageNo)
        {
            try
            {
                if (pageNo.HasValue && pageNo > 0)
                {
                    this.PagingInformation.PageNo = pageNo.Value;
                    return ((pageNo.Value - 1) * this.PagingInformation.PageSize) + 1;
                }

                return 0;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// End Row Index for Current Page
        /// </summary>
        /// <param name="pageNo">page no</param>
        /// <returns>returns end row index</returns>
        protected int EndRowIndex(int? pageNo)
        {

            try
            {
                if (pageNo.HasValue && pageNo > 0)
                {
                    return ((pageNo.Value - 1) * this.PagingInformation.PageSize) + this.PagingInformation.PageSize;
                }

                return 0;
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Set Pagination Information
        /// </summary>
        /// <param name="totalRecords">total records</param>
        private void SetPaginationInformation(int totalRecords)
        {
            try
            {
                this.PagingInformation.TotalRecords = totalRecords;
                this.PagingInformation.TotalPages = (this.PagingInformation.TotalRecords / this.PagingInformation.PageSize) + ((this.PagingInformation.TotalRecords % this.PagingInformation.PageSize > 0) ? 1 : 0);
                this.PagingInformation.HasPreviousPage = this.PagingInformation.PageNo > this.PagingInformation.PagerSize;
                int currentPagerNo = this.PagingInformation.PageNo / (this.PagingInformation.PagerSize + (this.PagingInformation.PageNo % this.PagingInformation.PagerSize > 0 ? 1 : 0));
                int currentPagerRecords = currentPagerNo * this.PagingInformation.PagerSize;
                this.PagingInformation.HasNextPage = this.PagingInformation.TotalPages > this.PagingInformation.PagerSize ? ((this.PagingInformation.TotalPages % this.PagingInformation.PagerSize) == 0 ? (currentPagerRecords < this.PagingInformation.TotalPages - (this.PagingInformation.TotalPages % this.PagingInformation.PagerSize)) : (currentPagerRecords <= this.PagingInformation.TotalPages - (this.PagingInformation.TotalPages % this.PagingInformation.PagerSize))) : false;
            }
            catch (Exception ex) { throw ex; }

        }

        public IList<TEntity> ApplyPagging<TEntity>(IList<TEntity> list, int? pageNo)
        {
            try
            {
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(list.Count());
                }

                if (pageNo > 0)
                {
                    return list.Skip(this.StartRowIndex(pageNo) - 1).Take(this.PagingInformation.PageSize).ToList();
                }
                else
                {
                    return list;
                }
            }
            catch (Exception ex) { throw ex; }

        }

        /// <summary>
        /// Execute Procedure for custom methods
        /// </summary>
        /// <typeparam name="TEntity">Entity Type which require to be return as list</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">parameter list</param>
        /// <returns>return procedure output</returns>
        public virtual IList<TEntity> ExecuteQuery<TEntity>(string strQuery, System.Collections.ObjectModel.Collection<DBParameters> parameters, int? pageNo, int DBTye, string ConnectionString)
        {
            try
            {
                var list = DapperDBClient.ExecuteQueryConnection<TEntity>(strQuery, parameters, DBTye, ConnectionString);
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(list.Count());
                }

                if (pageNo > 0)
                {
                    return list.Skip(this.StartRowIndex(pageNo) - 1).Take(this.PagingInformation.PageSize).ToList();
                }
                else
                {
                    return list;
                }
            }
            catch (Exception ex) { throw ex; }

        }

        #region "Email Dispatcher Methods"

        /// <summary>
        /// Execute Procedure for custom methods
        /// </summary>
        /// <typeparam name="TEntity">Entity Type which require to be return as list</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">parameter list</param>
        /// <returns>return procedure output</returns>
        protected virtual IList<TEntity> ExecuteQueryWithPagingANDClientID<TEntity>(string strQuery, System.Collections.ObjectModel.Collection<DBParameters> parameters, int? pageNo, string clientCode)
        {
            try
            {
                var list = DapperDBClient.ExecuteQueryWithClientID<TEntity>(strQuery, parameters, clientCode);
                if (list != null && list.Count() > 0)
                {
                    this.SetPaginationInformation(list.Count());
                }

                if (pageNo > 0)
                {
                    return list.Skip(this.StartRowIndex(pageNo) - 1).Take(this.PagingInformation.PageSize).ToList();
                }
                else
                {
                    return list;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        #endregion

        #region "WebServices"

        #region Get Seconds from given Date

        public static object GetEnglishDateSecond(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != DateTime.MinValue))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd hh:mm:ss tt", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            else
            {
                ConvertedDate = DateTime.MinValue.ToString("yyyy/MM/dd hh:mm:ss tt", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }

        #endregion

        #region Set Heartbeat Value Dynamically

        //public static int GetHeratBeatValue()
        //{
        //    return 300;
        //}

        #endregion


        private static string lStr = "1902938476772261234512432153441351234215324135125341236214549262346928759650234209348723048798791231234124312312434123214324";
        private static string rStr = "1230912312435451253215514163621351253234712381232163521471231283126354278731293891204397346354819801098398274375682347264876349";


        public static string DecryptString(string sString)
        {


            int i = 0;
            int NewChar = 0;
            int lLEn = sString.Length;
            string decryString = string.Empty;
            i = 1;

            //Read one by one character and get Asc value of it and append that value in decrypt string           
            // Decryprt string return value must be same as "lstr" and "rstr" (which are define as globally)

            while (!(i == lLEn + 1))
            {
                string stss = Strings.Mid(sString, i, 1);

                NewChar = Strings.AscW(Strings.Mid(sString, i, 1)) - 13;

                decryString = string.Concat(decryString, Strings.ChrW(NewChar));
                i = i + 1;
            }

            // decrypt sting must match with "lstr" string 

            if (!lStr.Equals(decryString.Substring(0, lStr.Length)))
            {
                decryString = "-1";
                return decryString;
            }

            // decrypt sting must match with "rStr" string

            if (!rStr.Equals(decryString.Substring(decryString.Length - rStr.Length, rStr.Length)))
            {
                decryString = "-1";
                return decryString;
            }
            decryString = Strings.Mid(decryString, lStr.Length + 1, decryString.Length - lStr.Length - rStr.Length);
            return decryString;
        }

        #region Convert String To Data-Time

        /// <summary>
        /// Convert String to Date time
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime? ConvertStringToDataTime(string strDate)
        {
            DateTime dateValue;
            DateTime.TryParseExact(strDate, "yyyy-MM-dd HH-mm-ss", CultureInfo.InvariantCulture, 0, out dateValue);
            if (dateValue == DateTime.MinValue)
            {
                return null;
            }
            return dateValue;
        }

        #endregion

        #region Convert Datetime to String

        public static string GetEnglishDateWithoutTime(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != null))
            {
                ConvertedDate = value.Value.ToString("yyyy/MM/dd", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }

        /// <summary>
        /// To convert datetime to string with date-time format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnglishDateWithTime(Nullable<System.DateTime> value)
        {
            string ConvertedDate = null;
            if ((value != null))
            {
                ConvertedDate = value.Value.ToString("yyyy-MM-dd HH:mm:ss", new System.Globalization.CultureInfo("en-US").DateTimeFormat);
            }
            return ConvertedDate;
        }

        


        #endregion


        /// <summary>
        /// Save Attachment to Physical folder
        /// </summary>
        /// <param name="fileContents"></param>
        /// <param name="strFolderName"></param>
        /// <param name="strFileName"></param>
        /// <param name="destinationPath"></param>
        public static int SaveAttachmentFile(byte[] fileContents, string strFolderName, string strFileName, string destinationPath,bool isUploadForcefully = false)
        {
            try
            {
                string strFolderPath = destinationPath + strFolderName;
                string strFilePath = strFolderPath + @"\" + strFileName;

                string returnValuePath = string.Empty;

                //set File Link from DB
                int index = strFilePath.IndexOf("Attachments");
                string pathForDBTable = strFilePath.Substring(index);
                returnValuePath = pathForDBTable.Replace(@"\", @"/");

                //check if folder exists or not
                if (!Directory.Exists(strFolderPath))
                {
                    Directory.CreateDirectory(strFolderPath);
                }
                //check if file exists or not
                if ((!File.Exists(strFilePath)) || isUploadForcefully)
                {
                    using (FileStream stream = new FileStream(strFilePath, FileMode.Create))
                    {
                        System.IO.BinaryWriter writer = new BinaryWriter(stream);
                        writer.Write(fileContents, 0, fileContents.Length);
                        writer.Close();
                    }
                }
                else
                {
                    return -1;
                }
                return 1;

            }
            catch (Exception)
            {
                return 0;
            }
        }




        #endregion

    }
}
