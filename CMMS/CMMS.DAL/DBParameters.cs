//-----------------------------------------------------------------------
// <copyright file="DBParameters.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This class to define default properties for Parameters for SQL Command 
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    public sealed class DBParameters
    {
        #region Properties

        /// <summary>
        /// Gets or sets Parameter Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Parameter Value
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Gets or sets Parameter DBType
        /// </summary>
        public DbType DBType { get; set; }

        /// <summary>
        /// Gets or sets Parameter DBType
        /// </summary>
        public SqlDbType SqlDbType { get; set; }

        public bool IsSqlDbType { get; set; }

        public ParameterDirection Direction { get; set; }

        #endregion
    }
}
