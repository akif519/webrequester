﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestDbFactory.DAL
{
    public class ServiceContext : DataWorker
    {
        #region Property Declaration

        /// <summary>
        /// Gets Prefix of Stored Procedure used in Database
        /// </summary>
        public string ProcedurePrefix
        {
            get
            {
                return "Usp";
            }
        }

        #region Duplicate Check Property

        /// <summary>
        /// Gets or sets a value indicating whether check duplication require or not
        /// </summary>
        public bool CheckForDuplicate { get; set; }

        /// <summary>
        /// Gets or sets Column 1 name for duplication
        /// </summary>
        public string Col1Name { get; set; }

        /// <summary>
        /// Gets or sets Column 2 name for duplication
        /// </summary>
        public string Col2Name { get; set; }

        public string ParentColumnName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether combination check required or not
        /// </summary>
        public bool CombinationCheckRequired { get; set; }

        #endregion

        #endregion

        #region Public Methods

        /// <summary>
        /// Save the Current Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        public virtual int Save<TEntity>(TEntity entity)
        {
            if ((this.CheckForDuplicate && !this.HasDuplicate(entity)) || !this.CheckForDuplicate)
            {
                string procedureName = this.ProcedurePrefix + GetTableName(entity) + "Save";

                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity);

                PropertyInfo[] infos = entity.GetType().GetProperties();
                int primaryKey = Convert.ToInt32(infos.FirstOrDefault().GetValue(entity, null));
                bool isUpdate = false;

                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();

                if (primaryKey > 0)
                {
                    isUpdate = true;

                    //Add into user activity log :: This record has been updated
                    //Developed By Juhi Paunikar On Date 26th Feb,2016
                    SaveUserActivityOnInsertUpdate(entity, isUpdate);
                    SaveEmployeeHistory(entity, primaryKey);
                    primaryKeyValue = DBClient.ExecuteProcedure(procedureName, ExecuteType.ExecuteScalar, parameters, this.DatabaseConnection);

                }
                else
                {
                    //Add into user activity log :: This record has been newly added
                    //Developed By Juhi Paunikar On Date 26th Feb,2016
                    primaryKeyValue = DBClient.ExecuteProcedure(procedureName, ExecuteType.ExecuteScalar, parameters, this.DatabaseConnection);
                    SaveUserActivityOnInsertUpdate(entity, isUpdate, Convert.ToInt32(primaryKeyValue));
                }

                return Convert.ToInt32(primaryKeyValue, CultureInfo.InvariantCulture);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Delete the matching record with primary key value
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="primaryKey">primary key value of record to be delete</param>
        /// <returns>return deleted entity primary key value</returns>
        public virtual int Delete<TEntity>(int primaryKey, bool checkReferences = false)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            /*Execute Stored Procedure*/
            try
            {
                if ((checkReferences && !this.HasReferencesRecords(entityObject, primaryKey)) || !checkReferences)
                {
                    /*define Stored Procedure Name*/
                    string procedureName = this.ProcedurePrefix + GetTableName(entityObject) + "Delete";

                    /*Add Primary Key as Parameter*/
                    System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
                    parameters.Add(new DBParameters()
                    {
                        Name = GetKeyName(entityObject),
                        Value = primaryKey,
                        DBType = GetPropertyType(primaryKey.GetType())
                    });

                    //Add into user activity :: this record has been deleted :)
                    SaveUserActivityOnInsertUpdate(entityObject, false, primaryKey, true);

                    DBClient.ExecuteProcedure(procedureName, ExecuteType.ExecuteNonQuery, parameters, this.DatabaseConnection);
                    return 0;
                }
                else
                {
                    return -2;
                }
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity)
        {
            return this.Search<TEntity>(entity, 0, string.Empty, string.Empty);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo)
        {
            return this.Search<TEntity>(entity, pageNo, string.Empty, string.Empty);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection)
        {
            /*define Stored Procedure Name*/
            string procedureName = this.ProcedurePrefix + GetTableName(entity) + "Search";

            /*Add Parameters*/
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);
            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            if (!string.IsNullOrEmpty(sortExpression) && !string.IsNullOrEmpty(sortDirection))
            {
                parameters.Add(new DBParameters() { Name = "SortExpression", Value = sortExpression, DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "SortDirection", Value = sortDirection, DBType = DbType.String });
            }

            /*Convert Dataset to Model List object*/
            return this.ExecuteProcedure<TEntity>(procedureName, parameters);
        }


        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection, string procedureName)
        {
            /*Add Parameters*/
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);
            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            if (!string.IsNullOrEmpty(sortExpression) && !string.IsNullOrEmpty(sortDirection))
            {
                parameters.Add(new DBParameters() { Name = "SortExpression", Value = sortExpression, DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "SortDirection", Value = sortDirection, DBType = DbType.String });
            }

            /*Convert Dataset to Model List object*/
            return this.ExecuteProcedure<TEntity>(procedureName, parameters);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection, int ActiveRecords = 0)
        {
            /*define Stored Procedure Name*/
            string procedureName = this.ProcedurePrefix + GetTableName(entity) + "Search";
            /*Add Parameters*/
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);
            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            if (!string.IsNullOrEmpty(sortExpression) && !string.IsNullOrEmpty(sortDirection))
            {
                parameters.Add(new DBParameters() { Name = "SortExpression", Value = sortExpression, DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "SortDirection", Value = sortDirection, DBType = DbType.String });
            }

            if (ActiveRecords > 0)
            {
                parameters.Add(new DBParameters() { Name = "ActiveRecordsFilter", Value = ActiveRecords, DBType = DbType.Int16 });
            }

            /*Convert Dataset to Model List object*/
            return this.ExecuteProcedure<TEntity>(procedureName, parameters);
        }


        /// <summary>
        /// Return the Object of Model with data for given primary key value
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="primaryKey">primary key value of record to be select</param>
        /// <returns>Model Object</returns>
        public virtual TEntity SelectObject<TEntity>(int primaryKey)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();
            //// set primary Key value to object
            PropertyInfo info = entityObject.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == GetKeyName(entityObject).ToLower());
            if (info != null)
            {
                /*Set the Value to Model*/
                info.SetValue(entityObject, primaryKey, null);

                var list = Search(entityObject);
                if (list.Count() > 0)
                {
                    return list.FirstOrDefault();
                }
            }

            return entityObject;
        }

        public virtual void SaveUserActivityOnInsertUpdate<TEntity>(TEntity newObject, bool isUpdate, int primaryKey = 0, bool isDeleted = false)
        {
            try
            {
                string tablename = newObject.GetType().Name;
                bool enableLog = false;

                enableLog = Convert.ToBoolean(HttpContext.Current.Session["EnableLog"]);

                if (tablename != "UserLogInLog" && tablename != "ErrorLog" && enableLog)
                {
                    PropertyInfo[] infos = newObject.GetType().GetProperties();
                    List<UserLog> userLog = new List<UserLog>();

                    if (primaryKey == 0)
                    {
                        primaryKey = Convert.ToInt32(infos.FirstOrDefault().GetValue(newObject, null));
                    }
                    int activityID = 0;

                    if (isUpdate)
                    {
                        activityID = UserActivity.Update.GetHashCode();
                        var oldObject = SelectObject<TEntity>(primaryKey);

                        //updated
                        foreach (PropertyInfo info in infos)
                        {
                            var keyName = info.Name.ToString();
                            var oldValue = Convert.ToString(info.GetValue(oldObject, null));
                            var newValue = Convert.ToString(info.GetValue(newObject, null));
                            if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName) && oldValue.ToString() != newValue.ToString())
                            {
                                userLog.Add(new UserLog() { Key = keyName, OldValue = oldValue, NewValue = newValue });
                            }
                        }

                    }
                    else
                    {

                        if (!isDeleted)
                        {
                            //inserted
                            activityID = UserActivity.Add.GetHashCode();
                        }
                        else
                        {
                            //deleted
                            activityID = UserActivity.Delete.GetHashCode();
                        }

                        newObject = SelectObject<TEntity>(primaryKey);
                        foreach (PropertyInfo info in infos)
                        {
                            var keyName = info.Name.ToString();
                            var newValue = Convert.ToString(info.GetValue(newObject, null));
                            if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName))
                            {
                                userLog.Add(new UserLog() { Key = keyName, NewValue = newValue });
                            }
                        }
                    }
                    JavaScriptSerializer jsonResult = new JavaScriptSerializer();
                    string output = jsonResult.Serialize(userLog);

                    int mainID = Convert.ToInt32(HttpContext.Current.Session["MainID"]);

                    //Add into useractivityLog
                    System.Collections.ObjectModel.Collection<DBParameters> activityParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "ActivityID",
                        Value = activityID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "EntityID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "TableName",
                        Value = tablename,
                        DBType = DbType.String
                    });

                    object primaryKeyValue = 0;
                    primaryKeyValue = DBClient.ExecuteProcedure("UspUserActivityLogSave", ExecuteType.ExecuteScalar, activityParameters, this.DatabaseConnection);

                    //Add into useractivityLogDetails
                    System.Collections.ObjectModel.Collection<DBParameters> detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "DetailID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "UserActivityLogID",
                        Value = primaryKeyValue,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "RecordID",
                        Value = primaryKey,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "ChangedValue",
                        Value = output,
                        DBType = DbType.String
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.Int32
                    });


                    DBClient.ExecuteProcedure("UspUserActivityLogDetailSave", ExecuteType.ExecuteScalar, detailParameters,);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public virtual void SaveEmployeeHistory<TEntity>(TEntity newObject, int primaryKey)
        {
            try
            {
                string tablename = newObject.GetType().Name;

                if (tablename == "EmployeeInfo" && primaryKey > 0)
                {
                    PropertyInfo[] infos = newObject.GetType().GetProperties();

                    int activityID = UserActivity.Update.GetHashCode();
                    var oldObject = SelectObject<TEntity>(primaryKey);

                    int mainID = Convert.ToInt32(HttpContext.Current.Session["MainID"]);

                    //Add into EmployeeInfoLog
                    System.Collections.ObjectModel.Collection<DBParameters> activityParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "LogID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "PerformedOn",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "ActivityID",
                        Value = activityID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.String
                    });

                    object primaryKeyValue = 0;
                    primaryKeyValue = DBClient.ExecuteProcedure("UspEmployeeInfoLogSave", ExecuteType.ExecuteScalar, activityParameters, this.DatabaseConnection);


                    //Add into EmployeeInfoLogDetails
                    System.Collections.ObjectModel.Collection<DBParameters> detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    //updated
                    foreach (PropertyInfo info in infos)
                    {
                        var keyName = info.Name.ToString();
                        var oldValue = Convert.ToString(info.GetValue(oldObject, null));
                        var newValue = Convert.ToString(info.GetValue(newObject, null));
                        if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName) && oldValue.ToString() != newValue.ToString())
                        {
                            detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "DetailID",
                                Value = 0,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "LogID",
                                Value = primaryKeyValue,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "EmployeeID",
                                Value = primaryKey,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "ColumnName",
                                Value = keyName,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "OldValue",
                                Value = oldValue,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "NewValue",
                                Value = newValue,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "MainID",
                                Value = mainID,
                                DBType = DbType.Int32
                            });


                            DBClient.ExecuteProcedure("UspEmployeeInfoLogDetailSave", ExecuteType.ExecuteScalar, detailParameters, this.DatabaseConnection);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region static methods

        /// <summary>
        /// Check Current Property is Primary Key or not
        /// </summary>
        /// <param name="info">Property Information</param>
        /// <returns>true or false</returns>
        private static bool IsPrimaryKey(PropertyInfo info)
        {
            var attribute = Attribute.GetCustomAttribute(info, typeof(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute));

            /* This property has a KeyAttribute*/
            return attribute != null;
        }

        /// <summary>
        /// Get Primary Key of Type T
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model object</param>
        /// <returns>Primary key value</returns>
        private static int? GetKeyValue<TEntity>(TEntity entity)
        {
            try
            {
                PropertyInfo[] infos = entity.GetType().GetProperties();
                foreach (PropertyInfo info in infos)
                {
                    /* Check for Key Attribute for Primary Key*/
                    if (IsPrimaryKey(info))
                    {
                        /* Get Property Value and return*/
                        object val = info.GetValue(entity, null);
                        return Convert.ToInt32(val, CultureInfo.InvariantCulture);
                    }
                }

                return null;
            }
            catch
            {
                Console.WriteLine("There is an error in getting primary key value.");
                throw;
            }
        }

        /// <summary>
        ///  Get Primary Key of Type TEntity
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Get Name</param>
        /// <returns>String : Key Name</returns>
        private static string GetKeyName<TEntity>(TEntity entity)
        {
            try
            {
                PropertyInfo[] infos = entity.GetType().GetProperties();
                foreach (PropertyInfo info in infos)
                {
                    /* Check for Key Attribute for Primary Key*/
                    if (IsPrimaryKey(info))
                    {
                        // Return Name of Key Property
                        return info.Name;
                    }
                }

                return null;
            }
            catch (Exception)
            {
                Console.WriteLine("There is an error in getting primary key Name.");
                throw;
            }
        }

        /// <summary>
        /// Get Table Name from Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Get Name</param>
        /// <returns>Table Name</returns>
        private static string GetTableName<TEntity>(TEntity entity)
        {
            var tableAttribute = Attribute.GetCustomAttribute(typeof(TEntity), typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute;
            if (tableAttribute != null)
            {
                return tableAttribute.Name;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Add Parameter 
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">entity of object</param>
        /// <param name="isForSearch">require for search</param>
        /// <returns>returns list of parameters</returns>
        private static System.Collections.ObjectModel.Collection<DBParameters> AddParameters<TEntity>(TEntity entity, bool isForSearch = false)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            PropertyInfo[] infos = entity.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                var value = info.GetValue(entity, null);

                // Verify Property Validation and than add as paramter
                if (ParameterValidation(info, value, isForSearch))
                {
                    if (info.PropertyType == typeof(string))
                    {
                        /* Added by Darshit Babariya 02 September 2013
                         To add trim functionality in All string parameters                        */
                        value = value == null ? null : value.ToString().Trim();
                    }

                    parameters.Add(new DBParameters()
                    {
                        Name = info.Name,
                        Value = value,
                        DBType = GetPropertyType(info.PropertyType)
                    });
                }
            }

            return parameters;
        }

        /// <summary>
        /// Check the Property Information and verify for adding in Parameter
        /// </summary>
        /// <param name="info">Property Info</param>
        /// <param name="value">Property Value</param>
        /// <param name="isForSearch">Is validation for Search</param>
        /// <returns>true for Add as Parameter or false</returns>
        private static bool ParameterValidation(PropertyInfo info, object value, bool isForSearch = false)
        {
            var notMapped = info.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute), true);
            var complexType = info.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), true);

            /* Check Property is primariy key with value or not primary but has value and either Table column or complex type and with search truue */
            if ((notMapped.Count() == 0 || (complexType.Count() > 0 && isForSearch)) && ((value != null && !IsPrimaryKey(info)) || (IsPrimaryKey(info) && Convert.ToInt32(value, CultureInfo.InvariantCulture) > 0)))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the Command DBType from Property
        /// </summary>
        /// <param name="type">Property Type</param>
        /// <returns>appropriate DBType</returns>
        private static DbType GetPropertyType(Type type)
        {
            // Match type Name and return DB Type
            switch (type.Name.ToLower(System.Globalization.CultureInfo.CurrentCulture))
            {
                case "byte[]":
                    return DbType.Binary;
                case "int64":
                case "int64?":
                case "long":
                case "long?":
                    return DbType.Int64;
                case "boolean":
                case "bool":
                case "bool?":
                    return DbType.Boolean;
                case "char":
                case "string":
                    return DbType.String;
                case "datetime":
                case "datetime?":
                    return DbType.DateTime;
                case "decimal":
                case "decimal?":
                    return DbType.Decimal;
                case "double":
                case "double?":
                case "float":
                case "float?":
                    return DbType.Double;
                case "int":
                case "int?":
                case "int32":
                case "int32?":
                    return DbType.Int32;
                case "int16":
                case "int16?":
                    return DbType.Int16;
                case "real":
                    return DbType.Single;
                case "guid":
                    return DbType.Guid;
                case "nullable`1":
                    if (type == typeof(Nullable<int>))
                    {
                        /* || type == typeof(Nullable<Int16>) || type == typeof(Nullable<Int32>))*/
                        return DbType.Int32;
                    }
                    else if (type == typeof(Nullable<long>))
                    {
                        /*|| type == typeof(Nullable<Int64>))*/
                        return DbType.Int64;
                    }
                    else if (type == typeof(Nullable<DateTime>))
                    {
                        return DbType.DateTime;
                    }
                    else if (type == typeof(Nullable<bool>))
                    {
                        /* || type == typeof(Nullable<Boolean>))*/
                        return DbType.Boolean;
                    }
                    else if (type == typeof(Nullable<decimal>))
                    {
                        /* || type == typeof(Nullable<Decimal>))*/
                        return DbType.Decimal;
                    }
                    else if (type == typeof(Nullable<float>) || type == typeof(Nullable<double>))
                    {
                        /* || type == typeof(Nullable<Double>))*/
                        return DbType.Double;
                    }
                    else
                    {
                        return DbType.String;
                    }
            }

            return DbType.String;
        }

        #endregion

        #region Default IDBContext Methods

        /// <summary>
        /// Check Duplicate Records in Database
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Entity Model</param>
        /// <returns>returns entity is duplicate or not</returns>
        private bool HasDuplicate<TEntity>(TEntity entity)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            parameters.Add(new DBParameters() { Name = "tableName", Value = GetTableName(entity), DBType = DbType.String });
            parameters.Add(new DBParameters() { Name = "columnName", Value = this.Col1Name, DBType = DbType.String });
            parameters.Add(new DBParameters() { Name = "columnNameValue", Value = GetPropertyValue(entity, this.Col1Name).Replace("'", "''"), DBType = DbType.String });
            if (!string.IsNullOrEmpty(this.Col2Name))
            {
                parameters.Add(new DBParameters() { Name = "columnName2", Value = this.Col2Name, DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "columnName2Value", Value = GetPropertyValue(entity, this.Col2Name).Replace("'", "''"), DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "IsCombinationCheck", Value = this.CombinationCheckRequired, DBType = DbType.Boolean });
            }

            if (!string.IsNullOrEmpty(this.ParentColumnName))
            {
                parameters.Add(new DBParameters() { Name = "parentColumnKey", Value = this.ParentColumnName, DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "parentColumnKeyValue", Value = GetPropertyValue(entity, this.ParentColumnName).Replace("'", "''"), DBType = DbType.String });
            }

            parameters.Add(new DBParameters() { Name = "primaryKey", Value = GetKeyName(entity), DBType = DbType.String });

            parameters.Add(new DBParameters() { Name = "primaryKeyValue", Value = GetKeyValue(entity).ToString(), DBType = DbType.String });

            DataSet ds = (DataSet)DBClient.ExecuteProcedure("UspGeneralCheckDuplicate", ExecuteType.ExecuteDataSet, parameters, this.DatabaseConnection);
            return ds.Tables[0].Rows.Count > 0;
        }

        public bool HasReferencesRecords<TEntity>(TEntity entity, int primaryKey)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            parameters.Add(new DBParameters() { Name = "tableName", Value = GetTableName(entity), DBType = DbType.String });
            parameters.Add(new DBParameters() { Name = "primaryKeyValue", Value = primaryKey, DBType = DbType.Int64 });

            DataSet ds = (DataSet)DBClient.ExecuteProcedure("UspGeneralCheckReferences", ExecuteType.ExecuteDataSet, parameters, this.DatabaseConnection);
            return ds.Tables[0].Rows.Count > 0;
        }


        #endregion
        public IList<T> SelectAll<T>()
        {
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                using (IDbCommand command = database.CreateCommand("SELECT * FROM TESTDATATYPES", connection))
                {
                    using (IDataReader reader = command.ExecuteReader())
                    {
                        return DataReaderToList<T>(reader);
                    }
                }
            }
        }

        private IList<T> DataReaderToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();

            T obj = default(T);

            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();

                for (int i = 0; i < dr.FieldCount; i++)
                {
                    PropertyInfo info = obj.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr.GetName(i).ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        info.SetValue(obj, dr.GetValue(i) != System.DBNull.Value ? dr.GetValue(i) : null, null);
                    }
                }

                list.Add(obj);
            }

            return list;
        }
    }
}

