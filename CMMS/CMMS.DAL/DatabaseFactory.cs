﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Configuration;
using CMMS.Infrastructure;
namespace CMMS.DAL
{
    public sealed class DatabaseFactory
    {
        //public static DatabaseFactorySectionHandler sectionHandler = (DatabaseFactorySectionHandler)ConfigurationManager.GetSection("DatabaseFactoryConfiguration");
        private static string ConnectionStrings = string.Empty;
        private static int DatabaseType;
        private DatabaseFactory() { }

        private string _DbType = string.Empty;
        public static Database CreateDatabase()
        {
            
            Database createdObject = null;
            try
            {
                
                //Type database = null;
                if (string.IsNullOrEmpty(ProjectSession.ConnectionString) || CMMS.Infrastructure.ProjectSession.UseMasterDB)
                {
                    ConnectionStrings = System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString;
                    DatabaseType = SystemEnum.DBTYPE.SQL.GetHashCode();
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    ConnectionStrings = System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString;
                    DatabaseType = SystemEnum.DBTYPE.SQL.GetHashCode();
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else
                {
                    ConnectionStrings = ProjectSession.ConnectionString;
                    DatabaseType = ProjectSession.DbType;
                }

                // Find the class
                if (DatabaseType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    //database = Type.GetType("CMMS.DAL.MsSqlDataBase");
                    ProjectSession.DbType = SystemEnum.DBTYPE.SQL.GetHashCode();
                    createdObject = new MsSqlDataBase();
                }
                else if (DatabaseType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    //database = Type.GetType("CMMS.DAL.OracleDataBase");
                    ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();
                    createdObject = new OracleDataBase();
                }
                else if (DatabaseType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    //database = Type.GetType("CMMS.DAL.MySqlDataBase");
                    ProjectSession.DbType = SystemEnum.DBTYPE.MYSQL.GetHashCode();
                    createdObject = new MySqlDataBase();
                }
                else
                {
                    //database = Type.GetType("CMMS.DAL.MsSqlDataBase");
                    ProjectSession.DbType = SystemEnum.DBTYPE.SQL.GetHashCode();
                    createdObject = new MsSqlDataBase();
                }

                // Get it's constructor
                //ConstructorInfo constructor = database.GetConstructor(new Type[] { });

                // Invoke it's constructor, which returns an instance.
                //Database createdObject = (Database)constructor.Invoke(null);
                

                // Initialize the connection string property for the database.
                createdObject.connectionString = ConnectionStrings;

                // Pass back the instance as a Database
                return createdObject;
            }
            catch (Exception excep)
            {
                throw new Exception("Error instantiating database " + DatabaseType + ". " + excep.Message);
            }
        }
    }
}
