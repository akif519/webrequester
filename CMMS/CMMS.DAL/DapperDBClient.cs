﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.DAL
{
    using MySql.Data.MySqlClient;
    using Oracle.DataAccess.Client;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using CMMS.Infrastructure;
    using Dapper;
    using System.Text.RegularExpressions;
    using System.IO;

    internal sealed class DapperDBClient : DataWorker
    {
        #region "Dapper Methods"

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static object Execute(string Query, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            object returnValue = null;
            IDbConnection connection = null;
            bool CMMSMasterConfigContext = false;

            if (Query.Contains("OPEN SYMMETRIC KEY"))
            {
                CMMSMasterConfigContext = true;
            }
           
            try
            {
                connection = null;
                var dynamicparam = new DynamicParameters();

                if (parameters != null)
                {
                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }

                if (ProjectSession.UseMasterDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                    //connection = new OracleConnection(ProjectSession.ConnectionString);
                }
                else if (CMMSMasterConfigContext == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSMasterConfigContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }


                returnValue = connection.Execute(Query, dynamicparam);

                }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("foreign key") || ex.Message.ToLower().Contains("child record found"))
                {
                    returnValue = -2;
                }
                else
                {
                    throw ex;
                }

            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static int Execute<TEntity>(string Query, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            int returnValue = 0;
            IDbConnection connection = null;
            bool CMMSMasterConfigContext = false;

            if (Query.Contains("OPEN SYMMETRIC KEY"))
            {
                CMMSMasterConfigContext = true;
            }
            try
            {

                var dynamicparam = new DynamicParameters();
                if (ProjectSession.UseMasterDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                    
                    Query = Query.Replace("@", ":");
                    Query = Query.Replace("[", "");
                    Query = Query.Replace("]", "");
                    Query = Regex.Replace(Query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    Query = Regex.Replace(Query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    Query = Regex.Replace(Query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    Query = Query.Replace("dbo.", " ");
                }
                else if (CMMSMasterConfigContext == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSMasterConfigContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                foreach (DBParameters parameter in parameters)
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                    {
                        if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                    }
                    dynamicparam.Add(parameter.Name, parameter.Value);

                }

                if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    returnValue = connection.Query<int>(Query, dynamicparam).Single();
                }
                else
                {
                    if(!Query.Contains("ErrorLog"))                    
                    {
                        dynamicparam.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    }
                    connection.Query<int>(Query, dynamicparam);
                    if (!Query.Contains("ErrorLog"))                    
                        returnValue = dynamicparam.Get<int>("Id");
                }

            }
            catch (Exception ex)
            {
                // CMMS.Infrastructure.Common.WriteLog((ex.InnerException + "==>Exception");
                // CMMS.Infrastructure.Common.WriteLog((ex.Message + "==>Message");
                // CMMS.Infrastructure.Common.WriteLog((ex.StackTrace + "==>StackTrace");
                // CMMS.Infrastructure.Common.WriteLog((ex.Source + "==>Source");                
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static string ExecuteReturnString<TEntity>(string Query, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            string returnValue = string.Empty;
            IDbConnection connection = null;
            try
            {

                var dynamicparam = new DynamicParameters();
                if (ProjectSession.UseMasterDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                }

                foreach (DBParameters parameter in parameters)
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                    {
                        if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                    }
                    dynamicparam.Add(parameter.Name, parameter.Value);

                }

                if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    returnValue = connection.Query<string>(Query, dynamicparam).Single();
                }
                else
                {

                    dynamicparam.Add(name: "Id", dbType: DbType.String, direction: ParameterDirection.Output);

                    connection.Query<string>(Query, dynamicparam);
                    returnValue = dynamicparam.Get<string>("Id");

                    if (returnValue == string.Empty || returnValue == null)
                        returnValue = "0";
                }

            }
            catch
            {
                
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <param name="databaseConnection">Database Connection String</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteProcedure<T>(string procedureName, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            /* Create Database object
              Database db = DatabaseFactory.CreateDatabase();*/
            List<T> returnValue = new List<T>();

            // Create a suitable command type and add the required parameter
            IDbConnection connection = null;

            try
            {
                var dynamicparam = new DynamicParameters();
                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                    {
                        if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                    }
                    dynamicparam.Add(parameter.Name, parameter.Value);
                }

                /*Execute Procedure from supplied Execution type*/
                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                }

                returnValue = DataReaderToList<T>(connection.ExecuteReader(procedureName, dynamicparam, commandType: CommandType.StoredProcedure));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <param name="databaseConnection">Database Connection String</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteQuery<T>(string Query, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            IDbConnection connection = null;
            try
            {
                /* Create Database object
                Database db = DatabaseFactory.CreateDatabase();*/
                List<T> list = new List<T>();
                bool CMMSMasterConfigContext = false;
                // // CMMS.Infrastructure.Common.WriteLog(("ExecuteQuery Start");
                if (Query.Contains("OPEN SYMMETRIC KEY"))
                {
                    // // CMMS.Infrastructure.Common.WriteLog(("OPEN SYMMETRIC KEY found");
                    CMMSMasterConfigContext = true;
                    ProjectSession.UseMasterDB = false;
                }                
                /*Execute Procedure from supplied Execution type*/
                if (ProjectSession.UseMasterDB == true)
                {
                    // // CMMS.Infrastructure.Common.WriteLog(("ProjectSession.UseMasterDB=" + ProjectSession.UseMasterDB);
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    // // CMMS.Infrastructure.Common.WriteLog(("ProjectSession.UseHelpDB=" + ProjectSession.UseHelpDB);
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (CMMSMasterConfigContext == true)
                {
                    // // CMMS.Infrastructure.Common.WriteLog(("CMMSMasterConfigContext=" + CMMSMasterConfigContext);
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSMasterConfigContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    // // CMMS.Infrastructure.Common.WriteLog(("ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode()=" + ProjectSession.DbType + " " + SystemEnum.DBTYPE.SQL.GetHashCode());
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {

                    Query = Query.Replace("[", "`");
                    Query = Query.Replace("]", "`");
                    Query = Regex.Replace(Query, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "GETDATE()", "Now()", RegexOptions.IgnoreCase);
                    Query = Query.Replace("dbo.", " ");
                    //strQuery.Replace("ISNULL(", "IFNULL(");
                    // // CMMS.Infrastructure.Common.WriteLog(("ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode()=" + ProjectSession.DbType + " " + SystemEnum.DBTYPE.MYSQL.GetHashCode());
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    Query = Query.Replace("@", ":");
                    Query = Query.Replace("[", "");
                    Query = Query.Replace("]", "");
                    Query = Regex.Replace(Query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    Query = Regex.Replace(Query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    Query = Regex.Replace(Query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    Query = Regex.Replace(Query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    Query = Query.Replace("dbo.", " ");

                    // // CMMS.Infrastructure.Common.WriteLog((ProjectSession.ConnectionString + "==>ProjectSession.ConnectionString oracle assign start");
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                    // // CMMS.Infrastructure.Common.WriteLog((connection.ConnectionString + "==>connection.ConnectionString oracle Assign Connection String");
                    // // CMMS.Infrastructure.Common.WriteLog((ProjectSession.ConnectionString + "==>ProjectSession.ConnectionString oracle assign end");
                }
                else
                {
                    // // CMMS.Infrastructure.Common.WriteLog((ProjectSession.ConnectionString + "==>ProjectSession.ConnectionString sql assign start");
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                    // // CMMS.Infrastructure.Common.WriteLog((ProjectSession.ConnectionString + "==>ProjectSession.ConnectionString sql assign end");
                }

                // // CMMS.Infrastructure.Common.WriteLog((connection.ConnectionString + "==>" + ProjectSession.DbType + "==>" + SystemEnum.DBTYPE.ORACLE.GetHashCode());

                var dynamicparam = new DynamicParameters();
                /*Add different Parameter from Model object Property*/
                if (parameters != null)
                {
                    foreach (DBParameters parameter in parameters)
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        // // CMMS.Infrastructure.Common.WriteLog((parameter.Name + "==>parameter.Name");
                        // // CMMS.Infrastructure.Common.WriteLog((parameter.Value + "==>parameter.Value");
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }

                // // CMMS.Infrastructure.Common.WriteLog((Query + "==>Query Execute Start");
                //list = DataReaderToList<T>(connection.ExecuteReader(Query, dynamicparam));                
                list = connection.Query<T>(Query, dynamicparam).ToList();
                // // CMMS.Infrastructure.Common.WriteLog(("==>Query Execute End");

                return list;
            }
            catch (Exception ex)
            {
                // // CMMS.Infrastructure.Common.WriteLog((ex.InnerException + "==>Exception");
                // // CMMS.Infrastructure.Common.WriteLog((ex.Message + "==>Message");
                // // CMMS.Infrastructure.Common.WriteLog((ex.StackTrace + "==>StackTrace");
                // // CMMS.Infrastructure.Common.WriteLog((ex.Source + "==>Source");
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        public static string ExecuteScalar(string query)
        {
            IDbConnection connection = null;
            IDbCommand command = null;
            string returnValue = string.Empty;
            try
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                    command = (SqlCommand)new SqlCommand();
                    command.CommandText = query;
                    command.Connection = (SqlConnection)connection;
                    command.CommandType = CommandType.Text;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                    command = (MySqlCommand)new MySqlCommand();
                    command.CommandText = query;
                    command.Connection = (MySqlConnection)connection;
                    command.CommandType = CommandType.Text;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                    command = (OracleCommand)new OracleCommand();
                    command.CommandText = query;
                    command.Connection = (OracleConnection)connection;
                    command.CommandType = CommandType.Text;
                }
                connection.Open();
                returnValue = Convert.ToString(command.ExecuteScalar());

                if (returnValue == "null")
                {
                    returnValue = string.Empty;
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }

        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <param name="databaseConnection">Database Connection String</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteQueryConnection<T>(string Query, System.Collections.ObjectModel.Collection<DBParameters> parameters = null, int DBType = 1, string ConnectionString = "")
        {
            IDbConnection connection = null;
            try
            {
                /* Create Database object
                Database db = DatabaseFactory.CreateDatabase();*/
                List<T> list = new List<T>();

                /*Execute Procedure from supplied Execution type*/
                //if (ProjectSession.UseMasterDB == true)
                //{
                //    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                //    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                //}
                //else
                if (DBType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ConnectionString);
                }
                else if (DBType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ConnectionString);
                }
                else if (DBType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ConnectionString);
                }
                else
                {
                    connection = new SqlConnection(ConnectionString);
                }


                var dynamicparam = new DynamicParameters();
                /*Add different Parameter from Model object Property*/
                if (parameters != null)
                {
                    foreach (DBParameters parameter in parameters)
                    {
                        if (DBType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }


                //list = DataReaderToList<T>(connection.ExecuteReader(Query, dynamicparam));
                list = connection.Query<T>(Query, dynamicparam).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        #endregion

        #region "Common Method"
        /// <summary>
        /// Convert Data Reader to List
        /// </summary>
        /// <typeparam name="T">Entity Object</typeparam>
        /// <param name="dr">data reader object</param>
        /// <returns>return list of objects</returns>
        private static List<T> DataReaderToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();

            T obj = default(T);

            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();

                for (int i = 0; i < dr.FieldCount; i++)
                {
                    PropertyInfo info = obj.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr.GetName(i).ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        info.SetValue(obj, dr.GetValue(i) != System.DBNull.Value ? dr.GetValue(i) : null, null);
                    }
                }

                list.Add(obj);
            }

            return list;
        }

        /// <summary>
        /// Get Identity Value After Insert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="setId"></param>
        protected static int SetIdentity<T>(IDbConnection connection)
        {
            return connection.Query("SELECT @@IDENTITY AS Id").Single();
        }


        #endregion

        #region "Transaction Related Methods"

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static object Execute(string Query, IDbConnection connection, IDbTransaction transaction, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            object returnValue = null;

            try
            {
                var dynamicparam = new DynamicParameters();

                if (parameters != null)
                {
                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                returnValue = connection.Execute(Query, dynamicparam, transaction);

            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("foreign key"))
                {
                    returnValue = -2;
                }
                else
                {
                    throw ex;
                }

            }
            finally
            {

            }
            return returnValue;
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> Execute<T>(string Query, IDbConnection connection, IDbTransaction transaction, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            List<T> list = new List<T>();

            try
            {

                var dynamicparam = new DynamicParameters();

                foreach (DBParameters parameter in parameters)
                {
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                    {
                        if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                    }
                    dynamicparam.Add(parameter.Name, parameter.Value);

                }

                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                list = connection.Query<T>(Query, dynamicparam, transaction).ToList();

                return list;

            }
            catch (Exception ex)
            {
                throw ex; ;
            }
            finally
            {

            }
        }

        public static IDbConnection OpenConnection()
        {
            try
            {
                IDbConnection connection = null;

                if (ProjectSession.UseMasterDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseMasterDB = false;
                }
                else if (ProjectSession.UseHelpDB == true)
                {
                    connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBHelpContext"].ConnectionString);
                    CMMS.Infrastructure.ProjectSession.UseHelpDB = false;
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    connection = new SqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    connection = new MySqlConnection(ProjectSession.ConnectionString);
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    connection = new OracleConnection(ProjectSession.ConnectionString);
                }

                connection.Open();

                return connection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IDbTransaction BeginTransaction(IDbConnection connection)
        {
            try
            {
                return connection.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void CommitTransaction(IDbTransaction transaction)
        {
            try
            {
                if (transaction != null)
                {
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void RollBackTransaction(IDbTransaction transaction)
        {
            try
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region "Email Dispatcher Related Methods"

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteQueryWithClientID<T>(string strQuery, System.Collections.ObjectModel.Collection<DBParameters> parameters, string clientCode)
        {
            IDbConnection connection = null;
            try
            {
                /*clientID = 0 -- Master DB */

                List<T> list = new List<T>();
                int dbType = 1;
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    CMMS.Model.Account objAccount = null;

                    objAccount = GetAccountConnectionDetails(clientCode).FirstOrDefault();

                    if (objAccount != null && objAccount.DbType != null && !string.IsNullOrEmpty(objAccount.DbConString))
                    {
                        dbType = Convert.ToInt32(objAccount.DbType);
                        connectionString = Convert.ToString(objAccount.DbConString);

                        if (dbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            connection = new SqlConnection(connectionString);
                        }
                        else if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            connection = new OracleConnection(connectionString);                                                        
                            strQuery = strQuery.Replace("= @", "= :");
                            strQuery = strQuery.Replace("=@", "=:");
                            strQuery = strQuery.Replace("@", ":");
                            strQuery = strQuery.Replace("[", "");
                            strQuery = strQuery.Replace("]", "");
                            strQuery = Regex.Replace(strQuery, "ISNULL", "NVL", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);

                            strQuery = strQuery.Replace("dbo.", " ");

                        }
                        else if (dbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            connection = new MySqlConnection(connectionString);

                            strQuery = strQuery.Replace("[", "`");
                            strQuery = strQuery.Replace("]", "`");
                            strQuery = Regex.Replace(strQuery, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "GETDATE()", "Now()", RegexOptions.IgnoreCase);
                            strQuery = strQuery.Replace("dbo.", " ");
                        }
                    }
                }



                var dynamicparam = new DynamicParameters();
                /*Add different Parameter from Model object Property*/
                if (parameters != null && parameters.Count > 0)
                {
                    foreach (DBParameters parameter in parameters)
                    {
                        if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }


                //list = DataReaderToList<T>(connection.ExecuteReader(Query, dynamicparam));
                list = connection.Query<T>(strQuery, dynamicparam).ToList();

                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static object ExecuteWithClientID(string strQuery, System.Collections.ObjectModel.Collection<DBParameters> parameters, string clientCode)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            object returnValue = null;
            IDbConnection connection = null;
            int dbType = 1;
            string connectionString = string.Empty;

            try
            {
                connection = null;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    CMMS.Model.Account objAccount = null;

                    objAccount = GetAccountConnectionDetails(clientCode).FirstOrDefault();

                    if (objAccount != null && objAccount.DbType != null && !string.IsNullOrEmpty(objAccount.DbConString))
                    {
                        dbType = Convert.ToInt32(objAccount.DbType);
                        connectionString = Convert.ToString(objAccount.DbConString);

                        if (dbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            connection = new SqlConnection(connectionString);
                        }
                        else if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            connection = new OracleConnection(connectionString);

                            strQuery = strQuery.Replace("@", ":");
                            strQuery = strQuery.Replace("[", "");
                            strQuery = strQuery.Replace("]", "");
                            strQuery = Regex.Replace(strQuery, "ISNULL", "NVL", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);

                            strQuery = strQuery.Replace("dbo.", " ");
                        }
                        else if (dbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            connection = new MySqlConnection(connectionString);

                            strQuery = strQuery.Replace("[", "`");
                            strQuery = strQuery.Replace("]", "`");
                            strQuery = Regex.Replace(strQuery, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);
                            strQuery = Regex.Replace(strQuery, "GETDATE()", "Now()", RegexOptions.IgnoreCase);

                            strQuery = strQuery.Replace("dbo.", " ");
                        }
                    }
                }

                var dynamicparam = new DynamicParameters();

                if (parameters != null)
                {
                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);
                    }
                }

                returnValue = connection.Execute(strQuery, dynamicparam);

            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("foreign key") || ex.Message.ToLower().Contains("child record found"))
                {
                    returnValue = -2;
                }
                else
                {
                    throw ex;
                }

            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }

        public static List<CMMS.Model.Account> GetAccountConnectionDetails(string clientCode)
        {
            IDbConnection connection = null;

            try
            {
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                //string masterQuery = "Select dbType,dbConString from Accounts";

                //string masterQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;Select CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString', Clientid  from Accounts";

                string masterQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [ClientID],CONVERT(nvarchar(max), DecryptByKey([UserName])) AS 'UserName',CONVERT(nvarchar(max), DecryptByKey([ClientCode])) AS 'ClientCode',[HdrLogo],[RprtLogo],[HdrNameEn],[HdrNameAr],[CreatedBy],[ModifiedDate],[SessionTimeOut],CONVERT(nvarchar(max), DecryptByKey([Password])) AS 'Password',CONVERT(int, DecryptByKey(IsNamedUser)) AS 'IsNamedUser',CONVERT(int, DecryptByKey(ConCurrentUsers)) AS 'ConCurrentUsers',CONVERT(int, DecryptByKey(MobAppLicCon)) AS 'MobAppLicCon',CONVERT(int, DecryptByKey(MobAppLicNamed)) AS 'MobAppLicNamed',DateCreated,CONVERT(int, DecryptByKey(SubCycle)) AS 'SubCycle',ExpDate,CONVERT(int, DecryptByKey(AccStatus)) AS 'AccStatus',CONVERT(int, DecryptByKey(dbType)) AS 'dbType',CONVERT(nvarchar(50), DecryptByKey(dbName)) AS 'dbName',CONVERT(nvarchar(max), DecryptByKey(dbConString)) AS 'dbConString',CONVERT(nvarchar(50), DecryptByKey(LicenseNo)) AS 'LicenseNo',CONVERT(int, DecryptByKey(NoOfLicenses)) AS 'NoOfLicenses',[IsItemNoAutoIncrement],[SharedFolderPath],[AllowedFileTypes],[AllowedMaxFilesize],[DashboardRefreshTime],[SessionTimeOutMobApp] FROM [dbo].[Accounts] ";

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    masterQuery += " Where CONVERT(nvarchar(max), DecryptByKey(ClientCode)) = '" + clientCode + "'";
                }

                return connection.Query<CMMS.Model.Account>(masterQuery, new DynamicParameters()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }
        }


        public static List<CMMS.Model.FirstLevelTbl> GetFirstLevelTbl(int clientId)
        {
            IDbConnection connection = null;

            try
            {
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                //string masterQuery = "Select dbType,dbConString from Accounts";

                string masterQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT ClientID,L1_ID,CONVERT(nvarchar(max), DecryptByKey(L1_en)) AS 'L1_en',CONVERT(nvarchar(max), DecryptByKey(L1_ar)) AS 'L1_ar' FROM dbo.FirstLevelTbl ";

                if (clientId >0)
                {
                    masterQuery += " WHERE ClientID = " + clientId;
                }

                return connection.Query<CMMS.Model.FirstLevelTbl>(masterQuery, new DynamicParameters()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }
        }



        public static List<CMMS.Model.LevelNamesTbl> GetLevelNamesTbl(int L1_ID)
        {
            IDbConnection connection = null;

            try
            {
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                //string masterQuery = "Select dbType,dbConString from Accounts";

                string masterQuery = "OPEN SYMMETRIC KEY CMMSMasterEncryptKey DECRYPTION BY CERTIFICATE Config_Master;SELECT [L1_ID],CONVERT(varchar(max), DecryptByKey(L2_en)) AS 'L2_en',CONVERT(nvarchar(max), DecryptByKey(L2_ar)) AS 'L2_ar',CONVERT(nvarchar(max), DecryptByKey(L3_en)) AS 'L3_en',CONVERT(nvarchar(max), DecryptByKey(L3_ar)) AS 'L3_ar',CONVERT(nvarchar(max), DecryptByKey(L4_en)) AS 'L4_en',CONVERT(nvarchar(max), DecryptByKey(L4_ar)) AS 'L4_ar',CONVERT(nvarchar(max), DecryptByKey(L5_en)) AS 'L5_en',CONVERT(nvarchar(max), DecryptByKey(L5_ar)) AS 'L5_ar'FROM [dbo].[LevelNamesTbl] ";

                if (L1_ID > 0)
                {
                    masterQuery += " WHERE L1_ID = " + L1_ID;
                }

                return connection.Query<CMMS.Model.LevelNamesTbl>(masterQuery, new DynamicParameters()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }
        }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static string ExecuteReturnStringWithClientID<TEntity>(string Query,string clientCode, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            string returnValue = string.Empty;
            IDbConnection connection = null;
            try
            {
                int dbType = 1;
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    CMMS.Model.Account objAccount = null;

                    objAccount = GetAccountConnectionDetails(clientCode).FirstOrDefault();

                    if (objAccount != null && objAccount.DbType != null && !string.IsNullOrEmpty(objAccount.DbConString))
                    {
                        dbType = Convert.ToInt32(objAccount.DbType);
                        connectionString = Convert.ToString(objAccount.DbConString);
                        connection = new SqlConnection(connectionString);
                    }



                    var dynamicparam = new DynamicParameters();
                    foreach (DBParameters parameter in parameters)
                    {
                        if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);

                    }

                    if (dbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        returnValue = connection.Query<string>(Query, dynamicparam).Single();
                    }
                    else
                    {

                        dynamicparam.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 150);
                        
                        connection.Query<string>(Query, dynamicparam);
                        returnValue = dynamicparam.Get<string>("Id");

                        if (returnValue == string.Empty || returnValue == null)
                            returnValue = "0";
                    }

                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static string ExecuteReturnStringWithClientIDTest<TEntity>(string Query, string clientCode, System.Collections.ObjectModel.Collection<DBParameters> parameters = null)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            string returnValue = string.Empty;
            IDbConnection connection = null;
            try
            {
                int dbType = 1;
                string connectionString = string.Empty;

                connectionString = System.Configuration.ConfigurationManager.AppSettings["CMMSConnectionString"];

                if (string.IsNullOrEmpty(connectionString))
                {
                    throw new Exception("Master Database Connection not found in App.Config file with Key name: CMMSConnectionString.");
                }

                connection = new SqlConnection(connectionString);

                if (!string.IsNullOrWhiteSpace(clientCode))
                {
                    CMMS.Model.Account objAccount = null;

                    objAccount = GetAccountConnectionDetails(clientCode).FirstOrDefault();

                    if (objAccount != null && objAccount.DbType != null && !string.IsNullOrEmpty(objAccount.DbConString))
                    {
                        dbType = Convert.ToInt32(objAccount.DbType);
                        connectionString = Convert.ToString(objAccount.DbConString);
                        connection = new SqlConnection(connectionString);
                    }



                    var dynamicparam = new DynamicParameters();
                    foreach (DBParameters parameter in parameters)
                    {
                        if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && parameter.DBType == System.Data.DbType.Boolean)
                        {
                            if (Convert.ToBoolean(parameter.Value) == true) { parameter.Value = 1; } else if (Convert.ToBoolean(parameter.Value) == false) { parameter.Value = 0; }
                        }
                        dynamicparam.Add(parameter.Name, parameter.Value);

                    }

                    if (dbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        returnValue = connection.Query<string>(Query, dynamicparam).Single();
                    }
                    else
                    {

                        dynamicparam.Add(name: "Id", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 150);

                        connection.Open();
                        connection.Query<string>(Query, dynamicparam);
                        returnValue = dynamicparam.Get<string>("Id");

                        if (returnValue == string.Empty || returnValue == null)
                            returnValue = "0";
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                connection.Dispose();
            }
            return returnValue;
        }
        #endregion
    }
}
