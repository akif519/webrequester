﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.DAL
{
    public class DataWorker
    {
        [ThreadStaticAttribute]
        private static Database _database = null;

        static DataWorker()
        {
            try
            {
                _database = DatabaseFactory.CreateDatabase();
            }
            catch (Exception excep)
            {
                throw excep;
            }
        }

        public static Database database
        {
            get
            {
                    _database = DatabaseFactory.CreateDatabase();
                    return _database;
            }
        }
    }
}
