﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMMS.DAL
{
    using CMMS.Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Script.Serialization;

    public class DapperDBContext : DBExecute, IDBContext, IDisposable
    {
        /// <summary>
        /// dispose status
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Finalizes an instance of the DBContext class.
        /// </summary>
        ~DapperDBContext()
        {
            this.Dispose(false);
        }

        #region Property Declaration

        /// <summary>
        /// Gets Prefix of Stored Procedure used in Database
        /// </summary>
        public string ProcedurePrefix
        {
            get
            {
                return "Usp";
            }
        }

        #region Duplicate Check Property

        /// <summary>
        /// Gets or sets a value indicating whether check duplication require or not
        /// </summary>
        public bool CheckForDuplicate { get; set; }

        /// <summary>
        /// Gets or sets Column 1 name for duplication
        /// </summary>
        public string Col1Name { get; set; }

        /// <summary>
        /// Gets or sets Column 2 name for duplication
        /// </summary>
        public string Col2Name { get; set; }

        public string ParentColumnName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether combination check required or not
        /// </summary>
        public bool CombinationCheckRequired { get; set; }

        /// <summary>
        /// Gets or sets a value
        /// </summary>
        public IDbConnection Connection { get; set; }

        private IDbTransaction _Transaction { get; set; }

        #endregion

        #endregion

        #region static methods

        /// <summary>
        /// Check Current Property is Primary Key or not
        /// </summary>
        /// <param name="info">Property Information</param>
        /// <returns>true or false</returns>
        private static bool IsPrimaryKey(PropertyInfo info)
        {
            var attribute = Attribute.GetCustomAttribute(info, typeof(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedAttribute));

            /* This property has a KeyAttribute*/
            return attribute != null;
        }

        /// <summary>
        /// Get Primary Key of Type T
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model object</param>
        /// <returns>Primary key value</returns>
        private static int? GetKeyValue<TEntity>(TEntity entity)
        {
            try
            {
                PropertyInfo[] infos = entity.GetType().GetProperties();
                foreach (PropertyInfo info in infos)
                {
                    /* Check for Key Attribute for Primary Key*/
                    if (IsPrimaryKey(info))
                    {
                        /* Get Property Value and return*/
                        object val = info.GetValue(entity, null);
                        return Convert.ToInt32(val, CultureInfo.InvariantCulture);
                    }
                }

                return null;
            }
            catch
            {
                Console.WriteLine("There is an error in getting primary key value.");
                throw;
            }
        }

        /// <summary>
        ///  Get Primary Key of Type TEntity
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Get Name</param>
        /// <returns>String : Key Name</returns>
        private static string GetKeyName<TEntity>(TEntity entity)
        {
            try
            {
                PropertyInfo[] infos = entity.GetType().GetProperties();
                foreach (PropertyInfo info in infos)
                {
                    /* Check for Key Attribute for Primary Key*/
                    if (IsPrimaryKey(info))
                    {
                        // Return Name of Key Property
                        return info.Name;
                    }
                }

                return null;
            }
            catch (Exception)
            {
                Console.WriteLine("There is an error in getting primary key Name.");
                throw;
            }
        }

        /// <summary>
        /// Get Table Name from Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Get Name</param>
        /// <returns>Table Name</returns>
        private static string GetTableName<TEntity>(TEntity entity)
        {
            var tableAttribute = Attribute.GetCustomAttribute(typeof(TEntity), typeof(System.ComponentModel.DataAnnotations.Schema.TableAttribute)) as System.ComponentModel.DataAnnotations.Schema.TableAttribute;
            if (tableAttribute != null)
            {
                if (tableAttribute.Name == "\"return\"" && ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    string tablename = "`return`";
                    return tablename;
                }
                else if (tableAttribute.Name == "\"return\"" && ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    string tablename = "\"RETURN\"";
                    return tablename;
                }
                else
                {
                    return tableAttribute.Name;
                }

            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Add Parameter 
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">entity of object</param>
        /// <param name="isForSearch">require for search</param>
        /// <returns>returns list of parameters</returns>
        private static System.Collections.ObjectModel.Collection<DBParameters> AddParameters<TEntity>(TEntity entity, bool isForSearch = false)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            PropertyInfo[] infos = entity.GetType().GetProperties();
            Boolean UserMasterDB = false;
            int DbType = 1;

            try
            {
                UserMasterDB = ProjectSession.UseMasterDB;
                DbType = ProjectSession.DbType;
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }

            foreach (PropertyInfo info in infos)
            {
                var value = info.GetValue(entity, null);

                // Verify Property Validation and than add as paramter
                if (ParameterValidation(info, value, isForSearch))
                {
                    if (info.PropertyType == typeof(string))
                    {
                        /* Added by Darshit Babariya 02 September 2013
                         To add trim functionality in All string parameters                        */
                        value = value == null ? null : value.ToString().Trim();
                    }

                    if (UserMasterDB != true && DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        if (info.Name.ToLower() == "date")
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = "date_",
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                        else if (info.Name.ToLower() == "comment")
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = "Comment_",
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                        else
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = info.Name,
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = info.Name,
                            Value = value,
                            DBType = GetPropertyType(info.PropertyType)
                        });
                    }

                }
            }

            return parameters;
        }



        /// <summary>
        /// Check the Property Information and verify for adding in Parameter
        /// </summary>
        /// <param name="info">Property Info</param>
        /// <param name="value">Property Value</param>
        /// <param name="isForSearch">Is validation for Search</param>
        /// <returns>true for Add as Parameter or false</returns>
        private static bool ParameterValidation(PropertyInfo info, object value, bool isForSearch = false)
        {
            var notMapped = info.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.NotMappedAttribute), true);
            var complexType = info.GetCustomAttributes(typeof(System.ComponentModel.DataAnnotations.Schema.ColumnAttribute), true);

            var NullableDate = info.GetCustomAttributes(typeof(CMMS.Model.AllowNullForSave), true);


            /* Check Property is primariy key with value or not primary but has value and either Table column or complex type and with search truue */

            //if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
            //{
            //    if ((notMapped.Count() == 0 || (complexType.Count() > 0 && isForSearch)) && ((!IsPrimaryKey(info)) || (IsPrimaryKey(info) && Convert.ToInt32(value, CultureInfo.InvariantCulture) > 0)))
            //    {
            //        return true;
            //    }

            //}
            //else
            if ((NullableDate.Count() > 0 && !isForSearch) || ((notMapped.Count() == 0 || (complexType.Count() > 0 && isForSearch)) && ((value != null && !IsPrimaryKey(info)) || (IsPrimaryKey(info) && Convert.ToInt32(value, CultureInfo.InvariantCulture) > 0))))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the Command DBType from Property
        /// </summary>
        /// <param name="type">Property Type</param>
        /// <returns>appropriate DBType</returns>
        private static DbType GetPropertyType(Type type)
        {
            // Match type Name and return DB Type
            switch (type.Name.ToLower(System.Globalization.CultureInfo.CurrentCulture))
            {
                case "byte[]":
                    return DbType.Binary;
                case "int64":
                case "int64?":
                case "long":
                case "long?":
                    return DbType.Int64;
                case "boolean":
                case "bool":
                case "bool?":
                    return DbType.Boolean;
                case "char":
                case "string":
                    return DbType.String;
                case "datetime":
                case "datetime?":
                    return DbType.DateTime;
                case "decimal":
                case "decimal?":
                    return DbType.Decimal;
                case "double":
                case "double?":
                case "float":
                case "float?":
                    return DbType.Double;
                case "int":
                case "int?":
                case "int32":
                case "int32?":
                    return DbType.Int32;
                case "int16":
                case "int16?":
                    return DbType.Int16;
                case "real":
                    return DbType.Single;
                case "guid":
                    return DbType.Guid;
                case "nullable`1":
                    if (type == typeof(Nullable<int>))
                    {
                        /* || type == typeof(Nullable<Int16>) || type == typeof(Nullable<Int32>))*/
                        return DbType.Int32;
                    }
                    else if (type == typeof(Nullable<long>))
                    {
                        /*|| type == typeof(Nullable<Int64>))*/
                        return DbType.Int64;
                    }
                    else if (type == typeof(Nullable<DateTime>))
                    {
                        return DbType.DateTime;
                    }
                    else if (type == typeof(Nullable<bool>))
                    {
                        /* || type == typeof(Nullable<Boolean>))*/
                        return DbType.Boolean;
                    }
                    else if (type == typeof(Nullable<decimal>))
                    {
                        /* || type == typeof(Nullable<Decimal>))*/
                        return DbType.Decimal;
                    }
                    else if (type == typeof(Nullable<float>) || type == typeof(Nullable<double>))
                    {
                        /* || type == typeof(Nullable<Double>))*/
                        return DbType.Double;
                    }
                    else
                    {
                        return DbType.String;
                    }
            }

            return DbType.String;
        }

        #endregion

        #region Default IDBContext Methods

        private bool HasDuplicateOracle<TEntity>(TEntity entity)
        {
            int count = 0;
            string query = string.Empty;

            query = "Select 1 from " + GetTableName(entity) + " where lower(" + GetKeyName(entity) + ") != '" + GetKeyValue(entity) + "'";
            if (!string.IsNullOrEmpty(this.Col1Name))
            {
                query += " and (lower(" + this.Col1Name + ") = '" + GetPropertyValue(entity, this.Col1Name).Replace("'", "''").ToLower() + "'";
            }
            if (!string.IsNullOrEmpty(this.Col2Name))
            {
                if (this.CombinationCheckRequired)
                {
                    query += " and lower(" + this.Col2Name + ") = '" + GetPropertyValue(entity, this.Col2Name).Replace("'", "''").ToLower() + "' )";
                }
                else
                {
                    query += " or lower(" + this.Col2Name + ") = '" + GetPropertyValue(entity, this.Col2Name).Replace("'", "''").ToLower() + "')";
                }
            }
            else
            {
                query += ")";
            }

            count = DapperDBClient.ExecuteQuery<TEntity>(query, new Collection<DBParameters>()).Count();

            if (count > 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Check Duplicate Records in Database
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Entity Model</param>
        /// <returns>returns entity is duplicate or not</returns>
        private bool HasDuplicate<TEntity>(TEntity entity)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            parameters.Add(new DBParameters() { Name = "tableName", Value = GetTableName(entity), DBType = DbType.String });
            parameters.Add(new DBParameters() { Name = "columnName", Value = this.Col1Name, DBType = DbType.String });
            parameters.Add(new DBParameters() { Name = "columnNameValue", Value = GetPropertyValue(entity, this.Col1Name).Replace("'", "''"), DBType = DbType.String });
            if (!string.IsNullOrEmpty(this.Col2Name))
            {
                parameters.Add(new DBParameters() { Name = "columnName2", Value = this.Col2Name, DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "columnName2Value", Value = GetPropertyValue(entity, this.Col2Name).Replace("'", "''"), DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "IsCombinationCheck", Value = this.CombinationCheckRequired, DBType = DbType.Boolean });
            }

            if (!string.IsNullOrEmpty(this.ParentColumnName))
            {
                parameters.Add(new DBParameters() { Name = "parentColumnKey", Value = this.ParentColumnName, DBType = DbType.String });

                parameters.Add(new DBParameters() { Name = "parentColumnKeyValue", Value = GetPropertyValue(entity, this.ParentColumnName).Replace("'", "''"), DBType = DbType.String });
            }

            parameters.Add(new DBParameters() { Name = "primaryKey", Value = GetKeyName(entity), DBType = DbType.String });

            parameters.Add(new DBParameters() { Name = "primaryKeyValue", Value = GetKeyValue(entity).ToString(), DBType = DbType.String });

            DataSet ds = (DataSet)DBClient.ExecuteProcedure("UspGeneralCheckDuplicate", ExecuteType.ExecuteDataSet, parameters);
            return ds.Tables[0].Rows.Count > 0;
        }

        public bool HasReferencesRecords<TEntity>(TEntity entity, int primaryKey)
        {
            if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
                parameters.Add(new DBParameters() { Name = "tableName", Value = GetTableName(entity), DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "primaryKeyValue", Value = primaryKey, DBType = DbType.Int64 });

                DataSet ds = (DataSet)DBClient.ExecuteProcedure("UspGeneralCheckReferences", ExecuteType.ExecuteDataSet, parameters);
                return ds.Tables[0].Rows.Count > 0;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region Protected Methods

        /// <summary>
        /// The virtual dispose method that allows
        /// classes inherited from this one to dispose their resources.
        /// </summary>
        /// <param name="disposing">Is Dispose</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    // Dispose managed resources here.
                }

                // Dispose unmanaged resources here.
            }
            DisposeConnection(Connection);
            this.disposed = true;
        }

        /// <summary>
        /// Return the Object of Model with data for given primary key value
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="primaryKey">primary key value of record to be select</param>
        /// <returns>Model Object</returns>
        public virtual TEntity SelectObject<TEntity>(int primaryKey)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();
            //// set primary Key value to object
            PropertyInfo info = entityObject.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == GetKeyName(entityObject).ToLower());
            if (info != null)
            {
                /*Set the Value to Model*/
                info.SetValue(entityObject, primaryKey, null);

                var list = SearchAllByID(entityObject, primaryKey);
                if (list.Count() > 0)
                {
                    return list.FirstOrDefault();
                }
            }

            return entityObject;
        }

        /// <summary>
        /// Execute Query with return entity list object
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public virtual IList<TEntity> ExecuteQuery<TEntity>(string strQuery, Collection<DBParameters> parameters)
        {
            //Replace @ to : in For Oracle as Oracle Support : for Parameters
            //Replace '[' & ']' with " For Oracle , backtick '`' for MySQL and no change for SQL.

            bool CMMSMasterConfigContext = false;

            if (strQuery.Contains("OPEN SYMMETRIC KEY"))
            {
                CMMSMasterConfigContext = true;
                ProjectSession.UseMasterDB = true;
            }

            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false && ProjectSession.UseHelpDB == false)
            {
                strQuery = strQuery.Replace("@", ":");
                strQuery = strQuery.Replace("[", "");
                strQuery = strQuery.Replace("]", "");
                strQuery = Regex.Replace(strQuery, "ISNULL", "NVL", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);

                strQuery = strQuery.Replace("dbo.", " ");
            }
            else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode() && ProjectSession.UseMasterDB == false && ProjectSession.UseHelpDB == false)
            {
                strQuery = strQuery.Replace("[", "`");
                strQuery = strQuery.Replace("]", "`");
                strQuery = Regex.Replace(strQuery, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);
                strQuery = strQuery.Replace("dbo.", " ");
                //strQuery.Replace("ISNULL(", "IFNULL(");
            }
            return DapperDBClient.ExecuteQuery<TEntity>(strQuery, parameters);

        }

        /// <summary>
        /// Search All Data From Passed Table By Primary Key
        /// </summary>
        /// <returns></returns>
        public virtual IList<TEntity> SearchAllByID<TEntity>(TEntity newObject, int primaryKey)
        {
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();


            string query = "SELECT * FROM " + GetTableName(entityObject) + " Where " + GetKeyName(entityObject) + "=" + primaryKey;

            return DapperDBClient.ExecuteQuery<TEntity>(query);
        }

        public virtual void SaveUserActivityOnInsertUpdate<TEntity>(TEntity newObject, bool isUpdate, int primaryKey = 0, bool isDeleted = false)
        {
            try
            {
                string tablename = newObject.GetType().Name;
                bool enableLog = false;

                enableLog = Convert.ToBoolean(HttpContext.Current.Session["EnableLog"]);

                if (tablename != "UserLogInLog" && tablename != "ErrorLog" && enableLog)
                {
                    PropertyInfo[] infos = newObject.GetType().GetProperties();
                    List<UserLog> userLog = new List<UserLog>();

                    if (primaryKey == 0)
                    {
                        primaryKey = Convert.ToInt32(infos.FirstOrDefault().GetValue(newObject, null));
                    }
                    int activityID = 0;

                    if (isUpdate)
                    {
                        activityID = UserActivity.Update.GetHashCode();
                        var oldObject = SelectObject<TEntity>(primaryKey);

                        //updated
                        foreach (PropertyInfo info in infos)
                        {
                            var keyName = info.Name.ToString();
                            var oldValue = Convert.ToString(info.GetValue(oldObject, null));
                            var newValue = Convert.ToString(info.GetValue(newObject, null));
                            if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName) && oldValue.ToString() != newValue.ToString())
                            {
                                userLog.Add(new UserLog() { Key = keyName, OldValue = oldValue, NewValue = newValue });
                            }
                        }

                    }
                    else
                    {

                        if (!isDeleted)
                        {
                            //inserted
                            activityID = UserActivity.Add.GetHashCode();
                        }
                        else
                        {
                            //deleted
                            activityID = UserActivity.Delete.GetHashCode();
                        }

                        newObject = SelectObject<TEntity>(primaryKey);
                        foreach (PropertyInfo info in infos)
                        {
                            var keyName = info.Name.ToString();
                            var newValue = Convert.ToString(info.GetValue(newObject, null));
                            if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName))
                            {
                                userLog.Add(new UserLog() { Key = keyName, NewValue = newValue });
                            }
                        }
                    }
                    JavaScriptSerializer jsonResult = new JavaScriptSerializer();
                    string output = jsonResult.Serialize(userLog);

                    int mainID = Convert.ToInt32(HttpContext.Current.Session["MainID"]);

                    //Add into useractivityLog
                    System.Collections.ObjectModel.Collection<DBParameters> activityParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "ActivityID",
                        Value = activityID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "EntityID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "TableName",
                        Value = tablename,
                        DBType = DbType.String
                    });

                    object primaryKeyValue = 0;
                    primaryKeyValue = DBClient.ExecuteProcedure("UspUserActivityLogSave", ExecuteType.ExecuteScalar, activityParameters);

                    //Add into useractivityLogDetails
                    System.Collections.ObjectModel.Collection<DBParameters> detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "DetailID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "UserActivityLogID",
                        Value = primaryKeyValue,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "RecordID",
                        Value = primaryKey,
                        DBType = DbType.Int32
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "ChangedValue",
                        Value = output,
                        DBType = DbType.String
                    });

                    detailParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.Int32
                    });


                    DBClient.ExecuteProcedure("UspUserActivityLogDetailSave", ExecuteType.ExecuteScalar, detailParameters);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// The dispose method that implements IDisposable.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            /*GC.SuppressFinalize(this);        */
        }

        /// <summary>
        /// Save the Current Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        public virtual int Save<TEntity>(TEntity entity)
        {
            //if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
            //{
            //    this.CheckForDuplicate = false;
            //}

            if ((this.CheckForDuplicate && (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() ? !this.HasDuplicateOracle(entity) : !this.HasDuplicate(entity))) || !this.CheckForDuplicate)
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity);

                PropertyInfo[] infos = entity.GetType().GetProperties();
                int primaryKey = Convert.ToInt32(infos.FirstOrDefault().GetValue(entity, null));
                string primarykeyColumn = infos.FirstOrDefault().Name.ToString();
                bool isUpdate = false;
                var updateIgnoreList = new List<string>();
                updateIgnoreList.Add(infos.FirstOrDefault().Name.ToString());
                updateIgnoreList.Add("CreatedBy");
                updateIgnoreList.Add("CreatedDate");

                var sqlQuery = "";
                var sqlQueryParams = "";
                var query = "";
                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                if (primaryKey > 0)
                {
                    isUpdate = true;

                    //Add into user activity log :: This record has been updated
                    SaveUserActivityOnInsertUpdate(entity, isUpdate);
                    SaveEmployeeHistory(entity, primaryKey);
                    sqlQuery += @"UPDATE " + GetTableName(entity) + " SET ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (!updateIgnoreList.Contains(parameter.Name))
                        {
                            sqlQuery += parameter.Name + " =  ";
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                sqlQuery += ":" + parameter.Name + ", ";
                            }
                            else
                            {
                                sqlQuery += "@" + parameter.Name + ", ";
                            }
                        }
                    }
                    sqlQuery = sqlQuery.Trim().TrimEnd(',');
                    sqlQuery += " WHERE " + (infos.FirstOrDefault().Name.ToString()) + "= " + primaryKey;
                    if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += " ; SELECT " + primaryKey;
                    }
                    else
                    {
                        sqlQuery += " returning " + primaryKey + " into :Id ";
                    }

                    query += sqlQuery;
                    primaryKeyValue = DapperDBClient.Execute<TEntity>(query, parameters);

                }
                else
                {
                    //Add into user activity log :: This record has been newly added
                    sqlQuery += @"INSERT INTO " + GetTableName(entity) + " ( ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        sqlQuery += parameter.Name + ",  ";
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            sqlQueryParams += ":" + parameter.Name + ", ";
                        }
                        else
                        {
                            sqlQueryParams += "@" + parameter.Name + ", ";
                        }
                    }

                    sqlQuery = sqlQuery.Trim().TrimEnd(',');

                    sqlQuery += ") VALUES (";

                    sqlQueryParams = sqlQueryParams.Trim().TrimEnd(',');

                    sqlQuery += sqlQueryParams + ")";

                    if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += "; SELECT @@IDENTITY";
                    }
                    else
                    {
                        string tablename = GetTableName(entity);
                        if (tablename != "ErrorLog")
                        {
                            sqlQuery += " RETURNING " + primarykeyColumn + " into :Id ";
                        }
                    }

                    query += sqlQuery;

                    primaryKeyValue = DapperDBClient.Execute<TEntity>(query, parameters);
                    SaveUserActivityOnInsertUpdate(entity, isUpdate, Convert.ToInt32(primaryKeyValue));
                }
                return Convert.ToInt32(primaryKeyValue, CultureInfo.InvariantCulture);
            }
            else
            {
                return -1;
            }
        }

        /// <summary>
        /// Save the Current Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        public virtual string SaveWithReturnString<TEntity>(TEntity entity, bool isUpdate = false)
        {
            if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
            {
                this.CheckForDuplicate = false;
            }

            if ((this.CheckForDuplicate && (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() ? !this.HasDuplicateOracle(entity) : !this.HasDuplicate(entity))) || !this.CheckForDuplicate)
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity);

                PropertyInfo[] infos = entity.GetType().GetProperties();
                string primaryKey = Convert.ToString(infos.FirstOrDefault().GetValue(entity, null));
                string primarykeyColumn = infos.FirstOrDefault().Name.ToString();

                var updateIgnoreList = new List<string>();
                updateIgnoreList.Add(infos.FirstOrDefault().Name.ToString());
                updateIgnoreList.Add("CreatedBy");
                updateIgnoreList.Add("CreatedDate");

                var sqlQuery = "";
                var sqlQueryParams = "";
                var query = "";
                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                if (isUpdate)
                {
                    isUpdate = true;

                    //Add into user activity log :: This record has been updated
                    SaveUserActivityOnInsertUpdate(entity, isUpdate);
                    //SaveEmployeeHistory(entity, primaryKey);
                    sqlQuery += @"UPDATE " + GetTableName(entity) + " SET ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (!updateIgnoreList.Contains(parameter.Name))
                        {
                            sqlQuery += parameter.Name + " =  ";
                            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                sqlQuery += ":" + parameter.Name + ", ";
                            }
                            else
                            {
                                sqlQuery += "@" + parameter.Name + ", ";
                            }
                        }
                    }
                    sqlQuery = sqlQuery.Trim().TrimEnd(',');
                    sqlQuery += " WHERE " + (infos.FirstOrDefault().Name.ToString()) + "= " + primaryKey;
                    if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += " ; SELECT " + primaryKey;
                    }
                    else
                    {
                        sqlQuery += " returning " + primaryKey + " into :Id ";
                    }

                    query += sqlQuery;
                    primaryKeyValue = DapperDBClient.ExecuteReturnString<TEntity>(query, parameters);

                }
                else
                {
                    //Add into user activity log :: This record has been newly added
                    sqlQuery += @"INSERT INTO " + GetTableName(entity) + " ( ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        sqlQuery += parameter.Name + ",  ";
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            sqlQueryParams += ":" + parameter.Name + ", ";
                        }
                        else
                        {
                            sqlQueryParams += "@" + parameter.Name + ", ";
                        }
                    }

                    sqlQuery = sqlQuery.Trim().TrimEnd(',');

                    sqlQuery += ") VALUES (";

                    sqlQueryParams = sqlQueryParams.Trim().TrimEnd(',');

                    sqlQuery += sqlQueryParams + ")";

                    if (ProjectSession.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += "; SELECT @@IDENTITY";
                    }
                    else
                    {
                        sqlQuery += " returning " + primarykeyColumn + " into :Id ";
                    }

                    query += sqlQuery;

                    primaryKeyValue = DapperDBClient.ExecuteReturnString<TEntity>(query, parameters);
                    //SaveUserActivityOnInsertUpdate(entity, isUpdate, Convert.ToInt32(primaryKeyValue));
                }
                return Convert.ToString(primaryKeyValue, CultureInfo.InvariantCulture);
            }
            else
            {
                return "-1";
            }
        }

        /// <summary>
        /// Delete the matching record with primary key value
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="primaryKey">primary key value of record to be delete</param>
        /// <returns>return deleted entity primary key value</returns>
        public virtual int Delete<TEntity>(int primaryKey, bool checkReferences = false)
        {
            if (ProjectSession.DbType != SystemEnum.DBTYPE.SQL.GetHashCode())
            {
                checkReferences = false;
            }
            int returnvalue;
            TEntity entityObject = default(TEntity);
            entityObject = Activator.CreateInstance<TEntity>();

            /*Execute Stored Procedure*/
            try
            {
                if ((checkReferences && !this.HasReferencesRecords(entityObject, primaryKey)) || !checkReferences)
                {
                    /*define Stored Procedure Name*/
                    string query = "DELETE FROM " + GetTableName(entityObject) + " Where " + GetKeyName(entityObject) + " = " + primaryKey;

                    //Add into user activity :: this record has been deleted :)
                    SaveUserActivityOnInsertUpdate(entityObject, false, primaryKey, true);

                    returnvalue = Convert.ToInt32(DapperDBClient.Execute(query));
                    if (returnvalue == -2) { return -2; };
                    return 0;
                }
                else
                {
                    return -2;
                }
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Executes the passed query
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="Parameters">Parameters</param>
        /// <returns></returns>
        public virtual int ExecuteQuery(string query, Collection<DBParameters> Parameters)
        {
            /*Execute Stored Procedure*/
            try
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("@", ":");
                    query = query.Replace("[", "");
                    query = query.Replace("]", "");
                    query = Regex.Replace(query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);

                    query = Regex.Replace(query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);

                    query = query.Replace("dbo.", " ");

                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("[", "`");
                    query = query.Replace("]", "`");
                    query = Regex.Replace(query, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "IFNULL(");
                    query = Regex.Replace(query, "GETDATE()", "Now()", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");
                }

                DapperDBClient.Execute(query, Parameters);
                return 0;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }


        /// <summary>
        /// Search All Data From Passed Table And Save those to Entity
        /// </summary>
        /// <returns></returns>
        public virtual IList<TEntity> SearchAll<TEntity>(TEntity entity)
        {
            try
            {
                TEntity entityObject = default(TEntity);
                entityObject = Activator.CreateInstance<TEntity>();                
                string query = "SELECT * FROM " + GetTableName(entityObject);                
                return DapperDBClient.ExecuteQuery<TEntity>(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity)
        {
            return this.Search<TEntity>(entity, 0, string.Empty, string.Empty);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo)
        {
            return this.Search<TEntity>(entity, pageNo, string.Empty, string.Empty);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection)
        {
            try
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);

                var SearchIgnoreList = new List<string>();
                SearchIgnoreList.Add("CreatedBy");
                SearchIgnoreList.Add("CreatedDate");


                var sqlQuery = "";

                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                sqlQuery += @"Select * from " + GetTableName(entity) + " Where 1 = 1 ";

                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    if (!SearchIgnoreList.Contains(parameter.Name))
                    {
                        sqlQuery += " AND " + parameter.Name + " =  @" + parameter.Name;
                    }
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    sqlQuery += " Order by " + sortExpression + " " + sortDirection;
                }



                if (ProjectSession.UseMasterDB == false && ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlQuery = sqlQuery.Replace("@", ":");
                }

                return this.ExecuteQuery<TEntity>(sqlQuery, parameters, pageNo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IList<TEntity> AdvanceSearch<TEntity>(string query, Collection<DBParameters> outParameters, int pageNo)
        {
            string whereClause = string.Empty;

            IList<TEntity> list;
            try
            {

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("@", ":");
                    query = query.Replace("[", "");
                    query = query.Replace("]", "");
                    query = Regex.Replace(query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");
                }

                list = this.ExecuteQuery<TEntity>(query, outParameters, pageNo);

                return list;

            }

            finally
            {

            }
        }

        public virtual IList<TEntity> AdvanceSearch<TEntity>(TEntity entity, string query, Collection<DBParameters> outParameters, int pageNo)
        {
            string whereClause = string.Empty;

            IList<TEntity> list;
            try
            {

                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("@", ":");
                    query = query.Replace("[", "");
                    query = query.Replace("]", "");
                    query = Regex.Replace(query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");
                }

                list = this.ExecuteQuery<TEntity>(query, outParameters, pageNo);

                return list;

            }

            finally
            {

            }
        }

        public virtual IList<TEntity> AdvanceSearch<TEntity>(string query, Collection<DBParameters> outParameters, int pageNo, int DbType, string connectionstring)
        {
            string whereClause = string.Empty;

            IList<TEntity> list;
            try
            {

                if (DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("@", ":");
                    query = query.Replace("[", "");
                    query = query.Replace("]", "");
                    query = Regex.Replace(query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");

                }
                else if (DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("[", "`");
                    query = query.Replace("]", "`");
                    query = Regex.Replace(query, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "GETDATE()", "Now()", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");
                }

                list = this.ExecuteQuery<TEntity>(query, outParameters, pageNo, DbType, connectionstring);

                return list;

            }

            finally
            {

            }
        }

        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection, string procedureName)
        {
            /*Add Parameters*/
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);
            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            if (!string.IsNullOrEmpty(sortExpression) && !string.IsNullOrEmpty(sortDirection))
            {
                parameters.Add(new DBParameters() { Name = "SortExpression", Value = sortExpression, DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "SortDirection", Value = sortDirection, DBType = DbType.String });
            }

            /*Convert Dataset to Model List object*/
            return this.ExecuteProcedure<TEntity>(procedureName, parameters);
        }

        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> Search<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection, int ActiveRecords = 0)
        {
            /*define Stored Procedure Name*/
            string procedureName = this.ProcedurePrefix + GetTableName(entity) + "Search";
            /*Add Parameters*/
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);
            if (this.StartRowIndex(pageNo) > 0 && this.EndRowIndex(pageNo) > 0)
            {
                parameters.Add(new DBParameters() { Name = "StartRowIndex", Value = this.StartRowIndex(pageNo), DBType = DbType.Int16 });
                parameters.Add(new DBParameters() { Name = "EndRowIndex", Value = this.EndRowIndex(pageNo), DBType = DbType.Int16 });
            }

            if (!string.IsNullOrEmpty(sortExpression) && !string.IsNullOrEmpty(sortDirection))
            {
                parameters.Add(new DBParameters() { Name = "SortExpression", Value = sortExpression, DBType = DbType.String });
                parameters.Add(new DBParameters() { Name = "SortDirection", Value = sortDirection, DBType = DbType.String });
            }

            if (ActiveRecords > 0)
            {
                parameters.Add(new DBParameters() { Name = "ActiveRecordsFilter", Value = ActiveRecords, DBType = DbType.Int16 });
            }

            /*Convert Dataset to Model List object*/
            return this.ExecuteProcedure<TEntity>(procedureName, parameters);
        }

        public virtual void SaveEmployeeHistory<TEntity>(TEntity newObject, int primaryKey)
        {
            try
            {
                string tablename = newObject.GetType().Name;

                if (tablename == "EmployeeInfo" && primaryKey > 0)
                {
                    PropertyInfo[] infos = newObject.GetType().GetProperties();

                    int activityID = UserActivity.Update.GetHashCode();
                    var oldObject = SelectObject<TEntity>(primaryKey);

                    int mainID = Convert.ToInt32(HttpContext.Current.Session["MainID"]);

                    //Add into EmployeeInfoLog
                    System.Collections.ObjectModel.Collection<DBParameters> activityParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "LogID",
                        Value = 0,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "PerformedOn",
                        Value = DateTime.Now,
                        DBType = DbType.DateTime
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "ActivityID",
                        Value = activityID,
                        DBType = DbType.Int32
                    });

                    activityParameters.Add(new DBParameters()
                    {
                        Name = "MainID",
                        Value = mainID,
                        DBType = DbType.String
                    });

                    object primaryKeyValue = 0;
                    primaryKeyValue = DBClient.ExecuteProcedure("UspEmployeeInfoLogSave", ExecuteType.ExecuteScalar, activityParameters);


                    //Add into EmployeeInfoLogDetails
                    System.Collections.ObjectModel.Collection<DBParameters> detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                    //updated
                    foreach (PropertyInfo info in infos)
                    {
                        var keyName = info.Name.ToString();
                        var oldValue = Convert.ToString(info.GetValue(oldObject, null));
                        var newValue = Convert.ToString(info.GetValue(newObject, null));
                        if (!Enum.GetNames(typeof(IgnoreColumnList)).Contains(keyName) && oldValue.ToString() != newValue.ToString())
                        {
                            detailParameters = new System.Collections.ObjectModel.Collection<DBParameters>();

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "DetailID",
                                Value = 0,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "LogID",
                                Value = primaryKeyValue,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "EmployeeID",
                                Value = primaryKey,
                                DBType = DbType.Int32
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "ColumnName",
                                Value = keyName,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "OldValue",
                                Value = oldValue,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "NewValue",
                                Value = newValue,
                                DBType = DbType.String
                            });

                            detailParameters.Add(new DBParameters()
                            {
                                Name = "MainID",
                                Value = mainID,
                                DBType = DbType.Int32
                            });


                            DBClient.ExecuteProcedure("UspEmployeeInfoLogDetailSave", ExecuteType.ExecuteScalar, detailParameters);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ExecuteScalar(string query)
        {
            try
            {
                return DapperDBClient.ExecuteScalar(query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IList<TEntity> ApplyPaging<TEntity>(IList<TEntity> lst, int? pageNo)
        {
            return this.ApplyPagging<TEntity>(lst, pageNo);
        }

        public virtual IList<TEntity> SearchWithConnectionString<TEntity>(TEntity entity, int? pageNo, string sortExpression, string sortDirection, int DBType, string Connectionsting)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity, true);

            var SearchIgnoreList = new List<string>();
            SearchIgnoreList.Add("CreatedBy");
            SearchIgnoreList.Add("CreatedDate");

            var sqlQuery = "";

            /*Execute Stored Procedure*/
            object primaryKeyValue = new object();
            // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

            sqlQuery += @"Select * from " + GetTableName(entity) + " Where 1 = 1 ";

            /*Add different Parameter from Model object Property*/
            foreach (DBParameters parameter in parameters)
            {
                if (!SearchIgnoreList.Contains(parameter.Name))
                {
                    sqlQuery += " AND " + parameter.Name + " =  @" + parameter.Name;
                }
            }

            if (!string.IsNullOrEmpty(sortExpression))
            {
                sqlQuery += " Order by " + sortExpression + " " + sortDirection;
            }



            //if (ProjectSession.UseMasterDB == false && ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
            //{
            //    sqlQuery = sqlQuery.Replace("@", ":");
            //}

            Connectionsting = System.Configuration.ConfigurationManager.ConnectionStrings["CMMSDBContext"].ConnectionString;
            return this.ExecuteQuery<TEntity>(sqlQuery, parameters, pageNo, DBType, Connectionsting);

        }

        #endregion

        #region "Transaction Related Methods"

        /// <summary>
        /// Executes the passed query
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="Parameters">Parameters</param>
        /// <returns></returns>
        public virtual int ExecuteQuery(string query, IDbConnection connection, Collection<DBParameters> Parameters)
        {
            /*Execute Stored Procedure*/
            try
            {
                if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("@", ":");
                    query = query.Replace("[", "");
                    query = query.Replace("]", "");
                    query = Regex.Replace(query, "ISNULL", "NVL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "NVL(");
                    query = Regex.Replace(query, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                    query = Regex.Replace(query, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                    query = query.Replace("dbo.", " ");

                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode() && ProjectSession.UseMasterDB == false)
                {
                    query = query.Replace("[", "`");
                    query = query.Replace("]", "`");
                    query = Regex.Replace(query, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "IFNULL(");
                    query = Regex.Replace(query, "GETDATE()", "Now()", RegexOptions.IgnoreCase);  //query.Replace("ISNULL(", "IFNULL(");

                    query = query.Replace("dbo.", " ");
                }

                DapperDBClient.Execute(query, connection, _Transaction, Parameters);
                return 0;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Execute Query with return entity list object
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public virtual IList<TEntity> ExecuteQuery<TEntity>(string strQuery, IDbConnection connection, Collection<DBParameters> parameters)
        {
            //Replace @ to : in For Oracle as Oracle Support : for Parameters
            //Replace '[' & ']' with " For Oracle , backtick '`' for MySQL and no change for SQL.
            if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode() && ProjectSession.UseMasterDB == false)
            {
                strQuery = strQuery.Replace("@", ":");
                strQuery = strQuery.Replace("[", "");
                strQuery = strQuery.Replace("]", "");
                strQuery = Regex.Replace(strQuery, "ISNULL", "NVL", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "cleaninginspectiongroupsequencenumber", "cigsequencenumber", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "irapprovallevelmappings_employees", "irapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "prapprovallevelmappings_employees", "prapprovallevelmappings_employ", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_employees", "emailnotificationrules_employe", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_escalation", "emailnotificationrules_esc", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_location", "emailnotificationrules_locatio", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_maintsubdept", "emailnotificationrules_subdept", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_workpriority", "emailrules_workpriority", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_workstatus", "emailrules_workstatus", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktype", "emailrules_worktype", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_worktrade", "emailrules_worktrade", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "emailnotificationrules_processedrule", "emailnotificationrules_process", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "notificationruleprocessedruleid", "notificationruleprocessedrulei", RegexOptions.IgnoreCase);
                strQuery = Regex.Replace(strQuery, "GETDATE()", "SysDate", RegexOptions.IgnoreCase);

                strQuery = strQuery.Replace("dbo.", " ");
            }
            else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode() && ProjectSession.UseMasterDB == false)
            {
                strQuery = strQuery.Replace("[", "`");
                strQuery = strQuery.Replace("]", "`");
                strQuery = Regex.Replace(strQuery, "ISNULL", "IFNULL", RegexOptions.IgnoreCase);

                strQuery = strQuery.Replace("dbo.", " ");
                //strQuery.Replace("ISNULL(", "IFNULL(");
            }
            return DapperDBClient.Execute<TEntity>(strQuery, connection, _Transaction, parameters);

        }

        public virtual IDbConnection OpenConnection()
        {
            try
            {
                return DapperDBClient.OpenConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void DisposeConnection(IDbConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
            }
        }

        public virtual void BeginTransaction()
        {
            try
            {
                if (Connection != null && Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }

                _Transaction = DapperDBClient.BeginTransaction(Connection);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void CommitTransaction()
        {
            try
            {
                if (_Transaction != null)
                {
                    DapperDBClient.CommitTransaction(_Transaction);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void RollBackTransaction()
        {
            try
            {
                if (_Transaction != null)
                {
                    DapperDBClient.RollBackTransaction(_Transaction);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Email Dispatcher Related Methods"

        /// <summary>
        /// Execute Query with return entity list object
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="strQuery"></param>
        /// <returns></returns>
        public virtual IList<TEntity> ExecuteQueryWithClientID<TEntity>(string strQuery, Collection<DBParameters> parameters, string clientCode = "")
        {
            return DapperDBClient.ExecuteQueryWithClientID<TEntity>(strQuery, parameters, clientCode);
        }

        public virtual List<TEntity> GetPageList<TEntity>(IList<TEntity> list, int page, int pageSize)
        {
            return list.Skip(page * pageSize).Take(pageSize).ToList();            
        }

        public virtual IList<TEntity> SearchAllWithClientID<TEntity>(TEntity entity, string clientCode)
        {
            try
            {
                TEntity entityObject = default(TEntity);
                entityObject = Activator.CreateInstance<TEntity>();


                string query = "SELECT * FROM " + GetTableName(entityObject);

                return DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, new Collection<DBParameters>(), clientCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public virtual CMMS.Model.Account GetAccountDetailsByClientCode(string clientCode)
        {
            CMMS.Model.Account objAccount = null;
            try
            {                
                objAccount = DapperDBClient.GetAccountConnectionDetails(clientCode).FirstOrDefault();
                return objAccount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual CMMS.Model.FirstLevelTbl GetFirstLevelTbl(int clientId)
        {
            CMMS.Model.FirstLevelTbl objFirstLevelTbl = null;
            try
            {
                objFirstLevelTbl = DapperDBClient.GetFirstLevelTbl(clientId).FirstOrDefault();
                return objFirstLevelTbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual CMMS.Model.LevelNamesTbl GetLevelNamesTbl(int L1_ID)
        {
            CMMS.Model.LevelNamesTbl objLevelNamesTbl = null;
            try
            {
                objLevelNamesTbl = DapperDBClient.GetLevelNamesTbl(L1_ID).FirstOrDefault();
                return objLevelNamesTbl;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        


        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> SearchWithClientID<TEntity>(TEntity entity, int pageNo, string sortExpression, string sortDirection, string clientCode)
        {
            try
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParametersWithClientID(entity, clientCode, true);

                var SearchIgnoreList = new List<string>();
                SearchIgnoreList.Add("CreatedBy");
                SearchIgnoreList.Add("CreatedDate");

                var sqlQuery = "";

                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                sqlQuery += @"Select * from " + GetTableName(entity) + " Where 1 = 1 ";

                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    if (!SearchIgnoreList.Contains(parameter.Name))
                    {
                        sqlQuery += " AND " + parameter.Name + " =  @" + parameter.Name;
                    }
                }

                if (!string.IsNullOrEmpty(sortExpression))
                {
                    sqlQuery += " Order by " + sortExpression + " " + sortDirection;
                }

                return this.ExecuteQueryWithPagingANDClientID<TEntity>(sqlQuery, parameters, pageNo, clientCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Executes the passed query
        /// </summary>
        /// <param name="query">Query</param>
        /// <param name="Parameters">Parameters</param>
        /// <returns></returns>
        public virtual int ExecuteWithClientID(string query, Collection<DBParameters> Parameters, string clientCode)
        {
            /*Execute Stored Procedure*/
            try
            {
               DapperDBClient.ExecuteWithClientID(query, Parameters, clientCode);
               return 0;
            }
            catch (System.Data.SqlClient.SqlException sqlEx)
            {
                if (sqlEx.Number == 50000 || sqlEx.Number == 547)
                {
                    return -1;
                }
                else
                {
                    throw;
                }
            }
        }

        private static System.Collections.ObjectModel.Collection<DBParameters> AddParametersWithClientID<TEntity>(TEntity entity, string clientCode, bool isForSearch = false)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = new System.Collections.ObjectModel.Collection<DBParameters>();
            PropertyInfo[] infos = entity.GetType().GetProperties();
            foreach (PropertyInfo info in infos)
            {
                var value = info.GetValue(entity, null);

                // Verify Property Validation and than add as paramter
                if (ParameterValidation(info, value, isForSearch))
                {
                    if (info.PropertyType == typeof(string))
                    {
                        /* Added by Darshit Babariya 02 September 2013
                         To add trim functionality in All string parameters                        */
                        value = value == null ? null : value.ToString().Trim();
                    }

                    int dbType = 1;
                    if (!string.IsNullOrWhiteSpace(clientCode))
                    {
                        CMMS.Model.Account objAccount = null;

                        objAccount = DapperDBClient.GetAccountConnectionDetails(clientCode).FirstOrDefault();

                        if (objAccount != null && objAccount.DbType != null && !string.IsNullOrEmpty(objAccount.DbConString))
                        {
                            dbType = Convert.ToInt32(objAccount.DbType);
                        }
                    }

                    if (dbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        if (info.Name.ToLower() == "date")
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = "date_",
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                        else if (info.Name.ToLower() == "comment")
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = "Comment_",
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                        else
                        {
                            parameters.Add(new DBParameters()
                            {
                                Name = info.Name,
                                Value = value,
                                DBType = GetPropertyType(info.PropertyType)
                            });
                        }
                    }
                    else
                    {
                        parameters.Add(new DBParameters()
                        {
                            Name = info.Name,
                            Value = value,
                            DBType = GetPropertyType(info.PropertyType)
                        });
                    }

                }
            }

            return parameters;
        }


        /// <summary>
        /// Return the list of model for given search criteria
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model with Search Criteria</param>
        /// <param name="pageNo">current page no</param>
        /// <param name="sortExpression">Sort Expression</param>
        /// <param name="sortDirection">Sort Direction</param>
        /// <returns>List of Models</returns>
        public virtual IList<TEntity> UpdateWithClientID<TEntity>(TEntity entity, string WhereCause, string clientCode)
        {
            try
            {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParametersWithClientID(entity, clientCode, true);

                var SearchIgnoreList = new List<string>();
                SearchIgnoreList.Add("CreatedBy");
                SearchIgnoreList.Add("CreatedDate");

                var sqlQuery = "";

                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                sqlQuery += @"Update " + GetTableName(entity) + " Set ";

                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    if (!SearchIgnoreList.Contains(parameter.Name))
                    {
                        sqlQuery += " " + parameter.Name + " =  @" + parameter.Name + " ,";
                    }
                }

                sqlQuery = sqlQuery.Trim(' ').Trim(',');

                sqlQuery = sqlQuery + " where " + WhereCause;


                return this.ExecuteQueryWithPagingANDClientID<TEntity>(sqlQuery, parameters, null, clientCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Save the Current Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        public virtual IList<TEntity> SaveWithClientID<TEntity>(TEntity entity, string clientCode, bool isInsert = false)        
        {
                System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity);

                CMMS.Model.Account account = GetAccountDetailsByClientCode(clientCode);

                PropertyInfo[] infos = entity.GetType().GetProperties();

                bool isPrimaryKeyString = false;
                if (infos.FirstOrDefault().GetValue(entity, null).GetType().Name.Equals("String"))
                {
                    isPrimaryKeyString = true;
                }

                int primaryKey = 0;
                string strprimaryKey = string.Empty;

                if (isPrimaryKeyString)
                {
                    strprimaryKey= Convert.ToString(infos.FirstOrDefault().GetValue(entity, null)); 
                }
                else
                {
                   primaryKey= Convert.ToInt32(infos.FirstOrDefault().GetValue(entity, null));
                }
                
                string primarykeyColumn = infos.FirstOrDefault().Name.ToString();
                
                var updateIgnoreList = new List<string>();
                updateIgnoreList.Add(infos.FirstOrDefault().Name.ToString());
                updateIgnoreList.Add("CreatedBy");
                updateIgnoreList.Add("CreatedDate");

                var sqlQuery = "";
                var sqlQueryParams = "";
                var query = "";
                /*Execute Stored Procedure*/
                object primaryKeyValue = new object();
                // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

                if ((!isInsert) && (primaryKey > 0 || !string.IsNullOrWhiteSpace(strprimaryKey)))
                {                                        
                    sqlQuery += @"UPDATE " + GetTableName(entity) + " SET ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        if (!updateIgnoreList.Contains(parameter.Name))
                        {
                            sqlQuery += parameter.Name + " =  ";
                            if (account.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                            {
                                sqlQuery += ":" + parameter.Name + ", ";
                            }
                            else
                            {
                                sqlQuery += "@" + parameter.Name + ", ";
                            }
                        }
                    }
                    sqlQuery = sqlQuery.Trim().TrimEnd(',');
                    sqlQuery += " WHERE " + (infos.FirstOrDefault().Name.ToString()) + "= " + (isPrimaryKeyString == true ? "'" + strprimaryKey + "'" : primaryKey.ToString());
                    if (account.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += " ; SELECT " + (isPrimaryKeyString == true ? "'" + strprimaryKey + "'" : primaryKey.ToString()) + " as '" + primarykeyColumn + "'";
                    }
                    else
                    {
                        sqlQuery += " returning " + primaryKey + " into :" + primarykeyColumn;
                    }

                    query += sqlQuery;
                    //primaryKeyValue = DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                    return DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);                    


                    //DapperDBClient.ExecuteQueryWithClientID

                }
                else
                {
                    //Add into user activity log :: This record has been newly added
                    sqlQuery += @"INSERT INTO " + GetTableName(entity) + " ( ";

                    /*Add different Parameter from Model object Property*/
                    foreach (DBParameters parameter in parameters)
                    {
                        sqlQuery += parameter.Name + ",  ";
                        if (account.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            sqlQueryParams += ":" + parameter.Name + ", ";
                        }
                        else
                        {
                            sqlQueryParams += "@" + parameter.Name + ", ";
                        }
                    }

                    sqlQuery = sqlQuery.Trim().TrimEnd(',');

                    sqlQuery += ") VALUES (";

                    sqlQueryParams = sqlQueryParams.Trim().TrimEnd(',');

                    sqlQuery += sqlQueryParams + ")";

                    if (account.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQuery += "; SELECT @@IDENTITY" + " as '" + primarykeyColumn + "'";
                    }
                    else
                    {
                        sqlQuery += " RETURNING " + primarykeyColumn + " into :Id";
                    }

                    query += sqlQuery;

                    //primaryKeyValue = DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                    return  DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);                    

                    //SaveUserActivityOnInsertUpdate(entity, isUpdate, Convert.ToInt32(primaryKeyValue));
                }
                //return primaryKeyValue;
          
        }



        /// <summary>
        /// Save the Current Model
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        //public virtual IList<TEntity> SaveWithClientID<TEntity>(TEntity entity, string clientCode, bool isInsert = false)
        public virtual string SaveWithClientID2<TEntity>(TEntity entity, string clientCode, bool isInsert = false)
        {
            System.Collections.ObjectModel.Collection<DBParameters> parameters = AddParameters(entity);

            CMMS.Model.Account account = GetAccountDetailsByClientCode(clientCode);

            PropertyInfo[] infos = entity.GetType().GetProperties();

            bool isPrimaryKeyString = false;
            if (infos.FirstOrDefault().GetValue(entity, null).GetType().Name.Equals("String"))
            {
                isPrimaryKeyString = true;
            }

            int primaryKey = 0;
            string strprimaryKey = string.Empty;

            if (isPrimaryKeyString)
            {
                strprimaryKey = Convert.ToString(infos.FirstOrDefault().GetValue(entity, null));
            }
            else
            {
                primaryKey = Convert.ToInt32(infos.FirstOrDefault().GetValue(entity, null));
            }

            string primarykeyColumn = infos.FirstOrDefault().Name.ToString();

            var updateIgnoreList = new List<string>();
            updateIgnoreList.Add(infos.FirstOrDefault().Name.ToString());
            updateIgnoreList.Add("CreatedBy");
            updateIgnoreList.Add("CreatedDate");

            var sqlQuery = "";
            var sqlQueryParams = "";
            var query = "";
            /*Execute Stored Procedure*/
            object primaryKeyValue = new object();
            // ProjectSession.DbType = SystemEnum.DBTYPE.ORACLE.GetHashCode();

            if ((!isInsert) && (primaryKey > 0 || !string.IsNullOrWhiteSpace(strprimaryKey)))
            {
                sqlQuery += @"UPDATE " + GetTableName(entity) + " SET ";

                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    if (!updateIgnoreList.Contains(parameter.Name))
                    {
                        sqlQuery += parameter.Name + " =  ";
                        if (account.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            sqlQuery += ":" + parameter.Name + ", ";
                        }
                        else
                        {
                            sqlQuery += "@" + parameter.Name + ", ";
                        }
                    }
                }
                sqlQuery = sqlQuery.Trim().TrimEnd(',');
                sqlQuery += " WHERE " + (infos.FirstOrDefault().Name.ToString()) + "= " + (isPrimaryKeyString == true ? "'" + strprimaryKey + "'" : primaryKey.ToString());
                if (account.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlQuery += " ; SELECT " + (isPrimaryKeyString == true ? "'" + strprimaryKey + "'" : primaryKey.ToString()) + " as '" + primarykeyColumn + "'";
                }
                else
                {
                    sqlQuery += " returning " + primaryKey + " into :Id";
                }

                query += sqlQuery;
                //primaryKeyValue = DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                //return DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                return DapperDBClient.ExecuteReturnStringWithClientID<TEntity>(query, clientCode, parameters);


                //DapperDBClient.ExecuteQueryWithClientID

            }
            else
            {
                //Add into user activity log :: This record has been newly added
                sqlQuery += @"INSERT INTO " + GetTableName(entity) + " ( ";

                /*Add different Parameter from Model object Property*/
                foreach (DBParameters parameter in parameters)
                {
                    sqlQuery += parameter.Name + ",  ";
                    if (account.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    {
                        sqlQueryParams += ":" + parameter.Name + ", ";
                    }
                    else
                    {
                        sqlQueryParams += "@" + parameter.Name + ", ";
                    }
                }

                sqlQuery = sqlQuery.Trim().TrimEnd(',');

                sqlQuery += ") VALUES (";

                sqlQueryParams = sqlQueryParams.Trim().TrimEnd(',');

                sqlQuery += sqlQueryParams + ")";

                if (account.DbType != SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlQuery += "; SELECT @@IDENTITY" + " as '" + primarykeyColumn + "'";
                }
                else
                {
                    //sqlQuery += " returning " + primaryKey + " into :" + primarykeyColumn;
                    sqlQuery += " RETURNING " + primarykeyColumn + " into :Id"; 
                }

                query += sqlQuery;

                //primaryKeyValue = DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                //return  DapperDBClient.ExecuteQueryWithClientID<TEntity>(query, parameters, clientCode);

                return DapperDBClient.ExecuteReturnStringWithClientIDTest<TEntity>(query, clientCode, parameters);

                //SaveUserActivityOnInsertUpdate(entity, isUpdate, Convert.ToInt32(primaryKeyValue));
            }
            //return primaryKeyValue;

        }





        /// <summary>
        /// Get Master Account table detail
        /// </summary>
        /// <typeparam name="TEntity">Model Type</typeparam>
        /// <param name="entity">Model to Save</param>
        /// <returns>Save records id value</returns>
        public virtual CMMS.Model.Account GetMasterAccountDetail(string clientCode)
        {
            CMMS.Model.Account objAccount = null;
            objAccount = DapperDBClient.GetAccountConnectionDetails(clientCode).FirstOrDefault();
            return objAccount;
        }


        #endregion
    }
}
