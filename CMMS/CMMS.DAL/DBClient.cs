﻿//-----------------------------------------------------------------------
// <copyright file="DBClient.cs" company="TatvaSoft">
//     Copyright (c) TatvaSoft. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace CMMS.DAL
{
    using MySql.Data.MySqlClient;
    using Oracle.DataAccess.Client;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using CMMS.Infrastructure;
    using Dapper;
    /// <summary>
    /// Define the possible execution type for Procedure
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    public enum ExecuteType
    {
        /// <summary>
        /// Execute as Scalar
        /// </summary>
        ExecuteScalar,

        /// <summary>
        /// Execute as Dataset
        /// </summary>
        ExecuteDataSet,

        /// <summary>
        /// Execute as non query
        /// </summary>
        ExecuteNonQuery,

        /// <summary>
        /// Execute as Reader
        /// </summary>
        ExecuteReader
    }

    /// <summary>
    /// This class is to define common methods for do the DBCall using Stored Procedure with SQL Command 
    /// </summary>
    /// <CreatedBy>Nikhil Rupada</CreatedBy>
    /// <CreatedDate>08-Sep-2015</CreatedDate>
    /// <ModifiedBy></ModifiedBy>
    /// <ModifiedDate></ModifiedDate>
    /// <ReviewBy></ReviewBy>
    /// <ReviewDate></ReviewDate>
    internal sealed class DBClient : DataWorker
    {
        #region Property/Enum

        /// <summary>
        /// Prevents a default instance of the DBClient class from being created.
        /// </summary>
        private DBClient()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <param name="procedureName">procedure name</param>
        /// <param name="executeType">execute type</param>
        /// <param name="parameters">procedure parameter</param>
        /// <param name="databaseConnection">Database Connection String</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static object ExecuteProcedure(string procedureName, ExecuteType executeType, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            object returnValue;
            //// Create a suitable command type and add the required parameter
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                IDbCommand sqlCommand = database.CreateStoredProcCommand(procedureName, connection);
                /*Add different Parameter from Model object Property*/
                AddParameters(ref sqlCommand, parameters);

                /*Execute Procedure from supplied Execution type*/

                if (executeType == ExecuteType.ExecuteScalar)
                {
                    returnValue = sqlCommand.ExecuteScalar();
                }
                else if (executeType == ExecuteType.ExecuteDataSet)
                {
                    DataSet dataSet = new DataSet();
                    DataAdapter sqlAdapter = null;

                    dataSet.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    { sqlAdapter = new SqlDataAdapter((SqlCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    { sqlAdapter = new OracleDataAdapter((OracleCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                    { sqlAdapter = new MySqlDataAdapter((MySqlCommand)sqlCommand); }

                    sqlAdapter.Fill(dataSet);
                    returnValue = dataSet;
                }
                else if (executeType == ExecuteType.ExecuteNonQuery)
                {
                    returnValue = sqlCommand.ExecuteNonQuery();
                }
                else if (executeType == ExecuteType.ExecuteReader)
                {
                    returnValue = sqlCommand.ExecuteReader();
                }
                else
                {
                    returnValue = "No Proper execute type provide";
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Executes the procedurewith OutParameter.
        /// This is added by Juhi Paunikar to get out parameter
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="executeType">Type of the execute.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="databaseConnection">The database connection.</param>
        /// <param name="dictionary">The dic.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static object ExecuteProcedurewithOutParameter(string procedureName, ExecuteType executeType, System.Collections.ObjectModel.Collection<DBParameters> parameters, out Dictionary<string, string> dictionary)
        {
            dictionary = new Dictionary<string, string>();
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            object returnValue;
            //// Create a suitable command type and add the required parameter
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                IDbCommand sqlCommand = database.CreateStoredProcCommand(procedureName, connection);

                /*Add different Parameter from Model object Property*/
                AddParameters(ref sqlCommand, parameters);

                /*Execute Procedure from supplied Execution type*/

                if (executeType == ExecuteType.ExecuteScalar)
                {
                    returnValue = sqlCommand.ExecuteScalar();
                }
                else if (executeType == ExecuteType.ExecuteDataSet)
                {
                    DataSet dataSet = new DataSet();
                    dataSet.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    DataAdapter sqlAdapter = null;

                    dataSet.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    { sqlAdapter = new SqlDataAdapter((SqlCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    { sqlAdapter = new OracleDataAdapter((OracleCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                    { sqlAdapter = new MySqlDataAdapter((MySqlCommand)sqlCommand); }

                    sqlAdapter.Fill(dataSet);
                    returnValue = dataSet;
                }
                else if (executeType == ExecuteType.ExecuteNonQuery)
                {
                    returnValue = sqlCommand.ExecuteNonQuery();
                }
                else if (executeType == ExecuteType.ExecuteReader)
                {
                    returnValue = sqlCommand.ExecuteReader();
                }
                else
                {
                    returnValue = "No Proper execute type provide";
                }

                if (parameters.Any(a => a.Direction == ParameterDirection.Output))
                {
                    string paramName = string.Empty;
                    string paramValue = string.Empty;

                    foreach (var db in parameters.Where(w => w.Direction == ParameterDirection.Output))
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            paramName = ((SqlCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((SqlCommand)sqlCommand).Parameters[db.Name].Value);
                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            paramName = ((OracleCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((OracleCommand)sqlCommand).Parameters[db.Name].Value);
                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            paramName = ((MySqlCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((MySqlCommand)sqlCommand).Parameters[db.Name].Value);
                        }

                        dictionary.Add(paramName, paramValue);
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Execute the Stored Procedure with given Parameters
        /// </summary>
        /// <typeparam name="T">Entity Type</typeparam>
        /// <param name="procedureName">procedure name</param>
        /// <param name="parameters">procedure parameter</param>
        /// <param name="databaseConnection">Database Connection String</param>
        /// <returns>return execute procedure </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteProcedure<T>(string procedureName, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            try
            {
                /* Create Database object
                Database db = DatabaseFactory.CreateDatabase();*/
                List<T> returnValue = new List<T>();

                // Create a suitable command type and add the required parameter
                using (IDbConnection connection = database.CreateOpenConnection())
                {
                    IDbCommand sqlCommand = database.CreateStoredProcCommand(procedureName, connection);

                    /*Add different Parameter from Model object Property*/
                    AddParameters(ref sqlCommand, parameters);

                    /*Execute Procedure from supplied Execution type*/
                    //DataSet ds = new DataSet();
                    //DataAdapter da = new SqlDataAdapter((SqlCommand)sqlCommand);

                    DataSet dataSet = new DataSet();
                    DataAdapter sqlAdapter = null;
                    dataSet.Locale = System.Globalization.CultureInfo.InvariantCulture;
                    if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                    { sqlAdapter = new SqlDataAdapter((SqlCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                    { sqlAdapter = new OracleDataAdapter((OracleCommand)sqlCommand); }
                    else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                    { sqlAdapter = new MySqlDataAdapter((MySqlCommand)sqlCommand); }
                    sqlAdapter.Fill(dataSet);

                    //da.Fill(ds);

                    returnValue = DataReaderToList<T>(sqlCommand.ExecuteReader());
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Executes the procedurewith out value.
        /// This is Added by Juhi Paunikar with out value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="databaseConnection">The database connection.</param>
        /// <param name="dic">The dic.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "This will only return predefined Procedure Name, not user inputs")]
        public static IList<T> ExecuteProcedurewithOutParameter<T>(string procedureName, System.Collections.ObjectModel.Collection<DBParameters> parameters, out Dictionary<string, string> dic)
        {
            dic = new Dictionary<string, string>();
            /* Create Database object
            Database db = DatabaseFactory.CreateDatabase();*/
            List<T> returnValue = new List<T>();

            // Create a suitable command type and add the required parameter
            using (IDbConnection connection = database.CreateOpenConnection())
            {
                IDbCommand sqlCommand = database.CreateStoredProcCommand(procedureName, connection);

                /*Add different Parameter from Model object Property*/
                AddParameters(ref sqlCommand, parameters);

                /*Execute Procedure from supplied Execution type*/
                returnValue = DataReaderToList<T>(sqlCommand.ExecuteReader());

                if (parameters.Any(a => a.Direction == ParameterDirection.Output))
                {
                    string paramName = string.Empty;
                    string paramValue = string.Empty;

                    foreach (var db in parameters.Where(w => w.Direction == ParameterDirection.Output))
                    {
                        if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                        {
                            paramName = ((SqlCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((SqlCommand)sqlCommand).Parameters[db.Name].Value);
                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                        {
                            paramName = ((OracleCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((OracleCommand)sqlCommand).Parameters[db.Name].Value);
                        }
                        else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                        {
                            paramName = ((MySqlCommand)sqlCommand).Parameters[db.Name].ParameterName;
                            paramValue = Convert.ToString(((MySqlCommand)sqlCommand).Parameters[db.Name].Value);
                        }

                        dic.Add(paramName, paramValue);
                    }
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Add Parameter from Model Object Property
        /// </summary>
        /// <param name="sqlCommand">Database object</param>
        /// <param name="parameters">Command Object</param>
        private static void AddParameters(ref IDbCommand sqlCommand, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            foreach (DBParameters parameter in parameters)
            {
                DbParameter sqlParameter = new SqlParameter();

                if (ProjectSession.DbType == SystemEnum.DBTYPE.SQL.GetHashCode())
                {
                    if (parameter.IsSqlDbType)
                    {
                        sqlParameter = new SqlParameter()
                        {
                            SqlDbType = parameter.SqlDbType,
                            Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                            ParameterName = parameter.Name,
                            Value = parameter.Value,
                        };
                    }
                    else
                    {
                        sqlParameter = new SqlParameter()
                        {
                            DbType = parameter.DBType,
                            Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                            ParameterName = parameter.Name,
                            Value = parameter.Value,
                        };
                    }
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.MYSQL.GetHashCode())
                {
                    sqlParameter = new MySqlParameter()
                    {
                        Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                        ParameterName = parameter.Name,
                        Value = parameter.Value,
                    };
                }
                else if (ProjectSession.DbType == SystemEnum.DBTYPE.ORACLE.GetHashCode())
                {
                    sqlParameter = new OracleParameter()
                    {
                        Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                        ParameterName = parameter.Name,
                        Value = parameter.Value,
                    };
                }

                //if (parameter.IsSqlDbType)
                //{
                //    sqlParameter = new SqlParameter()
                //   {
                //       SqlDbType = parameter.SqlDbType,
                //       Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                //       ParameterName = parameter.Name,
                //       Value = parameter.Value,
                //   };
                //}
                //else
                //{
                //    sqlParameter = new SqlParameter()
                //   {
                //       DbType = parameter.DBType,
                //       Direction = parameter.Direction == 0 ? ParameterDirection.Input : parameter.Direction,
                //       ParameterName = parameter.Name,
                //       Value = parameter.Value,
                //   };

                //}

                if (parameter.Direction == ParameterDirection.Output && parameter.DBType == DbType.String)
                {
                    sqlParameter.Size = 500;
                }

                sqlCommand.Parameters.Add(sqlParameter);
            }
        }

        /// <summary>
        /// Add Parameter from Model Object Property
        /// </summary>
        /// <param name="sqlCommand">Database object</param>
        /// <param name="parameters">Command Object</param>
        private static void AddDynamicParameters(ref DynamicParameters dynamicparam, System.Collections.ObjectModel.Collection<DBParameters> parameters)
        {
            foreach (DBParameters parameter in parameters)
            {
                dynamicparam.Add(parameter.Name, parameter.Value);
            }
        }

        /// <summary>
        /// Convert Data Reader to List
        /// </summary>
        /// <typeparam name="T">Entity Object</typeparam>
        /// <param name="dr">data reader object</param>
        /// <returns>return list of objects</returns>
        private static List<T> DataReaderToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();

            T obj = default(T);

            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();

                for (int i = 0; i < dr.FieldCount; i++)
                {
                    PropertyInfo info = obj.GetType().GetProperties().FirstOrDefault(o => o.Name.ToLower() == dr.GetName(i).ToLower());
                    if (info != null)
                    {
                        /*Set the Value to Model*/
                        info.SetValue(obj, dr.GetValue(i) != System.DBNull.Value ? dr.GetValue(i) : null, null);
                    }
                }

                list.Add(obj);
            }

            return list;
        }

        /// <summary>
        /// Set Pagination Properties
        /// </summary>
        /// <param name="totalRecords">Total Records</param>
        /// <param name="pages">Pagination Objects</param>
        private static void SetPaginationInformation(int totalRecords, ref Pagination pages)
        {
            pages.TotalRecords = totalRecords;
            pages.TotalPages = (pages.TotalRecords / pages.PageSize) + ((pages.TotalRecords % pages.PageSize > 0) ? 1 : 0);
            pages.HasPreviousPage = pages.PageNo > pages.PagerSize;
            int currentPagerNo = pages.PageNo / (pages.PagerSize + (pages.PageNo % pages.PagerSize > 0 ? 1 : 0));
            int currentPagerRecords = currentPagerNo * pages.PagerSize;
            pages.HasNextPage = pages.TotalPages > pages.PagerSize ? ((pages.TotalPages % pages.PagerSize) == 0 ? (currentPagerRecords < pages.TotalPages - (pages.TotalPages % pages.PagerSize)) : (currentPagerRecords <= pages.TotalPages - (pages.TotalPages % pages.PagerSize))) : false;
        }

        #endregion

        
    }
}
