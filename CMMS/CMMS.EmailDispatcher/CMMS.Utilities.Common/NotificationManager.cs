﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using CMMS.Utilities.Common.DataEntity;

namespace CMMS.Utilities.Common
{
    /// <summary>
    /// Manages all the notification related activities
    /// </summary>
    public class NotificationManager
    {
        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));

        public void SendEmail(Email email)
        {
            try
            {
                MailMessage message = new MailMessage();

                // Set the from
                message.From = new MailAddress(email.From);

                // Set to (recipient's email addresses)
                string[] mailTo = email.To.Split(Constants.EmailAddressSeperator);

                foreach (string strMailTo in mailTo)
                {
                    if (!(String.IsNullOrEmpty(strMailTo)))
                        message.To.Add(new MailAddress(strMailTo));
                }

                // Set the subject
                message.Subject = email.Subject;

                // Set the message body
                message.Body = email.Body;

                // Set the body format as HTML
                message.IsBodyHtml = true;

                // Set the priority of the message
                message.Priority = MailPriority.High;

                // Send the email using SMTP client
                SmtpClient client = new SmtpClient();
                
                client.Send(message);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error occurred while sending email in NotificationManager.SendEmail().", ex);
                throw;
            }
        }

        
    }

}
