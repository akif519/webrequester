﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;

namespace CMMS.Utilities.Common
{
    /// <summary>
    /// Gets the values of the key from the configuration file
    /// </summary>
    public class ConfigManager
    {
        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));
        
        /// <summary>
        /// Gets the success email template
        /// </summary>
        public static string SuccessEmailTemplate
        {
            get
            {
                string folder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string successEmailTemplate = System.IO.Path.Combine(folder , @"Template\EmailTemplateForReportRetrievalSuccess.txt");
                //string successEmailTemplate = @"D:\Projects\CMMS Utility\CMMS.Utilities\CMMS.Utilities.EmailDispatcher\bin\Release\Template\EmailTemplateSuccess.txt";
                //try
                //{
                //    if (ConfigurationManager.AppSettings["SuccessEmailTemplate"] != null)
                //        successEmailTemplate = ConfigurationManager.AppSettings["SuccessEmailTemplate"].ToString();
                //}
                //catch (Exception ex)
                //{
                //    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the SuccessEmailTemplate key value from config file. ", ex);
                //}

                return successEmailTemplate;
            }
        }

        /// <summary>
        /// Gets the failed email template
        /// </summary>
        public static string FailedEmailTemplate
        {
            get
            {
                string successEmailTemplate = @"Template\EmailTemplateForReportRetrievalFailed.txt";

                try
                {
                    if (ConfigurationManager.AppSettings["FailedEmailTemplate"] != null)
                        successEmailTemplate = ConfigurationManager.AppSettings["FailedEmailTemplate"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the FailedEmailTemplate key value from config file. ", ex);
                }

                return successEmailTemplate;
            }
        }
/// <summary>
        /// Gets the if the console log enabled. 
        /// </summary>
        public static Boolean IsConsoleLogEnabled
        {
            get
            {
                Boolean isConsoleLogEnabled = false;

                try
                {
                    if (ConfigurationManager.AppSettings["IsConsoleLogEnabled"] != null)
                        isConsoleLogEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsConsoleLogEnabled"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the IsConsoleLogEnabled key value from config file. ", ex);
                }

                return isConsoleLogEnabled;
            }
        }

        /// <summary>
        /// Gets the if the text log enabled. 
        /// </summary>
        public static Boolean IsTextLogEnabled
        {
            get
            {
                Boolean isTextLogEnabled = false;

                try
                {
                    if (ConfigurationManager.ConnectionStrings["IsTextLogEnabled"] != null)
                        isTextLogEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsTextLogEnabled"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the IsTextLogEnabled key value from config file. ", ex);
                }

                return isTextLogEnabled;
            }
        }

        /// <summary>
        /// Gets the if the db log enabled. 
        /// </summary>
        public static Boolean IsDBLoggingEnabled
        {
            get
            {
                Boolean isDBLoggingEnabled = false;

                try
                {
                    if (ConfigurationManager.AppSettings["IsDBLoggingEnabled"] != null)
                        isDBLoggingEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsDBLoggingEnabled"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the IsDBLoggingEnabled key value from config file. ", ex);
                }

                return isDBLoggingEnabled;
            }
        }

        /// <summary>
        /// Gets the no of days after which the reports saved on file server will get deleted automatically
        /// </summary>
        public static int HouseKeepingFileServerDays
        {
            get
            {
                int houseKeepingFileServerDays = 30;

                try
                {
                    if (ConfigurationManager.AppSettings["HouseKeepingFileServerDays"] != null)
                        houseKeepingFileServerDays = Convert.ToInt32(ConfigurationManager.AppSettings["HouseKeepingFileServerDays"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the HouseKeepingFileServerDays key value from config file. ", ex);
                }

                return houseKeepingFileServerDays;
            }
        }

        /// <summary>
        /// Gets the no of days after which the reports saved on DB server will get deleted automatically
        /// </summary>
        public static int HouseKeepingDatabaseDays
        {
            get
            {
                int houseKeepingDatabaseDays = 30;

                try
                {
                    if (ConfigurationManager.AppSettings["HouseKeepingDatabaseDays"] != null)
                        houseKeepingDatabaseDays = Convert.ToInt32(ConfigurationManager.AppSettings["HouseKeepingDatabaseDays"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the HouseKeepingDatabaseDays key value from config file. ", ex);
                }

                return houseKeepingDatabaseDays;
            }
        }

        /// <summary>
        /// Gets the host name used for the SMTP transations
        /// </summary>
        public static string EmailHostServer
        {
            get
            {
                string EmailHostServer = "";

                try
                {
                    if (ConfigurationManager.AppSettings["EmailHostServer"] != null)
                        EmailHostServer = ConfigurationManager.AppSettings["EmailHostServer"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the EmailHostServer key value from config file. ", ex);
                }

                return EmailHostServer;
            }
        }

        /// <summary>
        /// Gets the email id using which the email has to be sent out
        /// </summary>
        public static string EmailFrom
        {
            get
            {
                string emailFrom = "";

                try
                {
                    if (ConfigurationManager.AppSettings["EmailFrom"] != null)
                        emailFrom = ConfigurationManager.AppSettings["EmailFrom"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the EmailFrom key value from config file. ", ex);
                }

                return emailFrom;
            }
        }

        /// <summary>
        /// Gets the recipients email id
        /// </summary>
        public static string EmailTo
        {
            get
            {
                string emailTo = "";

                try
                {
                    if (ConfigurationManager.AppSettings["EmailTo"] != null)
                        emailTo = ConfigurationManager.AppSettings["EmailTo"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the EmailTo key value from config file. ", ex);
                }

                return emailTo;
            }
        }

        /// <summary>
        /// Gets the email subject
        /// </summary>
        public static string EmailSubject
        {
            get
            {
                string emailSubject = "";

                try
                {
                    if (ConfigurationManager.AppSettings["EmailSubject"] != null)
                        emailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the EmailSubject key value from config file. ", ex);
                }

                return emailSubject;
            }
        }

        /// <summary>
        /// Gets whether the email notification enabled
        /// </summary>
        public static bool IsEmailNotificationEnabled
        {
            get
            {
                bool isEmailNotificationEnabled = true;

                try
                {
                    if (ConfigurationManager.AppSettings["IsEmailNotificationEnabled"] != null)
                        isEmailNotificationEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["IsEmailNotificationEnabled"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the IsEmailNotificationEnabled key value from config file. ", ex);
                }

                return isEmailNotificationEnabled;
            }
        }

        /// <summary>
        /// Gets whether the email notification enabled
        /// </summary>
        public static bool EnableCustomDebugLog
        {
            get
            {
                bool isEnableCustomDebugLog = false;

                try
                {
                    if (ConfigurationManager.AppSettings["EnableCustomDebugLog"] != null)
                        isEnableCustomDebugLog = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCustomDebugLog"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the EnableCustomDebugLog key value from config file. ", ex);
                }

                return isEnableCustomDebugLog;
            }
        }

        public static bool ProcessMaterialModuleAnyTime
        {
            get
            {
                bool IsProcessMaterialModuleAnyTime = false;

                try
                {
                    if (ConfigurationManager.AppSettings["ProcessMaterialModuleAnyTime"] != null)
                        IsProcessMaterialModuleAnyTime = Convert.ToBoolean(ConfigurationManager.AppSettings["ProcessMaterialModuleAnyTime"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the ProcessMaterialModuleAnyTime key value from config file. ", ex);
                }

                return IsProcessMaterialModuleAnyTime;
            }
        }

        /// <summary>
        /// Gets the Test Mode
        /// </summary>
        public static bool IsTestMode
        {
            get
            {
                bool IsTestMode = false;
                try
                {
                    if (ConfigurationManager.AppSettings["IsTestMode"] != null)
                        IsTestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["IsTestMode"].ToString());
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the IsTestMode key value from config file. ", ex);
                }

                return IsTestMode;
            }
        }

        /// <summary>
        /// Gets the Test Handphone Number
        /// </summary>
        public static string TestMobileNumber
        {
            get
            {
                string TestMobileNumber = "";

                try
                {
                    if (ConfigurationManager.AppSettings["TestMobileNumber"] != null)
                        TestMobileNumber = ConfigurationManager.AppSettings["TestMobileNumber"].ToString();
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while getting the TestMobileNumber key value from config file. ", ex);
                }

                return TestMobileNumber;
            }
        }
    }
}
