﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMMS.Utilities.Common
{
    /// <summary>
    /// Enumerations used in the application for the pupose of code readability
    /// </summary>
    public class Enumerations
    {
        /// <summary>
        /// Indicates the log level
        /// </summary>
        public enum LogLevel
        {
            /// <summary>
            /// Indicates the log level as Debug
            /// </summary>
            DEBUG = 1,

            /// <summary>
            /// Indicates the log level as Error
            /// </summary>
            ERROR,

            /// <summary>
            /// Indicates the log level as Fatal
            /// </summary>
            FATAL,

            /// <summary>
            /// Indicates the log level as Info
            /// </summary>
            INFO,

            /// <summary>
            /// Indicates the log level as Warn
            /// </summary>
            WARN
        }
    }
}
