﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
//using Microsoft.Practices.EnterpriseLibrary.Security.Cryptography;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace CMMS.Utilities.Common
{
    /// <summary>
    /// Utility for the common functionalities in the application
    /// </summary>
    public class Utility
    {
        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(Utility));

        private static Boolean isConsoleLogEnabled = ConfigManager.IsConsoleLogEnabled;

        /// <summary>
        /// Writes the message to console
        /// </summary>
        /// <param name="message">Message</param>
        public static void WriteToConsole(string message)
        {
            if (isConsoleLogEnabled)
                Console.WriteLine(message);
        }

        /// <summary>
        /// Read the message from console
        /// </summary>
        public static void ReadConsole()
        {
            if (isConsoleLogEnabled)
            {
                // Write the "press any key to exit" to console
                Utility.WriteToConsole("\n \n Press any key to exit!");
                Console.Read();
            }
        }
    }
}
