﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;

namespace CMMS.Utilities.Common
{
    /// <summary>
    /// Common logger for all the applications
    /// </summary>
    public class AppLogger
    {
        #region Members
        private readonly ILog Logger;
        #endregion
        public AppLogger(Type type)
        {
            Logger = LogManager.GetLogger(type);
            XmlConfigurator.Configure();
        }

        #region Methods

        /// <summary>
        /// Logs the message in the log file 
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public void WriteLog(Enumerations.LogLevel logLevel, String message)
        {
            if (Logger.IsDebugEnabled && logLevel.Equals(Enumerations.LogLevel.DEBUG))
            {
               Logger.Debug(message);
            }
            else if (logLevel.Equals(Enumerations.LogLevel.ERROR))
            {
                Logger.Error(message);
            }
            else if (Logger.IsFatalEnabled && logLevel.Equals(Enumerations.LogLevel.FATAL))
            {
                Logger.Fatal(message);
            }
            else if (Logger.IsInfoEnabled && logLevel.Equals(Enumerations.LogLevel.INFO))
            {
                Logger.Info(message);
            }
            else if (logLevel.Equals(Enumerations.LogLevel.WARN))
            {
                Logger.Warn(message);
            }
        }

        /// <summary>
        /// Logs the message with the exception details in the log file 
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public void WriteLog(Enumerations.LogLevel logLevel, String message, Exception ex)
        {
            string errorMessage = string.Empty;

            if (Logger.IsDebugEnabled && logLevel.Equals(Enumerations.LogLevel.DEBUG))
            {
                Logger.Debug(message);
            }
            else if (logLevel.Equals(Enumerations.LogLevel.ERROR))
            {
                errorMessage = String.Concat(message, Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace);
                Logger.Error(errorMessage);
            }
            else if (Logger.IsFatalEnabled && logLevel.Equals(Enumerations.LogLevel.FATAL))
            {
                errorMessage = String.Concat(message, Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace);
                Logger.Fatal(errorMessage);
            }
            else if (Logger.IsInfoEnabled && logLevel.Equals(Enumerations.LogLevel.INFO))
            {
                Logger.Info(message);
            }
            else if (logLevel.Equals(Enumerations.LogLevel.WARN))
            {
                errorMessage = String.Concat(message, Environment.NewLine, ex.Message, Environment.NewLine, ex.StackTrace);
                Logger.Warn(errorMessage);
            }
        }

        #endregion
    }
}
