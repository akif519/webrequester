﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMMS.Utilities.Common.DataEntity
{
    /// <summary>
    /// Represents the email
    /// </summary>
    public class Email
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public Email()
        { }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        public Email(string from, string to, string subject, string body)
        {
            From = from;
            To = to;
            Subject = subject;
            Body = body;
        }

        /// <summary>
        /// Gets or sets the email id using which the email got sent
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the recipient's emails
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Gets or sets the subject line of the email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body of the email
        /// </summary>
        public string Body { get; set; }
    }
}
