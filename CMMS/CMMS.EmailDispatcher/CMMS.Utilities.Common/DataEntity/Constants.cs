﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMMS.Utilities.Common.DataEntity
{
    /// <summary>
    /// Constans used in application wide
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Email address seperator
        /// </summary>
        public const char EmailAddressSeperator = ';';
        public const string SMTPServer = "smtp_server";
        public const string SMTPServerPort = "smtp_server_port";
        public const string UsesAuthentication = "uses_authentication";
        public const string UsesSSL = "uses_ssl";
        public const string SMTPUserName = "smtp_username";
        public const string SMTPPassword = "smtp_password";

        public const string WorkRequestType = "WR";
        public const string WorkOrderType = "WO";

        public const string SMSGatewayAPIUrl = "SMS_GateWay_API_URL";
        public const string SMSUserName = "SMS_UserName";
        public const string SMSPassword = "SMS_Password";
        public const string SMSApiId = "SMS_ApiID";
        public const string SMSSenderName = "Sender_Name";
        
    }
}
