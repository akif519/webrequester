﻿using System;
using System.Collections.Generic;
using System.IO;
using CMMS.Utilities.Common;
using CMMS.Utilities.EmailDispatcher;
using CMMS.Model;
using CMMS.Service;
using System.Configuration;

namespace CMMS.Utilities.EmailDispatcher
{
    class Program
    {
        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(Program));

        /// <summary>
        /// 1: 
        /// 2: 
        /// 3: 
        /// 4: 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                ErrorLog.WriteDebugLog("Program.cs", "Main", "ENTRY- Starting the task.", string.Empty);
                ErrorLog.WriteDebugLog("Program.cs", "Main", "ENTRY- Starting the task.", args.Length.ToString());
                if (args.Length > 0)
                {

                    List<Account> lstActiveAccounts = new List<Account>();
                    ErrorLog.WriteDebugLog("Program.cs", "Main", "Enter Account Information.", string.Empty);
                    lstActiveAccounts = CommonDAL.GetAccountInformation(1);
                    ErrorLog.WriteDebugLog("Program.cs", "Main", "Exit Account Information.", string.Empty);

                    if (lstActiveAccounts != null && lstActiveAccounts.Count > 0)
                    {
                        ErrorLog.WriteDebugLog("Program.cs", "Main", "Active Accounts Found.", "No Of Active Accounts = " + lstActiveAccounts.Count);

                        foreach (Account objAccount in lstActiveAccounts)
                        {
                            ErrorLog.WriteDebugLog("Program.cs", "Main", "Starting Action For Account.", "Client ID = " + objAccount.ClientID);

                            try
                            {
                                ErrorLog.WriteDebugLog("Program.cs", "Main", "Arg of scheduler", args[0].ToString().ToLower());
                                switch (args[0].ToString().ToLower())
                                {
                                    case "emailsection":
                                        EmailSection(objAccount);
                                        break;
                                    case "notificationsection":
                                        ErrorLog.WriteDebugLog("Program.cs", "Main", "enter in Case : NotificationSection", "Client ID = " + objAccount.ClientID);
                                        NotificationSection(objAccount);
                                        break;
                                }
                            }
                            catch
                            {
                                continue;
                            }

                            ErrorLog.WriteDebugLog("Program.cs", "Main", "Completed Action For Account.", "Client ID = " + objAccount.ClientID);
                        }
                    }
                    else
                    {
                        ErrorLog.WriteDebugLog("Program.cs", "Main", "No Any Active Accounts Found.", "No Of Active Accounts = 0");
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("Program.cs", "Main", "Args Not found", args.Length.ToString());
                }


            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while processing Main method. ", ex);
                ErrorLog.WriteErrorLog("Program.cs", "Main", "", ex.Message, ex.StackTrace);
            }
        }

        #region Private method
        private static void EmailSection(Account objAccount)
        {
            try
            {
                using (ProcessedRules objectProcessedRules = new ProcessedRules())
                {
                    objectProcessedRules.ProcessProcessedRules(objAccount.ClientCode);
                }

                using (EscalationEvents objectEscalationEvents = new EscalationEvents())
                {
                    objectEscalationEvents.ProcessEscalationEvents(objAccount.ClientCode, Convert.ToInt32(objAccount.DbType));
                }

                DateTime dtCurrentTime = DateTime.Now;
                if ((dtCurrentTime.Hour == 0 && dtCurrentTime.Minute < 5) || CMMS.Utilities.Common.ConfigManager.ProcessMaterialModuleAnyTime)
                {
                    using (MaterialModule objectMaterialModule = new MaterialModule())
                    {
                        objectMaterialModule.ProcessMaterialModule(objAccount.ClientCode, Convert.ToInt32(objAccount.DbType));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while processing Main method. ", ex);
                ErrorLog.WriteErrorLog("Program.cs", "Main", "ClientID:" + objAccount.ClientID, ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        /// <summary>
        /// Sends the email notification
        /// </summary>
        private static void SendNotification(bool success, string message)
        {
            // Log the entry point of the method as debug log
            Logger.WriteLog(Enumerations.LogLevel.DEBUG, "ENTRY - Method name: CMMS.Utilities.EmailDispatcher.SendNotification(bool)");

            if (!ConfigManager.IsEmailNotificationEnabled)
                return;


            // Show the sending email notification message
            Utility.WriteToConsole(String.Concat(Environment.NewLine, "Sending email notification..."));

            // Show the sending email notification
            Utility.WriteToConsole(String.Concat("Sending email notification to ", ConfigManager.EmailTo));

            NotificationManager notificationManager = new NotificationManager();
            string emailBody = string.Empty;

            // Read the text from file
            string fileName = string.Empty;
            if (success)
                fileName = ConfigManager.SuccessEmailTemplate;
            else
                fileName = ConfigManager.FailedEmailTemplate;

            StreamReader reader = new StreamReader(fileName);
            emailBody = reader.ReadToEnd();

            // Get the current time in EST time-zone
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo);

            // Replace the place holders in the email template
            emailBody = emailBody.Replace("@ExecutionTime", TimeZoneInfo.ConvertTime(DateTime.Now, timeZoneInfo).ToString()).Replace("@Message", message);

            // Send email
            notificationManager.SendEmail(new Common.DataEntity.Email(ConfigManager.EmailFrom, ConfigManager.EmailTo, ConfigManager.EmailSubject, emailBody));

            // Log the entry point of the method as debug log
            Logger.WriteLog(Enumerations.LogLevel.DEBUG, "EXIT - Method name: CMMS.Utilities.EmailDispatcher.SendNotification(bool)");

            // Show the sent email notification message
            CMMS.Utilities.Common.Utility.WriteToConsole(String.Concat("Email notification sent."));
        }

        private static void NotificationSection(Account objAccount)
        {
            try
            {
                ErrorLog.WriteDebugLog("Program.cs", "NotificationSection", "Enter in NotificationSection", "Client ID = " + objAccount.ClientID);
                EmailNotificationService.InsertWOWRNotification(Convert.ToInt32(ConfigurationManager.AppSettings["NotificationInterval"]), objAccount.ClientCode, Convert.ToInt32(objAccount.DbType));
                ErrorLog.WriteDebugLog("Program.cs", "NotificationSection", "End of NotificationSection", "Client ID = " + objAccount.ClientID);
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while processing NotificationSection. ", ex);
                ErrorLog.WriteErrorLog("Program.cs", "NotificationSection", "", ex.Message, ex.StackTrace);
                ErrorLog.WriteDebugLog("Program.cs", "NotificationSection", ex.Message, ex.StackTrace);
            }
        }

        #endregion
    }
}
