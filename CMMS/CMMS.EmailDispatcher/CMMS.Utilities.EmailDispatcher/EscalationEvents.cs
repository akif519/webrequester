﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using CMMS.Model;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Utilities.Common;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace CMMS.Utilities.EmailDispatcher
{
    public class EscalationEvents : IDisposable
    {
        #region Private Variables

        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));
        private string ReportDirPath = "Report";

        #endregion

        /// <summary>
        /// Process EmailNotificationRule of Escalation Events
        /// </summary>
        public void ProcessEscalationEvents(string clientCode, int dbType)
        {
            try
            {
                RemoveReportDirectory();

                List<EmailNotificationEscalationRule> listEmailNotificationEscalationRule = EmailNotificationService.GetListEmailNotificationRule_EscalationRules(clientCode);
                foreach (EmailNotificationEscalationRule objectEmailNotificationEscalationRule in listEmailNotificationEscalationRule)
                {
                    if (objectEmailNotificationEscalationRule.ObjectEmailNotificationRule != null)
                    {

                        SendNotification_EscalationRules(objectEmailNotificationEscalationRule, clientCode, dbType);
                        RemoveReportDirectory();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("EscalationEvents.cs", "ProcessEscalationEvents", "ClientID:" + clientCode, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Get list of Employee and send mail
        /// </summary>
        /// <param name="objectEmailNotificationRule"></param>
        private void SendNotification_EscalationRules(EmailNotificationEscalationRule objectEmailNotificationEscalationRule, string clientCode, int dbType)
        {
            ErrorLog.WriteDebugLog("EscalationEvents.cs", "SendNotification_EscalationRules", "enter in method SendNotification_EscalationRules", "Client ID = " + clientCode);
            string stringFrom, stringTo = string.Empty;
            string stringSubjectEnglish = string.Empty, stringSubjectArabic = string.Empty, stringBodyEnglish = string.Empty, stringBodyArabic = string.Empty, stringAttachmentEnglish = string.Empty, stringAttachmentArabic = string.Empty;
            string stringApiURL = string.Empty, stringUserName = string.Empty, stringPassword = string.Empty, stringApiId = string.Empty, stringSenderName = string.Empty;
            string stringBody = string.Empty, stringSubject = string.Empty, stringToMobileNo = string.Empty;
            string stringSMSBodyEnglish = string.Empty, stringSMSBodyArabic = string.Empty, stringSMSBody = string.Empty;

            stringFrom = ConfigManager.EmailFrom;
            bool isAppliedToEmail = Convert.ToBoolean(objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.IsAppliedToEmail);
            bool isAppliedToSMS = Convert.ToBoolean(objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.IsAppliedToSMS);

            try
            {
                List<Configuration> listConfiguration = new List<Configuration>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    listConfiguration = objContext.SearchWithClientID<Configuration>(new Configuration(), 0, string.Empty, string.Empty, clientCode).Where(m => m.Module == "SMSNotification").ToList();
                }

                foreach (Configuration objectConfiguration in listConfiguration)
                {
                    switch (objectConfiguration.Key.ToString())
                    {
                        case Common.DataEntity.Constants.SMSGatewayAPIUrl:
                            stringApiURL = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSUserName:
                            stringUserName = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSPassword:
                            stringPassword = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSApiId:
                            stringApiId = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSSenderName:
                            stringSenderName = objectConfiguration.Value.ToString();
                            break;
                    }
                }

                GetEmailSubjectAndBodyWithAttachment(objectEmailNotificationEscalationRule, out stringSubjectEnglish, out stringSubjectArabic, out stringBodyEnglish, out stringBodyArabic, out stringAttachmentEnglish, out stringAttachmentArabic, out stringSMSBodyEnglish, out stringSMSBodyArabic, clientCode, dbType);
                System.Net.Mail.SmtpClient objectSmtpClient = CMMS.Utilities.EmailDispatcher.General.GetSMTPSettings(clientCode);

                foreach (EmailReceiptant objectEmailReceiptant in objectEmailNotificationEscalationRule.ListEmailReceiptant_Employee)
                {
                    string stringAttachment = string.Empty;
                    if (objectEmailReceiptant.LanguageCode.ToLower() == "en-us")
                    {
                        stringSubject = stringSubjectEnglish;
                        stringBody = stringBodyEnglish;
                        stringAttachment = stringAttachmentEnglish;
                        stringSMSBody = stringSMSBodyEnglish;
                    }
                    else if (objectEmailReceiptant.LanguageCode.ToLower() == "ar-sa")
                    {
                        stringSubject = stringSubjectArabic;
                        stringBody = stringBodyArabic;
                        stringAttachment = stringAttachmentArabic;
                        stringSMSBody = stringSMSBodyArabic;
                    }

                    stringBody = System.Text.RegularExpressions.Regex.Replace(stringBody, @"(\r|\n)+", "<br />");
                    //stringSMSBody = System.Text.RegularExpressions.Regex.Replace(stringSMSBody, @"(\r|\n)+", "<br />");

                    if ((objectEmailReceiptant.LastEmailSentTime.HasValue && objectEmailReceiptant.EscalationTimeInMin <= Convert.ToInt32((DateTime.Now - objectEmailReceiptant.LastEmailSentTime.Value).TotalMinutes)) || !objectEmailReceiptant.LastEmailSentTime.HasValue)
                    {
                        stringBody = GetReceipantDetailToEmailBody(stringBody, objectEmailReceiptant);
                        stringTo = objectEmailReceiptant.ReceiptantEmail;

                        if (isAppliedToEmail != null && isAppliedToEmail)
                        {
                            if (CMMS.Utilities.EmailDispatcher.General.SendMail(MailFrom: stringFrom, MailTo: objectEmailReceiptant.ReceiptantEmail, Subject: stringSubject, Body: stringBody, Attachment: stringAttachment, objectSMTP: objectSmtpClient))
                                EmailNotificationService.UpdateLastEmailSentTime_EscalationEmailNotificationRule(objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleID, objectEmailReceiptant.ReceiptantID, clientCode, dbType);
                        }

                        if (isAppliedToSMS != null && isAppliedToSMS)
                        {
                            stringToMobileNo = objectEmailReceiptant.HandPhone;

                            if (!string.IsNullOrEmpty(stringToMobileNo) && !string.IsNullOrEmpty(stringSMSBody))
                            {
                                if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("clickatell"))
                                {
                                    General.SendSMSFromAPI(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringApiId);
                                }
                                else if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("mobily"))
                                {
                                    General.SendSMSFromDll(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringSenderName);
                                }
                            }
                            else
                            {
                                ErrorLog.WriteErrorLog("EscalationEvents.cs", "SendNotification_EscalationRules", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending SMS From EscalationRules. Mobile Number or SMS Body is Empty.", string.Empty);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while sending Email From Escalation Events. ", ex);
                ErrorLog.WriteErrorLog("EscalationEvents.cs", "SendNotification_EscalationRules", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending Email or SMS From EscalationRules. " + ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Get Email Subject & Body in English & Arabic Language from EmailTemplateName
        /// </summary>
        private void GetEmailSubjectAndBodyWithAttachment(EmailNotificationEscalationRule objectEmailNotificationEscalationRule, out string EmailSubjectEnglish, out string EmailSubjectArabic, out string EmailBodyEnglish, out string EmailBodyArabic, out string EmailAttachmentEnglish, out string EmailAttachmentArabic, out string SMSBodyEnglish, out string SMSBodyArabic, string clientCode, int dbType)
        {
            EmailSubjectEnglish = EmailSubjectArabic = EmailBodyEnglish = EmailBodyArabic = EmailAttachmentEnglish = EmailAttachmentArabic = SMSBodyEnglish = SMSBodyArabic = string.Empty;
            Emailtemplate objectEmailtemplateEnglish = null, objectEmailtemplateArabic = null;

            if (!string.IsNullOrEmpty(objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.EmailTemplateName))
            {
                List<Emailtemplate> lstObjectEmailTemplate = General.GetEmailTemplateDetailByTemplateName(objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.EmailTemplateName, clientCode);
                if (lstObjectEmailTemplate.Count > 0)
                {
                    objectEmailtemplateEnglish = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "en-us");
                    objectEmailtemplateArabic = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "ar-sa");

                    if (objectEmailtemplateEnglish != null)
                    {
                        EmailSubjectEnglish = objectEmailtemplateEnglish.EmailSubject;
                        EmailBodyEnglish = objectEmailtemplateEnglish.EmailBody;
                        SMSBodyEnglish = objectEmailtemplateEnglish.SMSBody;

                        if (SMSBodyEnglish == null)
                        {
                            SMSBodyEnglish = string.Empty;
                        }

                        if (EmailBodyEnglish == null)
                        {
                            EmailBodyEnglish = string.Empty;
                        }

                        ErrorLog.WriteDebugLog("Program.cs", "GetEmailSubjectAndBodyWithAttachment", "Starting Get Excel", "NotificationRuleModuleID = " + objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleModuleID);

                        if (objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode() &&
                            objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleEventID == SystemEnum.ModuleEvent.JobRequestOpenStatusEscalation.GetHashCode())
                        {
                            EmailAttachmentEnglish = PrepareListOfWorkRequest_ExcelFile(objectEmailtemplateEnglish, objectEmailNotificationEscalationRule, clientCode, dbType);
                        }
                        else if (objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode() &&
                                  objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleEventID == SystemEnum.ModuleEvent.JobOrderOpenStatusEscalation.GetHashCode())
                        {
                            EmailAttachmentEnglish = PrepareListOfWorkOrder_ExcelFile(objectEmailtemplateEnglish, objectEmailNotificationEscalationRule, clientCode, dbType);
                        }
                    }

                    if (objectEmailtemplateArabic != null)
                    {
                        EmailSubjectArabic = objectEmailtemplateArabic.EmailSubject;
                        EmailBodyArabic = objectEmailtemplateArabic.EmailBody;
                        SMSBodyArabic = objectEmailtemplateArabic.SMSBody;

                        if (SMSBodyArabic == null)
                        {
                            SMSBodyArabic = string.Empty;
                        }

                        if (EmailBodyArabic == null)
                        {
                            EmailBodyArabic = string.Empty;
                        }

                        if (objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode() &&
                            objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleEventID == SystemEnum.ModuleEvent.JobRequestOpenStatusEscalation.GetHashCode())
                        {
                            EmailAttachmentArabic = PrepareListOfWorkRequest_ExcelFile(objectEmailtemplateArabic, objectEmailNotificationEscalationRule, clientCode, dbType);
                        }
                        else if (objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode() &&
                                  objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleEventID == SystemEnum.ModuleEvent.JobOrderOpenStatusEscalation.GetHashCode())
                        {
                            EmailAttachmentArabic = PrepareListOfWorkOrder_ExcelFile(objectEmailtemplateArabic, objectEmailNotificationEscalationRule, clientCode, dbType);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fill Receiptant Detail To Email Body
        /// </summary>
        /// <returns>Email Body</returns>
        private string GetReceipantDetailToEmailBody(string stringEmailTemplateBody, EmailReceiptant objectEmailReceiptant)
        {
            foreach (PropertyInfo objectPropertyInfo in objectEmailReceiptant.GetType().GetProperties())
            {
                string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                if (stringEmailTemplateBody.Contains(strField))
                {
                    object objectTemp = objectPropertyInfo.GetValue(objectEmailReceiptant, null);
                    stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                }
            }

            return stringEmailTemplateBody;
        }

        /// <summary>
        /// Prepare Excel file from Work Request list
        /// </summary>
        private string PrepareListOfWorkRequest_ExcelFile(Emailtemplate objectEmailTemplate, EmailNotificationEscalationRule objectEmailNotificationEscalationRule, string clientCode, int dbType)
        {
            FileStream stream = null;
            BinaryWriter writer = null;

            try
            {
                List<EmailTemplateWorkRequest> listEmailTemplateWorkRequest = objectEmailNotificationEscalationRule.ListWorkRequest;

                string stringWorkRequestListEnglishFile = "WorkRequestListEnglish_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffffff") + ".xlsx";
                string stringWorkRequestListArabicFile = "WorkRequestListArabic_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffffff") + ".xlsx";
                string stringWorkRequestListFile = ReportDirPath + "//" + (objectEmailTemplate.LanguageRef.ToLower() == "en-us" ? stringWorkRequestListEnglishFile : stringWorkRequestListArabicFile);

                if (!Directory.Exists(ReportDirPath))
                    Directory.CreateDirectory(ReportDirPath);

                System.IO.File.WriteAllText(stringWorkRequestListFile, string.Empty);
                stream = new FileStream(stringWorkRequestListFile, FileMode.OpenOrCreate);

                using (ExcelPackage objectExcelPackage = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet objectExcelWorksheet = objectExcelPackage.Workbook.Worksheets.Add("Normal");

                    objectExcelWorksheet.Cells["A1"].Value = "Work Request List";
                    objectExcelWorksheet.Cells["A2"].Value = "Date: " + CMMS.Infrastructure.Common.GetEnglishDate(DateTime.Now);

                    objectExcelWorksheet.Cells["A1"].Style.Font.Size = objectExcelWorksheet.Cells["A2"].Style.Font.Size = 12;
                    objectExcelWorksheet.Cells["A1"].Style.Font.Bold = objectExcelWorksheet.Cells["A2"].Style.Font.Bold = true;

                    if (!string.IsNullOrEmpty(objectEmailTemplate.WOWRFieldList))
                    {
                        string[] arrWOWRFields = objectEmailTemplate.WOWRFieldList.Split(',');
                        List<Emailtemplatefield> listEmailTemplateField = EmailNotificationService.GetEmailTemplateFields(CMMS.Utilities.Common.DataEntity.Constants.WorkRequestType, clientCode);

                        for (int index = 0; index < arrWOWRFields.Length; index++)
                        {
                            Emailtemplatefield objectEmailTemplateField = listEmailTemplateField.FirstOrDefault(item => item.EmailTemplateDBFieldName == arrWOWRFields[index]);
                            if (objectEmailTemplateField != null)
                                objectExcelWorksheet.Cells[4, (index + 1)].Value = objectEmailTemplateField.EmailTemplateFieldDisplayName;
                        }

                        //Format the header for column 1-3
                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, 4, arrWOWRFields.Length])
                        {
                            General.SetExcelHeaderStyle(objectExcelRange);
                        }

                        int rowNumber = 6;
                        System.Reflection.PropertyInfo[] arrWorkRequestPropertyInfo = listEmailTemplateWorkRequest[0].GetType().GetProperties().Where(x => arrWOWRFields.Contains(x.Name)).ToArray();
                        foreach (EmailTemplateWorkRequest objectEmailTemplateWorkRequest in listEmailTemplateWorkRequest)
                        {
                            for (int index = 0; index < arrWorkRequestPropertyInfo.Length; index++)
                            {
                                if (arrWOWRFields.Contains(arrWorkRequestPropertyInfo[index].Name.Trim()))
                                {
                                    object objectTemp = arrWorkRequestPropertyInfo[index].GetValue(objectEmailTemplateWorkRequest, null);
                                    objectExcelWorksheet.SetValue(rowNumber, (index + 1), (objectTemp != null ? objectTemp.ToString() : ""));
                                }
                            }


                            if (objectEmailTemplate.LanguageRef.ToLower() == "en-us")
                            {
                                using (ServiceContext context = new ServiceContext())
                                {
                                    WorkRequestEscalation objWorkRequestEscalationOld = new WorkRequestEscalation();
                                    WorkRequestEscalation objWorkRequestEscalation = new WorkRequestEscalation();
                                    objWorkRequestEscalation.NotificationRuleID = objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleID;
                                    objWorkRequestEscalation.RequestNo = objectEmailTemplateWorkRequest.RequestNo;

                                    //List <WorkOrderEscalation> lst = context.Search<WorkOrderEscalation>(objWorkOrderEscalation).ToList();                                
                                    List<WorkRequestEscalation> lst = EmailNotificationService.GetWorkRequestEscalation(objWorkRequestEscalation, clientCode);

                                    if (lst.Count > 0)
                                    {
                                        objWorkRequestEscalationOld = lst.FirstOrDefault();
                                        //Edit
                                        objWorkRequestEscalation.WorkRequestEscalationID = objWorkRequestEscalationOld.WorkRequestEscalationID;
                                        objWorkRequestEscalation.EscalationTimeInMin = objectEmailNotificationEscalationRule.ListEmailReceiptant_Employee.FirstOrDefault().EscalationTimeInMin;
                                        objWorkRequestEscalation.EscalationLevel = objWorkRequestEscalationOld.EscalationLevel + 1;
                                        objWorkRequestEscalation.ModifiedDate = DateTime.Now;
                                        EmailNotificationService.UpdateWorkRequestEscalation(objWorkRequestEscalation, clientCode, dbType);
                                    }
                                    else
                                    {
                                        //Add
                                        objWorkRequestEscalation.EscalationTimeInMin = objectEmailNotificationEscalationRule.ListEmailReceiptant_Employee.FirstOrDefault().EscalationTimeInMin;
                                        objWorkRequestEscalation.EscalationLevel = 1;
                                        objWorkRequestEscalation.CreatedDate = DateTime.Now;
                                        EmailNotificationService.InsertWorkRequestEscalation(objWorkRequestEscalation, clientCode, dbType);
                                    }
                                }
                            }
                            rowNumber++;
                        }

                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, rowNumber, arrWOWRFields.Length])
                        {
                            objectExcelRange.AutoFitColumns();
                        }

                        writer = new BinaryWriter(stream);
                        writer.Write(objectExcelPackage.GetAsByteArray());
                    }
                    return stringWorkRequestListFile;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while preparing Excel File of list of Work Request. ", ex);
                return string.Empty;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
                if (stream != null)
                    stream.Close();
            }
        }

        /// <summary>
        /// Prepare Excel file from Work Order list
        /// </summary>
        private string PrepareListOfWorkOrder_ExcelFile(Emailtemplate objectEmailTemplate, EmailNotificationEscalationRule objectEmailNotificationEscalationRule, string clientCode, int dbType)
        {
            FileStream stream = null;
            BinaryWriter writer = null;

            try
            {
                List<EmailTemplateWorkOrder> listEmailTemplateWorkOrder = objectEmailNotificationEscalationRule.ListWorkOrder;
                string stringWorkOrderListEnglishFile = "WorkOrderListEnglish_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffffff") + ".xlsx";
                string stringWorkOrderListArabicFile = "WorkOrderListArabic_" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffffff") + ".xlsx";
                string folder = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                string stringWorkOrderListFile = System.IO.Path.Combine(folder, ReportDirPath) + "//" + (objectEmailTemplate.LanguageRef.ToLower() == "en-us" ? stringWorkOrderListEnglishFile : stringWorkOrderListArabicFile);

                ErrorLog.WriteDebugLog("EscalationEvents.cs", "PrepareListOfWorkOrder_ExcelFile", "Path for Report is", "ReportDirPath = " + stringWorkOrderListFile);

                if (!Directory.Exists(ReportDirPath))
                    Directory.CreateDirectory(ReportDirPath);

                System.IO.File.WriteAllText(stringWorkOrderListFile, string.Empty);
                stream = new FileStream(stringWorkOrderListFile, FileMode.OpenOrCreate);

                using (ExcelPackage objectExcelPackage = new ExcelPackage())
                {
                    //Create the worksheet
                    ExcelWorksheet objectExcelWorksheet = objectExcelPackage.Workbook.Worksheets.Add("Normal");

                    objectExcelWorksheet.Cells["A1"].Value = "Work Order List";
                    objectExcelWorksheet.Cells["A2"].Value = "Date: " + CMMS.Infrastructure.Common.GetEnglishDate(DateTime.Now);

                    objectExcelWorksheet.Cells["A1"].Style.Font.Size = objectExcelWorksheet.Cells["A2"].Style.Font.Size = 12;
                    objectExcelWorksheet.Cells["A1"].Style.Font.Bold = objectExcelWorksheet.Cells["A2"].Style.Font.Bold = true;

                    if (!string.IsNullOrEmpty(objectEmailTemplate.WOWRFieldList))
                    {
                        string[] arrWOWRFields = objectEmailTemplate.WOWRFieldList.Split(',');
                        List<Emailtemplatefield> listEmailTemplateField = EmailNotificationService.GetEmailTemplateFields(CMMS.Utilities.Common.DataEntity.Constants.WorkOrderType, clientCode);

                        for (int index = 0; index < arrWOWRFields.Length; index++)
                        {
                            Emailtemplatefield objectEmailTemplateField = listEmailTemplateField.FirstOrDefault(item => item.EmailTemplateDBFieldName == arrWOWRFields[index]);
                            if (objectEmailTemplateField != null)
                                objectExcelWorksheet.Cells[4, (index + 1)].Value = objectEmailTemplateField.EmailTemplateFieldDisplayName;
                        }

                        //Format the header for column 1-3
                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, 4, arrWOWRFields.Length])
                        {
                            General.SetExcelHeaderStyle(objectExcelRange);
                        }

                        int rowNumber = 6;
                        System.Reflection.PropertyInfo[] arrWorkRequestPropertyInfo = listEmailTemplateWorkOrder[0].GetType().GetProperties().Where(x => arrWOWRFields.Contains(x.Name)).ToArray();
                        foreach (EmailTemplateWorkOrder objectEmailTemplateWorkOrder in listEmailTemplateWorkOrder)
                        {
                            try
                            {
                                for (int index = 0; index < arrWorkRequestPropertyInfo.Length; index++)
                                {
                                    if (arrWOWRFields.Contains(arrWorkRequestPropertyInfo[index].Name.Trim()))
                                    {
                                        object objectTemp = arrWorkRequestPropertyInfo[index].GetValue(objectEmailTemplateWorkOrder, null);
                                        objectExcelWorksheet.SetValue(rowNumber, (index + 1), (objectTemp != null ? objectTemp.ToString() : ""));
                                    }
                                }

                                if (objectEmailTemplate.LanguageRef.ToLower() == "en-us")
                                {
                                    using (ServiceContext context = new ServiceContext())
                                    {
                                        WorkOrderEscalation objWorkOrderEscalationOld = new WorkOrderEscalation();
                                        WorkOrderEscalation objWorkOrderEscalation = new WorkOrderEscalation();
                                        objWorkOrderEscalation.NotificationRuleID = objectEmailNotificationEscalationRule.ObjectEmailNotificationRule.NotificationRuleID;
                                        objWorkOrderEscalation.WorkOrderNo = objectEmailTemplateWorkOrder.WorkorderNo;

                                        //List <WorkOrderEscalation> lst = context.Search<WorkOrderEscalation>(objWorkOrderEscalation).ToList();                                
                                        List<WorkOrderEscalation> lst = EmailNotificationService.GetWorkOrderEscalation(objWorkOrderEscalation, clientCode);

                                        if (lst.Count > 0)
                                        {
                                            objWorkOrderEscalationOld = lst.FirstOrDefault();
                                            //Edit
                                            objWorkOrderEscalation.WorkOrderEscalationID = objWorkOrderEscalationOld.WorkOrderEscalationID;
                                            objWorkOrderEscalation.EscalationTimeInMin = objectEmailNotificationEscalationRule.ListEmailReceiptant_Employee.FirstOrDefault().EscalationTimeInMin;
                                            objWorkOrderEscalation.EscalationLevel = objWorkOrderEscalationOld.EscalationLevel + 1;
                                            objWorkOrderEscalation.ModifiedDate = DateTime.Now;
                                            EmailNotificationService.UpdateWorkOrderEscalation(objWorkOrderEscalation, clientCode, dbType);
                                        }
                                        else
                                        {
                                            //Add
                                            objWorkOrderEscalation.EscalationTimeInMin = objectEmailNotificationEscalationRule.ListEmailReceiptant_Employee.FirstOrDefault().EscalationTimeInMin;
                                            objWorkOrderEscalation.EscalationLevel = 1;
                                            objWorkOrderEscalation.CreatedDate = DateTime.Now;
                                            EmailNotificationService.InsertWorkOrderEscalation(objWorkOrderEscalation, clientCode, dbType);
                                        }
                                    }
                                }
                            }
                            catch
                            {

                            }
                            rowNumber++;
                        }

                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, rowNumber, arrWOWRFields.Length])
                        {
                            objectExcelRange.AutoFitColumns();
                        }

                        writer = new BinaryWriter(stream);
                        writer.Write(objectExcelPackage.GetAsByteArray());
                    }
                    ReportDirPath = string.Empty;
                    return stringWorkOrderListFile;
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while preparing Excel File of list of Work Order. ", ex);
                return string.Empty;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
                if (stream != null)
                    stream.Close();
            }
        }

        /// <summary>
        /// Remove temporary Report directory after sending mail
        /// </summary>
        private bool RemoveReportDirectory()
        {
            try
            {
                if (Directory.Exists(ReportDirPath))
                {
                    foreach (string stringFile in Directory.GetFiles(ReportDirPath))
                    {
                        File.Delete(stringFile);
                    }

                    Directory.Delete(ReportDirPath);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        /// <summary>
        /// Dispose global objects
        /// </summary>
        public void Dispose()
        {

        }
        #region Old Code

        //Get Work Request/Work Order list in HTML table format

        ///// <summary>
        ///// Prepare HTML table from table header & rows
        ///// </summary>
        //private string GetHTMLTable(string TableHeader , string TableRows)
        //{
        //    string stringHTMLTable = "";

        //    stringHTMLTable = stringHTMLTable + ("<style>table { border-collapse:collapse; margin-top:15px; } table, td, th { border:1px solid black; }</style>");
        //    stringHTMLTable = stringHTMLTable + ("<table cellpadding='5'>" + TableHeader + TableRows + "</table>");

        //    return stringHTMLTable;
        //}

        ///// <summary>
        ///// Get list of Work orders in HTML table format
        ///// </summary>
        //private string GetListOfWorkOrderInHtmlTable(emailtemplate objectEmailTemplate , List<EmailTemplateWorkOrder> listEmailTemplateWorkOrder)
        //{
        //    string stringTableWorkOrder = "", stringTableHeader = "", stringTableRows = "";
        //    if (!string.IsNullOrEmpty(objectEmailTemplate.WOWRFieldList))
        //    {
        //        string[] arrWOWRFields = objectEmailTemplate.WOWRFieldList.Split(',');
        //        List<emailtemplatefield> listEmailTemplateField = new DALEmailTemplate().GetEmailTemplateFields(CMMS.Utilities.Common.DataEntity.Constants.WorkOrderType);
        //        foreach (string stringField in arrWOWRFields)
        //        {
        //            emailtemplatefield objectEmailTemplateField = listEmailTemplateField.FirstOrDefault(item => item.EmailTemplateDBFieldName == stringField);
        //            if (objectEmailTemplateField != null)
        //                stringTableHeader = stringTableHeader + ("<th>" + objectEmailTemplateField.EmailTemplateFieldDisplayName + "</th>");
        //        }
        //        stringTableHeader = "<tr>" + stringTableHeader + "</tr>";

        //        System.Reflection.PropertyInfo[] arrWorkRequestPropertyInfo = listEmailTemplateWorkOrder[0].GetType().GetProperties().Where(x => arrWOWRFields.Contains(x.Name)).ToArray();
        //        foreach (EmailTemplateWorkOrder objectEmailTemplateWorkOrder in listEmailTemplateWorkOrder)
        //        {
        //            string strRow = string.Empty;
        //            foreach (System.Reflection.PropertyInfo objectPropertyInfo in arrWorkRequestPropertyInfo)
        //            {
        //                if (arrWOWRFields.Contains(objectPropertyInfo.Name.Trim()))
        //                {
        //                    object objectTemp = objectPropertyInfo.GetValue(objectEmailTemplateWorkOrder , null);
        //                    strRow = strRow + ("<td>" + (objectTemp != null ? objectTemp.ToString() : "") + "</td>");
        //                }
        //            }
        //            stringTableRows = stringTableRows + ("<tr>" + strRow + "</tr>");
        //        }

        //        stringTableWorkOrder = GetHTMLTable(stringTableHeader , stringTableRows);
        //    }

        //    return objectEmailTemplate.EmailBody.Replace("[WorkOrderList]" , stringTableWorkOrder);
        //}

        ///// <summary>
        ///// Get list of Work request in HTML table format
        ///// </summary>
        //private string GetListOfWorkRequestInHtmlTable(emailtemplate objectEmailTemplate , List<EmailTemplateWorkRequest> listEmailTemplateWorkRequest)
        //{
        //    string stringTableWorkRequest = "", stringTableHeader = "", stringTableRows = "";
        //    if (!string.IsNullOrEmpty(objectEmailTemplate.WOWRFieldList))
        //    {
        //        string[] arrWOWRFields = objectEmailTemplate.WOWRFieldList.Split(',');
        //        List<emailtemplatefield> listEmailTemplateField = new DALEmailTemplate().GetEmailTemplateFields(CMMS.Utilities.Common.DataEntity.Constants.WorkRequestType);
        //        foreach (string stringField in arrWOWRFields)
        //        {
        //            emailtemplatefield objectEmailTemplateField = listEmailTemplateField.FirstOrDefault(item => item.EmailTemplateDBFieldName == stringField);
        //            if (objectEmailTemplateField != null)
        //                stringTableHeader = stringTableHeader + ("<th>" + objectEmailTemplateField.EmailTemplateFieldDisplayName + "</th>");
        //        }
        //        stringTableHeader = "<tr>" + stringTableHeader + "</tr>";

        //        System.Reflection.PropertyInfo[] arrWorkRequestPropertyInfo = listEmailTemplateWorkRequest[0].GetType().GetProperties().Where(x => arrWOWRFields.Contains(x.Name)).ToArray();
        //        foreach (EmailTemplateWorkRequest objectEmailTemplateWorkRequest in listEmailTemplateWorkRequest)
        //        {
        //            string strRow = string.Empty;
        //            foreach (System.Reflection.PropertyInfo objectPropertyInfo in arrWorkRequestPropertyInfo)
        //            {
        //                if (arrWOWRFields.Contains(objectPropertyInfo.Name.Trim()))
        //                {
        //                    object objectTemp = objectPropertyInfo.GetValue(objectEmailTemplateWorkRequest , null);
        //                    strRow = strRow + ("<td>" + (objectTemp != null ? objectTemp.ToString() : "") + "</td>");
        //                }
        //            }
        //            stringTableRows = stringTableRows + ("<tr>" + strRow + "</tr>");
        //        }

        //        stringTableWorkRequest = GetHTMLTable(stringTableHeader , stringTableRows);
        //    }

        //    return objectEmailTemplate.EmailBody.Replace("[WorkRequestList]" , stringTableWorkRequest);
        //}

        #endregion
    }
}
