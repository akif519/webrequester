﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CMMS.Model;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Utilities.Common;

namespace CMMS.Utilities.EmailDispatcher
{
    public static class CommonDAL
    {

        public static List<Account> GetAccountInformation(int accStatus)
        {
            try 
            {
                return EmailNotificationService.GetAllClientsFromMasterDB(accStatus, "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
