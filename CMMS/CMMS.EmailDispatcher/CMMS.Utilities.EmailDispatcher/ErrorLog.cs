﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using CMMS.Utilities.Common;

namespace CMMS.Utilities.EmailDispatcher
{
    public static class ErrorLog
    {
        public static void WriteErrorLog(string className, string methodName, string arguments, string errorMessage, string stackTress)
        {
            string fileName = Convert.ToString(DateTime.Now.ToString("dd-MMM-yyy", new System.Globalization.CultureInfo("en-US"))) + ".txt";
            string folderPath = AppDomain.CurrentDomain.BaseDirectory + "/ErrorLogs";
            string monthFolder = DateTime.Now.ToString("MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            string filePath = string.Empty;
            string formatedMessage = string.Empty;

            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                folderPath = folderPath + "/" + monthFolder;

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                filePath = folderPath + "/" + fileName;

                formatedMessage = "Date Time: " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")) + Environment.NewLine;

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    formatedMessage += "Error Message: " + errorMessage + Environment.NewLine;
                }

                formatedMessage += "Class Name: " + className + ", Method Name: " + methodName + ", Arguments: " + arguments + Environment.NewLine;

                if (!string.IsNullOrEmpty(stackTress))
                {
                    formatedMessage += "StackTrace: " + stackTress + Environment.NewLine;
                }

                formatedMessage += "--------------------------------------";

                formatedMessage += Environment.NewLine;

                File.AppendAllText(filePath, formatedMessage);

            }
            catch
            {

            }
        }

        public static void WriteDebugLog(string className, string methodName, string message, string arguments)
        {
            string fileName = Convert.ToString(DateTime.Now.ToString("dd-MMM-yyy", new System.Globalization.CultureInfo("en-US"))) + ".txt";
            string folderPath = AppDomain.CurrentDomain.BaseDirectory + "/DebugLogs";
            string monthFolder = DateTime.Now.ToString("MMM-yyyy", new System.Globalization.CultureInfo("en-US"));
            string filePath = string.Empty;
            string formatedMessage = string.Empty;

            if (!ConfigManager.EnableCustomDebugLog)
            {
                return;
            }

            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                folderPath = folderPath + "/" + monthFolder;

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                filePath = folderPath + "/" + fileName;

                formatedMessage = "Date Time: " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss", new System.Globalization.CultureInfo("en-US")) + Environment.NewLine;

                if (!string.IsNullOrEmpty(message))
                {
                    formatedMessage += "Message: " + message + Environment.NewLine;
                }

                formatedMessage += "Class Name: " + className + ", Method Name: " + methodName + ", Arguments: " + arguments + Environment.NewLine;

                formatedMessage += "--------------------------------------";

                formatedMessage += Environment.NewLine;

                File.AppendAllText(filePath, formatedMessage);

            }
            catch
            {

            }
        }
    }
}
