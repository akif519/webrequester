﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CMMS.DAL;
using CMMS.Service;
using CMMS.Infrastructure;
using CMMS.Model;
using CMMS.Utilities.Common;

namespace CMMS.Utilities.EmailDispatcher
{
    public class MaterialModule : IDisposable
    {
        #region Private Variables

        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));

        private string ReportDirPath = "Report";
        private string PurchaseProposalNormalFile = "PurchaseProposalNormal.xlsx";
        private string PurchaseProposalCriticalFile = "PurchaseProposalCritical.xlsx";

        #endregion

        /// <summary>
        /// Process EmailNotificationRule of Material module
        /// </summary>
        public void ProcessMaterialModule(string clientCode, int dbType)
        {
            try
            {
                List<Emailnotificationrule> listEmailNotificationRule = EmailNotificationService.GetListEmailNotificationRule_Material(clientCode);

                foreach (Emailnotificationrule objectEmailNotificationRule in listEmailNotificationRule)
                {
                    if (objectEmailNotificationRule != null)
                    {
                        SendNotificationMail_Material(objectEmailNotificationRule, clientCode, dbType);
                        RemoveReportDirectory();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("MaterialModule.cs", "ProcessMaterialModule", "ClientID:" + clientCode, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Get list of Employee and send mail
        /// </summary>
        /// <param name="objectEmailNotificationRule"></param>
        private void SendNotificationMail_Material(Emailnotificationrule objectEmailNotificationRule, string clientCode, int dbType)
        {
            string stringFrom = ConfigManager.EmailFrom;
            string stringTo = string.Empty;
            string stringSubjectEnglish = string.Empty, stringSubjectArabic = string.Empty, stringBodyEnglish = string.Empty, stringBodyArabic = string.Empty;
            List<EmailReceiptant> listEmailReceiptant_Employee, listEmailReceiptantEmployee_CriticalProposal, listEmailReceiptantEmployee_NormalProposal;
            string stringSMSBodyEnglish = string.Empty, stringSMSBodyArabic = string.Empty, stringSMSBody = string.Empty;

            string stringApiURL = string.Empty, stringUserName = string.Empty, stringPassword = string.Empty, stringApiId = string.Empty, stringSenderName = string.Empty;
            string stringBody = string.Empty, stringSubject = string.Empty, stringToMobileNo = string.Empty;
            stringFrom = ConfigManager.EmailFrom;
            bool isAppliedToEmail = Convert.ToBoolean(objectEmailNotificationRule.IsAppliedToEmail);
            bool isAppliedToSMS = Convert.ToBoolean(objectEmailNotificationRule.IsAppliedToSMS);

            try
            {
                List<Configuration> listConfiguration = new List<Configuration>();

                using (ServiceContext objContext = new ServiceContext())
                {
                    listConfiguration = objContext.SearchWithClientID<Configuration>(new Configuration(), 0, string.Empty, string.Empty, clientCode).Where(m => m.Module == "SMSNotification").ToList();
                }

                foreach (Configuration objectConfiguration in listConfiguration)
                {
                    switch (objectConfiguration.Key.ToString())
                    {
                        case Common.DataEntity.Constants.SMSGatewayAPIUrl:
                            stringApiURL = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSUserName:
                            stringUserName = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSPassword:
                            stringPassword = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSApiId:
                            stringApiId = objectConfiguration.Value.ToString();
                            break;
                        case Common.DataEntity.Constants.SMSSenderName:
                            stringSenderName = objectConfiguration.Value.ToString();
                            break;
                    }
                }

                GetEmailSubjectAndBody(objectEmailNotificationRule.EmailTemplateName, out stringSubjectEnglish, out stringSubjectArabic, out stringBodyEnglish, out stringBodyArabic, out stringSMSBodyEnglish, out stringSMSBodyArabic, clientCode);

                listEmailReceiptant_Employee = EmailNotificationService.GetListEmployeeByNotificationRuleID(objectEmailNotificationRule.NotificationRuleID, clientCode);

                string stringPurchaseProposalNormalFile = PrepareExcelFile_PurchaseProposalNormal(clientCode, dbType);
                string stringPurchaseProposalCriticalFile = PrepareExcelFile_PurchaseProposalCritical(clientCode, dbType);

                listEmailReceiptantEmployee_CriticalProposal = listEmailReceiptant_Employee.Where(x => x.IsSelectedCriticalProposal.HasValue && x.IsSelectedCriticalProposal.Value).ToList();
                listEmailReceiptantEmployee_NormalProposal = listEmailReceiptant_Employee.Where(x => x.IsSelectedNormalProposal.HasValue && x.IsSelectedNormalProposal.Value).ToList();

                System.Net.Mail.SmtpClient objectSmtpClient = CMMS.Utilities.EmailDispatcher.General.GetSMTPSettings(clientCode);

                if (!string.IsNullOrWhiteSpace(stringPurchaseProposalCriticalFile))
                {

                    foreach (EmailReceiptant objectEmailReceiptant in listEmailReceiptantEmployee_CriticalProposal)
                    {
                        if (objectEmailReceiptant.LanguageCode.ToLower() == "en-us")
                        {
                            stringSubject = stringSubjectEnglish;
                            stringBody = stringBodyEnglish;
                            stringSMSBody = stringSMSBodyEnglish;
                        }
                        else if (objectEmailReceiptant.LanguageCode.ToLower() == "ar-sa")
                        {
                            stringSubject = stringSubjectArabic;
                            stringBody = stringBodyArabic;
                            stringSMSBody = stringSMSBodyArabic;
                        }

                        stringBody = GetReceipantDetailToEmailBody(stringBody, objectEmailReceiptant);
                        stringBody = System.Text.RegularExpressions.Regex.Replace(stringBody, @"(\r|\n)+", "<br />");
                        //stringSMSBody = System.Text.RegularExpressions.Regex.Replace(stringSMSBody, @"(\r|\n)+", "<br />");

                        if (isAppliedToEmail)
                        {
                            stringTo = objectEmailReceiptant.ReceiptantEmail;
                            if (!string.IsNullOrEmpty(stringTo))
                            {
                                if (CMMS.Utilities.EmailDispatcher.General.SendMail(MailFrom: stringFrom, MailTo: objectEmailReceiptant.ReceiptantEmail, Subject: stringSubject, Body: stringBody, Attachment: stringPurchaseProposalCriticalFile, objectSMTP: objectSmtpClient))
                                    EmailNotificationService.UpdateLastEmailSentTime_EmployeeEmailNotificationRule(objectEmailNotificationRule.NotificationRuleID, objectEmailReceiptant.ReceiptantID, clientCode, dbType);
                            }
                        }

                        if (isAppliedToSMS)
                        {
                            stringToMobileNo = objectEmailReceiptant.HandPhone;

                            if (!string.IsNullOrEmpty(stringToMobileNo) && !string.IsNullOrEmpty(stringSMSBody))
                            {
                                if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("clickatell"))
                                {
                                    General.SendSMSFromAPI(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringApiId);
                                }
                                else if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("mobily"))
                                {
                                    General.SendSMSFromDll(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringSenderName);
                                }
                            }
                            else
                            {
                                ErrorLog.WriteErrorLog("MaterialModule.cs", "SendNotificationMail_Material", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending SMS From Material Rules. Mobile Number or SMS Body is Empty.", string.Empty);
                            }
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(stringPurchaseProposalCriticalFile))
                {
                    foreach (EmailReceiptant objectEmailReceiptant in listEmailReceiptantEmployee_NormalProposal)
                    {
                        if (objectEmailReceiptant.LanguageCode.ToLower() == "en-us")
                        {
                            stringSubject = stringSubjectEnglish;
                            stringBody = stringBodyEnglish;
                            stringSMSBody = stringSMSBodyEnglish;
                        }
                        else if (objectEmailReceiptant.LanguageCode.ToLower() == "ar-sa")
                        {
                            stringSubject = stringSubjectArabic;
                            stringBody = stringBodyArabic;
                            stringSMSBody = stringSMSBodyArabic;
                        }

                        stringBody = GetReceipantDetailToEmailBody(stringBody, objectEmailReceiptant);
                        stringBody = System.Text.RegularExpressions.Regex.Replace(stringBody, @"(\r|\n)+", "<br />");
                        //stringSMSBody = System.Text.RegularExpressions.Regex.Replace(stringSMSBody, @"(\r|\n)+", "<br />");

                        if (isAppliedToEmail)
                        {
                            stringTo = objectEmailReceiptant.ReceiptantEmail;
                            if (!string.IsNullOrEmpty(stringTo))
                            {
                                if (CMMS.Utilities.EmailDispatcher.General.SendMail(MailFrom: stringFrom, MailTo: objectEmailReceiptant.ReceiptantEmail, Subject: stringSubject, Body: stringBody, Attachment: stringPurchaseProposalNormalFile, objectSMTP: objectSmtpClient))
                                    EmailNotificationService.UpdateLastEmailSentTime_EmployeeEmailNotificationRule(objectEmailNotificationRule.NotificationRuleID, objectEmailReceiptant.ReceiptantID, clientCode, dbType);
                            }
                        }

                        if (isAppliedToSMS)
                        {
                            stringToMobileNo = objectEmailReceiptant.HandPhone;

                            if (!string.IsNullOrEmpty(stringToMobileNo) && !string.IsNullOrEmpty(stringSMSBody))
                            {
                                if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("clickatell"))
                                {
                                    General.SendSMSFromAPI(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringApiId);
                                }
                                else if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("mobily"))
                                {
                                    General.SendSMSFromDll(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringSenderName);
                                }
                            }
                            else
                            {
                                ErrorLog.WriteErrorLog("MaterialModule.cs", "SendNotificationMail_Material", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending SMS From Material Rules. Mobile Number or SMS Body is Empty.", string.Empty);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while sending mail from Material Module.", ex);
                ErrorLog.WriteErrorLog("MaterialModule.cs", "SendNotificationMail_Material", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending Email or SMS From Material Rules." + ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Create & Prepare Excel file for Purchase Proposal Normal
        /// </summary>
        /// <returns>File path of  Purchase Proposal Normal Report</returns>
        private string PrepareExcelFile_PurchaseProposalNormal(string clientCode, int dbType)
        {

            List<Stockcode_level> listViewPurchaseProposalNormal = EmailNotificationService.GetPurchaseProposalNormal(clientCode, dbType);

            if (listViewPurchaseProposalNormal.Count > 0)
            {

                FileStream stream = null;
                BinaryWriter writer = null;

                try
                {
                    string stringPurchaseProposalNormalFile = ReportDirPath + "//" + PurchaseProposalNormalFile;

                    if (!Directory.Exists(ReportDirPath))
                        Directory.CreateDirectory(ReportDirPath);

                    System.IO.File.WriteAllText(stringPurchaseProposalNormalFile, string.Empty);
                    stream = new FileStream(stringPurchaseProposalNormalFile, FileMode.OpenOrCreate);

                    using (ExcelPackage objectExcelPackage = new ExcelPackage())
                    {
                        //Create the worksheet
                        ExcelWorksheet objectExcelWorksheet = objectExcelPackage.Workbook.Worksheets.Add("Normal");

                        objectExcelWorksheet.Cells["A1"].Value = "Purchase Proposal (based on Reorder and Minimum Levels)";
                        objectExcelWorksheet.Cells["A2"].Value = "Report Date: " + CMMS.Infrastructure.Common.GetEnglishDate(DateTime.Now);

                        objectExcelWorksheet.Cells["A1"].Style.Font.Size = objectExcelWorksheet.Cells["A2"].Style.Font.Size = 12;
                        objectExcelWorksheet.Cells["A1"].Style.Font.Bold = objectExcelWorksheet.Cells["A2"].Style.Font.Bold = true;

                        objectExcelWorksheet.Cells["A4"].Value = "Part No.";
                        objectExcelWorksheet.Cells["B4"].Value = "Part Description";
                        objectExcelWorksheet.Cells["C4"].Value = "Specification";
                        objectExcelWorksheet.Cells["D4"].Value = "Store Code";
                        objectExcelWorksheet.Cells["E4"].Value = "Balance";
                        objectExcelWorksheet.Cells["F4"].Value = "Max Level";
                        objectExcelWorksheet.Cells["G4"].Value = "Reorder Level";
                        objectExcelWorksheet.Cells["H4"].Value = "Min Level";
                        objectExcelWorksheet.Cells["I4"].Value = "Reorder Qty";
                        objectExcelWorksheet.Cells["J4"].Value = "Criticality";

                        //Format the header for column 1-3
                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells["A4:J4"])
                        {
                            General.SetExcelHeaderStyle(objectExcelRange);
                        }

                        int rowNumber = 6;
                        foreach (Stockcode_level objectViewPurchaseProposalNormal in listViewPurchaseProposalNormal)
                        {
                            objectExcelWorksheet.SetValue(rowNumber, 1, objectViewPurchaseProposalNormal.StockNo);
                            objectExcelWorksheet.SetValue(rowNumber, 2, objectViewPurchaseProposalNormal.StockDescription);
                            objectExcelWorksheet.SetValue(rowNumber, 3, objectViewPurchaseProposalNormal.Specification);
                            objectExcelWorksheet.SetValue(rowNumber, 4, objectViewPurchaseProposalNormal.SubStoreCode);
                            objectExcelWorksheet.SetValue(rowNumber, 5, objectViewPurchaseProposalNormal.BalanceStock);
                            objectExcelWorksheet.SetValue(rowNumber, 6, objectViewPurchaseProposalNormal.Max_level);
                            objectExcelWorksheet.SetValue(rowNumber, 7, objectViewPurchaseProposalNormal.Re_order_level);
                            objectExcelWorksheet.SetValue(rowNumber, 8, objectViewPurchaseProposalNormal.Min_level);
                            objectExcelWorksheet.SetValue(rowNumber, 9, objectViewPurchaseProposalNormal.Reorder_qty);
                            objectExcelWorksheet.SetValue(rowNumber, 10, "Normal");
                            rowNumber++;
                        }

                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, rowNumber, 10])
                        {
                            objectExcelRange.AutoFitColumns();
                        }

                        writer = new BinaryWriter(stream);
                        writer.Write(objectExcelPackage.GetAsByteArray());
                    }
                    return stringPurchaseProposalNormalFile;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while preparing Excel File of Purchase Proposal Normal. ", ex);
                    return string.Empty;
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                    if (stream != null)
                        stream.Close();
                }
            }
            else
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Create & Prepare Excel file for Purchase Proposal Critical
        /// </summary>
        /// <returns>File path of  Purchase Proposal Critical Report</returns>
        private string PrepareExcelFile_PurchaseProposalCritical(string clientCode, int dbType)
        {

            List<Stockcode_level> listViewPurchaseProposalCritical = EmailNotificationService.GetPurchaseProposalCritical(clientCode, dbType);

            if (listViewPurchaseProposalCritical.Count > 0)
            {

                FileStream stream = null;
                BinaryWriter writer = null;

                try
                {
                    string stringPurchaseProposalCriticalFile = ReportDirPath + "//" + PurchaseProposalCriticalFile;

                    if (!Directory.Exists(ReportDirPath))
                        Directory.CreateDirectory(ReportDirPath);

                    System.IO.File.WriteAllText(stringPurchaseProposalCriticalFile, string.Empty);
                    stream = new FileStream(stringPurchaseProposalCriticalFile, FileMode.OpenOrCreate);

                    using (ExcelPackage objectExcelPackage = new ExcelPackage())
                    {
                        //Create the worksheet
                        ExcelWorksheet objectExcelWorksheet = objectExcelPackage.Workbook.Worksheets.Add("Normal");

                        objectExcelWorksheet.Cells["A1"].Value = "Purchase Proposal (based on Reorder and Minimum Levels)";
                        objectExcelWorksheet.Cells["A2"].Value = "Report Date: " + CMMS.Infrastructure.Common.GetEnglishDate(DateTime.Now);

                        objectExcelWorksheet.Cells["A1"].Style.Font.Size = objectExcelWorksheet.Cells["A2"].Style.Font.Size = 12;
                        objectExcelWorksheet.Cells["A1"].Style.Font.Bold = objectExcelWorksheet.Cells["A2"].Style.Font.Bold = true;

                        objectExcelWorksheet.Cells["A4"].Value = "Part No.";
                        objectExcelWorksheet.Cells["B4"].Value = "Part Description";
                        objectExcelWorksheet.Cells["C4"].Value = "Specification";
                        objectExcelWorksheet.Cells["D4"].Value = "Store Code";
                        objectExcelWorksheet.Cells["E4"].Value = "Balance";
                        objectExcelWorksheet.Cells["F4"].Value = "Max Level";
                        objectExcelWorksheet.Cells["G4"].Value = "Reorder Level";
                        objectExcelWorksheet.Cells["H4"].Value = "Min Level";
                        objectExcelWorksheet.Cells["I4"].Value = "Reorder Qty";
                        objectExcelWorksheet.Cells["J4"].Value = "Criticality";

                        //Format the header for column 1-3
                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells["A4:J4"])
                        {
                            General.SetExcelHeaderStyle(objectExcelRange);
                        }

                        int rowNumber = 6;
                        foreach (Stockcode_level objectViewPurchaseProposalCritical in listViewPurchaseProposalCritical)
                        {
                            objectExcelWorksheet.SetValue(rowNumber, 1, objectViewPurchaseProposalCritical.StockNo);
                            objectExcelWorksheet.SetValue(rowNumber, 2, objectViewPurchaseProposalCritical.StockDescription);
                            objectExcelWorksheet.SetValue(rowNumber, 3, objectViewPurchaseProposalCritical.Specification);
                            objectExcelWorksheet.SetValue(rowNumber, 4, objectViewPurchaseProposalCritical.SubStoreCode);
                            objectExcelWorksheet.SetValue(rowNumber, 5, objectViewPurchaseProposalCritical.BalanceStock);
                            objectExcelWorksheet.SetValue(rowNumber, 6, objectViewPurchaseProposalCritical.Max_level);
                            objectExcelWorksheet.SetValue(rowNumber, 7, objectViewPurchaseProposalCritical.Re_order_level);
                            objectExcelWorksheet.SetValue(rowNumber, 8, objectViewPurchaseProposalCritical.Min_level);
                            objectExcelWorksheet.SetValue(rowNumber, 9, objectViewPurchaseProposalCritical.Reorder_qty);
                            objectExcelWorksheet.SetValue(rowNumber, 10, "Critical");
                            rowNumber++;
                        }

                        using (ExcelRange objectExcelRange = objectExcelWorksheet.Cells[4, 1, rowNumber, 10])
                        {
                            objectExcelRange.AutoFitColumns();
                        }

                        writer = new BinaryWriter(stream);
                        writer.Write(objectExcelPackage.GetAsByteArray());

                    }
                    return stringPurchaseProposalCriticalFile;
                }
                catch (Exception ex)
                {
                    Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while preparing Excel File of Purchase Proposal Critical. ", ex);
                    return string.Empty;
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                    if (stream != null)
                        stream.Close();
                }
            }
            else
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// Remove temporary Report directory after sending mail
        /// </summary>
        private bool RemoveReportDirectory()
        {
            try
            {
                if (Directory.Exists(ReportDirPath))
                {
                    foreach (string stringFile in Directory.GetFiles(ReportDirPath))
                    {
                        File.Delete(stringFile);
                    }

                    Directory.Delete(ReportDirPath);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        /// <summary>
        /// Get Email Subject & Body in English & Arabic Language from EmailTemplateName
        /// </summary>
        /// <param name="EmailTemplateName">Input EmailTemplateName</param>
        /// <param name="EmailSubjectEnglish">Output Email Subject in English Language</param>
        /// <param name="EmailSubjectArabic">Output Email Subject in English Arabic</param>
        /// <param name="EmailBodyEnglish">Output Email Body in English Language</param>
        /// <param name="EmailBodyArabic">Output Email Body in Arabic Language</param>
        private void GetEmailSubjectAndBody(string EmailTemplateName, out string EmailSubjectEnglish, out string EmailSubjectArabic, out string EmailBodyEnglish, out string EmailBodyArabic, out string SMSBodyEnglish, out string SMSBodyArabic, string clientCode)
        {
            EmailSubjectEnglish = EmailSubjectArabic = EmailBodyEnglish = EmailBodyArabic = SMSBodyEnglish = SMSBodyArabic = string.Empty;
            Emailtemplate objectEmailtemplateEnglish = null, objectEmailtemplateArabic = null;

            if (!string.IsNullOrEmpty(EmailTemplateName))
            {
                List<Emailtemplate> lstObjectEmailTemplate = General.GetEmailTemplateDetailByTemplateName(EmailTemplateName, clientCode);
                if (lstObjectEmailTemplate.Count > 0)
                {
                    objectEmailtemplateEnglish = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "en-us");
                    objectEmailtemplateArabic = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "ar-sa");

                    if (objectEmailtemplateEnglish != null)
                    {
                        EmailSubjectEnglish = objectEmailtemplateEnglish.EmailSubject;
                        EmailBodyEnglish = objectEmailtemplateEnglish.EmailBody;
                        SMSBodyEnglish = objectEmailtemplateEnglish.SMSBody;
                    }

                    if (objectEmailtemplateArabic != null)
                    {
                        EmailSubjectArabic = objectEmailtemplateArabic.EmailSubject;
                        EmailBodyArabic = objectEmailtemplateArabic.EmailBody;
                        SMSBodyArabic = objectEmailtemplateArabic.SMSBody;
                    }
                }
            }
        }

        /// <summary>
        /// Fill Receiptant Detail To Email Body
        /// </summary>
        /// <returns>Email Body</returns>
        private string GetReceipantDetailToEmailBody(string stringEmailTemplateBody, EmailReceiptant objectEmailReceiptant)
        {
            foreach (PropertyInfo objectPropertyInfo in objectEmailReceiptant.GetType().GetProperties())
            {
                string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                if (stringEmailTemplateBody.Contains(strField))
                {
                    object objectTemp = objectPropertyInfo.GetValue(objectEmailReceiptant, null);
                    stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                }
            }

            return stringEmailTemplateBody;
        }

        /// <summary>
        /// Dispose global objects
        /// </summary>
        public void Dispose()
        {

        }
    }
}
