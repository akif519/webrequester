﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using CMMS.Model;
using CMMS.DAL;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.Utilities.Common;
using MobilySendSMS;
using System.Net.Http;


namespace CMMS.Utilities.EmailDispatcher
{
    public class ProcessedRules : IDisposable
    {
        #region Private Variables

        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));

        #endregion

        /// <summary>
        /// Process EmailNotificationRule of Processed rules
        /// </summary>
        public void ProcessProcessedRules(string clientCode)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "ProcessProcessedRules", "ENTRY- ProcessProcessedRules.", "ClientID = " + clientCode);
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "ProcessProcessedRules", "Getting Email Notification Processed Rule...", "ClientID = " + clientCode);
                List<EmailNotificationProcessedRule> listEmailNotificationProcessedRule = EmailNotificationService.GetListEmailNotificationRule_ProceedRules(clientCode);

                if (listEmailNotificationProcessedRule != null && listEmailNotificationProcessedRule.Count > 0)
                {
                    foreach (EmailNotificationProcessedRule objectEmailNotificationProcessedRule in listEmailNotificationProcessedRule)
                    {
                        if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule != null)
                        {
                            SendNotification_ProceedRules(objectEmailNotificationProcessedRule, clientCode);

                            try
                            {
                                ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "Starting UpdateEmailSentStatus_ProceedRules", "ClientID = " + clientCode);
                                EmailNotificationService.UpdateEmailSentStatus_ProceedRules(listEmailNotificationProcessedRule, clientCode);
                                ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "Complted UpdateEmailSentStatus_ProceedRules", "ClientID = " + clientCode);
                            }
                            catch (Exception ex1)
                            {
                                ErrorLog.WriteErrorLog("ProcessedRules.cs", "ProcessProcessedRules", "ClientID:" + clientCode, "Error while updating EmailSentStatus_ProceedRules.", ex1.StackTrace);
                            }
                        }
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "ProcessProcessedRules", "No Email Notification Processed Rule Found.", "Count = 0,ClientID = " + clientCode);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "ProcessProcessedRules", "UnExpected Error Occurred.", "Error = "+ ex.Message);
                ErrorLog.WriteErrorLog("ProcessedRules.cs", "ProcessProcessedRules", "ClientID:" + clientCode, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Get list of Employee and send mail
        /// </summary>
        /// <param name="objectEmailNotificationRule"></param>
        private void SendNotification_ProceedRules(EmailNotificationProcessedRule objectEmailNotificationProcessedRule, string clientCode)
        {
            string stringFrom, stringTo = string.Empty;
            string stringSubjectEnglish = string.Empty, stringSubjectArabic = string.Empty, stringBodyEnglish = string.Empty, stringBodyArabic = string.Empty;
            string stringApiURL = string.Empty, stringUserName = string.Empty, stringPassword = string.Empty, stringApiId = string.Empty, stringSenderName = string.Empty;
            string stringBody = string.Empty, stringSubject = string.Empty, stringToMobileNo = string.Empty;
            string stringSMSBodyEnglish = string.Empty, stringSMSBodyArabic = string.Empty, stringSMSBody = string.Empty;
            stringFrom = ConfigManager.EmailFrom;
            bool isAppliedToEmail = Convert.ToBoolean(objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.IsAppliedToEmail);
            bool isAppliedToSMS = Convert.ToBoolean(objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.IsAppliedToSMS);

            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "ENTRY- SendNotification_ProceedRules", "ClientID = " + clientCode + "Notification Rule ID = " + objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleID);

                List<Configuration> listConfiguration = new List<Configuration>();

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "Getting Configuration data...", "ClientID = " + clientCode);

                using (ServiceContext objContext = new ServiceContext())
                {
                    listConfiguration = objContext.SearchWithClientID<Configuration>(new Configuration(), 0, string.Empty, string.Empty, clientCode).Where(m => m.Module == "SMSNotification").ToList();
                }

                if (listConfiguration != null && listConfiguration.Count > 0)
                {
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "Configuration data found.", "ClientID = " + clientCode);

                    foreach (Configuration objectConfiguration in listConfiguration)
                    {
                        switch (objectConfiguration.Key.ToString())
                        {
                            case Common.DataEntity.Constants.SMSGatewayAPIUrl:
                                stringApiURL = objectConfiguration.Value.ToString();
                                break;
                            case Common.DataEntity.Constants.SMSUserName:
                                stringUserName = objectConfiguration.Value.ToString();
                                break;
                            case Common.DataEntity.Constants.SMSPassword:
                                stringPassword = objectConfiguration.Value.ToString();
                                break;
                            case Common.DataEntity.Constants.SMSApiId:
                                stringApiId = objectConfiguration.Value.ToString();
                                break;
                            case Common.DataEntity.Constants.SMSSenderName:
                                stringSenderName = objectConfiguration.Value.ToString();
                                break;
                        }
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "Configuration data not found.", "ClientID = " + clientCode);
                }


                GetEmailSubjectAndBody(objectEmailNotificationProcessedRule, out stringSubjectEnglish, out stringSubjectArabic, out stringBodyEnglish, out stringBodyArabic, out stringSMSBodyEnglish, out stringSMSBodyArabic, clientCode);
                System.Net.Mail.SmtpClient objectSmtpClient = CMMS.Utilities.EmailDispatcher.General.GetSMTPSettings(clientCode);

                foreach (EmailReceiptant objectEmailReceiptant in objectEmailNotificationProcessedRule.ListEmailReceiptant_Employee)
                {
                    if (objectEmailReceiptant.LanguageCode.ToLower() == "en-us")
                    {
                        stringSubject = stringSubjectEnglish;
                        stringBody = stringBodyEnglish;
                        stringSMSBody = stringSMSBodyEnglish;
                    }
                    else if (objectEmailReceiptant.LanguageCode.ToLower() == "ar-sa")
                    {
                        stringSubject = stringSubjectArabic;
                        stringBody = stringBodyArabic;
                        stringSMSBody = stringSMSBodyArabic;
                    }

                    stringBody = GetReceipantDetailToEmailBody(stringBody, objectEmailReceiptant);
                    stringBody = System.Text.RegularExpressions.Regex.Replace(stringBody, @"(\r|\n)+", "<br />");
                    //stringSMSBody = System.Text.RegularExpressions.Regex.Replace(stringSMSBody, @"(\r|\n)+", "<br />");

                    if (isAppliedToEmail)
                    {
                        stringTo = objectEmailReceiptant.ReceiptantEmail;
                        if (!string.IsNullOrEmpty(stringTo))
                        {                           
                            General.SendMail(MailFrom: stringFrom, MailTo: stringTo, Subject: stringSubject, Body: stringBody, objectSMTP: objectSmtpClient);
                        }
                    }

                     if (isAppliedToSMS)
                    {
                        stringToMobileNo = objectEmailReceiptant.HandPhone;

                        if (!string.IsNullOrEmpty(stringToMobileNo) && !string.IsNullOrEmpty(stringSMSBody))
                        {
                            if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("clickatell"))
                            {
                                General.SendSMSFromAPI(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringApiId);
                            }
                            else if (!string.IsNullOrEmpty(stringApiURL) && stringApiURL.Contains("mobily"))
                            {
                                General.SendSMSFromDll(stringUserName, stringPassword, stringSMSBody, stringToMobileNo, stringSenderName);
                            }
                        }
                        else
                        {
                            ErrorLog.WriteErrorLog("ProcessedRules.cs", "SendNotification_ProceedRules", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending SMS From ProceedRules. Mobile Number or SMS Body is Empty.", string.Empty);
                        }
                    }
                }

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "SendNotification_ProceedRules", "EXIT-SendNotification_ProceedRules.", "ClientID = " + clientCode + "Notification Rule ID = " + objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleID);

            }
            catch (Exception ex)
            {
                Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while sending Email From ProceedRules. ", ex);
                ErrorLog.WriteErrorLog("ProcessedRules.cs", "SendNotification_ProceedRules", "ClientID:" + clientCode + ",isAppliedToEmail:" + isAppliedToEmail + ",isAppliedToSMS:" + isAppliedToSMS + ",MailTo:" + stringTo + ",ToMobileNo:" + stringToMobileNo, "Error while sending Email or SMS From ProceedRules." + ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Get EmailTemplate body replaced TemplateFields by original Work Request values
        /// </summary>
        private string GetEmailBodyByEmailTemplate_WorkRequestProcessedRule(string stringEmailTemplateBody, EmailTemplateWorkRequest objectEmailTemplateWorkRequest)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkRequestProcessedRule", "ENTRY- GetEmailBodyByEmailTemplate_WorkRequestProcessedRule.", string.Empty);

                if (stringEmailTemplateBody == null)
                {
                    stringEmailTemplateBody = string.Empty;
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkRequestProcessedRule", "Template Body Found Empty.", string.Empty);
                }

                foreach (PropertyInfo objectPropertyInfo in objectEmailTemplateWorkRequest.GetType().GetProperties())
                {
                    string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                    if (stringEmailTemplateBody != null && stringEmailTemplateBody.Contains(strField))
                    {
                        object objectTemp = objectPropertyInfo.GetValue(objectEmailTemplateWorkRequest, null);
                        stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                    }
                }

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkRequestProcessedRule", "EXIT- GetEmailBodyByEmailTemplate_WorkRequestProcessedRule", string.Empty);
                return stringEmailTemplateBody;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkRequestProcessedRule", "UnExpected Error Occurred.", "Error = " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get EmailTemplate body replaced TemplateFields by original Work Order values
        /// </summary>
        private string GetEmailBodyByEmailTemplate_WorkOrderProcessedRule(string stringEmailTemplateBody, EmailTemplateWorkOrder objectEmailTemplateWorkOrder)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", "ENTRY- GetEmailBodyByEmailTemplate_WorkOrderProcessedRule.", string.Empty);

                if (stringEmailTemplateBody == null)
                {
                    stringEmailTemplateBody = string.Empty;
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", "Template Body Found Empty.", string.Empty);
                }

                foreach (PropertyInfo objectPropertyInfo in objectEmailTemplateWorkOrder.GetType().GetProperties())
                {
                    string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                    if (stringEmailTemplateBody != null && stringEmailTemplateBody.Contains(strField))
                    {
                        object objectTemp = objectPropertyInfo.GetValue(objectEmailTemplateWorkOrder, null);
                        stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                    }
                }

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", "EXIT- GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", string.Empty);
                return stringEmailTemplateBody;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", "UnExpected Error Occurred.", "Error = " + ex.Message);
                throw ex;
            }
        }

        private string GetEmailBodyByEmailTemplate_SupplierProcessedRule(string stringEmailTemplateBody, EmailTemplateSupplier objectEmailTemplateSupplier)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_SupplierProcessedRule", "ENTRY- GetEmailBodyByEmailTemplate_SupplierProcessedRule.", string.Empty);

                if (stringEmailTemplateBody == null)
                {
                    stringEmailTemplateBody = string.Empty;
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_SupplierProcessedRule", "Template Body Found Empty.", string.Empty);
                }

                foreach (PropertyInfo objectPropertyInfo in objectEmailTemplateSupplier.GetType().GetProperties())
                {
                    string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                    if (stringEmailTemplateBody != null && stringEmailTemplateBody.Contains(strField))
                    {
                        object objectTemp = objectPropertyInfo.GetValue(objectEmailTemplateSupplier, null);
                        stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                    }
                }

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_SupplierProcessedRule", "EXIT- GetEmailBodyByEmailTemplate_SupplierProcessedRule", string.Empty);
                return stringEmailTemplateBody;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailBodyByEmailTemplate_WorkOrderProcessedRule", "UnExpected Error Occurred.", "Error = " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get Email Subject & Body in English & Arabic Language from EmailTemplateName
        /// </summary>
        /// <param name="objectEmailNotificationProcessedRule">Input object of EmailNotificationProcessedRule</param>
        /// <param name="EmailSubjectEnglish">Output Email Subject in English Language</param>
        /// <param name="EmailSubjectArabic">Output Email Subject in English Arabic</param>
        /// <param name="EmailBodyEnglish">Output Email Body in English Language</param>
        /// <param name="EmailBodyArabic">Output Email Body in Arabic Language</param>
        private void GetEmailSubjectAndBody(EmailNotificationProcessedRule objectEmailNotificationProcessedRule, out string EmailSubjectEnglish, out string EmailSubjectArabic, out string EmailBodyEnglish, out string EmailBodyArabic, out string SMSBodyEnglish, out string SMSBodyArabic, string clientCode)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "ENTRY- GetEmailSubjectAndBody", "ClientID = " + clientCode);

                EmailSubjectEnglish = EmailSubjectArabic = EmailBodyEnglish = EmailBodyArabic = SMSBodyEnglish = SMSBodyArabic = string.Empty;
                Emailtemplate objectEmailtemplateEnglish = null, objectEmailtemplateArabic = null;

                if (!string.IsNullOrEmpty(objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.EmailTemplateName))
                {
                    List<Emailtemplate> lstObjectEmailTemplate = General.GetEmailTemplateDetailByTemplateName(objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.EmailTemplateName, clientCode);
                    if (lstObjectEmailTemplate.Count > 0)
                    {
                        objectEmailtemplateEnglish = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "en-us");
                        objectEmailtemplateArabic = lstObjectEmailTemplate.FirstOrDefault(item => item.LanguageRef.ToLower() == "ar-sa");

                        if (objectEmailtemplateEnglish != null)
                        {
                            EmailSubjectEnglish = objectEmailtemplateEnglish.EmailSubject;
                            if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode())
                            {
                                EmailBodyEnglish = GetEmailBodyByEmailTemplate_WorkRequestProcessedRule(objectEmailtemplateEnglish.EmailBody, objectEmailNotificationProcessedRule.ObjectWorkRequest);
                                SMSBodyEnglish = GetEmailBodyByEmailTemplate_WorkRequestProcessedRule(objectEmailtemplateEnglish.SMSBody, objectEmailNotificationProcessedRule.ObjectWorkRequest);
                            }
                            else if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode())
                            {
                                EmailBodyEnglish = GetEmailBodyByEmailTemplate_WorkOrderProcessedRule(objectEmailtemplateEnglish.EmailBody, objectEmailNotificationProcessedRule.ObjectWorkOrder);
                                SMSBodyEnglish = GetEmailBodyByEmailTemplate_WorkOrderProcessedRule(objectEmailtemplateEnglish.SMSBody, objectEmailNotificationProcessedRule.ObjectWorkOrder);
                            }
                            else if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.Supplier.GetHashCode())
                            {
                                EmailBodyEnglish = GetEmailBodyByEmailTemplate_SupplierProcessedRule(objectEmailtemplateEnglish.EmailBody, objectEmailNotificationProcessedRule.ObjectSupplier);
                                SMSBodyEnglish = GetEmailBodyByEmailTemplate_SupplierProcessedRule(objectEmailtemplateEnglish.SMSBody, objectEmailNotificationProcessedRule.ObjectSupplier);
                            }
                        }
                        else
                        {
                            ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "objectEmailtemplateEnglish Found Null.", "ClientID = " + clientCode + ",Template Name = " + objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.EmailTemplateName);
                        }

                        if (objectEmailtemplateArabic != null)
                        {
                            EmailSubjectArabic = objectEmailtemplateArabic.EmailSubject;
                            if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkRequest.GetHashCode())
                            {
                                EmailBodyArabic = GetEmailBodyByEmailTemplate_WorkRequestProcessedRule(objectEmailtemplateArabic.EmailBody, objectEmailNotificationProcessedRule.ObjectWorkRequest);
                                SMSBodyArabic = GetEmailBodyByEmailTemplate_WorkRequestProcessedRule(objectEmailtemplateArabic.SMSBody, objectEmailNotificationProcessedRule.ObjectWorkRequest);
                            }
                            else if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.WorkOrder.GetHashCode())
                            {
                                EmailBodyArabic = GetEmailBodyByEmailTemplate_WorkOrderProcessedRule(objectEmailtemplateArabic.EmailBody, objectEmailNotificationProcessedRule.ObjectWorkOrder);
                                SMSBodyArabic = GetEmailBodyByEmailTemplate_WorkOrderProcessedRule(objectEmailtemplateArabic.SMSBody, objectEmailNotificationProcessedRule.ObjectWorkOrder);
                            }
                            else if (objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.NotificationRuleModuleID == SystemEnum.ModuleCode.Supplier.GetHashCode())
                            {
                                EmailBodyArabic = GetEmailBodyByEmailTemplate_SupplierProcessedRule(objectEmailtemplateArabic.EmailBody, objectEmailNotificationProcessedRule.ObjectSupplier);
                                SMSBodyArabic = GetEmailBodyByEmailTemplate_SupplierProcessedRule(objectEmailtemplateArabic.SMSBody, objectEmailNotificationProcessedRule.ObjectSupplier);
                            }
                        }
                        else
                        {
                            ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "objectEmailtemplateArabic Found Null.", "ClientID = " + clientCode + ",Template Name = " + objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.EmailTemplateName);
                        }
                    }
                    else
                    {
                        ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "No Email Template Detail Found By Template Name.", "ClientID = " + clientCode + ",Template Name = " + objectEmailNotificationProcessedRule.ObjectEmailNotificationRule.EmailTemplateName);
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "Email Template Name Found Null Or Empty.", "ClientID = " + clientCode);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetEmailSubjectAndBody", "UnExpected Error Occured.", "ClientID = " + clientCode + ",Error = " + ex.Message);
                throw ex;
            }
            
        }

        /// <summary>
        /// Fill Receiptant Detail To Email Body
        /// </summary>
        /// <returns>Email Body</returns>
        private string GetReceipantDetailToEmailBody(string stringEmailTemplateBody, CMMS.Model.EmailReceiptant objectEmailReceiptant)
        {
            try
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetReceipantDetailToEmailBody", "ENTRY- GetReceipantDetailToEmailBody", string.Empty);

                if (stringEmailTemplateBody == null)
                {
                    stringEmailTemplateBody = string.Empty;
                    ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetReceipantDetailToEmailBody", "Template Body Found Empty.", string.Empty);
                }

                foreach (PropertyInfo objectPropertyInfo in objectEmailReceiptant.GetType().GetProperties())
                {
                    string strField = "[" + objectPropertyInfo.Name.Trim() + "]";
                    if (stringEmailTemplateBody != null && stringEmailTemplateBody.Contains(strField))
                    {
                        object objectTemp = objectPropertyInfo.GetValue(objectEmailReceiptant, null);
                        stringEmailTemplateBody = stringEmailTemplateBody.Replace(strField, (objectTemp != null ? objectTemp.ToString() : string.Empty));
                    }
                }

                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetReceipantDetailToEmailBody", "EXIT- GetReceipantDetailToEmailBody.", string.Empty);
                return stringEmailTemplateBody;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("ProcessedRules.cs", "GetReceipantDetailToEmailBody", "UnExpected Error Occured.", "Error = " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Dispose global objects
        /// </summary>
        public void Dispose()
        {

        }
    }
}
