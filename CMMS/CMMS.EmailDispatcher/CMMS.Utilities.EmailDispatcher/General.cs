﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using CMMS.Utilities.Common;
using CMMS.Model;
using CMMS.Infrastructure;
using CMMS.Service;
using CMMS.DAL;
using MobilySendSMS;
using System.Net.Http;

namespace CMMS.Utilities.EmailDispatcher
{
    class General
    {
        #region Variables
        
        // Initialize the logger to log the message from this file only
        private static readonly AppLogger Logger = new AppLogger(typeof(ConfigManager));

        #endregion

        #region General Methods

        /// <summary>
        /// Send Email from Email parameters
        /// </summary>
        public static bool SendMail(string MailFrom , string MailTo , string Subject , string Body , string Attachment = "", string MailCC = "" , string MailBCC = "" , System.Net.Mail.SmtpClient objectSMTP = null)
        {
            try 
            {
                ErrorLog.WriteDebugLog("General.cs", "SendMail", "ENTRY-SendMail", "MailFrom:" + MailFrom + ",MailTo:" + MailTo);

                if (string.IsNullOrEmpty(MailTo))
                {
                    ErrorLog.WriteDebugLog("General.cs", "SendMail", "To Email Address Is Blank.", "MailFrom:" + MailFrom + ",MailTo:" + MailTo);
                    ErrorLog.WriteErrorLog("General.cs", "SendMail", "MailFrom:" + MailFrom + ",MailTo:" + MailTo, "Email cannot be sent. To Email Address Is Blank." , string.Empty);
                    return false;
                }

                string str = string.Empty;

                if (ConfigManager.IsTestMode)
                {
                    MailTo = ConfigManager.EmailTo;
                }

                System.Net.Mail.MailMessage MailMesg = new System.Net.Mail.MailMessage(MailFrom, MailTo);
                //System.Net.Mail.MailMessage MailMesg = new System.Net.Mail.MailMessage(MailFrom, "ravi.goriya@server1.com");

                if (MailCC != string.Empty)
                {
                    string[] mailCC = MailCC.Split(';');
                    foreach (string email in mailCC)
                        MailMesg.CC.Add(email);
                }

                if (MailBCC != string.Empty)
                {
                    //string[] mailBCC = MailBCC.Split(';');
                    //foreach (string email in mailBCC)
                    MailBCC = MailBCC.Replace(";", ",");
                    MailMesg.Bcc.Add(MailBCC);
                }

                if (!string.IsNullOrEmpty(Attachment))
                {
                    string[] attachment = Attachment.Split(';');
                    foreach (string attachFile in attachment)
                    {
                        try
                        {
                            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(attachFile);
                            MailMesg.Attachments.Add(attach);
                        }
                        catch (Exception ex)
                        {
                            Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while attachment adding in Email. ", ex);
                            ErrorLog.WriteErrorLog("General.cs", "SendMail", "MailFrom:" + MailFrom + ",MailTo:" + MailTo, "Error while attachment adding in Email. " + ex.Message, ex.StackTrace);
                        }
                    }
                }

                MailMesg.Subject = Subject;
                MailMesg.Body = Body;
                MailMesg.IsBodyHtml = true;

                if (objectSMTP != null)
                {
                    try
                    {
                        objectSMTP.Send(MailMesg);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLog(Enumerations.LogLevel.ERROR, "Error while sending Email. ", ex);
                        ErrorLog.WriteErrorLog("General.cs", "SendMail", "MailFrom:" + MailFrom + ",MailTo:" + MailTo, "Error while sending Email. " + ex.Message, ex.StackTrace);
                        MailMesg.Dispose();
                        MailMesg = null;
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("General.cs", "SendMail", "objectSMTP Found Null.", "MailFrom:" + MailFrom + ",MailTo:" + MailTo);
                }

                return false;
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("General.cs", "SendMail", "Unknown Error occurred."+ ex.Message + " " + ex.InnerException, "MailFrom:" + MailFrom + ",MailTo:" + MailTo);
                ErrorLog.WriteErrorLog("General.cs", "SendMail", "MailFrom:" + MailFrom + ",MailTo:" + MailTo, "Error while sending Email. " + ex.Message + " " + ex.InnerException, ex.StackTrace);
                return false;
            }
        }

        public static void SendSMSFromDll(string username, string password, string message, string toMobileno, string sender)
        {
            try
            {
                ErrorLog.WriteDebugLog("General.cs", "SendSMSFromDll", "ENTRY-SendSMSFromDll", "ToMobileno:" + toMobileno);

                if (ConfigManager.IsTestMode)
                {
                    toMobileno = ConfigManager.TestMobileNumber;
                }

                SendSMS snd = new SendSMS();
                var result = snd.SendSMS(ref username, ref password, ref message, ref toMobileno, ref sender);

                String strFailedReason = string.Empty;
                switch (result)
                {
                    case "1":
                        //SMS Have Been Send
                        strFailedReason = string.Empty;
                        break;
                    case "2":
                        //balabce is zero (SMS Not Send)
                        strFailedReason = "Balabce is zero (SMS Could Not Be Sent).";
                        break;
                    case "3":
                        //balance not enough (SMS Not Send)
                        strFailedReason = "Insufficient Balance (SMS Could Not Be Sent).";
                        break;
                    case "4":
                        //error in user name (SMS Not Send)
                        strFailedReason = "User name or number is not available (SMS Could Not Be Sent).";
                        break;
                    case "5":
                        //error in password (SMS Not Send)
                        strFailedReason = "Password is incorrect (SMS Could Not Be Sent).";
                        break;
                    case "6":
                        //there is a problem in sending, try again later  (SMS Not Send)
                        strFailedReason = "There is a problem in sending, try again later (SMS Could Not Be Sent).";
                        break;
                    case "10":
                        strFailedReason = "Number of numbers and number of messages not equal (SMS Could Not Be Sent).";
                        break;
                    case "13":
                        strFailedReason = "Unaccepted Username (SMS Could Not Be Sent).";
                        break;
                    case "-1":
                        strFailedReason = "Cannot connect to the server (SMS Could Not Be Sent).";
                        break;
                    case "-2":
                        strFailedReason = "Cannot connect to the server (SMS Could Not Be Sent).";
                        break;
                }

                if(strFailedReason != string.Empty)
                {
                    ErrorLog.WriteDebugLog("General.cs", "SendSMSFromDll", "Error while sending SMS from DLL.", "Reason: " + strFailedReason);
                    ErrorLog.WriteErrorLog("General.cs", "SendSMSFromDll", "ToMobileno:" + toMobileno, "Error while sending SMS from DLL. Reason: " + strFailedReason, string.Empty);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("General.cs", "SendSMSFromDll", "ToMobileno:" + toMobileno, "Error while sending SMS from DLL. " + ex.Message, ex.StackTrace);
                ErrorLog.WriteDebugLog("General.cs", "SendSMSFromDll", "Error while sending SMS from DLL.", "ToMobileno:" + toMobileno);
            }
        }

        public static void SendSMSFromAPI(string username, string password, string message, string toMobileno, string apiid)
        {
            try
            {

                if (ConfigManager.IsTestMode)
                {
                    toMobileno = ConfigManager.TestMobileNumber;
                }

                ErrorLog.WriteDebugLog("General.cs", "SendSMSFromAPI", "ENTRY-SendSMSFromAPI", "ToMobileno:" + toMobileno);

                string URL = "http://api.clickatell.com/http/";
                string urlParameters = "sendmsg?user=" + username + "&password=" + password + "&api_id=" + apiid + "&to=" + toMobileno + "&text=" + message;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                ErrorLog.WriteDebugLog("General.cs", "SendSMSFromAPI", "EXIT-SendSMSFromAPI", "ToMobileno:" + toMobileno);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteErrorLog("ProcessedRules.cs", "SendSMSFromAPI", "ToMobileno:" + toMobileno, "Error while sending SMS from API. " + ex.Message, ex.StackTrace);
                ErrorLog.WriteDebugLog("General.cs", "SendSMSFromAPI", "UnExpexted Erro Occurred.", "ToMobileno:" + toMobileno + ", Error=" + ex.Message);
            }
        }

        /// <summary>
        /// Get SMTP Client Setting from database
        /// </summary>
        public static System.Net.Mail.SmtpClient GetSMTPSettings(string clientCode)
        {
            try 
            {
                ErrorLog.WriteDebugLog("General.cs", "GetSMTPSettings", "Getting SMTP Settings.", "ClientID = " + clientCode);

                System.Net.Mail.SmtpClient objectSmtpClient = new System.Net.Mail.SmtpClient();
                List<Configuration> listConfiguration = new List<Configuration>();
                using (ServiceContext objContext = new ServiceContext())
                {
                    listConfiguration = objContext.SearchWithClientID<Configuration>(new Configuration(), 0, string.Empty, string.Empty, clientCode).ToList();
                }

                if (listConfiguration != null && listConfiguration.Count > 0)
                {
                    ErrorLog.WriteDebugLog("General.cs", "GetSMTPSettings", "SMTP Settings retrived successfully.", "ClientID = " + clientCode);

                    foreach (Configuration objectConfiguration in listConfiguration)
                    {
                        if (objectConfiguration.Key.Equals(Common.DataEntity.Constants.SMTPServer))
                            objectSmtpClient.Host = objectConfiguration.Value;

                        if (objectConfiguration.Key.Equals(Common.DataEntity.Constants.SMTPServerPort))
                        {
                            int _intValue = 0;
                            if (int.TryParse(objectConfiguration.Value, out _intValue))
                                objectSmtpClient.Port = _intValue;
                        }


                        if (objectConfiguration.Key.Equals(Common.DataEntity.Constants.UsesSSL))
                        {
                            int _intValue = 0;
                            if (int.TryParse(objectConfiguration.Value, out _intValue))
                                objectSmtpClient.EnableSsl = Convert.ToBoolean(_intValue);
                        }


                        if (objectConfiguration.Key.Equals(Common.DataEntity.Constants.UsesAuthentication))
                        {
                            int _intValue = 0;
                            if (int.TryParse(objectConfiguration.Value, out _intValue))
                                objectSmtpClient.UseDefaultCredentials = Convert.ToBoolean(_intValue);
                        }
                    }
                }
                else
                {
                    ErrorLog.WriteDebugLog("General.cs", "GetSMTPSettings", "No SMTP Settings Found.", "ClientID = " + clientCode);
                }

                if (listConfiguration.Any(x => x.Key == Common.DataEntity.Constants.SMTPUserName) && listConfiguration.Any(x => x.Key == Common.DataEntity.Constants.SMTPPassword))
                    objectSmtpClient.Credentials = new System.Net.NetworkCredential(listConfiguration.FirstOrDefault(x => x.Key == Common.DataEntity.Constants.SMTPUserName).Value, listConfiguration.FirstOrDefault(x => x.Key == Common.DataEntity.Constants.SMTPPassword).Value);

                return objectSmtpClient;            
            }
            catch(Exception ex)
            {
                ErrorLog.WriteDebugLog("General.cs", "GetSMTPSettings", "UnExpected Error Getting SMTP Settings.", "ClientID = " + clientCode + ",Error = " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get List of Email Template from Email Template Name
        /// </summary>
        public static List<Emailtemplate> GetEmailTemplateDetailByTemplateName(string emailTemplateName, string clientCode)
        {
            try 
            {
                ErrorLog.WriteDebugLog("General.cs", "GetEmailTemplateDetailByTemplateName", "Getting Email Template Detail By Template Name.", "ClientID = " + clientCode + ",Template Name = " + emailTemplateName);
                return EmailNotificationService.GetEmailTemplateByEmailTemplateName(emailTemplateName, clientCode);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("General.cs", "GetEmailTemplateDetailByTemplateName", "UnExpected Error Occurred.", "ClientID = " + clientCode + ",Error = " + ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Set Header Style in Excel
        /// </summary>
        public static void SetExcelHeaderStyle(ExcelRange objectExcelRange)
        {
            try 
            {
                ErrorLog.WriteDebugLog("General.cs", "SetExcelHeaderStyle", "ENTRY-SetExcelHeaderStyle", string.Empty);

                objectExcelRange.Style.Font.Bold = true;
                objectExcelRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                objectExcelRange.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#D9D9D9"));                      //Set Pattern for the background to Solid
                objectExcelRange.Style.Border.BorderAround(ExcelBorderStyle.Medium);

                ErrorLog.WriteDebugLog("General.cs", "SetExcelHeaderStyle", "EXIT-SetExcelHeaderStyle", string.Empty);
            }
            catch (Exception ex)
            {
                ErrorLog.WriteDebugLog("General.cs", "SetExcelHeaderStyle", "UnExpected Error Occurred.", "Error = " + ex.Message);
                throw ex;
            }
        }

        #endregion
    }

}
